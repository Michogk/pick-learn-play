//
//  XMLParser.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/23/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLParser : NSObject<NSXMLParserDelegate>{
    // XML PARSER
    NSXMLParser *rssParser;
    NSMutableArray *articles;
    //    NSMutableDictionary *item;
    NSMutableDictionary *dicWord;
    NSMutableDictionary *dicFolder;
    NSMutableDictionary *dicGrade;
    
    NSString* gradeOfParsedData;
    NSString* checkfForGrade;
    NSString *currentElement;
    NSMutableString *ElementValue;
    NSString *elemntExtractedValue;
    BOOL errorParsing;
    NSData *xmlFile;
    NSString* imageURL;
    
    NSString* parserFolderId;
    NSString* parserWordId;
    NSArray *componentsArr;
    
    NSMutableArray *arrFolderIds;
    NSMutableArray *arrFolderNames;
    NSMutableArray *arrWordIds;
    NSString* CPL;
    
    BOOL gradeIsPurchasedSoParseData;
}
- (void)parseXMLFileAtURL:(NSString *)URL;
@end
