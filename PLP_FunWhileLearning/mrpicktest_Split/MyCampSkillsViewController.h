//
//  MyCampSkillsViewController.h
//  Mr.Pick
//
//  Created by Muhammad Mosib Asad on 10/5/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MyCampSkillsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,AVAudioPlayerDelegate>{

    AVAudioPlayer *myCampSkillsPlayer;
    AVAudioPlayer *campSkillsHearWordPlayer;
    IBOutlet UILabel *CellText;
    IBOutlet UILabel *GrammarRuleLabel;
    IBOutlet UITableView *MyCampSkillsTable;
    
    UITableViewCell * _grammarCell;
    int _kNum;
    NSMutableDictionary *EnglishDic;
    NSMutableDictionary *ArabicDic;
    NSMutableDictionary *characterDic;
    NSMutableArray *_completeWordArray;
//    UILabel *label1;
    
    IBOutlet UIButton *btnCheckAnswer;
    NSString *_correctText;
    
    BOOL prevWordComplete;
    UIView* charactersView;
    int _selectedCell;
    
    int cheeringAnimationCounter;
    int cheeringImageNumber;
    
    UIImageView *cheeringImgView;
    NSTimer *cheeringAnimationTimer;
    UILabel *selectedCellTxt;
    int count;
    int yLine;
    BOOL isSentence;
    NSTimer *cheeringSoundTimer;
    NSTimer *checkTimer;
    int _currentWordIndex;

}

- (IBAction)gotoMainMenu:(id)sender;
@property (retain, nonatomic) IBOutlet UITableView *MyCampSkillsTable;
- (IBAction)ResetContent:(id)sender;
- (IBAction)CheckCellTex:(id)sender;
-(void) changeImage;
- (IBAction)HelpButton:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *lblSynonym;
@property (retain, nonatomic) IBOutlet UILabel *lblOpposite;

- (IBAction)helpURL:(id)sender;
@end
