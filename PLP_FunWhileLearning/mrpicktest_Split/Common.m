//
//  Common.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 22/11/2013.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "Common.h"

@implementation Common

+(NSString*) getPinNumber{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"]) {
        NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
        return pinNumber;
    }
    else
        return Nil;
}


+(void)showAllert{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Test Alert" message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    alert.delegate = self;
    [alert release];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
}

+ (id) loadNibName:(NSString*)nibName owner:(id)owner options:(NSDictionary*)options {
    NSString *name = [Common loadNibName:nibName];
    id cell = [[[NSBundle mainBundle]loadNibNamed:name owner:owner options:options]lastObject];
    return  cell;
}


+ (NSString*) loadNibName:(NSString*)nibName {
    NSString *name = nibName;    
    if([[NSBundle mainBundle] pathForResource:name ofType:@"nib"] == nil)
    {
        name = nibName;
    }
    return name;
}

+(int) getCurrentLanguage{
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    return activeLanguage;
}


@end
