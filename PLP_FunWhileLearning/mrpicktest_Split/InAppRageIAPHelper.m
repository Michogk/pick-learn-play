//
//  InAppRageIAPHelper.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 12/16/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "InAppRageIAPHelper.h"

@implementation InAppRageIAPHelper

static InAppRageIAPHelper * _sharedHelper;

+ (InAppRageIAPHelper *) sharedHelper {
    
    if (_sharedHelper != nil) {
        return _sharedHelper;
    }
    _sharedHelper = [[InAppRageIAPHelper alloc] init];
    return _sharedHelper;
    
}

- (id)init {
    NSSet *productIdentifiers = [NSSet setWithObjects:
                                 @"KG1_PD1",
                                 nil];
    
    if ((self = [super initWithProductIdentifiers:productIdentifiers])) {                
        
    }
    return self;
    
}

@end
