//
//  MyRewardsViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRewardsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    int reloadCells;

}
- (IBAction)helpURL:(id)sender;
- (IBAction)HelpSound:(id)sender;
+(MyRewardsViewController*) sharedInstance;
@property (retain, nonatomic) IBOutlet UITableView *firstRewardTableView;
@property (retain, nonatomic) IBOutlet UITableView *secondRewardTableView;
@property(nonatomic, retain) NSMutableArray *rewardsAvailable;
@end
