//
//  MrPDatabase.m
//  Mr.Pick
//
//  Created by Muhammad Mosib Asad on 9/14/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "MrPReport.h"
#import "Common.h"

#ifdef Map_G1
static NSString *_dbname=@"G1.sqlite3";
static NSString *_dbnameWithPath=@"G1.sqlite3";
static NSString *_target_name =@"G1";
#else
#ifdef Map_G2
static NSString *_dbname=@"G2.sqlite3";
static NSString *_dbnameWithPath=@"G2.sqlite3";
static NSString *_target_name =@"G2";
#else
#ifdef Map_G3
static NSString *_dbname=@"G3.sqlite3";
static NSString *_dbnameWithPath=@"G3.sqlite3";
static NSString *_target_name =@"G3";
#else
#ifdef Map_G4
static NSString *_dbname=@"G4.sqlite3";
static NSString *_dbnameWithPath=@"G4.sqlite3";
static NSString *_target_name =@"G4";
#else
#ifdef Map_KG1
static NSString *_dbname=@"KG1.sqlite3";
static NSString *_dbnameWithPath=@"KG1.sqlite3";
static NSString *_target_name =@"KG1";
#else
#ifdef Map_KG2
static NSString *_dbname=@"KG2.sqlite3";
static NSString *_dbnameWithPath=@"KG2.sqlite3";
static NSString *_target_name =@"KG2";
#else
#ifdef Map_KG0
static NSString *_dbname=@"KG0.sqlite3";
static NSString *_dbnameWithPath=@"KG0.sqlite3";
static NSString *_target_name =@"KG0";
#else
#ifdef School_KG0
static NSString *_dbname=@"KG0.sqlite3";
static NSString *_dbnameWithPath=@"KG0.sqlite3";
static NSString *_target_name =@"KG0";
#else
#ifdef School_KG1
static NSString *_dbname=@"KG1.sqlite3";
static NSString *_dbnameWithPath=@"KG1.sqlite3";
static NSString *_target_name =@"KG1";
#else
#ifdef School_KG2
static NSString *_dbname=@"KG2.sqlite3";
static NSString *_dbnameWithPath=@"KG2.sqlite3";
static NSString *_target_name =@"KG2";
#else
#ifdef School_G1
static NSString *_dbname=@"G1.sqlite3";
static NSString *_dbnameWithPath=@"G1.sqlite3";
static NSString *_target_name =@"G1";
#else
#ifdef School_G2
static NSString *_dbname=@"G2.sqlite3";
static NSString *_dbnameWithPath=@"G2.sqlite3";
static NSString *_target_name =@"G2";
#else
#ifdef School_G3
static NSString *_dbname=@"G3.sqlite3";
static NSString *_dbnameWithPath=@"G3.sqlite3";
static NSString *_target_name =@"G3";
#else
#ifdef School_G4
static NSString *_dbname=@"G4.sqlite3";
static NSString *_dbnameWithPath=@"G4.sqlite3";
static NSString *_target_name =@"G4";
#else
static NSString *_dbname=@"G1.sqlite3";
static NSString *_dbnameWithPath=@"G1.sqlite3";
static NSString *_target_name =@"G1";
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif





@implementation MrPDatabase

static MrPDatabase *_database;
//static NSString *_dbname=@"G1.sqlite3";
static NSString *_ArSQL = @"";
static NSString *_EnSQL = @"";

static NSString *_ArSQLForMainMenu = @"";
static NSString *_EnSQLForMainMenu = @"";

static NSString *_ArSkillSQL = @"";
static NSString *_EnSkillSQL = @"";
static NSString *_CurrentGrade=@"";
static NSString *_PrincipleSQL=@"";
static NSString *_GrammarSQL=@"";




+ (MrPDatabase*)database {

    
    if (_database == nil) {
        _database = [[MrPDatabase alloc] init];
    }
    return _database;
}

- (id)init {
    
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    
    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"harkat"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    int mainMenuPrincipleId = [[NSUserDefaults standardUserDefaults] integerForKey:@"MainMenuPrincipleId"];
    int mainMenuGrammarId = [[NSUserDefaults standardUserDefaults] integerForKey:@"MainMenuGrammarId"];
    
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSLog(@"%@",ActiveGrade);
    ActiveGrade = _target_name;
    if([ActiveGrade isEqual:Nil] || [ActiveGrade isEqualToString:@"FREE"] || !ActiveGrade){
        if(([[NSUserDefaults standardUserDefaults] boolForKey:@"harkat"] || (activeLanguage == 2)) || (cId > 989 && cId < 999) || (cId == 99999)){
//            _dbname =@"G1";

            }
        else{
//            _dbname =@"G1.sqlite3";
            
        }
        NSLog(@"dbName decided is %@",_dbname);
        _ArSQL =[NSString stringWithFormat:@"SELECT word_id, arabic_word, Is_Sentence, image_id, voice_id, translate, english_Ref FROM arabic_word WHERE classification_id = %d order by word_id",cId];
        _EnSQL =[NSString stringWithFormat:@"SELECT word_id, english_word, lang_dir, arabic_Ref, Principle_group_id, Is_Sentence, translate, sub_classification_id, image_id, voice_id FROM english_word WHERE classification_id = %d order by word_id",cId];
        
        _ArSkillSQL = [NSString stringWithFormat:@"SELECT word_id, arabic_word, image_id, voice_id,Is_Sentence, translate, english_Ref FROM arabic_word WHERE grammar_id = %d AND Principle_id = %d"];
        
        _EnSkillSQL = [NSString stringWithFormat:@"SELECT word_id, english_word, image_id, voice_id,Is_Sentence, translate, arabic_Ref FROM english_word WHERE grammar_id = %d AND Principle_id = %d"];
        
        _PrincipleSQL = [NSString stringWithFormat: @"SELECT Lang_id, principles_id, principles_desc FROM learning_principles"];
        _GrammarSQL=[NSString stringWithFormat: @"SELECT grammar_id, desc FROM learning_grammars WHERE lang_id = %d",activeLanguage ];
    }
    else{
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"harkat"] || (activeLanguage == 2) || (cId > 989 && cId < 999) || (cId == 99999)){
            _dbname =[ActiveGrade stringByAppendingString:@".sqlite3"];
        }
        else{
            _dbname = [NSString stringWithFormat:@"%@.sqlite3",ActiveGrade];
        }

    _CurrentGrade = ActiveGrade;
//    _ArSQL =[NSString stringWithFormat:@"SELECT word_id, arabic_word, Is_Sentence, image_id, voice_id, translate, english_Ref FROM arabic_word WHERE %@=1 and classification_id = %d order by word_id",_CurrentGrade,cId];

        _ArSQL =[NSString stringWithFormat:@"SELECT word_id, arabic_word, Is_Sentence, image_id, voice_id, translate, english_Ref,Principle_id,grammar_id ,Principle_Name,grammar_Name FROM arabic_word WHERE classification_id = %d AND grammar_id = %d AND Principle_id = %d order by word_id",cId,mainMenuGrammarId,mainMenuPrincipleId];
        _EnSQL =[NSString stringWithFormat:@"SELECT word_id, english_word, lang_dir, arabic_Ref, Principle_group_id, Is_Sentence, translate, sub_classification_id, image_id, voice_id,Principle_id,grammar_id,Principle_Name,grammar_Name FROM english_word WHERE classification_id = %d order by word_id",cId];
        
        
        
        
        _ArSQLForMainMenu =[NSString stringWithFormat:@"SELECT word_id, arabic_word, Is_Sentence, image_id, voice_id, translate, english_Ref,Principle_id,grammar_id ,Principle_Name,grammar_Name FROM arabic_word WHERE classification_id = %d order by word_id",cId];
        _EnSQLForMainMenu =[NSString stringWithFormat:@"SELECT word_id, english_word, lang_dir, arabic_Ref, Principle_group_id, Is_Sentence, translate, sub_classification_id, image_id, voice_id,Principle_id,grammar_id,Principle_Name,grammar_Name FROM english_word WHERE classification_id = %d order by word_id",cId];
        

//    _EnSQL =[NSString stringWithFormat:@"SELECT word_id, english_word, lang_dir, arabic_Ref, Principle_group_id, Is_Sentence, translate, sub_classification_id, image_id, voice_id, FROM english_word WHERE %@=1 and classification_id = %d order by word_id",_CurrentGrade,cId];
//        
    _ArSkillSQL = [NSString stringWithFormat:@"SELECT word_id, arabic_word, image_id, voice_id,Is_Sentence, translate, english_Ref FROM arabic_word WHERE %@=1 and grammar_id = %d AND Principle_id = %d",_CurrentGrade];
        
    _EnSkillSQL = [NSString stringWithFormat:@"SELECT word_id, english_word, image_id, voice_id,Is_Sentence, translate, arabic_Ref FROM english_word WHERE %@=1 and grammar_id = %d AND Principle_id = %d",_CurrentGrade];
    _PrincipleSQL = [NSString stringWithFormat: @"SELECT Lang_id, principles_id, principles_desc FROM learning_principles where %@=1",_CurrentGrade];
    _GrammarSQL=[NSString stringWithFormat: @"SELECT grammar_id, desc FROM learning_grammars WHERE %@=1 and lang_id = %d",_CurrentGrade,activeLanguage ];
    }    
    
    if ((self = [super init])) {
        NSString *sqLiteDb = [[NSBundle mainBundle] pathForResource:_dbnameWithPath
                                                             ofType:@"sqlite3"];
        
        if (sqlite3_open([sqLiteDb UTF8String], &_database) != SQLITE_OK) {
            NSLog(@"Failed to open database!");
        }
        
//        [self createEditableCopyOfDatabaseIfNeeded];
        [self checkDatabase];
    }
    return self;
}
// Only for Second Classification
- (NSArray *)fetchAllAvailableClassificationsForSelectedParamters:(int )cId andActiveGrade:(NSString*) activeGrade andActiveLanguage:(NSString*) table{
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query = [NSString stringWithFormat:@"SELECT classification_id FROM %@ WHERE classification_id <> %d AND %@ = 1 AND grammar_id != 3020",table,cId,activeGrade];
    NSLog(@"Query is %@",query);
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
        
    }    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int classifications = sqlite3_column_int(statement, 0);
            
            MrPContentInfo *info = [[[MrPContentInfo alloc]
                                     initWithClassificationIds:classifications] autorelease];
            [retval addObject:info];
        }
        sqlite3_finalize(statement);
    }
    return retval;
}
- (NSArray *)fetchAllAvailableClassificationsForStores{
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSString * table;
    if(activeLanguage == 2)
        table = @"english_word";
    else{
        table = @"arabic_word";
    }


    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    NSString *query = [NSString stringWithFormat:@"SELECT word_id, %@, Is_Sentence, image_id, voice_id, translate,Principle_id,grammar_id ,Principle_Name,grammar_Name,synonym_word,opposite_word FROM %@ WHERE grammar_id == 1350 OR grammar_id == 1360 OR grammar_id == 1630 AND classification_id != %d",table,table,cId];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
        
    }
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSLog(@"Success");
            int wordId = sqlite3_column_int(statement, 0);
            char *arabicWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *arabicWord = [[NSString alloc] initWithUTF8String:arabicWordChar];
            int isSentence = sqlite3_column_int(statement, 2);
            char *voice = (char *) sqlite3_column_text(statement, 4);
            NSString *arabicVoiceId = [[NSString alloc] initWithUTF8String:voice];
            int imageId = sqlite3_column_int(statement, 3);
            int translate = sqlite3_column_int(statement, 5);
            
            
            int principleId = sqlite3_column_int(statement, 6);
            int grammarId = sqlite3_column_int(statement, 7);
            
            char *principleNameChar = (char *) sqlite3_column_text(statement, 8);
            NSString *principleName = [[NSString alloc] initWithUTF8String:principleNameChar];
            
            char *grammarNameChar = (char *) sqlite3_column_text(statement, 9);
            NSString *grammarName = [[NSString alloc] initWithUTF8String:grammarNameChar];
            
            char *synonymChar = (char *) sqlite3_column_text(statement, 10);
            NSString *synonymName = [[NSString alloc] initWithUTF8String:synonymChar];
            
            char *oppositeChar = (char *) sqlite3_column_text(statement, 11);
            NSString *oppositeName = [[NSString alloc] initWithUTF8String:oppositeChar];
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:arabicWord isSentence:isSentence arabicVoiceId: arabicVoiceId imageId:imageId translate:translate andEnglishRef:0 andSynonym:synonymName andOppositeWord:oppositeName andPrincipleId:principleId andGrammarId:grammarId andPrincipleName:principleName andGrammarName:grammarName] autorelease];
            [retval addObject:info];
            [arabicWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

- (NSArray *)fetchAllAvailableClassificationsWithGrammarIdsIncluded:(BOOL)pGIdIncluded{
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSString * table;
    if(activeLanguage == 2)
        table = @"english_word";
        else{
         table = @"arabic_word";
        }
    
    NSString *nonEqualGrammarIds = [self createEqualGrammarStringWithgIDsIncluded:pGIdIncluded];

    NSString *pQuery = [NSString stringWithFormat:@"SELECT word_id, %@, Is_Sentence, image_id, voice_id, translate,Principle_id,grammar_id ,Principle_Name,grammar_Name,synonym_word,opposite_word FROM %@ WHERE %@ AND ALL_PRINCIPLE_IDS ",table,table,nonEqualGrammarIds];
    NSString* query = [self createPrincipleString:pQuery];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
        
    }
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSLog(@"Success");
            int wordId = sqlite3_column_int(statement, 0);
            char *arabicWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *arabicWord = [[NSString alloc] initWithUTF8String:arabicWordChar];
            int isSentence = sqlite3_column_int(statement, 2);
            char *voice = (char *) sqlite3_column_text(statement, 4);
            NSString *arabicVoiceId = [[NSString alloc] initWithUTF8String:voice];
            int imageId = sqlite3_column_int(statement, 3);
            int translate = sqlite3_column_int(statement, 5);
            
            
            int principleId = sqlite3_column_int(statement, 6);
            int grammarId = sqlite3_column_int(statement, 7);
            
            char *principleNameChar = (char *) sqlite3_column_text(statement, 8);
            NSString *principleName = [[NSString alloc] initWithUTF8String:principleNameChar];
            
            char *grammarNameChar = (char *) sqlite3_column_text(statement, 9);
            NSString *grammarName = [[NSString alloc] initWithUTF8String:grammarNameChar];
            
            char *synonymChar = (char *) sqlite3_column_text(statement, 10);
            NSString *synonymName = [[NSString alloc] initWithUTF8String:synonymChar];
            
            char *oppositeChar = (char *) sqlite3_column_text(statement, 11);
            NSString *oppositeName = [[NSString alloc] initWithUTF8String:oppositeChar];
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:arabicWord isSentence:isSentence arabicVoiceId: arabicVoiceId imageId:imageId translate:translate andEnglishRef:0 andSynonym:synonymName andOppositeWord:oppositeName andPrincipleId:principleId andGrammarId:grammarId andPrincipleName:principleName andGrammarName:grammarName] autorelease];
            [retval addObject:info];
            [arabicWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

// We are using it to pick second classification in Move it and pick it
- (NSArray *)databaseForParticularClassification:(int )cId andTable:(NSString*) table {
    
    
    
    NSLog(@"cid is %d",cId);
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    NSLog(@"Database path is %@",databasePath);
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query =[NSString stringWithFormat:@"SELECT word_id, %@, Is_Sentence, image_id, voice_id, translate,Principle_id,grammar_id ,Principle_Name,grammar_Name,synonym_word,opposite_word FROM %@ WHERE (classification_id = %d) & (Principle_id != 3020) order by word_id",table,table,cId];

    NSLog(@"New %@ yes Second classificaiton is:",query);
    //    NSString *query = [NSString stringWithFormat:_ArSQLForPrincipleGrammarRules, cId];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSLog(@"Success");
            int wordId = sqlite3_column_int(statement, 0);
            char *arabicWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *arabicWord = [[NSString alloc] initWithUTF8String:arabicWordChar];
            int isSentence = sqlite3_column_int(statement, 2);
            char *voice = (char *) sqlite3_column_text(statement, 4);
            NSString *arabicVoiceId = [[NSString alloc] initWithUTF8String:voice];
            int imageId = sqlite3_column_int(statement, 3);
            int translate = sqlite3_column_int(statement, 5);

            
            int principleId = sqlite3_column_int(statement, 6);
            int grammarId = sqlite3_column_int(statement, 7);
            
            char *principleNameChar = (char *) sqlite3_column_text(statement, 8);
            NSString *principleName = [[NSString alloc] initWithUTF8String:principleNameChar];
            
            char *grammarNameChar = (char *) sqlite3_column_text(statement, 9);
            NSString *grammarName = [[NSString alloc] initWithUTF8String:grammarNameChar];
            
            char *synonymChar = (char *) sqlite3_column_text(statement, 10);
            NSString *synonymName = [[NSString alloc] initWithUTF8String:synonymChar];
            
            char *oppositeChar = (char *) sqlite3_column_text(statement, 11);
            NSString *oppositeName = [[NSString alloc] initWithUTF8String:oppositeChar];
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:arabicWord isSentence:isSentence arabicVoiceId: arabicVoiceId imageId:imageId translate:translate andEnglishRef:0 andSynonym:synonymName andOppositeWord:oppositeName andPrincipleId:principleId andGrammarId:grammarId andPrincipleName:principleName andGrammarName:grammarName] autorelease];
            [retval addObject:info];
            [arabicWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
    
}


- (NSArray *)EnglishWordDatabaseArray:(int )cId {

    
    NSLog(@"cid is %d",cId);
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    NSLog(@"Database path is %@",databasePath);
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    
    NSString *query = [self createQuery:[NSString stringWithFormat:@"SELECT word_id, english_word, Is_Sentence, image_id, voice_id, translate,Principle_id,grammar_id ,synonym_word,opposite_word FROM english_word WHERE ALL_CLASSIFICATIONS_IDS AND ALL_GRAMMAR_IDS AND ALL_PRINCIPLE_IDS order by word_id"]];
    
    NSLog(@"New %@ yes",query);
    //    NSString *query = [NSString stringWithFormat:_ArSQLForPrincipleGrammarRules, cId];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSLog(@"Success");
            int wordId = sqlite3_column_int(statement, 0);
            char *arabicWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *arabicWord = [[NSString alloc] initWithUTF8String:arabicWordChar];
            int isSentence = sqlite3_column_int(statement, 2);
            char *voice = (char *) sqlite3_column_text(statement, 4);
            NSString *arabicVoiceId = [[NSString alloc] initWithUTF8String:voice];
            int imageId = sqlite3_column_int(statement, 3);
            int translate = sqlite3_column_int(statement, 5);

            
            int principleId = sqlite3_column_int(statement, 6);
            int grammarId = sqlite3_column_int(statement, 7);
            
//            char *principleNameChar = (char *) sqlite3_column_text(statement, 9);
//            NSString *principleName = [[NSString alloc] initWithUTF8String:principleNameChar];
//            
//            char *grammarNameChar = (char *) sqlite3_column_text(statement, 10);
//            NSString *grammarName = [[NSString alloc] initWithUTF8String:grammarNameChar];
            
            char *synonymChar = (char *) sqlite3_column_text(statement, 8);
            NSString *synonymName = [[NSString alloc] initWithUTF8String:synonymChar];
            
            char *oppositeChar = (char *) sqlite3_column_text(statement, 9);
            NSString *oppositeName = [[NSString alloc] initWithUTF8String:oppositeChar];
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:arabicWord isSentence:isSentence arabicVoiceId: arabicVoiceId imageId:imageId translate:translate andEnglishRef:0 andSynonym:synonymName andOppositeWord:oppositeName andPrincipleId:principleId andGrammarId:grammarId andPrincipleName:0 andGrammarName:0] autorelease];
            [retval addObject:info];
            [arabicWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
    
}

- (NSArray *)loadAllClassificaton{
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
//    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 1) {
        query = @"SELECT image_id, classification_id FROM arabic_word WHERE Classification_id < 900";
    }else{
        query = @"SELECT image_id, classification_id FROM english_word WHERE Classification_id < 900";
    }


    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
        
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {

            int imageId = sqlite3_column_int(statement, 0);
            int classificationId = sqlite3_column_int(statement, 1);
            
            MrPContentInfo *info = [[MrPContentInfo alloc]
                                     initWithimageId:imageId classificationId:classificationId];
            [retval addObject:info];
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

- (NSArray *)loadImageForAvailableClassifications:(NSArray*) pCIdArray{
    
    
    NSMutableString *classificationsStr = [NSMutableString string];
    classificationsStr = [Constants createQueryString:pCIdArray];

    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query = [NSString stringWithFormat:@"SELECT sub_id, Classification_id,desc FROM Classifications WHERE %@ order by Classification_id",classificationsStr];
    NSLog(@"@@@@@@@@@@@@@@@@@@@@ %@",query);
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
        
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            int imageId = sqlite3_column_int(statement, 0);
            int classificationId = sqlite3_column_int(statement, 1);
            
            char *descChar = (char *) sqlite3_column_text(statement, 2);
            NSString *classificationName = [[NSString alloc] initWithUTF8String:descChar];
            NSLog(@"%@",classificationName);
            
            MrPContentInfo *info = [[MrPContentInfo alloc]
                                    initWithimageId:imageId classificationId:classificationId andClassificationName:classificationName];
            [retval addObject:info];
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

- (NSArray *)EnglishWordDatabaseArrayForMainMenu:(NSArray* )cIdArray {
    
    
    NSMutableString *classificationsStr = [NSMutableString string];
    classificationsStr = [Constants createQueryString:cIdArray];
    
    _ArSQLForMainMenu =[NSString stringWithFormat:@"SELECT word_id, english_word, Is_Sentence, image_id, voice_id, translate,Principle_id,grammar_id ,Principle_Name,grammar_Name FROM english_word WHERE %@ order by word_id",classificationsStr];
    
    NSLog(@"%@",_ArSQLForMainMenu);
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    NSLog(@"Database path is %@",databasePath);
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    
    NSString *query = _ArSQLForMainMenu;
    NSLog(@"%@",query);
    //    NSString *query = [NSString stringWithFormat:_ArSQLForPrincipleGrammarRules, cId];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            //            NSLog(@"Success");
            int wordId = sqlite3_column_int(statement, 0);
            char *arabicWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *arabicWord = [[NSString alloc] initWithUTF8String:arabicWordChar];
            int isSentence = sqlite3_column_int(statement, 2);
            char *voice = (char *) sqlite3_column_text(statement, 4);
            NSString *arabicVoiceId = [[NSString alloc] initWithUTF8String:voice];
            int imageId = sqlite3_column_int(statement, 3);
            int translate = sqlite3_column_int(statement, 5);

            
            int principleId = sqlite3_column_int(statement, 6);
            int grammarId = sqlite3_column_int(statement, 7);
            
            char *principleNameChar = (char *) sqlite3_column_text(statement, 8);
            NSString *principleName = [[NSString alloc] initWithUTF8String:principleNameChar];
            
            char *grammarNameChar = (char *) sqlite3_column_text(statement, 9);
            NSString *grammarName = [[NSString alloc] initWithUTF8String:grammarNameChar];
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:arabicWord isSentence:isSentence arabicVoiceId: arabicVoiceId imageId:imageId translate:translate andEnglishRef:@"" andSynonym:@"0" andOppositeWord:@"0" andPrincipleId:principleId andGrammarId:grammarId andPrincipleName:principleName andGrammarName:grammarName] autorelease];
            [retval addObject:info];
            [arabicWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
}



- (NSArray *)ArabicWordDatabaseArrayForMainMenu:(NSArray* )cIdArray {
    
    
    NSMutableString *classificationsStr = [NSMutableString string];
    classificationsStr = [Constants createQueryString:cIdArray];

    _ArSQLForMainMenu =[NSString stringWithFormat:@"SELECT word_id, arabic_word, Is_Sentence, image_id, voice_id, translate, english_Ref,Principle_id,grammar_id ,Principle_Name,grammar_Name FROM arabic_word WHERE %@ order by word_id",classificationsStr];

    NSLog(@"%@",_ArSQLForMainMenu);
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    NSLog(@"Database path is %@",databasePath);
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    
    NSString *query = _ArSQLForMainMenu;
    NSLog(@"%@",query);
    //    NSString *query = [NSString stringWithFormat:_ArSQLForPrincipleGrammarRules, cId];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
//            NSLog(@"Success");
            int wordId = sqlite3_column_int(statement, 0);
            char *arabicWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *arabicWord = [[NSString alloc] initWithUTF8String:arabicWordChar];
            int isSentence = sqlite3_column_int(statement, 2);
            char *voice = (char *) sqlite3_column_text(statement, 4);
            NSString *arabicVoiceId = [[NSString alloc] initWithUTF8String:voice];
            int imageId = sqlite3_column_int(statement, 3);
            int translate = sqlite3_column_int(statement, 5);
            int englishRef = sqlite3_column_int(statement, 6);
            
            int principleId = sqlite3_column_int(statement, 7);
            int grammarId = sqlite3_column_int(statement, 8);
            
            char *principleNameChar = (char *) sqlite3_column_text(statement, 9);
            NSString *principleName = [[NSString alloc] initWithUTF8String:principleNameChar];
            
            char *grammarNameChar = (char *) sqlite3_column_text(statement, 10);
            NSString *grammarName = [[NSString alloc] initWithUTF8String:grammarNameChar];
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:arabicWord isSentence:isSentence arabicVoiceId: arabicVoiceId imageId:imageId translate:translate andEnglishRef:englishRef andSynonym:@"0" andOppositeWord:@"0" andPrincipleId:principleId andGrammarId:grammarId andPrincipleName:principleName andGrammarName:grammarName] autorelease];
            [retval addObject:info];
            [arabicWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

-(NSMutableString*) createQuery:(NSString*) pQuery{
    NSMutableString *classificationsStr = [NSMutableString string];
    NSArray *cIdArray = [[NSArray alloc] init];
    cIdArray = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    NSMutableString *queryString = [NSMutableString string];


    queryString = [Constants createQueryString:cIdArray];
    
    [classificationsStr appendString:pQuery];

    [classificationsStr replaceOccurrencesOfString:@"ALL_CLASSIFICATIONS_IDS" withString:queryString options:0 range:NSMakeRange(0, [classificationsStr length])];
    NSMutableString *newString  = [self createGrammarString:classificationsStr];
    return newString;
}

-(NSMutableString*) createGrammarString:(NSString*) pQuery{
    NSMutableString *classificationsStr = [NSMutableString string];
    NSArray *cIdArray = [[NSArray alloc] init];
    cIdArray = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
    NSLog(@"array is %@",cIdArray);
    NSMutableString *queryString = [NSMutableString string];
    NSLog(@"Number 1");
    if([cIdArray isKindOfClass:[NSArray class]] == YES){
        if (!cIdArray || cIdArray == NULL || [cIdArray isEqual:@""]) {
            return classificationsStr;
        }
    }
    NSLog(@"Number 2");
    queryString = [Constants createGrammarIdString:cIdArray];
    NSLog(@"Number 3");
    [classificationsStr appendString:pQuery];
    [classificationsStr replaceOccurrencesOfString:@"ALL_GRAMMAR_IDS" withString:queryString options:0 range:NSMakeRange(0, [classificationsStr length])];
    NSLog(@"Number 4");
    NSMutableString *newString = [self createPrincipleString:classificationsStr];
    NSLog(@"Number 5");
    return newString;
}



-(NSMutableString*) createPrincipleString:(NSString*) pQuery{
    NSMutableString *classificationsStr = [NSMutableString string];
    NSArray *cIdArray = [[NSArray alloc] init];
    cIdArray = [[NSUserDefaults standardUserDefaults] objectForKey:PRINCIPLE_ID_ARRAY];
    if([cIdArray isKindOfClass:[NSArray class]] == YES){
        if (!cIdArray || cIdArray == NULL || [cIdArray isEqual:@""]) {
            return classificationsStr;
        }
    }

    NSMutableString *queryString = [NSMutableString string];
    queryString = [Constants createPrincipleIdString:cIdArray];
    NSLog(@"Query string 1.0 is %@",queryString);
    [classificationsStr appendString:pQuery];
    [classificationsStr replaceOccurrencesOfString:@"ALL_PRINCIPLE_IDS" withString:queryString options:0 range:NSMakeRange(0, [classificationsStr length])];

    return classificationsStr;
}

- (NSArray *)ArabicWordDatabaseArray:(int )cId {
    NSLog(@"cid is %d",cId);
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    NSLog(@"Database path is %@",databasePath);
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    
    NSString *query = [self createQuery:[NSString stringWithFormat:@"SELECT word_id, arabic_word, Is_Sentence, image_id, voice_id, translate, english_Ref,Principle_id,grammar_id ,Principle_Name,grammar_Name,synonym_word,opposite_word FROM arabic_word WHERE ALL_CLASSIFICATIONS_IDS AND ALL_GRAMMAR_IDS AND ALL_PRINCIPLE_IDS order by word_id"]];
    
    NSLog(@"New %@ yes",query);
//    NSString *query = [NSString stringWithFormat:_ArSQLForPrincipleGrammarRules, cId];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            NSLog(@"Success");
            int wordId = sqlite3_column_int(statement, 0);            
            char *arabicWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *arabicWord = [[NSString alloc] initWithUTF8String:arabicWordChar];
            int isSentence = sqlite3_column_int(statement, 2);            
            char *voice = (char *) sqlite3_column_text(statement, 4);
            NSString *arabicVoiceId = [[NSString alloc] initWithUTF8String:voice];            
            int imageId = sqlite3_column_int(statement, 3);
            int translate = sqlite3_column_int(statement, 5);
            int englishRef = sqlite3_column_int(statement, 6);
            
            int principleId = sqlite3_column_int(statement, 7);
            int grammarId = sqlite3_column_int(statement, 8);

            char *principleNameChar = (char *) sqlite3_column_text(statement, 9);
            NSString *principleName = [[NSString alloc] initWithUTF8String:principleNameChar];

            char *grammarNameChar = (char *) sqlite3_column_text(statement, 10);
            NSString *grammarName = [[NSString alloc] initWithUTF8String:grammarNameChar];

            char *synonymChar = (char *) sqlite3_column_text(statement, 11);
            NSString *synonymName = [[NSString alloc] initWithUTF8String:synonymChar];

            char *oppositeChar = (char *) sqlite3_column_text(statement, 12);
            NSString *oppositeName = [[NSString alloc] initWithUTF8String:oppositeChar];

            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:arabicWord isSentence:isSentence arabicVoiceId: arabicVoiceId imageId:imageId translate:translate andEnglishRef:englishRef andSynonym:synonymName andOppositeWord:oppositeName andPrincipleId:principleId andGrammarId:grammarId andPrincipleName:principleName andGrammarName:grammarName] autorelease];
            [retval addObject:info];
            [arabicWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
}
// My Camp Skills

- (NSArray *)EnglishWordDatabaseArray:(int )gId andpId:(int) pId {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"newSmallSkills"]) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        _dbname = [NSString stringWithFormat:@"%@_Skiils_New.sqlite3",ActiveGrade];
    }
    [self createEditableCopyOfDatabaseIfNeeded];
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    NSString *query = [NSString stringWithFormat:@"SELECT word_id, english_word, image_id, voice_id,Is_Sentence, translate, arabic_Ref,opposite_word,synonym_word FROM english_word WHERE %@=1 and grammar_id = %d AND Principle_id = %d",_CurrentGrade, gId,pId];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int wordId = sqlite3_column_int(statement, 0);            
            char *englishWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *englishWord = [[NSString alloc] initWithUTF8String:englishWordChar];
            int imageId = sqlite3_column_int(statement, 2);            
            char *voiceIdChar = (char *) sqlite3_column_text(statement, 3);
            NSString *voiceId = [[NSString alloc] initWithUTF8String:voiceIdChar];
            int sentence = sqlite3_column_int(statement, 4); 
            int translate = sqlite3_column_int(statement, 5); 
            int englishRef = sqlite3_column_int(statement, 6);            
            char *synonymWordChar = (char *) sqlite3_column_text(statement, 8);
            NSString *synonymWord;
            if (synonymWordChar != NULL) {
                 synonymWord = [[[NSString alloc] initWithUTF8String:synonymWordChar] autorelease];                
            }else {
                synonymWord = [NSString stringWithFormat:@"%d",0];
            }

            char *oppositeWordChar = (char *) sqlite3_column_text(statement, 7);

            NSString *oppositeWord;
            if (oppositeWordChar != NULL) {
                oppositeWord = [[[NSString alloc] initWithUTF8String:oppositeWordChar] autorelease];                
            }
            else {
                oppositeWord = [NSString stringWithFormat:@"%d",0];
            }

            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:englishWord isSentence:sentence arabicVoiceId:voiceId imageId:imageId translate:translate andEnglishRef:englishRef andSynonym:synonymWord andOppositeWord:oppositeWord] autorelease];
            
            [retval addObject:info];
            [englishWord release];
            [voiceId release];
                    
        }
        sqlite3_finalize(statement);
    }
    return retval;
    
}

- (NSArray *)ArabicWordDatabaseArray:(int )gId andpId:(int) pId {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"newSmallSkills"]) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        _dbname = [NSString stringWithFormat:@"%@_Skiils_New.sqlite3",ActiveGrade];
                [self createEditableCopyOfDatabaseIfNeeded];
    }

    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    
    NSString *query = [NSString stringWithFormat:@"SELECT word_id, arabic_word, image_id, voice_id,Is_Sentence, translate, english_Ref,opposite_word,synonym_word FROM arabic_word WHERE %@=1 and grammar_id = %d AND Principle_id = %d",_CurrentGrade, gId, pId];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int wordId = sqlite3_column_int(statement, 0);            
            char *arabicWordChar = (char *) sqlite3_column_text(statement, 1);
            NSString *arabicWord = [[NSString alloc] initWithUTF8String:arabicWordChar];
            char *voice = (char *) sqlite3_column_text(statement, 3);
            NSString *arabicVoiceId = [[NSString alloc] initWithUTF8String:voice];            
            int imageId = sqlite3_column_int(statement, 2);
            int sentence = sqlite3_column_int(statement, 4);
            int translate = sqlite3_column_int(statement, 5);            
            int englishRef = sqlite3_column_int(statement, 6); 
            char *synonymWordChar = (char *) sqlite3_column_text(statement, 8);
            NSString *synonymWord;
            if (synonymWordChar != NULL) {
                synonymWord = [[[NSString alloc] initWithUTF8String:synonymWordChar] autorelease];                
            }else {
                synonymWord = [NSString stringWithFormat:@"%d",0];
            }
            
            char *oppositeWordChar = (char *) sqlite3_column_text(statement, 7);
            
            NSString *oppositeWord;
            if (oppositeWordChar != NULL) {
                oppositeWord = [[[NSString alloc] initWithUTF8String:oppositeWordChar] autorelease];                
            }
            else {
                oppositeWord = [NSString stringWithFormat:@"%d",0];
            }

            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithWordId:wordId arabicWord:arabicWord isSentence:sentence arabicVoiceId:arabicVoiceId imageId:imageId translate:translate andEnglishRef:englishRef andSynonym:synonymWord andOppositeWord:oppositeWord] autorelease];
            [retval addObject:info];
            [arabicWord release];
            [synonymWord release];
            [oppositeWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
    
}


// My Camp Skills Over

- (NSArray *)MySkillsGrammerArray{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"newSmallSkills"]) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        _dbname = [NSString stringWithFormat:@"%@_Skiils_New.sqlite3",ActiveGrade];
    }
        [self createEditableCopyOfDatabaseIfNeeded];
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];

    NSString *query = [NSString stringWithFormat:@"SELECT grammar_id, principles_id, desc, group_id FROM Principles_groups WHERE %@ = 1",ActiveGrade];
            NSLog(@"Query for grammar id is %@",query);
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int grammarId = sqlite3_column_int(statement, 0);            
            char *desc = (char *) sqlite3_column_text(statement, 2);
            NSString *grammarWord = [[NSString alloc] initWithUTF8String:desc];
            int principlesId = sqlite3_column_int(statement, 1);
            int groupId = sqlite3_column_int(statement, 3);
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] 
                                    initWithGrammarId:grammarId grammarWord:grammarWord principlesId:principlesId groupId:groupId] autorelease];
            [retval addObject:info];
            [grammarWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
    
}







- (NSArray *)MySkillsLearningGrammar{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"newSmallSkills"]) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        _dbname = [NSString stringWithFormat:@"%@_Skiils_New.sqlite3",ActiveGrade];
    }
        [self createEditableCopyOfDatabaseIfNeeded];
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    
    NSString *query = [NSString stringWithFormat:_GrammarSQL,activeLanguage];
    NSLog(@"Query for grammar id is %@",query);
    
//    NSString *query = @"SELECT * FROM learning_grammars";
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int grammarId = sqlite3_column_int(statement, 0);            
            char *desc = (char *) sqlite3_column_text(statement, 1);
            NSString *grammarWord = [[NSString alloc] initWithUTF8String:desc];

            
            MrPContentInfo *info = [[[MrPContentInfo alloc] 
                                    initWithGrammarId:grammarId grammarWord:grammarWord] autorelease];
            [retval addObject:info];
            [grammarWord release];
            
            
        } 
        sqlite3_finalize(statement);
    }
    
    return retval;
    
}
- (NSArray *)MySkillsPrinciplesArray{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"newSmallSkills"]) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        _dbname = [NSString stringWithFormat:@"%@_Skiils_New.sqlite3",ActiveGrade];
    }
        [self createEditableCopyOfDatabaseIfNeeded];
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    
    
    NSString *query = _PrincipleSQL;
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int LangId = sqlite3_column_int(statement, 0);            
            char *desc = (char *) sqlite3_column_text(statement, 2);
            NSString *principleWord = [[NSString alloc] initWithUTF8String:desc];
            int principlesId = sqlite3_column_int(statement, 1);
            int voiceId = sqlite3_column_int(statement, 3);
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] 
                                    initWithPrincipleId:principlesId principleWord:principleWord LangId:LangId voiceId:voiceId] autorelease];
            [retval addObject:info];
            [principleWord release];
            
            
        }
        sqlite3_finalize(statement);
    }
    return retval;
    
}


- (NSArray *)ClassificationDatabaseArray {
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    NSString *query = @"SELECT Classification_id, desc, sub_id FROM Classifications";
    //    const char *sql = [query UTF8String];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int classificationId = sqlite3_column_int(statement, 0);            
            char *descChar = (char *) sqlite3_column_text(statement, 1);
            NSString *descWord = [[NSString alloc] initWithUTF8String:descChar];
            int subClassificationId = sqlite3_column_int(statement, 4);                        
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] 
                                    initWithClassificationId:classificationId arabicWord:descWord subId:subClassificationId] autorelease];
            [retval addObject:info];
            [descWord release];                        
        }
        sqlite3_finalize(statement);
    }

    return retval;
    
}

- (NSArray *)ReadFolderDatabase {
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    NSString *query = @"SELECT Classification_id, desc FROM Classifications WHERE Classification_id > 989";
    NSLog(@"Query is %@",query );
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int classificationId = sqlite3_column_int(statement, 0);            
            char *descChar = (char *) sqlite3_column_text(statement, 1);
            NSString *descWord; 
            if (descChar != NULL || descChar) {
                descWord = [[NSString alloc] initWithUTF8String:descChar];
            }
            else {
                descWord = @"";
            }
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithName:classificationId  :descWord] autorelease];
            [retval addObject:info];
            [descWord release];                        
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

- (NSArray *)ReadBooksDatabase {
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query = @"SELECT Classification_id, desc FROM Classifications WHERE Classification_id > 999 AND Classification_id %10 = 0";
    NSLog(@"Query is %@",query );
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    NSLog(@"Opening Databse");
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int classificationId = sqlite3_column_int(statement, 0);
            char *descChar = (char *) sqlite3_column_text(statement, 1);
            NSString *descWord;
            if (descChar != NULL || descChar) {
                descWord = [[NSString alloc] initWithUTF8String:descChar];
            }
            else {
                descWord = @"";
            }
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithName:classificationId  :descWord] autorelease];
            NSLog(@"info is %d",info.classificationId);
            [retval addObject:info];
            [descWord release];
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

- (NSArray *)ReadBookFoldersDatabaseWithClassification:(int) classificationIdOfBook {
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query = [NSString stringWithFormat:@"SELECT Classification_id, desc FROM Classifications WHERE Classification_id > %d AND Classification_id < %d",classificationIdOfBook,classificationIdOfBook+10 ];
    NSLog(@"Query is %@",query );
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    NSLog(@"Opening Databse");
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int classificationId = sqlite3_column_int(statement, 0);
            char *descChar = (char *) sqlite3_column_text(statement, 1);
            NSString *descWord;
            if (descChar != NULL || descChar) {
                descWord = [[NSString alloc] initWithUTF8String:descChar];
            }
            else {
                descWord = @"";
            }
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithName:classificationId  :descWord] autorelease];
            NSLog(@"info is %d",info.classificationId);
            [retval addObject:info];
            [descWord release];
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

-(void) clearReportRecords{
    [self checkDatabase];
    
    [self createEditableCopyOfDatabaseIfNeeded];
    
    //    NSString *selectedGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString* updateSQLQuery;
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3 *database;
    //    // Open the database from the users filessytem and delete the row which i want to update
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        
        //Updating the row
        sqlite3_stmt    *statement;
        
        
        BOOL success = NO;
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
                       
                updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_answers_BasketBall = 0, total_correct_BasketBall = 0, total_correct_FixIt = 0, total_answers_FixIt = 0 , total_correct_SpinIt = 0, total_answers_SpinIt = 0, total_correct_PopIt = 0, total_answers_PopIt = 0,total_correct_MatchIt = 0, total_answers_MatchIt = 0, total_correct_PickIt = 0, total_answers_PickIt = 0, total_correct_TypeIt = 0, total_answers_TypeIt = 0, total_correct_MoveIt = 0, total_answers_MoveIt = 0, active_language = 0, Game_Id = 0"];
            
                
                NSLog(@"Update Query is Sir : %@",updateSQLQuery);
                const char *insert_stmt = [updateSQLQuery UTF8String];
                
                sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    success=YES;
                } else {
                    NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                    success=NO;
                }
                sqlite3_finalize(statement);
            }
        }
        sqlite3_close(database);
}


- (NSArray *)ReadClassificationTable {
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query = @"SELECT Classification_id, desc FROM Classifications order by Classification_id";
    NSLog(@"Query is %@",query );
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int classificationId = sqlite3_column_int(statement, 0);
            char *descChar = (char *) sqlite3_column_text(statement, 1);
            NSString *descWord;
            if (descChar != NULL || descChar) {
                descWord = [[NSString alloc] initWithUTF8String:descChar];
            }
            else {
                descWord = @"";
                NSLog(@"desc word %@",descWord);
            }
            
            MrPContentInfo *info = [[[MrPContentInfo alloc] initWithName:classificationId  :descWord] autorelease];
            [retval addObject:info];
            [descWord release];
        }
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        
            sqlite3_finalize(statement);
        }
    }
    return retval;
    
}

-(void) saveNewFolder:(NSString *) folderName andClassificationId:(int ) classificationId{
    
    [self checkDatabase];
    
    [self createEditableCopyOfDatabaseIfNeeded];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3 *database;
    // Open the database from the users filessytem and delete the row which i want to update
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {        
//        
        sqlite3_stmt    *deleteStmt;        
        const char *sql = "delete from Classifications where Classification_id = ?";
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
        
        sqlite3_bind_int(deleteStmt, 1, classificationId);    
        if (SQLITE_DONE != sqlite3_step(deleteStmt))
        NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));    
        sqlite3_reset(deleteStmt);

        //Updating the row
        sqlite3_stmt    *statement;
        NSString *insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO Classifications (Classification_id, desc) VALUES ('%d','%@')", classificationId, folderName];

        BOOL success = NO;
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            NSLog(@"%@",insertSQLQuery);
            const char *insert_stmt = [insertSQLQuery UTF8String];
            
            sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success=YES;
            } else {
                NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                success=NO;
//                NSLog(@"%d",sqlite3_step(statement));
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
//        return success;
    }
}



- (NSArray *)ReadBooksWordsDatabase:(int) classification {
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query = [NSString stringWithFormat:@"SELECT word_id, arabic_word, lang_dir,voice_id,image_id FROM arabic_word WHERE classification_id = %d",classification];
    NSLog(@"QUERY FOR WORD IS %@",query);
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int wordId = sqlite3_column_int(statement, 0);
            char *descChar = (char *) sqlite3_column_text(statement, 1);
            NSString *englishWord;
            if (descChar != NULL || descChar) {
                englishWord = [[NSString alloc] initWithUTF8String:descChar];
            }
            else {
                englishWord = @"";
            }
            int langDir = sqlite3_column_int(statement, 2);
            int imgId = sqlite3_column_int(statement, 4);
            descChar = (char *) sqlite3_column_text(statement, 3);
            NSString *voice;
            if (descChar != NULL || descChar) {
                voice = [[NSString alloc] initWithUTF8String:descChar];
            }
            else {
                voice = @"EmptySound";
            }
            MrPContentInfo *info = [[[MrPContentInfo alloc]
                                     initWithWordId:wordId andVoiceId:voice andfWordname:englishWord andLangDir:langDir andImageId:imgId] autorelease];
            [retval addObject:info];
            
            [englishWord release];
            [voice release];
        }
        sqlite3_finalize(statement);
    }
    return retval;
    
}








- (NSArray *)ReadWordDatabase:(int) classification {
    
    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;    
    NSString *query = [NSString stringWithFormat:@"SELECT word_id, english_word, lang_dir,voice_id,image_id FROM english_word WHERE Classification_id  = %d",classification];
    //    const char *sql = [query UTF8String];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int wordId = sqlite3_column_int(statement, 0);            
            char *descChar = (char *) sqlite3_column_text(statement, 1);
            NSString *englishWord; 
            if (descChar != NULL || descChar) {
                englishWord = [[NSString alloc] initWithUTF8String:descChar];
            }
            else {
                englishWord = @"";
            }
            int langDir = sqlite3_column_int(statement, 2);
            int imgId = sqlite3_column_int(statement, 4);
            descChar = (char *) sqlite3_column_text(statement, 3);
            NSString *voice; 
            if (descChar != NULL || descChar) {
                voice = [[NSString alloc] initWithUTF8String:descChar];
            }
            else {
                voice = @"EmptySound";
            }
            MrPContentInfo *info = [[[MrPContentInfo alloc] 
                                    initWithWordId:wordId andVoiceId:voice andfWordname:englishWord andLangDir:langDir andImageId:imgId] autorelease];
            [retval addObject:info];
            
            [englishWord release];                        
            [voice release];
        }
        sqlite3_finalize(statement);
    }
    return retval;
    
}
-(void ) removeWord:(int) wordIdToBeRemoved{

    [self checkDatabase];
    
    [self createEditableCopyOfDatabaseIfNeeded];

    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3 *database;
    // Open the database from the users filessytem and delete the row which i want to update
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {        
        //        
        sqlite3_stmt    *deleteStmt;
        const char *sql = "delete from english_word where word_id = ?";
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
        
        sqlite3_bind_int(deleteStmt, 1, wordIdToBeRemoved);    
        if (SQLITE_DONE != sqlite3_step(deleteStmt))
            NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));    
        sqlite3_reset(deleteStmt);
        
        
        sqlite3_stmt    *deleteStmtReport;
        const char *sqlReport = "delete from Report where word_id = ?";
        if(sqlite3_prepare_v2(database, sqlReport, -1, &deleteStmtReport, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
        
        sqlite3_bind_int(deleteStmtReport, 1, wordIdToBeRemoved);
        if (SQLITE_DONE != sqlite3_step(deleteStmtReport))
            NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
        sqlite3_reset(deleteStmtReport);
        

        sqlite3_close(database);
        NSLog(@"close successfully");
    }
}

-(void) DeleteFolder:(int )classificationToBeRemoved{

    [self checkDatabase];
    
    [self createEditableCopyOfDatabaseIfNeeded];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3 *database;
    // Open the database from the users filessytem and delete the row which i want to update
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {        
        //        
        sqlite3_stmt    *deleteStmt;        
        const char *sql = "delete from Classifications where Classification_id = ?";
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
        
        sqlite3_bind_int(deleteStmt, 1, classificationToBeRemoved);    
        if (SQLITE_DONE != sqlite3_step(deleteStmt))
            NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));    
        sqlite3_reset(deleteStmt);
        
        
        
        // Deleting Words 
        sqlite3_stmt    *deleteStmts;        
        sql = "delete from english_word where classification_id = ?";
        if(sqlite3_prepare_v2(database, sql, -1, &deleteStmts, NULL) != SQLITE_OK)
            NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
        
        sqlite3_bind_int(deleteStmts, 1, classificationToBeRemoved);    
        if (SQLITE_DONE != sqlite3_step(deleteStmts))
            NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));    
        sqlite3_reset(deleteStmts);

        
        //Updating the row
        sqlite3_stmt    *statement;
        NSString *insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO Classifications (Classification_id, desc) VALUES ('%d','%@')", classificationToBeRemoved, @"Capture your words"];
        
        BOOL success = NO;
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            NSLog(@"%@",insertSQLQuery);
            const char *insert_stmt = [insertSQLQuery UTF8String];
            
            sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                success=YES;
            } else {
                NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                success=NO;
                //                NSLog(@"%d",sqlite3_step(statement));
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(database);
    }
}

-(void) saveNewWordInFoler:(NSString*)word andWordId:(int )wordId whereClassificationId:(int )currentClassificationId andVoiceId:(NSString *)voice andisSentence:(int)isSentence andImageId:(int) imageId{
    
    [self checkDatabase];
    
    [self createEditableCopyOfDatabaseIfNeeded];
    NSLog(@"Table SAve new word in folder called");

    NSString *selectedGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString *insertSQLQuery;
    NSString *insertSQLQueryForReport;
    int grade = 1;
    if (selectedGrade && ![selectedGrade isEqualToString:@"FREE"]) {
         insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,%@,image_id,Principle_id,grammar_id,translate,synonym_word,opposite_word) VALUES ('%d','%@','%d','%d','%@','%d','%d','0','0','0','0','0')",selectedGrade, wordId, word,isSentence, currentClassificationId,voice,grade,imageId];
//        insertSQLQueryForReport = [NSString stringWithFormat:@"INSERT INTO Report (Word_Id, Word_desc, Classificaiton ) VALUES ('%d','%@','%d')", wordId, word, currentClassificationId];

        int insertZeroInteger = 0;
        insertSQLQueryForReport = [NSString stringWithFormat:@"INSERT INTO Report (Word_Id, Word_desc, classificaiton,Game_Id ,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,active_language) VALUES ('%d','%@','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d')", wordId, word, currentClassificationId,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger];
        
        NSLog(@"current insert statment is id is %@",insertSQLQueryForReport);
    }
    else {
         insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,image_id) VALUES ('%d','%@','%d','%d','%@','%d')", wordId, word,isSentence, currentClassificationId,voice,imageId];
 
    }
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3 *database;
//    // Open the database from the users filessytem and delete the row which i want to update
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {        

        //Updating the row
        sqlite3_stmt    *statement;
       
        
        BOOL success = NO;
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            const char *insert_stmt = [insertSQLQuery UTF8String];
            
            sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Insert word");
                success=YES;
            } else {
                NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                success=NO;
            }
            sqlite3_finalize(statement);
            
        }
        sqlite3_close(database);
        
        success = NO;
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            const char *insert_stmt_Report = [insertSQLQueryForReport UTF8String];
            
            sqlite3_prepare_v2(database, insert_stmt_Report, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"successfully inserted in REport table");
                success=YES;
            } else {
                NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                success=NO;
            }
            sqlite3_finalize(statement);
            
        }
        sqlite3_close(database);
        //        return success;
    }
}


-(void) saveReportProgress:(NSArray*) retVal{
    
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"harkat"]){
        _dbname =[ActiveGrade stringByAppendingString:@".sqlite3"];
    }
    NSLog(@"%@",_dbname);
    
    NSLog(@"Saving Report");
    [self checkDatabase];
    [self createEditableCopyOfDatabaseIfNeeded];
    int position = 0;
    tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[retVal count]];
    for (MrPReport *info in retVal) {
        
        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [tempContentDic setObject:info.wordDescription forKey:[NSString stringWithFormat:@"WordDesc%d",position]];
         [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswers] forKey:[NSString stringWithFormat:@"totalAnswers%d",position]];
         [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrect] forKey:[NSString stringWithFormat:@"noOfCorrect%d",position]];
         [tempContentDic setObject:[NSNumber numberWithInt:info.classification] forKey:[NSString stringWithFormat:@"classification%d",position]];
         [tempContentDic setObject:[NSNumber numberWithInt:info.activelanguage] forKey:[NSString stringWithFormat:@"activeLanguage%d",position]];
        
        [tempContentDic setObject:[NSNumber numberWithInt:info.gameId] forKey:[NSString stringWithFormat:@"gameId%d",position]];

        position = position + 1;
    }
    
    NSLog(@"temp content dic is %@",tempContentDic);
    
    NSString* updateSQLQuery;

    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3 *database;
    NSLog(@"db name is %@",_dbname);
    BOOL newPIN;
    
    // check if record exist or not
    
    for (MrPReport *info in retVal){
    int wordId = 0;
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement1;
    NSString *query = [NSString stringWithFormat:@"SELECT Word_Id,PINNUMBER FROM Report WHERE Word_id  = %d AND PINNUMBER = %d",info.wordId,[[Common getPinNumber] intValue]];
        // add addational condtion of WHERE word_id and PIN is common pin...... it will avoid repearting
    //    const char *sql = [query UTF8String];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement1, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement1, nil) == SQLITE_OK) {
        while (sqlite3_step(statement1) == SQLITE_ROW) {
            wordId = sqlite3_column_int(statement1, 0);
            int pinnumber = sqlite3_column_int(statement1, 1);
            if (pinnumber != info.PINNUMBER) {
                wordId = 0;
            }
            NSLog(@"wordid is %d",wordId);
                    }
        sqlite3_finalize(statement1);
    }
        
//    sqlite3_close(database);
    
    if (wordId > 0) {
        if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        
            //Updating the row
        
            sqlite3_stmt    *statement;
            
            
            BOOL success = NO;
            if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
            {
                
//                for (MrPReport *info in retVal)
//                {
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 1) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_FixIt = total_correct_FixIt + %d, total_answers_FixIt = total_answers_FixIt +  %d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue]];
                    }
                    
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 2) {
                        
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_SpinIt = total_correct_SpinIt + %d, total_answers_SpinIt = total_answers_SpinIt +%d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue]];
                        
                    }
                    
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 3) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_PopIt = total_correct_PopIt +%d, total_answers_PopIt = total_answers_PopIt +%d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue]];
                        
                    }
                    
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 4) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_PickIt = total_correct_PickIt+ %d, total_answers_PickIt = total_answers_PickIt +%d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue ],[[NSNumber numberWithInt:info.wordId] intValue] ];
                        
                    }
                    
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 5) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_MoveIt = total_correct_MoveIt +%d, total_answers_MoveIt = total_answers_MoveIt +%d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue ],[[NSNumber numberWithInt:info.wordId] intValue] ];
                        
                    }
                    
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 6) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_MatchIt = total_correct_MatchIt +%d, total_answers_MatchIt = total_answers_MatchIt + %d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue] ,[[NSNumber numberWithInt:info.wordId] intValue] ];
                        
                    }
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 7) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_TypeIt = total_correct_TypeIt +%d, total_answers_TypeIt = total_answers_TypeIt + %d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue] ,[[NSNumber numberWithInt:info.wordId] intValue] ];
                        
                    }
                    
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 9) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_Football = total_correct_Football +%d, total_answers_Football = total_answers_Football + %d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue] ,[[NSNumber numberWithInt:info.wordId] intValue] ];
                        
                    }
                    
                    // Hockey
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 10) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_Hockey = total_correct_Hockey +%d, total_answers_Hockey = total_answers_Hockey + %d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue] ,[[NSNumber numberWithInt:info.wordId] intValue] ];
                    }
                    
                    //BAsket ball
                    if ([[NSNumber numberWithInt:info.gameId] intValue] == 11) {
                        updateSQLQuery = [NSString stringWithFormat: @"UPDATE Report SET total_correct_BasketBall = total_correct_BasketBall +%d, total_answers_BasketBall = total_answers_BasketBall + %d, classificaiton = %d, active_language = %d, Game_Id = %d WHERE Word_Id = %d;",[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],[[NSNumber numberWithInt:info.gameId] intValue] ,[[NSNumber numberWithInt:info.wordId] intValue] ];
                    }
                    
                    
                    NSLog(@"Update Query is Sir : %@",updateSQLQuery);
                    const char *insert_stmt = [updateSQLQuery UTF8String];
                    
                    sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
                    
                    
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        NSLog(@"Successfully insterted");
                        success=YES;
                    } else {
                        
                        NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                        success=NO;
                    }
                    sqlite3_finalize(statement);
                }
            
            sqlite3_close(database);
        }
    }
    else{

        NSLog(@"Word NOT FOUND so it is %@",info.wordDescription);
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths lastObject];
        NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
        sqlite3 *database;
        // Open the database from the users filessytem and delete the row which i want to update
        if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
          
            
            //Updating the row
            sqlite3_stmt    *statement;
            /* Query Creating */
            
            NSString *updateSQLQuery;
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 1) {
                updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_Football,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 2) {
                    updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_SpinIt,total_answers_SpinIt,total_correct_FixIt,total_answers_FixIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_Football,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 3) {
                  updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_PopIt,total_answers_PopIt,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_Football,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 4) {
               updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_PickIt,total_answers_PickIt,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_Football,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 5) {
                  updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_MoveIt,total_answers_MoveIt,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_Football,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 6) {
                updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_Football,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 7) {
                updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_TypeIt,total_answers_TypeIt,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_Football,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 9) {
                updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_answers_Football,total_correct_Football,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_correct_Hockey,total_answers_Hockey,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            
            // Hockey
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 10) {
               updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_Hockey,total_answers_Hockey,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_answers_Football,total_correct_Football,total_correct_BasketBall,total_answers_BasketBall) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];            }
            
            //BAsket ball
            if ([[NSNumber numberWithInt:info.gameId] intValue] == 11) {
                updateSQLQuery = [NSString stringWithFormat:@"INSERT INTO REPORT (PINNUMBER, Game_Id,Word_Id,Classificaiton,active_language,Word_desc,total_correct_BasketBall,total_answers_BasketBall,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_Football) VALUES ('%d','%d','%d','%d','%d','%@','%d','%d','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')",[[Common getPinNumber] intValue],[[NSNumber numberWithInt:info.gameId] intValue],[[NSNumber numberWithInt:info.wordId] intValue],[[NSNumber numberWithInt:info.classification] intValue],[[NSNumber numberWithInt:info.activelanguage] intValue],info.wordDescription,[[NSNumber numberWithInt:info.noOfCorrect] intValue], [[NSNumber numberWithInt:info.totalAnswers] intValue] ];
            }
            
            
        
            
            NSLog(@"Update Query is Sir : %@",updateSQLQuery);
            
            
            
            
            /* Query Created */
            
            BOOL success = NO;
            if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
            {

                const char *insert_stmt = [updateSQLQuery UTF8String];
                
                sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    NSLog(@"Successfully DONE");
                    success=YES;
                } else {
                    NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                    success=NO;
                    //                NSLog(@"%d",sqlite3_step(statement));
                }
                sqlite3_finalize(statement);
            }
            sqlite3_close(database);
         
        }

    
        }
    }
    
    
    
    //    // Open the database from the users filessytem and delete the row which i want to update
   
}



-(NSArray*) getReportData{
    
    
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"harkat"]){
        _dbname =[ActiveGrade stringByAppendingString:@".sqlite3"];
    }

    NSMutableArray *retval = [[[NSMutableArray alloc] init] autorelease];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    sqlite3_open([databasePath UTF8String], &_database);
    sqlite3_stmt *statement;
    NSString *query = [NSString stringWithFormat:@"SELECT Game_Id, Word_Id, Word_desc, total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,classificaiton,active_language,total_correct_Football,total_answers_Football,total_correct_Hockey,total_answers_Hockey,total_correct_BasketBall,total_answers_BasketBall,PINNUMBER FROM Report order by PINNUMBER"];
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) != SQLITE_OK) {
        NSAssert(0, @"Failed to open database");
    }
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, NULL) != SQLITE_OK) {
        NSLog(@"[SQLITE] Error when preparing query!");
    }    
    
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int wordId = sqlite3_column_int(statement, 1);
            char *englishWordChar = (char *) sqlite3_column_text(statement, 2);
            if (!englishWordChar || englishWordChar == NULL || englishWordChar == Nil) {
                
            }
            else{
            NSString *englishWord = [[NSString alloc] initWithUTF8String:englishWordChar];
            int gameId = sqlite3_column_int(statement, 0);
            
            int totalCorrectFixIt = sqlite3_column_int(statement, 3);
            int totalAnswersFixIt = sqlite3_column_int(statement, 4);
            
            int totalCorrectSpinIt = sqlite3_column_int(statement, 5);
            int totalAnswersSpinIt = sqlite3_column_int(statement, 6);
            
            int totalCorrectPopIt = sqlite3_column_int(statement, 7);
            int totalAnswersPopIt = sqlite3_column_int(statement, 8);

            
            int totalCorrectPickIt = sqlite3_column_int(statement, 9);
            int totalAnswersPickIt = sqlite3_column_int(statement, 10);
            
            int totalCorrectMoveIt = sqlite3_column_int(statement, 11);
            int totalAnswersMoveIt = sqlite3_column_int(statement, 12);
            
            int totalCorrectMatchIt = sqlite3_column_int(statement, 13);
            int totalAnswersMatchIt = sqlite3_column_int(statement, 14);
            
            int totalCorrectTypeIt = sqlite3_column_int(statement, 15);
            int totalAnswersTypeIt = sqlite3_column_int(statement, 16);
            
            int classification = sqlite3_column_int(statement, 17);
            int activeLanguage = sqlite3_column_int(statement, 18);
            
            int totalCorrectFootball = sqlite3_column_int(statement, 19);
            int totalAnswersFootball = sqlite3_column_int(statement, 20);
            
            int totalCorrectHockey = sqlite3_column_int(statement, 21);
            int totalAnswersHockey = sqlite3_column_int(statement, 22);
            
            int totalCorrectBasketball = sqlite3_column_int(statement, 23);
            int totalAnswersBasketball = sqlite3_column_int(statement, 24);
                
            int PINNUMBER = sqlite3_column_int(statement, 25);
            
                MrPReport *info = [[MrPReport alloc] initWithWordId:wordId andWordDesc:englishWord andCorrectCountFixIt:totalCorrectFixIt andTotalCountFixIt:totalAnswersFixIt andCorrectCountPopIt:totalCorrectPopIt andTotalCountPopIt:totalAnswersPopIt andCorrectCountSpinIt:totalCorrectSpinIt andTotalCountSpinIt:totalAnswersSpinIt andCorrectCountPickIt:totalCorrectPickIt andTotalCountPickIt:totalAnswersPickIt andCorrectCountMoveIt:totalCorrectMoveIt andTotalCountMoveIt:totalAnswersMoveIt andCorrectCountMatchIt:totalCorrectMatchIt andTotalCountMatchIt:totalAnswersMatchIt andCorrectCountTypeIt:totalCorrectTypeIt andTotalCountTypeIt:totalAnswersTypeIt andCorrectCountFootball:totalCorrectFootball andTotalCountFootball:totalAnswersFootball andCorrectCountHockey:totalCorrectHockey andTotalCountHockey:totalAnswersHockey andCorrectCountBasketball:totalCorrectBasketball andTotalCountBasketball:totalAnswersBasketball andActiveLanguage:activeLanguage andClassification:classification andGameId:gameId andPinNumber:PINNUMBER];
            NSLog(@"english word is %@",englishWord);
            
            [retval addObject:info];
            [englishWord release];
            }
        }
        if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
            
            sqlite3_finalize(statement);
        }
    }
    return retval;



}

//-(void) saveNewWordInFoler:(NSString*)word andWordId:(int )wordId whereClassificationId:(int )currentClassificationId andVoiceId:(NSString *)voice andisSentence:(int)isSentence andImageId:(int) imageId{
//    
//    [self checkDatabase];
//    
//    [self createEditableCopyOfDatabaseIfNeeded];
//    
//    NSString *selectedGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
//    NSString *insertSQLQuery;
//    int grade = 1;
//    if (selectedGrade && ![selectedGrade isEqualToString:@"FREE"]) {
//        insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,%@,image_id) VALUES ('%d','%@','%d','%d','%@','%d','%d')",selectedGrade, wordId, word,isSentence, currentClassificationId,voice,grade,imageId];
//    }
//    else {
//        insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,image_id) VALUES ('%d','%@','%d','%d','%@','%d')", wordId, word,isSentence, currentClassificationId,voice,imageId];
//    }
//    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString* documentsDirectory = [paths lastObject];
//    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
//    sqlite3 *database;
//    //    // Open the database from the users filessytem and delete the row which i want to update
//    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
//        
//        //Updating the row
//        sqlite3_stmt    *statement;
//        
//        
//        BOOL success = NO;
//        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
//        {
//            NSLog(@"%@",insertSQLQuery);
//            const char *insert_stmt = [insertSQLQuery UTF8String];
//            
//            sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
//            if (sqlite3_step(statement) == SQLITE_DONE)
//            {
//                success=YES;
//            } else {
//                NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
//                success=NO;
//                //                NSLog(@"%d",sqlite3_step(statement));
//            }
//            sqlite3_finalize(statement);
//        }
//        sqlite3_close(database);
//        //        return success;
//    }
//}
//
//
//
//

-(NSMutableString*) createEqualGrammarStringWithgIDsIncluded:(BOOL)pGIdIncluded{
    NSMutableString *classificationsStr = [NSMutableString string];
    NSArray *cIdArray = [[NSArray alloc] init];
    cIdArray = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];

    NSMutableString *queryString = [NSMutableString string];
    if([cIdArray isKindOfClass:[NSArray class]] == YES){
        if (!cIdArray || cIdArray == NULL || [cIdArray isEqual:@""]) {
            return classificationsStr;
        }
    }

    queryString = [Constants createNotEqualToGrammarIdString:cIdArray withGIDIncluded:pGIdIncluded];
    return queryString;
}


- (void)createEditableCopyOfDatabaseIfNeeded {
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if([ActiveGrade isEqual:Nil] || [ActiveGrade isEqualToString:@"FREE"] || !ActiveGrade){
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"harkat"] || (cId > 989 && cId < 999) || (activeLanguage == 2)){
//            _dbname =@"G1.sqlite3";
            
        }
        else{
//            _dbname =@"G1.sqlite3";
        }
    }
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    NSLog(@"writeable path is %@",writableDBPath);
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success){

        NSURL *pathURL= [NSURL fileURLWithPath:writableDBPath];
        [self addSkipBackupAttributeToItemAtURL:pathURL];
        return;
}
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:_dbnameWithPath];
        NSLog(@"default path is path is %@",defaultDBPath);
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

-(void) checkDatabase{
    
    BOOL success;
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if([ActiveGrade isEqual:Nil] || [ActiveGrade isEqualToString:@"FREE"] || !ActiveGrade){
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"harkat"] || (cId > 989 && cId < 999) || (activeLanguage == 2)){
//            _dbname =@"G1.sqlite3";
            
        }
        else{
//            _dbname =@"G1.sqlite3";
        }
    }
    
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:_dbname];
    
    // Create a FileManager object, we will use this to check the status
    // of the database and to copy it over if required
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Check if the database has already been created in the users filesystem
    success = [fileManager fileExistsAtPath:databasePath];
    
    // If the database already exists then return without doing anything
    if(success) {
        
        NSURL *pathURL= [NSURL fileURLWithPath:databasePath];
        [self addSkipBackupAttributeToItemAtURL:pathURL];
        return;
    }
    
    // If not then proceed to copy the database from the application to the users filesystem
    
    // Get the path to the database in the application package
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:_dbnameWithPath];
    
    // Copy the database from the package to the users filesystem
    [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
    
    //[fileManager release];
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (void)dealloc {
    sqlite3_close(_database);
    [super dealloc];
}

@end
