//
//  RightsPopUp.h
//  GMG App
//
//  Created by Mosib on 4/27/15.
//  Copyright (c) 2015 Advansoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RightsPopUp;

@protocol RightsPopUpDelegate <NSObject>

@optional
- (void) RightsViewSubmitted:(RightsPopUp*)popUp;

@end

@interface RightsPopUp : UIView <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>{
}

@property (nonatomic,weak) IBOutlet id <RightsPopUpDelegate> delegate;
@property (nonatomic,weak) IBOutlet UIView *viewPopUp;


@property (weak, nonatomic) IBOutlet UITextField *textViewName;

@property (weak, nonatomic ) IBOutlet UIButton *buttonSubmit;

+ (RightsPopUp*) mainView;

- (void) showOnView:(UIView*)view withDelegate:(id<RightsPopUpDelegate>)delegate;
- (void) showOnView:(UIView*)view;
- (void) hide;

- (IBAction) actionSubmit:(id)sender;
- (IBAction)actionCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;

@end
