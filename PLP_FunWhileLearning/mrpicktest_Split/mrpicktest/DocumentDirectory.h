//
//  DocumentDirectory.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/30/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DocumentDirectory : NSObject{

}

+(UIImage*) imageNamed:(NSString*) image;
+(NSString*) playSound:(NSString*) soundName;
+(NSString*) playSkillsCampSound:(NSString*) soundName;
@end
