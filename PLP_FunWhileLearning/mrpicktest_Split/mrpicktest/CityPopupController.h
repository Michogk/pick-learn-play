//
//  CityPopupController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/17/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityPopupController : UIViewController  
@property (strong, nonatomic) IBOutlet UIButton *PopupClose;
- (IBAction)PopupCloseTouch:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *popView;
 
@property (retain, nonatomic) IBOutlet UIButton *BtnPractice;
     
@property (retain, nonatomic) IBOutlet UIButton *BtnPlay;

 


@end
