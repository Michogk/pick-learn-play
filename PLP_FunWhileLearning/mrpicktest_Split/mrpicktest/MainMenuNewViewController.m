//
//  mrpViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/13/12.
//  Copyright (c) 2012 School. All rights reserved.
//

#import "MainMenuNewViewController.h"
#import "MyStoreViewController.h"
#import "RightsPopUp.h"

#import "introViewController.h"
#import "Downloader.h"
#import "MrPDatabase.h"
#import "MovieViewController.h"
#import "DocumentDirectory.h"
//#define GRADE @"G1"
#import "MrPContentInfo.h"
#define GRADE_INTRO_VIDEO @"G1G4IntroVideo"
#import "mainmenuGrammarPrincipleCell.h"
#import "UIImage+animatedGIF.h"

#define FIRST_FEATHER 40
#define SECOND_FEATHER 50
#define THIRD_FEATHER 60
#define FOURTH_FEATHER 70
#define MY_HOME_FEATHER 80

#define MY_HOME_CLASSIFICATION 510

//static NSString *_dbname=@"Map_G3/G1.sqlite3";
//static NSString *_target_name =@"G4";


//#ifdef Map_G1
//static NSString *_dbname=@"Map_G1/G1.sqlite3";
//static NSString *_target_name =@"G1";
//#else
//#ifdef Map_G2
//static NSString *_dbname=@"Map_G2/G2.sqlite3";
//static NSString *_target_name =@"G2";
//#else
//static NSString *_dbname=@"Map_G3/G4.sqlite3";
//static NSString *_target_name =@"G5";
//#endif
//#endif



#define TABLE_VIEW_PRINCIPLES 2000
#define TABLE_VIEW_GRAMMAR 3000

#define mapToLoad @"kg"
#define TICK_TAG_BUFFER 19

#define SETTINGS 10
#define ACHIEVEMENTS 20
#define MY_HOME 30
#define MY_CAMP 40
#define MY_PLAYGROUND 50
//Video = KG_Video, Grade_Video



@interface MainMenuNewViewController (){

    IBOutlet UIScrollView *scrollViewClassifications;
    IBOutlet UITableView *tableViewGrammar;
    IBOutlet UITableView *tableViewPrincipleView;
    NSMutableArray *arrAllClassifications;
    NSMutableArray *arrAllClassificationsNames;
    NSMutableArray *arrAllClassificationsImages;
}

@end

@implementation MainMenuNewViewController

//const CGFloat kScrollObjHeights	= 100.0;
//const CGFloat kScrollObjWidths	= 120.0;

@synthesize CityMapView;

@synthesize MyCampView;
//@synthesize MyPlayGroundView;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    grammerIconClipArray = [[NSMutableArray alloc] init];

    NSURL *url = [[NSBundle mainBundle] URLForResource:@"feather" withExtension:@"gif"];
    imgView1Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    imgView2Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    imgView3Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    imgView4Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    imgView5Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    
    [self showFeather:FIRST_FEATHER];
    
#ifdef Map_G1
    _dbname=@"G1.sqlite3";
    _target_name =@"G1";
#else
#ifdef Map_G2
    _dbname=@"G2.sqlite3";
    _target_name =@"G2";
#else
#ifdef Map_G3
    _dbname=@"G3.sqlite3";
    _target_name =@"G3";
#else
#ifdef Map_G4
    _dbname=@"G4.sqlite3";
    _target_name =@"G4";
#else
#ifdef Map_KG1
    _dbname=@"KG1.sqlite3";
    _target_name =@"KG1";
#else
#ifdef Map_KG2
    _dbname=@"KG2.sqlite3";
    _target_name =@"KG2";
#else
#ifdef Map_KG0
    _dbname=@"KG0.sqlite3";
    _target_name =@"KG0";
#else
#ifdef School_KG0
    _dbname=@"KG0.sqlite3";
    _target_name =@"KG0";
#else
#ifdef School_KG1
    _dbname=@"KG1.sqlite3";
    _target_name =@"KG1";
#else
#ifdef School_KG2
    _dbname=@"KG2.sqlite3";
    _target_name =@"KG2";
#else
#ifdef School_G1
    _dbname=@"G1.sqlite3";
    _target_name =@"G1";
#else
#ifdef School_G2
    _dbname=@"G2.sqlite3";
    _target_name =@"G2";
#else
#ifdef School_G3
    _dbname=@"G3.sqlite3";
    _target_name =@"G3";
#else
#ifdef School_G4
    _dbname=@"G4.sqlite3";
    _target_name =@"G4";
#else
    _dbname=@"G1.sqlite3";
    _target_name =@"G1";
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
#endif
    
    principleIdSelectedArray = [[NSMutableArray alloc] init];
    grammarIdSelectedArray = [[NSMutableArray alloc] init];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:PRINCIPLE_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:GRAMMAR_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    int appUsedBefore = [[[NSUserDefaults standardUserDefaults] objectForKey:@"appUsedBefore"] intValue];
    if (appUsedBefore != 1) {
        [self loadInBackground];
//        [self performSelectorInBackground:@selector(loadInBackground) withObject:Nil];
    }
    
    NSString *appVersionNumber = [NSString stringWithFormat:@"%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    NSString *localVersionNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentAppVersionNumber"];
    if (![localVersionNumber isEqualToString:appVersionNumber] || !localVersionNumber) {
        [self replaceExistingDatabasesWithNewOnes];
        [[NSUserDefaults standardUserDefaults] setObject:appVersionNumber forKey:@"currentAppVersionNumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }


    
    tableViewGrammar.delegate = self;
    tableViewGrammar.dataSource = self;
    tableViewGrammar.tag = TABLE_VIEW_GRAMMAR;
    tableViewGrammar.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableViewGrammar.backgroundColor = [UIColor clearColor];
    
    tableViewPrincipleView.delegate = self;
    tableViewPrincipleView.dataSource = self;
    tableViewPrincipleView.tag = TABLE_VIEW_PRINCIPLES;
    tableViewPrincipleView.backgroundColor = [UIColor clearColor];
    tableViewPrincipleView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    MrPContentArray = [[NSMutableArray alloc] init];
    
    [self createClassificationsScrollView];
    [self playBackGroundMusic];
    

    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"NameTaken"]) {
        RightsPopUp* popup = [RightsPopUp mainView];
        [popup showOnView:self.view withDelegate:nil];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NameTaken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];

}
-(void) createClassificationsScrollView{
    
    
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];

    if (activeLanguage == 1) {
//        [[NSUserDefaults standardUserDefaults] setBool:false forKey:SYNONYM_ACTIVE];
//        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    if (activeLanguage == 1) {
        MrPContentArray = [database loadAllClassificaton];
    }else{
        MrPContentArray = [database loadAllClassificaton];
    }
    
    arrAllClassifications = [[NSMutableArray alloc] init];
    arrAllClassificationsImages = [[NSMutableArray alloc] init];
    classificationIdArray = [[NSMutableArray alloc] init];
    arrAllClassificationsNames = [[NSMutableArray alloc] init];

    for (MrPContentInfo *info in MrPContentArray) {
        [arrAllClassifications addObject:[NSString stringWithFormat:@"%d",info.classificationId ]];
    }
    
    NSInteger index = [arrAllClassifications count] - 1;
    for (id object in [arrAllClassifications reverseObjectEnumerator]) {
        if ([arrAllClassifications indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
            [arrAllClassifications removeObjectAtIndex:index];
        }
        index--;
    }

    MrPContentArray = nil;
    MrPContentArray = [database loadImageForAvailableClassifications:arrAllClassifications];
    [arrAllClassifications removeAllObjects];
    for (MrPContentInfo *info in MrPContentArray) {
        [arrAllClassificationsImages addObject:[NSString stringWithFormat:@"%d",info.imageId ]];
        [arrAllClassifications addObject:[NSString stringWithFormat:@"%d",info.classificationId ]];
        [arrAllClassificationsNames addObject:[NSString stringWithFormat:@"%@",info.englishWord ]];
    }

    _kNumImages = [arrAllClassificationsImages count];
    [scrollViewClassifications setCanCancelContentTouches:YES];
    scrollViewClassifications.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollViewClassifications.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our scrollview
    scrollViewClassifications.scrollEnabled = YES;
    int posX = 0;
    NSArray *selectedClassificationArray = [[NSArray alloc] init];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:CLASSIFICATIONS_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
//    if ([[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY]) {
//        selectedClassificationArray = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY]; // for tick mark only
//    }
    for (int i = 0; i < _kNumImages; i++)
    {
        NSString *imageName = [NSString stringWithFormat:@"%@.png",[arrAllClassificationsImages objectAtIndex:i]];
        UIImage *image = [UIImage imageNamed:imageName];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        
        [imageView setTag:[[arrAllClassifications objectAtIndex:i] intValue]];
        [imageView setBackgroundColor:[UIColor clearColor]];
        imageView.userInteractionEnabled=YES;
        
        // setup each frame to a default height and width, it will be properly placed when we call "updateScrollList"
        CGRect rect = imageView.frame;
        rect.size.height = 100;
        rect.size.width = 120;
        rect.origin.x = posX ;
        imageView.frame = rect;
        [scrollViewClassifications addSubview:imageView];
        
        UIImageView *tickImgView = [[UIImageView alloc] initWithFrame:imageView.frame];
        tickImgView.contentMode = UIViewContentModeScaleAspectFill;
        rect = tickImgView.frame;
        rect.size.height = 50;
        rect.size.width = 100;
        rect.origin.y = 70;
        rect.origin.x = posX-35;
        tickImgView.frame = rect;
        [tickImgView setTag:[[arrAllClassifications objectAtIndex:i] intValue] + TICK_TAG_BUFFER];
        [scrollViewClassifications addSubview:tickImgView];
        posX = posX + 120;
        
        UILabel *classifiationNamelbl = [[UILabel alloc] initWithFrame:CGRectMake(posX-120, 60, 120, 120)];
//        classifiationNamelbl.backgroundColor = [UIColor grayColor];
        classifiationNamelbl.font = [UIFont systemFontOfSize:12];
        classifiationNamelbl.textColor = [UIColor blackColor];
//        [classifiationNamelbl sizeToFit];
        classifiationNamelbl.numberOfLines = 3;
        classifiationNamelbl.text = [arrAllClassificationsNames objectAtIndex:i];
        
        [scrollViewClassifications addSubview:classifiationNamelbl];
        
        // add gesture recognizers to the image view
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(classificationTapped:)];
        singleTap.delegate = self;
        [imageView addGestureRecognizer:singleTap];

    }
 
    
    [scrollViewClassifications setContentSize:CGSizeMake(posX, [scrollViewClassifications bounds].size.height)];
//    [self layoutScrollImages];
}


- (void)layoutScrollImages
{
    UIImageView *view = nil;
    NSArray *subviews = [scrollViewClassifications subviews];
    
    
    // reposition all image subviews in a horizontal serial fashion
    CGFloat curXLoc = 0;
    for (view in subviews)
    {
        if ([view isKindOfClass:[UIImageView class]] && view.tag > 0)
        {
            CGRect frame = view.frame;
            frame.origin = CGPointMake(curXLoc, 0);
            view.frame = frame;
            
            curXLoc += (120);
        }
    }
    
    // set the content offest x,y
    [scrollViewClassifications setContentOffset:CGPointMake(2, 2)];
    
    // set the content size so it can be scrollable
    [scrollViewClassifications setContentSize:CGSizeMake((_kNumImages * 120), [scrollViewClassifications bounds].size.height)];
    
}

-(void) classificationTapped:(UITapGestureRecognizer *)sender{


    if ([principleIdSelectedArray count] > 0) {
        [principleIdSelectedArray removeAllObjects];
        [[NSUserDefaults standardUserDefaults] setObject:principleIdSelectedArray forKey:PRINCIPLE_ID_ARRAY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    if ([grammarIdSelectedArray count] > 0) {
        [grammarIdSelectedArray removeAllObjects];
        [[NSUserDefaults standardUserDefaults] setObject:grammarIdSelectedArray forKey:GRAMMAR_ID_ARRAY];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:PRINCIPLE_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:GRAMMAR_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];

    

    
    for (UIView* b in self.grammerSelectionViewBackground.subviews)
    {
        [b removeFromSuperview];
    }
    for (UIView* b in self.principleSelectionViewBackground.subviews)
    {
        [b removeFromSuperview];
    }
    
    
    
    if (grammarsTagArray) {
        [grammarsTagArray release];
        grammarsTagArray = nil;
    }
    
    [tableViewGrammar reloadData];
    
    int cId = sender.view.tag;
    
    [[NSUserDefaults standardUserDefaults] setInteger:cId forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    BOOL classificaitonAlreadySelected = false;
    for (int i = 0; i < [classificationIdArray count]; i++) {
        if ([[classificationIdArray objectAtIndex:i] intValue] == cId) {
            classificaitonAlreadySelected = TRUE;
            [classificationIdArray removeObject:[NSString stringWithFormat:@"%d",cId]];

            UIImageView *imgView = (UIImageView*)[scrollViewClassifications viewWithTag:cId+TICK_TAG_BUFFER];
            imgView.image = [UIImage imageNamed:@""];
            break;
        }
    }
    
    if (!classificaitonAlreadySelected) {
        [classificationIdArray addObject:[NSString stringWithFormat:@"%d",cId]];

        UIImageView *imgView = (UIImageView*)[scrollViewClassifications viewWithTag:cId+TICK_TAG_BUFFER];
        imgView.image = [UIImage imageNamed:@"icon-clip.png"];
    }
    
    if ([classificationIdArray count] == 0) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:cId forKey:@"selectedClassificationId"];

    [[NSUserDefaults standardUserDefaults] setObject:classificationIdArray forKey:CLASSIFICATIONS_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    if ([classificationIdArray count] == 1) {
        if ([[classificationIdArray objectAtIndex:0] intValue] == MY_HOME_CLASSIFICATION) {
            [self showFeather:MY_HOME_FEATHER];
            [principleTagsArr removeAllObjects];
            [tableViewPrincipleView reloadData];
            return;
        }
        else
        {
            [self showFeather:SECOND_FEATHER];
        }
    }
    else{
        [self showFeather:SECOND_FEATHER];
    }

    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];

    MrPContentArray = [[NSMutableArray alloc] init];
    if (activeLanguage == 1) {
        MrPContentArray = [database ArabicWordDatabaseArrayForMainMenu:classificationIdArray];
    }else{
        MrPContentArray = [database EnglishWordDatabaseArrayForMainMenu:classificationIdArray];
    }

    NSMutableArray *principleIdArray = [[NSMutableArray alloc] init];
    _arabicWordDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    for (MrPContentInfo *info in MrPContentArray) {
        [_arabicWordDic setObject:[NSNumber numberWithInt:info.grammarId] forKey:[NSString stringWithFormat:@"grammarId%d",info.grammarId]];
        [_arabicWordDic setObject:info.grammarWord forKey:[NSString stringWithFormat:@"grammarName%d",info.grammarId]]; ;
        [_arabicWordDic setObject:info.principleWord forKey:[NSString stringWithFormat:@"PrincipleName%d",info.principleId]]; ;
        [principleIdArray addObject:[NSString stringWithFormat:@"%d",info.principleId]];
    }
    
    NSLog(@"%@",_arabicWordDic);
    principleTagsArr = [[NSMutableArray alloc] init];
    
    for (id obj in principleIdArray) {
        if (![principleTagsArr containsObject:obj]) {
            [principleTagsArr addObject:obj];
        }
    }
    
    [tableViewPrincipleView reloadData];
}

-(void) loadPrincipleTable:(int)cid{
    
    [classificationIdArray addObject:[NSString stringWithFormat:@"%d",cid]];
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    MrPContentArray = [[NSMutableArray alloc] init];
    if (activeLanguage == 1) {
        MrPContentArray = [database ArabicWordDatabaseArrayForMainMenu:classificationIdArray];
    }else{
        MrPContentArray = [database EnglishWordDatabaseArrayForMainMenu:classificationIdArray];
    }
    
    NSMutableArray *principleIdArray = [[NSMutableArray alloc] init];
    _arabicWordDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    for (MrPContentInfo *info in MrPContentArray) {
        [_arabicWordDic setObject:[NSNumber numberWithInt:info.grammarId] forKey:[NSString stringWithFormat:@"grammarId%d",info.grammarId]];
        [_arabicWordDic setObject:info.grammarWord forKey:[NSString stringWithFormat:@"grammarName%d",info.grammarId]]; ;
        [_arabicWordDic setObject:info.principleWord forKey:[NSString stringWithFormat:@"PrincipleName%d",info.principleId]]; ;
        [principleIdArray addObject:[NSString stringWithFormat:@"%d",info.principleId]];
    }
    
    NSLog(@"%@",_arabicWordDic);
    principleTagsArr = [[NSMutableArray alloc] init];
    
    for (id obj in principleIdArray) {
        if (![principleTagsArr containsObject:obj]) {
            [principleTagsArr addObject:obj];
        }
    }
    
    [tableViewPrincipleView reloadData];
}

-(void) loadGrammars:(int) pId{
    if (grammarsTagArray) {
        [grammarsTagArray release];
        grammarsTagArray = nil;
    }
    
    // Reset all selections
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:GRAMMAR_ID_ARRAY];

    grammarsTagArray = [[NSMutableArray alloc] init];
    MrPContentArray = [[NSMutableArray alloc] init];
    
    if ([classificationIdArray count] == 0) {
        return;
    }
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 1) {
        MrPContentArray = [database ArabicWordDatabaseArrayForMainMenu:classificationIdArray];
    }else{
        MrPContentArray = [database EnglishWordDatabaseArrayForMainMenu:classificationIdArray];
    }
    for (MrPContentInfo *infos in MrPContentArray) {
        if (infos.principleId == pId) {
            [grammarsTagArray addObject:[NSString stringWithFormat:@"%d",infos.grammarId]];
        }
    }
    

    NSInteger index = [grammarsTagArray count] - 1;
    for (id object in [grammarsTagArray reverseObjectEnumerator]) {
        if ([grammarsTagArray indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
            [grammarsTagArray removeObjectAtIndex:index];
        }
        index--;
    }
    
    [tableViewGrammar reloadData];
}


- (void)loadInBackground
{
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"ActiveLanguage"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"LanguageSupport"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:_target_name forKey:[NSString stringWithFormat:@"gradesPurchased"]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:_target_name forKey:@"activeGrade"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"freeContentDownloaded"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"01 02 03 04 05 06 07 08 09 010 011 012 013 014 015 016 017 018 019 020" forKey:@"availableRewards"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self moveFreeResourcesToDocumentDirectory];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allBackgroundMusic"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:mapToLoad forKey:@"mapToLoad"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"freeContentDownloaded"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"appUsedBefore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"freeContentDownloaded2"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"freeContentDownloaded1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PHOTOFEATURE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentGrammarRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentPrincipleRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentGrammarString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentPrincipleString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"version1.5Downloaded"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMyHomeSetup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffAchievements"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMyCamp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMyHomeAddword"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffPlayground"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffSkills"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffCityMap"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffFixIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMainScreenFirstVideo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMatchIt"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMoveIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMyPlayground"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMy_Home_new_concept"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMain_menu_New_concept"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffPickIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffPOPIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffSpinIt"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffTypeIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffWriteIt"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allBackgroundMusic"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"harkat"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentGrammarRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentPrincipleRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentGrammarString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentPrincipleString"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSUserDefaults standardUserDefaults] setBool:true forKey:SYNONYM_ACTIVE];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 120) {
        return;
    }
    
    if (alertView.tag == 121) {
        return;
    }
    
    if (alertView.tag == 100001) {
        
        //        MovieViewController* moviePlayers = [[MovieViewController alloc] init];
        //        [moviePlayers.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
        //        [self presentModalViewController:moviePlayers animated:NO];
        //        NSString* movieName = [NSString stringWithFormat:@"G1G4IntroVideo"];//@"Grade_Video";
        //        [moviePlayers playMovie:movieName];
    }
    else{
        //    if (buttonIndex == 0)
        //    {
        //        if (myCampPlayer) {
        //            [myCampPlayer stop];
        //            [myCampPlayer release];
        //            myCampPlayer = nil;
        //        }
        //
        //        MovieViewController* moviePlayers = [[MovieViewController alloc] init];
        //        [moviePlayers.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
        //        [self presentModalViewController:moviePlayers animated:NO];
        //        NSString* movieName = @"KG_Video";
        //        [moviePlayers playMovie:movieName];
        //        }
    }
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    
    NSString* fileName = [NSString stringWithFormat:@"2-Camp"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    myCampPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    myCampPlayer.volume = 0.15;
    myCampPlayer.numberOfLoops = 20;
    myCampPlayer.delegate = self;
    
    if ([myCampPlayer prepareToPlay]) {
        [myCampPlayer setCurrentTime:0.0];
        [myCampPlayer play];
    }
    
}
- (void)viewDidUnload
{
    [tableViewPrincipleView release];
    tableViewPrincipleView = nil;
    [tableViewGrammar release];
    tableViewGrammar = nil;
    [scrollViewClassifications release];
    scrollViewClassifications = nil;
    [btnPlayHelpSpund release];
    btnPlayHelpSpund = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    CityMap =nil;
    SetupBtn = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (BOOL)checkForBoundaryConditionsBeforeExiting{
    
    NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    if([objectClassification isKindOfClass:[NSString class]] == YES)
    {
        UIAlertView *selectionAlert;
        selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Please select 1 2 3 and then 4" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
        [selectionAlert show];
            return false;
        
    }
    
    NSArray *objectPrinciple = [[NSUserDefaults standardUserDefaults] objectForKey:PRINCIPLE_ID_ARRAY];
    if([objectPrinciple isKindOfClass:[NSString class]] == YES)
    {
        UIAlertView *selectionAlert;
        selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Please select 1 2 3 and then 4" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
        [selectionAlert show];
            return false;
        
    }
    
    NSArray *objectGrammar = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
    if([objectGrammar isKindOfClass:[NSString class]] == YES)
    {
        UIAlertView *selectionAlert;
        selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Please select 1 2 3 and then 4" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
        [selectionAlert show];
            return false;
        
    }
    else if ([objectGrammar isKindOfClass:[NSArray class]] == YES)
    {
        if ([objectGrammar count] == 0) {
            UIAlertView *selectionAlert;
            selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Please select 1 2 3 and then 4" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
            [selectionAlert show];
            return false;
            
        }
    }
    return true;
}
- (IBAction)exitMainMenu:(id)sender {
    
    

    
    
    
    if (myCampPlayer) {
        [myCampPlayer stop];
        [myCampPlayer release];
        myCampPlayer = nil;
    }
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];


    
    switch ([sender tag]) {
        case SETTINGS:
        {
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SetUpViewControllerIdentifier"];
            vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];

        }
            break;
        case MY_CAMP:
        {
            
            NSArray *objectPrincipleId = [[NSUserDefaults standardUserDefaults] objectForKey:PRINCIPLE_ID_ARRAY];
            if([objectPrincipleId isKindOfClass:[NSArray class]] == YES){
                if ([objectPrincipleId count] == 0) {
                    UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly Select Training" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                    [selectionAlert show];
                    
                    return;
                }
                
                if ([[objectPrincipleId objectAtIndex:0] isEqualToString:@"3020"]) {
                    UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Go to Playground" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                    [selectionAlert show];

                    return;
                }
            }
            
            
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 0) {
                    UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly Select Choices" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                    [selectionAlert show];
                    return;
                }
                
                if ([objGrammarId count] == 1) {
                if ([[objGrammarId objectAtIndex:0] isEqualToString:@"1380"]) {
                    UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Go to Playground" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                    [selectionAlert show];
                    return;
                    }
                }
            }
            
            NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
            if ([ActiveGrade isEqualToString:@"G1"]) {
                NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
                if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                    if ([objGrammarId count] == 1) {
                        if (
                            [[objGrammarId objectAtIndex:0] isEqualToString:@"294"] ||
                            [[objGrammarId objectAtIndex:0] isEqualToString:@"295"] ||
                            [[objGrammarId objectAtIndex:0] isEqualToString:@"296"] ||
                            [[objGrammarId objectAtIndex:0] isEqualToString:@"297"] ||
                            [[objGrammarId objectAtIndex:0] isEqualToString:@"298"] ||
                            [[objGrammarId objectAtIndex:0] isEqualToString:@"299"]
                            )
                        {
                            UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Go to Playground" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                            [selectionAlert show];
                            return;
   
                        }
                        
                    }
                }
            }
            
            
            if ([self checkForBoundaryConditionsBeforeExiting]) {
                    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyCampControllerIderntifier"];
                    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                    [self presentViewController:vc animated:YES completion:NULL];
            }
            

            }
            break;
        case MY_HOME:
        {
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyHomeViewControllerIdentifier"];
            vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];

        }
            break;
        case MY_PLAYGROUND:
        {
            if ([self checkForBoundaryConditionsBeforeExiting]) {
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyPlaygroundIdentifier"];
            vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];
            }
            }
            break;
        case ACHIEVEMENTS:{
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyAcheivementViewControllerIderntifier"];
            vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];

        }
            break;
            
        default:
            break;
    }

}

- (IBAction)introButtonPressed:(id)sender {
}

- (IBAction)playVideo:(id)sender {
    [self loadMoviePlayer];
}

- (void)loadMoviePlayer
{
    // Play movie from the bundle
    
    //    moviePlayer = [[[CustomMoviePlayerViewController alloc] initWithPath:moviePlayed] autorelease];
    //    [self presentModalViewController:moviePlayer animated:NO];
    //    // Prep and play the movie
    //    NSString* movieName = @"loadFromURL";
    //    [moviePlayer readyPlayer:movieName];
    

    if (myCampPlayer) {
        [myCampPlayer stop];
        [myCampPlayer release];
        myCampPlayer = nil;
    }
    
    MovieViewController* moviePlayers = [[MovieViewController alloc] init];
    [moviePlayers.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayers animated:NO];
    
    // Prep and play the movie
    NSString* movieName = @"Main_menu_New_concept";
    [moviePlayers playMovie:movieName];
    
}
// Copy Databases and G1/ G1 school resources
- (void)replaceExistingDatabasesWithNewOnes{
    // First, test for existence.
    NSArray *arrDatabases = [[NSArray alloc] initWithObjects:_dbname,/*@"NH_G3.sqlite3",@"G3_Skiils_New.sqlite3",@"G1.sqlite3",@"G3.sqlite3",@"G4.sqlite3",*/ nil];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    for (int i = 0; i < [arrDatabases count]; i++) {
        NSLog(@"Copying Database %@ to doc dir",[arrDatabases objectAtIndex:i]);
        NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:[arrDatabases objectAtIndex:i]];
        success = [fileManager fileExistsAtPath:writableDBPath];
        if (success){
            
            [[NSFileManager defaultManager] removeItemAtPath:writableDBPath error:nil];
            
            NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[arrDatabases objectAtIndex:i]];
            success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
            
            if (!success) {
                NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
            }
            NSLog(@"Successfully copied %@ to doc dir",[arrDatabases objectAtIndex:i]);
            NSURL *pathURL= [NSURL fileURLWithPath:writableDBPath];
            [self addSkipBackupAttributeToItemAtURL:pathURL];
        }
    }
}

- (void)didReceiveMemoryWarning {
    
    
}

-(void) moveFreeResourcesToDocumentDirectory{
    
    
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:_target_name];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];


    //Moving Books
//    NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"KG2"];
//
//    NSString *destinationPath;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    destinationPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:_target_name];
//    NSError *error;
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    BOOL freecontent = [fileManager fileExistsAtPath:destinationPath];
//    if (!freecontent){
//        BOOL success = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
//        if (!success) {
//            NSLog(@"copying not successful with error: %@",error);
//        }
//        NSLog(@"Documents directory: %@", [[NSFileManager defaultManager] contentsOfDirectoryAtPath:destinationPath error:&error]);
//        NSURL *pathURL= [NSURL fileURLWithPath:destinationPath];
//        [self addSkipBackupAttributeToItemAtURL:pathURL];
//    }

    // Books Coping Finished
    
//    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
//    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:_target_name];
//    
//    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
//        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil]; //Create folder
//    NSURL *pathURL= [NSURL fileURLWithPath:dataPath];
//    [self addSkipBackupAttributeToItemAtURL:pathURL];

    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (tableView.tag == TABLE_VIEW_PRINCIPLES) {

        return [principleTagsArr count];
    }
    else{

        return [grammarsTagArray count];
    }
    return 0;
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == TABLE_VIEW_PRINCIPLES) {
    NSString *kCellIdentifier = @"mainmenuGrammarPrincipleCell";
    
    mainmenuGrammarPrincipleCell *principleCell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (principleCell == nil)
    {
        
        principleCell = [mainmenuGrammarPrincipleCell createCell];
        principleCell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSString *imageName =  [_arabicWordDic objectForKey:[NSString stringWithFormat:@"PrincipleName%@",[principleTagsArr objectAtIndex:indexPath.row]]];

    principleCell.imgViewCellBackground.image = [UIImage imageNamed:imageName];
    principleCell.tag = [[principleTagsArr objectAtIndex:indexPath.row] intValue];
    [principleCell.textLabel setFont:[UIFont boldSystemFontOfSize:24]];
    principleCell.textLabel.textColor = [UIColor blackColor];
    principleCell.backgroundColor = [UIColor clearColor];
    principleCell.textLabel.textAlignment = UITextAlignmentRight;

    return principleCell;
    }else{
    
        
        NSString *kCellIdentifier = @"mainmenuGrammarPrincipleCell";
        
        mainmenuGrammarPrincipleCell *grammarCell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
        if (grammarCell == nil)
        {
            
            grammarCell = [mainmenuGrammarPrincipleCell createCell];
            grammarCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        NSString *imageName =  [_arabicWordDic objectForKey:[NSString stringWithFormat:@"grammarName%@",[grammarsTagArray objectAtIndex:indexPath.row]]];        
        grammarCell.imgViewCellBackground.image = [UIImage imageNamed:imageName];
        grammarCell.tag = [[grammarsTagArray objectAtIndex:indexPath.row] intValue];
        [grammarCell.textLabel setFont:[UIFont boldSystemFontOfSize:20]];
        grammarCell.textLabel.textColor = [UIColor blackColor];
        grammarCell.backgroundColor = [UIColor clearColor];
        [grammarCell.textLabel sizeToFit];
        grammarCell.textLabel.textAlignment = UITextAlignmentRight;

        for (NSString* val in grammerIconClipArray) {
            if ([val isEqualToString:[NSString stringWithFormat:@"%d",indexPath.row+100]]) {
                
                UIImageView *selectionView = [[UIImageView alloc] initWithFrame:CGRectMake(grammarCell.frame.size.width-60, 0, 60, 84)];
                selectionView.contentMode = UIViewContentModeScaleAspectFill;
                selectionView.tag = indexPath.row + 100;
                selectionView.image = [UIImage imageNamed:@"icon-clip2.png"];
                [grammarCell addSubview:selectionView];

            }
        }
//        if ([grammarCell viewWithTag:indexPath.row+100]) {
//        }
        
        return grammarCell;
    }
    return nil;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int tag;
    if (tableView.tag == TABLE_VIEW_PRINCIPLES) {
        [self showFeather:THIRD_FEATHER];
        tag = [[principleTagsArr objectAtIndex:indexPath.row] intValue];
        
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [NSString stringWithFormat:@"%d",tag]];
            NSArray *results = [principleIdSelectedArray filteredArrayUsingPredicate:predicate];
            if ([results count] > 0) {
               [principleIdSelectedArray removeObject:[NSString stringWithFormat:@"%d",tag ]];
            }
            else{
                [principleIdSelectedArray addObject:[NSString stringWithFormat:@"%d",tag ]];
            }
        
        for ( int i = 0; i < [principleIdSelectedArray count]; i++) {
            if (![[NSString stringWithFormat:@"%d",tag ] isEqualToString:[principleIdSelectedArray objectAtIndex:i]])
                [principleIdSelectedArray removeObjectAtIndex:i];
        }
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:PRINCIPLE_ID_ARRAY];

        [[NSUserDefaults standardUserDefaults] setObject:principleIdSelectedArray forKey:PRINCIPLE_ID_ARRAY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        

        
        currentSelectedPrincipleIndex = indexPath.row;
        [self loadGrammars:tag];
        [self updatePrincipleSelectionView:indexPath.row];
    }
    else{
        [self showFeather:FOURTH_FEATHER];
        tag = [[grammarsTagArray objectAtIndex:indexPath.row] intValue];
        

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [NSString stringWithFormat:@"%d",tag]];
        NSArray *results = [grammarIdSelectedArray filteredArrayUsingPredicate:predicate];
        if ([results count] > 0) {
            [grammarIdSelectedArray removeObject:[NSString stringWithFormat:@"%d",tag ]];
        }
        else{
            [grammarIdSelectedArray addObject:[NSString stringWithFormat:@"%d",tag ]];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:grammarIdSelectedArray forKey:GRAMMAR_ID_ARRAY];
        [[NSUserDefaults standardUserDefaults] synchronize];

        
//        [[NSUserDefaults standardUserDefaults] setInteger:tag forKey:@"MainMenuGrammarId"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        mainmenuGrammarPrincipleCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if ([cell viewWithTag:indexPath.row+100]) {
            [[cell viewWithTag:indexPath.row+100] removeFromSuperview];
            [grammerIconClipArray removeObject:[NSString stringWithFormat:@"%d",indexPath.row+100]];
        }
        else{
            UIImageView *selectionView = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.size.width-60, 0, 60, 84)];
            selectionView.contentMode = UIViewContentModeScaleAspectFill;
            selectionView.tag = indexPath.row + 100;
            selectionView.image = [UIImage imageNamed:@"icon-clip2.png"];
            [cell addSubview:selectionView];
            [grammerIconClipArray addObject:[NSString stringWithFormat:@"%d",indexPath.row+100]];
        }
        

        [self updateGrammarSelectionView:indexPath.row];
    }
}

-(void) updatePrincipleSelectionView:(int) selectedIndex{
    for (UIView* b in self.grammerSelectionViewBackground.subviews)
    {
        [b removeFromSuperview];
    }
    
    for (int i = 100; i < 105; i++) {
        if (i != selectedIndex+100) { // unselecting all other grammar arrows
        if ([self.principleSelectionViewBackground viewWithTag:i]) {
            [[self.principleSelectionViewBackground viewWithTag:i] removeFromSuperview];
            }
        }
    }
    
    if ([self.principleSelectionViewBackground viewWithTag:selectedIndex+100]) {
        [[self.principleSelectionViewBackground viewWithTag:selectedIndex+100] removeFromSuperview];
    }
    else{
//
        UIImageView *selectionView = [[UIImageView alloc] initWithFrame:CGRectMake(0, selectedIndex*44, self.principleSelectionViewBackground.frame.size.width, 44)];
//        selectionView.backgroundColor = [UIColor greenColor];
        selectionView.image = [UIImage imageNamed:@"icon-arrow.png"];
        selectionView.contentMode = UIViewContentModeScaleAspectFill;
        
        selectionView.tag = selectedIndex + 100;
        [self.principleSelectionViewBackground addSubview:selectionView];
    }
}

-(void) updateGrammarSelectionView:(int) selectedIndex{
    [self updatePrincipleSelectionViewIfNotSelected];
    return;
    if ([self.grammerSelectionViewBackground viewWithTag:selectedIndex+100]) {
        [[self.grammerSelectionViewBackground viewWithTag:selectedIndex+100] removeFromSuperview];
    }
    else{
        UIImageView *selectionView = [[UIImageView alloc] initWithFrame:CGRectMake(0, selectedIndex*44, self.grammerSelectionViewBackground.frame.size.width, 44)];
                selectionView.contentMode = UIViewContentModeScaleAspectFill;
        selectionView.tag = selectedIndex + 100;
        selectionView.image = [UIImage imageNamed:@"icon-clip2.png"];
        [self.grammerSelectionViewBackground addSubview:selectionView];

    }
}

-(void) updatePrincipleSelectionViewIfNotSelected{
    int selectedIndex = currentSelectedPrincipleIndex;
    if ([self.principleSelectionViewBackground viewWithTag:selectedIndex+100]) {
        return;
    }
    else{
//        UIView *selectionView = [[UIView alloc] initWithFrame:CGRectMake(0, selectedIndex*44, self.principleSelectionViewBackground.frame.size.width, 44)];
//        selectionView.backgroundColor = [UIColor greenColor];
//        
//        selectionView.tag = selectedIndex + 100;
        UIImageView *selectionView = [[UIImageView alloc] initWithFrame:CGRectMake(0, selectedIndex*44, self.principleSelectionViewBackground.frame.size.width, 44)];
                selectionView.contentMode = UIViewContentModeScaleAspectFill;
        //        selectionView.backgroundColor = [UIColor greenColor];
        selectionView.image = [UIImage imageNamed:@"icon-arrow.png"];
        selectionView.tag = selectedIndex + 100;
        [self.principleSelectionViewBackground addSubview:selectionView];

//        [self.principleSelectionViewBackground addSubview:selectionView];
    }

}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (void)dealloc {
    [tableViewPrincipleView release];
    [tableViewGrammar release];
    [scrollViewClassifications release];
    [btnPlayHelpSpund release];
    [imgView1Feather release];
    [imgView2Feather release];
    [imgView3Feather release];
    [imgView4Feather release];
    [imgView5Feather release];
    [super dealloc];
}
- (IBAction)actionPlayHelpSound:(id)sender {
    
    return;
    [self playVideo:@"Main_menu_New_concept"];
    return;
    
    if (myCampPlayer) {
        [myCampPlayer stop];
        [myCampPlayer release];
        myCampPlayer = Nil;
    }
    
    
    NSString* soundName;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 2) {
        return;

    }
    else {
        soundName = @"ASH040";
    }
    
    NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:squishPath];
    if (!success) {
        squishPath = [[NSBundle mainBundle] pathForResource:@"EmptySound" ofType:@"mp3"];
    }

    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    myCampPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    myCampPlayer.numberOfLoops = 0;
    myCampPlayer.delegate = self;
    [myCampPlayer play];
    
}

-(void) showFeather:(int) feather{
    switch (feather) {
        case FIRST_FEATHER:
        {
            imgView1Feather.hidden = NO;
            imgView2Feather.hidden = YES;
            imgView3Feather.hidden = YES;
            imgView4Feather.hidden = YES;
            imgView5Feather.hidden = YES;
        }
            break;
        case SECOND_FEATHER:
        {
            imgView1Feather.hidden = YES;
            imgView2Feather.hidden = NO;
            imgView3Feather.hidden = YES;
            imgView4Feather.hidden = YES;
            imgView5Feather.hidden = YES;
        }
            break;

        case THIRD_FEATHER:
        {
            imgView1Feather.hidden = YES;
            imgView2Feather.hidden = YES;
            imgView3Feather.hidden = NO;
            imgView4Feather.hidden = YES;
            imgView5Feather.hidden = YES;
        }
            break;

        case FOURTH_FEATHER:
        {
            imgView1Feather.hidden = YES;
            imgView2Feather.hidden = YES;
            imgView3Feather.hidden = YES;
            imgView4Feather.hidden = NO;
            imgView5Feather.hidden = YES;
        }
            break;
        case MY_HOME_FEATHER:
        {
            imgView1Feather.hidden = YES;
            imgView2Feather.hidden = YES;
            imgView3Feather.hidden = YES;
            imgView4Feather.hidden = YES;
            imgView5Feather.hidden = NO;
        }
            break;

            
            
        default:
            break;
    }

}

@end
