//
//  CityPopupController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/17/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "CityPopupController.h"

@interface CityPopupController ()

@end

@implementation CityPopupController
@synthesize PopupClose;
@synthesize popView;
@synthesize BtnPlay;
@synthesize BtnPractice;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [self setPopView:nil];
    [self setPopupClose:nil];
    [self setBtnPractice:nil];
    [self setBtnPractice:nil];
    [self setBtnPlay:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)PopupCloseTouch:(id)sender {
     
    [self dismissModalViewControllerAnimated:NO];
////    [self setPopView:nil];
////    [self setPopupClose:nil];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self dismissViewControllerAnimated:NO completion:nil];
//    });
////    [self dismissViewControllerAnimated:NO completion:NULL];
////    [super viewDidUnload];
}
- (void)dealloc {
    [BtnPractice release];
    [BtnPractice release];
    [BtnPlay release];
    [super dealloc];
}
@end
