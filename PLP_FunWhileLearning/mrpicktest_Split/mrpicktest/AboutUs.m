//
//  AboutUs.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 12/18/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "AboutUs.h"
#import "MKParentalGate.h"


@interface AboutUs ()

@end

@implementation AboutUs
@synthesize btnMrPickIt;
@synthesize btnSmartSoft;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [btnMrPickIt setTitle:@"www.mrpickit.net" forState:UIControlStateSelected];
    [btnMrPickIt setTitle:@"www.mrpickit.net" forState:UIControlStateHighlighted];
    [btnSmartSoft setTitle:@"www.smartsoft.jo" forState:UIControlStateSelected];
    [btnSmartSoft setTitle:@"www.smartsoft.jo" forState:UIControlStateHighlighted];
    
    [btnMrPickIt setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [btnSmartSoft setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    
    [btnMrPickIt setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    [btnSmartSoft setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];

}





- (void)viewDidUnload
{
    [self setBtnMrPickIt:nil];
    [self setBtnSmartSoft:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}



- (void)dealloc {
    [btnMrPickIt release];
    [btnSmartSoft release];
    [super dealloc];
}
- (IBAction)openMrPickIt:(id)sender {
    
//    MKParentalGateSuccessBlock success = ^{
        NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.mrpickit.net" ];
        [[UIApplication sharedApplication] openURL:url];
        [url release];        
//    };
//    
//    [MKParentalGate displayGateWithCurrentViewController:self successBlock:success failureBlock:NULL];
    
}

- (IBAction)openSmartSoft:(id)sender {
//    MKParentalGateSuccessBlock success = ^{
        NSURL *url = [ [ NSURL alloc ] initWithString: @"http://www.smartsoft.jo" ];
        [[UIApplication sharedApplication] openURL:url];
        [url release];
//};
//    
//    [MKParentalGate displayGateWithCurrentViewController:self successBlock:success failureBlock:NULL];
//
}

// ParentalGate
#pragma mark
#pragma Parental Gate




@end
