//
//  mainmenuGrammarPrincipleCell.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 25/08/2014.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mainmenuGrammarPrincipleCell : UITableViewCell

+(id) createCell;

@property(nonatomic,retain) IBOutlet UIImageView *imgViewCellBackground;
@end
