//
//  MovieViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 4/23/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "MovieViewController.h"
#import "CityPopupController.h"
@interface MovieViewController ()

@end

@implementation MovieViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"VIEWDID LOAD MOVIE");
}

-(void)playMovie:(NSString*)sender
{
    NSLog(@"Playing MOVIE");
    
    movieNamePlayed = sender;
    
    [self showMovie:sender];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {

    NSLog(@"DID FINISH MOVIE");
    MPMoviePlayerController *player = [notification object];
    if ([player
         respondsToSelector:@selector(setFullscreen:animated:)])
    {
        NSLog(@"Removing PLAYER");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerWillExitFullscreenNotification object:player];
    
     _moviePlayer.controlStyle = MPMovieControlStyleNone;
    

        [player stop];
        [player.view removeFromSuperview];
        
        if (_moviePlayer) {
            _moviePlayer = Nil;
            [_moviePlayer release];
        }
    }
//    [self dismissModalViewControllerAnimated:NO];
    
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"videoTutorialDone" object:self]];
    


}

// UPdated functions

- (void)willEnterFullscreen:(NSNotification*)notification {
    NSLog(@"willEnterFullscreen");
}

- (void)enteredFullscreen:(NSNotification*)notification {
    NSLog(@"enteredFullscreen");
}

- (void)willExitFullscreen:(NSNotification*)notification {
    NSLog(@"willExitFullscreen");
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if (buttonIndex == 1) {
        if (movieNamePlayed) {
            NSString* switchoffString = [NSString stringWithFormat:@"videoHelpOff%@",movieNamePlayed];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:switchoffString];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

- (void)exitedFullscreen:(NSNotification*)notification {
    
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Video Help"];
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 1) {
        [alert setMessage:@"أعرض فيديو المساعدة مرة أخرة "];
    }
    else{
        [alert setMessage:@"Do you want video help to be played next time too?"];
    }
    
    [alert addButtonWithTitle:@"YES"];
    [alert addButtonWithTitle:@"NO"];
    [alert setDelegate:self];
    [alert show];
    [alert release];
    
    NSLog(@"exitedFullscreen");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"DID FINISH MOVIE");
    MPMoviePlayerController *player = [notification object];
    if ([player
         respondsToSelector:@selector(setFullscreen:animated:)])
    {
        NSLog(@"Removing PLAYER");
        
        _moviePlayer.controlStyle = MPMovieControlStyleNone;
        
        
        [player stop];
        [player.view removeFromSuperview];
        
        if (_moviePlayer) {
            _moviePlayer = Nil;
            [_moviePlayer release];
        }
    }
    [self dismissModalViewControllerAnimated:NO];
    
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"videoTutorialDone" object:self]];

}

- (void)playbackFinished:(NSNotification*)notification {
    NSNumber* reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([reason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
            NSLog(@"playbackFinished. Reason: Playback Ended");            
    
        [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"videoTutorialDone" object:self]];
            break;
        case MPMovieFinishReasonPlaybackError:
            NSLog(@"playbackFinished. Reason: Playback Error");
            break;
        case MPMovieFinishReasonUserExited:
            NSLog(@"playbackFinished. Reason: User Exited");
            break;
        default:
            break;
    }
//    [_moviePlayer setFullscreen:NO animated:YES];
}

- (void)showMovie:(NSString*) sender {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterFullscreen:) name:MPMoviePlayerWillEnterFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willExitFullscreen:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enteredFullscreen:) name:MPMoviePlayerDidEnterFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitedFullscreen:) name:MPMoviePlayerDidExitFullscreenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    NSURL *url;
//    if ([sender isEqualToString:@"loadFromURL"]) {
//        
//        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//        NSString* moviePlayed;
//        if (activeLanguage == 1) {
//            moviePlayed = @"http://www.mrpickit.net/apphelp/video/Arabic_Mian_screen_full.mp4";
//        }
//        else {
//            moviePlayed = @"http://www.mrpickit.net/apphelp/video/enfull.mp4";
//        }
//        
//        NSString *encodedUrl = [moviePlayed stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//        url = [NSURL URLWithString:encodedUrl];
//        [url retain];
//    }
//    else{
        url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                      pathForResource:sender ofType:@"m4v"]];
//    }
    
    _moviePlayer =  [[MPMoviePlayerController alloc]
                     initWithContentURL:url];
    
//    [[_moviePlayer view] setFrame:CGRectMake(0, 0, 1028, 768)];
    
    
    _moviePlayer.controlStyle = MPMovieControlStyleDefault;
    _moviePlayer.shouldAutoplay = YES;
    [self.view addSubview:_moviePlayer.view];
    [_moviePlayer setFullscreen:YES animated:YES];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadView
{
    [self setView:[[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]] autorelease]];
    [[self view] setBackgroundColor:[UIColor blackColor]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)dealloc {
    [_moviePlayer release];
    [super dealloc];
}

@end
