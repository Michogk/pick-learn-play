//
//  GirlCharacterSelectionViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 8/25/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "GirlCharacterSelectionViewController.h"

@interface GirlCharacterSelectionViewController ()

@end

@implementation GirlCharacterSelectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:Nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showActionSheet{
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    UIActionSheet *action;
    if (activeLanguage == 1) {
        action = [[UIActionSheet alloc] initWithTitle:@"لتفعيل الشخصية يجب زيارة انجازاتي"
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
    }
    
    else{    
        action = [[UIActionSheet alloc] initWithTitle:@"To activate character visit 'My Achievements'"
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
    }

    
    [action showFromRect:CGRectMake(0, 0, 200, 200) inView:self.view animated:NO];    
    
}

-(void) showActionSheetForActiveCharacter{
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    UIActionSheet *action;
    if (activeLanguage == 1) {
        action = [[UIActionSheet alloc] initWithTitle:@"ستجد الشخصية المختارة في ملعبي"
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:@"OK"
                                    otherButtonTitles:nil, nil];
    }
    
    else{
        action = [[UIActionSheet alloc] initWithTitle:@"Chosen character is active. Check it in 'My Playground'"
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:@"OK"
                                    otherButtonTitles:nil, nil];
    }
    
    
    [action showFromRect:CGRectMake(0, 0, 200, 200) inView:self.view animated:NO];
    
}

- (IBAction)btnCharacterBasketBallPressed:(id)sender {
    
//    UIAlertView *alert = [[UIAlertView alloc] init];
//    [alert setTitle:@"Alert"];
//    [alert setMessage:@"Do you want to activate this character? "];
//    [alert addButtonWithTitle:@"YES"];
//    [alert addButtonWithTitle:@"NO"];
//    [alert setDelegate:self];
//    [alert show];
//    [alert release];
//    return;
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"BasketBallGirl"]) {
        [self showActionSheet];
    }
    else{
    
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:@"girlSelectedCurrent.png"]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:Nil]];
    
    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }

    
}

- (IBAction)btnCharacterDoctorPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"LadyDoctor"]) {
        [self showActionSheet];
    }
    else{
    
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:@"girlSelectedCurrent.png"]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:6 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        [self showActionSheetForActiveCharacter];
    }

}

- (IBAction)btnCharacterSimpleGirlPressed:(id)sender {
    
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:@"girlSelectedCurrent.png"]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
            [self showActionSheetForActiveCharacter];

}

- (IBAction)btnCharacterChefPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"ChefGirl"]) {
        [self showActionSheet];
    }
    else{
    
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:@"girlSelectedCurrent.png"]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:8 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }
    
}

- (IBAction)btnCharacterFootballPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FootBallGirl"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:@"girlSelectedCurrent.png"]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:12 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }
}

- (IBAction)btnCharacterPolicePressed:(id)sender {
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"PoliceWoman"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:@"girlSelectedCurrent.png"]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:16 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }
}

- (IBAction)btnCharacterArabPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"ArabGirl"]) {
        [self showActionSheet];
    }
    else{
    
    [_imgViewArab setImage:[UIImage imageNamed:@"girlSelectedCurrent.png"]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:14 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }
}

- (IBAction)btnCharacterFiremanPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FiremanGirl"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleGirl setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFiregirl setImage:[UIImage imageNamed:@"girlSelectedCurrent.png"]];
    [[NSUserDefaults standardUserDefaults] setInteger:10 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }
}

- (void)dealloc {
    [_btnBasketGirl release];
    [_imgViewSimpleGirl release];
    [_imgViewChef release];
    [_imgViewFootball release];
    [_imgViewFiregirl release];
    [_imgViewDoctor release];
    [_imgViewPolice release];
    [_imgViewBasketBall release];
    [_imgViewArab release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setBtnBasketGirl:nil];
    [self setImgViewSimpleGirl:nil];
    [self setImgViewChef:nil];
    [self setImgViewFootball:nil];
    [self setImgViewFiregirl:nil];
    [self setImgViewDoctor:nil];
    [self setImgViewPolice:nil];
    [self setImgViewBasketBall:nil];
    [self setImgViewArab:nil];
    [super viewDidUnload];
}
@end
