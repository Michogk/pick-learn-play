//
//  PickItViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 3/31/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "MoveItViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "DocumentDirectory.h"
#import "SimpleAudioEngine.h"
#import "MyPlayGroundViewController.h"
#import "MrPReport.h"
#import "Common.h"

#define Moveit 7

@interface MoveItViewController ()

@end

@implementation MoveItViewController
@synthesize imgCheck1 = _imgCheck1;
@synthesize imgCheck2 = _imgCheck2;
@synthesize imgCheck3 = _imgCheck3;
@synthesize btnNext = _btnNext;
@synthesize imgViewCheering = _imgViewCheering;
@synthesize lblClassification = _lblClassification;
//@synthesize btnWords = _btnWords;
//@synthesize BtnImages = _BtnImages;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}
-(NSMutableArray*) generateRandomSelectionsOfSecondClassification{
    int randomIndex;
    NSMutableArray *arrRandItemsSelection  = [[NSMutableArray alloc] init];
    for (int j = 0; j < 5; j++) {
        if (j == 0) {
            randomIndex = rand() % [tempIncontentDicCorrect count]/6;
            [arrRandItemsSelection addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
        if (j == 1) {
            do {
                randomIndex = rand() % [tempIncontentDicCorrect count]/6;
            } while (randomIndex == [[arrRandItemsSelection objectAtIndex:0] intValue]);
            [arrRandItemsSelection addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
        if (j == 2) {
            do {
                randomIndex = rand() % [tempIncontentDicCorrect count]/6;
            } while (randomIndex == [[arrRandItemsSelection objectAtIndex:0] intValue] || randomIndex == [[arrRandItemsSelection objectAtIndex:1] intValue]);
            [arrRandItemsSelection addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
        if (j == 3) {
            do {
                randomIndex = rand() % [tempIncontentDicCorrect count]/6;
            } while (randomIndex == [[arrRandItemsSelection objectAtIndex:0] intValue] || randomIndex == [[arrRandItemsSelection objectAtIndex:1] intValue] || randomIndex == [[arrRandItemsSelection objectAtIndex:2] intValue]);
            [arrRandItemsSelection addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
        
        if (j == 4) {
            do {
                randomIndex = rand() % [tempIncontentDicCorrect count]/6;
            } while (randomIndex == [[arrRandItemsSelection objectAtIndex:0] intValue] || randomIndex == [[arrRandItemsSelection objectAtIndex:1] intValue] || randomIndex == [[arrRandItemsSelection objectAtIndex:2] intValue] || randomIndex == [[arrRandItemsSelection objectAtIndex:3] intValue]);
            [arrRandItemsSelection addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
    }
    return arrRandItemsSelection;
}

-(NSMutableArray*)  generateRandomPositioningOfItemsOfSecondClassification{
    int randomIndex;
    NSMutableArray *arrRandPositionMaking = [[NSMutableArray alloc] init];
    for (int j = 0; j < 5; j++) {
        if (j == 0) {
            randomIndex = rand() % 6 + 1;
            [arrRandPositionMaking addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
        if (j == 1) {
            do {
                randomIndex = rand() % 6 + 1;
            } while (randomIndex == [[arrRandPositionMaking objectAtIndex:0] intValue]);
            [arrRandPositionMaking addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
        if (j == 2) {
            do {
                randomIndex = rand() % 6 + 1;
            } while (randomIndex == [[arrRandPositionMaking objectAtIndex:0] intValue] || randomIndex == [[arrRandPositionMaking objectAtIndex:1] intValue]);
            [arrRandPositionMaking addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
        if (j == 3) {
            do {
                randomIndex = rand() % 6 + 1;
            } while (randomIndex == [[arrRandPositionMaking objectAtIndex:0] intValue] || randomIndex == [[arrRandPositionMaking objectAtIndex:1] intValue] || randomIndex == [[arrRandPositionMaking objectAtIndex:2] intValue]);
            [arrRandPositionMaking addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
        
        if (j == 4) {
            do {
                randomIndex = rand() % 6 + 1;
            } while (randomIndex == [[arrRandPositionMaking objectAtIndex:0] intValue] || randomIndex == [[arrRandPositionMaking objectAtIndex:1] intValue] || randomIndex == [[arrRandPositionMaking objectAtIndex:2] intValue] || randomIndex == [[arrRandPositionMaking objectAtIndex:3] intValue]);
            [arrRandPositionMaking addObject:[NSString stringWithFormat:@"%d",randomIndex]];
        }
    }
    return arrRandPositionMaking;
    
}
-(void) clearPrevLevel{
    
    [_imgCheck1 setImage:NULL];
    //    [_imgCheck2 setImage:NULL];
    //    [_imgCheck3 setImage:NULL];
    
    // Removing correct tags
    for (int k = 0; k < 1; k++) {
        if ([self.view viewWithTag:[[arrRecordCorrectTagsOfItems objectAtIndex:k] intValue]]) {
            [[self.view viewWithTag:[[arrRecordCorrectTagsOfItems objectAtIndex:k] intValue] ] removeFromSuperview];
        }
    }
    
    for (int k = 0; k < 5; k++) {
        if ([self.view viewWithTag:[[arrRecordINCorrectTagsOfItems objectAtIndex:k] intValue]]) {
            [[self.view viewWithTag:[[arrRecordINCorrectTagsOfItems objectAtIndex:k] intValue] ] removeFromSuperview];
        }
    }
    
    if (arrRecordCorrectTagsOfItems) {
        [arrRecordCorrectTagsOfItems removeAllObjects];
        arrRecordCorrectTagsOfItems = Nil;
        [arrRecordCorrectTagsOfItems release];
    }
    if (arrRecordINCorrectTagsOfItems) {
        [arrRecordINCorrectTagsOfItems removeAllObjects];
        arrRecordINCorrectTagsOfItems = Nil;
        [arrRecordINCorrectTagsOfItems release];
    }
}
-(void) classificationCompleted{
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Game Ends"];
    [alert setMessage:@"You have completed this location. Please select other location from MAP"];
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"OK"];
    [alert setTag:2];
    [alert show];
    [alert release];
    [_btnNext setEnabled:NO];
    //    [_btnWords setEnabled:NO];
    //    [_BtnImages setEnabled:NO];
    
    NSLog(@"Completed Level");
}

-(void) startGame{
    [self clearPrevLevel];
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],26,cId,activeLanguage,ActiveGrade];
    wordsCount = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCounterString];
    
    
    if (wordsCount + 2 >= [tempContentDicCorrect count]/6) {
        wordsCount = 0;
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        //        cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Moveit,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount+1 forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self saveProgressForReport];
        [self insitiantiateForReportCouting];
    }
    
    NSString *labelClassifcation;
    if((cId > 989) & (cId < 999)){
        MrPDatabase *database = [[MrPDatabase alloc] init];
        NSArray *MrPClassificationsArray = [database ClassificationDatabaseArray];
        NSMutableDictionary *dicClassifications = [[NSMutableDictionary alloc] initWithCapacity:[MrPClassificationsArray count]];
        // Fetch Data for that particular ciD if u want to speedup process.
        for (MrPContentInfo *info in MrPClassificationsArray) {
                [dicClassifications setObject:info.descWord forKey:[NSString stringWithFormat:@"classification%d",info.classificationId]];
            }
        labelClassifcation = [NSString stringWithFormat:@"%@",[dicClassifications objectForKey:[NSString stringWithFormat:@"classification%d",cId]]];
        [dicClassifications release];
        [database release];
        database = Nil;
        
    }
//    if (cId == 99999) {
//        int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
//            MrPDatabase *database = [[MrPDatabase alloc] init];
//            NSArray *MrPrinciplesArray = [database MySkillsPrinciplesArray];
//            NSMutableDictionary *dicPrinciples = [[NSMutableDictionary alloc] initWithCapacity:[MrPrinciplesArray count]];
//            // Fetch Data for that particular ciD if u want to speedup process.
//            for (MrPContentInfo *info in MrPrinciplesArray) {
//                [dicPrinciples setObject:info.principleWord forKey:[NSString stringWithFormat:@"principle%d",info.principleId]];
//            }
//        
//            labelClassifcation = [NSString stringWithFormat:@"%@",[dicPrinciples objectForKey:[NSString stringWithFormat:@"principle%d",pId]]];
//            [dicPrinciples release];
//            database = Nil;
//            [database release];
//
//    }
    if (cId < 989 || cId > 999) {
        labelClassifcation = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:@"Word0"]];
    }
    
    NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    if([objectClassification isKindOfClass:[NSArray class]] == YES)
    {
        MrPDatabase *database = [[MrPDatabase alloc] init];
        NSArray* MrPContentArray  = [database loadImageForAvailableClassifications:objectClassification];
            
        for (MrPContentInfo *info in MrPContentArray) {
            NSLog(@"%@",info.englishWord);
            labelClassifcation = info.englishWord;
            break;
        }
    }
    _lblClassification.text = labelClassifcation;
    [_lblClassification setFont:[UIFont boldSystemFontOfSize:26]];
    _lblClassification.textColor = [UIColor blackColor];
    _lblClassification.textAlignment = UITextAlignmentCenter;
    _lblClassification.lineBreakMode = UILineBreakModeWordWrap;
    _lblClassification.numberOfLines = 0;
    
    answersCounter = 0;
    
    NSArray* arrRandomPosOfSecItems = [[NSArray alloc] initWithArray:[self generateRandomPositioningOfItemsOfSecondClassification]];
    NSArray* arrRandomItemsOfSecClassification = [[NSArray alloc] initWithArray:[self generateRandomSelectionsOfSecondClassification]];
    NSLog(@"Random Generated Array is %@",arrRandomPosOfSecItems);
    
    int xCount = 0;
    int yCount = 0;
    int secondItemPos1 = [[arrRandomPosOfSecItems objectAtIndex:0] intValue];
    int secondItemPos2 = [[arrRandomPosOfSecItems objectAtIndex:1] intValue];
    int secondItemPos3 = [[arrRandomPosOfSecItems objectAtIndex:2] intValue];
    int secondItemPos4 = [[arrRandomPosOfSecItems objectAtIndex:3] intValue];
    int secondItemPos5 = [[arrRandomPosOfSecItems objectAtIndex:4] intValue];

    
    arrRecordCorrectTagsOfItems = [[NSMutableArray alloc] init];
    arrRecordINCorrectTagsOfItems = [[NSMutableArray alloc] init];
    int secondItemInsertingCounter = 0;
    
    for (int i = 1; i < 7; i++) {
        UILabel * _item = [[UILabel alloc] initWithFrame:CGRectMake(100+xCount, 117+yCount, 170, 170)];
        
        //        [_item setBackgroundColor:[UIColor greenColor]];
        _item.userInteractionEnabled = YES;
        [_item setFont:[UIFont boldSystemFontOfSize:25]];
        _item.textColor = [UIColor blackColor];
        _item.textAlignment = UITextAlignmentCenter;
        _item.lineBreakMode = UILineBreakModeWordWrap;
        _item.numberOfLines = 0;
        
        NSString *imgItem;
        if (i == secondItemPos1 || i == secondItemPos2 || i == secondItemPos3 || i == secondItemPos4 || i == secondItemPos5 ) {

            if (secondItemInsertingCounter >= [tempIncontentDicCorrect count]) {
                secondItemInsertingCounter = 0;
            }
                _item.text = [NSString stringWithFormat:@"%@",[tempIncontentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",[[arrRandomItemsOfSecClassification objectAtIndex:secondItemInsertingCounter ] intValue]]]];

            
            imgItem = [NSString stringWithFormat:@"%@",[tempIncontentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",[[arrRandomItemsOfSecClassification objectAtIndex:secondItemInsertingCounter ] intValue]]]];
            secondItemInsertingCounter = secondItemInsertingCounter + 1;
            [arrRecordINCorrectTagsOfItems addObject:imgItem];
            
        }
        else{
            imgItem = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",wordsCount]]];
               _item.text = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount]]];
            [arrRecordCorrectTagsOfItems addObject:imgItem];
            NSString *soundId = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"voiceId%d",wordsCount]];
            [self playSound:soundId];
            if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"synonym%d",wordsCount]] isEqualToString:@"0"]) {
                labelSynonym.text = @"";
            }
            else{
                labelSynonym.text = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"synonym%d",wordsCount]];
            }
            wordsCount = wordsCount + 1;
            [[NSUserDefaults standardUserDefaults] setInteger:wordsCount forKey:wordsCounterString];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
        }
        
        //        NSLog(@"ITEM image is %@",imgItem);
//        NSLog(@"Tag of correct items is %@",arrRecordCorrectTagsOfItems);
//                NSLog(@"Tag of NOT corrects items is %@",arrRecordINCorrectTagsOfItems);
        UIImage *loadingImgItemFromDocDir = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@", imgItem]];
        _item.backgroundColor = [UIColor clearColor];
        
        UIImageView * imageViewOfItem = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 170, 170)];
        [imageViewOfItem setImage:loadingImgItemFromDocDir];

        imageViewOfItem = nil;
        [imageViewOfItem release];
        _item.tag = [imgItem intValue];
        imgItem = nil;
        // UITapGestureRecognizer for getting tapped and UIPangestureRecognizer for dragging image.
        UIPanGestureRecognizer *panGesture = [[[UIPanGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(itemDragged:)] autorelease];
        [_item addGestureRecognizer:panGesture];
        [self.view addSubview:_item];
        
        UITapGestureRecognizer *tapGesture = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemTapped:)] autorelease];
        [_item addGestureRecognizer:tapGesture];
        _item = nil;
        [_item release];
        if (i == 3) {
            xCount = 290;
        }
        
        yCount = yCount + 190;
        if (i == 3) {
            yCount = 0;
        }
    }
    draggingBegin = TRUE;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    if([objectClassification isKindOfClass:[NSArray class]] == YES)
    {
        cId = [[objectClassification objectAtIndex:0] intValue];
    }

//    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];

    //    [self.view addSubview:_bannerView];

    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
            [self insitiantiateForReportCouting];
    [self playBackGroundMusic];
    firstLaunch = TRUE;
    correctCounter = 0;
    [self loadCorrectDatabase];
    [self loadIncorrectDatabase];
    NSString *soundId = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"voiceId%d",0]];
    [self playSound:soundId];
    //    [_BtnImages setEnabled:NO];
    _wordCounterlbl = [[UILabel alloc] initWithFrame:CGRectMake(600, 30, 300, 30)];
    NSString *currentNumber = [NSString stringWithFormat:@"Current Word : %d / %d",currentWord + 1, [tempContentDicCorrect count]/6];
    [_wordCounterlbl setText:currentNumber];
    //    [_wordCounterlbl setFont:[UIFont fontWithName:@"Arial" size:21]];
    UIFont* font = [UIFont systemFontOfSize: 21];
    [_wordCounterlbl setFont:font];
    _wordCounterlbl.textAlignment = UITextAlignmentCenter;
    [_wordCounterlbl setBackgroundColor:[UIColor clearColor]];
    //    [self.view addSubview:_wordCounterlbl];
    sessionCounter = 0;
    correctWordCounter = 0;
    
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Moveit,cId,activeLanguage];
    level = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    if (!level)
    {
        level = 1;
    }
    
    
    // Level Counter Label
    NSString *points1 = [NSString stringWithFormat:@"Game%dLevel%d",6,1];
    _lblLevel1 = [[UILabel alloc] initWithFrame:CGRectMake(600, 36, 300, 25)];
    NSString *currentLevel = [NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:points1]];
    
    
    [_lblLevel1 setText:currentLevel];
    //    [_lblLevel1 setFont:[UIFont fontWithName:@"Arial" size:21]];
    font = [UIFont systemFontOfSize: 21];
    [_lblLevel1 setFont:font];
    _lblLevel1.textAlignment = UITextAlignmentCenter;
    [_lblLevel1 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel1];
    
    NSString *points2 = [NSString stringWithFormat:@"Game%dLevel%d",6,2];
    _lblLevel2 = [[UILabel alloc] initWithFrame:CGRectMake(600, 62, 300, 25)];
    currentLevel = [NSString stringWithFormat:@"Level 02 : %d", [[NSUserDefaults standardUserDefaults] integerForKey:points2]];
    [_lblLevel2 setText:currentLevel];
    font = [UIFont systemFontOfSize: 21];
    [_lblLevel2 setFont:font];
    _lblLevel2.textAlignment = UITextAlignmentCenter;
    [_lblLevel2 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel2];

    [self startGame];
}

- (void)itemDragged:(UIPanGestureRecognizer *)gesture
{
    UILabel *itemNewlbl = (UILabel *)gesture.view;
    //    CGRect itemOriginalframe;
    int draggedItemTag = gesture.view.tag;
     NSLog(@"draggedImaged Tag is %d",draggedItemTag);
    if (draggingBegin) {
        itemOriginalframe = gesture.view.frame;
        //        NSLog(@"My view at the time of touch is frame: %@", NSStringFromCGRect(itemOriginalframe));
        draggingBegin = FALSE;
    }
    CGPoint translation = [gesture translationInView:itemNewlbl];
    
    // move label
    itemNewlbl.center = CGPointMake(itemNewlbl.center.x + translation.x,
                                    itemNewlbl.center.y + translation.y);
    [itemNewlbl setFont:[UIFont boldSystemFontOfSize:25]];
    //    NSLog(@"%f", [gesture locationInView:itemNewlbl.superview].x);
    //    NSLog(@"%f", [gesture locationInView:itemNewlbl.superview].y);
    [gesture setTranslation:CGPointZero inView:itemNewlbl];
    
    if(gesture.state == UIGestureRecognizerStateEnded)
    {
        int xLowerLimit = 726; // 113
        int yLowerLimit = 215; // 269
        int xUpperLimit = 947; // 654
        int yUpperLimit = 399; //345
        if ([gesture locationInView:itemNewlbl.superview].x > xLowerLimit && [gesture locationInView:itemNewlbl.superview].x < xUpperLimit && [gesture locationInView:itemNewlbl.superview].y > yLowerLimit && [gesture locationInView:itemNewlbl.superview].y < yUpperLimit) {
            [itemNewlbl removeFromSuperview];
            draggingBegin = TRUE;
            answersCounter = answersCounter + 1;
            BOOL correctAnswer = FALSE;
            if (answersCounter == 1) {
                for (int p = 0; p < 1; p ++) {
                    if ([[arrRecordCorrectTagsOfItems objectAtIndex:p] intValue] == draggedItemTag) {
                        correctAnswer = TRUE;
                    }
                }
                if (correctAnswer) {
                    [_imgCheck1 setImage:[UIImage imageNamed:@"correct.png"]];
                    correctCounter = correctCounter + 1;
                    [self correctItemSaveForReportLocally:draggedItemTag];
                }
                else{
                    [_imgCheck1 setImage:[UIImage imageNamed:@"X.png"]];
                    [self wrongItemSaveForReportLocally:draggedItemTag];
                }
                
                answersCounter = 0;
                [self moveToNextItems];
            }
            
            
        }
        
        else{ // flow goes in this check if u drop item anywhere on the screen but not in the nest
            //            NSLog(@"My view at the time of touch END is is frame: %@", NSStringFromCGRect(itemOriginalframe));
            [itemNewlbl removeFromSuperview];
            draggingBegin = TRUE;
            UILabel * _item = [[UILabel alloc] initWithFrame:itemOriginalframe];
            //            _item.text = @"TESSTCCSAE";
            _item.tag = draggedItemTag;
            _item.userInteractionEnabled = YES;
            [_item setFont:[UIFont boldSystemFontOfSize:25]];
            _item.textColor = [UIColor blackColor];
            _item.textAlignment = UITextAlignmentCenter;
            _item.backgroundColor = [UIColor clearColor];
            NSString *imgItem = [NSString stringWithFormat:@"%d",draggedItemTag];
            NSLog(@"ITEM image is %@",imgItem);
            UIImage *loadingImgItemFromDocDir = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@", imgItem]];
            //            _item.backgroundColor = [UIColor clearColor];
            imgItem = nil;
            
            UIImageView * imageViewOfItem = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 170, 170)];
            [imageViewOfItem setImage:loadingImgItemFromDocDir];
            
            NSString * itemLabel = [(UILabel*)[self.view viewWithTag:draggedItemTag] text];
        
            BOOL itemLocInDicFound = FALSE;
            for (int i = 0; i <   [tempContentDicCorrect count]/6; i++) {
                if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",i]] intValue] == draggedItemTag) {
                        itemLocInDicFound = TRUE;
                        itemLabel = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",i]];
                        break;
                    }
                }
            if (!itemLocInDicFound) {
                for (int i = 0; i <   [tempIncontentDicCorrect count]/6; i++) {
                    if ([[tempIncontentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",i]] intValue] == draggedItemTag) {
                        itemLabel = [tempIncontentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",i]];
                        itemLocInDicFound = TRUE;
                        break;
                    }
                }
            }
            _item.text = itemLabel;
            _item.lineBreakMode = UILineBreakModeWordWrap;
            _item.numberOfLines = 0;            
            imageViewOfItem = nil;
            [imageViewOfItem release];
            
            UIPanGestureRecognizer *panGesture = [[[UIPanGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(itemDragged:)] autorelease];
            [_item addGestureRecognizer:panGesture];
            [self.view addSubview:_item];
            
            UITapGestureRecognizer *tapGesture = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemTapped:)] autorelease];
            [_item addGestureRecognizer:tapGesture];
        }
        
    }
}


-(void) correctItemSaveForReportLocally:(int) draggedItemTag{
    
    if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",wordsCount-1]] intValue] == draggedItemTag ) {
        reportCorrectCount01 = reportCorrectCount01 + 1;
        reportTotatCount01 = reportTotatCount01 + 1;
    }
//    
//    if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",wordsCount-2]] intValue] == draggedItemTag ) {
//        reportCorrectCount02 = reportCorrectCount02 + 1;
//        reportTotatCount02 = reportTotatCount02 + 1;
//    }
//    
//    if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",wordsCount-1]] intValue] == draggedItemTag ) {
//        reportCorrectCount03 = reportCorrectCount03 + 1;
//        reportTotatCount03 = reportTotatCount03 + 1;
//    }
}

-(void) wrongItemSaveForReportLocally:(int) draggedItemTag{
    
    if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",wordsCount-1]] intValue] == draggedItemTag ) {
        reportTotatCount01 = reportTotatCount01 + 1;
    }
//    
//    if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",wordsCount-2]] intValue] == draggedItemTag ) {
//        reportTotatCount02 = reportTotatCount02 + 1;
//    }
//    
//    if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",wordsCount-1]] intValue] == draggedItemTag ) {
//        reportTotatCount03 = reportTotatCount03 + 1;
//    }
}

-(void) moveToNextItems{
    
    // Give Delay
    viewCompletedTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(endTimerAndLoadNextViewItems) userInfo:nil repeats:NO];
    [_btnNext setEnabled:NO];
    // playAnimation For three seconds
    if (correctCounter == 1) {
        [self playAnimation];
        [self playCheeringSound];
        [self answerPoints];
    }
    else{
        [self playEncouragingSound];
    }

    
}
-(void) stopTimerAndMakeButtonsClickable{
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        cheeringSoundTimer = nil;
    }
    [_btnNext setEnabled:YES];
    
}

-(void) playCheeringSound{
    
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:3.0
                                                               target:self
                                                             selector:@selector(stopTimerAndMakeButtonsClickable)
                                                             userInfo:nil
                                                              repeats:YES]retain];
        
        
        [_btnNext setEnabled:NO];
    }
    
    int randomNumer = arc4random() % 3 ;
    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
        
    }
    
}


-(void) playAnimation{
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:20];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1) // SIMPLE BOY
    {
        for(int count = 1; count <= 4; count++)
        {
            [images addObject:[UIImage imageNamed:@"Cheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"Cheering_7.png"]];
            [images addObject:[UIImage imageNamed:@"Cheering_8.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2) // SIMPLE GIRL
    {
        for(int count = 1; count <= 2; count++)
        {
            [images addObject:[UIImage imageNamed:@"GCheering_1.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_2.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_3.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_4.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_7.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_11.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_12.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3) // BASKET BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"BCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"BCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4) // BASKET GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"BCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_5.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5) // DOCTOR BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"DCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"DCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6) // DOCTOR GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"DCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_5.png"]];
        }
        
    }

    
    
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7) // Chef  BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"CCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"CCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8) // Chef GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"CCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9) // FIRE BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FireCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"FireCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10) // FIRE GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11) // FOOTBALL BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"FCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12) // Football GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13) // Arab BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"ACheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"ACheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14) // Arab GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"ACheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15) // POLICE BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"PCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"PCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16) // POLICE GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"PCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_5.png"]];
        }
        
    }
    
    _imgViewCheering.animationImages = images;
    _imgViewCheering.animationDuration = 1.5;
    _imgViewCheering.animationRepeatCount = 2; //Repeats indefinitely
    
    [_imgViewCheering startAnimating];
    [images removeAllObjects];
    [images release];
    [_imgViewCheering setImage:NULL];
}
-(void) endTimerAndLoadNextViewItems{
    [self updateReportCurrentWord];
        
    NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",6,1];
    [_lblLevel1 setText:[NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]]];
    
    NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",6,2];
    [_lblLevel2 setText:[NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]]];    
    
    [_btnNext setEnabled:YES];
    correctCounter = 0;
    if (viewCompletedTimer) {
        [viewCompletedTimer invalidate];
        viewCompletedTimer = Nil;
    }
    
    NSLog(@"%d",wordsCount);
//    if (wordsCount + 2 == [tempContentDicCorrect count]/6) {
//        wordsCount = wordsCount - 1;
//        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
//        NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],Moveit,cId,activeLanguage,ActiveGrade];
//        [[NSUserDefaults standardUserDefaults] setInteger:wordsCount forKey:wordsCounterString];
//        
//    }
//    else    
    if (wordsCount + 1 > [tempContentDicCorrect count]/6) {
        wordsCount = 0;
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//        int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Moveit,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount+1 forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self saveProgressForReport];
        [self insitiantiateForReportCouting];
    }
    
    [self startGame];
    
}
//-(void) ClassificationTapped:(UITapGestureRecognizer *)sender{
//    [self playSound:[tempContentDicCorrect objectForKey:@"voiceId0"]];
//
//
//}

-(void)itemTapped:(UITapGestureRecognizer *)sender{
//    int tagOfTappedItem = sender.view.tag;
//    NSLog(@"tag of tapped item is %d to play sound",tagOfTappedItem);
//    NSString *soundId;
//    
//    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId" ] == 99999){
//        
//        return;
//    }
//
//
//    BOOL itemLocInDicFound = FALSE;
//    for (int i = 0; i <   [tempContentDicCorrect count]/6; i++) {
//        if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",i]] intValue] == tagOfTappedItem) {
//            itemLocInDicFound = TRUE;
//            soundId = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"voiceId%d",i]];
//            break;
//        }
//    }
//    if (!itemLocInDicFound) {
//        for (int i = 0; i <   [tempIncontentDicCorrect count]/6; i++) {
//            if ([[tempIncontentDicCorrect objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",i]] intValue] == tagOfTappedItem) {
//                soundId = [tempIncontentDicCorrect objectForKey:[NSString stringWithFormat:@"voiceId%d",i]];
//                itemLocInDicFound = TRUE;
//                if(([[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"] > 989) && ([[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"] < 999) )
//                    {
////                        cityMapClickedWithActiveFolder = TRUE;
////                        [[NSUserDefaults standardUserDefaults] setInteger:310 forKey:@"selectedClassificationId"];
////                        [[NSUserDefaults standardUserDefaults] synchronize];
//                    }
//                break;
//            }
//        }
//    }
//
//    if (!itemLocInDicFound) {
//        soundId =@"EmptySound";
//    }
//    [self playSound:soundId];
}

-(void) playSound:(NSString*) soundName{
    if (firstLaunch) {
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"] > 989 && [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"] < 999) {
               firstLaunch = FALSE;
            return;
        }
     
    }


   
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
    if (cityMapClickedWithActiveFolder) {
        cityMapClickedWithActiveFolder = FALSE;
//        [[NSUserDefaults standardUserDefaults] setInteger:cId forKey:@"selectedClassificationId"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    
}

-(void) loadCorrectDatabase{
    int position = 0;

    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    int gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
    int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
    
    NSArray *MrPContentEnglishArray;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 2 || cId > 989) {
        MrPContentEnglishArray = [database EnglishWordDatabaseArray:cId];
    }
    else {
        MrPContentEnglishArray = [database ArabicWordDatabaseArray:cId];
    }
    
    tempContentDicCorrect = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentEnglishArray count]];
    
    for (MrPContentInfo *info in MrPContentEnglishArray) {
        
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",position]];
        [tempContentDicCorrect setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
        [tempContentDicCorrect setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
        [tempContentDicCorrect setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
        [tempContentDicCorrect setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];
        position++;
    }
    NSLog(@"Dic is %@",tempContentDicCorrect);
    
    if ([tempContentDicCorrect count]/6 < 1) {
        UIAlertView *alerts = [[UIAlertView alloc] init];
        [alerts setTitle:@"In Sufficient Data"];
        [alerts setMessage:@"Please go to Main Menu and select Anyother location"];
        [alerts setDelegate:self];
        [alerts addButtonWithTitle:@"OK"];
        [alerts setTag:12];
        [alerts show];
        [alerts release];
    }
    return;

}



-(int ) generateSecondClassification:(int) currentlyActiveClassifcation{
    
    //    int secondClassification = 30;
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    if([objectClassification isKindOfClass:[NSArray class]] == YES)
    {
        cId = [[objectClassification objectAtIndex:0] intValue];
    }
    
    NSString *selectedTable;
    if (activeLanguage == 2) {
        selectedTable = @"english_word";
    }
    else{
        selectedTable = @"arabic_word";
    }
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    NSMutableArray *arrAvailableClassifications = [[NSMutableArray alloc] initWithArray:[database fetchAllAvailableClassificationsForSelectedParamters:cId andActiveGrade:ActiveGrade andActiveLanguage:selectedTable]];
    
    int position = 0;
    NSMutableDictionary* dicSecondClassifications = [[NSMutableDictionary alloc] init];
    for (MrPContentInfo *info in arrAvailableClassifications) {
        [dicSecondClassifications setObject:[NSNumber numberWithInt:info.classificationId] forKey:[NSString stringWithFormat:@"classificationId%d",position]];
        position++;
    }
    int secondClassification = 310;
    int arrAvailableClassificationsLength = position-1;
    int randomGenerator = rand() % arrAvailableClassificationsLength;
    id secondClass = [dicSecondClassifications objectForKey:[NSString stringWithFormat:@"classificationId%d",randomGenerator]];
    secondClassification = [secondClass intValue];
    NSLog(@"Second Classification is %d ",secondClassification);
    if (!secondClassification ||
        (secondClassification == 10 && [ActiveGrade isEqualToString:@"G1"] && activeLanguage == 2) ||
        (secondClassification == 60 && activeLanguage == 2) ||
        (secondClassification == 230) ||
        (secondClassification > 989 && secondClassification < 999)
        ) {
        
        if (cId > 989 && cId < 999) {
            MrPDatabase *database = [[MrPDatabase alloc] init];
            NSArray *foldersInfoArray = [database ReadFolderDatabase];
            
            database = Nil;
            NSMutableDictionary *foldersContentDic = [[NSMutableDictionary alloc] initWithCapacity:9];
            //            NSMutableDictionary *wordsContentDic = [[NSMutableDictionary alloc] initWithCapacity:50];
            for (MrPContentInfo *info in foldersInfoArray) {
                //        NSLog(@"Folder id is %d: and Folder Name is  %@", info.folderid, info.foldername);
                [foldersContentDic setObject:info.foldername forKey:[NSString stringWithFormat:@"%d",info.folderid]];
            }
            int checkForFolderId = cId + 1;
            NSString *secondText = [NSString stringWithFormat:@"%d",checkForFolderId];
            if ([secondText isEqualToString:@""]) {
                int checkForFolderId = cId -1;
                NSString *secondText = [NSString stringWithFormat:@"%d",checkForFolderId];
                if ([secondText isEqualToString:@""]) {
                    return 310;
                }
                else return checkForFolderId;
                
            }
            else return checkForFolderId;
        }
        else
        {
            if (cId == 310) {
                secondClassification = 30;
            }
            else{
                secondClassification = 310;
            }
        }
    }
    return secondClassification;
    
}

-(void) loadIncorrectDatabase{
    int position = 0;
    // Loaading data  from database and saving in dictationaries
    
    // Our backend is designed in a way that it fetches active classification from userdefaults instead of manually sending it. In order to load dummy classification, we will change classification in userdefaults with dummy classification, load data, and then revert back to original classification in userdefaults. We can send classiication from the class but dont want to change existing flow.
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    //    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    //    NSArray *MrPContentEnglishArray;
    //    MrPDatabase *database = [[MrPDatabase alloc] init];
    //    if (activeLanguage == 2 || cId > 989) {
    //        MrPContentEnglishArray = [database EnglishWordDatabaseArray:cId];
    //    }
    //    else {
    //        MrPContentEnglishArray = [database ArabicWordDatabaseArray:cId];
    //    }
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    NSArray *MrPContentArray;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    
    int originalClassification;
    int dummyClassification;
    int pId = 0;
    //    int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
    if (cId > 989) {
    }
    else{
        NSArray *objectPrinciple = [[NSUserDefaults standardUserDefaults] objectForKey:PRINCIPLE_ID_ARRAY];
        if([objectPrinciple isKindOfClass:[NSString class]] == YES || [objectPrinciple count] == 1)
        {
            pId = [[objectPrinciple objectAtIndex:0] intValue];
        }
        
    }
    
    if ((cId > 989 && cId < 999) || pId != 3000) { // 3010
        if (pId != 3000 && !(cId > 989 && cId < 999)) {
            dummyClassification = cId;
        }
        else{
            originalClassification = cId;
            dummyClassification = [self generateSecondClassification:originalClassification];
            
        }
        if ((dummyClassification > 989 && dummyClassification < 999)) {
            MrPContentArray = [database databaseForParticularClassification:dummyClassification andTable:@"english_word"];
        }
        else
        {
            if(cId > 989)
            {
                MrPContentArray = [database fetchAllAvailableClassificationsForStores]; // 3010
            }
            else{
                MrPContentArray = [database fetchAllAvailableClassificationsWithGrammarIdsIncluded:NO]; // 3010
            }
            
        }
    }
    
    else{
        MrPContentArray = [database fetchAllAvailableClassificationsWithGrammarIdsIncluded:YES]; // 3000
        if ([MrPContentArray count] < 6) {
            MrPContentArray = [database fetchAllAvailableClassificationsWithGrammarIdsIncluded:NO]; // 3000
        }
    }
    
    
    
    if ([MrPContentArray count] < 3) {
        [[NSUserDefaults standardUserDefaults] setInteger:30 forKey:@"selectedClassificationId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self loadIncorrectDatabase];
        return;
    }
    
    tempIncontentDicCorrect = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    for (MrPContentInfo *info in MrPContentArray) {
        [tempIncontentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [tempIncontentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",position]];
        [tempIncontentDicCorrect setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]]
        ;
        if (activeLanguage == 2 || (dummyClassification > 989 && dummyClassification < 999))
        {
            [tempIncontentDicCorrect setObject:info.englishWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        else
        {
            [tempIncontentDicCorrect setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        [tempIncontentDicCorrect setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
        [tempIncontentDicCorrect setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
        position++;
    }
    
    // saving original classification back in userdefaults
    // saving original classification back in userdefaults
    [[NSUserDefaults standardUserDefaults] setInteger:originalClassification forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    position = 0;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    [_imgCheck1 release];
    [_imgCheck2 release];
    [_imgCheck3 release];
    [_btnNext release];
    //    [_BtnImages release];
    //    [_btnWords release];
    [_imgViewCheering release];
    [_lblClassification release];
        [labelSynonym release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setImgCheck1:nil];
    [self setImgCheck2:nil];
    [self setImgCheck3:nil];
    [self setBtnNext:nil];
    //    [self setBtnImages:nil];
    //    [self setBtnWords:nil];
    [self setImgViewCheering:nil];
    [self setLblClassification:nil];
    [super viewDidUnload];
}


-(void) answerPoints{
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Moveit,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        if (!LevelLastCount) {
            LevelLastCount = 1;
        }
        
        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",6,LevelLastCount];
        float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
        if (!PointsLastCount) {
            PointsLastCount = 0;
        }
        
        if (LevelLastCount < 3) {
            PointsLastCount = PointsLastCount + 6;
            [[NSUserDefaults standardUserDefaults] setInteger:PointsLastCount forKey:Points];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
//        [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount forKey:levelCount];
//        [[NSUserDefaults standardUserDefaults] synchronize];
    
//        correctWordCounter = correctWordCounter + 1;
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 10) {
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
            correctWordCounter = 0;
        }
        if (buttonIndex == 1)
        {
            
            if (player) {
                [player stop];
                [player release];
                player = Nil;
            }
            
            if (myMoveItPlayer) {
                [myMoveItPlayer stop];
                [myMoveItPlayer release];
                myMoveItPlayer = Nil;
            }
            
            [self dismissModalViewControllerAnimated:NO];
                [self saveProgressForReport];
        }
    }
    
    
    
    if (alertView.tag == 12) {
        
        if (player) {
            [player stop];
            [player release];
            player = Nil;
        }
        
        if (myMoveItPlayer) {
            [myMoveItPlayer stop];
            [myMoveItPlayer release];
            myMoveItPlayer = Nil;
        }
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        MyPlayGroundViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyPlaygroundIdentifier"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:NO completion:NULL];

            [self saveProgressForReport];
    }
}

-(void) sessionCompletionCheck{
    
    
    UIAlertView *sessionAlert;
                int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (correctWordCounter > 7) {

        // Awarding Bonus
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Moveit,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        
        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",6,LevelLastCount];
        float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
        if (!PointsLastCount) {
            PointsLastCount = 0;
        }
        
        PointsLastCount = PointsLastCount + 2;
        [[NSUserDefaults standardUserDefaults] setInteger:PointsLastCount forKey:Points];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (activeLanguage == 2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good! You have earned Bonus of +2" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
            
            
            
            
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
        }
        
        [sessionAlert setTag:10];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"RewardUnlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        
        if (activeLanguage ==2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your progress needs improvement. Try again." delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عفوا !" message:@"تقدمك غير كافي , يجب أن تحاول أكثر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب" otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:1];
    }
    
    [sessionAlert show];
    [sessionAlert release];
    
}


-(void) upDateClassificationStatus{
    
    //PickItIdentifier
}


-(void) playEncouragingSound{
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:3.0
                                                               target:self
                                                             selector:@selector(stopTimerAndMakeButtonsClickable)
                                                             userInfo:nil
                                                              repeats:YES]retain];
        
        
        
        [_btnNext setEnabled:NO];
    }
    
    int randomNumer = arc4random() % 3 ;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC25";
        NSString *second = @"EC30";
        NSString *third = @"EC31";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }else {
        NSString *first = @"AC04";
        NSString *second = @"AC05";
        NSString *third = @"AC06";
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }
}


- (IBAction)btnNextPressed:(id)sender {
//    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];    
    NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],Moveit,cId,activeLanguage,ActiveGrade];
    [[NSUserDefaults standardUserDefaults] setInteger:wordsCount+1 forKey:wordsCounterString];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self startGame];
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"5-MatchIT"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    myMoveItPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    myMoveItPlayer.volume = 0.15;
    myMoveItPlayer.numberOfLoops = 20;
    myMoveItPlayer.delegate = self;
    
    if ([myMoveItPlayer prepareToPlay]) {
        [myMoveItPlayer play];
    }
}


-(void) saveProgressForReport{
    // write in db!
    
    if (!retval || retval == NULL) {
        return;
    }
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database saveReportProgress:retval];
    
    if (retval) {
        [retval removeAllObjects];
        [retval release];
        retval = nil;
    }
}
-(void) updateReportCurrentWord{
    reportWordDesc01 = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount-1]];
    reportWordId01 = [[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordId%d",wordsCount-1]] intValue];
    
    NSLog(@"Report Word Desc is %@",reportWordDesc01);
    NSLog(@"Report Word Desc is %d",reportWordId01);
    
    if (reportTotatCount01 == 0) {
        reportTotatCount01 = reportTotatCount01 + 1;
    }
    
//    if (reportTotatCount02 == 0) {
//        reportTotatCount02 = reportTotatCount02 + 1;
//    }
//    
//    if (reportTotatCount03 == 0) {
//        reportTotatCount03 = reportTotatCount03 + 1;
//    }
//    
    int gameId = 5;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    MrPReport *info = [[MrPReport alloc]
                       initWithWordId:reportWordId01 andWordDesc:reportWordDesc01 andCorrectCount:reportCorrectCount01 andTotalCount:reportTotatCount01 andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
    
    [retval addObject:info];
    
//    reportTotatCount01 = 0;
//    reportCorrectCount01 = 0;
//    reportWordDesc01 = @"";
//    reportWordId01 = 0;
//    
//    reportWordDesc02 = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount-2]];
//    reportWordId02 = [[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordId%d",wordsCount-2]] intValue];
//    NSLog(@"Report Word Desc is %@",reportWordDesc02);
//    NSLog(@"Report Word Desc is %d",reportWordId02);
//    
//    
//    info = [[MrPReport alloc]
//            initWithWordId:reportWordId02 andWordDesc:reportWordDesc02 andCorrectCount:reportCorrectCount02 andTotalCount:reportTotatCount02 andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
//    
//    [retval addObject:info];
//    
//    reportTotatCount02 = 0;
//    reportCorrectCount02 = 0;
//    reportWordDesc02 = @"";
//    reportWordId02 = 0;
//    
//    
//    reportWordDesc03 = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount-1]];
//    reportWordId03 = [[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordId%d",wordsCount-1]] intValue];
//    NSLog(@"Report Word Desc is %@",reportWordDesc03);
//    NSLog(@"Report Word Desc is %d",reportWordId03);
//    
//    
//    info = [[MrPReport alloc]
//            initWithWordId:reportWordId03 andWordDesc:reportWordDesc03 andCorrectCount:reportCorrectCount03 andTotalCount:reportTotatCount03 andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
//    
//    [retval addObject:info];
//    
//    reportTotatCount03 = 0;
//    reportCorrectCount03 = 0;
//    reportWordDesc03 = @"";
//    reportWordId03 = 0;
}

-(void) insitiantiateForReportCouting{
    retval = [[NSMutableArray alloc] init];
    
    reportCorrectCount01 = 0;
    reportTotatCount01 = 0;
    reportWordDesc01 = @"";
    reportWordId01 = 0;
    
//    reportCorrectCount02 = 0;
//    reportTotatCount02 = 0;
//    reportWordDesc02 = @"";
//    reportWordId02 = 0;
//    
//    reportCorrectCount03 = 0;
//    reportTotatCount03 = 0;
//    reportWordDesc03 = @"";
//    reportWordId03 = 0;
    
    
}

- (IBAction)hearCorrectWord:(id)sender {
    int currentwordCount = wordsCount - 1;
    if (currentwordCount <0) {
        return;
    }
    NSString *soundId = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"voiceId%d",currentwordCount]];
    [self playSound:soundId];
    
}

- (IBAction)gotoMainMenu:(id)sender {
    

    if (myMoveItPlayer) {
        [myMoveItPlayer stop];
        [myMoveItPlayer release];
        myMoveItPlayer = nil;

    }
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
        [self saveProgressForReport];

}

- (IBAction)playClassificationSound:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"] > 989 && [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"] < 999) {
        return;
    }

        [self playSound:[tempContentDicCorrect objectForKey:@"voiceId0"]];
}
- (BOOL)bannerViewActionShouldBegin:
(ADBannerView *)banner
               willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
        [_bannerView removeFromSuperview];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Did Fail to receive ad");
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"received ad");
    [self.view addSubview:_bannerView];
    //    self.view = _bannerView;
}
@end
