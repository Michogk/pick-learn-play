//
//  HelpViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
