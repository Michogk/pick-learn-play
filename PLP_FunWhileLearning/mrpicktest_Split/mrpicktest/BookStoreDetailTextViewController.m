//
//  BookStoreDetailTextViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 4/13/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "BookStoreDetailTextViewController.h"

@interface BookStoreDetailTextViewController ()

@end

@implementation BookStoreDetailTextViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString* activePackage = [[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"];
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    int indexPath = [[NSUserDefaults standardUserDefaults] integerForKey:@"BookStoreButtonForDescription"];
    UILabel *lblGrade = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 450)];
    [lblGrade setBackgroundColor:[UIColor clearColor]];
    UIFont *font = [UIFont systemFontOfSize: 15];
    [lblGrade setTextColor:[UIColor blackColor]];
    [lblGrade setFont:font];
    [lblGrade setNumberOfLines:0];
    if (activeLanguage == 2) {
        lblGrade.textAlignment = UITextAlignmentLeft;
    }
    else{
        lblGrade.textAlignment = UITextAlignmentRight;
    }
    
    
    
    lblGrade.lineBreakMode = UILineBreakModeWordWrap;
    //    lblDescription.numberOfLines = 9;
    
    if ([activePackage isEqualToString:@"KG1"]){
        if (indexPath == 0) {
            if (activeLanguage == 1) {
                [lblGrade setText:@"أضف صورك لكلماتك وجملك\n حروف الأبجدية مع ارشادات لكتابتها, اصوات حروف الأبجدية, حروف الأبجدية مع كلمات. المزرعة وحديقة الحيوانات. خطوات النجاح للمبتدئين في القراءة والكتابة. مجموع اللغتين العربية والانجليزية: 196 كلمة وتحتوي على صور."];
            }
            else {
                [lblGrade setText:@"Alphabet list with directions on writing, Alphabet sounds, Alphabet with words. Farm and Zoo animals. Road to Success for beginners in reading and writing. Total: 196 words with image support between English and Arabic "];
            }
            
            [lblGrade sizeToFit];
        }
        
        
        if (indexPath == 1) {
            if (activeLanguage == 1) {
                [lblGrade setText:@"أضف صورك لكلماتك وجملك"];
            }
            else {
                [lblGrade setText:@"Add photo to your own words and sentences"];
            }
            
            [lblGrade sizeToFit];
        }
        
        if (indexPath == 2) {
            if (activeLanguage == 1) {
                [lblGrade setText:@"حروف الأبجدية مع ارشادات لكتابتها, اصوات حروف الأبجدية, حروف الأبجدية مع كلمات. المزرعة وحديقة الحيوانات. خطوات النجاح للمبتدئين في القراءة والكتابة. مجموع اللغتين العربية والانجليزية: 196 كلمة وتحتوي على صور.\n\nأضف صورك لكلماتك وجملك"];
            }
            else {
                [lblGrade setText:@"Alphabet list with directions on writing, Alphabet sounds, Alphabet with words. Farm and Zoo animals. Road to Success for beginners in reading and writing. Total: 196 words with image support between English and Arabic \n\nAdd photo to your own words and sentences"];
            }
            
            [lblGrade sizeToFit];
        }
        
    }
    
    else
        if ([activePackage isEqualToString:@"KG2"]){
            if (indexPath == 0) {
                if (activeLanguage == 1) {
                    [lblGrade setText:@"حروف الأبجدية مع ارشادات لكتابتها, اصوات حروف الأبجدية, حروف الأبجدية مع كلمات. المزرعة وحديقة الحيوانات. خطوات النجاح للمبتدئين في القراءة والكتابة. مجموع اللغتين العربية والانجليزية: 218 كلمة وتحتوي على صور."];
                        }
                else {
                    [lblGrade setText:@"Alphabet list with directions on writing, Alphabet sounds, Alphabet with words. Farm and Zoo animals. Road to Success for beginners in reading and writing. Total: 218 words with image support between English and Arabic "];
                }
                [lblGrade sizeToFit];
            }
            if (indexPath == 1) {
                if (activeLanguage == 1) {
                    [lblGrade setText:@"أضف صورك لكلماتك وجملك"];
                }
                else {
                    [lblGrade setText:@"Add photo to your own words and sentences"];
                }
                
                [lblGrade sizeToFit];
            }
            
            if (indexPath == 2) {
                if (activeLanguage == 1) {
             [lblGrade setText:@"حروف الأبجدية مع ارشادات لكتابتها, اصوات حروف الأبجدية, حروف الأبجدية مع كلمات. المزرعة وحديقة الحيوانات. خطوات النجاح للمبتدئين في القراءة والكتابة. مجموع اللغتين العربية والانجليزية: 218 كلمة وتحتوي على صور.\n\nأضف صورك لكلماتك وجملك"];
                }
                else {
               [lblGrade setText:@"Alphabet list with directions on writing, Alphabet sounds, Alphabet with words. Farm and Zoo animals. Road to Success for beginners in reading and writing. Total: 218 words with image support between English and Arabic \n\nAdd photo to your own words and sentences "];
                }
                
                [lblGrade sizeToFit];
            }
        }
        else
            if ([activePackage isEqualToString:@"G1"]){
                if (indexPath == 0) {
                
                    if (activeLanguage == 1) {
                        [lblGrade setText:@"29 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: 1080  كلمة وتحتوي على صور.\nايضاً, حروف الأبجدية و خطوات النجاح لتقوية القراءة والكتابة. اكثر من 120  كلمة باللغتين العربية والانجليزية وتحتوي على صور."];
                                            }
                    else {
                        [lblGrade setText:@"29 locations in the city map. Total: 1080 words with image support between English and Arabic.\n\nIn addition to the most important sections for reading and writing: Alphabet and Road to Success (more than 120 words) with image support between English and Arabic."];
                        //                        [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                    }
                    
                    [lblGrade sizeToFit];
                    
             
                }
                
                if (indexPath == 1) {
                    if (activeLanguage == 1) {
                        [lblGrade setText:@"أضف صورك لكلماتك وجملك"];
                    }
                    else {
                        [lblGrade setText:@"Add photo to your own words and sentences"];
                    }
                    
                    [lblGrade sizeToFit];
                }
                
                if (indexPath == 2) {
                    if (activeLanguage == 1) {
                        [lblGrade setText:@"عربي: 23 من القواعد وتحتوي على اكثر من 50 مبدأ مع 1830 كلمة وجملة\n\n.الانجليزي: 20  من القواعد وتحتوي تقريباً على 70  مبدأ مع 740  كلمة وجملة\n \n إبرع في القراءة الانجليزية (370  كلمة)."];
                    }
                    else {
                        [lblGrade setText:@"Phonics: listen and lean more than 250 words. Arabic: 23 grammar rules include more than 50 principles with 1830 examples containing sentences.\n\nEnglish: 20 grammar rules include almost 70 principles with 740 examples containing sentences\n\nEnglish outshine in reading (more than 370 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                        //                        [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                    }                                
                    [lblGrade sizeToFit];
                }
                
                if (indexPath == 3) {
                    if (activeLanguage == 1) {
                     [lblGrade setText:@"29 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: 1080  كلمة وتحتوي على صور.\nايضاً, حروف الأبجدية و خطوات النجاح لتقوية القراءة والكتابة. اكثر من 120  كلمة باللغتين العربية والانجليزية وتحتوي على صور.\n\nعربي: 23 من القواعد وتحتوي على اكثر من 50 مبدأ مع 1830 كلمة وجملة\n\n.الانجليزي: 20  من القواعد وتحتوي تقريباً على 70  مبدأ مع 740  كلمة وجملة\n إبرع في القراءة الانجليزية (370  كلمة).\n\nأضف صورك لكلماتك وجملك"];
                    }
                    else {
                         [lblGrade setText:@"29 locations in the city map. Total: 1080 words with image support between English and Arabic.\n\nIn addition to the most important sections for reading and writing: Alphabet and Road to Success (more than 120 words) with image support between English and Arabic.\n\nAdd photo to your own words and sentences\n\nPhonics: listen and lean more than 250 words\nArabic: 23 grammar rules include more than 50 principles with 1830 examples containing sentences.\n\nEnglish: 20 grammar rules include almost 70 principles with 740 examples containing sentences\nEnglish outshine in reading (more than 370 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                    }
                    
                    [lblGrade sizeToFit];
                }
                
            }
    
            else
                if ([activePackage isEqualToString:@"G2"]){
                    if (indexPath == 0) {
                        if (activeLanguage == 1) {
                            [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: ما يقارب1200  كلمة وتحتوي على صور.\n \n خطوات النجاح لتقوية القراءة والكتابة. اكثر من 190  كلمة باللغتين العربية والانجليزية وتحتوي على صور."];
                        }
                        else {
                            [lblGrade setText:@"30 locations in the city map. Total: almost 1200 words and sentences with image support between English and Arabic.\n\nIn addition to Road to Success (more than 190 words) for reading and writing, with image support between English and Arabic "];
                            //                                [packageLevel setText:@"Basic Words"];
                            //                                [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                        }
                        [lblGrade sizeToFit];
            
                    }
                    if (indexPath == 1) {
                        if (activeLanguage == 1) {
                            [lblGrade setText:@"أضف صورك لكلماتك وجملك"];
                        }
                        else {
                            [lblGrade setText:@"Add photo to your own words and sentences"];
                        }
                        
                        [lblGrade sizeToFit];
                    }
                    if (indexPath == 2) {
                        if (activeLanguage == 1) {
                            [lblGrade setText:@"عربي: 22  من القواعد وتحتوي على اكثر من 70  مبدأ مع 1300  كلمة وجملة.\n \n.الانجليزي: 24  من القواعد.\n \n وتحتوي تقريباً على 75  مبدأ مع 765  كلمة وجملة..\n \n إبرع في القراءة الانجليزية (اكثر من 500 كلمة)."];

                        }
                        else {
                            [lblGrade setText:@"Phonics: listen and learn more than 165words. Arabic: 22 grammar rules include more than 70 principles with 1300 examples containing sentences\n\nEnglish: 24 grammar rules include almost 75 principles with 765 examples containing sentences\n\nEnglish outshine in reading (more than 500 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                            //                            [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                        }
                        
                        [lblGrade sizeToFit];

                    }
                    if (indexPath == 3) {
                        if (activeLanguage == 1) {
                             [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: ما يقارب1200  كلمة وتحتوي على صور.\n \n خطوات النجاح لتقوية القراءة والكتابة. اكثر من 190  كلمة باللغتين العربية والانجليزية وتحتوي على صور.\n\nأضف صورك لكلماتك وجملك\n\nعربي: 22  من القواعد وتحتوي على اكثر من 70  مبدأ مع 1300  كلمة وجملة.\n \n.الانجليزي: 24  من القواعد.\n وتحتوي تقريباً على 75  مبدأ مع 765  كلمة وجملة..\n إبرع في القراءة الانجليزية (اكثر من 500 كلمة)."];
                        }
                        else {
                            [lblGrade setText:@"30 locations in the city map. Total: almost 1200 words and sentences with image support between English and Arabic.\n\nIn addition to Road to Success (more than 190 words) for reading and writing, with image support between English and Arabic\n\nAdd photo to your own words and sentences\nPhonics: listen and learn more than 165words\nArabic: 22 grammar rules include more than 70 principles with 1300 examples containing sentences\nEnglish: 24 grammar rules include almost 75 principles with 765 examples containing sentences\nEnglish outshine in reading (more than 500 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                        }
                        
                        [lblGrade sizeToFit];
                    }
                }
                else
                    if ([activePackage isEqualToString:@"G3"]){
                        if (indexPath == 0) {
                            if (activeLanguage == 1) {
                                [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: اكثر من 950  كلمة وتحتوي على صور. بعض الجُمَل تتضمن حقائق علمية."];
                            }
                            else {
                                [lblGrade setText:@" 30 locations in the city map. Total: more than 950 words and sentences with image support between English and Arabic.  Some sentences state scientific facts."];
                                
                                //                                    [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                            }
                            
                            //                                [packageLevel setText:@"Basic Words"];
                            [lblGrade sizeToFit];
                   
                        }
                        if (indexPath == 1) {
                            if (activeLanguage == 1) {
                                [lblGrade setText:@"أضف صورك لكلماتك وجملك"];
                            }
                            else {
                                [lblGrade setText:@"Add photo to your own words and sentences"];
                            }
                            
                            [lblGrade sizeToFit];
                        }
                        
                        if (indexPath == 2) {
                            if (activeLanguage == 1) {
                                [lblGrade setText:@"عربي: 26  من القواعد وتحتوي على اكثر من 92  مبدأ مع 1920  كلمة وجملة. \n  \n الانجليزي: 32  من القواعد وتحتوي تقريباً على 110  مبدأ مع 1250  كلمة وجملة. \n  \n إبرع في القراءة الانجليزية (اكثر من 530  كلمة)."];
                            }
                            else {
                                [lblGrade setText:@"Phonics: listen and learn more than 200 words. Arabic: 26 grammar rules include more than 92 principles with more than 1920 examples containing sentences\n\nEnglish: 32 grammar rules include almost 110 principles with more than 1250 examples containing sentences\n\nEnglish outshine in reading (more than 530 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                                //                                [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                                
                            }
                        
                            [lblGrade sizeToFit];

                        }
                        if (indexPath == 3) {
                            if (activeLanguage == 1) {
                         [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: اكثر من 950  كلمة وتحتوي على صور. بعض الجُمَل تتضمن حقائق علمية.\n\nأضف صورك لكلماتك وجملك\n\nعربي: 26  من القواعد وتحتوي على اكثر من 92  مبدأ مع 1920  كلمة وجملة. \n\n الانجليزي: 32  من القواعد وتحتوي تقريباً على 110  مبدأ مع 1250  كلمة وجملة. \n  \n إبرع في القراءة الانجليزية (اكثر من 530  كلمة)."];
                            }
                            else {
                                 [lblGrade setText:@" 30 locations in the city map. Total: more than 950 words and sentences with image support between English and Arabic.  Some sentences state scientific facts.\n\nAdd photo to your own words and sentences\nPhonics: listen and learn more than 200 words\nArabic: 26 grammar rules include more than 92 principles with more than 1920 examples containing sentences\nEnglish: 32 grammar rules include almost 110 principles with more than 1250 examples containing sentences\nEnglish outshine in reading (more than 530 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                            }
                            
                            [lblGrade sizeToFit];
                        }
          
                    }
                    else
                        
                        if ([activePackage isEqualToString:@"G4"]){
                            if (indexPath == 0) {
                                if (activeLanguage == 1) {
                                    [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: اكثر من 475 كلمة وتحتوي على صور. بعض الجُمَل تتضمن حقائق علمية.."];
            
                                }
                                else {
                                    [lblGrade setText:@"30 locations in the city map. Total: more than 475 words and sentences with image support between English and Arabic. Some sentences state scientific facts."];

                                }
                                
                                [lblGrade sizeToFit];

                            }
                            if (indexPath == 1) {
                                if (activeLanguage == 1) {
                                    [lblGrade setText:@"أضف صورك لكلماتك وجملك"];
                                }
                                else {
                                    [lblGrade setText:@"Add photo to your own words and sentences"];
                                }
                                
                                [lblGrade sizeToFit];
                            }
                            
                            if (indexPath == 2) {
                                if (activeLanguage == 1) {
                                    [lblGrade setText:@"عربي: 32  من القواعد وتحتوي على اكثر من 122  مبدأ مع 2120  كلمة وجملة.\n  \n الانجليزي: 25  من القواعد \n وتحتوي على اكثر من 60  مبدأ مع 950  كلمة وجملة."];
                                }
                                else {
                                    [lblGrade setText:@"Phonics: listen and learn more than 240 words. Arabic: 32 grammar rules include more than 122 principles with almost 2120 examples containing sentences\n\nEnglish: 25 grammar rules include more than 60 principles with more than 950 examples containing sentences"];
                                    //                                        [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                                } 
                                
                                [lblGrade sizeToFit];
                            }
                            if (indexPath == 3) {
                                if (activeLanguage == 1) {
                                    [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: اكثر من 475 كلمة وتحتوي على صور. بعض الجُمَل تتضمن حقائق علمية..\n\nأضف صورك لكلماتك وجملك\n\nعربي: 32  من القواعد وتحتوي على اكثر من 122  مبدأ مع 2120  كلمة وجملة.\n  \n الانجليزي: 25  من القواعد \n وتحتوي على اكثر من 60  مبدأ مع 950  كلمة وجملة."];
                                }
                                else {
                                 [lblGrade setText:@"30 locations in the city map. Total: more than 475 words and sentences with image support between English and Arabic. Some sentences state scientific facts.\n\nAdd photo to your own words and sentences\nPhonics: listen and learn more than 240 words\nArabic: 32 grammar rules include more than 122 principles with almost 2120 examples containing sentences\n\nEnglish: 25 grammar rules include more than 60 principles with more than 950 examples containing sentences"];
                                }
                                
                                [lblGrade sizeToFit];
                            }
                            
                            
                        }
    
    
    
    
    
    
    
    
    
    
    



    [self.view addSubview:lblGrade];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
