//
//  CostumesViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 8/29/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CostumesViewController : UIViewController

- (IBAction)BtnChefBoyPressed:(id)sender;

- (IBAction)btnChefGirlPressed:(id)sender;

- (IBAction)btnFootballBoyPressed:(id)sender;
- (IBAction)btnFootballGirlPressed:(id)sender;
- (IBAction)btnFireManPressed:(id)sender;
- (IBAction)btnFireGirlPressed:(id)sender;
- (IBAction)btnLadyDoctorPressed:(id)sender;
- (IBAction)btnDoctorPressed:(id)sender;
- (IBAction)BtnPoliceManPressed:(id)sender;
- (IBAction)btnPoliceGirlPressed:(id)sender;
- (IBAction)btnBasketBallBoyPressed:(id)sender;
- (IBAction)btnBasketBallGirlPressed:(id)sender;
- (IBAction)btnArabBoyPressed:(id)sender;
- (IBAction)btnArabGirlPressed:(id)sender;

@property (retain, nonatomic) IBOutlet UIButton *btnChefBoy;
@property (retain, nonatomic) IBOutlet UIButton *btnChefGirl;
@property (retain, nonatomic) IBOutlet UIButton *btnFootBallGirl;
@property (retain, nonatomic) IBOutlet UIButton *btnFootballBoy;
@property (retain, nonatomic) IBOutlet UIButton *btnFireManBoy;
@property (retain, nonatomic) IBOutlet UIButton *btnFireGirl;
@property (retain, nonatomic) IBOutlet UIButton *btnDoctorBoy;
@property (retain, nonatomic) IBOutlet UIButton *btnLadyDoctor;
@property (retain, nonatomic) IBOutlet UIButton *btnPoliceMan;
@property (retain, nonatomic) IBOutlet UIButton *btnPoliceWoman;
@property (retain, nonatomic) IBOutlet UIButton *btnBasketBallBoy;
@property (retain, nonatomic) IBOutlet UIButton *btnBasketBallGirl;
@property (retain, nonatomic) IBOutlet UIButton *btnArabBoy;
@property (retain, nonatomic) IBOutlet UIButton *btnArabGirl;



@end
