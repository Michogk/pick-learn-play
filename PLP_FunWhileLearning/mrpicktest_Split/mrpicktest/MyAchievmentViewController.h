//
//  MyAchievmentViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#define kBorderInset            20.0
#define kBorderWidth            1.0
#define kMarginInset            10.0

//Line drawing
#define kLineWidth              1.0
@interface MyAchievmentViewController : UIViewController<MFMailComposeViewControllerDelegate,UIAlertViewDelegate>{

    IBOutlet UILabel *lblAverage;
    IBOutlet UILabel *lblTotalWords;
    IBOutlet UILabel *lblCorrectWords;
    NSString *Game1;
    NSString *Game2;
    NSString *Game3;
    NSString *Game4;
    BOOL clearPrevData;
      NSString *Game5;  NSString *Game6;  NSString *Game7;
          NSString *Game8;  NSString *Game9;  NSString *Game10;
    CGSize pageSize;
        BOOL showMovie;
    NSMutableDictionary *tempContentDic;
    
    NSMutableArray *arrAllWordIds;
    NSMutableArray *arrAllWordDesc;
    NSMutableArray *arrClassifications;
    NSMutableArray *arrAllWordGameId01;
    NSMutableArray *arrAllWordGameId02;
    NSMutableArray *arrAllWordGameId03;
    NSMutableArray *arrAllWordGameId04;
    NSMutableArray *arrAllWordGameId05;
    NSMutableArray *arrAllWordGameId06;
    NSMutableArray *arrAllWordGameId07;
        NSMutableArray *arrAllPin;
    
        NSMutableArray *arrAllWordGameId08;
    
        NSMutableArray *arrAllWordGameId09;
    
        NSMutableArray *arrAllWordGameId10;
        NSString *classificationName;
    
    UIImageView* characterWalkingAnimationImgView;
    int characterAnimationStep;
    int animationImageNumber;
    NSString *animationImageDirection;
    NSTimer *characterWalkingTimer;
    NSString* animationImageGender;
    int Position;

    

}
- (IBAction)HelpSound:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *BtnRewards;
- (IBAction)RewardBtnPressed:(id)sender;
- (IBAction)helpURL:(id)sender;
- (IBAction)generatePDFMail:(id)sender;

@end
