
//
//  main.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/13/12.
//  Copyright (c) 2012 School. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "mrpAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([mrpAppDelegate class]));
    }
}
