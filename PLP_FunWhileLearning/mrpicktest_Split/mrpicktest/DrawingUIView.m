//
//  DrawingUIView.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "DrawingUIView.h"
#import <QuartzCore/QuartzCore.h>
#import "DocumentDirectory.h"

@interface DrawingUIView ()

@end

@implementation DrawingUIView


- (id)initWithFrame:(CGRect)frame andWordsSoundArray:(NSMutableArray *) pArray andImagesSoundArray:(NSMutableArray*) pImagesArray
{
    self = [super initWithFrame:frame];
    self.userInteractionEnabled = TRUE;
    NSLog(@"INITIALIZING");
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self viewLoad];
        wordsSoundArray = [[NSMutableArray alloc] initWithCapacity:4];
        wordsSoundArray = pArray;
        
        imagesArray = [[NSMutableArray alloc] initWithCapacity:4];
        imagesArray = pImagesArray;
    }
    return self;
}

-(void) viewLoad{
    positionArray = [[NSMutableArray alloc] initWithCapacity:4];
    positionArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"positionArray"];
    userResults = [[NSMutableDictionary alloc] init];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {    
    touchStartingPoint = FALSE;
//    NSLog(@"dragBegan......");
    UITouch *touch = [[event allTouches] anyObject];
    touchPoint = [touch locationInView:self];
//    NSLog(@"Touch x : %f y : %f", touchPoint.x, touchPoint.y);
    touchMovedTrue = FALSE;
    
    
    // Ends from first point
    if (touchPoint.x > 660 && touchPoint.x< 758 && touchPoint.y > 61  && touchPoint.y< 159)
    {
        resultWord = 0;
    }
    
    // Ends from second point
    if (touchPoint.x > 660 && touchPoint.x< 758 && touchPoint.y > 187 && touchPoint.y< 285)
    {
        resultWord = 1;
    }
    
    // Ends from third point
    if  (touchPoint.x > 660 && touchPoint.x< 758 && touchPoint.y > 310&& touchPoint.y< 408)
    {
        resultWord = 2;
        
    }
    
    // Ends from fourth point
    if (touchPoint.x > 660 && touchPoint.x< 758 && touchPoint.y > 438&& touchPoint.y< 536)
    {
        resultWord = 3;
        
    }
    
    
    
    
    
    
    // started from first point
    if ((touchPoint.x > 119 && touchPoint.x<354 && touchPoint.y > 61 && touchPoint.y<159))
    {
        resultWord = 0;
    }
    
    // started from second point
    if ((touchPoint.x > 119 && touchPoint.x<354 && touchPoint.y > 187 && touchPoint.y<285))
    {
        resultWord = 1;
    }
    
    // started from third point
    if  ((touchPoint.x > 119 && touchPoint.x<354 && touchPoint.y > 310 && touchPoint.y<408))
    {
        resultWord = 2;
    }
    
    // started from fourth point
    if ((touchPoint.x > 119 && touchPoint.x<354 && touchPoint.y > 438 && touchPoint.y<536)) 
    {
        resultWord = 3;
    }
    
    if ((touchPoint.x > 119 && touchPoint.x<354 && touchPoint.y > 61 && touchPoint.y<159) ||
        (touchPoint.x > 119 && touchPoint.x<354 && touchPoint.y > 187 && touchPoint.y<285) ||
        (touchPoint.x > 119 && touchPoint.x<354 && touchPoint.y > 310 && touchPoint.y<408) ||
        (touchPoint.x > 119 && touchPoint.x<354 && touchPoint.y > 438 && touchPoint.y<536)  ) 
    {
        touchStartingPoint = TRUE;
        touchFromWordBegan = TRUE;
                touchFromImageBegan = FALSE;

    }
    
    
    if( (touchPoint.x > 660 && touchPoint.x< 758 && touchPoint.y > 61 && touchPoint.y< 159) ||
       (touchPoint.x > 660 && touchPoint.x< 758 && touchPoint.y > 187 && touchPoint.y< 285) ||
       (touchPoint.x > 660 && touchPoint.x< 758 && touchPoint.y > 310 && touchPoint.y< 408) ||
       (touchPoint.x > 660 && touchPoint.x< 758 && touchPoint.y > 438 && touchPoint.y< 536)  )
    {
        touchStartingPoint = TRUE;
                touchFromWordBegan = FALSE;
        touchImageButton = TRUE;
        touchFromImageBegan = TRUE;
    }
    
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{    
    if (touchStartingPoint) {
        touchMovedTrue = TRUE;
        UITouch *touch = [touches anyObject];
        lastTouchPoint = [touch previousLocationInView:self];
        [self setNeedsDisplay];    
    }
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    BOOL endTouchPoint = FALSE;
    
    if (!touchMovedTrue && touchStartingPoint) {
        [self playWord];
    }
    
    
    
    // Ends from first point
    if (lastTouchPoint.x > 660 && lastTouchPoint.x< 758 && lastTouchPoint.y > 61  && lastTouchPoint.y< 159)
    {
        if (touchFromImageBegan) {
            return;
        }
        [userResults setObject:[NSString stringWithFormat:@"1"] forKey:[NSString stringWithFormat:@"%d",resultWord]];
        NSLog(@"Recorded 1sst word");
    }
    
    // Ends from second point
    if (lastTouchPoint.x > 660 && lastTouchPoint.x< 758 && lastTouchPoint.y > 187 && lastTouchPoint.y< 285)
    {
        if (touchFromImageBegan) {
            return;
        }
        [userResults setObject:[NSString stringWithFormat:@"2"] forKey:[NSString stringWithFormat:@"%d",resultWord]];
                NSLog(@"Recorded 2nd word");
    }
    
    // Ends from third point
    if  (lastTouchPoint.x > 660 && lastTouchPoint.x< 758 && lastTouchPoint.y > 310&& lastTouchPoint.y< 408)
    {
        if (touchFromImageBegan) {
            return;
        }
        [userResults setObject:[NSString stringWithFormat:@"3"] forKey:[NSString stringWithFormat:@"%d",resultWord]];
                NSLog(@"Recorded 3rd word");
        
    }
    
    // Ends from fourth point
    if (lastTouchPoint.x > 660 && lastTouchPoint.x< 758 && lastTouchPoint.y > 438&& lastTouchPoint.y< 536)
    {
        if (touchFromImageBegan) {
            return;
        }
        [userResults setObject:[NSString stringWithFormat:@"4"] forKey:[NSString stringWithFormat:@"%d",resultWord]];
                NSLog(@"Recorded 4th word");
        
    }

    
    if ((lastTouchPoint.x > 119 && lastTouchPoint.x<354 && lastTouchPoint.y > 61 && lastTouchPoint.y<159))
    {
        if (touchFromWordBegan) {
            return;
        }
        [userResults setObject:[NSString stringWithFormat:@"1"] forKey:[NSString stringWithFormat:@"%d",resultWord]];
    }
    
    // started from second point
    if ((lastTouchPoint.x > 119 && lastTouchPoint.x<354 && lastTouchPoint.y > 187 && lastTouchPoint.y<285))
    {
        if (touchFromWordBegan) {
            return;
        }
        [userResults setObject:[NSString stringWithFormat:@"2"] forKey:[NSString stringWithFormat:@"%d",resultWord]];
    }
    
    // started from third point
    if  ((lastTouchPoint.x > 119 && lastTouchPoint.x<354 && lastTouchPoint.y > 310 && lastTouchPoint.y<408))
    {
        if (touchFromWordBegan) {
            return;
        }
        [userResults setObject:[NSString stringWithFormat:@"3"] forKey:[NSString stringWithFormat:@"%d",resultWord]];
    }
    
    // started from fourth point
    if ((lastTouchPoint.x > 119 && lastTouchPoint.x<354 && lastTouchPoint.y > 438 && lastTouchPoint.y<536))
    {
        if (touchFromWordBegan) {
            return;
        }
        [userResults setObject:[NSString stringWithFormat:@"4"] forKey:[NSString stringWithFormat:@"%d",resultWord]]; 
    }
    
    if( (lastTouchPoint.x > 660 && lastTouchPoint.x< 758 && lastTouchPoint.y > 61 && lastTouchPoint.y< 159) ||
        (lastTouchPoint.x > 660 && lastTouchPoint.x< 758 && lastTouchPoint.y > 187 && lastTouchPoint.y< 285) ||
        (lastTouchPoint.x > 660 && lastTouchPoint.x< 758 && lastTouchPoint.y > 310 && lastTouchPoint.y< 408) ||
        (lastTouchPoint.x > 660 && lastTouchPoint.x< 758 && lastTouchPoint.y > 438 && lastTouchPoint.y< 536)  ) 
    {
        endTouchPoint = TRUE;        
    }
    
    if ((lastTouchPoint.x > 119 && lastTouchPoint.x<354 && lastTouchPoint.y > 61 && lastTouchPoint.y<159) ||
        (lastTouchPoint.x > 119 && lastTouchPoint.x<354 && lastTouchPoint.y > 187 && lastTouchPoint.y<285) ||
        (lastTouchPoint.x > 119 && lastTouchPoint.x<354 && lastTouchPoint.y > 310 && lastTouchPoint.y<408) ||
        (lastTouchPoint.x > 119 && lastTouchPoint.x<354 && lastTouchPoint.y > 438 && lastTouchPoint.y<536)  )
    {
        endTouchPoint = TRUE;
    }
    
    if (!endTouchPoint) {
        touchPoint.x = 0;
        touchPoint.y = 0;
        lastTouchPoint.x = 0;
        lastTouchPoint.y = 0;
        touchMovedTrue = FALSE;
        [self setNeedsDisplay];                
    }


    if (touchMovedTrue) {
        if (line3Drawn) {
            line4Drawn = TRUE;
            line4StartPoint =  touchPoint;
            line4EndPoint = lastTouchPoint;
            [self setNeedsDisplay];
        }
        
        if (line2Drawn) {
            line3Drawn = TRUE;
            if (!line4Drawn) {
            line3StartPoint =  touchPoint;
            line3EndPoint = lastTouchPoint;
            }
            [self setNeedsDisplay];
        }        
        
        if (line1Drawn) {
            line2Drawn = TRUE;
            if (!line3Drawn) {
            line2StartPoint =  touchPoint;
            line2EndPoint = lastTouchPoint;
            }
            [self setNeedsDisplay];
        }
        
        line1Drawn = TRUE;    
        if (!line2Drawn) {
            line1StartPoint = touchPoint;
            line1EndPoint = lastTouchPoint;
        }
        [self setNeedsDisplay];                
    }        
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (line1Drawn) {
        CGContextRef context    = UIGraphicsGetCurrentContext();                
        CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);    
        CGContextSetLineWidth(context, 2.0);        
        CGContextMoveToPoint(context, line1StartPoint.x ,line1StartPoint.y); //start at this point    
        CGContextAddLineToPoint(context, line1EndPoint.x, line1EndPoint.y); //draw to this point    
        // and now draw the Path!
        CGContextStrokePath(context);  
    }
    
    if (line2Drawn) {
        CGContextRef context    = UIGraphicsGetCurrentContext();                
        CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);    
        CGContextSetLineWidth(context, 2.0);        
        CGContextMoveToPoint(context, line2StartPoint.x ,line2StartPoint.y); //start at this point    
        CGContextAddLineToPoint(context, line2EndPoint.x, line2EndPoint.y); //draw to this point    
        // and now draw the Path!
        CGContextStrokePath(context);  
    }
    
    if (line3Drawn) {
        CGContextRef context    = UIGraphicsGetCurrentContext();                
        CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);    
        CGContextSetLineWidth(context, 2.0);        
        CGContextMoveToPoint(context, line3StartPoint.x ,line3StartPoint.y); //start at this point    
        CGContextAddLineToPoint(context, line3EndPoint.x, line3EndPoint.y); //draw to this point    
        // and now draw the Path!
        CGContextStrokePath(context);  
    }
    
    if (line4Drawn) {
        CGContextRef context    = UIGraphicsGetCurrentContext();                
        CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);    
        CGContextSetLineWidth(context, 2.0);        
        CGContextMoveToPoint(context, line4StartPoint.x ,line4StartPoint.y); //start at this point    
        CGContextAddLineToPoint(context, line4EndPoint.x, line4EndPoint.y); //draw to this point    
        // and now draw the Path!
        CGContextStrokePath(context);  
    }

    CGContextRef context    = UIGraphicsGetCurrentContext();     
    struct CGColor *color = [UIColor greenColor].CGColor;    
    if (line4Drawn) {
        color = [UIColor redColor].CGColor;    
        self.userInteractionEnabled = FALSE;
        [[NSUserDefaults standardUserDefaults] setObject:userResults forKey:@"userResults"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    CGContextSetStrokeColorWithColor(context, color);    
    CGContextSetLineWidth(context, 2.0);        
    CGContextMoveToPoint(context, touchPoint.x,touchPoint.y); //start at this point    
    CGContextAddLineToPoint(context, lastTouchPoint.x, lastTouchPoint.y); //draw to this point    
    // and now draw the Path!
    CGContextStrokePath(context);    
}

-(void) playWord{
    

    
    if (wordPlayer) {
        [wordPlayer stop];
        [wordPlayer release];
        wordPlayer = Nil;
    }          
    NSString* soundName;
    
    if (touchImageButton) {
        touchImageButton = false;
        soundName = [imagesArray objectAtIndex:resultWord];
    }
    else{
        soundName = [wordsSoundArray objectAtIndex:resultWord];
    }
    
    
    NSString *squishPath = [DocumentDirectory playSound:soundName];    
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];    
    wordPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    wordPlayer.numberOfLoops = 0;
    wordPlayer.delegate = self;    
    [wordPlayer play];  
    
}

- (void)setNeedsDisplayInRect:(CGRect)invalidRect{

}

-(void) checkCorrectMatch{

    
}

@end
