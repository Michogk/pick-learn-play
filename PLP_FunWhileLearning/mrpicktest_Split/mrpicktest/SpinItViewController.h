//
//  SpinItViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 3/30/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>

@interface SpinItViewController : UIViewController<AVAudioPlayerDelegate,ADBannerViewDelegate>{
    NSMutableDictionary* tempContentDicCorrect;
    AVAudioPlayer *player;
    int cId;
    int activeLanguage;
    NSString* ActiveGrade;
    int wordsCount;
    AVAudioPlayer *mySpinItPlayer;
    NSTimer* cheeringSoundTimer;
    int sessionCounter;
    int correctWordCounter;
    UILabel *_wordCounterlbl;
    UILabel *_lblLevel1;
    UILabel *_lblLevel2;
    int currentWord;
    int level;
    
    // Report Data
    NSMutableArray *retval;
    int reportWordId;
    NSString *reportWordDesc;
    int reportCorrectCount;
    int reportTotatCount;
}
@property (retain, nonatomic) IBOutlet UIButton *btnLeft;
@property (retain, nonatomic) IBOutlet UIButton *btnRight;

- (IBAction)btnLeftPressed:(id)sender;
- (IBAction)btnRightPressed:(id)sender;
- (IBAction)repeatSound:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnRepeatSound;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewCheering;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;
- (IBAction)gotoMainMenu:(id)sender;

@property (strong, nonatomic) ADBannerView *bannerView;
@property (retain, nonatomic) IBOutlet UILabel *lblSynonym;
@property (retain, nonatomic) IBOutlet UILabel *lblOpposite;

@end
