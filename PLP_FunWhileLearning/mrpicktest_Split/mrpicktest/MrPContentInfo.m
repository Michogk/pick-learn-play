//
//  MrPContentInfo.m
//  Mr.Pick
//
//  Created by Muhammad Mosib Asad on 9/14/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//


#import "MrPContentInfo.h"

@implementation MrPContentInfo


@synthesize wordId = _wordId;
@synthesize englishWord = _englishWord;
@synthesize arabicRef = _arabicRef;
@synthesize isSentence = _isSentence;
@synthesize classificationId = _classificationId;
@synthesize imageId = _imageId;
@synthesize voiceId = _voiceId;
@synthesize arabicWord = _arabicWord;

@synthesize descWord = _descWord;
@synthesize subClassificationId = _subClassificationId;

@synthesize grammarId = _grammarId;
@synthesize principleId = _principleId;
@synthesize grammarWord = _grammarWord;
@synthesize groupId = _groupId;
@synthesize principleWord = _principleWord;
@synthesize langId = _langId;
@synthesize principleVoiceId = _principleVoiceId;
@synthesize arabicVoiceid = _arabicVoiceid;
@synthesize folderid =_folderid;
@synthesize foldername = _foldername;
@synthesize translate = _translate;
@synthesize english_ref= _english_ref;
@synthesize synonymWord = _synonymWord;
@synthesize oppositeWord = _oppositeWord;

- (id)initWithName:(int) fid :(NSString *) fname  {
    if ((self = [super init])) {  
        self.folderid =fid;
        self.foldername=fname;
    }
    return self;        
}
- (id)initWithWordId:(int )wordId andVoiceId :(NSString *)voice andfWordname:(NSString*)word andLangDir:(int) langDir andImageId:(int)imgId{
    if ((self = [super init])) {  
        self.wordId = wordId;
        self.voiceId = voice;
        self.englishWord = word;
        self.langId = langDir;
        self.imageId = imgId;
    }
    return self; 




}

- (id)initWithWordId:(int)wordId englishWord:(NSString *)englishWord arabicRef:(int )arabicRef 
          isSentence:(int )isSentence imageId:(int)imageId voiceId:(NSString* )voiceId  translation:(int) translation{
    
    
    if ((self = [super init])) {
        self.wordId = wordId;
        self.englishWord = englishWord;
        self.arabicRef = arabicRef;
        self.isSentence = isSentence;
        self.imageId = imageId;
        self.voiceId = voiceId;
        self.translate = translation;
    }    
    return self;
    
}

- (id)initWithimageId:(int)imageId classificationId:(int )classificationId andClassificationName:(NSString*) name {
    
    NSLog(@"Entered");
    if ((self = [super init])) {
        self.imageId = imageId;
        self.classificationId = classificationId;
        self.englishWord = name;
    }
    NSLog(@"%@",name);
    return self;
}

- (id)initWithimageId:(int)imageId classificationId:(int )classificationId{
    
    NSLog(@"Entered");
    if ((self = [super init])) {
        self.imageId = imageId;
        self.classificationId = classificationId;

    }
    return self;
}
- (id) initWithClassificationIds:(int)classifications{
    if ((self = [super init])) {
        self.classificationId = classifications;
    }
    return self;

}

-(id)initWithClassificationId:(int )classificationId arabicWord:(NSString *)descWord subId:(int )subClassificationId{
    if ((self = [super init])) {
    self.classificationId = classificationId;
    self.subClassificationId = subClassificationId;
    self.descWord = descWord;
    }
    return self;
}

-(id) initWithWordId:(int )wordId arabicWord:(NSString *)arabicWord isSentence:(int )isSentence arabicVoiceId:(NSString* ) arabicVoiceId imageId:(int) imageId translate:(int)translate andEnglishRef:(int) englishRefrence andSynonym:(NSString*) synonym andOppositeWord:(NSString*) opposite andPrincipleId:(int) principleId andGrammarId:(int) grammarId andPrincipleName:(NSString*)principleName andGrammarName:(NSString*) grammarName{
    
     if ((self = [super init])) {
    self.wordId = wordId;
    self.arabicWord = arabicWord;
    self.englishWord = arabicWord;
    self.isSentence = isSentence;
    self.voiceId = arabicVoiceId;
    self.imageId = imageId;
    self.translate = translate;
    self.english_ref = englishRefrence;
    self.synonymWord = synonym;
    self.oppositeWord = opposite;
         self.principleId = principleId;
         self.grammarId = grammarId;
         self.principleWord = principleName;
         self.grammarWord = grammarName;

     
    }
    return self;
}


-(id) initWithWordId:(int )wordId arabicWord:(NSString *)arabicWord arabicVoiceId:(NSString* ) arabicVoiceId imageId:(int) imageId{
    
    if ((self = [super init])) {
        self.wordId = wordId;
        self.arabicWord = arabicWord;
        self.voiceId = arabicVoiceId;
        self.imageId = imageId;
        
    }
    return self;
}

-(id) initWithGrammarId:(int )grammarId grammarWord:(NSString *)grammarWord principlesId:(int )principlesId groupId:(int )groupId{
    if ((self = [super init])) {
        self.grammarWord = grammarWord;
        self.grammarId = grammarId;
        self.principleId = principlesId;
        self.groupId = groupId;
    }
    return self;

}



-(id) initWithGrammarId:(int )grammarId grammarWord:(NSString *)grammarWord{
    if ((self = [super init])) {
        self.grammarWord = grammarWord;
        self.grammarId = grammarId;
    }
    return self;
    
}

-(id) initWithPrincipleId:(int )principlesId principleWord:(NSString *)principleWord LangId:(int )LangId voiceId:(int )voiceId{
    if ((self = [super init])) {
        self.principleWord = principleWord;
        self.principleId = principlesId;
        self.LangId = LangId;
        self.principleVoiceId = voiceId;
    }
    return self;

}

@end
