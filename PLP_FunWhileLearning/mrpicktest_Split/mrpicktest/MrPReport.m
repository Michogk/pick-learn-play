//
//  MrPReport.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/31/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "MrPReport.h"

@implementation MrPReport
@synthesize wordDescription = _wordDescription;
@synthesize wordId = _wordId;
@synthesize activelanguage = _activelanguage;
@synthesize totalAnswers = _totalAnswers;
@synthesize noOfCorrect = _noOfCorrect;
@synthesize gameId = _gameId;
@synthesize classification = _classification;
@synthesize PINNUMBER = _PINNUMBER;

@synthesize noOfCorrectFixIt = _noOfCorrectFixIt;
@synthesize noOfCorrectSpinIt = _noOfCorrectSpinIt;
@synthesize noOfCorrectPopIt = _noOfCorrectPopIt;
@synthesize noOfCorrectPickIt = _noOfCorrectPickIt;
@synthesize noOfCorrectMoveIt = _noOfCorrectMoveIt;
@synthesize noOfCorrectMatchIt = _noOfCorrectMatchIt;
@synthesize noOfCorrectTypeIt = _noOfCorrectTypeIt;
@synthesize noOfCorrectBasketBall = _noOfCorrectBasketBall;
@synthesize noOfCorrectFootball = _noOfCorrectFootball;
@synthesize noOfCorrectHockey = _noOfCorrectHockey;

@synthesize totalAnswersFixIt = _totalAnswersFixIt;
@synthesize totalAnswersSpinIt = _totalAnswersSpinIt;
@synthesize totalAnswersPopIt = _totalAnswersPopIt;
@synthesize totalAnswersPickIt = _totalAnswersPickIt;
@synthesize totalAnswersMoveIt = _totalAnswersMoveIt;
@synthesize totalAnswersMatchIt = _totalAnswersMatchIt;
@synthesize totalAnswersTypeIt = _totalAnswersTypeIt;
@synthesize totalAnswersBasketBall = _totalAnswersBasketBall;
@synthesize totalAnswersFootball = _totalAnswersFootball;
@synthesize totalAnswersHockey = _totalAnswersHockey;

- initWithWordId:(int)reportWordId andWordDesc:(NSString*)reportWordDesc andCorrectCount:(int)reportCorrectCount andTotalCount:(int)reportTotatCount andActiveLanguage:(int)activeLanguage andClassification:(int)classification andGameId:(int)gamenumber{
    
    if ((self = [super init])) {
        self.wordId = reportWordId;
        self.wordDescription = reportWordDesc;
        self.totalAnswers = reportTotatCount;
        self.noOfCorrect = reportCorrectCount;
        self.classification = classification;
        self.activelanguage = activeLanguage;
        self.gameId = gamenumber;
    }
    return self;
}

- initWithWordId:(int)reportWordId andWordDesc:(NSString*)reportWordDesc andCorrectCountFixIt:(int)reportCorrectCountFixIt andTotalCountFixIt:(int)reportTotatCountFixIt andCorrectCountPopIt:(int)reportCorrectCountPopIt andTotalCountPopIt:(int)reportTotatCountPopIt andCorrectCountSpinIt:(int)reportCorrectCountSpinIt andTotalCountSpinIt:(int)reportTotatCountSpinIt andCorrectCountPickIt:(int)reportCorrectCountPickIt andTotalCountPickIt:(int)reportTotatCountPickIt andCorrectCountMoveIt:(int)reportCorrectCountMoveIt andTotalCountMoveIt:(int)reportTotatCountMoveIt andCorrectCountMatchIt:(int)reportCorrectCountMatchIt andTotalCountMatchIt:(int)reportTotatCountMatchIt andCorrectCountTypeIt:(int)reportCorrectCountTypeIt andTotalCountTypeIt:(int)reportTotatCountTypeIt andCorrectCountFootball:(int)reportCorrectCountFootball andTotalCountFootball:(int)reportTotatCountFootball andCorrectCountHockey:(int)reportCorrectCountHockey andTotalCountHockey:(int)reportTotatCountHockey
andCorrectCountBasketball:(int)reportCorrectCountBasketball andTotalCountBasketball:(int)reportTotatCountBasketball andActiveLanguage:(int)activeLanguage andClassification:(int)classification andGameId:(int)gamenumber andPinNumber:(int)pinNumber{
    if ((self = [super init])) {
        self.wordId = reportWordId;
        self.wordDescription = reportWordDesc;
        
        self.totalAnswersFixIt = reportTotatCountFixIt;
        self.noOfCorrectFixIt = reportCorrectCountFixIt;
        self.totalAnswersSpinIt = reportTotatCountSpinIt;
        self.noOfCorrectSpinIt = reportCorrectCountSpinIt;
        self.totalAnswersPopIt = reportTotatCountPopIt;
        self.noOfCorrectPopIt = reportCorrectCountPopIt;
        self.totalAnswersPickIt = reportTotatCountPickIt;
        self.noOfCorrectPickIt = reportCorrectCountPickIt;
        self.totalAnswersMoveIt = reportTotatCountMoveIt;
        self.noOfCorrectMoveIt = reportCorrectCountMoveIt;
        self.totalAnswersMatchIt = reportTotatCountMatchIt;
        self.noOfCorrectMatchIt = reportCorrectCountMatchIt;
        self.totalAnswersTypeIt = reportTotatCountTypeIt;
        self.noOfCorrectTypeIt = reportCorrectCountTypeIt;
        
        self.totalAnswersFootball = reportTotatCountFootball;
        self.noOfCorrectFootball = reportCorrectCountFootball;
        
        self.totalAnswersHockey = reportTotatCountHockey;
        self.noOfCorrectHockey = reportCorrectCountHockey;
        
        self.totalAnswersBasketBall = reportTotatCountBasketball;
        self.noOfCorrectBasketBall = reportCorrectCountBasketball;
        
        self.classification = classification;
        self.activelanguage = activeLanguage;
        self.gameId = gamenumber;
        self.PINNUMBER = pinNumber;
    }
    return self;
}

@end
