//
//  SetupViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/23/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetupViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource,NSXMLParserDelegate>{

//    NSArray *grades;
    UIPickerView *myPickerView;
    IBOutlet UIButton *BtnGrade;
    IBOutlet UITextField *Age;
    IBOutlet UITextField *Name;
    IBOutlet UISwitch *LSSwitch;
    IBOutlet UISegmentedControl *languageSegment;
    UIImageView *imgViewBoy;
    UIImageView *imgViewGirl;
    IBOutlet UIButton *BtnMyCampBlue;
    IBOutlet UIButton *BtnMyCampPink;
    IBOutlet UIButton *BtnMyPlaygroundBlue;
    IBOutlet UIButton *BtnMyPlaygroundPink;
    BOOL showMovie;
    
    NSXMLParser *rssParser;
    NSData *xmlFile;
    BOOL errorParsing;
    NSString *currentElement;
    NSMutableString *ElementValue;
    NSString *reasonPINRegistration;
}
@property (retain, nonatomic) IBOutlet UIScrollView *characterScrollView;
- (IBAction)noHarkatPressed:(id)sender;
- (IBAction)backgroundMusicStatusChangePressed:(id)sender;
@property (retain, nonatomic) IBOutlet UISegmentedControl *backgroundMusicSegmentControl;
@property (retain, nonatomic) IBOutlet UISegmentedControl *noTashkeelSegmentedControl;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;

- (IBAction)SelectLanguage:(id)sender;
- (IBAction)segmentPressedVideoHelp:(id)sender;

- (IBAction)LanguageSupportSwitch:(id)sender;
- (IBAction)BoySelected:(id)sender;
- (IBAction)GirlSelected:(id)sender;
- (IBAction)NameTxt:(id)sender;
- (IBAction)helpURL:(id)sender;
@property (retain, nonatomic) IBOutlet UISegmentedControl *VideoHelp;
@property (retain, nonatomic) IBOutlet UIButton *btnBoy;
@property (retain, nonatomic) IBOutlet UIButton *btnGirl;

- (IBAction)MyCampBlueBackground:(id)sender;
- (IBAction)MyCampPinkBackground:(id)sender;
- (IBAction)MyPlaygroundBlueBackground:(id)sender;
- (IBAction)MyPlaygroundPinkBackground:(id)sender;
- (IBAction)selectGrade:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *txtFieldPinNumber;
- (IBAction)btnCheckPinPressed:(id)sender;
@property (retain, nonatomic) IBOutlet UITextField *txtFieldName;
- (IBAction)btnPressedSaveName:(id)sender;

- (IBAction)helpButton:(id)sender;
@property(nonatomic, retain) NSMutableArray *grades;
@end
