//
//  BackendUpdateCheck.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 6/26/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "BackendUpdateCheck.h"
#import "CAURLDownload.h"
#import "MrPContentInfo.h"
#import "MrPDatabase.h"
#import "ASIHTTPRequest.h"

@implementation BackendUpdateCheck



- (void)parserDidStartDocument:(NSXMLParser *)parser{
    NSLog(@"File found and parsing started");
    
}

- (void)parseXMLFileAtURL:(NSString *)URL
{
    
    // check which grades need parsing
    NSString* gradesPurchased = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"gradesPurchased"]];
    componentsArr = [[NSArray alloc] init];
    componentsArr = [gradesPurchased componentsSeparatedByString: @" "];
    
    //    NSString *agentString = @"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-us) AppleWebKit/525.27.1 (KHTML, like Gecko) Version/3.2.1 Safari/525.27.1";
    
    
    NSString *agentString = [NSString stringWithFormat:@"application/x-%@",URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
                                    [NSURL URLWithString:URL]];
    
    [request setValue:agentString forHTTPHeaderField:@"content-type"];
    //    [request setValue:agentString forHTTPHeaderField:@"User-Agent"];
    xmlFile = [ NSURLConnection sendSynchronousRequest:request returningResponse: nil error: nil ];
    
    articles = [[NSMutableArray alloc] init];
    errorParsing=NO;
    
    //    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getdata.aspx" ofType:@"xml"];
    //    NSData *myData = [NSData dataWithContentsOfFile:filePath];
    //    if ( myData ) {
    //        rssParser = [[NSXMLParser alloc] initWithData:myData];
    //    }
    
    rssParser = [[NSXMLParser alloc] initWithData:xmlFile];
    [rssParser setDelegate:self];
    [rssParser setShouldProcessNamespaces:NO];
    [rssParser setShouldReportNamespacePrefixes:NO];
    [rssParser setShouldResolveExternalEntities:NO];
    
    [rssParser parse];
    
    
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    NSString *errorString = [NSString stringWithFormat:@"Error code %i", [parseError code]];
    NSLog(@"Error parsing XML: %@", errorString);
    errorParsing=YES;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    currentElement = [elementName copy];
    ElementValue = [[NSMutableString alloc] init];
    if ([elementName isEqualToString:@"StudentRequest"]) {
        dicWord = [[NSMutableDictionary alloc] init];
        arrFolderIds = [[NSMutableArray alloc] init];
        arrFolderNames = [[NSMutableArray alloc] init];
        arrWordIds = [[NSMutableArray alloc] init];
        
        gradeOfParsedData = [NSString retain];
        gradeOfParsedData = @"Start";
    }
    
    if ([elementName isEqualToString:@"Grade"]) {
        
        if ([gradeOfParsedData isEqualToString:@"Start"]) {
            gradeOfParsedData = ElementValue;
            NSLog(@"grade of purchased data is %@",gradeOfParsedData);
            gradeIsPurchasedSoParseData = TRUE;
            //            [self checkIfGradeisPurchasedByUserToo:gradeOfParsedData];
            return;
        }
        else{
            if (gradeIsPurchasedSoParseData == TRUE) {
                [self updateDatabase];
            }
            gradeIsPurchasedSoParseData = TRUE;
            
            gradeOfParsedData = ElementValue;
            //            [self checkIfGradeisPurchasedByUserToo:gradeOfParsedData];
            [dicWord removeAllObjects];
            [arrWordIds removeAllObjects];
            [arrFolderNames removeAllObjects];
            [arrFolderIds removeAllObjects];
        }
    }
}
-(void) checkIfGradeisPurchasedByUserToo:(NSString*) XMLElementValue{
    gradeIsPurchasedSoParseData = TRUE;
    //    NSLog(@"Value is %@",elemntExtractedValue);
    for (int i = 0; i < [componentsArr count]; i++) {
        if([ [componentsArr objectAtIndex:i] isEqualToString:[NSString stringWithString:elemntExtractedValue ]]){
            gradeIsPurchasedSoParseData = TRUE;
            break;
        }
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if (!gradeIsPurchasedSoParseData) {
        return;
    }
    string =[string stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    string =[string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    if(!ElementValue){
        ElementValue = [[NSMutableString alloc] initWithString:string];
    }
    else{
        [ElementValue appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    if (!gradeIsPurchasedSoParseData) {
        return;
    }
    if ([elementName isEqualToString:@"FolderID"]) {
        parserFolderId = ElementValue;
        [arrFolderIds addObject:parserFolderId];
    }
    
    if ([elementName isEqualToString:@"FolderName"]) {
        [arrFolderNames addObject:ElementValue];
        //        [dicFolder setObject:ElementValue forKey:[NSString stringWithFormat:@"FolderName%@",parserFolderId]];
    }
    
    if ([elementName isEqualToString:@"WordID"]) {
        parserWordId = ElementValue;
        [arrWordIds addObject:parserWordId];
    }
    
    if ([elementName isEqualToString:@"WordDescription"]) {
        [dicWord setObject:ElementValue forKey:[NSString stringWithFormat:@"WordDescription%@" ,parserWordId]];
    }
    
    if ([elementName isEqualToString:@"ImageURL"]) {
        [dicWord setObject:ElementValue forKey:[NSString stringWithFormat:@"imageURL%@" ,parserWordId]];
    }
    
    if ([elementName isEqualToString:@"VoiceURL"]) {
        [dicWord setObject:ElementValue forKey:[NSString stringWithFormat:@"voiceURL%@" ,parserWordId]];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    if (errorParsing == NO)
    {
        // To parse Grade 4 i.e last element
        if (gradeIsPurchasedSoParseData == TRUE) {
            [self updateDatabase];
            //            [dicFolder removeAllObjects];
            [dicWord removeAllObjects];
            [arrWordIds removeAllObjects];
            [arrFolderIds removeAllObjects];
            [arrFolderNames removeAllObjects];
        }
        [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"Reload" object:self]];
        NSLog(@"XML processing done!");
    }
    else {
        NSLog(@"Error occurred during XML processing");
    }
}

-(void) updateDatabase{
    
    [self updateFolderData];
    [self updateWordData];
}

-(void) updateWordData{
    
    //    NSString *selectedGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite3",gradeOfParsedData]];
    
    BOOL success;
    
    if ([gradeOfParsedData isEqualToString:@"Grade1"]) {
        databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"G1.sqlite3"]];
    }
    if ([gradeOfParsedData isEqualToString:@"Grade2"]) {
        databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"G2.sqlite3"]];
    }
    if ([gradeOfParsedData isEqualToString:@"Grade3"]) {
        databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"G3.sqlite3"]];
    }
    if ([gradeOfParsedData isEqualToString:@"Grade4"]) {
        databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"G4.sqlite3"]];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    success = [fileManager fileExistsAtPath:databasePath];
    if (!success) {
        NSLog(@"Database Doesnot Exist");
        return;
    }
    
    
    sqlite3 *database;
    //    // Open the database from the users filessytem and delete the row which i want to update
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        
        //Updating the row
        sqlite3_stmt *statement;
        
        BOOL success = NO;
        if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
        {
            for (int k = 0; k < [arrWordIds count]; k++)
            {
                int grade = 1;
                // Sample Data
                int wordId = [[arrWordIds objectAtIndex:k] intValue];
                NSString *word = [dicWord objectForKey:[NSString stringWithFormat:@"WordDescription%@" ,[arrWordIds objectAtIndex:k]]];
                // checking if its word or sentence
                int isSentence = 0;
                NSString *enteredword = word;
                int Length = [enteredword length];
                for (int i = 0; i <Length ; i++) {
                    NSString * newString = [enteredword substringWithRange:NSMakeRange(i, 1)];
                    if ([newString isEqualToString:@" "])
                    {
                        isSentence = 1;
                    }
                }
                int currentClassificationId = [[[arrWordIds objectAtIndex:k] substringToIndex:3] intValue];
                NSString* voice = [NSString stringWithFormat:@"E%@",[arrWordIds objectAtIndex:k]];
                int imageId = [[arrWordIds objectAtIndex:k] intValue];
                
                // Deleting Old Data
                sqlite3_stmt    *deleteStmt;
                const char *sql = "delete from english_word where word_id = ?";
                if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
                    NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
                sqlite3_bind_int(deleteStmt, 1, wordId);
                if (SQLITE_DONE != sqlite3_step(deleteStmt))
                    NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
                sqlite3_reset(deleteStmt);
                
                
                sqlite3_stmt    *deleteStmtReport;
                const char *sqlReport = "delete from Report where word_id = ?";
                if(sqlite3_prepare_v2(database, sqlReport, -1, &deleteStmtReport, NULL) != SQLITE_OK)
                    NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
                sqlite3_bind_int(deleteStmtReport, 1, wordId);
                if (SQLITE_DONE != sqlite3_step(deleteStmtReport))
                    NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
                sqlite3_reset(deleteStmtReport);
                
                // Updating With New Data

                NSString *insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,%@,image_id) VALUES ('%d','%@','%d','%d','%@','%d','%d')",gradeOfParsedData, wordId, word,isSentence, currentClassificationId,voice,grade,imageId];
                int insertZeroInteger = 0;
                
                NSString *insertSQLQueryReport = [NSString stringWithFormat:@"INSERT INTO Report (Word_Id, Word_desc, classificaiton,Game_Id ,total_correct_FixIt,total_answers_FixIt,total_correct_SpinIt,total_answers_SpinIt,total_correct_PopIt,total_answers_PopIt,total_correct_PickIt,total_answers_PickIt,total_correct_MoveIt,total_answers_MoveIt,total_correct_MatchIt,total_answers_MatchIt,total_correct_TypeIt,total_answers_TypeIt,active_language) VALUES ('%d','%@','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d','%d')", wordId, word, currentClassificationId,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger,insertZeroInteger];
                
                if ([gradeOfParsedData isEqualToString:@"Grade1"]) {
                    insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,G1,image_id) VALUES ('%d','%@','%d','%d','%@','%d','%d')", wordId, word,isSentence, currentClassificationId,voice,grade,imageId];
                }
                if ([gradeOfParsedData isEqualToString:@"Grade2"]) {
                    insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,G2,image_id) VALUES ('%d','%@','%d','%d','%@','%d','%d')", wordId, word,isSentence, currentClassificationId,voice,grade,imageId];
                }
                if ([gradeOfParsedData isEqualToString:@"Grade3"]) {
                    insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,G3,image_id) VALUES ('%d','%@','%d','%d','%@','%d','%d')", wordId, word,isSentence, currentClassificationId,voice,grade,imageId];
                }
                if ([gradeOfParsedData isEqualToString:@"Grade4"]) {
                    insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO english_word (word_id, english_word, Is_Sentence, classification_id,voice_id,G4,image_id) VALUES ('%d','%@','%d','%d','%@','%d','%d')", wordId, word,isSentence, currentClassificationId,voice,grade,imageId];
                }
                
                const char *insert_stmt = [insertSQLQuery UTF8String];
                sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    success=YES;
                }
                else
                {
                    NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                    success=NO;
                }
                sqlite3_finalize(statement);
                
                
                success = NO;
                const char *insert_stmt_Report = [insertSQLQueryReport UTF8String];
                sqlite3_prepare_v2(database, insert_stmt_Report, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    success=YES;
                }
                else
                {
                    NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                    success=NO;
                }
                sqlite3_finalize(statement);
                
            }
        }
        sqlite3_close(database);
    }
    NSLog(@"Downloading resources");
    
    // Downloading Resources
    NSLog(@"Dic words is %@",dicWord);
    NSLog(@"arrwords ids is %@",arrWordIds);
    for (int k = 0; k < [arrWordIds count]; k++)
    {
        //        int wordId = [[arrWordIds objectAtIndex:k] intValue];
        NSString *imageURLName = [dicWord objectForKey:[NSString stringWithFormat:@"imageURL%@" ,[arrWordIds objectAtIndex:k]]];
        NSString *voiceURLName = [dicWord objectForKey:[NSString stringWithFormat:@"voiceURL%@" ,[arrWordIds objectAtIndex:k]]];
        
        NSLog(@"this is %@",imageURLName);
        CPL = [arrWordIds objectAtIndex:k];
        NSLog(@"CPL is %@",CPL);
        
        // Download File
        if(imageURLName){
            
            NSString* activeGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
            
            //            NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            //            NSString  *documentsDirectory = [paths objectAtIndex:0];
            
            //            NSLog(@"filePath is %@",filePath);
            //    [documentsDirectory stringByAppendingPathComponent:@"E9920.png"];
            //            [recievedData writeToFile:filePath atomically:YES];
            //            NSURL *pathURL= [NSURL fileURLWithPath:filePath];
            
            UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageURLName]]];
            
            NSLog(@"%f,%f",image.size.width,image.size.height);
            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            
            // If you go to the folder below, you will find those pictures
            NSLog(@"%@",docDir);
            
            NSLog(@"saving png");
            
            NSString  *dir = [NSString stringWithFormat:@"%@/%@", docDir,activeGrade];
            int imagenumber = [[arrWordIds objectAtIndex:k] intValue];
            NSString *pngFilePath = [NSString stringWithFormat:@"%@/%d.png",dir,imagenumber];
            NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
            [data1 writeToFile:pngFilePath atomically:YES];
            NSURL *pathURL= [NSURL fileURLWithPath:pngFilePath];
            
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            success = [fileManager fileExistsAtPath:pngFilePath];
            if (!success) {
                NSLog(@"png file Doesnot Exist");
                //                return;
            }
            else{
                [self addSkipBackupAttributeToItemAtURL:pathURL];
            }
            
            //            [CAURLDownload downloadURL:[NSURL URLWithString:imageURLName]
            //                            target:self
            //                          selector:@selector(downloadFinished:conneciton:)
            //                      failSelector:@selector(downloadFailed:)
            //                          userInfo:nil];
        }
        
        //        voiceURLName = @"http://212.78.67.102/mrpickit/WordVoice/E9918.mp3";
        if(voiceURLName){
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
            NSString* activeGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
            NSString *soundName = [NSString stringWithFormat:@"E%@.caf",[arrWordIds objectAtIndex:k] ];
            //            soundName = [NSString stringWithFormat:@"E9918.caf"];
            NSString  *datapath = [NSString stringWithFormat:@"%@/%@/%@", documentsDirectory,activeGrade,soundName];
            
            
            NSURL *url = [NSURL URLWithString:voiceURLName];
            ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
            [request setDownloadDestinationPath:[NSString stringWithFormat:@"%@",datapath]]; //use the path from earlier
            [request setDelegate:self];
            [request startAsynchronous];
            
            //            NSData *data = [NSData dataWithContentsOfURL:url];
            
            //            NSArray  *docList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            //            NSString *documentDir  = [docList objectAtIndex:0];
            //            NSString *soundfilePath = [datapath stringByAppendingPathComponent:@"reading.caf"];
            
            //            [data writeToFile:soundfilePath atomically:YES];
            
            
            //            NSFileManager *fileManager = [NSFileManager defaultManager];
            //            NSURL *pathURL= [NSURL fileURLWithPath:soundfilePath];
            //            success = [fileManager fileExistsAtPath:pngFilePath];
            //            if (!success) {
            //                NSLog(@"png file Doesnot Exist");
            //                return;
            //            }
            //            else{
            //                [self addSkipBackupAttributeToItemAtURL:pathURL];
            //            }
            
            
            
            
            
            
            //            [CAURLDownload downloadURL:[NSURL URLWithString:voiceURLName]
            //                                target:self
            //                              selector:@selector(downloadFinished:conneciton:)
            //                          failSelector:@selector(downloadFailed:)
            //                              userInfo:nil];
            
        }
        
        // Save in Doc Dir
        
        // Rename File
        
    }
}


- (void)downloadFinished:(NSData *)recievedData conneciton:(CAURLDownload *)connection {
    
    //    NSLog(@"Data Saved Successfully");
    NSString* activeGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    
    NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@/", documentsDirectory,activeGrade];
    NSLog(@"filePath is %@",filePath);
    //    [documentsDirectory stringByAppendingPathComponent:@"E9920.png"];
    [recievedData writeToFile:filePath atomically:YES];
    NSURL *pathURL= [NSURL fileURLWithPath:filePath];
    [self addSkipBackupAttributeToItemAtURL:pathURL];
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (void)downloadFailed:(CAURLDownload *)connection {
    NSLog(@"Download Failed!");
}


-(void) updateFolderData{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite3",gradeOfParsedData]];
    
    if ([gradeOfParsedData isEqualToString:@"Grade1"]) {
        databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"G1.sqlite3"]];
    }
    if ([gradeOfParsedData isEqualToString:@"Grade2"]) {
        databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"G2.sqlite3"]];
    }
    if ([gradeOfParsedData isEqualToString:@"Grade3"]) {
        databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"G3.sqlite3"]];
    }
    if ([gradeOfParsedData isEqualToString:@"Grade4"]) {
        databasePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"G4.sqlite3"]];
    }
    
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    success = [fileManager fileExistsAtPath:databasePath];
    if (!success) {
        NSLog(@"Database Doesnot Exist");
        return;
    }
    
    sqlite3 *database;
    // Open the database from the users filessytem and delete the row which i want to update
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        for (int i = 0; i < [arrFolderIds count]; i++)
        {
            NSString *folderName = [arrFolderNames objectAtIndex:i];
            int classificationId = [[arrFolderIds objectAtIndex:i] intValue];
            
            sqlite3_stmt    *deleteStmt;
            const char *sql = "delete from Classifications where Classification_id = ?";
            if(sqlite3_prepare_v2(database, sql, -1, &deleteStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating delete statement. '%s'", sqlite3_errmsg(database));
            sqlite3_bind_int(deleteStmt, 1, classificationId);
            //            if (SQLITE_DONE != sqlite3_step(deleteStmt))
            //                NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
            //            sqlite3_reset(deleteStmt);
            //Updating the row
            
            sqlite3_stmt    *statement;
            NSString *insertSQLQuery = [NSString stringWithFormat:@"INSERT INTO Classifications (Classification_id, desc) VALUES ('%d','%@')", classificationId, folderName];
            
            BOOL success = NO;
            if (sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
            {
                NSLog(@"%@",insertSQLQuery);
                const char *insert_stmt = [insertSQLQuery UTF8String];
                
                sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    success=YES;
                } else {
                    NSLog( @"Error while inserting '%s'", sqlite3_errmsg(database));
                    success=NO;
                }
                sqlite3_finalize(statement);
            }
        }
        sqlite3_close(database);
    }
}

-(void) extractParsedData{
    
    // Download New resources and remove existing resources if any!
    [self downloadUpdatedResourcesForMyHome];
}

-(void) downloadUpdatedResourcesForMyHome{
    
    for (int i = 0; i < [articles count]; i++) {
        imageURL = [[articles objectAtIndex:i] objectForKey:@"ImageURL"];
        //        NSLog(@"BEFORE%@",imageURL);
        imageURL = @"http://212.78.67.102/mrpickit/WordImage/9991.jpg";
        //        NSLog(@"After %@",imageURL);
        [CAURLDownload downloadURL:[NSURL URLWithString:imageURL]
                            target:self
                          selector:@selector(downloadFinished:conneciton:)
                      failSelector:@selector(downloadFailed:)
                          userInfo:nil];
    }
}

@end

