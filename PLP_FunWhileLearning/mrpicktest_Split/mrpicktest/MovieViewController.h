//
//  MovieViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 4/23/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MovieViewController : UIViewController<UIAlertViewDelegate>{
    NSString*movieNamePlayed;

}

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
- (IBAction)playMovie:(NSString*)sender;
@end