//
//  DocumentDirectory.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/30/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "DocumentDirectory.h"

@implementation DocumentDirectory

+(UIImage*) imageNamed:(NSString*) image{
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    NSLog(@"Image is %@",image);
    if (!(cId > 989 && cId < 3000)) {
        if (!image) {
          return  [UIImage imageNamed:@"1.png"];
        }
        return [UIImage imageNamed:[NSString stringWithFormat:@"%@/%@",ActiveGrade,image]];
    }

    if (cId > 999 & cId < 4000) {

//        NSString *imagePath = [[NSBundle mainBundle] pathForResource:[image substringToIndex:[image length]-4] ofType:@"png"];
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:image ofType:@"png"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL success = [fileManager fileExistsAtPath:imagePath];
        if (success) {
            return [UIImage imageWithContentsOfFile:imagePath];
        }
        else{
            [UIImage imageNamed:@"1"];
        }
    }
    

    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.png",ActiveGrade, image]];
    NSFileManager *fileManager = [NSFileManager defaultManager];    
    BOOL success = [fileManager fileExistsAtPath:filePath];
    if (!success) {
        if (cId > 989 && cId < 999) {
            return [UIImage imageNamed:@"2"];
        }
        return [UIImage imageNamed:@"1"];
    }
    return [[[UIImage alloc] initWithContentsOfFile:filePath] autorelease];
}


+(NSString*) playSound:(NSString*) soundName{
    
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];    
    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
//    if (!(cId > 989 && cId < 999)) {
    if (cId < 989 || cId > 4000) {
        return [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/%@",ActiveGrade,soundName] ofType:@"mp3"];
    }

    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* filePath;
    filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.caf",ActiveGrade, soundName]];
    
    if (cId > 989 && cId < 999) {
        filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.caf",ActiveGrade, soundName]];    
    }
    else {
        if (cId > 999 & cId < 4000) {
            
            NSString *filePath = [[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"];
            NSFileManager *fileManager = [NSFileManager defaultManager];
            BOOL success = [fileManager fileExistsAtPath:filePath];
            if (!success) {
               filePath = [[NSBundle mainBundle] pathForResource:@"EmptySound" ofType:@"mp3"];
            }
            return filePath;
//            filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.mp3",ActiveGrade, soundName]];
        }
    }

    
    NSFileManager *fileManager = [NSFileManager defaultManager]; 
    BOOL success = [fileManager fileExistsAtPath:filePath];
    if (!success) {
        filePath = [[NSBundle mainBundle] pathForResource:@"EmptySound" ofType:@"mp3"];
    }
    return filePath;
}


@end
