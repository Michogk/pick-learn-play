//
//  MyRewardTableViewCell.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 12/7/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MyRewardTableViewCell.h"
#import "MyRewardsViewController.h"
#import "CAURLDownload.h"

@implementation MyRewardTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        int indexPath = [[NSUserDefaults standardUserDefaults] integerForKey:@"RewardsCellNumber"];
        int tableViewNmbr = [[NSUserDefaults standardUserDefaults] integerForKey:@"RewardsTableNumber"];
        
        availableRewards = [[NSMutableArray alloc] init];
        totalPoints = 0;

//        availableRewards = [[MyRewardsViewController sharedInstance] _rewardsAvailable];       
        NSString *rewardsAvailableInMemory = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"availableRewards"]];
        [availableRewards removeAllObjects];
//        NSArray *componentsArr = [[NSArray alloc] init];
        NSArray* componentsArr = [[rewardsAvailableInMemory componentsSeparatedByString: @" "] retain];;           
        [availableRewards addObjectsFromArray:componentsArr];
        [componentsArr release];
        componentsArr = Nil;

        NSLog(@"Frest Available count is %d",[availableRewards count]) ;
        
        UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(-40, 40, 300, 222)];
        UIImageView *backgroundimgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,225 , 222)];
//        backgroundimgView.image = [UIImage imageNamed:@"game1.png"];
        [cellView addSubview:backgroundimgView];
        [backgroundimgView release];
        backgroundimgView = Nil;
        cellView.transform = CGAffineTransformMakeRotation(M_PI * 0.5); 
    
        UIImageView *rewardImgView = [[UIImageView alloc] initWithFrame:CGRectMake(40, 10,150 , 150)];
        [cellView addSubview:rewardImgView];
        
        UILabel *lblPointsRequired = [[UILabel alloc] initWithFrame:CGRectMake(50, 142, 130, 40)];
        lblPointsRequired.textAlignment = UITextAlignmentCenter;
        [lblPointsRequired setBackgroundColor:[UIColor clearColor]];
        UIFont *font = [UIFont systemFontOfSize: 13];
        [lblPointsRequired setFont:font];
        lblPointsRequired.text = @"Points Required : 150";
        [cellView addSubview:lblPointsRequired];
        [lblPointsRequired release];
        lblPointsRequired = Nil;

        UIButton *btnDownload = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btnDownload.tag = [[NSString stringWithFormat:@"%d%d",indexPath,tableViewNmbr] intValue];
        NSLog(@"Button Downloading Tag is %d",[[NSString stringWithFormat:@"%d%d",tableViewNmbr,indexPath] intValue]);
        [btnDownload addTarget:self 
                        action:@selector(downloadImage:)
              forControlEvents:UIControlEventTouchUpInside];
        [btnDownload setFrame:CGRectMake(50, 175, 130, 40)];
        [btnDownload setBackgroundColor:[UIColor clearColor]];
        [btnDownload setTitle:@"Download" forState:UIControlStateNormal];
        [cellView addSubview:btnDownload];
        
        if (tableViewNmbr == 1) {
            NSString *currentRewardImage = [availableRewards objectAtIndex:indexPath + 10];
            NSLog(@"currentRewardImage is |%@|",currentRewardImage);
            rewardImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", currentRewardImage]];            
        }
        else {
            NSString *currentRewardImage = [availableRewards objectAtIndex:indexPath];
            NSLog(@"currentRewardImage is |%@|",currentRewardImage);
            rewardImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",currentRewardImage]];
        }
        [self addSubview:cellView];   
        
        [rewardImgView release];
        rewardImgView = Nil;
        [cellView release];
        cellView = Nil;
        
    }
    return self;
}
-(void) downloadImage:(id) sender{
    

    for (int i = 1; i < 8; i ++) {
        NSString *PointsLevel1 = [NSString stringWithFormat:@"Game%dLevel%d",i,1];
        float level1Points = [[NSUserDefaults standardUserDefaults] integerForKey:PointsLevel1];     
        
        NSString *PointsLevel2 = [NSString stringWithFormat:@"Game%dLevel%d",i,2];
        float level2Points = [[NSUserDefaults standardUserDefaults] integerForKey:PointsLevel2]; 
        
        totalPoints = totalPoints + level1Points  + level2Points;
    }
    
    int alreadyDownloadedRewardsCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"rewardsDownloadedCount"];
        int alreadyPurchasedCostumersCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"CostumePurchasedCount"];
    totalPoints = totalPoints - (alreadyDownloadedRewardsCount * 150) - (alreadyPurchasedCostumersCount * 1000);
    NSLog(@" total points are %d",totalPoints);
    if (totalPoints < 150) {
    
        UIAlertView *alert = [[UIAlertView alloc] init];
        int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        if (activeLanguage == 2) {
            [alert setTitle:@"WARNING"]; 
            [alert setMessage:@"SORRY! You dont have sufficient points to download this image"];
            [alert addButtonWithTitle:@"Exit"];
        }
        else {
            [alert setTitle:@"اتحذير !"]; 
            [alert setMessage:@" لايوجد نقاط كافية لتحميل أية صور  "]; 
            [alert addButtonWithTitle:@"خروج"]; 
        }
 
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];        
        return;
    }
    
    
    int idnetifyTable = [sender tag] % 10;
    int indexPath = ([sender tag] - idnetifyTable) / 10;
    NSLog(@"Image to be downloaded belows to %d table and index PAth is %d",idnetifyTable,indexPath);
    if (idnetifyTable == 1) { // means table 2
        imageName = [availableRewards objectAtIndex:indexPath + 10];        
    }
    else {
        imageName = [availableRewards objectAtIndex:indexPath];
    }
    NSString *imageToBeDownloadedFromWeb = [NSString stringWithFormat:@"%@.png",imageName];     
        
    NSString *URL = [NSString stringWithFormat:@"http://www.mrpickit.net/rewards/%@",imageToBeDownloadedFromWeb];
    NSLog(@"URL is %@",URL);
        
    [CAURLDownload downloadURL:[NSURL URLWithString:URL]
                        target:self
                      selector:@selector(downloadFinished:conneciton:)
                  failSelector:@selector(downloadFailed:)
                      userInfo:nil];
}

- (void)downloadFinished:(NSData *)recievedData conneciton:(CAURLDownload *)connection {
    NSLog(@"Data is saved Successfully");
    NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];          
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@.png", documentsDirectory,imageName];
    NSURL *pathURLs= [NSURL fileURLWithPath:filePath];
    [recievedData writeToFile:filePath atomically:YES];
    [self addSkipBackupAttributeToItemAtURL:pathURLs];
    UIImageWriteToSavedPhotosAlbum([[[UIImage alloc] initWithContentsOfFile:filePath] autorelease], nil, nil, nil);
    
    // remove image from document dirctory to avoid lost of unnecessary memory
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager fileExistsAtPath:filePath];
    if (success) {
//        [fileManager removeItemAtPath: filePath error:NULL]; 
    }
    
    NSString *rewardsAvailableInMemory = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"availableRewards"]];
    
    NSLog(@"AVailable rewards are : %@",rewardsAvailableInMemory);
    
    NSString *updateRewardsStatus = [rewardsAvailableInMemory
                                    stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ",imageName ] withString:@""];
    if ([updateRewardsStatus isEqualToString:rewardsAvailableInMemory]) { // it means its last rewards with no space at end
        updateRewardsStatus = [rewardsAvailableInMemory
                               stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@" %@",imageName] withString:@""];
        if ([updateRewardsStatus isEqualToString:rewardsAvailableInMemory]) { // it means its last rewards with no space at end
            updateRewardsStatus = [rewardsAvailableInMemory
                                   stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@",imageName] withString:@""];
        }
    }
        NSLog(@"NEW AVailable rewards are : %@",updateRewardsStatus);
    [[NSUserDefaults standardUserDefaults] setObject:updateRewardsStatus forKey:@"availableRewards"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"updateRewardsTableView" object:self]];        
    
    int alreadyDownloadedRewardsCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"rewardsDownloadedCount"];
    if (!alreadyDownloadedRewardsCount) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"rewardsDownloadedCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        alreadyDownloadedRewardsCount = 0;
    }
    
    alreadyDownloadedRewardsCount = alreadyDownloadedRewardsCount + 1;
    [[NSUserDefaults standardUserDefaults] setInteger:alreadyDownloadedRewardsCount forKey:@"rewardsDownloadedCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];    
        
        UIAlertView *alert = [[UIAlertView alloc] init];
    int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 2) {
        [alert setTitle:@"CONGRATULATIONS"]; 
        [alert setMessage:@"Image has been saved in your iPad photo album."];
        [alert addButtonWithTitle:@"OK"];
    }
    else {
        [alert setTitle:@"انجار جيد"]; 
        [alert setMessage:@" تم تحميل الصور في مجموعة صورة الخاصة على جهازك  "]; 
        [alert addButtonWithTitle:@"خروج"]; 
    }

        [alert setDelegate:self];
        [alert setTag:3];
        [alert show];
        [alert release];        
        return;
    
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (void)downloadFailed:(CAURLDownload *)connection {
    UIAlertView *alert = [[UIAlertView alloc] init];
    int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 2) {
        [alert setTitle:@"WARNING"]; 
        [alert setMessage:@"SORRY! Downloading failed. May be try again some time"];
        [alert addButtonWithTitle:@"Exit"];
    }
    else {
        [alert setTitle:@"تحذير !"]; 
        [alert setMessage:@" يوجد مشكله في التحميل , الرجاء المحاوله مرة اخرى "]; 
        [alert addButtonWithTitle:@"خروج"]; 
    }

    [alert setDelegate:self];

    [alert setTag:2];
    [alert show];
    [alert release];        
    return;
    NSLog(@"Download Failed!");

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
