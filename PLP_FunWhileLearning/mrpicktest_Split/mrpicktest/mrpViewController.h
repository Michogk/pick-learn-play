//
//  mrpViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/13/12.
//  Copyright (c) 2012 School. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityMapViewController.h"
#import "MyCampController.h"
#import "MyPlayGroundViewController.h"
#import "MyHomeViewController.h"
#import <AudioToolbox/AudioToolbox.h>


@class CustomMoviePlayerViewController;

@interface mrpViewController : UIViewController<AVAudioPlayerDelegate> {
    
    IBOutlet UIButton *CityMap;
    IBOutlet UIButton *SetupBtn;
    
    CityMapViewController *CityMapView;
  
    MyCampController *MyCampView;
    MyPlayGroundViewController *MyPlayGroundView;
    MyHomeViewController *MyHomeView;

        AVAudioPlayer *myCampPlayer;
    	CustomMoviePlayerViewController *moviePlayer;
    BOOL repeatLaunch;
    
    
}
@property(nonatomic,retain) CityMapViewController *CityMapView;
- (IBAction)playVideo:(id)sender;

@property(nonatomic,retain ) MyCampController *MyCampView;
- (IBAction)exitMainMenu:(id)sender;
- (IBAction)introButtonPressed:(id)sender;


@end
