//
//  CustomMoviePlayerViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 12/18/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//
#import "CustomMoviePlayerViewController.h"

@implementation CustomMoviePlayerViewController

- (id)initWithPath:(NSString *)moviePath
{

    if (self = [super init])
    {
        NSString *encodedUrl = [moviePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        movieURL = [NSURL URLWithString:encodedUrl];
        [movieURL retain];
    }
    return self;
}

- (void) moviePlayerLoadStateChanged:(NSNotification*)notification 
{
    // Unless state is unknown, start playback
    if ([mp loadState] != MPMovieLoadStateUnknown)
    {
        // Remove observer
        [[NSNotificationCenter 	defaultCenter] 
         removeObserver:self
         name:MPMoviePlayerLoadStateDidChangeNotification 
         object:nil];
        
        // When tapping movie, status bar will appear, it shows up
        // in portrait mode by default. Set orientation to landscape
//        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
        
        // Rotate the view for landscape playback
        [[self view] setBounds:CGRectMake(0, 0, 1028, 768)];
        [[self view] setCenter:CGPointMake(768/2, 1028/2)];
        [[self view] setTransform:CGAffineTransformMakeRotation(-M_PI_2)]; 
        
        // Set frame of movieplayer
        [[mp view] setFrame:CGRectMake(0, 0, 1028, 768)];
        
        // Add movie player as subview
        [[self view] addSubview:[mp view]];   
        
        // Play the movie
        [mp play];
    }
}

- (void) moviePreloadDidFinish:(NSNotification*)notification 
{
    // Remove observer
    [[NSNotificationCenter 	defaultCenter] 
     removeObserver:self
     name:MPMoviePlayerLoadStateDidChangeNotification
     object:nil];
    
    // Play the movie
    [mp play];
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification 
{    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    // Remove observer
    [[NSNotificationCenter 	defaultCenter] 
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification 
     object:nil];
    
    [self dismissModalViewControllerAnimated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"videoTutorialDone" object:self]];
}

- (void) readyPlayer:(NSString*) movieName
{
    
    if ([movieName isEqualToString:@"loadFromURL"]) {
        NSLog(@"helpURL is %@",movieURL);
    }
    else{
        movieURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                           pathForResource:movieName ofType:@"m4v"]];
    }
    mp =  [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
    
    if ([mp respondsToSelector:@selector(loadState)]) 
    {
        // Set movie player layout
        [mp setControlStyle:MPMovieControlStyleFullscreen];
        [mp setFullscreen:YES];
        
        // May help to reduce latency
        [mp prepareToPlay];
        
        // Register that the load state changed (movie is ready)
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(moviePlayerLoadStateChanged:) 
                                                     name:MPMoviePlayerLoadStateDidChangeNotification 
                                                   object:nil];
    }  
    else
    {
        // Register to receive a notification when the movie is in memory and ready to play.
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(moviePreloadDidFinish:) 
                                                     name:MPMoviePlayerLoadStateDidChangeNotification 
                                                   object:nil];
    }
    
    // Register to receive a notification when the movie has finished playing. 
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(moviePlayBackDidFinish:) 
                                                 name:MPMoviePlayerPlaybackDidFinishNotification 
                                               object:nil];
}
- (void) loadView
{
    [self setView:[[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]] autorelease]];
    [[self view] setBackgroundColor:[UIColor blackColor]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)dealloc 
{
    [mp release];
//    [movieURL release];
    [super dealloc];
}

@end
