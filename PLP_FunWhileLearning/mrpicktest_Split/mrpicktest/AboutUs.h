//
//  AboutUs.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 12/18/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AboutUs : UIViewController{


}
@property (retain, nonatomic) IBOutlet UIButton *btnMrPickIt;
@property (retain, nonatomic) IBOutlet UIButton *btnSmartSoft;
- (IBAction)openMrPickIt:(id)sender;
- (IBAction)openSmartSoft:(id)sender;

@end
