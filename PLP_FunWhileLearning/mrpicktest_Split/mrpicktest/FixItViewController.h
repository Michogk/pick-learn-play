//
//  FixItViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 10/21/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <iAd/iAd.h>

@interface FixItViewController : UIViewController<AVAudioPlayerDelegate,ADBannerViewDelegate>{
    AVAudioPlayer *myFixItPlayer;
        AVAudioPlayer *player;
//    int _currentWordIndex;
    int cId;
    NSMutableDictionary *tempContentDic;
    NSMutableDictionary *_arabicWordDic;
    UILabel *cellText;
    UILabel *_wordCounterlbl;
    UILabel *_levelCounterlbl;
    
    NSMutableArray *_completeWordArray;
    NSMutableDictionary *characterDic;
    UIView *charactersView;
    NSString *_correctText;
    int yLine;
    int count;
    int currentWord;
    int level;
    NSArray *_englishExtraWordsArr;
    NSArray *_englishExtraCharactersArr;
    BOOL isSentence;
    int activeLanguage;
    
    NSTimer *soundTimer;
    
    IBOutlet UIButton *BtnCheckForCorrect;
    IBOutlet UIButton *BtnGoNext;
    IBOutlet UIImageView *backgroundImageFixIt;
    int cheeringAnimationCounter;
    int cheeringImageNumber;
    NSTimer *cheeringAnimationTimer;
    NSTimer *cheeringSoundTimer;
    NSTimer *characterWalkingTimer;
    UIImageView *cheeringImgView;
    UIImageView* characterWalkingAnimationImgView;
    int characterAnimationStep;
    int animationImageNumber;
    NSString *animationImageGender;
    NSString *animationImageDirection;
    int sessionCounter;
    int correctWordCounter;
    
    UILabel *_lblLevel1;
    UILabel *_lblLevel2;
    
    // Report Data
    NSMutableArray *retval;
    int reportWordId;
    NSString *reportWordDesc;
    int reportCorrectCount;
    int reportTotatCount;
    
}
- (IBAction)UndoLastStep:(id)sender;
@property (strong, nonatomic) ADBannerView *bannerView;
- (IBAction)CheckAnswer:(id)sender;
- (IBAction)playHelpSound:(id)sender;
- (IBAction)GoToNextWord:(id)sender;
- (IBAction)gotoMainMenu:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *lblSynonym;
@property (retain, nonatomic) IBOutlet UILabel *lblOpposite;


@end
