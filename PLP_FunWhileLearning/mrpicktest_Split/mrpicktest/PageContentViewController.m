//
//  BookViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/3/14.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import "PageContentViewController.h"
#import "DocumentDirectory.h"
#import "UIImage+animatedGIF.h"
#import <ImageIO/ImageIO.h>

@interface PageContentViewController ()

@end

@implementation PageContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([[self.imageFile substringToIndex:1] isEqualToString:@"g"]){
        NSString *imageName = [self.imageFile substringToIndex:[self.imageFile length] - 1];
        NSURL *url = [[NSBundle mainBundle] URLForResource:imageName withExtension:@"gif"];
        self.backgroundImageView.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    }
    else
    {
        self.backgroundImageView.image = [DocumentDirectory imageNamed:self.imageFile];
    }
    self.titleLabel.text = self.titleText;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
