//
//  MatchIt.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>

@interface MatchIt : UIViewController<AVAudioPlayerDelegate,ADBannerViewDelegate>{
    AVAudioPlayer *matchItPlayer;
    AVAudioPlayer *fireWorkPlayer;

    NSMutableDictionary *tempContentDic;
    int activeLanguage;
    int cId;    
    BOOL fistTimeLoad;
    NSTimer *moveToNextSetTimer;
    NSTimer *playFireWorksTimer;
    CALayer *myLayer;
    
    UILabel *_lblLevel1;
    UILabel *_lblLevel2;
    // Report Data
    NSMutableArray *retval;
    int reportCounter;
    
    int reportWordId01;
    NSString *reportWordDesc01;
    int reportCorrectCount01;
    int reportTotatCount01;
    
    int reportWordId02;
    NSString *reportWordDesc02;
    int reportCorrectCount02;
    int reportTotatCount02;
    
    int reportWordId03;
    NSString *reportWordDesc03;
    int reportCorrectCount03;
    int reportTotatCount03;
    
    int reportWordId04;
    NSString *reportWordDesc04;
    int reportCorrectCount04;
    int reportTotatCount04;
    
    BOOL resetCounter;
    

}

@property (retain, nonatomic) IBOutlet UIButton *btnWord1;
@property (retain, nonatomic) IBOutlet UIButton *btnWord2;
@property (retain, nonatomic) IBOutlet UIButton *btnWord3;
@property (retain, nonatomic) IBOutlet UIButton *BtnWord4;

@property (retain, nonatomic) IBOutlet UIButton *BtnImage1;
@property (retain, nonatomic) IBOutlet UIButton *BtnImage2;
@property (retain, nonatomic) IBOutlet UIButton *BtnImage3;
@property (retain, nonatomic) IBOutlet UIButton *BtnImage4;
- (IBAction)checkBtn:(id)sender;
- (IBAction)BtnNextPressed:(id)sender;
- (IBAction)btnResetPressed:(id)sender;
- (IBAction)btnGoToPlaygroundPressed:(id)sender;
- (IBAction)btnHelpURLPressed:(id)sender;
- (IBAction)btnHelpHear:(id)sender;

@property (retain, nonatomic) IBOutlet UIButton *btnCheck;
@property (retain, nonatomic) IBOutlet UIButton *btnReset;
@property (retain, nonatomic) IBOutlet UIButton *btnNext;
@property (retain, nonatomic) IBOutlet UIImageView *checkImg1;
@property (retain, nonatomic) IBOutlet UIImageView *checkImg2;
@property (retain, nonatomic) IBOutlet UIImageView *checkImg3;
@property (retain, nonatomic) IBOutlet UIImageView *checkImg4;
@property (strong, nonatomic) ADBannerView *bannerView;
@end
