//
//  BookStoreCells.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/15/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookStoreCells : UITableViewCell
{
    NSMutableArray* packageContentArray;
    NSString *activePackage;
    NSArray * downloadedPackArr;
    int activeLanguage;
    UIView *cellView;
//    UILabel *lblGrade;
}

@property(nonatomic, retain) NSArray * downloadedPackArr;
//@property(nonatomic, retain) UILabel *lblGrade;

@end
