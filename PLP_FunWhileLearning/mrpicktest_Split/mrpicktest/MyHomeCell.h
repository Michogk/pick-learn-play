//
//  MyHomeCell.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 23/09/2015.
//  Copyright (c) 2015 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyHomeCell : UITableViewCell
+(id) createCell;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImgView;
@property (retain, nonatomic) IBOutlet UIImageView *selectionImageView;
@property (retain, nonatomic) IBOutlet UILabel *titleLbl;

@end
