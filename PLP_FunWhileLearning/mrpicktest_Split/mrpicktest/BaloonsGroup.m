//
//  Baloons.m
//  KidsBubbleLearner
//
//  Created by Muhammad Mosib Asad on 6/24/12.
//  Copyright (c) 2012 GameView Studios. All rights reserved.
//

#import "BaloonsGroup.h"
#import "Balloon.h"
#import "GameLayer.h"

#define FIRST_BALOON 50
#define SECOND_BALOON 60
#define THIRD_BALOON 70

@implementation BaloonsGroup

+ (BaloonsGroup*) balloons: (NSString*) word
{
	return [[self alloc] initWithCorrectWord:word];
}

- (BaloonsGroup*) initWithCorrectWord:(NSString*) missingWord
{
    wordCorrect = missingWord;
    
    
    self.position = ccp(0, 0);
    
    
    // Adding arabicWords in wordList Array
    arabicWordListArr = [[GameLayer sharedLayer] getTempContentDic];

    int cWord = [[GameLayer sharedLayer] getcurrentWord];
    wordCorrect = [arabicWordListArr objectForKey:[NSString stringWithFormat:@"englishWord%d",cWord]];
    [self initializeLeftRightWords];
    
    int totalBalloons = 2;
    int correctWordPosition = arc4random() % totalBalloons;
    NSLog(@"Correct word position: %i and missing word is %@" , correctWordPosition,missingWord);
    
    
    switch (correctWordPosition) 
    {
        case 0:   /* Right word is at left */
            balloonLeft = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:NO Word:wordCorrect];            
//            balloonMiddle = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:NO Word:wordOne];
            balloonRight = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:YES Word:wordOne];
            break;
        case 1:   /* Right word is in middle */
            balloonLeft = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:YES Word:wordOne];            
//            balloonMiddle = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:YES Word:wordTwo];
            balloonRight = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:NO Word:wordCorrect];
            break;
        case 2:   /* Right word is in at right */
            balloonLeft = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:YES Word:wordOne];            
//            balloonMiddle = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:NO Word:wordTwo];
            balloonRight = [Balloon balloonWithType:[self getRandomBalloonType] IsCorrect:NO Word:wordCorrect];
            break;            
        default:
            break;
    }
    
    [self setBalloonsInitialPositions];
    
    balloonLeft.tag = FIRST_BALOON;          
//    balloonMiddle.tag = SECOND_BALOON;
    balloonRight.tag = THIRD_BALOON;
    
    if ((self = [CCMenu menuWithItems:balloonLeft, /*balloonMiddle,*/ balloonRight, nil])) 
    {
        NSLog(@"Balloons have been properly added");
    }
    return self;
}

- (void) setBalloonsInitialPositions
{
    CGPoint middlePos = ccp( 0, 400);
//    int bubbleDist = 50 + arc4random()% 250;
    int bubbleDist = 50 + 150;
    int randomYStart = -30;
    int randomYLimit = 60;
    int randomY = randomYStart + arc4random()%randomYLimit;
    
    CGPoint pos = ccp(middlePos.x - bubbleDist-20, middlePos.y+randomY);
    balloonLeft.position = pos;
    
    randomY = randomYStart + arc4random()%randomYLimit;

    bubbleDist = 80 + 110;

    pos = ccp(middlePos.x + bubbleDist+20, middlePos.y+randomY);
    balloonRight.position = pos;
    
}

- (void) initializeLeftRightWords
{
    wordOne = [self getRandomWord];
}

- (NSString*) getRandomWord
{
    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    NSLog(@"gettin random word");
    if (cId == 99999) {
        NSMutableDictionary *dummyDic = [[GameLayer sharedLayer] getDummyDic];  
        int dummyDicCount = [dummyDic count];
        NSLog(@"Random Word Limit is %d",[dummyDic count]);
        int randomLimitDummy = dummyDicCount/5;
        int Zy = 0;
            Zy = arc4random() % randomLimitDummy;
        NSLog(@"Zy is %d",Zy);
        NSString *randomWord = [dummyDic objectForKey:[NSString stringWithFormat:@"englishWord%d",Zy]];
        NSLog(@"random word fetched is %@",randomWord);
        if (!randomWord || randomWord == NULL) {
            NSLog(@"NULL scenerio");
            NSArray * dummyArray;
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"]== 1) {
                
                dummyArray = [[NSArray alloc] initWithObjects:@"كَتَبَ", @"وَلَدٌ رَائِعٌ", @"لا تَرْسُمْ", @"أُرْسُمْ", @"هناك",@"يَكْتُبُ", @"كُتُبٌ", @"يُسافِرُ", @"يُسافِرون", @"شَكَرْتُه",@"شَكَرْتُهم", @"شَكَرْتُهم", @"جِدًّا", @"آلَةٌ", @"الطَّعامِ",@"الْعُصْفورُ", @"فَراشاتٍ", @"ثَوْرٌ", @"بَبُطْءٍ", @"دَعوني",@"اُتْرُكوني", @"أُحِبُّهُ", @"يُراقِبُ", @"تَخْتَلِطُ", @"رِجالٌ",@"أُحِبُّهُ", @"مَنَحَ", @"خِلالِ", @"تَدورُ", @"ظَلَّ", nil];
                
            }
            else{
                dummyArray = [[NSArray alloc] initWithObjects:@"This", @"are", @"is", @"was", @"shall",@"This", @"it", @"would", @"could", @"may",@"can", @"shall", @"must", @"could", @"swim",@"play", @"think", @"go", @"come", @"boldly",@"bravely", @"are", @"is", @"was", @"shall",@"This", @"it", @"would", @"could", @"may", nil];
                
            }
            
            randomWord = [dummyArray objectAtIndex:Zy];
            if (!randomWord || randomWord == NULL) {
                randomWord = @"the";
            }
            [dummyArray release];
            
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"]== 1 && randomWord == NULL) {
                randomWord = @"طاولة";
            }
        }
        return randomWord;
    }
    

    
    
    
    int cWord = [[GameLayer sharedLayer] getcurrentWord];
    NSLog(@"arabic Word Count is %d",[arabicWordListArr count]/5);
    if (!cWord /* || cWord + 1 == [arabicWordListArr count]/5*/ || cWord > [arabicWordListArr count]) {
        cWord = 0;
    }
    
    int randomLimit = [arabicWordListArr count]/5;
//    int x = arc4random() % randomLimit;
//    if (x == cWord) {
//        x = arc4random() % randomLimit;
//    }
    
    int x;
    NSString *randomWord;
    do {
        x = arc4random() % randomLimit;
        randomWord = [arabicWordListArr objectForKey:[NSString stringWithFormat:@"englishWord%d",x]];
    } while ((x == cWord) && ([randomWord isEqualToString:wordCorrect]));
    
    randomWord = [arabicWordListArr objectForKey:[NSString stringWithFormat:@"englishWord%d",x]];
    return randomWord;
}

- (BalloonTypes) getRandomBalloonType
{
    return arc4random() % BalloonType_MAX;
}

- (void) stopAllBalloons
{
    [balloonLeft stop];
//    [balloonMiddle stop];
    [balloonRight stop];
}

- (void) dealloc
{
	[super dealloc];
}

@end
