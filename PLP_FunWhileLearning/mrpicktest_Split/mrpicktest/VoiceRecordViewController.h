//
//  VoiceRecordViewController.m
//  mrpicktest
//
//  Created by Muhammad Mosib Asad on 09/29/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface VoiceRecordViewController : UIViewController <AVAudioRecorderDelegate,AVAudioPlayerDelegate>
{
            AVAudioPlayer *player;
	IBOutlet UIProgressView *progressView;
	IBOutlet UILabel *lblStatusMsg;
	
	NSMutableDictionary *recordSetting;
	NSMutableDictionary *editedObject;
	NSString *recorderFilePath;
	AVAudioRecorder *recorder;
	
	SystemSoundID soundID;
	NSTimer *timer;
}
- (IBAction)CancelRecording:(id)sender;

- (IBAction) startRecording;
- (IBAction) stopRecording;
- (IBAction)playSound;
- (void) handleTimer;

@end

