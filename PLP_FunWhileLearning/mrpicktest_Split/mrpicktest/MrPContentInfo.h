//
//  MrPContentInfo.h
//  Mr.Pick
//
//  Created by Muhammad Mosib Asad on 9/14/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface MrPContentInfo : NSObject{
    
    
    int _wordId;
    NSString *_englishWord;
    int _arabicRef;
    int _isSentence;
    int _grammerId;
    int _classificationId;
    int _imageId;
    NSString* _voiceId;
    NSString* _descWord;
    int _subClassificationId;
    NSString *_arabicWord;    
    int _grammarId;
    NSString* _grammarWord;
    int _principleId;
    int _groupId;    
    NSString *_principleWord;
    NSString *_synonymWord;
    NSString *_oppositeWord;
    int _langId;
    int _principleVoiceId;
    int _arabicVoiceid;
    
    int folderid;
    NSString *foldername;
    int _translate;
    int _english_ref;
}

@property (nonatomic, assign) int wordId;
@property (nonatomic, assign) int translate;
@property (nonatomic, assign) int arabicVoiceid;
@property (nonatomic, copy) NSString *englishWord;
@property (nonatomic, assign) int arabicRef;
@property (nonatomic, assign) int isSentence;
@property (nonatomic, assign) int classificationId;
@property (nonatomic, assign) int imageId;
@property (nonatomic, copy) NSString* voiceId;
@property (nonatomic, copy) NSString *descWord;
@property (nonatomic, copy) NSString *arabicWord;

@property (nonatomic, copy) NSString *synonymWord;
@property (nonatomic, copy) NSString *oppositeWord;
@property (nonatomic, assign) int subClassificationId;

@property (nonatomic, copy) NSString *grammarWord;
@property (nonatomic, copy) NSString *principleWord;
@property (nonatomic, assign) int grammarId;
@property (nonatomic, assign) int principleId;
@property (nonatomic, assign) int groupId;
@property (nonatomic, assign) int langId;
@property (nonatomic, assign) int english_ref;
@property (nonatomic, assign) int principleVoiceId;
@property (nonatomic, assign) int folderid;
@property (nonatomic, copy) NSString *foldername;


- (id)initWithName:(int ) fid :(NSString *) fname;
- (id) initWithClassificationIds:(int)classifications;
- (id)initWithWordId:(int )word andVoiceId :(NSString *)voice andfWordname:(NSString*)word andLangDir:(int) langDir andImageId:(int)imgId;
- (id) initWithWordId:(int)wordId englishWord:(NSString *)englishWord arabicRef:(int )arabicRef 
           isSentence:(int )isSentence imageId:(int)imageId voiceId:(NSString* )voiceId translation:(int) translation;
-(id) initWithClassificationId:(int )classificationId arabicWord:(NSString *)descWord subId:(int )subClassificationId;
-(id) initWithWordId:(int )wordId arabicWord:(NSString *)arabicWord isSentence:(int )isSentence arabicVoiceId:(NSString* ) arabicVoiceId imageId:(int) imageId translate:(int) translate andEnglishRef:(int) englishRefrence andSynonym:(NSString*) synonym andOppositeWord:(NSString*) opposite;
-(id) initWithGrammarId:(int )grammarId grammarWord:(NSString *)grammarWord principlesId:(int )principlesId groupId:(int )groupId;
-(id) initWithGrammarId:(int )grammarId grammarWord:(NSString *)grammarWord;
-(id) initWithPrincipleId:(int )principlesId principleWord:(NSString *)principleWord LangId:(int )LangId voiceId:(int )voiceId;
-(id) initWithWordId:(int )wordId arabicWord:(NSString *)arabicWord arabicVoiceId:(NSString* ) arabicVoiceId imageId:(int) imageId;
-(id) initWithWordId:(int )wordId arabicWord:(NSString *)arabicWord isSentence:(int )isSentence arabicVoiceId:(NSString* ) arabicVoiceId imageId:(int) imageId translate:(int)translate andEnglishRef:(int) englishRefrence andSynonym:(NSString*) synonym andOppositeWord:(NSString*) opposite andPrincipleId:(int) principleId andGrammarId:(int) grammarId andPrincipleName:(NSString*)principleName andGrammarName:(NSString*) grammarName;
- (id)initWithimageId:(int)imageId classificationId:(int )classificationId andClassificationName:(NSString*) name;
- (id)initWithimageId:(int)imageId classificationId:(int )classificationId;
@end
