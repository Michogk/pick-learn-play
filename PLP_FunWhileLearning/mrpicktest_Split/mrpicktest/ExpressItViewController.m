//
//  ExpressItViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 9/20/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "ExpressItViewController.h"
#import "MrPDatabase.h"
#import "DocumentDirectory.h"
#import "SimpleAudioEngine.h"
#import "MrPReport.h"
#import "MrPContentInfo.h"
#import "ExpressItSetOrderViewController.h"
#import "ExpressAnswersPopUp.h"

#define EXPRESSIT 11
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

@interface ExpressItViewController ()

@end

@implementation ExpressItViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self playBackGroundMusic];
    
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    activeGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    currentWord = 0;
    
    [self loadDatabase];
    
    if ([tempContentDic count]/5 < 3) {
        UIAlertView *alerts = [[UIAlertView alloc] init];
        [alerts setTitle:@"In Sufficient Data"];
        [alerts setMessage:@"Please go to Main Menu and select another location"];
        [alerts setDelegate:self];
        [alerts addButtonWithTitle:@"OK"];
        [alerts setTag:12];
        [alerts show];
        [alerts release];
        return;
    }
    
    [self initializeContent];
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"5-MatchIT"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    expressItBackgroundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    expressItBackgroundPlayer.volume = 0.35;
    expressItBackgroundPlayer.numberOfLoops = 20;
    expressItBackgroundPlayer.delegate = self;
    
    if ([expressItBackgroundPlayer prepareToPlay]) {
        [expressItBackgroundPlayer play];
    }
}

-(void) initializeContent{
    
//    [_btnNext setEnabled: NO];    
    
//    NSString *wordsCounter = [NSString stringWithFormat:@"Game%dClassification%dlanguage%dWordsCount%@",EXPRESSIT,cId,activeLanguage,activeGrade];
//    currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCounter];
//    NSLog(@"Temp count is %d and current word is %d",[tempContentDic count]/5, currentWord);
    
    if (currentWord  > [tempContentDic count]/5)
    {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        ExpressItSetOrderViewController *setOrderVC= [sb instantiateViewControllerWithIdentifier:@"ExpressItSetOrderIdentifier"];
        setOrderVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:setOrderVC animated:NO completion:NULL];
    }
    else{

    }
    
    
//    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCounter];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self loadGameElements];
}

-(void) loadGameElements{
    

    if (currentWord < [tempContentDic count]/ 5) {
        [_btnFirstImage setBackgroundImage:[DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",currentWord]]]] forState:UIControlStateNormal];
    }
    else if(currentWord == [tempContentDic count] / 5 ){
        [_btnFirstImage setBackgroundImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    }
    else if(currentWord > [tempContentDic count] / 5 ){
        [_btnFirstImage setBackgroundImage:nil forState:UIControlStateNormal];
    }


    
    if (currentWord + 1 < [tempContentDic count] / 5) {
            [_btnSecondImg setBackgroundImage:[DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",currentWord+1]]]] forState:UIControlStateNormal];
    }
    else if(currentWord + 1 == [tempContentDic count] / 5 ){
        [_btnSecondImg setBackgroundImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];

    }
    else if(currentWord + 1 > [tempContentDic count] / 5 ){
        [_btnSecondImg setBackgroundImage:nil forState:UIControlStateNormal];
    }
    
    if (currentWord + 2 < [tempContentDic count] / 5) {
        [_btnThirdImg setBackgroundImage:[DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",currentWord+2]]]] forState:UIControlStateNormal];
    }
    else if(currentWord + 2 == [tempContentDic count] / 5 ){
        [_btnThirdImg setBackgroundImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    }
    else if(currentWord + 2 > [tempContentDic count] / 5 ){
        [_btnThirdImg setBackgroundImage:nil forState:UIControlStateNormal];
    }

    
    
}

-(void) loadDatabase{
    int position = 0;
    // Loaading data  from database and saving in dictationaries
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    NSArray *MrPContentArray;

    if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
        MrPContentArray = [database EnglishWordDatabaseArray:cId];
    }
    else {
        MrPContentArray = [database ArabicWordDatabaseArray:cId];
    }
    NSLog(@"data is loaded successfully and array count is %d",[MrPContentArray count]);
    tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    for (MrPContentInfo *info in MrPContentArray) {
        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]]
        ;
    if (activeLanguage == 2 || (cId > 989 && cId < 999))
        {
            [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        else
        {
            [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
        position++;
        }
//    }
    position = 0;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_btnFirstImage release];
    [_btnSecondImg release];
    [_btnThirdImg release];
    [_btnNext release];
    [_firstProgessView release];
    [_secondProgressView release];
    [_thirdProgressView release];
    [_lblRecordingStatusFirst release];
    [_lblRecordingStatusTwo release];
    [_lblRecordingStatusThree release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setBtnFirstImage:nil];
    [self setBtnSecondImg:nil];
    [self setBtnThirdImg:nil];
    [self setBtnNext:nil];
    [self setFirstProgessView:nil];
    [self setSecondProgressView:nil];
    [self setThirdProgressView:nil];
    [self setLblRecordingStatusFirst:nil];
    [self setLblRecordingStatusTwo:nil];
    [self setLblRecordingStatusThree:nil];
    [super viewDidUnload];
}
- (IBAction)BtnFirstImagePressed:(id)sender {
    NSString *soundId = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
    if (!soundId || soundId == NULL) {
        soundId = @"EmptySound";
    }
    [self playSound:soundId];
}

- (IBAction)btnSecondImgPressed:(id)sender {
    NSString *soundId = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord+1]];
    if (!soundId || soundId == NULL) {
        soundId = @"EmptySound";
    }
    [self playSound:soundId];

}

- (IBAction)btnThirdImgPressed:(id)sender {
    NSString *soundId = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord+2]];
    if (!soundId || soundId == NULL) {
        soundId = @"EmptySound";
    }
    [self playSound:soundId];
}

-(void) playSound:(NSString*) soundName{
    if (wordPlayer) {
        [wordPlayer stop];
        [wordPlayer release];
        wordPlayer = Nil;
    }
    

    NSString *squishPath = [DocumentDirectory playSound:soundName];
    if ([soundName isEqualToString:@"EmptySound"]) {
        squishPath = [[NSBundle mainBundle] pathForResource:@"EmptySound" ofType:@"mp3"];
    }
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    wordPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    wordPlayer.numberOfLoops = 0;
    wordPlayer.delegate = self;
    [wordPlayer play];
}

-(NSString*) createAndStoreFile:(NSString*) fileName{

    NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
    activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString *folderNAme = [[NSUserDefaults standardUserDefaults] objectForKey:@"folderName"];
    NSLog(@"folderName is %@",folderNAme);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    if (!pinNumber || pinNumber == NULL) {
        pinNumber = @"1";
    }
    if ((!folderNAme ) || folderNAme == NULL) {
        folderNAme = @"Express It";
    }
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@-%@",activeGrade,pinNumber,pinNumber,folderNAme]];
    NSError *error;
    BOOL isDir = NO;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath  isDirectory:&isDir]){
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error];
        NSLog(@"Trying to create doc directory");
     }
    if (!isDir)
    {
        NSLog(@"Cannot proceed!");
        // Throw exception
    }
    recorderFilePath = [[NSString stringWithFormat:@"%@/%@.caf", dataPath, fileName] retain];
    return recorderFilePath;
}

- (IBAction)btnStartRecrodingFirstImgAnswer:(id)sender {
    NSLog(@"start Recording");
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    
    NSLog(@"#Record Setting");
    recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];
    
    // We can use 44100, 32000, 24000, 16000 or 12000 depending on sound quality
    [recordSetting setValue:[NSNumber numberWithFloat:16000.0] forKey:AVSampleRateKey];
    
    
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    
    
    NSString *voiceId = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord]] ];

    
    NSString *newString = [voiceId stringByReplacingOccurrencesOfString:@" " withString:@""];

    if (cId > 989 && cId < 999 && activeLanguage == 1) {
        int folderId = cId - 990;
        newString = [NSString stringWithFormat:@"Folder %d - Word %d",folderId,currentWord+1];
    }

    activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    
    recorderFilePath = [self createAndStoreFile:newString];
    
    
//    recorderFilePath = [[NSString stringWithFormat:@"%@/%@/%@.caf", DOCUMENTS_FOLDER,activeGrade,newString] retain];
    
    NSLog(@"#recorderFilePath: %@",recorderFilePath);
    
    NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
    
    err = nil;
    
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if(audioData)
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[url path] error:&err];
    }
    
    err = nil;
    recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
    if(!recorder){
        NSLog(@"#recorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    
    //prepare to record
    [recorder setDelegate:self];
    [recorder prepareToRecord];
    recorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        [cantRecordAlert release];
        return;
    }
    
    // start recording
    [recorder recordForDuration:(NSTimeInterval) 10];
    //            lblStatusMsg.font = [UIFont fontWithName:@"Arial" size:22];
    UIFont *font = [UIFont systemFontOfSize: 22];
    [_lblRecordingStatusFirst setFont:font];
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        _lblRecordingStatusFirst.text = @"Recording...";
    }
    else {
        _lblRecordingStatusFirst.text = @"تسجيل...";
        
    }
    
    _firstProgessView.progress = 0.0;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(handleTimer1) userInfo:nil repeats:YES];
}

- (IBAction)btnStartRecrodingSecondImgAnswer:(id)sender {
    NSLog(@"start Recording");
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    
    NSLog(@"#Record Setting");
    recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];
    
    // We can use 44100, 32000, 24000, 16000 or 12000 depending on sound quality
    [recordSetting setValue:[NSNumber numberWithFloat:16000.0] forKey:AVSampleRateKey];
    
    
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    
    
    NSString *voiceId = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord+1]] ];

    NSString *newString = [voiceId stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (cId > 989 && cId < 999 && activeLanguage == 1) {
        int folderId = cId - 990;
        newString = [NSString stringWithFormat:@"Folder %d - Word %d",folderId,currentWord+2];
    }
    activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
        recorderFilePath = [self createAndStoreFile:newString];
//    recorderFilePath = [[NSString stringWithFormat:@"%@/%@/%@.caf", DOCUMENTS_FOLDER,activeGrade,newString] retain];
    
    NSLog(@"#recorderFilePath: %@",recorderFilePath);
    
    NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
    
    err = nil;
    
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if(audioData)
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[url path] error:&err];
    }
    
    err = nil;
    recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
    if(!recorder){
        NSLog(@"#recorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    
    //prepare to record
    [recorder setDelegate:self];
    [recorder prepareToRecord];
    recorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        [cantRecordAlert release];
        return;
    }
    
    // start recording
    [recorder recordForDuration:(NSTimeInterval) 10];
    //            lblStatusMsg.font = [UIFont fontWithName:@"Arial" size:22];
    UIFont *font = [UIFont systemFontOfSize: 22];
    [_lblRecordingStatusTwo setFont:font];
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        _lblRecordingStatusTwo.text = @"Recording...";
    }
    else {
        _lblRecordingStatusTwo.text = @"تسجيل...";
        
    }
    
    _secondProgressView.progress = 0.0;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(handleTimer2) userInfo:nil repeats:YES];
}

- (IBAction)btnStartRecrodingThirdImgAnswer:(id)sender {
    NSLog(@"start Recording");
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    
    NSLog(@"#Record Setting");
    recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];
    
    // We can use 44100, 32000, 24000, 16000 or 12000 depending on sound quality
    [recordSetting setValue:[NSNumber numberWithFloat:16000.0] forKey:AVSampleRateKey];
    
    
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    
    
    NSString *voiceId = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord+2]] ];

    NSString *newString = [voiceId stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (cId > 989 && cId < 999 && activeLanguage == 1) {
        int folderId = cId - 990;
        newString = [NSString stringWithFormat:@"Folder %d - Word %d",folderId,currentWord+3];
    }
    activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    recorderFilePath = [self createAndStoreFile:newString];

    
    NSLog(@"#recorderFilePath: %@",recorderFilePath);
    
    NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
    
    err = nil;
    
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if(audioData)
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[url path] error:&err];
    }
    
    err = nil;
    recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
    if(!recorder){
        NSLog(@"#recorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    
    //prepare to record
    [recorder setDelegate:self];
    [recorder prepareToRecord];
    recorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        [cantRecordAlert release];
        return;
    }
    
    // start recording
    [recorder recordForDuration:(NSTimeInterval) 10];
    //            lblStatusMsg.font = [UIFont fontWithName:@"Arial" size:22];
    UIFont *font = [UIFont systemFontOfSize: 22];
    [_lblRecordingStatusThree setFont:font];
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        _lblRecordingStatusThree.text = @"Recording...";
    }
    else {
        _lblRecordingStatusThree.text = @"تسجيل...";
        
    }
    
    _thirdProgressView.progress = 0.0;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(handleTimer3) userInfo:nil repeats:YES];
}

- (IBAction)btnStopRecordingFirstImgAnswer:(id)sender {
    [recorder stop];
    
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
    _lblRecordingStatusFirst.text = @"Stopped";
    _firstProgessView.progress = 1.0;
    AudioSessionSetActive(NO);
    
    
}

- (IBAction)btnStopRecordingSecondImgAnswer:(id)sender {
    [recorder stop];
    
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
    _lblRecordingStatusTwo.text = @"Stopped";
    _secondProgressView.progress = 1.0;
    AudioSessionSetActive(NO);
}

- (IBAction)btnStopRecordingThirdImgAnswer:(id)sender {
    [recorder stop];
    
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
    _lblRecordingStatusThree.text = @"Stopped";
    _thirdProgressView.progress = 1.0;
    AudioSessionSetActive(NO);
}

- (void) handleTimer1
{
    _firstProgessView.progress += .07/5;
    if(_firstProgessView.progress == 1.0)
    {
        [timer invalidate];
        timer = nil;
        _lblRecordingStatusFirst.text = @"Stopped";
    }
    recorderFilePath = Nil;
}

- (void) handleTimer2
{
    _secondProgressView.progress += .07/5;
    if(_secondProgressView.progress == 1.0)
    {
        [timer invalidate];
        timer = nil;
        _lblRecordingStatusTwo.text = @"Stopped";
    }
    recorderFilePath = Nil;
}

- (void) handleTimer3
{
    _thirdProgressView.progress += .07/5;
    if(_thirdProgressView.progress == 1.0)
    {
        [timer invalidate];
        timer = nil;
        _lblRecordingStatusThree.text = @"Stopped";
    }
    recorderFilePath = Nil;
}


- (IBAction)btnListenFirstAnswer:(id)sender {
//    if(!recorderFilePath){
    
    
    NSString *voiceIdWord = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord]] ];

    NSString *voiceId = [voiceIdWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (cId > 989 && cId < 999 && activeLanguage == 1) {
        int folderId = cId - 990;
        voiceId = [NSString stringWithFormat:@"Folder %d - Word %d",folderId,currentWord+1];
    }
    
    [self createAndStoreFile:voiceId]; 
    if(soundID)
    {
        AudioServicesDisposeSystemSoundID(soundID);
    }
    
    //Get a URL for the sound file
    NSURL *filePath = [NSURL fileURLWithPath:recorderFilePath isDirectory:NO];

    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
    
    
}

- (IBAction)btnListenSecondAnswer:(id)sender {
//    if(!recorderFilePath){
    NSString *voiceIdWord = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord+1]] ];

    NSString *voiceId = [voiceIdWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (cId > 989 && cId < 999 && activeLanguage == 1) {
        int folderId = cId - 990;
        voiceId = [NSString stringWithFormat:@"Folder %d - Word %d",folderId,currentWord+2];
    }
        activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
        [self createAndStoreFile:voiceId];
//        recorderFilePath = [[NSString stringWithFormat:@"%@/%@/%@.caf", DOCUMENTS_FOLDER,activeGrade,voiceId] retain];
//    }
    
    if(soundID)
    {
        AudioServicesDisposeSystemSoundID(soundID);
    }
    
    //Get a URL for the sound file
    NSURL *filePath = [NSURL fileURLWithPath:recorderFilePath isDirectory:NO];
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
}

- (IBAction)btnListenThirdAnswer:(id)sender {
//    if(!recorderFilePath){
    NSString *voiceIdWord = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord+2]] ];

    NSString *voiceId = [voiceIdWord stringByReplacingOccurrencesOfString:@" " withString:@""];
        activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    if (cId > 989 && cId < 999 && activeLanguage == 1) {
        int folderId = cId - 990;
        voiceId = [NSString stringWithFormat:@"Folder %d - Word %d",folderId,currentWord+3];
    }
        [self createAndStoreFile:voiceId];
//        recorderFilePath = [[NSString stringWithFormat:@"%@/%@/%@.caf", DOCUMENTS_FOLDER,activeGrade,voiceId] retain];
//    }
    
    if(soundID)
    {
        AudioServicesDisposeSystemSoundID(soundID);
    }
    
    //Get a URL for the sound file
    NSURL *filePath = [NSURL fileURLWithPath:recorderFilePath isDirectory:NO];
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
}

- (IBAction)btnNextPressed:(id)sender {
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    currentWord = currentWord + 3;
    

    
    
    [self initializeContent];
}
- (IBAction)gotoMainMenu:(id)sender {
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
}

- (IBAction)btnBackPressed:(id)sender {
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    if (currentWord-3 >= 0) {
        currentWord = currentWord - 3;
    }
    [self initializeContent];
    
}
- (IBAction)actionAnswerTypingTwo:(id)sender {
    [self UpdateAnswer:currentWord+1];
}

- (IBAction)actionAnswerTypingThree:(id)sender {
    [self UpdateAnswer:currentWord+2];
}

- (IBAction)actionAnswerTypingOne:(id)sender {
    [self UpdateAnswer:currentWord];
}

-(void) UpdateAnswer:(int) _currentWord{
    NSString *voiceId = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",_currentWord]] ];
    
    
    NSString *newString = [voiceId stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (cId > 989 && cId < 999 && activeLanguage == 1) {
        int folderId = cId - 990;
        newString = [NSString stringWithFormat:@"Folder %d - Word %d",folderId,currentWord+1];
    }
    
    popup = [ExpressAnswersPopUp mainView];
    popup.fileName = newString;
    [popup showOnView:self.view withDelegate:self];

}
#pragma mark - Rights View Submitted
- (void) RightsViewSubmitted:(ExpressAnswersPopUp*)popUp{

}
@end
