//
//  FixItViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 10/21/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "FixItViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "SimpleAudioEngine.h"
#import "HelpSoundViewController.h"
#import "DocumentDirectory.h"
#import "MrPReport.h"
#import "Common.h"
#define FIXIT 2

@interface FixItViewController ()

@end

@implementation FixItViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];

    [self insitiantiateForReportCouting];
    
    
    
    
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    [self playBackGroundMusic];
    [self loadDatabase];
    
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],FIXIT,cId,activeLanguage];
    int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    if (LevelLastCount > 5) {
        
        NSLog(@"Level is greater then five");
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Game Ends"]; 
        [alert setMessage:@"You have completed this location. Please select other location from MAP"];
        [alert setDelegate:self];
        [alert addButtonWithTitle:@"Exit"];       
        [alert setTag:2];
        [alert show];
        [alert release];
        return;
    }
    sessionCounter = 0;
    correctWordCounter = 0;
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 1) { // in case of arabic
         
        _englishExtraWordsArr = [[NSArray alloc] initWithObjects:@"طاولة", @"طاولة", @"الشَمْسُ", @"أُرْسُمْ", @"يَكْتُبُ",@"طاولة", @"طاولة", @"الشَمْسُ", @"أُرْسُمْ", @"يَكْتُبُ",
                                 @"على", @"أسود", @"كبير", @"أتعلم", @"ألوان",@"فهد", @"قمر", @"شارع", @"بني", @"جدة",
                                 @"فهد", @"ثعلب", @"أذن", @"أرنب", @"أرض",@"ضخم", @"أحمد", @"أزرق", @"جدة", @"ثعلب",
                                 @"شعر", @"جمل", @"كبير", @"قلعة", @"تمر",@"ثوب", @"صديق", @"بني", @"طيور", @"حوت",
                                 nil];
        
        
        _englishExtraCharactersArr = [[NSArray alloc] initWithObjects:@"و", @"ط", @"ا", @"سُ", @"كْ",@"و", @"ط", @"ا", @"سُ", @"كْ",
                                      @"أ", @"نـ", @"جـ", @"ح", @"خ",@"ـبـ", @"حـ", @"بُ", @"ـأ",
                                      @"حُ", @"ـتـ", @"ح", @"صَ", @"شَ",@"طَ", @"ـد", @"ذ", @"دَ",
                                      @"تْ", @"ـخـ", @"جْ", @"رْ", @"شْ",@"سُ", @"صُ", @"ـزـ", @"رُ", @"جُ",
                                      @"صْ",@"ضُ", @"طِ", @"صَ",  @"ـت", @"رُ", @"ـه", @"خِ", @"مِ",@"جـ", @"دـ", @"ثَ", @"ث", @"ـزـ",nil];
    }
    else {
        _englishExtraWordsArr = [[NSArray alloc] initWithObjects:@"This", @"Sand", @"is", @"them", @"play",@"football", @"food", @"fire", @"book", @"story",
                                 @"car", @"fly", @"air", @"water", @"earth",@"wear", @"socks", @"and", @"one", @"three",
                                 @"two", @"four", @"seven", @"born", @"building",@"green", @"brown", @"red", @"basketball", @"color",
                                 @"spin", @"balloon", @"water", @"aeroplane", @"house",@"lawn", @"ambulance", @"hapy", @"sad", @"fast",
                                 nil];
        _englishExtraCharactersArr = [[NSArray alloc] initWithObjects:@"T", @"a", @"s", @"e", @"m",@"T", @"i", @"s", @"e", @"m",
                                      @"a", @"b", @"c", @"d", @"e",@"f", @"g", @"h", @"i", @"j",
                                      @"T", @"s", @"r", @"q", @"p",@"o", @"n", @"m", @"l", @"k",
                                      @"u", @"v", @"w", @"w", @"y",@"z", @"i", @"s", @"e", @"m",nil];
    }


//    if (cId == 99999) {
//        UIAlertView *alert = [[UIAlertView alloc] init];
//        int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//        
//        if (activeLanguage == 2) {
//            [alert setTitle:@"WARNING"]; 
//            [alert setMessage:@"SORRY! This game doesnot support current Grammar Rule"];
//            [alert setDelegate:self];
//            [alert addButtonWithTitle:@"Exit"];   
//        }
//        else {
//            [alert setTitle:@"تحذير !"]; 
//            [alert setMessage:@"عفوا , هذة اللعبه غير متوفره في القواعد "];
//            [alert setDelegate:self];
//            [alert addButtonWithTitle:@"خروج"];
//        }
//
//        [alert setTag:2];
//        [alert show];
//        [alert release];
//        return;
//    }
//    else 
//    {
        if ([tempContentDic count] < 2)
        {            
            UIAlertView *alert = [[UIAlertView alloc] init];
            int  activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
            
            if (activeLanguage == 1) {
                [alert setTitle:@"تحذير !"]; 
                [alert setMessage:@"االبيانات غير كافية الرجاء التأكد ان يكون اختيارك صحيحاً"];
                [alert addButtonWithTitle:@"موافق"];
            }
            else {
                [alert setTitle:@"WARNING"]; 
                [alert setMessage:@"SORRY! No Sufficient Data. Please Make Sure that your selection is correct."];
                [alert addButtonWithTitle:@"OK"];  
            }
            [alert setDelegate:self];
            [alert setTag:2];
            [alert show];
            [alert release];
            return;
//        }
    }

    
    [self initializeContent];
    [self charactersInteraction];
    
    // Label where characters/words will be dragged
    _completeWordArray = [[NSMutableArray alloc] initWithCapacity:60];
    cellText = [[UILabel alloc] initWithFrame:CGRectMake(50, 220, 890, 70)];
//    [cellText setFont:[UIFont fontWithName:@"Arial" size:35]];
    UIFont *font = [UIFont systemFontOfSize: 50];
    [cellText setFont:font];
    cellText.textAlignment = UITextAlignmentCenter;
    [cellText setBackgroundColor:[UIColor lightTextColor]];
    [self.view addSubview:cellText]; 

    // Word Counter Label    
    _wordCounterlbl = [[UILabel alloc] initWithFrame:CGRectMake(650, 30, 300, 30)];
    NSString *currentNumber = [NSString stringWithFormat:@"Current Word : %d / %d",currentWord + 1, [tempContentDic count]/8];
    [_wordCounterlbl setText:currentNumber];
//    [_wordCounterlbl setFont:[UIFont fontWithName:@"Arial" size:21]];
    font = [UIFont systemFontOfSize: 21];
    [_wordCounterlbl setFont:font];
    _wordCounterlbl.textAlignment = UITextAlignmentCenter;
    [_wordCounterlbl setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_wordCounterlbl]; 
    
    // Level Counter Label
    _levelCounterlbl = [[UILabel alloc] initWithFrame:CGRectMake(650, 70, 300, 30)];

    NSString *currentLevel = [NSString stringWithFormat:@"Level : %d",LevelLastCount];;
    [_levelCounterlbl setText:currentLevel];
//    [_levelCounterlbl setFont:[UIFont fontWithName:@"Arial" size:21]];
    font = [UIFont systemFontOfSize: 21];
    [_levelCounterlbl setFont:font];
    _levelCounterlbl.textAlignment = UITextAlignmentCenter;
    [_levelCounterlbl setBackgroundColor:[UIColor clearColor]];
//    [self.view addSubview:_levelCounterlbl];
    
    NSString *points1 = [NSString stringWithFormat:@"Game%dLevel%d",FIXIT,1];
    _lblLevel1 = [[UILabel alloc] initWithFrame:CGRectMake(650, 70, 300, 30)];
    currentLevel = [NSString stringWithFormat:@"Level 01 : %d", [[NSUserDefaults standardUserDefaults] integerForKey:points1]];
    [_lblLevel1 setText:currentLevel];
    font = [UIFont systemFontOfSize: 21];
    [_lblLevel1 setFont:font];
    _lblLevel1.textAlignment = UITextAlignmentCenter;
    [_lblLevel1 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel1];
    
    NSString *points2 = [NSString stringWithFormat:@"Game%dLevel%d",FIXIT,2];
    _lblLevel2 = [[UILabel alloc] initWithFrame:CGRectMake(650, 100, 300, 30)];
    currentLevel = [NSString stringWithFormat:@"Level 02 : %d", [[NSUserDefaults standardUserDefaults] integerForKey:points2]];
    [_lblLevel2 setText:currentLevel];
    font = [UIFont systemFontOfSize: 21];
    [_lblLevel2 setFont:font];
    _lblLevel2.textAlignment = UITextAlignmentCenter;
    [_lblLevel2 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel2];

    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyPlaygroundBackground"] intValue] == 1) 
        [backgroundImageFixIt setImage:[UIImage imageNamed:@"Gamebackground_pink"]];
    [_lblSynonym setHidden:NO];
    [_lblOpposite setHidden:NO];

    
    [_lblOpposite setText:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"opposite%d",currentWord]]]];
    
    [_lblSynonym setText:[NSString stringWithFormat:@"Meaning :%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"synonym%d",currentWord]]]];
    
    if([_lblSynonym.text isEqualToString:@"0"] || [_lblSynonym.text isEqualToString:@"Meaning :"]){
        [_lblSynonym setHidden:YES];
    }
    
    if([_lblOpposite.text isEqualToString:@"0"]){
        [_lblOpposite setHidden:YES];
    }
    else{
        [_lblOpposite setHidden:NO];
    }


    if (![[NSUserDefaults standardUserDefaults] boolForKey:SYNONYM_ACTIVE]) {
        [_lblSynonym setHidden:YES];
    }
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    if (cId > 989) {
        _lblOpposite.hidden = YES;
        _lblSynonym.hidden = YES;
    }
 

}

-(void) insitiantiateForReportCouting{
    retval = [[NSMutableArray alloc] init];
    reportCorrectCount = 0;
    reportTotatCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;

}

-(void) initializeContent{
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FIXIT,cId,activeLanguage,ActiveGrade];
    currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCount];
    
    if ((currentWord == [tempContentDic count]/8) || (!currentWord))
    {
        currentWord = 0;
    }
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],FIXIT,cId,activeLanguage];
    
    level = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    
    if (!level) 
    {
        level = 1;
    }
}
-(void) loadDatabase{
    int position = 0;
    // Loaading data  from database and saving in dictationaries
    MrPDatabase *database = [[MrPDatabase alloc] init];    
    int LanguageSupport = [[NSUserDefaults standardUserDefaults] integerForKey:@"LanguageSupport"];    
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    //    int cId = CURRENT_CLASSIFICATION;    
    NSArray *MrPContentArray;
        activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    
    if (cId == 99999) {
        
        int gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
        int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
        
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
            NSArray *MrPContentEnglishArray = [database EnglishWordDatabaseArray:gId andpId:pId];
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentEnglishArray count]];
            
            for (MrPContentInfo *info in MrPContentEnglishArray) {
                
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                [tempContentDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
                [tempContentDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];
                position++;
            }

            
            return;
        }
    }
    
    
    
    
    
    if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
        MrPContentArray = [database EnglishWordDatabaseArray:cId];
    }
    else {
        MrPContentArray = [database ArabicWordDatabaseArray:cId];
    }

    tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    for (MrPContentInfo *info in MrPContentArray) {
        

            [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
            [tempContentDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]
             ];
            [tempContentDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"dummy%d",position]];
                
            if (activeLanguage == 2 || (cId > 989 && cId < 999))
                {
                    [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"Word%d",position]];
                }
            else
                {
                    [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
                }
            [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
            [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
        [tempContentDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
        [tempContentDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];

            position++;
    }
    position = 0;
}

-(void) characterWalkingAnimation{

    if (characterWalkingAnimationImgView) {
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;


    }
        characterAnimationStep = 0;
        animationImageNumber = 1; 
        animationImageDirection = @"R";
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        NSString* animationImg = [NSString stringWithFormat:@"Walking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        NSString* animationImg = [NSString stringWithFormat:@"GWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        NSString* animationImg = [NSString stringWithFormat:@"BBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        NSString* animationImg = [NSString stringWithFormat:@"BGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        NSString* animationImg = [NSString stringWithFormat:@"DBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        NSString* animationImg = [NSString stringWithFormat:@"DGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        NSString* animationImg = [NSString stringWithFormat:@"CBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        NSString* animationImg = [NSString stringWithFormat:@"CGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FireBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FireGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        NSString* animationImg = [NSString stringWithFormat:@"ABWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        NSString* animationImg = [NSString stringWithFormat:@"AGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        NSString* animationImg = [NSString stringWithFormat:@"PBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        NSString* animationImg = [NSString stringWithFormat:@"PGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+356, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    [characterWalkingAnimationImgView setBackgroundColor:[UIColor clearColor]];
    
    UIButton *characterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    characterBtn.titleLabel.text = @"Hear Word";
    [characterBtn addTarget:self 
               action:@selector(characterSpeakWord)
     forControlEvents:UIControlEventTouchUpInside];
    characterBtn.frame = CGRectMake(262, 21, 78, 85);

    [self.view addSubview:characterBtn];
    [characterBtn setUserInteractionEnabled:YES];
    [characterBtn setBackgroundColor:[UIColor clearColor]];
    
    UIImageView* hearlblImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hear.png"]];
    hearlblImgView.frame = CGRectMake(262, 21, 78, 83);
    [self.view addSubview:hearlblImgView];
    
    UILabel* lblHearWord = [[UILabel alloc] 
                            initWithFrame:CGRectMake(250, 21, 78, 83)];
//    lblHearWord.text = @"Hear Word";
    [lblHearWord setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"hear.png"]]];
    lblHearWord.font = [UIFont fontWithName:@"Arial-BoldMT" size: 30];
    lblHearWord.textColor = [UIColor greenColor];
    lblHearWord.backgroundColor = [UIColor clearColor];
    lblHearWord.textAlignment = UITextAlignmentCenter;
    [self.view addSubview:lblHearWord];
    
//    UITapGestureRecognizer *walkingCharacterTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(speakWord:)];
//    walkingCharacterTapped.numberOfTapsRequired = 1;
//    walkingCharacterTapped.numberOfTouchesRequired = 1;
//    [characterWalkingAnimationImgView addGestureRecognizer:walkingCharacterTapped];


    [self.view addSubview:characterWalkingAnimationImgView];
    [self.view bringSubviewToFront:characterWalkingAnimationImgView];
//    [characterWalkingAnimationImgView bringSubviewToFront:characterBtn];
    [characterWalkingAnimationImgView setUserInteractionEnabled:YES];


    
    characterWalkingTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1
                                                               target:self 
                                                             selector:@selector(updateCharacterStep) 
                                                             userInfo:nil 
                                                              repeats:YES]retain]; 

    
}

- (void)characterSpeakWord{
    [self playSound];
}

-(void) updateCharacterStep{

    animationImageNumber = animationImageNumber + 1;
    if (animationImageNumber == 8) {
        animationImageNumber = 1;
    }
    

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        animationImageGender = @"";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        animationImageGender = @"G";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        animationImageGender = @"BB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        animationImageGender = @"BG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        animationImageGender = @"DB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        animationImageGender = @"DG";
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        animationImageGender = @"CB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        animationImageGender = @"CG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        animationImageGender = @"FireB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        animationImageGender = @"FireG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        animationImageGender = @"FB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        animationImageGender = @"FG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        animationImageGender = @"AB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        animationImageGender = @"AG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        animationImageGender = @"PB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        animationImageGender = @"PG";
    }
    NSString* animationImg = [NSString stringWithFormat:@"%@Walking_%@%d.png",animationImageGender,animationImageDirection,animationImageNumber];
    [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    
    if ([animationImageDirection isEqualToString:@"R"]) {
        characterAnimationStep = characterAnimationStep + 8;        
    }
    else {
            characterAnimationStep = characterAnimationStep - 8;
    }

    CGRect frame = characterWalkingAnimationImgView.frame;
    frame.origin.x =  characterAnimationStep;
    characterWalkingAnimationImgView.frame = frame;
    
    if (characterAnimationStep > 900) {
        animationImageDirection = @"L";
    }
    if (characterAnimationStep < 20) {
        animationImageDirection = @"R";
    }

}

-(void) charactersInteraction{

    if (sessionCounter == 10) {
        sessionCounter = 0;
        [self sessionCompletionCheck];
        return;
    }
    sessionCounter = sessionCounter + 1;
    
    cellText.text = @"";
    [self playSound];
    
    if (_completeWordArray) {
        [_completeWordArray removeAllObjects];
    }
    
    if (charactersView) {
        [charactersView removeFromSuperview];
        [charactersView release];
        charactersView = nil;        
    }
    
    charactersView = [[UIView alloc] initWithFrame:CGRectMake(0, 346-146, 1024, 248+200)];
    [charactersView setBackgroundColor:[UIColor clearColor]];
    charactersView.userInteractionEnabled = YES;
    [self.view addSubview:charactersView];
    
    
    _correctText = [tempContentDic objectForKey:[NSString stringWithFormat:@"Word%d",currentWord]];
    
    // Report
    
    NSLog(@"correct text is %@",_correctText);
    
    int noOfCharacters;
    isSentence = false;
    if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"isSentence%d",currentWord]] intValue] == 1) {
        noOfCharacters =  [[_correctText componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] count];
        isSentence = TRUE;
        
    }
    else {
        noOfCharacters = _correctText.length;
    }

           
    if (characterDic) {
        [characterDic removeAllObjects];
    }
    
 
    // populatin character array with words/ characters
    NSMutableArray *charactersArray = [[NSMutableArray alloc] initWithCapacity:noOfCharacters + level - 1]; // level - 1 is because we have by default level one and we dnt want extra word in level one
    if (isSentence) {    
        NSArray *sentenceArray = [_correctText componentsSeparatedByString: @" "];
        [charactersArray addObjectsFromArray:sentenceArray];                
        for (int i = 1; i < level; i++) {
            int r = arc4random() % [_englishExtraWordsArr count];
            [charactersArray addObject:[_englishExtraWordsArr objectAtIndex:r]];
            noOfCharacters = noOfCharacters + 1;
        }
    }
    else {
        
    unichar characters;
    for (int k = 0; k < noOfCharacters ; k++) {
        characters = [_correctText characterAtIndex:k];
        NSString * charactr = [NSString stringWithFormat:@"%C", characters]; 
        [charactersArray addObject:charactr];
        }
        for (int i = 1; i < 5; i++) {
            int r = arc4random() % [_englishExtraCharactersArr count];
            [charactersArray addObject:[_englishExtraCharactersArr objectAtIndex:r]];
            noOfCharacters = noOfCharacters + 1;
        }
    }
        
    count = 10;
    yLine = 1;
    int randomCounter = noOfCharacters;
    if (noOfCharacters < 44) {
        yLine = yLine + 110;
    }
    
    for (int n = 0; n< noOfCharacters; n++) {
        int randomNumer = arc4random() % randomCounter ;
        NSString *characterExtracted = [charactersArray objectAtIndex:randomNumer];
        [charactersArray removeObjectAtIndex:randomNumer];
        [characterDic removeObjectForKey:[NSString stringWithFormat:@"%d",randomNumer]];
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 128 + yLine, 100, 100)];
        [label1 setFont:[UIFont boldSystemFontOfSize:50]];
        if (isSentence) {
            label1.frame = CGRectMake(10+count, 120 + yLine, 140, 38);
            [label1 setFont:[UIFont boldSystemFontOfSize:25]];
        }
        label1.textAlignment = UITextAlignmentCenter;
        label1.text = characterExtracted;
        label1.tag = n;
        
        //        [label1 setFont:font];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        else {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        
        label1.textAlignment = UITextAlignmentCenter;
        label1.layer.borderWidth = 1.0;
        label1.layer.cornerRadius = 8;
        
        label1.userInteractionEnabled = YES;
        
        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(labelDragging:)] autorelease];
        [label1 addGestureRecognizer:gesture];
        if ([label1.text isEqualToString:@""]) {
            continue;
        }
        [charactersView addSubview:label1];
        
        count = count + 115;
        if (isSentence) {
            count = count + 100;
        }
        if (count > 850) {
            yLine = yLine + 115;
            count = 10;
        }
        randomCounter = randomCounter - 1;
    }
        [self characterWalkingAnimation];
}

- (void)labelDragging:(UIPanGestureRecognizer *)gesture
{
    UILabel *label2 = (UILabel *)gesture.view;
    
    CGPoint translation = [gesture translationInView:label2];
    
    // move label
    label2.center = CGPointMake(label2.center.x + translation.x,
                                label2.center.y + translation.y);
    
    //        [label2 setFont:[UIFont fontWithName:@"Arial" size:42]];
    //        UIFont *font = [UIFont systemFontOfSize: 20];
    [label2 setFont:[UIFont boldSystemFontOfSize:50]];
    if (isSentence) {
        [label2 setFont:[UIFont boldSystemFontOfSize:35]];
    }
    //        [label2 setFont:font];
    
    NSLog(@"%f", [gesture locationInView:label2.superview].x);
    NSLog(@"%f", [gesture locationInView:label2.superview].y);
    
    int xLowerLimit = 50; // 113
    int yLowerLimit = 50; // 269
    int xUpperLimit = 50+590; // 654
    int yUpperLimit = 50+50; //345
    if ([gesture locationInView:label2.superview].x > xLowerLimit && [gesture locationInView:label2.superview].x < xUpperLimit && [gesture locationInView:label2.superview].y > yLowerLimit && [gesture locationInView:label2.superview].y < yUpperLimit) {
        [label2 removeFromSuperview];
        [_completeWordArray addObject:label2.text];
        NSString *guessWord = @"";
        int numberOfCharacters = [_completeWordArray count];
        for (int k = 0; k < numberOfCharacters; k++) {
            if (isSentence) {
                guessWord = [guessWord stringByAppendingFormat:[NSString stringWithFormat:@"%@ ",[_completeWordArray objectAtIndex:k]]];
            }
            else {
                guessWord = [guessWord stringByAppendingFormat:[NSString stringWithFormat:@"%@",[_completeWordArray objectAtIndex:k]]];
            }
            k = k+1;
        }
        
        cellText.text = guessWord;
        //         [self checkDraggedText:Nil]
        
    }
    
    [gesture setTranslation:CGPointZero inView:label2];
    
    if(gesture.state == UIGestureRecognizerStateEnded)
    {
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 128 + yLine, 100 , 100)];
        [label1 setFont:[UIFont boldSystemFontOfSize:50]];
        if (isSentence) {
            label1.frame = CGRectMake(10 + count, 120 + yLine, 140, 38);
            [label1 setFont:[UIFont boldSystemFontOfSize:35]];
        }
        label1.text = label2.text;
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        else {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        label1.textAlignment = UITextAlignmentCenter;
        label1.layer.borderWidth = 1.0;
        label1.layer.cornerRadius = 8;
        label1.userInteractionEnabled = YES;
        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(labelDragging:)] autorelease];
        [label1 addGestureRecognizer:gesture];
        [charactersView addSubview:label1];
        
        count = count + 115;
        if (isSentence) {
            count = count + 100;
        }
        if (count > 850) {
            yLine = yLine + 115;
            count = 10;
        }
        
        if (count == 10 && yLine > 46) {
            yLine = 1;
            count = 10;
        }
        [label2 removeFromSuperview];
    }
    
}

-(void) NextWord:(BOOL) answer{

    if (characterWalkingAnimationImgView) {
        [characterWalkingAnimationImgView removeFromSuperview];
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
    }

    if (characterWalkingTimer) {
        [characterWalkingTimer invalidate];
        [characterWalkingTimer release];
        characterWalkingTimer = nil;
    }
    
    if (!soundTimer) {
        soundTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0
                                                       target:self 
                                                     selector:@selector(reActivateAllButtons) 
                                                     userInfo:nil 
                                                      repeats:YES]retain];
        
        [BtnCheckForCorrect setEnabled:NO];
        [BtnGoNext setEnabled:NO];
        
    }
    
    [self updateReportCurrentWord];
    currentWord = currentWord + 1;
    
    if (currentWord == [tempContentDic count]/8) {
        currentWord = 0;
        level = level + 1;
        [self saveProgressForReport];
        [self insitiantiateForReportCouting];
    }
    
    if (level > 5) {
        [_levelCounterlbl removeFromSuperview];
        [charactersView removeFromSuperview];
        [_wordCounterlbl removeFromSuperview];
        NSLog(@"Level is greater then five");
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Game Ends"]; 
        [alert setMessage:@"You have completed this location. Please select other location from MAP"];
        [alert setDelegate:self];
        [alert addButtonWithTitle:@"Exit"];       
        [alert setTag:2];
        [alert show];
        [alert release];
        NSLog(@"Level is greater then five");
        return;
    }
    [self updateProgress:answer];
    
    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:[NSString stringWithFormat:@"FixItForClassification%d",cId]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:level forKey:[NSString stringWithFormat:@"FixItForClassification%dLEVEL",cId]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (answer == NO) {
            NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FIXIT,cId,activeLanguage,ActiveGrade];
        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],FIXIT,cId,activeLanguage];
        [[NSUserDefaults standardUserDefaults] setInteger:level forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];        
    }
    
    [self charactersInteraction];
    
    [_lblSynonym setHidden:NO];
    [_lblOpposite setHidden:NO];

    [_lblOpposite setText:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"opposite%d",currentWord]]]];
    
    [_lblSynonym setText:[NSString stringWithFormat:@"Meaning :%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"synonym%d",currentWord]]]];
    
    if([_lblSynonym.text isEqualToString:@"0"] || [_lblSynonym.text isEqualToString:@"Meaning :"]){
        [_lblSynonym setHidden:YES];
    }
    
    if([_lblOpposite.text isEqualToString:@"0"]){
        [_lblOpposite setHidden:YES];
    }
    else{
        [_lblOpposite setHidden:NO];
    }
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    if (cId > 989) {
        _lblOpposite.hidden = YES;
        _lblSynonym.hidden = YES;
    }


    
    NSString *currentNumber = [NSString stringWithFormat:@"Current Word : %d / %d",currentWord + 1, [tempContentDic count]/8];
    [_wordCounterlbl setText:currentNumber];    
    
    NSString *currentLevel = [NSString stringWithFormat:@"Level : %d",level]; 
    [_levelCounterlbl setText:currentLevel];            
}

-(void) playSound{

    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }  

    
    NSString* soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];    
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;    
    [player play];

}
-(void) stopTimerAndMakeSoundButtonsClickable{
    
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        cheeringSoundTimer = nil;
        [cheeringSoundTimer release];
    }
    
    [self NextWord:TRUE];

}

- (void)viewDidUnload
{
    [backgroundImageFixIt release];
    backgroundImageFixIt = nil;
    [BtnGoNext release];
    BtnGoNext = nil;
    [BtnCheckForCorrect release];
    BtnCheckForCorrect = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)UndoLastStep:(id)sender {
    
    // scenerio: cellText.text length] > 0 in case of word and cellText.text length] > 1 in case of sentence.. Vow!
    if ([cellText.text length] > 0) {
        
        
        NSString *lastCharacter = [cellText.text substringFromIndex: [cellText.text length] - 1];
        if (isSentence) {
            lastCharacter = [_completeWordArray lastObject];
        }
        if ([_completeWordArray count] == 0) {
            return;
        }

        [_completeWordArray removeLastObject];
        [_completeWordArray removeLastObject];
        
        if ( [cellText.text length] > 0 && !isSentence)
            cellText.text = [cellText.text substringToIndex:[cellText.text length] - 1];
        
        if (isSentence && [cellText.text length] > 0) {
            NSLog(@"cell Text is %@",cellText.text);

            cellText.text = [cellText.text stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ",lastCharacter] withString:@""];
            
        }
        
        
        // adding character now
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 128 + yLine, 100, 100)];
        [label1 setFont:[UIFont boldSystemFontOfSize:50]];
        int sentenceCheck;
        if (activeLanguage == 1 && (cId < 989 || cId > 999) && [_arabicWordDic count] > 1) {
            sentenceCheck = [[_arabicWordDic objectForKey:[NSString stringWithFormat:@"isSentence%d",currentWord]] intValue];
        }
        else {
            sentenceCheck = [[tempContentDic objectForKey:[NSString stringWithFormat:@"isSentence%d",currentWord]] intValue];
        }
                
        if (sentenceCheck == 1) {
            label1.frame = CGRectMake(10+count, 120+ yLine, 140, 38);
            [label1 setFont:[UIFont boldSystemFontOfSize:25]];
        }
        
        if (isSentence) {
            
        }
        label1.text = lastCharacter;
        //        [label1 setFont:[UIFont fontWithName:@"Arial" size:25]];
        //        UIFont* font = [UIFont systemFontOfSize: 20];
        
        //        [label1 setFont:font];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        else {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        
        
        
        
        label1.textAlignment = UITextAlignmentCenter;
        label1.layer.borderWidth = 1.0;
        label1.layer.cornerRadius = 8;
        
        label1.userInteractionEnabled = YES;
        
        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(labelDragging:)] autorelease];
        [label1 addGestureRecognizer:gesture];
        [charactersView addSubview:label1];
        
        count = count + 115;
        if (isSentence) {
            count = count + 100;
        }
        if (count > 850) {
            yLine = yLine + 115;
            count = 10;
        }
        
        if (count == 10 && yLine > 46) {
            yLine = 1;
            count = 10;
        }
    }
}

- (IBAction)CheckAnswer:(id)sender {
    
    if (!soundTimer) {
        soundTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                       target:self 
                                                     selector:@selector(reActivateAllButtons) 
                                                     userInfo:nil 
                                                      repeats:YES]retain];
        
        [BtnCheckForCorrect setEnabled:NO];
        [BtnGoNext setEnabled:NO];
        
    }
    

    NSString *tempStoreCellWord = cellText.text;
    if ( [tempStoreCellWord length] > 0){
        NSString *lastWord = [tempStoreCellWord substringFromIndex: [tempStoreCellWord length] - 1];
        if ([lastWord isEqualToString:@" "]) 
        {
            tempStoreCellWord = [tempStoreCellWord substringToIndex:[tempStoreCellWord length] - 1];        
        }
    }
    
    NSString *tempStoreCorrectWord = _correctText;
    if ( [tempStoreCorrectWord length] > 0){
        NSString *lastWord = [tempStoreCorrectWord substringFromIndex: [tempStoreCorrectWord length] - 1];
        if ([lastWord isEqualToString:@" "]) 
        {
            tempStoreCorrectWord = [tempStoreCorrectWord substringToIndex:[tempStoreCorrectWord length] - 1];        
        }
    }
    
    if ([tempStoreCellWord isEqualToString: tempStoreCorrectWord]) {

        //Report
        reportCorrectCount = reportCorrectCount + 1;
        reportTotatCount = reportTotatCount + 1;
        
        correctWordCounter = correctWordCounter + 1;
        NSLog(@"CorrectWord Counter is %d",correctWordCounter);
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 160+146, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 160+146, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_1.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 160+146, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 160+146, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_1.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 160+146, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 160+146, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_1.png"]];
        }
        


        CGRect frame = characterWalkingAnimationImgView.frame;
        frame.origin.y =  frame.origin.y - 200;
        cheeringImgView.frame = frame;
        
        [cheeringImgView setBackgroundColor:[UIColor clearColor]];
        [charactersView addSubview:cheeringImgView];
        cheeringAnimationCounter = 0;
        cheeringImageNumber = 1;
        
        [characterWalkingAnimationImgView removeFromSuperview];
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
        if (characterWalkingTimer) {
            [characterWalkingTimer invalidate];
            [characterWalkingTimer release];
            characterWalkingTimer = nil;
        }
        
        
        cheeringAnimationTimer = [[NSTimer scheduledTimerWithTimeInterval:0.2
                                                                   target:self 
                                                                 selector:@selector(changeImage) 
                                                                 userInfo:nil 
                                                                  repeats:YES]retain]; 
        [self playCheeringSound];

    }
    else {
        
        reportTotatCount = reportTotatCount + 1;
        [self playEncouragingSound];
        [self updateProgress:FALSE];
    }
}













-(void) playEncouragingSound{
    
    int randomNumer = arc4random() % 3 ;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC25";
        NSString *second = @"EC30";
        NSString *third = @"EC31";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }else {
        NSString *first = @"AC04";
        NSString *second = @"AC05";
        NSString *third = @"AC06";
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }
}
- (IBAction)playHelpSound:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"GH018" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void) reActivateAllButtons{
    [BtnCheckForCorrect setEnabled:YES];
    [BtnGoNext setEnabled:YES];
    
    if (soundTimer) {
        [soundTimer invalidate];
        soundTimer = nil;
        [soundTimer release];
    }
}

- (IBAction)GoToNextWord:(id)sender {
    
    [self NextWord:FALSE];

  
}

- (IBAction)gotoMainMenu:(id)sender {
    if (myFixItPlayer) {
        [myFixItPlayer stop];
        myFixItPlayer = nil;
    }
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    if (cheeringAnimationTimer) {
        [cheeringAnimationTimer invalidate];
        cheeringAnimationTimer = nil;
    }
    
    
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        cheeringSoundTimer = nil;
    }
    
    if (characterWalkingTimer) {
        [characterWalkingTimer invalidate];
        characterWalkingTimer = nil;
    }
    
    [self saveProgressForReport];
}

-(void) saveProgressForReport{
    // write in db!
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database saveReportProgress:retval];
    
    
    
    
    
 
//    NSLog(@"temp is %@",tempContentDic);

    if (retval) {
        [retval removeAllObjects];
        [retval release];
        retval = nil;
    }

}
-(void) updateReportCurrentWord{
    reportWordDesc = [tempContentDic objectForKey:[NSString stringWithFormat:@"Word%d",currentWord]];
    reportWordId = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord]] intValue];
    NSLog(@"current word is %d",currentWord);
    NSLog(@"temp content dic is %@",tempContentDic);
    NSLog(@"reportWord DEsc is %@ and id is %d",reportWordDesc,reportWordId);
    int gameId = 1;
    MrPReport *info = [[MrPReport alloc]
                       initWithWordId:reportWordId andWordDesc:reportWordDesc andCorrectCount:reportCorrectCount andTotalCount:reportTotatCount andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
    
    [retval addObject:info];
    
    reportTotatCount = 0;
    reportCorrectCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;

}

-(void) updateProgress:(BOOL) correctAnswer{
    


    

    if (correctAnswer) {
        // Report

        
       NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
        int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount]; 
        totalWordsLastCount = totalWordsLastCount + 1;
        
       NSString *correctWordsCount = [NSString stringWithFormat:@"correctWordsCount"];
        int correctWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:correctWordsCount];
        correctWordsLastCount = correctWordsLastCount + 1;
        
        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",[Common getPinNumber],FIXIT,cId,activeLanguage];
        int triesLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:triesCount];
        
        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",FIXIT,level];
        float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
        if (!PointsLastCount) {
            PointsLastCount = 0;
        }
        else{
        
        
        }
        
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],FIXIT,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        
        NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",FIXIT,1];
        [_lblLevel1 setText:[NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]]];
        
        NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",FIXIT,2];
        [_lblLevel2 setText:[NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]]];
        
        
        if (triesLastCount < 10) {
            if (LevelLastCount == 2) {
                PointsLastCount = PointsLastCount + 2 * 1.5;
                
            }
            else {
                PointsLastCount = PointsLastCount + (3 * 1.5);                
            }
            
            
            
            
        }   
        else {

        }
        triesLastCount = 0;                        
        NSLog(@"Tries Last Count is %d", triesLastCount);
        NSLog(@"Current Points is %f", PointsLastCount);
        NSLog(@"Current Word is %d", currentWord);
//        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",FIXIT,cId,activeLanguage];
        [[NSUserDefaults standardUserDefaults] setInteger:triesLastCount forKey:triesCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
            NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FIXIT,cId,activeLanguage,ActiveGrade];
        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
//        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",FIXIT,cId,activeLanguage];
        [[NSUserDefaults standardUserDefaults] setInteger:level forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
//        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",FIXIT,level];
        [[NSUserDefaults standardUserDefaults] setFloat:PointsLastCount forKey:Points]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:totalWordsLastCount forKey:totalWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:correctWordsLastCount forKey:correctWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else {
        
        NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
        int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount]; 
        totalWordsLastCount = totalWordsLastCount + 1;
        
        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",[Common getPinNumber],FIXIT,cId,activeLanguage];
        int triesLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:triesCount];
        triesLastCount = triesLastCount + 1;                
    
        [[NSUserDefaults standardUserDefaults] setInteger:totalWordsLastCount forKey:totalWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:triesLastCount forKey:triesCount];
        [[NSUserDefaults standardUserDefaults] synchronize];  
        
        NSLog(@"Tries Last Count is %d", triesLastCount);
    }    
}

-(void) changeImage{
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_7.png"]];
                
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_8.png"]];
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 1;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_5.png"]];
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 9) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_6.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 8:
                cheeringImageNumber = 9;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_9.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 9:
                cheeringImageNumber = 10;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 10:
                cheeringImageNumber = 11;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 11:
                cheeringImageNumber = 1;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 18) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 10) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    
    // Chef Boy
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // CHEF GIRL
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // FIRE
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // Fire Girl
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // Football Boy
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    //ARAB BOY
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // POLICE BOY
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    
    
    
}


-(void) playCheeringSound{
    
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                               target:self 
                                                             selector:@selector(stopTimerAndMakeSoundButtonsClickable) 
                                                             userInfo:nil 
                                                              repeats:YES]retain];
        
        
        
    }
    
    int randomNumer = arc4random() % 3 ;
    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
        
    }
    
}

-(void) sessionCompletionCheck{
    
    
    UIAlertView *sessionAlert;
    if (correctWordCounter > 7) {
               
        if (activeLanguage == 2) {
             sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good!You have earned Bonus of +2" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
             sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
        }
        
        [sessionAlert setTag:10];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"RewardUnlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        
        if (activeLanguage ==2) {
                    sessionAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your progress needs improvement. Try again." delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
                    sessionAlert = [[UIAlertView alloc] initWithTitle:@"عفوا !" message:@"تقدمك غير كافي , يجب أن تحاول أكثر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب" otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:1];
    }
    
    [sessionAlert show];
    [sessionAlert release];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
 
    correctWordCounter = 0;
    if(alertView.tag == 2)
    {
        if (myFixItPlayer) {
            [myFixItPlayer stop];
            myFixItPlayer = nil;
        }
        [self dismissModalViewControllerAnimated:NO];
        [self saveProgressForReport];
    }
    
    if (alertView.tag == 10) {
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
        }
        if (buttonIndex == 1)
        {
            if (myFixItPlayer) {
                [myFixItPlayer stop];
                myFixItPlayer = nil;
            }
            [self dismissModalViewControllerAnimated:NO];
                    [self saveProgressForReport];
        }
    }
    
    else {                            
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
        }
        if (buttonIndex == 1)
        {
            if (myFixItPlayer) {
                [myFixItPlayer stop];
                myFixItPlayer = nil;
            }
            [self dismissModalViewControllerAnimated:NO];
                    [self saveProgressForReport];
        }        
        
    }   
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    
    NSString* fileName = [NSString stringWithFormat:@"3-fixIT"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    myFixItPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    myFixItPlayer.volume = 0.15;
    myFixItPlayer.numberOfLoops = 20;
    myFixItPlayer.delegate = self;
    
    if ([myFixItPlayer prepareToPlay]) {
        [myFixItPlayer play];            
    }    
}

- (void)dealloc {
    [backgroundImageFixIt release];
    [BtnGoNext release];
    [BtnCheckForCorrect release];
    [super dealloc];
}
- (BOOL)bannerViewActionShouldBegin:
(ADBannerView *)banner
               willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
        [_bannerView removeFromSuperview];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Did Fail to receive ad");
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"received ad");
    [self.view addSubview:_bannerView];
    //    self.view = _bannerView;
}
@end
