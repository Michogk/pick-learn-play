//
//  introViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 2/7/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "introViewController.h"

@interface introViewController ()

@end

@implementation introViewController
@synthesize scrollView, pageControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self playBackGroundMusic];
    [self.view setBackgroundColor:[UIColor grayColor]];
    
    pageControlBeingUsed = NO;

    NSArray *images = [NSArray arrayWithObjects:[UIImage imageNamed:@"1-main.png"], [UIImage imageNamed:@"2-help-audio.png"], [UIImage imageNamed:@"3-help-watch-video.png"],[UIImage imageNamed:@"4-home.png"], [UIImage imageNamed:@"5-words.png"], [UIImage imageNamed:@"6-Free.png"],[UIImage imageNamed:@"7-camp-1.png"], [UIImage imageNamed:@"8-camp-2.png"], [UIImage imageNamed:@"9-camp-last.png"],[UIImage imageNamed:@"11-download.png"], [UIImage imageNamed:@"12-keyboard-1.png"], [UIImage imageNamed:@"13-keyboard-2.png"],[UIImage imageNamed:@"14-keyboard-3.png"], [UIImage imageNamed:@"15-keyboard-4.png"],nil];
    for (int i = 0; i < images.count; i++) {
        CGRect frame;
        frame.origin.x = 1024*i;
        frame.origin.y = 0;
        frame.size = CGSizeMake(1024, 725-125);
        
        UIImageView *subview = [[UIImageView alloc] initWithFrame:frame];
        subview.image = [images objectAtIndex:i];
        [self.scrollView addSubview:subview];
        [subview release];
    }
    
    self.scrollView.contentSize = CGSizeMake(1024*14, 725-125);
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = images.count;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!pageControlBeingUsed) {
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = self.scrollView.frame.size.width;
        int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pageControl.currentPage = page;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)changePage:(id)sender {
    CGRect frame;
    frame.origin.x = self.scrollView.frame.size.width * self.pageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.scrollView.frame.size;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    pageControlBeingUsed = YES;
}

- (IBAction)endIntro:(id)sender {
    if (introBackgroundMusic) {
        [introBackgroundMusic stop];
        [introBackgroundMusic release];
        introBackgroundMusic = nil;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlBeingUsed = NO;
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"Light_Hearted_030_sec"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    introBackgroundMusic = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    introBackgroundMusic.volume = 0.15;
    introBackgroundMusic.numberOfLoops = 20;
    introBackgroundMusic.delegate = self;
    
    if ([introBackgroundMusic prepareToPlay]) {
        [introBackgroundMusic setCurrentTime:0.0];
        [introBackgroundMusic play];
    }
    
}

- (void)dealloc {
    [scrollView release];
    [pageControl release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setScrollView:nil];
    [self setPageControl:nil];
    [super viewDidUnload];
}
@end
