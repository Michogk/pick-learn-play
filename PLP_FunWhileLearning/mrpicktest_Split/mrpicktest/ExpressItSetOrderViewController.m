//
//  ExpressItSetOrderViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 28/09/2013.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "ExpressItSetOrderViewController.h"
#import "ExpressItViewController.h"
#import "MrPDatabase.h"
#import "DocumentDirectory.h"
#import "SimpleAudioEngine.h"
#import "MrPReport.h"
#import "MrPContentInfo.h"
#import <QuartzCore/QuartzCore.h>
#import "ZipArchive.h"

#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]

@interface ExpressItSetOrderViewController ()

@end

@implementation ExpressItSetOrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self resetClassificationsPrincipleGrammarIDs];
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    activeGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    framesOfTaggedImages = [[NSMutableDictionary alloc] init];
        [self loadDatabase];
    [self createBackground];

        [self populateImages];
}

-(void) resetClassificationsPrincipleGrammarIDs{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:PRINCIPLE_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:GRAMMAR_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:CLASSIFICATIONS_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];

}
-(void) createBackground{
    

    UIImageView *backgroundBoc;
    int posX = 70;
    int posY = 0;//120;
    
    for (int i = 1; i <= [tempContentDic count]/5+1 ; i++) {
        backgroundBoc = [[UIImageView alloc] initWithFrame:CGRectMake(posX, posY, 135, 135)];
        backgroundBoc.image = [UIImage imageNamed:@"Box-Express-IT.png"];
        [scrollViewBackground addSubview:backgroundBoc];
        [backgroundBoc release];
     
        if ( i%5 == 0) {
            posX = 70;
            posY = posY + 150;
        }
        else{
            posX = posX + 170;
        }
    }
}

-(void) populateImages{
    
    
    UIImageView *ItemImgView;
    int posX = 80;
    int posY = 5;//125;
    tagsArr = [[NSMutableArray alloc] init];
    
    for (int i = 1; i <= ([tempContentDic count]/5+1); i++)
    {
        ItemImgView = [[UIImageView alloc] initWithFrame:CGRectMake(posX, posY, 110, 110)];
                int tag = [[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",i]]] intValue];
        ItemImgView.image = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@.png",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",tag]]]];

        if (tag == 0 || !tag) {
            tag = 1;
        }
        [ItemImgView setTag:tag];
        [tagsArr addObject:[NSString stringWithFormat:@"%d",tag]];
        [framesOfTaggedImages setObject:[NSString stringWithFormat:@"%d",posX] forKey:[NSString stringWithFormat:@"X%d",tag]];
        [framesOfTaggedImages setObject:[NSString stringWithFormat:@"%d",posY] forKey:[NSString stringWithFormat:@"Y%d",tag]];
       [ItemImgView setUserInteractionEnabled:YES];
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        singleTap.delegate = self;
        [ItemImgView addGestureRecognizer:singleTap];
        
        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(labelDragging:)] autorelease];
        [ItemImgView addGestureRecognizer:gesture];
        
        [scrollViewBackground addSubview:ItemImgView];
        if ( i%5 == 0) {
            posX = 80;
            posY = posY + 150;
        }
        else{ posX = posX + 170; }
        
        [ItemImgView release];
    }
    scrollViewBackground.contentSize = CGSizeMake(scrollViewBackground.frame.size.width, posY);
}

-(NSString*) createAndStoreFile:(NSString*) fileName{
    
    NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
    activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString *folderNAme = [[NSUserDefaults standardUserDefaults] objectForKey:@"folderName"];
    NSLog(@"folderName is %@",folderNAme);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    if (!pinNumber || pinNumber == NULL) {
        pinNumber = @"1";
    }
    if ((!folderNAme ) || folderNAme == NULL) {
        folderNAme = @"Express It";
    }
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@-%@/",activeGrade,pinNumber,pinNumber,folderNAme]];

    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) return Nil;
//    if (activeLanguage == 1) {
////        recorder
////        int folderId = cId - 990;
//        recorderFilePath = [[NSString stringWithFormat:@"%@/Folder %d - Word %d.caf",dataPath,folderId,arabicCurrentWord] retain];
//
//    }
//    else{
        recorderFilePath = [[NSString stringWithFormat:@"%@/%@.caf", dataPath, fileName] retain];
//    }
    
    return recorderFilePath;
}

- (void)handleTap:(UITapGestureRecognizer *)sender{
    int tag = sender.view.tag;
    recorderFilePath = Nil;
    for (int k = 0; k <= [tempContentDic count]/5+1; k++) {
        int currentWordId = [[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",k]]] intValue];
        
        if (currentWordId == tag) {
            NSString *voiceIdWord = [NSString stringWithFormat:@"%@", [tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",k]]];
            arabicCurrentWord = k-1;
            NSString *voiceId = [voiceIdWord stringByReplacingOccurrencesOfString:@" " withString:@""];
            activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
            recorderFilePath = [self createAndStoreFile:voiceId];
            break;
            
        }
    }
    
    if (!recorderFilePath) {
        return;
    }
        //    }
    
    if(soundID)
    {
        AudioServicesDisposeSystemSoundID(soundID);
    }
    
    //Get a URL for the sound file
    NSURL *filePath = [NSURL fileURLWithPath:recorderFilePath isDirectory:NO];
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;

    [player play];

    
}

- (void)labelDragging:(UIPanGestureRecognizer *)gesture
{
    
    int tag = gesture.view.tag;
    UIImageView *label2 = (UIImageView *)gesture.view;
    
    CGPoint translation = [gesture translationInView:label2];
    
    // move label
    label2.center = CGPointMake(label2.center.x + translation.x,
                                label2.center.y + translation.y);
    
    
    NSLog(@"%f", [gesture locationInView:label2.superview].x);
    NSLog(@"%f", [gesture locationInView:label2.superview].y);
    
    int xLowerLimit = 0; // 113
    int yLowerLimit = 0; // 269
    int xUpperLimit = 0; // 654
    int yUpperLimit = 0; //345
 
    
    [gesture setTranslation:CGPointZero inView:label2];
    
    if(gesture.state == UIGestureRecognizerStateEnded)
    {
        BOOL gestureEndedWithinBox = false;
        
        for (int i = 0; i <= [tempContentDic count]/5; i++) {
            NSLog(@"%d",i);
            [framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"X%@",[tagsArr objectAtIndex:i]]];
            xLowerLimit = [[framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"X%@",[tagsArr objectAtIndex:i]]] intValue];
            yLowerLimit = [[framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"Y%@",[tagsArr objectAtIndex:i]]] intValue];
            xUpperLimit = xLowerLimit + 135;
            yUpperLimit = yLowerLimit + 135;
            if ([gesture locationInView:label2.superview].x > xLowerLimit && [gesture locationInView:label2.superview].x < xUpperLimit && [gesture locationInView:label2.superview].y > yLowerLimit && [gesture locationInView:label2.superview].y < yUpperLimit) {
//                [label2 removeFromSuperview];
                NSLog(@"Over Dragged Region");
                gestureEndedWithinBox = TRUE;
                
                
                if (label2) {
                    [label2 removeFromSuperview];
                    
                    xLowerLimit = [[framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"X%d",tag]] intValue];
                    yLowerLimit = [[framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"Y%d",tag]] intValue];
                    xUpperLimit = xLowerLimit + 135;
                    yUpperLimit = yLowerLimit + 135;
                    int tagPositionDragged = 0;
                    for (int k = 0; k <= [tempContentDic count]/5; k++) {
                        if (tag == [[tagsArr objectAtIndex:k] intValue]) {
                            tagPositionDragged = k;
                            break;
                        }
                    }
//
                    CGRect draggedImgFrame = CGRectMake(xLowerLimit, yLowerLimit, 120, 120);
//
                    int tagPositionVictim = i;
                    int PositionXVictim = [[framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"X%@",[tagsArr objectAtIndex:i]]] intValue];
                    int PositionYVictim = [[framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"Y%@",[tagsArr objectAtIndex:i]]] intValue];
                    
                    CGRect VictimImgFrame = CGRectMake(PositionXVictim, PositionYVictim, 120, 120);
                    
                    // Drawing Dragged Item
                    
                        UIImageView* ItemImgView = [[UIImageView alloc] initWithFrame:VictimImgFrame];
                        ItemImgView.image = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@.png",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",tag]]]];

                        if (tag == 0 || !tag) {
                            tag = 1;
                        }
                        [ItemImgView setTag:tag];
                        //                [tagsArr addObject:[NSString stringWithFormat:@"%d",tag]];
                        [ItemImgView setUserInteractionEnabled:YES];
                        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                        singleTap.delegate = self;
                        [ItemImgView addGestureRecognizer:singleTap];
                        
                        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc]
                                                            initWithTarget:self
                                                            action:@selector(labelDragging:)] autorelease];
                        [ItemImgView addGestureRecognizer:gesture];
                        [scrollViewBackground addSubview:ItemImgView];
                        
                        [ItemImgView release];

//                    ItemImgView = Nil;
                    
                    // DRAWING VICTIM
                    int tagValueOfVictim = [[tagsArr objectAtIndex:tagPositionVictim] intValue];
                    [[scrollViewBackground viewWithTag:tagValueOfVictim ] removeFromSuperview];
                    
                    UIImageView* ItemImgView2 = [[UIImageView alloc] initWithFrame:draggedImgFrame];
                    ItemImgView2.image = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@.png",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",tagValueOfVictim]]]];
                    
                    if (tagValueOfVictim == 0 || !tagValueOfVictim) {
                        tagValueOfVictim = 1;
                    }
                    [ItemImgView2 setTag:tagValueOfVictim];
                    //                [tagsArr addObject:[NSString stringWithFormat:@"%d",tag]];
                    [ItemImgView2 setUserInteractionEnabled:YES];
                    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                    singleTap.delegate = self;
                    [ItemImgView2 addGestureRecognizer:singleTap];
                    
                    gesture = [[[UIPanGestureRecognizer alloc]
                                                        initWithTarget:self
                                                        action:@selector(labelDragging:)] autorelease];
                    [ItemImgView2 addGestureRecognizer:gesture];
                    [scrollViewBackground addSubview:ItemImgView2];
                    
                    [ItemImgView2 release];
                
                    NSString* tempObj = [NSString stringWithFormat:@"%@",[tagsArr objectAtIndex:tagPositionVictim] ];
                    [tagsArr replaceObjectAtIndex:tagPositionVictim withObject:[tagsArr objectAtIndex:tagPositionDragged]];
                    [tagsArr replaceObjectAtIndex:tagPositionDragged withObject:tempObj];
                    

                    
                    [framesOfTaggedImages setObject:[NSString stringWithFormat:@"%d",PositionXVictim] forKey:[NSString stringWithFormat:@"X%d",tag]];
                    [framesOfTaggedImages setObject:[NSString stringWithFormat:@"%d",PositionYVictim] forKey:[NSString stringWithFormat:@"Y%d",tag]];

                    
                    [framesOfTaggedImages setObject:[NSString stringWithFormat:@"%d",xLowerLimit] forKey:[NSString stringWithFormat:@"X%d",tagValueOfVictim]];
                    [framesOfTaggedImages setObject:[NSString stringWithFormat:@"%d",yLowerLimit] forKey:[NSString stringWithFormat:@"Y%d",tagValueOfVictim]];
                    
                    return;                    
                }

            }
        }
        
        if (gestureEndedWithinBox == false) {
            if (label2) {
                [label2 removeFromSuperview];
                
                xLowerLimit = [[framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"X%d",tag]] intValue];
                yLowerLimit = [[framesOfTaggedImages objectForKey:[NSString stringWithFormat:@"Y%d",tag]] intValue];
                xUpperLimit = xLowerLimit + 135;
                yUpperLimit = yLowerLimit + 135;
                int tagPosition = 0;
                for (int k = 0; k <= [tempContentDic count]/5; k++) {
                    if (tag == [[tagsArr objectAtIndex:k] intValue]) {
                        tagPosition = k+1;
                        break;
                    }
                }

                UIImageView* ItemImgView = [[UIImageView alloc] initWithFrame:CGRectMake(xLowerLimit, yLowerLimit, 120, 120)];
                ItemImgView.image = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@.png",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",tag]]]];
                int tag = [[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",tagPosition]]] intValue];
                if (tag == 0 || !tag) {
                    tag = 1;
                }
                [ItemImgView setTag:tag];
//                [tagsArr addObject:[NSString stringWithFormat:@"%d",tag]];
                [ItemImgView setUserInteractionEnabled:YES];
                UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                singleTap.delegate = self;
                [ItemImgView addGestureRecognizer:singleTap];
                
                UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(labelDragging:)] autorelease];
                [ItemImgView addGestureRecognizer:gesture];
                        [scrollViewBackground addSubview:ItemImgView];
                
                [ItemImgView release];
                
                return;
            }
        }
    }
}


-(void) loadDatabase{
    int position = 2;
    // Loaading data  from database and saving in dictationaries
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    NSArray *MrPContentArray;
    
    if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
        MrPContentArray = [database EnglishWordDatabaseArray:cId];
    }
    else {
        MrPContentArray = [database ArabicWordDatabaseArray:cId];
    }
    NSLog(@"data is loaded successfully and array count is %d",[MrPContentArray count]);
    tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:0];
    for (MrPContentInfo *info in MrPContentArray) {
  
        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]]
        ;
        
        if (activeLanguage == 2 || (cId > 989 && cId < 999))
        {
            [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        else
        {
            [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%@",[NSNumber numberWithInt:info.wordId]]];
        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%@",[NSNumber numberWithInt:info.wordId]]];
        position++;
    }
    //    }
    position = 0;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnEditPressed:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    ExpressItViewController *expressItVC= [sb instantiateViewControllerWithIdentifier:@"ExpressItIdentifier"];
    expressItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:expressItVC animated:NO completion:NULL];
}

- (IBAction)btnDonePressed:(id)sender {
    NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
    activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString *folderNAme = [[NSUserDefaults standardUserDefaults] objectForKey:@"folderName"];
    NSLog(@"folderName is %@",folderNAme);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    if (!pinNumber || pinNumber == NULL) {
        pinNumber = @"1";
    }
    if ((!folderNAme ) || folderNAme == NULL) {
        folderNAme = @"Express It";
    }
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@-%@/",activeGrade,pinNumber,pinNumber,folderNAme]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) return;
    NSString* snapShotName = folderNAme;
    NSString *imagePath = [[NSString stringWithFormat:@"%@/%@.png", dataPath, snapShotName] retain];
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
//    NSString *imagePath = [[NSString stringWithFormat:@"%@/%@/%@.png", DOCUMENTS_FOLDER,activeGrade,snapShotName] retain];
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:imagePath atomically:NO];
    
    [self sendReportToteacher];
}

-(void) sendReportToteacher{
    
    NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
    activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString *folderNAme = [[NSUserDefaults standardUserDefaults] objectForKey:@"folderName"];
    NSLog(@"folderName is %@",folderNAme);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    if (!pinNumber || pinNumber == NULL) {
        pinNumber = @"1";
    }
    if ((!folderNAme ) || folderNAme == NULL) {
        folderNAme = @"Express It";
    }
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@-%@",activeGrade,pinNumber,pinNumber,folderNAme]];
    

    BOOL isDir=NO;
    
//    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSArray *subpaths;
    
//    NSString *toCompress = dataPath;
    NSString *pathToCompress = dataPath;// [documentsDirectory stringByAppendingPathComponent:toCompress];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:pathToCompress isDirectory:&isDir] && isDir){
        subpaths = [fileManager subpathsAtPath:pathToCompress];
    } else if ([fileManager fileExistsAtPath:pathToCompress]) {
        subpaths = [NSArray arrayWithObject:pathToCompress];
    }
    
    NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@.zip",pinNumber, folderNAme ]];
    
    ZipArchive *za = [[ZipArchive alloc] init];
    [za CreateZipFile2:zipFilePath];
    if (isDir) {
        for(NSString *path in subpaths){
            NSString *fullPath = [pathToCompress stringByAppendingPathComponent:path];
            if([fileManager fileExistsAtPath:fullPath isDirectory:&isDir] && !isDir){
                [za addFileToZip:fullPath newname:path];
            }
        }
    } else {
        [za addFileToZip:pathToCompress newname:[NSString stringWithFormat:@"%@-%@.zip",pinNumber, folderNAme ]];
    }
    
    BOOL successCompressing = [za CloseZipFile2];
    if (successCompressing) {
        NSLog(@"True");
        
        [self mailReportToTeacher];
    }
    else{
        NSLog(@"fail");
    }
}

-(void) mailReportToTeacher{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
        activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
        NSString *folderNAme = [[NSUserDefaults standardUserDefaults] objectForKey:@"folderName"];
        [mailViewController setSubject:@"Pick Learn Play Achievements Express It Status Report"];
        [mailViewController setMessageBody:[NSString stringWithFormat:@"Student progress : \nPIN Number: %@\nGrade: %@\nFolder Name :%@",pinNumber,activeGrade,folderNAme] isHTML:NO];
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths lastObject];
        if (!pinNumber || pinNumber == NULL) {
            pinNumber = @"1";
        }
        if ((!folderNAme ) || folderNAme == NULL) {
            folderNAme = @"Express It";
        }
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@.zip",pinNumber,folderNAme]];
        NSData *myData = [NSData dataWithContentsOfFile:dataPath];
        [mailViewController addAttachmentData:myData mimeType:@"application/zip" fileName:[NSString stringWithFormat:@"%@-%@.zip",pinNumber,folderNAme]];
        [self presentModalViewController:mailViewController animated:YES];
        [mailViewController release];
    }
    else {
        NSLog(@"Device is unable to send email in its current state.");
    }
    
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {

    if (!error) {
        NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
        activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
        NSString *folderNAme = [[NSUserDefaults standardUserDefaults] objectForKey:@"folderName"];
        
        // Remove Zip File
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths lastObject];
        if (!pinNumber || pinNumber == NULL) {
            pinNumber = @"1";
        }
        if ((!folderNAme ) || folderNAme == NULL) {
            folderNAme = @"Express It";
        }
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@.zip",pinNumber,folderNAme]];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:dataPath])
        {
            [fileManager removeItemAtPath:dataPath error:NULL];
        }
        
//        // Removing Folder
//        dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@-%@",activeGrade, pinNumber,folderNAme]];
//        if([fileManager fileExistsAtPath:dataPath])
//        {
//            [fileManager removeItemAtPath:dataPath error:NULL];
//        }
    }
    [self dismissModalViewControllerAnimated:YES];
}


- (void)dealloc {
    [scrollViewBackground release];
    [super dealloc];
}
@end
