//
//  WordsCell.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 23/09/2015.
//  Copyright (c) 2015 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WordsCell : UITableViewCell
+(id) createCell;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImgView;
@property (retain, nonatomic) IBOutlet UILabel *titleLbl;
@property (retain, nonatomic) IBOutlet UIImageView *itemImgView;

@end
