//
//  HelloWorldLayer.m
//  BubbleIpad
//
//  Created by Muhammad Mosib Asad on 6/28/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "GameLayer.h"
#import "BaloonsGroup.h"
#import "SimpleAudioEngine.h"
#import "MyPlayGroundViewController.h"

#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "HelpSoundViewController.h"
#import "DocumentDirectory.h"
#import "MyRewardsViewController.h"
#import "Common.h"
#import "MrPReport.h"



#define BIRD_TAG 80

#define FIRST_BALOON 50
#define SECOND_BALOON 60
#define THIRD_BALOON 70
#define SUCCESS_POINTS 10
#define LEVEL_DIFFICULTY_BONOUS 5

#define POPIT 1

#define DEFAULT_LANGUAGE_ID LanguageIdOne

@implementation GameLayer
@synthesize _answerIsCorrect;

static GameLayer* _gameLayerRef;
static int currentLevel;
//@synthesize arabicWordsArr = _arabicWordsArr;

+ (GameLayer*) sharedLayer
{
    return _gameLayerRef;
}

+ (int) currentLevel
{
    return currentLevel;
}

+ (CCScene *) scene
{
    
    CCScene *scene = [CCScene node];
    GameLayer *layer = [GameLayer node];
    [scene addChild: layer];
    
    return scene;
}

- (id) init
{
    if( (self=[super init])) {
        _gameLayerRef = self;
        
        [self insitiantiateForReportCouting];
        
        
        self.isTouchEnabled = YES;
        
        //        self.arabicWordsArr = [BalloonGameDatabase dB].ArabicWordInfos;
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"4-PopIT.mp3" loop:YES];
            [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume :0.10f];
        }
        
        
        
        // 1 for pink and 2 for blue
        int activeBackground = [[NSUserDefaults standardUserDefaults] integerForKey:@"MyPlaygroundBackground"];
        CCSprite * backgroundSprite;
        if (activeBackground == 2){
            backgroundSprite = [CCSprite spriteWithFile:@"Game_background.png"];
        }
        else{
            backgroundSprite = [CCSprite spriteWithFile:@"Gamebackground_pink.png"];
        }
        
        backgroundSprite.position = ccp(0, 0);
        backgroundSprite.anchorPoint = ccp(0, 0);
        [self addChild:backgroundSprite];
        
        
        NSString* loadPrincipleRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentPrincipleString"];
        NSString* loadGrammarRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentGrammarString"];
        cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
        if ((cId == 99999) && ([loadGrammarRule isEqualToString:@""] || [loadPrincipleRule isEqualToString:@""])) {
            
            UIAlertView *alert = [[UIAlertView alloc] init];
            [alert setTitle:@"CAUTION"];
            [alert setMessage:@"Please select any Grammar and Principle Rule and come back again"];
            [alert setDelegate:self];
            [alert addButtonWithTitle:@"OK"];
            [alert setTag:2];
            [alert show];
            [alert release];
            return self;
        }
        
        [self loadDatabase];
        
        if (insufficientDataInSelectedSkillsPackage) {
            return self;
        }
        
        
        sessionCounter = 0;
        sessionProgressCounter = 0;
        
        
        
        CCMenuItem *repeatWordSound = [CCMenuItemImage
                                       itemFromNormalImage:@"hear.png" selectedImage:@"hear.png"
                                       target:self selector:@selector(speakArabicWord)];
        repeatWordSound.position = ccp(785, 707);
        repeatWordSound.scaleX = 0.35;
        repeatWordSound.scaleY = 0.35;
        
        CCMenuItem *goToPlayGroundMenuitem = [CCMenuItemImage
                                              itemFromNormalImage:@"MyPlayground_btn.png" selectedImage:@"MyPlayground_btn.png"
                                              target:self selector:@selector(removeScene)];
        goToPlayGroundMenuitem.position = ccp(120, 700);
        goToPlayGroundMenuitem.scaleX = 0.7;
        goToPlayGroundMenuitem.scaleY = 0.6;
        
        
        
        
        // Points 1
        NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",POPIT,1];
        NSString *lblLevel1 = [NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]];
        CCLabelTTF *lblPoints1TTF = [CCLabelTTF labelWithString:lblLevel1 fontName:@"Helvetica" fontSize:20];
        lblPoints1TTF.anchorPoint = ccp(0, 0);
        lblPoints1TTF.color = ccBLACK;
        CCMenuItemLabel* pointsMenuItem1 = [CCMenuItemLabel itemWithLabel:lblPoints1TTF target:self selector:@selector(DoNothing:)];
        pointsMenuItem1.position = ccp(810, 640);
        pointsMenuItem1.tag =123;
        
        // Points 2
        NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",POPIT,2];
        NSString *lblLevel2 = [NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]];
        CCLabelTTF *lblPoints2TTF = [CCLabelTTF labelWithString:lblLevel2 fontName:@"Helvetica" fontSize:20];
        lblPoints2TTF.anchorPoint = ccp(0, 0);
        lblPoints2TTF.color = ccBLACK;
        CCMenuItemLabel* pointsMenuItem2 = [CCMenuItemLabel itemWithLabel:lblPoints2TTF target:self selector:@selector(DoNothing:)];
        pointsMenuItem2.position = ccp(810, 610);
        pointsMenuItem2.tag =124;
        
        CCMenu *pointsMenu = [CCMenu menuWithItems:pointsMenuItem1,pointsMenuItem2, nil];
        [pointsMenu setEnabled:NO];
        pointsMenu.position = CGPointZero;
        [self addChild:pointsMenu z:25 tag:125];
        
        if ([tempContentDic count] > 5) {
            CCMenuItem *helpBtn = [CCMenuItemImage
                                   itemFromNormalImage:@"Sound_Help.png" selectedImage:@"Sound_Help.png"
                                   target:self selector:@selector(HelpSound:)];
            helpBtn.position = ccp(903, 710);
            
            
            if (cId == 99999) { // in case of My skills playground
                
                NSString *grammarRuleLbl = [NSString stringWithFormat:@"%@ / %@",loadGrammarRule,loadPrincipleRule];
                CGSize textSize = [grammarRuleLbl sizeWithFont:[UIFont fontWithName:@"Helvetica" size:35] constrainedToSize:CGSizeMake( 600, 300 ) lineBreakMode:UILineBreakModeWordWrap];
                
                CCLabelTTF *myskillsLbl = [CCLabelTTF labelWithString:grammarRuleLbl
                                                           dimensions:textSize
                                                            alignment:UITextAlignmentCenter
                                                             fontName:@"Helvetica"
                                                             fontSize:30];
                
                myskillsLbl.anchorPoint = ccp(0, 0);
                myskillsLbl.color = ccWHITE;
                
                CCMenuItemLabel* myskillsMenuItem = [CCMenuItemLabel itemWithLabel:myskillsLbl target:self selector:@selector(DoNothing:)];
                myskillsMenuItem.position = ccp(400, 70);
                
                
                
                
                goToPlayGroundMenu = [CCMenu menuWithItems:myskillsMenuItem,helpBtn, goToPlayGroundMenuitem,repeatWordSound, nil];
                goToPlayGroundMenu.position = CGPointZero;
                [self addChild:goToPlayGroundMenu];
            }
            
            
            else {
                goToPlayGroundMenu = [CCMenu menuWithItems:goToPlayGroundMenuitem,helpBtn,repeatWordSound, nil];
                goToPlayGroundMenu.position = CGPointZero;
                [self addChild:goToPlayGroundMenu];
            }
            [self startGame];
        }
        else
        {
            if (cId > 989 ) {
                UIAlertView *alert = [[UIAlertView alloc] init];
                if (activeLanguage == 1) {
                    [alert setTitle:@"احذر"];
                    [alert setMessage:@" البيانات لا تكفي في ملفاتك"];
                }
                else {
                    [alert setTitle:@"WARNING"];
                    [alert setMessage:@"Data NOT sufficient in your folder"];
                }
                
                [alert setDelegate:self];
                [alert addButtonWithTitle:@"OK"];
                [alert setTag:2];
                [alert show];
                [alert release];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] init];
                [alert setTitle:@"WARNING"];
                [alert setMessage:@"SORRY! Data No Sufficient. Please Make Sure you have selected correct Object"];
                [alert setDelegate:self];
                [alert addButtonWithTitle:@"OK"];
                [alert setTag:2];
                [alert show];
                [alert release];
            }
            
        }
        
    }
    return self;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    
    sessionProgressCounter = 0;
    if(alertView.tag == 2)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        }
        
        [[CCDirector sharedDirector] end];
        [[CCDirector sharedDirector].openGLView removeFromSuperview];
    }
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
            [self startGame];
        }
        if (buttonIndex == 1)
        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            }
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].openGLView removeFromSuperview];
        }
    }
    if (alertView.tag == 10) {
        if (alertView.tag == 1)
        {
            if (buttonIndex == 0)
            {
                sessionCounter = 0;
                [self startGame];
            }
            if (buttonIndex == 1)
            {
                // Open Reward SCreen
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
                    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
                }
                [[CCDirector sharedDirector] end];
                [[CCDirector sharedDirector].openGLView removeFromSuperview];
                
                //                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                //                MyRewardsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"rewardVCIdentifier"];
                //                vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                //                [self.parent presentViewController:vc animated:YES completion:NULL];
                
            }
        }
        
    }
}

- (void) updatePlayerPoints
{
    /* Level */
    //    [[NSUserDefaults standardUserDefaults] setInteger:currentLevel forKey:@"currentLevel"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    //
    //    int previousPoints = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentScores"];
    //    if (previousPoints) {
    //        previousPoints += SUCCESS_POINTS + (LEVEL_DIFFICULTY_BONOUS * currentLevel);
    //
    //    }
    //    else
    //    {
    //        previousPoints = SUCCESS_POINTS;
    //    }
    //    [[NSUserDefaults standardUserDefaults] setInteger:previousPoints forKey:@"currentScores"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *) levelPointsFileSavePath
{
    //    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    return [[pathArray objectAtIndex:0] stringByAppendingPathComponent:@"levelPoints.plist"];
    return nil;
}

-(void) sessionCompletionCheck{
    UIAlertView *sessionAlert;
    if (sessionProgressCounter > 7) {
        
        if (activeLanguage == 2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good!" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:1];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"RewardUnlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        if (activeLanguage ==2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your progress needs improvement. Try again." delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عفوا !" message:@"تقدمك غير كافي , يجب أن تحاول أكثر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب" otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:1];
    }
    
    [sessionAlert show];
    [sessionAlert release];
    
}

-(void) startGame{
    
    if(helpMenu)
    {
        [helpMenu removeFromParentAndCleanup:YES];
        helpMenu = nil;
    }
    
    if (sessionCounter == 10) {
        sessionCounter = 0;
        [self sessionCompletionCheck];
        return;
    }
    
    sessionCounter = sessionCounter + 1;
    
    
    self.isTouchEnabled = YES;
    correctBalloonAlreadyBusted = false;
    wrongBalloonAlreadyBusted = false;
    
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],POPIT,cId,activeLanguage,ActiveGrade];
    currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCount];
    if (cId ==99999) {
        if ((currentWord == [tempContentDic count]/5) || (!currentWord)) {
            currentWord = 0;
        }
    }
    else {

        if ((currentWord == [tempContentDic count]/5) || (!currentWord)) {
            currentWord = 0;
        }
    }
    
    
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],POPIT,cId,activeLanguage];
    int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    if (!LevelLastCount || LevelLastCount != 2) {
        //        LevelLastCount = 1;
        level = 1;
    }
    
    // Load Game Data from UserDefault coming from Calling Application
    // current Level is of no use now!
    [self loadLevelData:currentLevel];
    //    [self showCorrectWord];
    [self speakArabicWord];
    
    if (![self getChildByTag:BIRD_TAG]) {
        [self birdAnimation];
    }
    
    
    if (lblCorrectVerb)
    {
        [lblCorrectVerb stopAllActions];
        [self removeChild:lblCorrectVerb cleanup:YES];
        lblCorrectVerb = nil;
        NSLog(@"Correct Word has been Removed");
    }
    
    //    [self showSentence];
    [self callBubblesAnimation];
    [self bottomBarLabel];
    self.isTouchEnabled = false;
    
//    if (cId == 99999) {
        if ([self getChildByTag:146]) {
            [self removeChildByTag:146 cleanup:YES];
        }
    
        NSString *synonymOppLbl = @"";
        CGSize textSize = [synonymOppLbl sizeWithFont:[UIFont fontWithName:@"Helvetica" size:25] constrainedToSize:CGSizeMake( 400, 200 ) lineBreakMode:UILineBreakModeWordWrap];
        
        CCLabelTTF *synonymWordlblTTF = [CCLabelTTF labelWithString:synonymOppLbl
                                                         dimensions:textSize
                                                          alignment:UITextAlignmentCenter
                                                           fontName:@"Helvetica"
                                                           fontSize:25];
        synonymWordlblTTF.anchorPoint = ccp(0, 0);
        synonymWordlblTTF.color = ccWHITE;
        
        
        CCLabelTTF *oppositeLblTTF = [CCLabelTTF labelWithString:synonymOppLbl
                                                      dimensions:textSize
                                                       alignment:UITextAlignmentCenter
                                                        fontName:@"Helvetica"
                                                        fontSize:25];
        oppositeLblTTF.anchorPoint = ccp(0, 0);
        oppositeLblTTF.color = ccWHITE;
        
        if (![[tempContentDic objectForKey:[NSString stringWithFormat:@"synonym%d",currentWord]] isEqualToString:@"0"])
        {
            NSString *synonymLbl = [NSString stringWithFormat:@" %@",[tempContentDic objectForKey:[NSString stringWithFormat: @"synonym%d",currentWord]]];
            CGSize textSize = [synonymLbl sizeWithFont:[UIFont fontWithName:@"Helvetica" size:35] constrainedToSize:CGSizeMake( 400, 200 ) lineBreakMode:UILineBreakModeWordWrap];
            
            synonymWordlblTTF = [CCLabelTTF labelWithString:synonymLbl
                                                 dimensions:textSize
                                                  alignment:UITextAlignmentCenter
                                                   fontName:@"Helvetica"
                                                   fontSize:25];
            synonymWordlblTTF.anchorPoint = ccp(0, 0);
            synonymWordlblTTF.color = ccBLACK;
            //    CCMenuItemLabel* myskillsMenuItemSynonym = [CCMenuItemLabel itemWithLabel:synonymWordlblTTF target:self selector:@selector(DoNothing:)];
            //    myskillsMenuItemSynonym.position = ccp(800, 130);
        }
        
        
        
        if (![[tempContentDic objectForKey:[NSString stringWithFormat:@"opposite%d",currentWord]] isEqualToString:@"0"]) {
            NSString *oppositeLbl = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat: @"opposite%d",currentWord]]];
            CGSize textSize = [oppositeLbl sizeWithFont:[UIFont fontWithName:@"Helvetica" size:35] constrainedToSize:CGSizeMake( 400, 200 ) lineBreakMode:UILineBreakModeWordWrap];
            
            oppositeLblTTF = [CCLabelTTF labelWithString:oppositeLbl
                                              dimensions:textSize
                                               alignment:UITextAlignmentCenter
                                                fontName:@"Helvetica"
                                                fontSize:25];
            oppositeLblTTF.anchorPoint = ccp(0, 0);
            oppositeLblTTF.color = ccBLACK;
            //    CCMenuItemLabel* myskillsMenuItemOpposite = [CCMenuItemLabel itemWithLabel:oppositeLblTTF target:self selector:@selector(DoNothing:)];
            //    myskillsMenuItemOpposite.position = ccp(700, 130);
        }
        
        CCMenuItemLabel* myskillsMenuItemSynonym = [CCMenuItemLabel itemWithLabel:synonymWordlblTTF target:self selector:@selector(DoNothing:)];
        myskillsMenuItemSynonym.position = ccp(825, 140);
        
        CCMenuItemLabel* myskillsMenuItemOpposite = [CCMenuItemLabel itemWithLabel:oppositeLblTTF target:self selector:@selector(DoNothing:)];
        myskillsMenuItemOpposite.position = ccp(825, 175);
        
        CCMenu *synonymOppMenu = [CCMenu menuWithItems:myskillsMenuItemSynonym,myskillsMenuItemOpposite,nil];
        synonymOppMenu.position = CGPointZero;
        [self addChild:synonymOppMenu z:200 tag:146];
//    }
}

- (void) removeCurrentLevel
{
    if (lblSentence) {
        [lblSentence removeFromParentAndCleanup:YES];
        lblSentence = nil;
    }
    if (lblbottomBar) {
        [lblbottomBar removeFromParentAndCleanup:YES];
        lblbottomBar = nil;
    }
    
    if (lblCorrectVerb) {
        [lblCorrectVerb removeFromParentAndCleanup:YES];
        lblCorrectVerb = nil;
    }
    
    [[balloons getChildByTag: FIRST_BALOON] removeFromParentAndCleanup:YES];
    //    [[balloons getChildByTag: SECOND_BALOON] removeFromParentAndCleanup:YES];
    [[balloons getChildByTag: THIRD_BALOON] removeFromParentAndCleanup:YES];
    
    
}

-(void) birdAnimation{
    // gender 1 for boy and gender 2 for girl..
    int gender = [[NSUserDefaults standardUserDefaults] integerForKey:@"gender"];
    
    if (gender == 2 || gender == 4 || gender == 6 || gender == 8 ||  gender == 10 ||  gender == 12 || gender == 14 || gender == 16 ) {
        CCSprite *birdSprite;
        if (gender == 2) {
            birdSprite = [CCSprite spriteWithFile:@"GWalking_R1.png"];
        }
        
        if (gender == 4) {
            birdSprite = [CCSprite spriteWithFile:@"BGWalking_R1.png"];
        }
        
        if (gender == 6) {
            birdSprite = [CCSprite spriteWithFile:@"DGWalking_R1.png"];
        }
        
        if (gender == 8) {
            birdSprite = [CCSprite spriteWithFile:@"CGWalking_R1.png"];
        }
        
        if (gender == 10) {
            birdSprite = [CCSprite spriteWithFile:@"FireGWalking_R1.png"];
        }
        
        if (gender == 12) {
            birdSprite = [CCSprite spriteWithFile:@"FGWalking_R1.png"];
        }
        
        if (gender == 14) {
            birdSprite = [CCSprite spriteWithFile:@"AGWalking_R1.png"];
        }
        
        if (gender == 16) {
            birdSprite = [CCSprite spriteWithFile:@"PGWalking_R1.png"];
        }
        
        birdSprite.position = ccp(50 , 225);
        [self addChild:birdSprite z:10 tag:BIRD_TAG];
        CCAnimation *birdWingsAnimation = [CCAnimation animation];
        [birdWingsAnimation setDelayPerUnit:0.11];
        //    [birdWingsAnimation setDelay:0.095];
        
        
        if (gender == 2) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimation addFrameWithFilename:@"GWalking_R1.png"];
                [birdWingsAnimation addFrameWithFilename:@"GWalking_R2.png"];
                [birdWingsAnimation addFrameWithFilename:@"GWalking_R3.png"];
                [birdWingsAnimation addFrameWithFilename:@"GWalking_R4.png"];
                [birdWingsAnimation addFrameWithFilename:@"GWalking_R5.png"];
                [birdWingsAnimation addFrameWithFilename:@"GWalking_R6.png"];
                [birdWingsAnimation addFrameWithFilename:@"GWalking_R7.png"];
            }
        }
        
        if (gender == 4) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimation addFrameWithFilename:@"BGWalking_R1.png"];
                [birdWingsAnimation addFrameWithFilename:@"BGWalking_R2.png"];
                [birdWingsAnimation addFrameWithFilename:@"BGWalking_R3.png"];
                [birdWingsAnimation addFrameWithFilename:@"BGWalking_R4.png"];
                [birdWingsAnimation addFrameWithFilename:@"BGWalking_R5.png"];
                [birdWingsAnimation addFrameWithFilename:@"BGWalking_R6.png"];
                [birdWingsAnimation addFrameWithFilename:@"BGWalking_R7.png"];
            }
        }
        
        if (gender == 6) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimation addFrameWithFilename:@"DGWalking_R1.png"];
                [birdWingsAnimation addFrameWithFilename:@"DGWalking_R2.png"];
                [birdWingsAnimation addFrameWithFilename:@"DGWalking_R3.png"];
                [birdWingsAnimation addFrameWithFilename:@"DGWalking_R4.png"];
                [birdWingsAnimation addFrameWithFilename:@"DGWalking_R5.png"];
                [birdWingsAnimation addFrameWithFilename:@"DGWalking_R6.png"];
                [birdWingsAnimation addFrameWithFilename:@"DGWalking_R7.png"];
            }
        }
        
        if (gender == 8) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimation addFrameWithFilename:@"CGWalking_R1.png"];
                [birdWingsAnimation addFrameWithFilename:@"CGWalking_R2.png"];
                [birdWingsAnimation addFrameWithFilename:@"CGWalking_R3.png"];
                [birdWingsAnimation addFrameWithFilename:@"CGWalking_R4.png"];
                [birdWingsAnimation addFrameWithFilename:@"CGWalking_R5.png"];
                [birdWingsAnimation addFrameWithFilename:@"CGWalking_R6.png"];
                [birdWingsAnimation addFrameWithFilename:@"CGWalking_R7.png"];
            }
        }
        
        if (gender == 10) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimation addFrameWithFilename:@"FireGWalking_R1.png"];
                [birdWingsAnimation addFrameWithFilename:@"FireGWalking_R2.png"];
                [birdWingsAnimation addFrameWithFilename:@"FireGWalking_R3.png"];
                [birdWingsAnimation addFrameWithFilename:@"FireGWalking_R4.png"];
                [birdWingsAnimation addFrameWithFilename:@"FireGWalking_R5.png"];
                [birdWingsAnimation addFrameWithFilename:@"FireGWalking_R6.png"];
                [birdWingsAnimation addFrameWithFilename:@"FireGWalking_R7.png"];
            }
        }
        
        if (gender == 12) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimation addFrameWithFilename:@"FGWalking_R1.png"];
                [birdWingsAnimation addFrameWithFilename:@"FGWalking_R2.png"];
                [birdWingsAnimation addFrameWithFilename:@"FGWalking_R3.png"];
                [birdWingsAnimation addFrameWithFilename:@"FGWalking_R4.png"];
                [birdWingsAnimation addFrameWithFilename:@"FGWalking_R5.png"];
                [birdWingsAnimation addFrameWithFilename:@"FGWalking_R6.png"];
                [birdWingsAnimation addFrameWithFilename:@"FGWalking_R7.png"];
            }
        }
        
        if (gender == 14) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimation addFrameWithFilename:@"AGWalking_R1.png"];
                [birdWingsAnimation addFrameWithFilename:@"AGWalking_R2.png"];
                [birdWingsAnimation addFrameWithFilename:@"AGWalking_R3.png"];
                [birdWingsAnimation addFrameWithFilename:@"AGWalking_R4.png"];
                [birdWingsAnimation addFrameWithFilename:@"AGWalking_R5.png"];
                [birdWingsAnimation addFrameWithFilename:@"AGWalking_R6.png"];
                [birdWingsAnimation addFrameWithFilename:@"AGWalking_R7.png"];
            }
        }
        
        if (gender == 16) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimation addFrameWithFilename:@"PGWalking_R1.png"];
                [birdWingsAnimation addFrameWithFilename:@"PGWalking_R2.png"];
                [birdWingsAnimation addFrameWithFilename:@"PGWalking_R3.png"];
                [birdWingsAnimation addFrameWithFilename:@"PGWalking_R4.png"];
                [birdWingsAnimation addFrameWithFilename:@"PGWalking_R5.png"];
                [birdWingsAnimation addFrameWithFilename:@"PGWalking_R6.png"];
                [birdWingsAnimation addFrameWithFilename:@"PGWalking_R7.png"];
            }
        }
        
        
        
        
        
        id birdWingsAction = [CCAnimate actionWithAnimation:birdWingsAnimation];
        CGPoint moveTo = CGPointMake(880, 225);
        CCMoveTo * birdMovementAction = [CCMoveTo actionWithDuration:4.5 position:moveTo];
        id actionLR = [CCSpawn actions:birdWingsAction, birdMovementAction,nil];
        CCAnimation *birdWingsAnimationRL = [CCAnimation animation];
        [birdWingsAnimationRL setDelayPerUnit:0.11];
        
        
        
        if (gender == 2) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimationRL addFrameWithFilename:@"GWalking_L1.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"GWalking_L2.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"GWalking_L3.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"GWalking_L4.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"GWalking_L5.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"GWalking_L6.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"GWalking_L7.png"];
            }
        }
        
        if (gender == 4) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimationRL addFrameWithFilename:@"BGWalking_L1.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"BGWalking_L2.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"BGWalking_L3.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"BGWalking_L4.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"BGWalking_L5.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"BGWalking_L6.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"BGWalking_L7.png"];
            }
        }
        
        if (gender == 6) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimationRL addFrameWithFilename:@"DGWalking_L1.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"DGWalking_L2.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"DGWalking_L3.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"DGWalking_L4.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"DGWalking_L5.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"DGWalking_L6.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"DGWalking_L7.png"];
            }
        }
        if (gender == 8) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimationRL addFrameWithFilename:@"CGWalking_L1.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"CGWalking_L2.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"CGWalking_L3.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"CGWalking_L4.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"CGWalking_L5.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"CGWalking_L6.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"CGWalking_L7.png"];
            }
        }
        if (gender == 10) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimationRL addFrameWithFilename:@"FireGWalking_L1.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FireGWalking_L2.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FireGWalking_L3.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FireGWalking_L4.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FireGWalking_L5.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FireGWalking_L6.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FireGWalking_L7.png"];
            }
        }
        if (gender == 12) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimationRL addFrameWithFilename:@"FGWalking_L1.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FGWalking_L2.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FGWalking_L3.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FGWalking_L4.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FGWalking_L5.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FGWalking_L6.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"FGWalking_L7.png"];
            }
        }
        if (gender == 14) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimationRL addFrameWithFilename:@"AGWalking_L1.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"AGWalking_L2.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"AGWalking_L3.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"AGWalking_L4.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"AGWalking_L5.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"AGWalking_L6.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"AGWalking_L7.png"];
            }
        }
        if (gender == 16) {
            for (int p = 1; p<7; p++) {
                [birdWingsAnimationRL addFrameWithFilename:@"PGWalking_L1.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"PGWalking_L2.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"PGWalking_L3.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"PGWalking_L4.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"PGWalking_L5.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"PGWalking_L6.png"];
                [birdWingsAnimationRL addFrameWithFilename:@"PGWalking_L7.png"];
            }
        }
        
        
        
        id birdWingsActionRL = [CCAnimate actionWithAnimation:birdWingsAnimationRL];
        CGPoint moveToRL = CGPointMake(50, 225);
        CCMoveTo * birdMovementActionRL = [CCMoveTo actionWithDuration:4.5 position:moveToRL];
        id actionRL = [CCSpawn actions:birdWingsActionRL, birdMovementActionRL,nil];
        
        //        CCAnimation *boyTurningAnimation = [CCAnimation animation];
        //        [boyTurningAnimation setDelayPerUnit:0.2];
        //        [boyTurningAnimation addFrameWithFilename:@"Turn_1.png"];
        //        [boyTurningAnimation addFrameWithFilename:@"Turn_2.png"];
        //        [boyTurningAnimation addFrameWithFilename:@"Turn_3.png"];
        //        [boyTurningAnimation addFrameWithFilename:@"Turn_4.png"];
        //        id boyTurningAnimationId = [CCAnimate actionWithAnimation:boyTurningAnimation];
        //        CGPoint turnLR = CGPointMake(40, 243);
        //        CCMoveTo * boyTurningRLMovement = [CCMoveTo actionWithDuration:1.0 position:turnLR];
        //        id actionTurningRL = [CCSpawn actions:boyTurningAnimationId, boyTurningRLMovement,nil];
        CCSequence* sequence = [CCSequence actions:actionLR, actionRL, /*actionTurningRL,*/ nil];
        CCRepeatForever *repeat = [CCRepeatForever actionWithAction:sequence];
        [birdSprite runAction:repeat];
        
    }
    else {
        
        
        
        if (gender == 1 || gender == 3 || gender == 5 || gender == 7 ||  gender == 9 ||  gender == 11 || gender == 13 || gender == 15) {
            CCSprite *birdSprite;
            if (gender == 1) {
                birdSprite = [CCSprite spriteWithFile:@"Walking_R1.png"];
            }
            
            if (gender == 3) {
                birdSprite = [CCSprite spriteWithFile:@"BBWalking_R1.png"];
            }
            
            if (gender == 5) {
                birdSprite = [CCSprite spriteWithFile:@"DBWalking_R1.png"];
            }
            
            
            if (gender == 7) {
                birdSprite = [CCSprite spriteWithFile:@"CBWalking_R1.png"];
            }
            
            if (gender == 9) {
                birdSprite = [CCSprite spriteWithFile:@"FireBWalking_R1.png"];
            }
            
            
            if (gender == 11) {
                birdSprite = [CCSprite spriteWithFile:@"FBWalking_R1.png"];
            }
            
            if (gender == 13) {
                birdSprite = [CCSprite spriteWithFile:@"ABWalking_R1.png"];
            }
            
            if (gender == 15) {
                birdSprite = [CCSprite spriteWithFile:@"PBWalking_R1.png"];
            }
            
            
            birdSprite.position = ccp(50 , 225);
            [self addChild:birdSprite z:10 tag:BIRD_TAG];
            CCAnimation *birdWingsAnimation = [CCAnimation animation];
            [birdWingsAnimation setDelayPerUnit:0.095];
            //    [birdWingsAnimation setDelay:0.095];
            
            
            if (gender == 1) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimation addFrameWithFilename:@"Walking_R1.png"];
                    [birdWingsAnimation addFrameWithFilename:@"Walking_R2.png"];
                    [birdWingsAnimation addFrameWithFilename:@"Walking_R3.png"];
                    [birdWingsAnimation addFrameWithFilename:@"Walking_R4.png"];
                    [birdWingsAnimation addFrameWithFilename:@"Walking_R5.png"];
                    [birdWingsAnimation addFrameWithFilename:@"Walking_R6.png"];
                }
            }
            
            if (gender == 3) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimation addFrameWithFilename:@"BBWalking_R1.png"];
                    [birdWingsAnimation addFrameWithFilename:@"BBWalking_R2.png"];
                    [birdWingsAnimation addFrameWithFilename:@"BBWalking_R3.png"];
                    [birdWingsAnimation addFrameWithFilename:@"BBWalking_R4.png"];
                    [birdWingsAnimation addFrameWithFilename:@"BBWalking_R5.png"];
                    [birdWingsAnimation addFrameWithFilename:@"BBWalking_R6.png"];
                }
            }
            
            if (gender == 5) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimation addFrameWithFilename:@"DBWalking_R1.png"];
                    [birdWingsAnimation addFrameWithFilename:@"DBWalking_R2.png"];
                    [birdWingsAnimation addFrameWithFilename:@"DBWalking_R3.png"];
                    [birdWingsAnimation addFrameWithFilename:@"DBWalking_R4.png"];
                    [birdWingsAnimation addFrameWithFilename:@"DBWalking_R5.png"];
                    [birdWingsAnimation addFrameWithFilename:@"DBWalking_R6.png"];
                }
            }
            
            if (gender == 7) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimation addFrameWithFilename:@"CBWalking_R1.png"];
                    [birdWingsAnimation addFrameWithFilename:@"CBWalking_R2.png"];
                    [birdWingsAnimation addFrameWithFilename:@"CBWalking_R3.png"];
                    [birdWingsAnimation addFrameWithFilename:@"CBWalking_R4.png"];
                    [birdWingsAnimation addFrameWithFilename:@"CBWalking_R5.png"];
                    [birdWingsAnimation addFrameWithFilename:@"CBWalking_R6.png"];
                }
            }
            
            if (gender == 9) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimation addFrameWithFilename:@"FireBWalking_R1.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FireBWalking_R2.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FireBWalking_R3.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FireBWalking_R4.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FireBWalking_R5.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FireBWalking_R6.png"];
                }
            }
            
            if (gender == 11) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimation addFrameWithFilename:@"FBWalking_R1.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FBWalking_R2.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FBWalking_R3.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FBWalking_R4.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FBWalking_R5.png"];
                    [birdWingsAnimation addFrameWithFilename:@"FBWalking_R6.png"];
                }
            }
            
            if (gender == 13) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimation addFrameWithFilename:@"ABWalking_R1.png"];
                    [birdWingsAnimation addFrameWithFilename:@"ABWalking_R2.png"];
                    [birdWingsAnimation addFrameWithFilename:@"ABWalking_R3.png"];
                    [birdWingsAnimation addFrameWithFilename:@"ABWalking_R4.png"];
                    [birdWingsAnimation addFrameWithFilename:@"ABWalking_R5.png"];
                    [birdWingsAnimation addFrameWithFilename:@"ABWalking_R6.png"];
                }
            }
            
            if (gender == 15) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimation addFrameWithFilename:@"PBWalking_R1.png"];
                    [birdWingsAnimation addFrameWithFilename:@"PBWalking_R2.png"];
                    [birdWingsAnimation addFrameWithFilename:@"PBWalking_R3.png"];
                    [birdWingsAnimation addFrameWithFilename:@"PBWalking_R4.png"];
                    [birdWingsAnimation addFrameWithFilename:@"PBWalking_R5.png"];
                    [birdWingsAnimation addFrameWithFilename:@"PBWalking_R6.png"];
                }
            }
            
            
            
            
            
            
            
            
            
            
            id birdWingsAction = [CCAnimate actionWithAnimation:birdWingsAnimation];
            CGPoint moveTo = CGPointMake(880, 225);
            CCMoveTo * birdMovementAction = [CCMoveTo actionWithDuration:4.5 position:moveTo];
            id actionLR = [CCSpawn actions:birdWingsAction, birdMovementAction,nil];
            CCAnimation *birdWingsAnimationRL = [CCAnimation animation];
            [birdWingsAnimationRL setDelayPerUnit:0.095];
            
            
            
            if (gender == 1) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimationRL addFrameWithFilename:@"Walking_L1.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"Walking_L2.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"Walking_L3.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"Walking_L4.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"Walking_L5.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"Walking_L6.png"];
                }
            }
            
            if (gender == 3) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimationRL addFrameWithFilename:@"BBWalking_L1.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"BBWalking_L2.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"BBWalking_L3.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"BBWalking_L4.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"BBWalking_L5.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"BBWalking_L6.png"];
                }
            }
            
            if (gender == 5) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimationRL addFrameWithFilename:@"DBWalking_L1.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"DBWalking_L2.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"DBWalking_L3.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"DBWalking_L4.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"DBWalking_L5.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"DBWalking_L6.png"];
                }
            }
            
            if (gender == 7) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimationRL addFrameWithFilename:@"CBWalking_L1.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"CBWalking_L2.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"CBWalking_L3.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"CBWalking_L4.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"CBWalking_L5.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"CBWalking_L6.png"];
                }
            }
            
            if (gender == 9) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimationRL addFrameWithFilename:@"FireBWalking_L1.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FireBWalking_L2.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FireBWalking_L3.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FireBWalking_L4.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FireBWalking_L5.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FireBWalking_L6.png"];
                }
            }
            
            if (gender == 11) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimationRL addFrameWithFilename:@"FBWalking_L1.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FBWalking_L2.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FBWalking_L3.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FBWalking_L4.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FBWalking_L5.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"FBWalking_L6.png"];
                }
            }
            
            if (gender == 13) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimationRL addFrameWithFilename:@"ABWalking_L1.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"ABWalking_L2.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"ABWalking_L3.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"ABWalking_L4.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"ABWalking_L5.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"ABWalking_L6.png"];
                }
            }
            
            if (gender == 15) {
                for (int p = 1; p<9; p++) {
                    [birdWingsAnimationRL addFrameWithFilename:@"PBWalking_L1.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"PBWalking_L2.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"PBWalking_L3.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"PBWalking_L4.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"PBWalking_L5.png"];
                    [birdWingsAnimationRL addFrameWithFilename:@"PBWalking_L6.png"];
                }
            }
            
            
            
            
            
            id birdWingsActionRL = [CCAnimate actionWithAnimation:birdWingsAnimationRL];
            CGPoint moveToRL = CGPointMake(50, 225);
            CCMoveTo * birdMovementActionRL = [CCMoveTo actionWithDuration:4.5 position:moveToRL];
            id actionRL = [CCSpawn actions:birdWingsActionRL, birdMovementActionRL,nil];
            //        CCAnimation *boyTurningAnimation = [CCAnimation animation];
            //        [boyTurningAnimation setDelayPerUnit:0.2];
            //        [boyTurningAnimation addFrameWithFilename:@"Turn_1.png"];
            //        [boyTurningAnimation addFrameWithFilename:@"Turn_2.png"];
            //        [boyTurningAnimation addFrameWithFilename:@"Turn_3.png"];
            //        [boyTurningAnimation addFrameWithFilename:@"Turn_4.png"];
            //        id boyTurningAnimationId = [CCAnimate actionWithAnimation:boyTurningAnimation];
            //        CGPoint turnLR = CGPointMake(40, 243);
            //        CCMoveTo * boyTurningRLMovement = [CCMoveTo actionWithDuration:1.0 position:turnLR];
            //        id actionTurningRL = [CCSpawn actions:boyTurningAnimationId, boyTurningRLMovement,nil];
            CCSequence* sequence = [CCSequence actions:actionLR, actionRL, nil];
            CCRepeatForever *repeat = [CCRepeatForever actionWithAction:sequence];
            [birdSprite runAction:repeat];
            
        }
    }
}

-(void) loadLevelData: (int) level
{
    
    //    NSString *levelArr = [NSString stringWithFormat:@"Level%i", level];
    
    //    int pickItem = arc4random() % 4;
    
    //    arabicWordData = [_arabicWordsArr objectAtIndex:currentLevel];
    //    lblCorrectVerb = [CCLabelTTF labelWithString:arabicWordData.arabicWord fontName:@"Marker Felt" fontSize:64];
    
    
    //    sentence = arabicWordData.arabicWord;
    missngWord = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",currentWord]];
    audiofile = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
    
    sentence = @"";
    
}
-(void) stopTimerAndMakeSoundButtonClickable{
    [goToPlayGroundMenu setEnabled:YES];
    
    
    if (cheeringSoundTimerPopIt) {
        [cheeringSoundTimerPopIt invalidate];
        cheeringSoundTimerPopIt = nil;
        [cheeringSoundTimerPopIt release];
    }
    
    if (wordStopTimer) {
        [wordStopTimer invalidate];
        wordStopTimer = nil;
        [wordStopTimer release];
    }
    
}


-(void) speakArabicWord{
    
    // disabling button to avoid multiple taps and hearing sound repeatedly while prev sound is in progress
    if (wordStopTimer) {
        [wordStopTimer invalidate];
        wordStopTimer = nil;
        [wordStopTimer release];
    }
    wordStopTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                      target:self
                                                    selector:@selector(stopTimerAndMakeSoundButtonClickable)
                                                    userInfo:nil
                                                     repeats:YES]retain];
    [goToPlayGroundMenu setEnabled:NO];
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    
    NSString* soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
}

- (void) balloonTapped: (CGPoint) pos IsRight:(BOOL) isRight
{
    
    if (isRight && !wrongBalloonAlreadyBusted) {
        reportCorrectCount = reportCorrectCount + 1;
        reportTotatCount = reportTotatCount + 1;
        [self updateReportCurrentWord];
        correctBalloonAlreadyBusted = true;
        
        CCSprite * Popped_Baloon = [CCSprite spriteWithFile:@"Popped_baloon.png"];
        NSLog(@"position x is %f and position y is %f",pos.x,pos.y);
        
        Popped_Baloon.position =  ccp(pos.x+460, pos.y-120);
        [self addChild:Popped_Baloon];
        [self birdExcitementAnimation];
        id action1=[CCScaleTo actionWithDuration:0.15 scale:1.0];
        id action2=[CCScaleBy actionWithDuration:0.15 scale:1.25];
        id action3=[CCScaleTo actionWithDuration:0.15 scale:1.50];
        id action4=[CCScaleTo actionWithDuration:0.15 scale:1.75];
        id action5=[CCFadeOut actionWithDuration:1.90];
        id updateLevel;
        _answerIsCorrect = TRUE;
        updateLevel = [CCCallFuncN actionWithTarget:self selector:@selector(nextLevel:)];
        [Popped_Baloon runAction:[CCSequence actions:action1,action2,action3,action4,action5,updateLevel, nil]];
        currentLevel++;
        
    }
    else {
        if (!isRight && !correctBalloonAlreadyBusted) {
            
            reportTotatCount = reportTotatCount + 1;
            [self updateReportCurrentWord];
            
            wrongBalloonAlreadyBusted = true;
            CCSprite * Popped_Baloon = [CCSprite spriteWithFile:@"Popped_baloonwrong.png"];
            NSLog(@"position x is %f and position y is %f",pos.x,pos.y);
            
            Popped_Baloon.position =  ccp(pos.x+460, pos.y-120);
            [self addChild:Popped_Baloon];
            id action1=[CCScaleTo actionWithDuration:0.15 scale:1.0];
            id action2=[CCScaleBy actionWithDuration:0.15 scale:1.25];
            id action3=[CCScaleTo actionWithDuration:0.15 scale:1.50];
            id action4=[CCScaleTo actionWithDuration:0.15 scale:1.75];
            id action5=[CCFadeOut actionWithDuration:1.90];
            id updateLevel;
            //        updateLevel = [CCCallFuncN actionWithTarget:self selector:@selector(DoNothing:)];
            if (correctBalloonAlreadyBusted) {
                //            _answerIsCorrect = TRUE;
                updateLevel = [CCCallFuncN actionWithTarget:self selector:@selector(nextLevel:)];
            }
            else {
                updateLevel = [CCCallFuncN actionWithTarget:self selector:@selector(nextLevel:)];
            }
            
            [Popped_Baloon runAction:[CCSequence actions:action1,action2,action3,action4,action5,updateLevel, nil]];
        }
    }
}

-(BOOL) wrongBalloonBusted{
    return wrongBalloonAlreadyBusted;
}

-(BOOL) correctBallonBusted{
    
    return correctBalloonAlreadyBusted;
}

- (void) nextLevel:(id)sender {
    
    [sender removeFromParentAndCleanup:YES];
    
    [self removeCurrentLevel];
    currentWord = currentWord + 1;
    
    if (currentWord > [tempContentDic count]/5) {
        level = level + 1;
        [self saveProgressForReport];
        [self insitiantiateForReportCouting];
    }
    
    [self updateProgress:TRUE];
    
    NSString *wordSaver = [NSString stringWithFormat:@"PopItwordForClassification%d",cId];
    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    [self startGame];
    //    }
    
}

-(void) updateProgress:(BOOL) correctAnswer{
    
    if (_answerIsCorrect) {
        
        _answerIsCorrect = false; // set false for next word
        sessionProgressCounter = sessionProgressCounter + 1;
        NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
        int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount];
        totalWordsLastCount = totalWordsLastCount + 1;
        
        NSString *correctWordsCount = [NSString stringWithFormat:@"correctWordsCount"];
        int correctWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:correctWordsCount];
        correctWordsLastCount = correctWordsLastCount + 1;
        
        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",[Common getPinNumber],POPIT,cId,activeLanguage];
        int triesLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:triesCount];
        
        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",POPIT,level];
        float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
        if (!PointsLastCount) {
            PointsLastCount = 0;
        }
        
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],POPIT,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        
        if (triesLastCount < 10) {
            if (LevelLastCount == 2) {
                PointsLastCount = PointsLastCount + 1;
            }
            else {
                PointsLastCount = PointsLastCount + 1;
            }
            
            [[[self getChildByTag:125] getChildByTag:124] removeFromParentAndCleanup:YES];
            [[[self getChildByTag:125] getChildByTag:123] removeFromParentAndCleanup:YES];
            
            NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",POPIT,1];
            NSString *lblLevel1 = [NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]];
            CCLabelTTF *lblPoints1TTF = [CCLabelTTF labelWithString:lblLevel1 fontName:@"Helvetica" fontSize:20];
            lblPoints1TTF.anchorPoint = ccp(0, 0);
            lblPoints1TTF.color = ccBLACK;
            CCMenuItemLabel* pointsMenuItem1 = [CCMenuItemLabel itemWithLabel:lblPoints1TTF target:self selector:@selector(DoNothing:)];
            pointsMenuItem1.position = ccp(810, 640);
            pointsMenuItem1.tag =123;
            
            // Points 2
            NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",POPIT,2];
            NSString *lblLevel2 = [NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]];
            CCLabelTTF *lblPoints2TTF = [CCLabelTTF labelWithString:lblLevel2 fontName:@"Helvetica" fontSize:20];
            lblPoints2TTF.anchorPoint = ccp(0, 0);
            lblPoints2TTF.color = ccBLACK;
            CCMenuItemLabel* pointsMenuItem2 = [CCMenuItemLabel itemWithLabel:lblPoints2TTF target:self selector:@selector(DoNothing:)];
            pointsMenuItem2.position = ccp(810, 610);
            pointsMenuItem2.tag =124;
            
            [[self getChildByTag:125] addChild:pointsMenuItem1];
            [[self getChildByTag:125] addChild:pointsMenuItem2];
            
            //        CCMenu *pointsMenu = [CCMenu menuWithItems:pointsMenuItem1,pointsMenuItem2, nil];
            //        [pointsMenu setEnabled:NO];
            //        pointsMenu.position = CGPointZero;
            //        [self addChild:pointsMenu z:25 tag:125];
            
        }
        else {
        }
        triesLastCount = 0;
        NSLog(@"Tries Last Count is %d", triesLastCount);
        NSLog(@"Current Points is %f", PointsLastCount);
        NSLog(@"Current Word is %d", currentWord);
        //        NSString *triesCount = [NSString stringWithFormat:@"Game%dClassification%dlanguage%dtriesCount",FIXIT,cId,activeLanguage];
        [[NSUserDefaults standardUserDefaults] setInteger:triesLastCount forKey:triesCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],POPIT,cId,activeLanguage,ActiveGrade];
        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSUserDefaults standardUserDefaults] setInteger:level forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSUserDefaults standardUserDefaults] setFloat:PointsLastCount forKey:Points]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:totalWordsLastCount forKey:totalWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:correctWordsLastCount forKey:correctWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else {
        
        NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
        int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount];
        totalWordsLastCount = totalWordsLastCount + 1;
        
        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",[Common getPinNumber],POPIT,cId,activeLanguage];
        int triesLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:triesCount];
        triesLastCount = triesLastCount + 1;
        
        [[NSUserDefaults standardUserDefaults] setInteger:totalWordsLastCount forKey:totalWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:triesLastCount forKey:triesCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],POPIT,cId,activeLanguage,ActiveGrade];
        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"Tries Last Count is %d", triesLastCount);
    }
}


-(void) DoNothing:(id) sender{
    
    
}

- (void) updateGuesSentence
{
    //    _answerIsCorrect = false;
    [self nextLevel:nil];
    //    currentWord = currentWord + 1;
    //    [self removeCurrentLevel];
    //    [self startGame];
}

- (void) showPlayerPoints
{
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    int level = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentLevel"];
    int points = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentScores"];
    
    /* Show player level */
    //    CCLabelTTF* levelLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Level: %i",level] fontName:@"Marker Felt" fontSize:64];
    
    CCLabelTTF* levelLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Level: %i",level] fontName:[UIFont systemFontOfSize:[UIFont systemFontSize]].fontName fontSize:64];
    levelLabel.color = ccc3(0,160,60);
    levelLabel.position =  ccp( size.width/2 - 160, size.height/2 + 200);
    [self addChild: levelLabel z:160];
    
    
    
    CCLabelTTF* scoreLabel = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Points: %i",points] fontName:[UIFont systemFontOfSize:[UIFont systemFontSize]].fontName fontSize:64];
    scoreLabel.color = ccc3(0,160,60);
    scoreLabel.position =  ccp( size.width/2 + 160, size.height/2 + 200);
    [self addChild: scoreLabel z:160];
    
}
- (void) gameEnded
{
    if ([balloons getChildByTag:FIRST_BALOON]) {
        [[balloons getChildByTag: FIRST_BALOON] removeFromParentAndCleanup:YES];
    }
    
    //    if ([balloons getChildByTag:SECOND_BALOON]) {
    //        [[balloons getChildByTag: SECOND_BALOON] removeFromParentAndCleanup:YES];
    //    }
    
    if ([balloons getChildByTag:THIRD_BALOON]) {
        [[balloons getChildByTag: THIRD_BALOON] removeFromParentAndCleanup:YES];
    }
    
    if (lblSentence) {
        [lblSentence setString:@" "];
        lblSentence = nil;
    }
    
    if (lblbottomBar) {
        [lblbottomBar removeFromParentAndCleanup:YES];
        lblbottomBar = nil;
    }
    
    if (lblCorrectVerb) {
        [lblCorrectVerb removeFromParentAndCleanup:YES];
        lblCorrectVerb = nil;
    }
    
    if ([self getChildByTag:BIRD_TAG]) {
        [self removeChildByTag:BIRD_TAG cleanup:YES];
    }
    
    [self setIsTouchEnabled:NO];
    [self stopAllActions];
    
    //    CCSprite * gameOverSprite = [CCSprite spriteWithFile:@"gameOver.png"];
    //    //backgroundSprite.position = ccp(winSize.width+50, winSize.height+63);
    //    gameOverSprite.position = ccp(550, 310);
    //    //gameOverSprite.anchorPoint = ccp(0, 0);
    //    [self addChild:gameOverSprite];
    //
    //    id action1=[CCScaleTo actionWithDuration:3 scale:1.25];
    //    id action2=[CCScaleBy actionWithDuration:3 scale:.5];
    //    id action3=[CCScaleTo actionWithDuration:3 scale:1.25];
    //    id action4=[CCScaleTo actionWithDuration:3 scale:.5];
    //
    //    CCRepeatForever *repeat = [CCRepeatForever actionWithAction:[CCSequence actions:action1,action2,action3,action4, nil]];
    //    [gameOverSprite runAction:repeat];
    //
    //    [self showPlayerPoints];
    
    
}
-(void) playCheeringSound{
    if (!cheeringSoundTimerPopIt) {
        cheeringSoundTimerPopIt = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                                    target:self
                                                                  selector:@selector(stopTimerAndMakeSoundButtonClickable)
                                                                  userInfo:nil
                                                                   repeats:YES]retain];
        [goToPlayGroundMenu setEnabled:NO];
    }
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    int randomNumer = arc4random() % 3 ;
    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
    }
}

-(void) birdExcitementAnimation{
    
    [self playCheeringSound];
    [[self getChildByTag:BIRD_TAG] stopAllActions];
    
    
    
    CCAnimation *birdAnimation = [CCAnimation animation];
    [birdAnimation setDelayPerUnit:0.12];
    
    int gender = [[NSUserDefaults standardUserDefaults] integerForKey:@"gender"];
    
    if (gender == 1) {
        for (int p = 1; p<7; p++) {
            [birdAnimation addFrameWithFilename:@"Cheering_5.png"];
            [birdAnimation addFrameWithFilename:@"Cheering_7.png"];
            [birdAnimation addFrameWithFilename:@"Cheering_8.png"];
        }
    }
    
    else
        if (gender == 2) {
            for (int p = 1; p<3; p++) {
                [birdAnimation setDelayPerUnit:0.09];
                [birdAnimation addFrameWithFilename:@"GCheering_1.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_2.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_3.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_4.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_5.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_6.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_7.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_8.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_9.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_10.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_11.png"];
                [birdAnimation addFrameWithFilename:@"GCheering_12.png"];
                
            }
        }
    
        else
            if (gender == 3) {
                for (int p = 1; p<3; p++) {
                    [birdAnimation addFrameWithFilename:@"BCheering_5.png"];
                    [birdAnimation addFrameWithFilename:@"BCheering_6.png"];
                    [birdAnimation addFrameWithFilename:@"BCheering_7.png"];
                    [birdAnimation addFrameWithFilename:@"BCheering_8.png"];
                    [birdAnimation addFrameWithFilename:@"BCheering_9.png"];
                    [birdAnimation addFrameWithFilename:@"BCheering_10.png"];
                    [birdAnimation addFrameWithFilename:@"BCheering_11.png"];
                }
                
            }
            else
                if (gender == 4) {
                    for (int p = 1; p<4; p++) {
                        [birdAnimation addFrameWithFilename:@"BCheering-girl_1.png"];
                        [birdAnimation addFrameWithFilename:@"BCheering-girl_2.png"];
                        [birdAnimation addFrameWithFilename:@"BCheering-girl_3.png"];
                        [birdAnimation addFrameWithFilename:@"BCheering-girl_4.png"];
                        [birdAnimation addFrameWithFilename:@"BCheering-girl_5.png"];
                    }
                }
                else
                    if (gender == 5) {
                        for (int p = 1; p<3; p++) {
                            [birdAnimation addFrameWithFilename:@"DCheering_5.png"];
                            [birdAnimation addFrameWithFilename:@"DCheering_6.png"];
                            [birdAnimation addFrameWithFilename:@"DCheering_7.png"];
                            [birdAnimation addFrameWithFilename:@"DCheering_8.png"];
                            [birdAnimation addFrameWithFilename:@"DCheering_9.png"];
                            [birdAnimation addFrameWithFilename:@"DCheering_10.png"];
                            [birdAnimation addFrameWithFilename:@"DCheering_11.png"];
                        }
                        
                    }
                    else
                        if (gender == 6) {
                            for (int p = 1; p<5; p++) {
                                [birdAnimation addFrameWithFilename:@"DCheering-girl_1.png"];
                                [birdAnimation addFrameWithFilename:@"DCheering-girl_2.png"];
                                [birdAnimation addFrameWithFilename:@"DCheering-girl_3.png"];
                                [birdAnimation addFrameWithFilename:@"DCheering-girl_4.png"];
                                [birdAnimation addFrameWithFilename:@"DCheering-girl_5.png"];
                            }
                            
                        }
                        else
                            if (gender == 7) {
                                for (int p = 1; p<3; p++) {
                                    [birdAnimation addFrameWithFilename:@"CCheering_5.png"];
                                    [birdAnimation addFrameWithFilename:@"CCheering_6.png"];
                                    [birdAnimation addFrameWithFilename:@"CCheering_7.png"];
                                    [birdAnimation addFrameWithFilename:@"CCheering_8.png"];
                                    [birdAnimation addFrameWithFilename:@"CCheering_9.png"];
                                    [birdAnimation addFrameWithFilename:@"CCheering_10.png"];
                                    [birdAnimation addFrameWithFilename:@"CCheering_11.png"];
                                }
                                
                            }
                            else
                                if (gender == 8) {
                                    for (int p = 1; p<5; p++) {
                                        [birdAnimation addFrameWithFilename:@"CCheering-girl_1.png"];
                                        [birdAnimation addFrameWithFilename:@"CCheering-girl_2.png"];
                                        [birdAnimation addFrameWithFilename:@"CCheering-girl_3.png"];
                                        [birdAnimation addFrameWithFilename:@"CCheering-girl_4.png"];
                                        [birdAnimation addFrameWithFilename:@"CCheering-girl_5.png"];
                                    }
                                    
                                }
    
    
                                else
                                    if (gender == 9) {
                                        for (int p = 1; p<3; p++) {
                                            [birdAnimation addFrameWithFilename:@"FireCheering_5.png"];
                                            [birdAnimation addFrameWithFilename:@"FireCheering_6.png"];
                                            [birdAnimation addFrameWithFilename:@"FireCheering_7.png"];
                                            [birdAnimation addFrameWithFilename:@"FireCheering_8.png"];
                                            [birdAnimation addFrameWithFilename:@"FireCheering_9.png"];
                                            [birdAnimation addFrameWithFilename:@"FireCheering_10.png"];
                                            [birdAnimation addFrameWithFilename:@"FireCheering_11.png"];
                                        }
                                        
                                    }
                                    else
                                        if (gender == 10) {
                                            for (int p = 1; p<5; p++) {
                                                [birdAnimation addFrameWithFilename:@"FireCheering-girl_1.png"];
                                                [birdAnimation addFrameWithFilename:@"FireCheering-girl_2.png"];
                                                [birdAnimation addFrameWithFilename:@"FireCheering-girl_3.png"];
                                                [birdAnimation addFrameWithFilename:@"FireCheering-girl_4.png"];
                                                [birdAnimation addFrameWithFilename:@"FireCheering-girl_5.png"];
                                            }
                                            
                                        }
    
    
                                        else
                                            if (gender == 11) {
                                                for (int p = 1; p<3; p++) {
                                                    [birdAnimation addFrameWithFilename:@"FCheering_5.png"];
                                                    [birdAnimation addFrameWithFilename:@"FCheering_6.png"];
                                                    [birdAnimation addFrameWithFilename:@"FCheering_7.png"];
                                                    [birdAnimation addFrameWithFilename:@"FCheering_8.png"];
                                                    [birdAnimation addFrameWithFilename:@"FCheering_9.png"];
                                                    [birdAnimation addFrameWithFilename:@"FCheering_10.png"];
                                                    [birdAnimation addFrameWithFilename:@"FCheering_11.png"];
                                                }
                                                
                                            }
                                            else
                                                if (gender == 12) {
                                                    for (int p = 1; p<5; p++) {
                                                        [birdAnimation addFrameWithFilename:@"FCheering-girl_1.png"];
                                                        [birdAnimation addFrameWithFilename:@"FCheering-girl_2.png"];
                                                        [birdAnimation addFrameWithFilename:@"FCheering-girl_3.png"];
                                                        [birdAnimation addFrameWithFilename:@"FCheering-girl_4.png"];
                                                        [birdAnimation addFrameWithFilename:@"FCheering-girl_5.png"];
                                                    }
                                                    
                                                }
    
    
                                                else
                                                    if (gender == 13) {
                                                        for (int p = 1; p<3; p++) {
                                                            [birdAnimation addFrameWithFilename:@"ACheering_5.png"];
                                                            [birdAnimation addFrameWithFilename:@"ACheering_6.png"];
                                                            [birdAnimation addFrameWithFilename:@"ACheering_7.png"];
                                                            [birdAnimation addFrameWithFilename:@"ACheering_8.png"];
                                                            [birdAnimation addFrameWithFilename:@"ACheering_9.png"];
                                                            [birdAnimation addFrameWithFilename:@"ACheering_10.png"];
                                                            [birdAnimation addFrameWithFilename:@"ACheering_11.png"];
                                                        }
                                                        
                                                    }
                                                    else
                                                        if (gender == 14) {
                                                            for (int p = 1; p<5; p++) {
                                                                [birdAnimation addFrameWithFilename:@"ACheering-girl_1.png"];
                                                                [birdAnimation addFrameWithFilename:@"ACheering-girl_2.png"];
                                                                [birdAnimation addFrameWithFilename:@"ACheering-girl_3.png"];
                                                                [birdAnimation addFrameWithFilename:@"ACheering-girl_4.png"];
                                                                [birdAnimation addFrameWithFilename:@"ACheering-girl_5.png"];
                                                            }
                                                            
                                                        }
    
    
                                                        else
                                                            if (gender == 15) {
                                                                for (int p = 1; p<3; p++) {
                                                                    [birdAnimation addFrameWithFilename:@"PCheering_5.png"];
                                                                    [birdAnimation addFrameWithFilename:@"PCheering_6.png"];
                                                                    [birdAnimation addFrameWithFilename:@"PCheering_7.png"];
                                                                    [birdAnimation addFrameWithFilename:@"PCheering_8.png"];
                                                                    [birdAnimation addFrameWithFilename:@"PCheering_9.png"];
                                                                    [birdAnimation addFrameWithFilename:@"PCheering_10.png"];
                                                                    [birdAnimation addFrameWithFilename:@"PCheering_11.png"];
                                                                }
                                                                
                                                            }
                                                            else
                                                                if (gender == 16) {
                                                                    for (int p = 1; p<5; p++) {
                                                                        [birdAnimation addFrameWithFilename:@"PCheering-girl_1.png"];
                                                                        [birdAnimation addFrameWithFilename:@"PCheering-girl_2.png"];
                                                                        [birdAnimation addFrameWithFilename:@"PCheering-girl_3.png"];
                                                                        [birdAnimation addFrameWithFilename:@"PCheering-girl_4.png"];
                                                                        [birdAnimation addFrameWithFilename:@"PCheering-girl_5.png"];
                                                                    }
                                                                    
                                                                }
    
    id birdAction = [CCAnimate actionWithAnimation:birdAnimation];
    
    CCSequence* sequence = [CCSequence actions:birdAction, [CCCallFuncN actionWithTarget:self selector:@selector(removeBirdSprite)], nil];
    
    [[self getChildByTag:BIRD_TAG] runAction:sequence];
    
}

-(void) removeBirdSprite{
    //    [self removeChild:[self getChildByTag:BIRD_TAG] cleanup:YES];
    [self removeChildByTag:BIRD_TAG cleanup:YES];
    //    if ([self getChildByTag:BIRD_TAG]) {
    //        [self removeChildByTag:BIRD_TAG cleanup:YES];
    //    }
}

-(void) birdDisappointmentAnimation{
    
    //    CCAnimation *birdAnimation = [CCAnimation animation];
    //    [birdAnimation setDelay:0.11];
    //
    //    for (int p = 1; p<9; p++) {
    //        //            [birdAnimation addFrameWithFilename:[NSString stringWithFormat:@"monkey_idle_0%i.png",p]];
    //
    //        [birdAnimation addFrameWithFilename:@"monkey_stun_wake_09.png"];
    //    }
    //
    //    [birdAnimation addFrameWithFilename:@"monkey_stun_wake_10.png"];
    //    [birdAnimation addFrameWithFilename:@"monkey_stun_wake_10.png"];
    //    [birdAnimation addFrameWithFilename:@"monkey_stun_wake_10.png"];
    //    [birdAnimation addFrameWithFilename:@"monkey_idle_07.png"];
    //    [birdAnimation addFrameWithFilename:@"monkey_idle_02.png"];
    //    id birdAction = [CCAnimate actionWithAnimation:birdAnimation];
    //
    //    [[self getChildByTag:BIRD_TAG] runAction:birdAction];
}

-(void) updateCompleteSentence{
    [lblSentence stopAllActions];
    //String Cancatentation to write Make Complete Sentence @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    NSString *completeSentence = [lblSentence string];
    
    completeSentence = [completeSentence stringByReplacingOccurrencesOfString:@"__" withString:missngWord];
    
    [lblSentence setString:completeSentence];
    [lblSentence setColor:ccc3(0, 245, 0)];
    
    
    //Updating Label of Bottom Bar
    [lblbottomBar setString:@"Correct Answer"];
}

-(void) showSentence{
    
    //    lblSentence = [CCLabelTTF labelWithString:sentence fontName:@"Arial" fontSize:48];
    
    lblSentence = [CCLabelTTF labelWithString:sentence dimensions:CGSizeMake(1000, 165) alignment:UITextAlignmentCenter lineBreakMode:UILineBreakModeClip fontName:@"Georgia" fontSize:48];
    
    lblSentence.color = ccc3(60,60,60);
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    // position the label on the center of the screen
    lblSentence.position =  ccp( 520 , size.height/2+60);
    [self addChild:lblSentence z:100];
    
    CCTintTo* tint1 = [CCTintTo actionWithDuration:3 red:255 green:0 blue:0];
    CCTintTo* tint2 = [CCTintTo actionWithDuration:3 red:0 green:0 blue:255];
    CCTintTo* tint3 = [CCTintTo actionWithDuration:3 red:0 green:255 blue:0];
    
    CCSequence* sequence = [CCSequence actions:tint1, tint2, tint3, nil];
    
    CCRepeatForever *repeat = [CCRepeatForever actionWithAction:sequence];
    [lblSentence runAction:repeat];
    
}

-(void) callBubblesAnimation{
    
    balloons = [BaloonsGroup balloons:missngWord];
    [self addChild:balloons z:300];
    
}

-(void) bottomBarLabel{
    
    //    lblbottomBar = [CCLabelTTF labelWithString:@"Correct Verb Is : " dimensions:CGSizeMake(1000, 165) alignment:UITextAlignmentCenter lineBreakMode:UILineBreakModeClip fontName:@"Georgia" fontSize:48];
    
    lblbottomBar = [CCLabelTTF labelWithString:@" " dimensions:CGSizeMake(1000, 165) alignment:UITextAlignmentCenter lineBreakMode:UILineBreakModeClip fontName:@"Georgia" fontSize:48];
    
    lblbottomBar.color = ccc3(60,60,60);
    
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    // position the label on the center of the screen
    lblbottomBar.position =  ccp( 520 , size.height/2-370);
    [self addChild:lblbottomBar z:100];
    
}

- (void)ccTouchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    
    if (lblCorrectVerb)
    {
        [lblCorrectVerb stopAllActions];
        [self removeChild:lblCorrectVerb cleanup:YES];
        lblCorrectVerb = nil;
        NSLog(@"Correct Word has been Removed");
    }
    
    //    [self showSentence];
    [self callBubblesAnimation];
    [self bottomBarLabel];
    self.isTouchEnabled = false;
    
}
-(void) removeScene{
    
    [self saveProgressForReport];
    
    [tempContentDic release];
    tempContentDic = Nil;
    [self gameEnded];
    [self unscheduleAllSelectors];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    }
    
    //    [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:0.1f], [CCCallFunc actionWithTarget:self selector:@selector(gameEnd)], nil]];
    [[CCDirector sharedDirector] end];
    [[CCDirector sharedDirector].view removeFromSuperview];
    //    [CCDirector sharedDirector].
    
}

//-(void) gameEnd{
//
//    [[CCDirector sharedDirector] end];
//    [[CCDirector sharedDirector].openGLView removeFromSuperview];
//
//}

//-(void) loadDatabase{
//
//
//        MrPDatabase *database = [[MrPDatabase alloc] init];
//
////        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
////        int LanguageSupport = [[NSUserDefaults standardUserDefaults] integerForKey:@"LanguageSupport"];
//        int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
//
//        //    int cId = CURRENT_CLASSIFICATION;
//        int position = 0;
//        NSArray *MrPContentArray = [database EnglishWordDatabaseArray:cId];
//
//        tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
//        for (MrPContentInfo *info in MrPContentArray) {
//
//            [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
//            [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
//            [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
//            position++;
//
//        }
//
//}

-(void) loadDatabase{
    
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    //    int LanguageSupport = [[NSUserDefaults standardUserDefaults] integerForKey:@"LanguageSupport"];
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    
    int position = 0;
    
    // cId 99999 means its myskillscamp, otherwise its simple classification
    if (cId == 99999) {
        
        
        gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
        int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
        
        
        int position = 0;
        
        if (activeLanguage == 2) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
//                [tempContentDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
//                [tempContentDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];
                position++;
            }
            [self loadDummyData];
        }
        
        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
//                [tempContentDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
//                [tempContentDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];
                position++;
            }
            
            [self loadDummyData];
        }
    }
    
    
    
    // if goes in the loop below, it means its simple classification from city map
    else
    {
        if (activeLanguage == 2 || cId > 989  ) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:cId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                //                if (info.isSentence == 1) {
                //
                //                }
                //                else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                [tempContentDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
                [tempContentDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];

                position++;
                //                }
                
            }
        }
        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:cId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                //                if (info.isSentence == 1) {
                //
                //                }
                //                else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]]; // keeping it english word so in class, our convention remain same
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                [tempContentDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
                [tempContentDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];

                position++;
                //                }
            }
        }
    }
    [database release];
    database = Nil;
}

-(void) loadDummyData{
    int attemptsOffindingsecondGrammarId = 0;
    BOOL dataLoadedSuccessfully = NO;
    do {
        MrPDatabase *database = [[MrPDatabase alloc] init];
        NSArray* MrPGrammarArray = [database MySkillsGrammerArray];
        
        NSMutableDictionary* _tempGrammarContentDicToGetPid = [[NSMutableDictionary alloc] initWithCapacity:[MrPGrammarArray count]];
        int position = 0;
        for (MrPContentInfo *infos in MrPGrammarArray) {
            
            [_tempGrammarContentDicToGetPid setObject:[NSNumber numberWithInt:infos.grammarId] forKey:[NSString stringWithFormat:@"grammarId%d",position]];
            [_tempGrammarContentDicToGetPid setObject:[NSNumber numberWithInt:infos.principleId] forKey:[NSString stringWithFormat:@"principleId%d",position]];
            position++;
        }
        
        position  = position - 1;
        if (position < 1) {
            
            
            NSArray * dummyArray;
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"]== 1) {
                
                dummyArray = [[NSArray alloc] initWithObjects:@"كَتَبَ", @"وَلَدٌ رَائِعٌ", @"لا تَرْسُمْ", @"أُرْسُمْ", @"هناك",@"يَكْتُبُ", @"كُتُبٌ", @"يُسافِرُ", @"يُسافِرون", @"شَكَرْتُه",@"شَكَرْتُهم", @"شَكَرْتُهم", @"جِدًّا", @"آلَةٌ", @"الطَّعامِ",@"الْعُصْفورُ", @"فَراشاتٍ", @"ثَوْرٌ", @"بَبُطْءٍ", @"دَعوني",@"اُتْرُكوني", @"أُحِبُّهُ", @"يُراقِبُ", @"تَخْتَلِطُ", @"رِجالٌ",@"أُحِبُّهُ", @"مَنَحَ", @"خِلالِ", @"تَدورُ", @"ظَلَّ", nil];
                
            }
            else{
                dummyArray = [[NSArray alloc] initWithObjects:@"This", @"are", @"is", @"was", @"shall",@"This", @"it", @"would", @"could", @"may",@"can", @"shall", @"must", @"could", @"swim",@"play", @"think", @"go", @"come", @"boldly",@"bravely", @"are", @"is", @"was", @"shall",@"This", @"it", @"would", @"could", @"may", nil];
                
            }
            _dummyGrammarDic = [[NSMutableDictionary alloc] initWithCapacity:30];
            for (int posi = 0; posi < 30; posi ++){
                [_dummyGrammarDic setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"wordId%d",posi]];
                [_dummyGrammarDic setObject:[NSString stringWithFormat:@"%@",[dummyArray objectAtIndex:posi] ] forKey:[NSString stringWithFormat:@"englishWord%d",posi]];
                //                [_dummyGrammarDic setObject:@"OK" forKey:[NSString stringWithFormat:@"englishWord%d",posi]];
                [_dummyGrammarDic setObject:@"EmptySound" forKey:[NSString stringWithFormat:@"voiceId%d",posi]];
                posi++;
            }
            [dummyArray release];
            dummyArray = Nil;
            
            //            _dummyGrammarDic = [[NSMutableDictionary alloc] initWithCapacity:3];
            //            [_dummyGrammarDic setObject:@"" forKey:[NSString stringWithFormat:@"wordId%d",position]];
            //            [_dummyGrammarDic setObject:@"" forKey:[NSString stringWithFormat:@"englishWord%d",position]];
            //            [_dummyGrammarDic setObject:@"" forKey:[NSString stringWithFormat:@"voiceId%d",position]];
            //
            //
            //            UIAlertView *alert = [[UIAlertView alloc] init];
            //            [alert setTitle:@"CAUTION"];
            //            [alert setMessage:@"Data not sufficient in selected Grade. Please select anyother grade from setup screen or purchase new package"];
            //            [alert setDelegate:self];
            //            [alert addButtonWithTitle:@"OK"];
            //            [alert setTag:2];
            //            [alert show];
            //            [alert release];
            //            insufficientDataInSelectedSkillsPackage = TRUE;
            //
            //            database = Nil;
            //            [_dummyGrammarDic release];
            //            _dummyGrammarDic = Nil;
            //
            //            [tempContentDic release];
            //            tempContentDic = Nil;
            //            [_tempGrammarContentDicToGetPid release];
            //
            //            _tempGrammarContentDicToGetPid = Nil;
            break;
            
            
        }
        int dummerGrammarId = (arc4random() % position);
        //        int dummerGrammarId = (arc4random() % 100 + 1) * 10;
        
        //        if (dummerGrammarId == gId) {
        //            dummerGrammarId = dummerGrammarId + 1;
        //        }
        //        int dummyPrincipleId;
        
        int secondGrammarId = [[_tempGrammarContentDicToGetPid objectForKey:[NSString stringWithFormat:@"grammarId%d",dummerGrammarId]] intValue] ;
        int secondPrincipleId = [[_tempGrammarContentDicToGetPid objectForKey:[NSString stringWithFormat:@"principleId%d",dummerGrammarId]] intValue] ;
        
        NSLog(@"SEcond grammar Id is %d and principle Id is %d",secondGrammarId,secondPrincipleId);
        [_tempGrammarContentDicToGetPid release];
        
        _tempGrammarContentDicToGetPid = Nil;
        
        
        // send query to database against selected gId and pId
        position = 0;
        NSArray *MrPDummyArray;
        if (activeLanguage == 2) {
            MrPDummyArray = [database EnglishWordDatabaseArray:secondGrammarId andpId:secondPrincipleId];
        }
        else {
            MrPDummyArray = [database ArabicWordDatabaseArray:secondGrammarId andpId:secondPrincipleId];
        }
        //        NSArray *MrPDummyArray = [database EnglishWordDatabaseArray:dummerGrammarId andpId:dummyPrincipleId];
        [database release];
        database = Nil;
        
        if ([MrPDummyArray count] > 1 && secondGrammarId != gId) {
            dataLoadedSuccessfully = YES;
        }
        else{
            attemptsOffindingsecondGrammarId = attemptsOffindingsecondGrammarId + 1;
            if(attemptsOffindingsecondGrammarId > 10)
            {
                //                UIAlertView *alert = [[UIAlertView alloc] init];
                //                [alert setTitle:@"CAUTION"];
                //                [alert setMessage:@"Data not sufficient in selected Grade. Please select anyother grade from setup screen or purchase new package"];
                //                [alert setDelegate:self];
                //                [alert addButtonWithTitle:@"OK"];
                //                [alert setTag:2];
                //                [alert show];
                //                [alert release];
                //                insufficientDataInSelectedSkillsPackage = TR``````````````````````UE;
                dataLoadedSuccessfully = YES;
            }
        }
        _dummyGrammarDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPDummyArray count]];
        if ([MrPDummyArray count] < 1) {
            NSArray * dummyArray;
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"]== 1) {
                
                dummyArray = [[NSArray alloc] initWithObjects:@"كَتَبَ", @"وَلَدٌ رَائِعٌ", @"لا تَرْسُمْ", @"أُرْسُمْ", @"هناك",@"يَكْتُبُ", @"كُتُبٌ", @"يُسافِرُ", @"يُسافِرون", @"شَكَرْتُه",@"شَكَرْتُهم", @"شَكَرْتُهم", @"جِدًّا", @"آلَةٌ", @"الطَّعامِ",@"الْعُصْفورُ", @"فَراشاتٍ", @"ثَوْرٌ", @"بَبُطْءٍ", @"دَعوني",@"اُتْرُكوني", @"أُحِبُّهُ", @"يُراقِبُ", @"تَخْتَلِطُ", @"رِجالٌ",@"أُحِبُّهُ", @"مَنَحَ", @"خِلالِ", @"تَدورُ", @"ظَلَّ", nil];
                
            }
            else{
                dummyArray = [[NSArray alloc] initWithObjects:@"This", @"are", @"is", @"was", @"shall",@"This", @"it", @"would", @"could", @"may",@"can", @"shall", @"must", @"could", @"swim",@"play", @"think", @"go", @"come", @"boldly",@"bravely", @"are", @"is", @"was", @"shall",@"This", @"it", @"would", @"could", @"may", nil];
                
            }
            _dummyGrammarDic = [[NSMutableDictionary alloc] initWithCapacity:30];
            for (int posi = 0; posi < 30; posi ++){
                [_dummyGrammarDic setObject:[NSNumber numberWithInt:1] forKey:[NSString stringWithFormat:@"wordId%d",posi]];
                [_dummyGrammarDic setObject:[NSString stringWithFormat:@"%@",[dummyArray objectAtIndex:posi] ] forKey:[NSString stringWithFormat:@"englishWord%d",posi]];
                //                [_dummyGrammarDic setObject:@"OK" forKey:[NSString stringWithFormat:@"englishWord%d",posi]];
                [_dummyGrammarDic setObject:@"EmptySound" forKey:[NSString stringWithFormat:@"voiceId%d",posi]];
                posi++;
            }
            [dummyArray release];
            dummyArray = Nil;
            break;
        }
        else{
            for (MrPContentInfo *info in MrPDummyArray) {
                //            if (info.isSentence == 1) {
                //
                //            }
                //            else {
                [_dummyGrammarDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [_dummyGrammarDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [_dummyGrammarDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                position++;
            }
        }
    } while (!dataLoadedSuccessfully);
    
    
}

-(NSMutableDictionary *) getTempContentDic{
    return tempContentDic;
}

-(NSMutableDictionary *) getDummyDic{
    return _dummyGrammarDic ;
}

-(int ) getcurrentWord{
    NSLog(@"Check 1: Current word is %d",currentWord);
    return currentWord;
}

- (void)HelpSound:(id)sender {
    // 159,56
    [[NSUserDefaults standardUserDefaults] setObject:@"GH017" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    CCMenuItem *helpBtnEnglish = [CCMenuItemImage
                                  itemFromNormalImage:@"HelpEnglish.png" selectedImage:@"HelpEnglish.png"
                                  target:self selector:@selector(playHelpSoundEng)];
    
    helpBtnEnglish.position = ccp(901, 640);
    
    CCMenuItem *helpBtnArabic = [CCMenuItemImage
                                 itemFromNormalImage:@"HelpArabic.png" selectedImage:@"HelpArabic.png"
                                 target:self selector:@selector(playHelpSoundAra)];
    
    helpBtnArabic.position = ccp(901, 585);
    
    helpMenu = [CCMenu menuWithItems:helpBtnEnglish,helpBtnArabic, nil];
    helpMenu.anchorPoint = ccp(0, 0);
    helpMenu.position = ccp(0, 0);
    [self addChild:helpMenu];
}

-(void) playHelpSoundEng{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayEnglishHelpSound:nil];
    //    [helpSoundVC release];
    //    helpSoundVC = Nil;
    
    
}
-(void) playHelpSoundAra{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayArabicHelpSound:nil];
    //    [helpSoundVC release];
    //    helpSoundVC = Nil;
    
}

-(void) saveProgressForReport{
    // write in db!
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database saveReportProgress:retval];
    
    //    NSLog(@"temp is %@",tempContentDic);
    
    if (retval) {
        [retval removeAllObjects];
        [retval release];
        retval = nil;
    }
}
-(void) updateReportCurrentWord{
    reportWordDesc = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",currentWord]];
    reportWordId = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord]] intValue];
    int gameId = 3;
    MrPReport *info = [[MrPReport alloc]
                       initWithWordId:reportWordId andWordDesc:reportWordDesc andCorrectCount:reportCorrectCount andTotalCount:reportTotatCount andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
    
    [retval addObject:info];
    
    reportTotatCount = 0;
    reportCorrectCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
    
}

-(void) insitiantiateForReportCouting{
    retval = [[NSMutableArray alloc] init];
    reportCorrectCount = 0;
    reportTotatCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
}



- (void) dealloc
{
    //	[super dealloc];
}
@end
