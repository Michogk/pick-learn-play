//
//  ExpressItSetOrderViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 28/09/2013.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ExpressItSetOrderViewController : UIViewController<AVAudioPlayerDelegate,AVAudioRecorderDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate>{

    AVAudioPlayer *expressItBackgroundPlayer;
    AVAudioPlayer *wordPlayer;
    NSMutableDictionary* tempContentDic;
    int currentWord;
    int cId;
    NSString* activeGrade;
    int activeLanguage;
    NSMutableArray *tagsArr;
    int arabicCurrentWord;
    AVAudioPlayer *player;
    NSMutableDictionary *recordSetting;
    NSMutableDictionary *editedObject;
    NSString *recorderFilePath;
    AVAudioRecorder *recorder;
    SystemSoundID soundID;
    NSTimer *timer;
    NSMutableDictionary *framesOfTaggedImages;
    IBOutlet UIScrollView *scrollViewBackground;
}
- (IBAction)btnEditPressed:(id)sender;
- (IBAction)btnDonePressed:(id)sender;

@end
