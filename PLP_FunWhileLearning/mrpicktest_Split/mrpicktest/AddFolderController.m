//
//  AddFolderController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/24/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "AddFolderController.h"
//#import "FoldersTableViewController.h"
#import "MyHomeViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"

@interface AddFolderController ()

@end

@implementation AddFolderController
@synthesize txtFolder;
@synthesize btnAdd;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setTxtFolder:nil];
    [self setBtnAdd:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)CancelAddFolder:(id)sender {
        [self.txtFolder resignFirstResponder];
        [self dismissModalViewControllerAnimated:NO];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (IBAction)AddFolder:(id)sender {
    
    [self.txtFolder resignFirstResponder];
    int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSString *folderNameText = txtFolder.text;
    
    NSCharacterSet* englishSet = [NSCharacterSet characterSetWithCharactersInString:
                            @"abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVXYZ"];
    
    NSCharacterSet* arabicSet = [NSCharacterSet characterSetWithCharactersInString:
                                 @"ابتثجخدذرزسشصضطظعغفقكلمنهوي"];
    
    if (activeLanguage == 1) {
        // Perform Arabic Test
        BOOL languageTest = TRUE;
        languageTest = [[folderNameText stringByTrimmingCharactersInSet:englishSet] isEqualToString:@""];
        
        if (languageTest) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            [alert setTitle:@"WARNING"];
            [alert setMessage:@"SORRY! You can only add Folder in Arabic Language OR go to setup screen and switch language "];
            [alert addButtonWithTitle:@"OK"];
            [alert setDelegate:self];
            [alert show];
            [alert release];
            return;
        }
    }
    else{
        // Perform English Test
        BOOL languageTest = TRUE;
        languageTest = [[folderNameText stringByTrimmingCharactersInSet:arabicSet] isEqualToString:@""];
        
        
        if (languageTest) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            [alert setTitle:@"WARNING"];
            [alert setMessage:@"SORRY! You can only add Folder in English Language OR go to setup screen and switch language"];
            [alert addButtonWithTitle:@"OK"];
            [alert setDelegate:self];
            [alert show];
            [alert release];
            return;
            return;
        }
    }
    
    
   // Reading Database        
    MrPDatabase *database = [[MrPDatabase alloc] init];
    NSArray *foldersInfoArray = [database ReadFolderDatabase];
    NSMutableDictionary *folderDic = [[NSMutableDictionary alloc] initWithCapacity:20];
    for (MrPContentInfo *info in foldersInfoArray) {
        [folderDic setObject:[NSString stringWithFormat:@"%d",info.folderid] forKey:[NSString stringWithFormat:@"folderId%d",info.folderid]];
        [folderDic setObject:info.foldername forKey:[NSString stringWithFormat:@"folderName%d",info.folderid]];
    }

    for (int i = 990; i < 999; i++) {
        int folderid = [[folderDic objectForKey:[NSString stringWithFormat:@"folderId%d",i]] intValue];
        NSString *folderNameExtracted = [folderDic objectForKey:[NSString stringWithFormat:@"folderName%d",i]];

        if ([folderNameExtracted isEqualToString:@"Capture your words"] || ([folderNameExtracted isEqualToString:@"كلماتك"])) {
            
            NSString *folderName = txtFolder.text;    
            int classificationId = folderid;
            //            int classificationId = info.folderid;
            NSLog(@"integer value is %d", classificationId);
            
            MrPDatabase *databaseWriteMode = [[MrPDatabase alloc] init];
            [databaseWriteMode saveNewFolder:folderName andClassificationId:classificationId];
                   
            [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"Reload" object:self]];
            
            [self dismissModalViewControllerAnimated:NO];
            [database release];
            [databaseWriteMode release];
            return;
        }

    }
    UIAlertView *alert = [[UIAlertView alloc] init];
    
//    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];        
    if (activeLanguage == 2) {
        [alert setTitle:@"WARNING"]; 
        [alert setMessage:@"All folders are used. First, delete one folder then you may create a new one."];
        [alert addButtonWithTitle:@"OK"];
    }                
    else {
        [alert setTitle:@"تحذير !"]; 
        [alert setMessage:@"لقد تم استعمال كافة الملفات. يجب الغاء احد الملفات ثم ادخال ملف جديد"];
        [alert addButtonWithTitle:@"موافق"];
        
    }

    [alert setDelegate:self];
    [alert show];
    [alert release];
}
  
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
                [self dismissModalViewControllerAnimated:NO];
}

@end
