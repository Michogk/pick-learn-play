//
//  introViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 2/7/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface introViewController : UIViewController<AVAudioPlayerDelegate>{
    UIScrollView* scrollView;
    UIPageControl* pageControl;
    BOOL pageControlBeingUsed;
    
    AVAudioPlayer *introBackgroundMusic;
}
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIPageControl *pageControl;
- (IBAction)changePage:(id)sender;
- (IBAction)endIntro:(id)sender;

@end
