//
//  MyStoreViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/30/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookStoreTableViewController.h"

@interface MyStoreViewController : UIViewController {

    UIAlertView *downlaodingStatusAlert;
    BookStoreTableViewController *bookStoreTVC;
    
    // Animation during downloading
    NSTimer *characterWalkingTimer;
    UIImageView* characterWalkingAnimationImgView;
    int characterAnimationStep;
    int animationImageNumber;
    NSString *animationImageDirection;
    NSString* animationImageGender;
    int controlRepitation;
}

- (IBAction)HelpSound:(id)sender;
- (IBAction)exitMyStore:(id)sender;
-(void) loadPackageTableView:(id) sender;
- (IBAction)helpURL:(id)sender;

@end
