//
//  MatchIt.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MatchIt.h"
#import "DrawingUIView.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "DocumentDirectory.h"
#import <QuartzCore/QuartzCore.h>
#import "MrPReport.h"
#import "Common.h"

@interface MatchIt ()

@end

@implementation MatchIt
@synthesize btnCheck;
@synthesize btnReset;
@synthesize btnNext;
@synthesize checkImg1;
@synthesize checkImg2;
@synthesize checkImg3;
@synthesize checkImg4;
@synthesize btnWord1;
@synthesize btnWord2;
@synthesize btnWord3;
@synthesize BtnWord4;
@synthesize BtnImage1;
@synthesize BtnImage2;
@synthesize BtnImage3;
@synthesize BtnImage4;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if ([ActiveGrade isEqualToString:@"FREE"]) {
    _bannerView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    _bannerView.requiredContentSizeIdentifiers = [NSSet setWithObject:ADBannerContentSizeIdentifierLandscape];
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    
    CGRect adFrame = _bannerView.frame;
    adFrame.origin.y = self.view.frame.size.width-_bannerView.frame.size.height;
    _bannerView.frame = adFrame;
    _bannerView.delegate = self;
    }
    //    [self.view addSubview:_bannerView];
    
   
    
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];    
    NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    if([objectClassification isKindOfClass:[NSArray class]] == YES)
    {
        cId = [[objectClassification objectAtIndex:0] intValue];
    }

    [self insitiantiateForReportCouting];
    int position = 1;
    MrPDatabase *database = [[MrPDatabase alloc] init];    
    NSArray *MrPContentArray;
    if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
        MrPContentArray = [database EnglishWordDatabaseArray:cId];
//        MrPContentArray = [database databaseForParticularClassification:cId andTable:@"english_word"];
    }
    else {
        MrPContentArray = [database ArabicWordDatabaseArray:cId];
//        MrPContentArray = [database databaseForParticularClassification:cId andTable:@"arabic_word"];
    }
    
    [database release];
     database = Nil;
    tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    
    for (MrPContentInfo *info in MrPContentArray) {
//        if (info.isSentence) {
//            
//        }
//        else {
            [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
            if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
                [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];                
            }
            else {
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
            }

            
            [tempContentDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]];
            [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
            if (activeLanguage == 2) {
                [tempContentDic setObject:[NSNumber numberWithInt:info.arabicRef] forKey:[NSString stringWithFormat:@"arabicRef%d",position]];
            }
            else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.english_ref] forKey:[NSString stringWithFormat:@"arabicRef%d",position]];
            }
;
            [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
            [tempContentDic setObject:[NSNumber numberWithInt:info.translate] forKey:[NSString stringWithFormat:@"translate%d",position]];
            position++; 
//        }           
    }    
    
    if ([tempContentDic count] < 28) {
        UIAlertView *alert = [[UIAlertView alloc] init];

        
        activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        
        if (activeLanguage == 1) {
            [alert setTitle:@"تحذير !"]; 
            [alert setMessage:@"البيانات غير كافية لأستمرار"];
            [alert addButtonWithTitle:@"موافق"];
        }
        else {
            [alert setTitle:@"WARNING"]; 
            [alert setMessage:@"SORRY! Data No Sufficient."];
            [alert addButtonWithTitle:@"OK"];  
        }
                [alert setDelegate:self];
        [alert setTag:20];
        [alert show];
        [alert release]; 
        return;
    }
    
    fistTimeLoad = TRUE;
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"userResults"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:0 forKey:@"positionArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self playBackGroundMusic];
    [self updataGameElements];
    
    // Level Counter Label
    NSString *points1 = [NSString stringWithFormat:@"Game%dLevel%d",4,1];
    _lblLevel1 = [[UILabel alloc] initWithFrame:CGRectMake(670, 36, 300, 25)];
    NSString *currentLevel = [NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:points1]];
    
    [_lblLevel1 setText:currentLevel];
    UIFont *font = [UIFont systemFontOfSize: 31];
    [_lblLevel1 setFont:font];
    _lblLevel1.textAlignment = UITextAlignmentCenter;
    [_lblLevel1 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel1];
    
    NSString *points2 = [NSString stringWithFormat:@"Game%dLevel%d",4,2];
    _lblLevel2 = [[UILabel alloc] initWithFrame:CGRectMake(670, 62, 300, 25)];
    currentLevel = [NSString stringWithFormat:@"Level 02 : %d", [[NSUserDefaults standardUserDefaults] integerForKey:points2]];
    [_lblLevel2 setText:currentLevel];
    font = [UIFont systemFontOfSize: 31];
    [_lblLevel2 setFont:font];
    _lblLevel2.textAlignment = UITextAlignmentCenter;
    [_lblLevel2 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel2];
    [self createBtnBorders];
    
}

-(void) createBtnBorders{
    btnWord1.layer.borderWidth = 2.0f;
    btnWord1.layer.borderColor = [[UIColor grayColor] CGColor];
    btnWord2.layer.borderWidth = 2.0f;
    btnWord2.layer.borderColor = [[UIColor grayColor] CGColor];
    btnWord3.layer.borderWidth = 2.0f;
    btnWord3.layer.borderColor = [[UIColor grayColor] CGColor];
    BtnWord4.layer.borderWidth = 2.0f;
    BtnWord4.layer.borderColor = [[UIColor grayColor] CGColor];
    
    BtnImage1.layer.borderWidth = 2.0f;
    BtnImage1.layer.borderColor = [[UIColor grayColor] CGColor];
    BtnImage2.layer.borderWidth = 2.0f;
    BtnImage2.layer.borderColor = [[UIColor grayColor] CGColor];
    BtnImage3.layer.borderWidth = 2.0f;
    BtnImage3.layer.borderColor = [[UIColor grayColor] CGColor];
    BtnImage4.layer.borderWidth = 2.0f;
    BtnImage4.layer.borderColor = [[UIColor grayColor] CGColor];


}

- (void)viewDidUnload
{
    [self setBtnWord1:nil];
    [self setBtnWord2:nil];
    [self setBtnWord3:nil];
    [self setBtnWord4:nil];
    [self setBtnImage1:nil];
    [self setBtnImage2:nil];
    [self setBtnImage3:nil];
    [self setBtnImage4:nil];
    [self setCheckImg1:nil];
    [self setCheckImg2:nil];
    [self setCheckImg4:nil];
    [self setCheckImg3:nil];
    [self setBtnCheck:nil];
    [self setBtnReset:nil];
    [self setBtnNext:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)checkBtn:(id)sender {
    

    NSArray* positionArray = [[[NSUserDefaults standardUserDefaults] objectForKey:@"positionArray"] retain];
    NSDictionary* userResults = [[[NSUserDefaults standardUserDefaults] objectForKey:@"userResults"] retain];
                NSLog(@"Position Result %@ ",userResults);
    if (userResults == NULL) {
        NSLog(@"Insufficient Answers");
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Wait!"]; 
        [alert setMessage:@"Please match all elements and try again"];
        [alert setDelegate:self];
        [alert addButtonWithTitle:@"OK"];
        [alert setTag:200];
        [alert show];
        [alert release]; 
        return;
    }

    int correctAnswersCounter = 0;
    for (int i = 0; i < [userResults count]; i++) {
        int userAnswer = [[userResults objectForKey:[NSString stringWithFormat:@"%d",i]] intValue];
        if (userAnswer == 0) {
            userAnswer = 1;
        }

        int imageOnUserAnsweredPosition = [[positionArray objectAtIndex:userAnswer-1] intValue]; 
        if (imageOnUserAnsweredPosition == i+1) {
            [(UIImageView*)[self.view viewWithTag:userAnswer] setImage:[UIImage imageNamed:@"check_mark_Matchit.png"]];
            // SAve score here
            [self correctItemSaveForReportLocally:i+1];
            NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],5,cId,activeLanguage];
            int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
            if (!LevelLastCount) {
                LevelLastCount = 1;
            }
            
            NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",4,LevelLastCount];
            float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
            if (!PointsLastCount) {
                PointsLastCount = 0;
            }
            
            if (LevelLastCount < 3) {
                PointsLastCount = PointsLastCount + 2;
                [[NSUserDefaults standardUserDefaults] setInteger:PointsLastCount forKey:Points];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            NSLog(@"Answer is correct");
            

            NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",4,1];
            [_lblLevel1 setText:[NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]]];
            
            NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",4,2];
            [_lblLevel2 setText:[NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]]];

            NSLog(@"Points one are %d and points 2 are %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1],[[NSUserDefaults standardUserDefaults] integerForKey:Points2]);
            
            correctAnswersCounter = correctAnswersCounter + 1;
        }
        else {
            NSLog(@"Wrong Answer");
        }

    }
    
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"userResults"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    if (correctAnswersCounter == 4) {        
        if (matchItPlayer) {
            [matchItPlayer stop];
            [matchItPlayer release];
            matchItPlayer = Nil;
        }
        
        [btnNext setEnabled:NO];
        [btnReset setEnabled:NO];
        [btnCheck setEnabled:NO];
        
        NSString* fileName = [NSString stringWithFormat:@"fireworks"];
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        fireWorkPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        fireWorkPlayer.volume = 2.0;
        fireWorkPlayer.numberOfLoops = 20;
        fireWorkPlayer.delegate = self;
        
        if ([fireWorkPlayer prepareToPlay]) {
            [fireWorkPlayer play];            
        }  
        
        playFireWorksTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(changeFireWork) userInfo:nil repeats:NO];
        
        UIView *fireWorksView = [[UIView alloc] initWithFrame:CGRectMake(300, 300, 300, 300)];
        [fireWorksView setTag:35];
        UIImageView *fireWorksImgView = [[UIImageView alloc] init];//WithImage:[UIImage imageNamed:@"bluefireWork.png"]];
        [fireWorksView addSubview:fireWorksImgView];    
        [self.view addSubview:fireWorksView];
        
        myLayer = fireWorksView.layer;
        UIImage *image = [UIImage imageNamed:@"bluefireWork.png"];
        myLayer.contents = (id)image.CGImage;
        fireWorksView.layer.cornerRadius = 20.0;
        
//        CABasicAnimation *animationOne = [CABasicAnimation animationWithKeyPath:@"transform"]; 
//        animationOne.beginTime = 0;
//        animationOne.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//        animationOne.toValue =   [NSValue valueWithCATransform3D:CATransform3DMakeScale(4.0, 4.0, 4.0)];    
//        [animationOne setDuration:2.0];
//        animationOne.fillMode=kCAFillModeBackwards;
//        animationOne.removedOnCompletion=YES;
//        [myLayer addAnimation:animationOne forKey:@"zoom"];
        
        CABasicAnimation *animationOne = [CABasicAnimation animationWithKeyPath:@"opacity"];
        animationOne.duration = 2.0;
        animationOne.removedOnCompletion = YES;
        animationOne.fillMode = kCAFillModeForwards;
        animationOne.toValue = [NSNumber numberWithFloat:0.0f];
        [myLayer addAnimation:animationOne forKey:@"animateOpacity"];
                [self updateReportCurrentWord];
    }
    
    else {
                [self updateReportCurrentWord];
    moveToNextSetTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(BtnNextPressed:) userInfo:nil repeats:NO];
        
    }
    [positionArray release];
    positionArray = Nil;
    [userResults release];
    userResults = Nil;
}

-(void) changeFireWork{
    if (playFireWorksTimer) {
        [playFireWorksTimer invalidate];
        playFireWorksTimer = nil;
    }
    playFireWorksTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(lastFireWork) userInfo:nil repeats:NO];
    
    [[self.view viewWithTag:35] setFrame:CGRectMake(150, 220, 300, 300)];
    
//    CABasicAnimation *animationTwo = [CABasicAnimation animationWithKeyPath:@"transform"]; 
////    animationTwo.beginTime = 1.5;
//    myLayer.contents = (id)[UIImage imageNamed:@"OrangefireWork.png"].CGImage;
//    animationTwo.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//    animationTwo.toValue =   [NSValue valueWithCATransform3D:CATransform3DMakeScale(4.0, 4.0, 4.0)];    
//    [animationTwo setDuration:2.0];
//    animationTwo.fillMode=kCAFillModeBackwards;
//    animationTwo.removedOnCompletion=YES;
//            [myLayer addAnimation:animationTwo forKey:@"zoom"];
    
    CABasicAnimation *animationTwo = [CABasicAnimation animationWithKeyPath:@"opacity"];
    myLayer.contents = (id)[UIImage imageNamed:@"OrangefireWork.png"].CGImage;
    animationTwo.duration = 1.5;
    animationTwo.removedOnCompletion = YES;
    animationTwo.fillMode = kCAFillModeForwards;
    animationTwo.toValue = [NSNumber numberWithFloat:0.0f];
    [myLayer addAnimation:animationTwo forKey:@"animateOpacity"];
    
    
    
}

-(void) lastFireWork{
    if (playFireWorksTimer) {
        [playFireWorksTimer invalidate];
        playFireWorksTimer = nil;
    }
    
    [[self.view viewWithTag:35] setFrame:CGRectMake(450, 320, 300, 300)];
    
    if (fireWorkPlayer) {
        [fireWorkPlayer stop];
        [fireWorkPlayer release];
        fireWorkPlayer = nil;
    }
    
    NSString* fileName = [NSString stringWithFormat:@"fireworks"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];        
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    fireWorkPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    fireWorkPlayer.volume = 2.0;
    fireWorkPlayer.numberOfLoops = 20;
    fireWorkPlayer.delegate = self;
    
    if ([fireWorkPlayer prepareToPlay]) {
        [fireWorkPlayer play];            
    } 
    
    
    playFireWorksTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(exitFireWork) userInfo:nil repeats:NO];
    
    CABasicAnimation *animationThree = [CABasicAnimation animationWithKeyPath:@"opacity"];
    myLayer.contents = (id)[UIImage imageNamed:@"purplefireWork.png"].CGImage;
    animationThree.duration = 2.5;
    animationThree.removedOnCompletion = YES;
    animationThree.fillMode = kCAFillModeForwards;
    animationThree.toValue = [NSNumber numberWithFloat:0.0f];
    [myLayer addAnimation:animationThree forKey:@"animateOpacity"];
}

-(void) exitFireWork{
    if (playFireWorksTimer) {
        [playFireWorksTimer invalidate];
        playFireWorksTimer = nil;
    }
    
    if (fireWorkPlayer) {
        [fireWorkPlayer stop];
        [fireWorkPlayer release];
        fireWorkPlayer = nil;
    }
    
    [btnNext setEnabled:YES];
    [btnReset setEnabled:YES];
    [btnCheck setEnabled:YES];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        NSString* fileName = [NSString stringWithFormat:@"5-MatchIT"];
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        matchItPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        matchItPlayer.volume = 0.15;
        matchItPlayer.numberOfLoops = 20;
        matchItPlayer.delegate = self;
        
        if ([matchItPlayer prepareToPlay]) {
            [matchItPlayer play];
        }  

    }
    
    if ([self.view viewWithTag:35]) {
        [[self.view viewWithTag:35] removeFromSuperview];
//        [[self.view viewWithTag:35] release];
    }
    
    if (myLayer) {
        [myLayer removeFromSuperlayer];
        myLayer = Nil;  
    }

    [self BtnNextPressed:nil];

}


- (IBAction)BtnNextPressed:(id)sender {

    fistTimeLoad = FALSE;
    
    NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
    int WordCounterStored = [[[NSUserDefaults standardUserDefaults] objectForKey:wordCount] intValue];
    WordCounterStored  =WordCounterStored + 5;
    [[NSUserDefaults standardUserDefaults] setInteger:WordCounterStored forKey:wordCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self updataGameElements];
}

- (IBAction)btnResetPressed:(id)sender {
    
   
    [self updataGameElements];
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"userResults"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)btnGoToPlaygroundPressed:(id)sender {
    if (matchItPlayer) {
        [matchItPlayer stop];
        [matchItPlayer release];
        matchItPlayer = Nil;
    }
    [self saveProgressForReport];
}

- (IBAction)btnHelpURLPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"myplayground" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)btnHelpHear:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"GH021" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    if(alertView.tag == 20)
    {
        if (matchItPlayer) {
            [matchItPlayer stop];
            [matchItPlayer release];
            matchItPlayer = nil;
        }
        [self dismissModalViewControllerAnimated:NO];
//            [self saveProgressForReport];
    }
 
}


-(void) updataGameElements{
    if (moveToNextSetTimer) {
        [moveToNextSetTimer invalidate];
        moveToNextSetTimer = Nil;
    }
    
    NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
    int WordCounterStored = [[[NSUserDefaults standardUserDefaults] objectForKey:wordCount] intValue];
    if (WordCounterStored == 0) {
        WordCounterStored = 1;
    }
    
    reportCounter = WordCounterStored;
    
    if (![tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",WordCounterStored+3]] || [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",WordCounterStored+3]] == NULL) {
        if (fistTimeLoad) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            [alert setTitle:@"Wait!"]; 
            [alert setMessage:@"Please select other location from map and match it"];
            [alert setDelegate:self];
            [alert addButtonWithTitle:@"OK"];
            [alert setTag:20];
            [alert show];
            [alert release]; 
            return;
        }
        else {
            
            if (resetCounter) {
                resetCounter = FALSE;
                
                NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],5,cId,activeLanguage];
                int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
                [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount+1 forKey:levelCount];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self saveProgressForReport];
                
                [self insitiantiateForReportCouting];
                WordCounterStored = 1;
                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:wordCount];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
                if (WordCounterStored - 4 == [tempContentDic count]/7) {
                    resetCounter = TRUE;
                WordCounterStored = WordCounterStored - 7;

                NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
                [[NSUserDefaults standardUserDefaults] setInteger:WordCounterStored forKey:wordCount];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
            }else
            if (WordCounterStored - 3 == [tempContentDic count]/7) {
                                    resetCounter = TRUE;
                WordCounterStored = WordCounterStored - 6;
//                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:wordCount];
//                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
                [[NSUserDefaults standardUserDefaults] setInteger:WordCounterStored forKey:wordCount];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else
                if (WordCounterStored - 2 == [tempContentDic count]/7) {
                                        resetCounter = TRUE;
                    WordCounterStored = WordCounterStored - 5;
                    //                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:wordCount];
                    //                [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
                    [[NSUserDefaults standardUserDefaults] setInteger:WordCounterStored forKey:wordCount];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else
                    if (WordCounterStored - 1 == [tempContentDic count]/7) {
                                            resetCounter = TRUE;
                        WordCounterStored = WordCounterStored - 4;
                        //                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:wordCount];
                        //                [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
                        [[NSUserDefaults standardUserDefaults] setInteger:WordCounterStored forKey:wordCount];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
            
                    else
                        if (WordCounterStored + 1 == [tempContentDic count]/7) {
                                                resetCounter = TRUE;
                            WordCounterStored = WordCounterStored - 3;
                            //                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:wordCount];
                            //                [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
                            [[NSUserDefaults standardUserDefaults] setInteger:WordCounterStored forKey:wordCount];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                        }
                        else
                            if (WordCounterStored + 2 == [tempContentDic count]/7) {
                                                    resetCounter = TRUE;
                                WordCounterStored = WordCounterStored - 2;
                                //                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:wordCount];
                                //                [[NSUserDefaults standardUserDefaults] synchronize];
                                
                                NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
                                [[NSUserDefaults standardUserDefaults] setInteger:WordCounterStored forKey:wordCount];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                            }
                            else
                                if (WordCounterStored + 3 == [tempContentDic count]/7) {
                                                        resetCounter = TRUE;
                                    WordCounterStored = WordCounterStored - 1;
                                    //                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:wordCount];
                                    //                [[NSUserDefaults standardUserDefaults] synchronize];
                                    
                                    NSString *wordCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCounttest5",[Common getPinNumber],5,cId,activeLanguage];
                                    [[NSUserDefaults standardUserDefaults] setInteger:WordCounterStored forKey:wordCount];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                }
            
            
            else{
                                    resetCounter = FALSE;
            
            NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],5,cId,activeLanguage];
            int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
            [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount+1 forKey:levelCount];
            [[NSUserDefaults standardUserDefaults] synchronize];
                        [self saveProgressForReport];

                [self insitiantiateForReportCouting];
                WordCounterStored = 1;
                [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:wordCount];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
       
        }

    }

    [btnWord1 setTitle:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",WordCounterStored]]]forState:UIControlStateNormal];
        if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"isSentence%d",WordCounterStored]] intValue] == 1) {
            UIFont *font = [UIFont boldSystemFontOfSize: 28];
            btnWord1.titleLabel.font = font;
            if (activeLanguage == 1) {
                btnWord1.titleLabel.textAlignment = UITextAlignmentRight;
            }
        }
    [btnWord2 setTitle:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",WordCounterStored+1]]]forState:UIControlStateNormal];
        if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"isSentence%d",WordCounterStored+1]] intValue] == 1) {
            UIFont *font = [UIFont boldSystemFontOfSize: 28];
            btnWord2.titleLabel.font = font;
            if (activeLanguage == 1) {
                btnWord2.titleLabel.textAlignment = UITextAlignmentRight;
            }
        }
    [btnWord3 setTitle:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",WordCounterStored+2]]]forState:UIControlStateNormal];
        if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"isSentence%d",WordCounterStored+2]] intValue] == 1) {
            UIFont *font = [UIFont boldSystemFontOfSize: 28];
            btnWord3.titleLabel.font = font;
            if (activeLanguage == 1) {
                btnWord3.titleLabel.textAlignment = UITextAlignmentRight;
            }
        }
    [BtnWord4 setTitle:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",WordCounterStored+3]]]forState:UIControlStateNormal];
        if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"isSentence%d",WordCounterStored+3]] intValue] == 1) {
            UIFont *font = [UIFont boldSystemFontOfSize: 28];
            BtnWord4.titleLabel.font = font;
            if (activeLanguage == 1) {
                BtnWord4.titleLabel.textAlignment = UITextAlignmentRight;
            }

        }
    
    BOOL randomDifferentiator = FALSE;
            
        NSMutableArray* ArrRandomizer = [[NSMutableArray alloc] initWithCapacity:4];
            int ran;
            for (int i = 0; i < 4; i ++) {       
                do {            
                    ran = arc4random() %(4) + 1;
                    if ([ArrRandomizer count] == 0) {
                        randomDifferentiator = TRUE;                
                    }
                    for (int k = 0; k < [ArrRandomizer count]; k++) {
                        randomDifferentiator = TRUE;
                        if (ran == [[ArrRandomizer objectAtIndex:k] intValue]) {
                            randomDifferentiator = FALSE;
                            break;
                        }
                    }
                } while (!randomDifferentiator); 
                [ArrRandomizer insertObject:[NSString stringWithFormat:@"%d",ran] atIndex:i];                                    
                randomDifferentiator = FALSE;
            }
//            NSLog(@"Array is %@",ArrRandomizer);   

    NSString *str = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",[[ArrRandomizer objectAtIndex:0] intValue]+WordCounterStored-1]]];
    UIImage *img = [DocumentDirectory imageNamed:str];
    
    [BtnImage1 setImage:img forState:UIControlStateNormal];
    [BtnImage2 setImage:[DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",[[ArrRandomizer objectAtIndex:1] intValue]+WordCounterStored-1]]]] forState:UIControlStateNormal];
    [BtnImage3 setImage:[DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",[[ArrRandomizer objectAtIndex:2] intValue]+WordCounterStored-1]]]] forState:UIControlStateNormal];    
    [BtnImage4 setImage:[DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",[[ArrRandomizer objectAtIndex:3] intValue]+WordCounterStored-1]]]] forState:UIControlStateNormal];
    
    [[NSUserDefaults standardUserDefaults] setObject:ArrRandomizer forKey:@"positionArray"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([self.view viewWithTag:115]) {
        [[self.view viewWithTag:115] removeFromSuperview];
        [[self.view viewWithTag:115] release];
    }
        
    NSMutableArray *wordsSoundArray = [[NSMutableArray alloc] initWithCapacity:4];
    NSMutableArray *imagesSoundArray = [[NSMutableArray alloc] initWithCapacity:4];
      
    for (int q = 1; q < 5; q++)
    {
        [wordsSoundArray addObject:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",WordCounterStored+q-1]]]];
    }
    
    for (int q = 0; q < 4; q++)
    {
        [imagesSoundArray addObject:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",WordCounterStored-1+[[ArrRandomizer objectAtIndex:q] intValue]]]]];
    }
    
//    NSLog(@"Images sound array is %@",imagesSoundArray);
    DrawingUIView *aCustomView = [[DrawingUIView alloc] initWithFrame:CGRectMake(59,151,901,600) andWordsSoundArray:wordsSoundArray andImagesSoundArray:imagesSoundArray];
    [aCustomView setTag:115];
    [self.view addSubview:aCustomView];

        
    [[NSUserDefaults standardUserDefaults] setObject:NULL forKey:@"userResults"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [checkImg1 setImage:Nil];
    [checkImg2 setImage:Nil];
    [checkImg3 setImage:Nil];
    [checkImg4 setImage:Nil];   
}



-(void) correctItemSaveForReportLocally:(int) draggedItemTag{
    
    if (draggedItemTag == 1 ) {
        reportCorrectCount01 = reportCorrectCount01 + 1;
        reportTotatCount01 = reportTotatCount01 + 1;
    }
    
    if (draggedItemTag == 2 ) {
        reportCorrectCount02 = reportCorrectCount02 + 1;
        reportTotatCount02 = reportTotatCount02 + 1;
    }
    
    if (draggedItemTag == 3 ) {
        reportCorrectCount03 = reportCorrectCount03 + 1;
        reportTotatCount03 = reportTotatCount03 + 1;
    }
    
    if (draggedItemTag == 4 ) {
        reportCorrectCount04 = reportCorrectCount04 + 1;
        reportTotatCount04 = reportTotatCount04 + 1;
    }
}

//-(void) wrongItemSaveForReportLocally:(int) draggedItemTag{
//    
//    if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",reportCounter]] intValue] == draggedItemTag ) {
//        reportTotatCount01 = reportTotatCount01 + 1;
//    }
//    
//    if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",reportCounter]] intValue] == draggedItemTag ) {
//        reportTotatCount02 = reportTotatCount02 + 1;
//    }
//    
//    if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",reportCounter]] intValue] == draggedItemTag ) {
//        reportTotatCount03 = reportTotatCount03 + 1;
//    }
//    
//    if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",reportCounter]] intValue] == draggedItemTag ) {
//        reportTotatCount04 = reportTotatCount04 + 1;
//    }
//}



-(void) saveProgressForReport{
    // write in db!
    
    if (!retval || retval == NULL || [retval count] < 1) {
        return;
    }
    
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database saveReportProgress:retval];
    
    if (retval) {
        [retval removeAllObjects];
        [retval release];
        retval = nil;
    }
}
-(void) updateReportCurrentWord{
    reportWordDesc01 = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",reportCounter]];
    reportWordId01 = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",reportCounter]] intValue];
    
    NSLog(@"Report Word Desc is %@",reportWordDesc01);
    NSLog(@"Report Word Desc is %d",reportWordId01);
    

    if (reportWordDesc01 == NULL) {
        return;
    }
    
    if (reportTotatCount01 == 0) {
        reportTotatCount01 = reportTotatCount01 + 1;
    }
    
    if (reportTotatCount02 == 0) {
        reportTotatCount02 = reportTotatCount02 + 1;
    }
    
    if (reportTotatCount03 == 0) {
        reportTotatCount03 = reportTotatCount03 + 1;
    }
    
    if (reportTotatCount04 == 0) {
        reportTotatCount04 = reportTotatCount04 + 1;
    }
    
    
    int gameId = 6;
    MrPReport *info;
    
    if (reportWordDesc01) {
        info = [[MrPReport alloc]
                           initWithWordId:reportWordId01 andWordDesc:reportWordDesc01 andCorrectCount:reportCorrectCount01 andTotalCount:reportTotatCount01 andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
        
        [retval addObject:info];
        
        reportTotatCount01 = 0;
        reportCorrectCount01 = 0;
        reportWordDesc01 = @"";
        reportWordId01 = 0;
        
    }
    
    if (reportWordDesc02) {
        
        reportWordDesc02 = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",reportCounter + 1]];
        reportWordId02 = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",reportCounter + 1]] intValue];
        NSLog(@"Report Word Desc is %@",reportWordDesc02);
        NSLog(@"Report Word Desc is %d",reportWordId02);
        
        if (reportWordDesc02 == NULL) {
            return;
        }
        info = [[MrPReport alloc]
                initWithWordId:reportWordId02 andWordDesc:reportWordDesc02 andCorrectCount:reportCorrectCount02 andTotalCount:reportTotatCount02 andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
        
        [retval addObject:info];
        
        reportTotatCount02 = 0;
        reportCorrectCount02 = 0;
        reportWordDesc02 = @"";
        reportWordId02 = 0;
    }
    

 
    
    if (reportWordDesc03) {
        reportWordDesc03 = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",reportCounter + 2]];
        reportWordId03 = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",reportCounter + 2]] intValue];
        NSLog(@"Report Word Desc is %@",reportWordDesc03);
        NSLog(@"Report Word Desc is %d",reportWordId03);
        
        if (reportWordDesc03 == NULL) {
            return;
        }

        info = [[MrPReport alloc]
                initWithWordId:reportWordId03 andWordDesc:reportWordDesc03 andCorrectCount:reportCorrectCount03 andTotalCount:reportTotatCount03 andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
        
        [retval addObject:info];
        
        reportTotatCount03 = 0;
        reportCorrectCount03 = 0;
        reportWordDesc03 = @"";
        reportWordId03 = 0;
    }
    
    
    

    
    if (reportWordDesc04) {
        reportWordDesc04 = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",reportCounter + 3]];
        reportWordId04 = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",reportCounter + 3]] intValue];
        
        NSLog(@"Report Word Desc is %@",reportWordDesc04);
        NSLog(@"Report Word Desc is %d",reportWordId04);
        
        if (reportWordDesc04 == NULL) {
            return;
        }

        info = [[MrPReport alloc]
                initWithWordId:reportWordId04 andWordDesc:reportWordDesc04 andCorrectCount:reportCorrectCount04 andTotalCount:reportTotatCount04 andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
        
        [retval addObject:info];
        
        reportTotatCount04 = 0;
        reportCorrectCount04 = 0;
        reportWordDesc04 = @"";
        reportWordId04 = 0;
    }
    
    
  
}

-(void) insitiantiateForReportCouting{
    retval = [[NSMutableArray alloc] init];
    
    reportCounter = 0;
    
    reportCorrectCount01 = 0;
    reportTotatCount01 = 0;
    reportWordDesc01 = @"";
    reportWordId01 = 0;
    
    reportCorrectCount02 = 0;
    reportTotatCount02 = 0;
    reportWordDesc02 = @"";
    reportWordId02 = 0;
    
    reportCorrectCount03 = 0;
    reportTotatCount03 = 0;
    reportWordDesc03 = @"";
    reportWordId03 = 0;
    
    reportCorrectCount04 = 0;
    reportTotatCount04 = 0;
    reportWordDesc04 = @"";
    reportWordId04 = 0;
    
    
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"5-MatchIT"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    matchItPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    matchItPlayer.volume = 0.15;
    matchItPlayer.numberOfLoops = 20;
    matchItPlayer.delegate = self;
    
    if ([matchItPlayer prepareToPlay]) {
        [matchItPlayer play];            
    }    
}




- (void)dealloc {
 
    [btnWord1 release];
    [btnWord2 release];
    [btnWord3 release];
    [BtnWord4 release];
    [BtnImage1 release];
    [BtnImage2 release];
    [BtnImage3 release];
    [BtnImage4 release];
    [checkImg1 release];
    [checkImg2 release];
    [checkImg4 release];
    [checkImg3 release];
    
    [btnCheck release];
    [btnReset release];
    [btnNext release];
    [super dealloc];
}
- (BOOL)bannerViewActionShouldBegin:
(ADBannerView *)banner
               willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
        [_bannerView removeFromSuperview];    //        [_bannerView removeFromSuperview];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Did Fail to receive ad");
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"received ad");
    [self.view addSubview:_bannerView];
    //    self.view = _bannerView;
}

@end
