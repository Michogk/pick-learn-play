//
//  MyHomeViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/21/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "WordsTableViewController.h"
//#import "FoldersTableViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "SelectBookQuestions.h"
#import "MyHomeCell.h"
#import "WordsCell.h"
@interface MyHomeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,AVAudioPlayerDelegate,NSXMLParserDelegate>

{

//    FoldersTableViewController *FoldersController;    
//    WordsTableViewController *WordsController;
    
    int selectedIndexRow;

    SelectBookQuestions *bookQuestionsView;
    MyHomeCell *_folderCell;
    WordsCell *_wordsCell;
    int _nextClassificationId;
    NSMutableDictionary *foldersContentDic;
    NSMutableDictionary *wordsContentDic;
    int _missingClassificationCounter;
    
    int selectedWordCell;
    BOOL removingData;
    AVAudioPlayer *player;
    
    NSString *ActiveGrade;
    BOOL showMovie;
    
    UIImageView* characterWalkingAnimationImgView;
    int characterAnimationStep;
    int animationImageNumber;
    NSString *animationImageDirection;
    NSTimer *characterWalkingTimer;
    NSString* animationImageGender;
    int Position;
    
    BOOL errorParsing;
    NSString *currentElement;
    NSMutableString *ElementValue;

    IBOutlet UIScrollView *booksScrollView;
    NSMutableDictionary *bookFoldersContentDic;
    NSMutableDictionary *booksContentDic;
    NSMutableDictionary *bookWordsContentDic;
    BOOL booksSelected;
    int numberOfBooks;
    int numberOfFoldersInBook;
    int loadingClassification;
    
    BOOL currentTappedBookFolder;

    NSMutableArray *retainArrTagsOfBooks;
    // For FREE
    int foldersCountForFreeVersion;
    IBOutlet UIImageView *imgView1Feather;
    NSArray *foldersCellBackgroundImageArray;
    NSArray *wordssCellBackgroundImageArray;
}

- (IBAction)refrestDataFromBackend:(id)sender;
- (IBAction)actionLoadBook:(id)sender;
- (IBAction)actionLoadMyHome:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (retain, nonatomic) IBOutlet UIButton *btnAddWord;
@property (retain, nonatomic) IBOutlet UIButton *btnHearWordSound;
@property (retain, nonatomic) IBOutlet UIButton *BtnDeleteWord;
@property (retain, nonatomic) IBOutlet UIButton *btnRefreshDataFromServer;
- (IBAction)exitMyHome:(id)sender;

@property (retain, nonatomic) IBOutlet UIButton *BtnAddFolder;
@property (strong, nonatomic) IBOutlet UITableView *FoldersTable;
@property (strong, nonatomic) IBOutlet UITableView *WordsTable;
@property (retain, nonatomic) IBOutlet UIButton *BtnDeleteFolder;
@property (retain, nonatomic) NSIndexPath* selectedIndex;

- (IBAction)playSoundOfSelectedWord:(id)sender;
- (IBAction)DeleteWord:(id)sender;
- (IBAction)helpURL:(id)sender;

//- (IBAction)AddFolder:(id)sender;
+(int) assignClassificationId;
+ (MyHomeViewController*)homeViewController;
- (void)Reload;
- (IBAction)DeleteFolder:(id)sender;
- (IBAction)AddWord:(id)sender;
- (IBAction)AddFolder:(id)sender;
- (IBAction)HelpSound:(id)sender;

@end

 


