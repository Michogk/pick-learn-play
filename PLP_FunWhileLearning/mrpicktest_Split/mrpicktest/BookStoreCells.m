//
//  BookStoreCells.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/15/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "BookStoreCells.h"
#import "Downloader.h"
#import "CAURLDownload.h"
#import "mrpAppDelegate.h"
//#import "InAppRageIAPHelper.h"
#import "BookStoreDetailTextViewController.h"

@implementation BookStoreCells
@synthesize downloadedPackArr;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        activePackage = [[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"];
        int indexPath = [[NSUserDefaults standardUserDefaults] integerForKey:@"BookStoreCellNumber"];
        cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 450)];
        
        UIImageView *backgroundimgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 230, 500)];
//        backgroundimgView.image = [UIImage imageNamed:@"game1.png"];
        [cellView addSubview:backgroundimgView];
        if (backgroundimgView) {
            [backgroundimgView release];            
            backgroundimgView = Nil;
        }


        [self addSubview:cellView];
        [cellView setBackgroundColor:[UIColor clearColor]];
        cellView.transform = CGAffineTransformMakeRotation(M_PI * 0.5); 
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 100, 145, 160)];
        [cellView addSubview:imgView];
      
        UILabel *descritption = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, 145, 110)];
        descritption.textAlignment = UITextAlignmentCenter;
        [descritption setBackgroundColor:[UIColor clearColor]];
        UIFont *font = [UIFont systemFontOfSize: 18];
        [descritption setFont:font];

        descritption.lineBreakMode = UILineBreakModeWordWrap;
        descritption.numberOfLines = 2;
        [cellView addSubview:descritption]; 


        UILabel* lblGrade = [[UILabel alloc] initWithFrame:CGRectMake(40+113, 165, 245, 230)];

        [lblGrade setBackgroundColor:[UIColor clearColor]];
        font = [UIFont systemFontOfSize: 14];
        [lblGrade setFont:font];
        [lblGrade setNumberOfLines:0];
//        [lblGrade sizeToFit];
        if (activeLanguage == 1){
            lblGrade.textAlignment = UITextAlignmentRight;
        }
        else {
            lblGrade.textAlignment = UITextAlignmentLeft;
        }
        
        lblGrade.lineBreakMode = UILineBreakModeWordWrap;
//        lblGrade.numberOfLines = 9;

//        [cellView addSubview:lblGrade];
        
        UILabel *packageLevel = [[UILabel alloc] initWithFrame:CGRectMake(140, 155+100, 120, 90)];
        packageLevel.textAlignment = UITextAlignmentCenter;
        [packageLevel setBackgroundColor:[UIColor clearColor]];
        [packageLevel setFont:[UIFont fontWithName:@"Sansation-Bold" size:25]];
//        packageLevel.lineBreakMode = UILineBreakModeWordWrap;
        packageLevel.numberOfLines = 2;
//        [cellView addSubview:packageLevel]; 
        
        

        UIButton *btnPurchase = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnPurchase addTarget:self 
                        action:@selector(purchaseMethod:)
         forControlEvents:UIControlEventTouchUpInside];
        [btnPurchase setBackgroundColor:[UIColor clearColor]];
        [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
     

        UIButton *btnDetail = [UIButton buttonWithType:UIButtonTypeInfoDark];
//                UIButton *btnDetail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btnDetail addTarget:self
                      action:@selector(descriptionDetails:)
              forControlEvents:UIControlEventTouchUpInside];
        [btnDetail setBackgroundColor:[UIColor clearColor]];
//        btnDetail.frame =   CGRectMake(80, 350, 50, 40);
        btnDetail.frame =     CGRectMake(59, 320, 50, 20);
        btnDetail.userInteractionEnabled = YES;

        
        UILabel *lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(80-34, 310, 70, 60)];
        lblPrice.textAlignment = UITextAlignmentCenter;
        [lblPrice setBackgroundColor:[UIColor clearColor]];
        font = [UIFont systemFontOfSize: 20];
        [lblPrice setFont:font];        
        
        NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"];
//        NSString* packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"AlreadyDownloadedPackage"];
        NSString* packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"gradesPurchased"];
        
        if (packagesDownloadedBefore.length > 2) {            
        self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];   
        for (int i = 0; i < [self.downloadedPackArr count]; i++) {
            
            BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
            if (photo && indexPath == 1) {
                if (activeLanguage == 1) {
                    [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                }
                else {
                    [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                }
                self.downloadedPackArr = nil;
            }
            else
            {
                if (activeLanguage == 1) {
                    [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                }
                else {
                    [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                }
            }
            
            if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:activeGrade] || [[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                if (activeLanguage == 1) {
                    [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                }
                else {
                    [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];    
                }
                self.downloadedPackArr = nil;
                    }
                }
            }
//        }
        
        btnPurchase.frame = CGRectMake(100-72, 280, 110, 40);
        btnPurchase.tag = indexPath;
        btnDetail.tag = indexPath;
        
        if ([activePackage isEqualToString:@"free"]){
            if (indexPath == 0) {
                imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"kg%d.png",indexPath + 1]]; 
                if (activeLanguage == 1) {
                    [lblGrade setText:@"اضغط على الشاطىء وتعرف على 37 كلمة و 23 جملة باللغتين العربية والانجليزية."];   
                    [descritption setText:[NSString stringWithFormat:@"مجاناً: ",indexPath + 1]];
                    [btnPurchase setTitle:@"تحميل مجاني" forState:UIControlStateNormal];
                }
                else {                    
                    [lblGrade setText:@"Click on the Beach and enjoy the 37 words in Arabic and English as well 23 sentences.!"];                
                    [descritption setText:[NSString stringWithFormat:@"FREE",indexPath + 1]];
                    [btnPurchase setTitle:@"Download FREE" forState:UIControlStateNormal];
                }

                [lblGrade sizeToFit];
                [packageLevel setText:@"Basic Words"];
//                [lblPrice setText:@"$1.50"];
//                [cellView addSubview:lblPrice];

                [cellView addSubview:btnPurchase];

                UIFont *font = [UIFont systemFontOfSize: 13];
                btnPurchase.titleLabel.font = font ;
            }

            
        }

        if ([activePackage isEqualToString:@"KG1"]){
            if (indexPath == 0) {
                imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"kg%d.png",indexPath + 1]];  
                if (activeLanguage == 1) {
                    [lblGrade setText:@"حروف الأبجدية مع ارشادات لكتابتها, اصوات حروف الأبجدية, حروف الأبجدية مع كلمات. المزرعة وحديقة الحيوانات. خطوات النجاح للمبتدئين في القراءة والكتابة. مجموع اللغتين العربية والانجليزية: 196 كلمة وتحتوي على صور."];   
                    [descritption setText:[NSString stringWithFormat:@"KG - 1\nMy City ",indexPath + 1]];
                    [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                }
                else {                    
                [lblGrade setText:@"Alphabet list with directions on writing, Alphabet sounds, Alphabet with words. Farm and Zoo animals. Road to Success for beginners in reading and writing. Total: 196 words with image support between English and Arabic "];
                [descritption setText:[NSString stringWithFormat:@"KG - 1\nMy City ",indexPath + 1]];
//                [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                }
               
                [lblGrade sizeToFit];
                [packageLevel setText:@"Basic Words"];

                [cellView addSubview:btnPurchase];
//                [lblPrice setText:@"$0.99"];
                [cellView addSubview:lblPrice];
                        [cellView addSubview:btnDetail];
            }
            if (indexPath == 1) {
                imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Add-photos"]];
                [imgView setBackgroundColor:[UIColor clearColor]];
                [descritption setText:[NSString stringWithFormat:@"Add your photos"]];
                [cellView addSubview:btnPurchase];
                BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                if (photo && indexPath == 1) {
                    if (activeLanguage == 1) {
                        [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                    }
                    else {
                        [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                    }
                    self.downloadedPackArr = nil;
                }
                else
                {
                    if (activeLanguage == 1) {
                        [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                    }
                    else {
                        [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                    }
                }

                
                [cellView addSubview:btnDetail];
            }
            
            if (indexPath == 2) {
                imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-all-in-one.png",activePackage]];
                [imgView setBackgroundColor:[UIColor clearColor]];
                [descritption setText:[NSString stringWithFormat:@"%@ City Map + Photos",activePackage]];
                BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
    
                
                
                if (photo) {
                    if (activeLanguage == 1) {
                        [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                    }
                    else {
                        [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                    }
                    self.downloadedPackArr = nil;
                }
                else
                {
                    if (activeLanguage == 1) {
                        [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                    }
                    else {
                        [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                    }
                }

                [cellView addSubview:btnPurchase];
                [cellView addSubview:btnDetail];
            }

        }
        
        else
            if ([activePackage isEqualToString:@"KG2"]){
                if (indexPath == 0) {
                    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"kg%d.png",indexPath + 2]];
                    if (activeLanguage == 1) {
                        [lblGrade setText:@"حروف الأبجدية مع ارشادات لكتابتها, اصوات حروف الأبجدية, حروف الأبجدية مع كلمات. المزرعة وحديقة الحيوانات. خطوات النجاح للمبتدئين في القراءة والكتابة. مجموع اللغتين العربية والانجليزية: 218 كلمة وتحتوي على صور."];
                        [descritption setText:[NSString stringWithFormat:@"KG - 2\nMy City ",indexPath + 1]];
                        [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                    }
                    else {                    
                        [lblGrade setText:@"Alphabet list with directions on writing, Alphabet sounds, Alphabet with words. Farm and Zoo animals. Road to Success for beginners in reading and writing. Total: 218 words with image support between English and Arabic "];
                        [descritption setText:[NSString stringWithFormat:@"KG - 2\nMy City ",indexPath + 1]];
//                        [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                    }
//                    [lblGrade setText:@""];
                    [lblGrade sizeToFit];

                    [packageLevel setText:@"Basic Words"];
                    [cellView addSubview:btnPurchase];
//                    [lblPrice setText:@"$0.99"];
                    [cellView addSubview:lblPrice];
                            [cellView addSubview:btnDetail];
                }
                if (indexPath == 1) {
                    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Add-photos"]];
                    [imgView setBackgroundColor:[UIColor clearColor]];
                    [descritption setText:[NSString stringWithFormat:@"Add your photos"]];
                    [cellView addSubview:btnPurchase];
                    BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                    if (photo && indexPath == 1) {
                        if (activeLanguage == 1) {
                            [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                        }
                        else {
                            [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                        }
                        self.downloadedPackArr = nil;
                    }
                    else
                    {
                        if (activeLanguage == 1) {
                            [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                        }
                        else {
                            [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                        }
                    }

                    [cellView addSubview:btnDetail];
                }
                
                if (indexPath == 2) {
                    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-all-in-one.png",activePackage]];
                    [imgView setBackgroundColor:[UIColor clearColor]];
                    [descritption setText:[NSString stringWithFormat:@"%@ City Map + Photos",activePackage]];

                    BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];

                    
                    
                    if (photo) {
                        if (activeLanguage == 1) {
                            [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                        }
                        else {
                            [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                        }
                        self.downloadedPackArr = nil;
                    }
                    else
                    {
                        if (activeLanguage == 1) {
                            [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                        }
                        else {
                            [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                        }
                    }

                    [cellView addSubview:btnPurchase];
                    [cellView addSubview:btnDetail];
                }

            }
            else
                if ([activePackage isEqualToString:@"G1"]){
                    if (indexPath == 0) {
                        imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"grade-1.png",indexPath + 1]];
                        if (activeLanguage == 1) {
                            [lblGrade setText:@"29 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: 1080  كلمة وتحتوي على صور.\nايضاً, حروف الأبجدية و خطوات النجاح لتقوية القراءة والكتابة. اكثر من 120  كلمة باللغتين العربية والانجليزية وتحتوي على صور."];
                            [descritption setText:[NSString stringWithFormat:@"Grade - 1\nMy City ",indexPath + 1]];
                            [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                        }
                        else {                    
                        [lblGrade setText:@"29 locations in the city map. Total: 1080 words with image support between English and Arabic.\n\nIn addition to the most important sections for reading and writing: Alphabet and Road to Success (more than 120 words) with image support between English and Arabic."];
                        [descritption setText:[NSString stringWithFormat:@"Grade - 1\nMy City ",indexPath+ 1]];
//                        [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                        }                        

                        [packageLevel setText:@"Basic Words"];
                        [lblGrade sizeToFit];

                        [cellView addSubview:btnPurchase];
//                        [lblPrice setText:@"$2.99"];
                        [cellView addSubview:lblPrice];
                                [cellView addSubview:btnDetail];
                    }
                    
                    if (indexPath == 2) {
                        imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"SKILLS-G1.png",indexPath + 1]]; 
                        if (activeLanguage == 1) {
                            [lblGrade setText:@"لعربي: 23 من القواعد وتحتوي على اكثر من 50 مبدأ مع 1830 كلمة وجملة\n\n.الانجليزي: 20  من القواعد وتحتوي تقريباً على 70  مبدأ مع 740  كلمة وجملة\n \n إبرع في القراءة الانجليزية (370  كلمة)."];   
                            [descritption setText:[NSString stringWithFormat:@"Grade - 1\nMy Skills",indexPath + 1]];
                            [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                        }
                        else {                    
                            [lblGrade setText:@"Arabic: 23 grammar rules include more than 50 principles with 1830 examples containing sentences.\n\nEnglish: 20 grammar rules include almost 70 principles with 740 examples containing sentences\n\nEnglish outshine in reading (more than 370 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                        [descritption setText:[NSString stringWithFormat:@"Grade - 1\nMy Skills ",indexPath+ 1]];
//                        [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                        }        

                        [packageLevel setText:@"Basic Skills"];
                        [lblGrade sizeToFit];
                        if (packagesDownloadedBefore.length > 2) {
                            self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];
                            for (int i = 0; i < [self.downloadedPackArr count]; i++) {
                                
                                NSLog(@"%@",downloadedPackArr);

                                if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                                    if (activeLanguage == 1) {
                                        [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                    }
                                    else {
                                        [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                    }
                                    self.downloadedPackArr = nil;
                                }
                                else
                                {
                                    if (activeLanguage == 1) {
                                        [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                    }
                                    else {
                                        [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                    }
                                }
                            }
                        }

                        [cellView addSubview:btnPurchase];
//                        [lblPrice setText:@"$2.99"];
                        [cellView addSubview:lblPrice];
                                [cellView addSubview:btnDetail];
                        
                    }
                    
                    if (indexPath == 1) {
                        imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Add-photos"]];
                        [imgView setBackgroundColor:[UIColor clearColor]];
                        [descritption setText:[NSString stringWithFormat:@"Add your photos"]];
                        [cellView addSubview:btnPurchase];
                        BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                        if (photo && indexPath == 1) {
                            if (activeLanguage == 1) {
                                [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                            }
                            else {
                                [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                            }
                            self.downloadedPackArr = nil;
                        }
                        else
                        {
                            if (activeLanguage == 1) {
                                [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                            }
                            else {
                                [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                            }
                        }

                        [cellView addSubview:btnDetail];
                    }
                    
                    if (indexPath == 3) {
                        imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-All-in-one.png",activePackage]];
                        [imgView setBackgroundColor:[UIColor clearColor]];
                        [descritption setText:[NSString stringWithFormat:@"%@ City Map + %@ Skills + Photos",activePackage,activePackage]];
                        BOOL skillsPurchased = false;
                        BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                        if (packagesDownloadedBefore.length > 2) {
                            self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];
                            for (int i = 0; i < [self.downloadedPackArr count]; i++) {
                                
                                if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                                    skillsPurchased = true;
                                }
                            }
                        }
                        
                        
                        if (photo && skillsPurchased == 1) {
                            if (activeLanguage == 1) {
                                [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                            }
                            else {
                                [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                            }
                            self.downloadedPackArr = nil;
                        }
                        else
                        {
                            if (activeLanguage == 1) {
                                [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                            }
                            else {
                                [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                            }
                        }

                        [cellView addSubview:btnPurchase];
                        [cellView addSubview:btnDetail];
                    }
                    
                }
        
                else
                    if ([activePackage isEqualToString:@"G2"]){
                        if (indexPath == 0) {
                            imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"grade-2.png",indexPath + 1]];
                            if (activeLanguage == 1) {
                                [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: ما يقارب1200  كلمة وتحتوي على صور.\n \n خطوات النجاح لتقوية القراءة والكتابة. اكثر من 190  كلمة باللغتين العربية والانجليزية وتحتوي على صور."];   
                                [descritption setText:[NSString stringWithFormat:@"Grade - 2\nMy City ",indexPath + 1]];
                                [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                            }
                            else {                    
                                [lblGrade setText:@"30 locations in the city map. Total: almost 1200 words and sentences with image support between English and Arabic.\n\nIn addition to Road to Success (more than 190 words) for reading and writing, with image support between English and Arabic "];
//                                [packageLevel setText:@"Basic Words"];
                            [descritption setText:[NSString stringWithFormat:@"Grade - 2\nMy City",indexPath+ 1]];
//                                [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                            }                                                                                          
                            [lblGrade sizeToFit];
                            [cellView addSubview:btnPurchase];
//                            [lblPrice setText:@"$2.99"];
                            [cellView addSubview:lblPrice];
                                    [cellView addSubview:btnDetail];
                        }
                        
                        if (indexPath == 2) {
                            imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"SKILLS-G2.png",indexPath + 1]];  
                            if (activeLanguage == 1) {
                                [lblGrade setText:@"لعربي: 22  من القواعد وتحتوي على اكثر من 70  مبدأ مع 1300  كلمة وجملة.\n \n.الانجليزي: 24  من القواعد.\n \n وتحتوي تقريباً على 75  مبدأ مع 765  كلمة وجملة..\n \n إبرع في القراءة الانجليزية (اكثر من 500 كلمة)."];   
                                [descritption setText:[NSString stringWithFormat:@"Grade - 2\nMy Skills ",indexPath + 1]];
                                [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                            }
                            else {                    
                            [lblGrade setText:@"Arabic: 22 grammar rules include more than 70 principles with 1300 examples containing sentences\n\nEnglish: 24 grammar rules include almost 75 principles with 765 examples containing sentences\n\nEnglish outshine in reading (more than 500 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                            [descritption setText:[NSString stringWithFormat:@"Grade - 2\nMy Skills",indexPath+ 1]];
//                            [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                            } 
                            
                            [packageLevel setText:@"Basic Skills"];
                            [lblGrade sizeToFit];
                            if (packagesDownloadedBefore.length > 2) {
                                self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];
                                for (int i = 0; i < [self.downloadedPackArr count]; i++) {
                                    
                                    
                                    
                                    if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                                        if (activeLanguage == 1) {
                                            [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                        }
                                        else {
                                            [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                        }
                                        self.downloadedPackArr = nil;
                                    }
                                    else
                                    {
                                        if (activeLanguage == 1) {
                                            [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                        }
                                        else {
                                            [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                        }
                                    }
                                }
                            }
                            [cellView addSubview:btnPurchase];
//                            [lblPrice setText:@"$2.99"];
                            [cellView addSubview:lblPrice];
                                    [cellView addSubview:btnDetail];
                        }
                        if (indexPath == 1) {
                            imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Add-photos"]];
                            [imgView setBackgroundColor:[UIColor clearColor]];
                            [descritption setText:[NSString stringWithFormat:@"Add your photos"]];
                            [cellView addSubview:btnPurchase];
                            BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                            if (photo && indexPath == 1) {
                                if (activeLanguage == 1) {
                                    [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                }
                                else {
                                    [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                }
                                self.downloadedPackArr = nil;
                            }
                            else
                            {
                                if (activeLanguage == 1) {
                                    [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                }
                                else {
                                    [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                }
                            }

                            [cellView addSubview:btnDetail];
                        }
                        
                        if (indexPath == 3) {
                            imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-All-in-one.png",activePackage]];
                            [imgView setBackgroundColor:[UIColor clearColor]];
                            [descritption setText:[NSString stringWithFormat:@"%@ City Map + %@ Skills + Photos",activePackage,activePackage]];
                            BOOL skillsPurchased = false;
                            BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                            if (packagesDownloadedBefore.length > 2) {
                                self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];
                                for (int i = 0; i < [self.downloadedPackArr count]; i++) {
                                    
                                    if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                                        skillsPurchased = true;
                                    }
                                }
                            }
                            
                            
                            if (photo && skillsPurchased == 1) {
                                if (activeLanguage == 1) {
                                    [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                }
                                else {
                                    [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                }
                                self.downloadedPackArr = nil;
                            }
                            else
                            {
                                if (activeLanguage == 1) {
                                    [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                }
                                else {
                                    [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                }
                            }

                            [cellView addSubview:btnPurchase];
                            [cellView addSubview:btnDetail];
                        }
                    }
                    else
                        if ([activePackage isEqualToString:@"G3"]){
                            if (indexPath == 0) {
                                imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"grade-3.png",indexPath + 1]];                         
                                if (activeLanguage == 1) {
                                    [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: اكثر من 950  كلمة وتحتوي على صور. بعض الجُمَل تتضمن حقائق علمية."];   
                                [descritption setText:[NSString stringWithFormat:@"Grade - 3\nMy City",indexPath+ 1]];
                                    [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                }
                                else {                    
                                     [lblGrade setText:@" 30 locations in the city map. Total: more than 950 words and sentences with image support between English and Arabic.  Some sentences state scientific facts."];
                                [descritption setText:[NSString stringWithFormat:@"Grade - 3\nMy City",indexPath+ 1]];
//                                    [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                                } 
                               
//                                [packageLevel setText:@"Basic Words"];
                                [lblGrade sizeToFit];

                                [cellView addSubview:btnPurchase];
//                                [lblPrice setText:@"$2.99"];
                                [cellView addSubview:lblPrice];
                                        [cellView addSubview:btnDetail];
                            }
                            
                            if (indexPath == 2) {
                                imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"SKILLS-G3.png",indexPath + 1]]; 
                                if (activeLanguage == 1) {
                                    [lblGrade setText:@"العربي: 26  من القواعد وتحتوي على اكثر من 92  مبدأ مع 1920  كلمة وجملة. \n  \n الانجليزي: 32  من القواعد وتحتوي تقريباً على 110  مبدأ مع 1250  كلمة وجملة. \n  \n إبرع في القراءة الانجليزية (اكثر من 530  كلمة)."];   
                                [descritption setText:[NSString stringWithFormat:@"Grade - 3\nMy Skills",indexPath+ 1]];
                                    [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                }
                                else {                    
                                     [lblGrade setText:@"Arabic: 26 grammar rules include more than 92 principles with more than 1920 examples containing sentences\n\nEnglish: 32 grammar rules include almost 110 principles with more than 1250 examples containing sentences\n\nEnglish outshine in reading (more than 530 words): these words are to familiarize the student with the key words for beginners in reading and writing."];
                                [descritption setText:[NSString stringWithFormat:@"Grade - 3\nMy Skills",indexPath+ 1]];
//                                [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                                    
                                } 

                               
                                [packageLevel setText:@"Basic Skills"];
                                [lblGrade sizeToFit];
                                if (packagesDownloadedBefore.length > 2) {
                                    self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];
                                    for (int i = 0; i < [self.downloadedPackArr count]; i++) {
                                        
                                        
                                        
                                        if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                                            if (activeLanguage == 1) {
                                                [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                            }
                                            else {
                                                [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                            }
                                            self.downloadedPackArr = nil;
                                        }
                                        else
                                        {
                                            if (activeLanguage == 1) {
                                                [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                            }
                                            else {
                                                [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                            }
                                        }
                                    }
                                }

                                [cellView addSubview:btnPurchase];
//                                [lblPrice setText:@"$2.99"];
                                [cellView addSubview:lblPrice];
                                        [cellView addSubview:btnDetail];
                            }
                            if (indexPath == 1) {
                                imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Add-photos"]];
                                [imgView setBackgroundColor:[UIColor clearColor]];
                                [descritption setText:[NSString stringWithFormat:@"Add your photos"]];
                                BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                                if (photo && indexPath == 1) {
                                    if (activeLanguage == 1) {
                                        [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                    }
                                    else {
                                        [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                    }
                                    self.downloadedPackArr = nil;
                                }
                                else
                                {
                                    if (activeLanguage == 1) {
                                        [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                    }
                                    else {
                                        [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                    }
                                }

                                [cellView addSubview:btnPurchase];
                                [cellView addSubview:btnDetail];
                            }
                            
                            if (indexPath == 3) {
                                imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-All-in-one.png",activePackage]];
                                [imgView setBackgroundColor:[UIColor clearColor]];
                                [descritption setText:[NSString stringWithFormat:@"%@ City Map + %@ Skills + Photos",activePackage,activePackage]];
                                BOOL skillsPurchased = false;
                                BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                                if (packagesDownloadedBefore.length > 2) {
                                    self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];
                                    for (int i = 0; i < [self.downloadedPackArr count]; i++) {
                                        
                                        if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                                            skillsPurchased = true;
                                        }
                                    }
                                }
                                
                                
                                if (photo && skillsPurchased == 1) {
                                    if (activeLanguage == 1) {
                                        [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                    }
                                    else {
                                        [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                    }
                                    self.downloadedPackArr = nil;
                                }
                                else
                                {
                                    if (activeLanguage == 1) {
                                        [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                    }
                                    else {
                                        [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                    }
                                }
                                [cellView addSubview:btnPurchase];
                                [cellView addSubview:btnDetail];
                            }
                        }
                        else 
                            
                            if ([activePackage isEqualToString:@"G4"]){
                                if (indexPath == 0) {
                                    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"grade-4.png"]];
                                    if (activeLanguage == 1) {
                                        [lblGrade setText:@"30 موقِع من خريطة مدينتي. مجموع اللغتين العربية والانجليزية: اكثر من 475 كلمة وتحتوي على صور. بعض الجُمَل تتضمن حقائق علمية.."];   
                                    [descritption setText:[NSString stringWithFormat:@"Grade - 4\nMy City",indexPath+ 1]];
                                        [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                    }
                                    else {                    
                                         [lblGrade setText:@"30 locations in the city map. Total: more than 475 words and sentences with image support between English and Arabic. Some sentences state scientific facts."];
                                    [descritption setText:[NSString stringWithFormat:@"Grade - 4\nMy City",indexPath+ 1]];
//                                        [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                                    } 
                                   
                                    [packageLevel setText:@"Basic Words"];
                                    [lblGrade sizeToFit];

                                    [cellView addSubview:btnPurchase];
//                                    [lblPrice setText:@"$2.99"];
                                    [cellView addSubview:lblPrice];
                                            [cellView addSubview:btnDetail];
                                }
                                
                                if (indexPath == 2) {
                                    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"SKILLS-G4.png"]];  
                                    if (activeLanguage == 1) {
                                        [lblGrade setText:@"العربي: 32  من القواعد وتحتوي على اكثر من 122  مبدأ مع 2120  كلمة وجملة.\n  \n الانجليزي: 25  من القواعد \n وتحتوي على اكثر من 60  مبدأ مع 950  كلمة وجملة."];   
                                    [descritption setText:[NSString stringWithFormat:@"Grade - 4\nMy Skills",indexPath+ 1]];
                                        [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                    }
                                    else {                    
                                        [lblGrade setText:@"Arabic: 32 grammar rules include more than 122 principles with almost 2120 examples containing sentences\n\nEnglish: 25 grammar rules include more than 60 principles with more than 950 examples containing sentences"];
                                    [descritption setText:[NSString stringWithFormat:@"Grade - 4\nMy Skills",indexPath+ 1]];
//                                        [btnPurchase setTitle:@"Download" forState:UIControlStateNormal];
                                    } 
                                    
                                  
                                    [packageLevel setText:@"Basic Skills"];
                                    [lblGrade sizeToFit];
                                    
                                    
                                    if (packagesDownloadedBefore.length > 2) {
                                        self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];
                                        for (int i = 0; i < [self.downloadedPackArr count]; i++) {


                                            
                                            if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                                                if (activeLanguage == 1) {
                                                    [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                                }
                                                else {
                                                    [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];    
                                                }
                                                self.downloadedPackArr = nil;
                                            }
                                            else
                                            {
                                                if (activeLanguage == 1) {
                                                    [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                                }
                                                else {
                                                    [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                                }
                                            }
                                        }
                                    }
                                    
                                    

                                    [cellView addSubview:btnPurchase];
//                                    [lblPrice setText:@"$2.99"];
                                    [cellView addSubview:lblPrice];
                                            [cellView addSubview:btnDetail];
                                }
                                if (indexPath == 1) {
                                    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Add-photos"]];
                                    [imgView setBackgroundColor:[UIColor clearColor]];
                                    [descritption setText:[NSString stringWithFormat:@"Add your photos"]];
                                    [cellView addSubview:btnPurchase];
                                    BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                                    if (photo && indexPath == 1) {
                                        if (activeLanguage == 1) {
                                            [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                        }
                                        else {
                                            [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                        }
                                        self.downloadedPackArr = nil;
                                    }
                                    else
                                    {
                                        if (activeLanguage == 1) {
                                            [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                        }
                                        else {
                                            [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                        }
                                    }

                                    [cellView addSubview:btnDetail];
                                }
                                
                                if (indexPath == 3) {
                                    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-All-in-one.png",activePackage]];
                                    [imgView setBackgroundColor:[UIColor clearColor]];
                                    [descritption setText:[NSString stringWithFormat:@"%@ City Map + %@ Skills + Photos",activePackage,activePackage]];
                                    
                                    
                                    BOOL skillsPurchased = false;
                                      BOOL photo = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
                                    if (packagesDownloadedBefore.length > 2) {
                                        self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];
                                        for (int i = 0; i < [self.downloadedPackArr count]; i++) {
       
                                            if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {
                                                skillsPurchased = true;
                                            }
                                        }
                                    }
                                    
                                    
                                        if (photo && skillsPurchased == 1) {
                                            if (activeLanguage == 1) {
                                                [btnPurchase setTitle:@"تم شراؤها" forState:UIControlStateNormal];
                                            }
                                            else {
                                                [btnPurchase setTitle:@"PURCHASED" forState:UIControlStateNormal];
                                            }
                                            self.downloadedPackArr = nil;
                                        }
                                        else
                                        {
                                            if (activeLanguage == 1) {
                                                [btnPurchase setTitle:@"تحميل" forState:UIControlStateNormal];
                                            }
                                            else {
                                                [btnPurchase setTitle:@"BUY" forState:UIControlStateNormal];
                                            }
                                        }
                                    
                                    
                                    [cellView addSubview:btnPurchase];
                                    [cellView addSubview:btnDetail];
                                }
                            }
                            else {
                                NSLog(@"throw Exception. Doesnot match with any string");
                            }

        [lblPrice release];
        lblPrice = Nil;
        [imgView release];
        
        [packageLevel release];
        [lblGrade release];
        [descritption release];
        [cellView release];

        
    }
    return self;
}
-(void) descriptionDetails:(UIButton*) pressedBtn{
    int tag = pressedBtn.tag;
    [[NSUserDefaults standardUserDefaults] setInteger:tag forKey:@"BookStoreButtonForDescription"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    BookStoreDetailTextViewController* content = [[BookStoreDetailTextViewController alloc] init];
    UIPopoverController* popover = [[UIPopoverController alloc] initWithContentViewController:content];
    [popover presentPopoverFromRect:CGRectMake(78, 380-56, 10, 10) inView:cellView permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];


}

-(void) purchaseMethod:(UIButton*) sender{

    NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"];
//    NSString *gradeIsSelected;

     NSString* packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"AlreadyDownloadedPackage"];

    if ((sender.tag == 2) && (![activePackage isEqualToString:@"KG1"]) && (![activePackage isEqualToString:@"KG2"])) {
        if (packagesDownloadedBefore.length > 2){
            self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];       
            BOOL basicWordsPurchase = false;
            for (int i = 0; i < [self.downloadedPackArr count]; i++) {
                if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@",activeGrade]]) {                    
                    basicWordsPurchase = TRUE;
                }
            }
            if (basicWordsPurchase == FALSE) {
                UIAlertView *alert = [[UIAlertView alloc] init];
                int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];        
                if (activeLanguage == 2) {
                    [alert setTitle:@"CAUTION"]; 
                    [alert setMessage:@"You need to download Basic Words first before purchasing skills"];
                    [alert addButtonWithTitle:@"OK"];
                }                
                else {
                    [alert setTitle:@"تحذير !"]; 
                    [alert setMessage:@"يحب أن يتم تحميل الكلمات الاساسيه أولأ "];
                    [alert addButtonWithTitle:@"موافق"];
                    
                }

                [alert setDelegate:self];
//                [alert addButtonWithTitle:@"OK"];
                [alert setTag:6];
                [alert show];
                [alert release];
                return;
            }
                        
            activeGrade = [NSString stringWithFormat:@"%@SKILLS",activeGrade];
        }
    }
    
    if (sender.tag == 1) {
            activeGrade = @"KG_Grade4";
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"] forKey:@"activeSelectionInBookStore"];
           [[NSUserDefaults standardUserDefaults] setObject:@"PHOTOBUYING" forKey:@"activePackage"];
    }
    
    if (sender.tag == 3) {
       activeGrade = [NSString stringWithFormat:@"%@_3in1",activeGrade];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"COMBO"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if ((sender.tag == 2) && ([activePackage isEqualToString:@"KG1"] || [activePackage isEqualToString:@"KG2"])) {
        activeGrade = [NSString stringWithFormat:@"%@_2in1",activeGrade];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"COMBO"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    // check if grade has been downloaded before or not   
//    if (packagesDownloadedBefore.length > 2) {        
//    self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];       
//    for (int i = 0; i < [self.downloadedPackArr count]; i++) {
//        if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:activeGrade]) {
//            NSLog(@"PAckage has already been purchased");
//
//            }
//        }
//    }
    
    Downloader *downloadPackage = [[Downloader alloc] init];
    if (![downloadPackage connected]) {
        [downloadPackage release];
        downloadPackage = Nil;
        NSLog(@"Internet Connection Down");
                    NSLog(@"POSTING PURCHASE STARTED NOTIFICATION 1.");
        [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseStarted" object:self]];        
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"downloadingStatus"];        
        [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseCompletedCallBack" object:self]];                                
        return;
    }
    
    else {
        
        if ([activeGrade isEqualToString:@"FREE"] || [activeGrade isEqualToString:@"free"]) {
                [[NSUserDefaults standardUserDefaults] setObject:activeGrade forKey:@"gradeIsSelected"];
            NSLog(@"POSTING PURCHASE STARTED NOTIFICATION 2.");
            [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseStarted" object:self]];
            NSLog(@"Internet Connection Working");
            [downloadPackage fetchData];
            [downloadPackage release];
            downloadPackage = Nil;
                    
            return;            
        }
        if (downloadPackage) {
            [downloadPackage release];
            downloadPackage = Nil; 
        }
            NSLog(@"POSTING PURCHASE STARTED NOTIFICATION 3.");
        [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseStarted" object:self]];        
        [[NSUserDefaults standardUserDefaults] setObject:activeGrade forKey:@"gradeIsSelected"];
        
//        NSLog(@"Product identifier is %@",[NSString stringWithFormat:@"%@_PD1",activeGrade]);
//        [[InAppRageIAPHelper sharedHelper] requestProducts];
//        // beacause 3in1 is attached with active grade, so to remove that, we are setting this check.
//        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"COMBO"] == YES)  {
//            [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage" ] forKey:@"gradeIsSelected"];
//                  [[InAppRageIAPHelper sharedHelper] buyProductIdentifier:[NSString stringWithFormat:@"%@_PD1",activeGrade]];  
//        }
//        else{
//                [[InAppRageIAPHelper sharedHelper] buyProductIdentifier:[NSString stringWithFormat:@"%@_PD1",activeGrade]];
//        }

    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
        if (buttonIndex == 0 && [alertView tag] != 6 )
        {

//            Downloader *downloadPackage = [[Downloader alloc] init];
//            if (![downloadPackage connected]) {
//                [downloadPackage release];
//                downloadPackage = Nil;
//                NSLog(@"Internet Connection Down");
//                [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseStarted" object:self]];
//                
//                [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"downloadingStatus"];
//                
//                [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseCompletedCallBack" object:self]];                                
            }
            else {
                
//                [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseStarted" object:self]];
//                NSLog(@"Internet Connection Working");
//                [downloadPackage fetchData];
//                [downloadPackage release];
//                downloadPackage = Nil;
                
            
        }
        if (buttonIndex == 1)
        {

        }           
}


- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    for (UIView* child in [self subviews]) {
        
        CGRect frame1 = child.frame;
        //        CGRect frame1 = CGRectMake(0, 0, 600, 500);
        if ([child isKindOfClass:[UITableViewCell class]]) {
            float width = frame1.size.width;
            frame1.size.width = frame1.size.height;
            frame1.size.height = width;
        }
        child.frame = frame1;        
        //        } 
    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
