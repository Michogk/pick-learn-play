//
//  IAPHelper.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 12/16/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "IAPHelper.h"
#import "Downloader.h"

@implementation IAPHelper
@synthesize productIdentifiers = _productIdentifiers;
@synthesize products = _products;
@synthesize purchasedProducts = _purchasedProducts;
@synthesize request = _request;



- (void)requestProducts {
    
    self.request = [[[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers] autorelease];
    _request.delegate = self;
    [_request start];
    
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Received products results...");   
    self.products = response.products;
    self.request = nil;    
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsLoadedNotification object:_products];    
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    if ((self = [super init])) {
        
        // Store product identifiers
        _productIdentifiers = [productIdentifiers retain];
        
        // Check for previously purchased products
        NSMutableSet * purchasedProducts = [NSMutableSet set];
        for (NSString * productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [purchasedProducts addObject:productIdentifier];
                NSLog(@"Previously purchased: %@", productIdentifier);
            }
            NSLog(@"Not purchased: %@", productIdentifier);
        }
        self.purchasedProducts = purchasedProducts;
        
    }
    return self;
}
- (void)recordTransaction:(SKPaymentTransaction *)transaction {    
// Optional: Record the transaction on the server side...    
}

- (void)provideContent:(NSString *)productIdentifier {
    
    NSLog(@"Toggling flag for: %@", productIdentifier);
    NSString* PackageToBePurchased = [[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"];
    if ([PackageToBePurchased isEqualToString:@"PHOTOBUYING"]) {
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:productIdentifier];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"PHOTOBOUGHT" forKey:@"activePackage"];
        [_purchasedProducts addObject:productIdentifier];
        NSLog(@"POSTING purchaase completed notification for Photo");
        [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseCompletedCallBack" object:self]];        
        return;
    }
    Downloader *downloadPackage = [[Downloader alloc] init];    
//    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseStarted" object:self]];
    NSLog(@"Internet Connection Working");
    [downloadPackage fetchData];
    [downloadPackage release];
    downloadPackage = Nil;
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_purchasedProducts addObject:productIdentifier];    
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchasedNotification object:productIdentifier];
    
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {    
    NSLog(@"completeTransaction...");    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    [self recordTransaction: transaction];
    [self provideContent: transaction.payment.productIdentifier];    
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"restoreTransaction...");
    
    [self recordTransaction: transaction];
    [self provideContent: transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {


    
    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"downloadingStatus"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseCompletedCallBack" object:self]];  

    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:transaction];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{

    NSLog(@"payment Queued");
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void)buyProductIdentifier:(NSString *)productIdentifier {

    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    NSLog(@"Buying %@...", productIdentifier);
    SKPayment *payment = [SKPayment paymentWithProductIdentifier:productIdentifier];
//    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

- (void)dealloc
{
    [_productIdentifiers release];
    _productIdentifiers = nil;
    [_products release];
    _products = nil;
    [_purchasedProducts release];
    _purchasedProducts = nil;
    [_request release];
    _request = nil;
    [super dealloc];
}

@end
