//
//  ExpressAnswersPopUp.h
//  GMG App
//
//  Created by Mosib on 4/27/15.
//  Copyright (c) 2015 Advansoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ExpressAnswersPopUp;

@protocol ExpressAnswersPopUpDelegate <NSObject>

@optional
- (void) RightsViewSubmitted:(ExpressAnswersPopUp*)popUp;

@end

@interface ExpressAnswersPopUp : UIView <UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>{
}

@property (nonatomic,weak) IBOutlet id <ExpressAnswersPopUpDelegate> delegate;
@property (nonatomic,weak) IBOutlet UIView *viewPopUp;


@property (weak, nonatomic) IBOutlet UITextView *textViewAnswer;

@property (weak, nonatomic ) IBOutlet UIButton *buttonSubmit;

+ (ExpressAnswersPopUp*) mainView;

- (void) showOnView:(UIView*)view withDelegate:(id<ExpressAnswersPopUpDelegate>)delegate;
- (void) showOnView:(UIView*)view;
- (void) hide;

- (IBAction) actionSubmit:(id)sender;
- (IBAction)actionCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (nonatomic,retain) NSString* fileName;


@end
