
#import "GreyBorderView.h"
#import <QuartzCore/QuartzCore.h>

@implementation GreyBorderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self awakeFromNib];
    }
    return self;
}

- (void) awakeFromNib {
    [super awakeFromNib];
    self.layer.borderColor = [UIColor colorWithRed:186.0/255.0f green:186.0/255.0f blue:186.0/255.0f alpha:128.0/255.0f].CGColor;
    self.layer.borderWidth = 1.0f;
}


@end
