//
//  SpinItViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 3/30/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "SpinItViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "DocumentDirectory.h"
#import "SimpleAudioEngine.h"
#import "MrPDatabase.h"
#import "MrPReport.h"
#import "Common.h"

#define Spinit 6

@interface SpinItViewController ()

@end

@implementation SpinItViewController
@synthesize btnLeft = _btnLeft;
@synthesize btnRight = _btnRight;
@synthesize btnRepeatSound = _btnRepeatSound;
@synthesize imgViewCheering = _imgViewCheering;
@synthesize imgViewBackground = _imgViewBackground;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if ([ActiveGrade isEqualToString:@"FREE"]) {
    _bannerView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    _bannerView.requiredContentSizeIdentifiers = [NSSet setWithObject:ADBannerContentSizeIdentifierLandscape];
    _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;

    CGRect adFrame = _bannerView.frame;
    adFrame.origin.y = self.view.frame.size.width-_bannerView.frame.size.height;
    _bannerView.frame = adFrame;
    _bannerView.delegate = self;
    }
//    [self.view addSubview:_bannerView];
    [self playBackGroundMusic];
    
    [self insitiantiateForReportCouting];
//    [_imgViewBackground setImage:[UIImage imageNamed:@"spinIt01.png" ]];
    [self loadDatabase];
    [self initializeContent];
    
    _wordCounterlbl = [[UILabel alloc] initWithFrame:CGRectMake(600, 10, 300, 25)];
    NSString *currentNumber = [NSString stringWithFormat:@"Current Word : %d / %d",currentWord + 1, [tempContentDicCorrect count]/7];
    [_wordCounterlbl setText:currentNumber];
    //    [_wordCounterlbl setFont:[UIFont fontWithName:@"Arial" size:21]];
    UIFont* font = [UIFont systemFontOfSize: 21];
    [_wordCounterlbl setFont:font];
    _wordCounterlbl.textAlignment = UITextAlignmentCenter;
    [_wordCounterlbl setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_wordCounterlbl];
    sessionCounter = 0;
    correctWordCounter = 0;
    

    // Level Counter Label
    NSString *points1 = [NSString stringWithFormat:@"Game%dLevel%d",5,1];
    _lblLevel1 = [[UILabel alloc] initWithFrame:CGRectMake(600, 36, 300, 25)];
    NSString *currentLevel = [NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:points1]];
    [_lblLevel1 setText:currentLevel];
    //    [_lblLevel1 setFont:[UIFont fontWithName:@"Arial" size:21]];
    font = [UIFont systemFontOfSize: 21];
    [_lblLevel1 setFont:font];
    _lblLevel1.textAlignment = UITextAlignmentCenter;
    [_lblLevel1 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel1];
    
    NSString *points2 = [NSString stringWithFormat:@"Game%dLevel%d",5,2];
    _lblLevel2 = [[UILabel alloc] initWithFrame:CGRectMake(600, 62, 300, 25)];
    currentLevel = [NSString stringWithFormat:@"Level 02 : %d", [[NSUserDefaults standardUserDefaults] integerForKey:points2]];
    [_lblLevel2 setText:currentLevel];
    font = [UIFont systemFontOfSize: 21];
    [_lblLevel2 setFont:font];
    _lblLevel2.textAlignment = UITextAlignmentCenter;
    [_lblLevel2 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel2];
    
    [self startGame];
//    NSLog(@"Data loaded is %@",tempContentDicCorrect);
}
-(void) initializeContent{
    
    NSString* ActiveGrades  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCounter = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],Spinit,cId,activeLanguage,ActiveGrades];
    currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCounter];
    
    if ((currentWord == [tempContentDicCorrect count]/7) || (!currentWord))
    {
        currentWord = 0;
    }
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Spinit,cId,activeLanguage];
    
    level = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    
    if (!level)
    {
        level = 1;
    }
}

-(void) startGame{


    
    NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],Spinit,cId,activeLanguage,ActiveGrade];
    wordsCount = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCounterString];
    
    NSLog(@"WordsCount is %d",wordsCount);
    int totalNoOfCorrectItems = [tempContentDicCorrect count]/7;
    
    if ([tempContentDicCorrect count]/7 < 3) {
        UIAlertView *alerts = [[UIAlertView alloc] init];
        [alerts setTitle:@"In Sufficient Data"];
        [alerts setMessage:@"Please go to Main Menu and select another location"];
        [alerts setDelegate:self];
        [alerts addButtonWithTitle:@"OK"];
        [alerts setTag:12];
        [alerts show];
        [alerts release];
        return;
    }
    if (wordsCount == totalNoOfCorrectItems) {
        wordsCount = 0;
    }
    [self playSound];
    [self populateBalloons];
}

-(void) populateBalloons{
        [_imgViewBackground setImage:[UIImage imageNamed:@"spinIt01.png" ]];
    int randomBalloonSelector = rand() % 2;
    if (randomBalloonSelector == 0) {
        NSString *imageCorrect = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",wordsCount]] ];
        UIImage *correctImage = [DocumentDirectory imageNamed:imageCorrect];
        [_btnLeft setBackgroundImage:correctImage forState:UIControlStateNormal];
        _btnLeft.tag = 1;
        
        // secondBalloon
        NSString *imageWrong;
        int randomGenerator;
        do {
            randomGenerator = rand() % [tempContentDicCorrect count]/ 7;
            imageWrong = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",randomGenerator]] ];
            UIImage *WrongImage = [DocumentDirectory imageNamed:imageWrong];
            [_btnRight setBackgroundImage:WrongImage forState:UIControlStateNormal];
            _btnRight.tag = 0;
        } while (randomGenerator == wordsCount || [imageWrong isEqualToString:imageCorrect]);
            NSLog(@"Wrong is :%@\nCorrect is %@",imageWrong,imageCorrect);
    }
    else{
        NSString *imageCorrect = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",wordsCount]] ];
        UIImage *correctImage = [DocumentDirectory imageNamed:imageCorrect];
        [_btnRight setBackgroundImage:correctImage forState:UIControlStateNormal];
        _btnRight.tag = 1;
        
        // secondBalloon
        NSString *imageWrong;
        int randomGenerator;
        do {
            randomGenerator = rand() % [tempContentDicCorrect count]/ 7;
            imageWrong = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",randomGenerator]] ];
            UIImage *WrongImage = [DocumentDirectory imageNamed:imageWrong];
            [_btnLeft setBackgroundImage:WrongImage forState:UIControlStateNormal];
            _btnLeft.tag = 0;
        } while (randomGenerator == wordsCount || [imageWrong isEqualToString:imageCorrect]);
    
        NSLog(@"Wrong is :%@\nCorrect is %@",imageWrong,imageCorrect);
    
    }
//    NSLog(@"random number is %d",randomBalloonSelector);
    [_lblSynonym setHidden:NO];
    [_lblOpposite setHidden:NO];
    

    [_lblOpposite setText:[NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"opposite%d",wordsCount]]]];
    
    [_lblSynonym setText:[NSString stringWithFormat:@"Meaning :%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"synonym%d",wordsCount]]]];
    
    if([_lblSynonym.text isEqualToString:@"0"] || [_lblSynonym.text isEqualToString:@"Meaning :"]){
        [_lblSynonym setHidden:YES];
    }
    
    if([_lblOpposite.text isEqualToString:@"0"]){
        [_lblOpposite setHidden:YES];
    }
    else{
        [_lblOpposite setHidden:NO];
    }

    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:SYNONYM_ACTIVE]) {
        [_lblSynonym setHidden:YES];
    }
    
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    if (cId > 989) {
        _lblOpposite.hidden = YES;
        _lblSynonym.hidden = YES;
    }


    [self playSound];
}

-(void)playSound{
    
    NSString *soundId = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"voiceId%d",wordsCount]];
    if (!soundId || soundId == NULL) {
        soundId = @"EmptySound";
    }
    [self playSound:soundId];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

-(void) playSound:(NSString*) soundName{
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    if (!squishPath || squishPath == nil) {
        return;
    }
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
}

-(void) loadDatabase{
    int position = 0;
    // Loaading data  from database and saving in dictationaries
    MrPDatabase *database = [[MrPDatabase alloc] init];
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSArray *MrPContentArray;
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
        MrPContentArray = [database EnglishWordDatabaseArray:cId];
    }
    else {
        MrPContentArray = [database ArabicWordDatabaseArray:cId];
    }
//    NSLog(@"data is loaded successfully and array count is %d",[MrPContentArray count]);
    tempContentDicCorrect = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    for (MrPContentInfo *info in MrPContentArray) {
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]]
        ;
    if (activeLanguage == 2 || (cId > 989 && cId < 999))
        {
            [tempContentDicCorrect setObject:info.englishWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        else
        {
            [tempContentDicCorrect setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
        [tempContentDicCorrect setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
        [tempContentDicCorrect setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
        [tempContentDicCorrect setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];

        position++;
    }    
    position = 0;
}

-(void) playAnimation{

    // Cheering Anmiation
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:20];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1) // SIMPLE BOY
    {
        for(int count = 1; count <= 4; count++)
        {
            [images addObject:[UIImage imageNamed:@"Cheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"Cheering_7.png"]];
            [images addObject:[UIImage imageNamed:@"Cheering_8.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2) // SIMPLE GIRL
    {
        for(int count = 1; count <= 2; count++)
        {
            [images addObject:[UIImage imageNamed:@"GCheering_1.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_2.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_3.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_4.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_7.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_11.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_12.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3) // BASKET BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"BCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"BCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4) // BASKET GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"BCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_5.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5) // DOCTOR BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"DCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"DCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6) // DOCTOR GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"DCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_5.png"]];
        }
        
    }
    

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7) // Chef  BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"CCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"CCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8) // Chef GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"CCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9) // FIRE BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FireCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"FireCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10) // FIRE GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11) // FOOTBALL BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"FCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12) // Football GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13) // Arab BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"ACheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"ACheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14) // Arab GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"ACheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15) // POLICE BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"PCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_7.png"]];
        
            [images addObject:[UIImage imageNamed:@"PCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16) // POLICE GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"PCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_5.png"]];
        }
        
    }
    
    _imgViewCheering.animationImages = images;
    _imgViewCheering.animationDuration = 1.5;
    _imgViewCheering.animationRepeatCount = 2; //Repeats indefinitely
    
    [_imgViewCheering startAnimating];
    [images removeAllObjects];
    [images release];
    [_imgViewCheering setImage:NULL];
}

-(void) balloonAnimation:(NSString*) animationNumber{
//    Animation Number Convention :
//    b1 = left correct
//    b2 = left wrong
//    b3 = Right correct
//    b4 = Right Wrong
    
    // baloon Anmiation
    NSMutableArray *imagesBackground = [[NSMutableArray alloc] initWithCapacity:9];
    
    for(int count = 1; count <= 4; count++)
    {
        [imagesBackground addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@%d.png",animationNumber,count] ]];
    }
    
    _imgViewBackground.animationImages = imagesBackground;
    _imgViewBackground.animationDuration = .5;
    _imgViewBackground.animationRepeatCount = 6;
    
    [_imgViewBackground startAnimating];
    [imagesBackground removeAllObjects];
    [imagesBackground release];
//    [_imgViewBackground setImage:[UIImage imageNamed:@"spinIt01.png" ]];
//    [_imgViewBackground setImage:Nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_btnLeft release];
    [_btnRight release];
    [_btnRepeatSound release];
    [_imgViewCheering release];
    [_imgViewBackground release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setBtnLeft:nil];
    [self setBtnRight:nil];
    [self setBtnRepeatSound:nil];
    [self setImgViewCheering:nil];
    [self setImgViewBackground:nil];
    [super viewDidUnload];
}
- (IBAction)btnLeftPressed:(id)sender {
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    [_btnRight setEnabled:NO];
    [_btnLeft setEnabled:NO];
    if ([sender tag] == 1) {
        NSLog(@"Correct Answer");
        reportCorrectCount = reportCorrectCount + 1;
        reportTotatCount = reportTotatCount + 1;
        [self answerPoints];
        [self playAnimation];
        [self playCheeringSound];
            [self balloonAnimation:@"b1"];
    }
    else{        
        NSLog(@"Wrong Answer");
        reportTotatCount = reportTotatCount + 1;
        [self playEncouragingSound];
        [self balloonAnimation:@"b2"];
    }
    [self moveToNextSet];
}

- (IBAction)btnRightPressed:(id)sender {
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    [_btnRight setEnabled:NO];
    [_btnLeft setEnabled:NO];
    if ([sender tag] == 1) {
        NSLog(@"Correct Answer");
        reportCorrectCount = reportCorrectCount + 1;
        reportTotatCount = reportTotatCount + 1;
                [self answerPoints];
        [self playAnimation];
        [self playCheeringSound];
        [self balloonAnimation:@"b3"];
    }
    else{
        NSLog(@"Wrong Answer");
        reportTotatCount = reportTotatCount + 1;
        [self playEncouragingSound];
        [self balloonAnimation:@"b4"];
    }
    [self moveToNextSet];
}

-(void) answerPoints{
    
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Spinit,cId,activeLanguage];
    int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    if (!LevelLastCount) {
        LevelLastCount = 1;
    }
    
    NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",5,LevelLastCount];
    float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
    if (!PointsLastCount) {
        PointsLastCount = 0;
    }
    
    if (LevelLastCount < 3) {
        PointsLastCount = PointsLastCount + 1;
        [[NSUserDefaults standardUserDefaults] setInteger:PointsLastCount forKey:Points];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
//    if (wordsCount + 1 == [tempContentDicCorrect count]/7) {
//        LevelLastCount = LevelLastCount + 1;
//    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount forKey:levelCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    NSLog(@"Level is %d and points are %f",LevelLastCount,PointsLastCount);
    correctWordCounter = correctWordCounter + 1;
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 10) {
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
            correctWordCounter = 0;
            
        }
        if (buttonIndex == 1)
        {
            
            if (mySpinItPlayer) {
                [mySpinItPlayer stop];
                [mySpinItPlayer release];
                mySpinItPlayer = nil;
                
            }
            
            if (cheeringSoundTimer) {
                [cheeringSoundTimer invalidate];
                [cheeringSoundTimer release];
                cheeringSoundTimer = Nil;
            }
            
            if (player) {
                [player stop];
                [player release];
                player = Nil;
            }
            [self saveProgressForReport];
            [self dismissModalViewControllerAnimated:NO];
//            [self saveProgressForReport];
        }
    }
    
    if (alertView.tag == 12) {
        if (mySpinItPlayer) {
            [mySpinItPlayer stop];
            [mySpinItPlayer release];
            mySpinItPlayer = nil;
            
        }
        
        if (cheeringSoundTimer) {
            [cheeringSoundTimer invalidate];
            [cheeringSoundTimer release];
            cheeringSoundTimer = Nil;
        }
        
        if (player) {
            [player stop];
            [player release];
            player = Nil;
        }
        
        [self saveProgressForReport];
            [self dismissModalViewControllerAnimated:NO];
//            [self saveProgressForReport];
    }
}



-(void) sessionCompletionCheck{
        
    UIAlertView *sessionAlert;
    if (correctWordCounter > 7) {
        
        // Awarding Bonus
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Spinit,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        if (!LevelLastCount) {
            LevelLastCount = 1;
        }
        
        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",5,LevelLastCount];
        float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
        if (!PointsLastCount) {
            PointsLastCount = 0;
        }
        
        if (LevelLastCount < 3) {
            PointsLastCount = PointsLastCount + 2;
            [[NSUserDefaults standardUserDefaults] setInteger:PointsLastCount forKey:Points];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",5,1];
        [_lblLevel1 setText:[NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]]];
        
        NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",5,2];
        [_lblLevel2 setText:[NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]]];
        
        if (activeLanguage == 2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good! You have earned Bonus of +2" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
        }
        
        [sessionAlert setTag:10];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"RewardUnlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        
        if (activeLanguage ==2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your progress needs improvement. Try again." delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عفوا !" message:@"تقدمك غير كافي , يجب أن تحاول أكثر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب" otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:1];
    }    
    [sessionAlert show];
    [sessionAlert release];
}

-(void) moveToNextSet{
    

    [self updateReportCurrentWord];
    NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",5,1];
    [_lblLevel1 setText:[NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]]];
    
    NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",5,2];
    [_lblLevel2 setText:[NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]]];
    
    if (sessionCounter == 10) {
        sessionCounter = 0;
        
        if (wordsCount + 1== [tempContentDicCorrect count]/7) {
            wordsCount = -1; // because in next step it will be incremented by 1 and will be 1 again
            NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Spinit,cId,activeLanguage];
            int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
            [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount+1 forKey:levelCount];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self saveProgressForReport];
            [self insitiantiateForReportCouting];
            
        }
            NSString *activeGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        wordsCount = wordsCount + 1;
        
        NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],Spinit,cId,activeLanguage,ActiveGrade];
        
        [[NSUserDefaults standardUserDefaults] setInteger:wordsCount forKey:wordsCounterString];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"WordsCount is %d",wordsCount);
        
        
        [self sessionCompletionCheck];
        return;
    }
    sessionCounter = sessionCounter + 1;    
    [_btnRepeatSound setEnabled:NO];
    [_btnLeft setEnabled:NO];
    [_btnRight setEnabled:NO];
    
    

    
    if (wordsCount + 1== [tempContentDicCorrect count]/7) {
        wordsCount = -1; // because in next step it will be incremented by 1 and will be 1 again
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],Spinit,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount+1 forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
                    [self saveProgressForReport];
        [self insitiantiateForReportCouting];
        
    }
    wordsCount = wordsCount + 1;
    NSString *activeGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],Spinit,cId,activeLanguage,activeGrade];
    
    [[NSUserDefaults standardUserDefaults] setInteger:wordsCount forKey:wordsCounterString];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"WordsCount is %d",wordsCount);
    
    

    
//    levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",Spinit,cId,activeLanguage];
//    level = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];

//    NSString *currentLevel = [NSString stringWithFormat:@"Level 01 : %d",level];
//    [_lblLevel1 setText:currentLevel];
    
}

-(void) playEncouragingSound{
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:3.0
                                                               target:self
                                                             selector:@selector(endTimerAndLoadNextViewItems)
                                                             userInfo:nil
                                                              repeats:YES]retain];
        
    }
    
    int randomNumer = arc4random() % 3 ;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC25";
        NSString *second = @"EC30";
        NSString *third = @"EC31";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }else {
        NSString *first = @"AC04";
        NSString *second = @"AC05";
        NSString *third = @"AC06";
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }
}

-(void) playCheeringSound{
    
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:3.0
                                                               target:self
                                                             selector:@selector(endTimerAndLoadNextViewItems)
                                                             userInfo:nil
                                                              repeats:YES]retain];
        
        
        
    }
    
    int randomNumer = arc4random() % 3 ;
//    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);        
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
    }
}

-(void) endTimerAndLoadNextViewItems{
    
    NSString *currentNumber = [NSString stringWithFormat:@"Current Word : %d / %d",wordsCount + 1, [tempContentDicCorrect count]/7];
    NSLog(@"WordsCount is %@",currentNumber);
    [_wordCounterlbl setText:currentNumber];
    
    [_btnRepeatSound setEnabled:YES];
    [_btnLeft setEnabled:YES];
    [_btnRight setEnabled:YES];
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        cheeringSoundTimer = nil;
    }
//    if (viewCompletedTimer) {
//        [viewCompletedTimer invalidate];
//        viewCompletedTimer = nil;
//    }
    
    [self populateBalloons];
    
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"5-MatchIT"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    mySpinItPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    mySpinItPlayer.volume = 0.15;
    mySpinItPlayer.numberOfLoops = 20;
    mySpinItPlayer.delegate = self;
    
    if ([mySpinItPlayer prepareToPlay]) {
        [mySpinItPlayer play];
    }
}

-(void) saveProgressForReport{
    // write in db!
    
//    NSLog(@"retval is %@",retval);
    
    if (!retval || retval == NULL ) {
        return;
    }
    
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database saveReportProgress:retval];

    //    NSLog(@"temp is %@",tempContentDic);
    
    if (retval) {
        [retval removeAllObjects];
        [retval release];
        retval = nil;
    }
}
-(void) updateReportCurrentWord{
    reportWordDesc = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount]];
    reportWordId = [[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordId%d",wordsCount]] intValue];
    int gameId = 2;
    MrPReport *info = [[MrPReport alloc]
                       initWithWordId:reportWordId andWordDesc:reportWordDesc andCorrectCount:reportCorrectCount andTotalCount:reportTotatCount andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
    
    [retval addObject:info];
    
    reportTotatCount = 0;
    reportCorrectCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
    
}

-(void) insitiantiateForReportCouting{
    retval = [[NSMutableArray alloc] init];
    reportCorrectCount = 0;
    reportTotatCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
}

- (IBAction)repeatSound:(id)sender {
    [self playSound];
}
- (IBAction)gotoMainMenu:(id)sender {
    if (mySpinItPlayer) {
        [mySpinItPlayer stop];
        [mySpinItPlayer release];
        mySpinItPlayer = nil;

    }
    
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        [cheeringSoundTimer release];
        cheeringSoundTimer = Nil;
    }
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    if (_bannerView) {
        _bannerView.delegate = Nil;
        [_bannerView release];
        _bannerView = nil;
    }

    
    [self saveProgressForReport];
}

- (BOOL)bannerViewActionShouldBegin:
(ADBannerView *)banner
               willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
        [_bannerView removeFromSuperview];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Did Fail to receive ad");
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
        NSLog(@"received ad");
        [self.view addSubview:_bannerView];
//    self.view = _bannerView;
}
@end
