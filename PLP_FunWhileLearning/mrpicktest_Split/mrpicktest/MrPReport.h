//
//  MrPReport.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/31/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MrPReport : NSObject
{

}

- initWithWordId:(int)reportWordId andWordDesc:(NSString*)reportWordDesc andCorrectCount:(int)reportCorrectCount andTotalCount:(int)reportTotatCount andActiveLanguage:(int)activeLanguage andClassification:(int)classification andGameId:(int)gamenumber;


- initWithWordId:(int)reportWordId andWordDesc:(NSString*)reportWordDesc andCorrectCountFixIt:(int)reportCorrectCountFixIt andTotalCountFixIt:(int)reportTotatCountFixIt andCorrectCountPopIt:(int)reportCorrectCountPopIt andTotalCountPopIt:(int)reportTotatCountPopIt andCorrectCountSpinIt:(int)reportCorrectCountSpinIt andTotalCountSpinIt:(int)reportTotatCountSpinIt andCorrectCountPickIt:(int)reportCorrectCountPickIt andTotalCountPickIt:(int)reportTotatCountPickIt andCorrectCountMoveIt:(int)reportCorrectCountMoveIt andTotalCountMoveIt:(int)reportTotatCountMoveIt andCorrectCountMatchIt:(int)reportCorrectCountMatchIt andTotalCountMatchIt:(int)reportTotatCountMatchIt andCorrectCountTypeIt:(int)reportCorrectCountTypeIt andTotalCountTypeIt:(int)reportTotatCountTypeIt
andCorrectCountFootball:(int)reportCorrectCountFootball andTotalCountFootball:(int)reportTotatCountFootball
andCorrectCountHockey:(int)reportCorrectCountHockey andTotalCountHockey:(int)reportTotatCountHockey
andCorrectCountBasketball:(int)reportCorrectCountBasketball andTotalCountBasketball:(int)reportTotatCountBasketball
andActiveLanguage:(int)activeLanguage andClassification:(int)classification andGameId:(int)gamenumber andPinNumber:(int)pinNumber;

@property (nonatomic, assign) int wordId;
@property (nonatomic, assign) int noOfCorrect;
@property (nonatomic, assign) int totalAnswers;
@property (nonatomic, copy) NSString *wordDescription;
@property (nonatomic, assign) int activelanguage;
@property (nonatomic, assign) int classification;
@property (nonatomic, assign) int gameId;
@property (nonatomic, assign) int PINNUMBER;

@property (nonatomic, assign) int noOfCorrectFixIt;
@property (nonatomic, assign) int totalAnswersFixIt;

@property (nonatomic, assign) int noOfCorrectSpinIt;
@property (nonatomic, assign) int totalAnswersSpinIt;

@property (nonatomic, assign) int noOfCorrectPopIt;
@property (nonatomic, assign) int totalAnswersPopIt;

@property (nonatomic, assign) int noOfCorrectPickIt;
@property (nonatomic, assign) int totalAnswersPickIt;

@property (nonatomic, assign) int noOfCorrectMoveIt;
@property (nonatomic, assign) int totalAnswersMoveIt;

@property (nonatomic, assign) int noOfCorrectMatchIt;
@property (nonatomic, assign) int totalAnswersMatchIt;

@property (nonatomic, assign) int noOfCorrectTypeIt;
@property (nonatomic, assign) int totalAnswersTypeIt;

@property (nonatomic, assign) int noOfCorrectFootball;
@property (nonatomic, assign) int totalAnswersFootball;

@property (nonatomic, assign) int noOfCorrectHockey;
@property (nonatomic, assign) int totalAnswersHockey;

@property (nonatomic, assign) int noOfCorrectBasketBall;
@property (nonatomic, assign) int totalAnswersBasketBall;


@end
