//
//  Constants.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 24/07/2014.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CLASSIFICATIONS_ARRAY @"CLASSIFICATIONS_ARRAY"
#define SYNONYM_ACTIVE @"Synonym_Active"
#define PRINCIPLE_ID_ARRAY @"PRINCIPLE_ID"
#define GRAMMAR_ID_ARRAY @"GRAMMAR_ID"
#define ENGLISH 2
#define ARABIC 1


@interface Constants : NSObject

+(NSMutableString*) createQueryString:(NSArray*)pArray;
+(NSMutableString*) createGrammarIdString:(NSArray*)pArray;
+(NSMutableString*) createPrincipleIdString:(NSArray*)pArray;

+(NSMutableString*) createNotEqualToGrammarIdString:(NSArray*)pArray withGIDIncluded:(BOOL)pGIdIncluded;
@end
