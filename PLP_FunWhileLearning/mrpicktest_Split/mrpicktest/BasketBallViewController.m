//
//  BasketBallViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 8/7/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "BasketBallViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "DocumentDirectory.h"
#import "SimpleAudioEngine.h"
#import "MrPDatabase.h"
#import "MrPReport.h"
#import <QuartzCore/QuartzCore.h>
#import "Common.h"

#define BasketBall 11


@interface BasketBallViewController ()

@end

@implementation BasketBallViewController

@synthesize btnLeft = _btnLeft;
@synthesize btnRight = _btnRight;
@synthesize btnRepeatSound = _btnRepeatSound;
@synthesize imgViewCheering = _imgViewCheering;
@synthesize imgViewBackground = _imgViewBackground;
@synthesize imgBALL;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if ([ActiveGrade isEqualToString:@"FREE"]) {
        _bannerView = [[ADBannerView alloc] initWithFrame:CGRectZero];
        _bannerView.requiredContentSizeIdentifiers = [NSSet setWithObject:ADBannerContentSizeIdentifierLandscape];
        _bannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
        
        CGRect adFrame = _bannerView.frame;
        adFrame.origin.y = self.view.frame.size.width-_bannerView.frame.size.height;
        _bannerView.frame = adFrame;
        _bannerView.delegate = self;
    }
    //    [self.view addSubview:_bannerView];
    
    _lblRightGoal.numberOfLines = 0;
    _lblLeftGoal.numberOfLines = 0;
    
    _lblLeftGoal.lineBreakMode = UILineBreakModeWordWrap;
    _lblRightGoal.lineBreakMode = UILineBreakModeWordWrap;
    
    [self.view setUserInteractionEnabled:YES];
    [self playBackGroundMusic];
    [self insitiantiateForReportCouting];
    [_imgViewBackground setImage:[UIImage imageNamed:@"basketball-zero.png" ]];
    [self loadDatabase];
    [self initializeContent];
    
    _wordCounterlbl = [[UILabel alloc] initWithFrame:CGRectMake(600, 10, 300, 25)];
    NSString *currentNumber = [NSString stringWithFormat:@"Current Word : %d / %d",currentWord + 1, DictationaryCount];
    [_wordCounterlbl setText:currentNumber];
    //    [_wordCounterlbl setFont:[UIFont fontWithName:@"Arial" size:21]];
    UIFont* font = [UIFont systemFontOfSize: 21];
    [_wordCounterlbl setFont:font];
    _wordCounterlbl.textAlignment = UITextAlignmentCenter;
    [_wordCounterlbl setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_wordCounterlbl];
    sessionCounter = 0;
    correctWordCounter = 0;
    
    
    // Level Counter Label
    NSString *points1 = [NSString stringWithFormat:@"Game%dLevel%d",10,1];
    _lblLevel1 = [[UILabel alloc] initWithFrame:CGRectMake(600, 36, 300, 25)];
    NSString *currentLevel = [NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:points1]];
    [_lblLevel1 setText:currentLevel];
    //    [_lblLevel1 setFont:[UIFont fontWithName:@"Arial" size:21]];
    font = [UIFont systemFontOfSize: 21];
    [_lblLevel1 setFont:font];
    _lblLevel1.textAlignment = UITextAlignmentCenter;
    [_lblLevel1 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel1];
    
    NSString *points2 = [NSString stringWithFormat:@"Game%dLevel%d",10,2];
    _lblLevel2 = [[UILabel alloc] initWithFrame:CGRectMake(600, 62, 300, 25)];
    currentLevel = [NSString stringWithFormat:@"Level 02 : %d", [[NSUserDefaults standardUserDefaults] integerForKey:points2]];
    [_lblLevel2 setText:currentLevel];
    font = [UIFont systemFontOfSize: 21];
    [_lblLevel2 setFont:font];
    _lblLevel2.textAlignment = UITextAlignmentCenter;
    [_lblLevel2 setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_lblLevel2];
    if (cId == 99999) {
//        [_lblSynonym setHidden:NO];
//        [_lblOpposite setHidden:NO];
    }
    else{
//        [_lblSynonym setHidden:YES];
//        [_lblOpposite setHidden:YES];
    }
    
    [self startGame];
    //    NSLog(@"Data loaded is %@",tempContentDicCorrect);
}
-(void) initializeContent{
    
    NSString* ActiveGrades  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCounter = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],BasketBall,cId,activeLanguage,ActiveGrades];
    currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCounter];
    
    if ((currentWord == DictationaryCount) || (!currentWord))
    {
        currentWord = 0;
    }
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],BasketBall,cId,activeLanguage];
    
    level = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    
    if (!level)
    {
        level = 1;
    }
}

-(void) startGame{
    
    
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],BasketBall,cId,activeLanguage,ActiveGrade];
    wordsCount = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCounterString];
    
    NSLog(@"WordsCount is %d",wordsCount);
    int totalNoOfCorrectItems = DictationaryCount;
    
    if (DictationaryCount < 3) {
        UIAlertView *alerts = [[UIAlertView alloc] init];
        [alerts setTitle:@"In Sufficient Data"];
        [alerts setMessage:@"Please go to Main Menu and select another location"];
        [alerts setDelegate:self];
        [alerts addButtonWithTitle:@"OK"];
        [alerts setTag:12];
        [alerts show];
        [alerts release];
        return;
    }
    if (wordsCount == totalNoOfCorrectItems) {
        wordsCount = 0;
    }
    [self playSound];
    [self populateBalloons];
}

-(void) populateBalloons{
    
    [_imgViewBackground setImage:[UIImage imageNamed:@"basketball-zero.png" ]];
    int randomBalloonSelector = rand() % 2;
    if (randomBalloonSelector == 0) {
        
        NSString *imageCorrect = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",wordsCount]] ];
        NSString *correctText = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount]] ];
        [_lblLeftGoal setText:[NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount]] ]];
        
//        if (cId == 99999) {
            [_lblSynonym setHidden:NO];
            [_lblOpposite setHidden:NO];
            [_lblOpposite setText:[NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"opposite%d",wordsCount]]]];
            
            [_lblSynonym setText:[NSString stringWithFormat:@"Meaning :%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"synonym%d",wordsCount]]]];
            
        if([_lblSynonym.text isEqualToString:@"0"] || [_lblSynonym.text isEqualToString:@"Meaning :"]){
            [_lblSynonym setHidden:YES];
        }
        
            if([_lblOpposite.text isEqualToString:@"0"]){
                [_lblOpposite setHidden:YES];
            }
            else{
                [_lblOpposite setHidden:NO];
            }
//        }
        UIImage *correctImage = [DocumentDirectory imageNamed:imageCorrect];
        [_btnLeft setBackgroundImage:correctImage forState:UIControlStateNormal];
        _btnLeft.tag = 1;
        
        // secondBalloon
        imageCorrect = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",wordsCount]] ];
        NSString *imageWrong;
        NSString *wrongText;
        int randomGenerator;
        do {
            randomGenerator = rand() % DictationaryCount;
            imageWrong = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",randomGenerator]] ];
            UIImage *WrongImage = [DocumentDirectory imageNamed:imageWrong];
            wrongText = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",randomGenerator]]];
            [_lblRightGoal setText:[NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",randomGenerator]] ]];
            [_btnRight setBackgroundImage:WrongImage forState:UIControlStateNormal];
            _btnRight.tag = 0;
        } while (randomGenerator == wordsCount || [imageWrong isEqualToString:imageCorrect] || [correctText isEqualToString:wrongText]);
    }
    else{
        NSString *imageCorrect = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",wordsCount]] ];
        UIImage *correctImage = [DocumentDirectory imageNamed:imageCorrect];
        NSString *correctText = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount]]];
        [_btnRight setBackgroundImage:correctImage forState:UIControlStateNormal];
        [_lblRightGoal setText:[NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount]] ]];
        _btnRight.tag = 1;
        
        // secondBalloon
        NSString *imageWrong;
        NSString *wrongText;
        int randomGenerator;
        do {
            randomGenerator = rand() % DictationaryCount;
            imageWrong = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"imageId%d",randomGenerator]] ];
            wrongText = [NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",randomGenerator]] ];
            [_lblLeftGoal setText:[NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",randomGenerator]] ]];
            
//            if (cId == 99999) {
                [_lblSynonym setHidden:NO];
                [_lblOpposite setHidden:NO];
                [_lblOpposite setText:[NSString stringWithFormat:@"%@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"opposite%d",wordsCount]]]];
                
                [_lblSynonym setText:[NSString stringWithFormat:@"Meaning : %@",[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"synonym%d",wordsCount]]]];
//            }
            
            if([_lblSynonym.text isEqualToString:@"0"] || [_lblSynonym.text isEqualToString:@"Meaning : "]){
                [_lblSynonym setHidden:YES];
            }

            
            if([_lblOpposite.text isEqualToString:@"0"]){
                [_lblOpposite setHidden:YES];
            }
            else{
                [_lblOpposite setHidden:NO];
            }

            UIImage *WrongImage = [DocumentDirectory imageNamed:imageWrong];
            [_btnLeft setBackgroundImage:WrongImage forState:UIControlStateNormal];
            _btnLeft.tag = 0;
        } while (randomGenerator == wordsCount || [imageCorrect isEqualToString:imageWrong] || [wrongText isEqualToString:correctText]);
    }
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:SYNONYM_ACTIVE]) {
        [_lblSynonym setHidden:YES];
    }

    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    if (cId > 989) {
        _lblOpposite.hidden = YES;
        _lblSynonym.hidden = YES;
    }

    
    [self playSound];
}

-(void)playSound{
    
    NSString *soundId = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"voiceId%d",wordsCount]];
    if (!soundId || soundId == NULL) {
        soundId = @"EmptySound";
    }
    [self playSound:soundId];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

-(void) playSound:(NSString*) soundName{
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
}


-(void) playBallHittingFieldSidesSounds{
    
    if (ballPlayer) {
        [ballPlayer stop];
        [ballPlayer release];
        ballPlayer = Nil;
    }
    NSString *soundPath =[[NSBundle mainBundle] pathForResource:@"Basketball_bounce" ofType:@"mp3"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundPath];
    ballPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    ballPlayer.numberOfLoops = 0;
    ballPlayer.delegate = self;
    [ballPlayer play];
}

-(void) playBallHITSounds{
    
    if (ballPlayer) {
        [ballPlayer stop];
        [ballPlayer release];
        ballPlayer = Nil;
    }
    NSString *soundPath =[[NSBundle mainBundle] pathForResource:@"Basketball" ofType:@"mp3"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundPath];
    ballPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    ballPlayer.numberOfLoops = 0;
    ballPlayer.delegate = self;
    [ballPlayer play];
}

-(void) loadDatabase{
    int position = 0;
    // Loaading data  from database and saving in dictationaries
    MrPDatabase *database = [[MrPDatabase alloc] init];
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSArray *MrPContentArray;
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    
    if (cId == 99999) {
        
        int gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
        int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
        
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
            NSArray *MrPContentEnglishArray = [database EnglishWordDatabaseArray:gId andpId:pId];
            tempContentDicCorrect = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentEnglishArray count]];
            
            for (MrPContentInfo *info in MrPContentEnglishArray) {
                
                [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDicCorrect setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
                [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]];
                [tempContentDicCorrect setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                [tempContentDicCorrect setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
                [tempContentDicCorrect setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];
                [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",position]];
                position++;
            }
            NSLog(@"Dic is %@",tempContentDicCorrect);
            if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"opposite0"]] isEqualToString:@""])
            {
                [_lblOpposite setHidden:YES];
                [_lblSynonym setHidden:YES];
            }
            else{
                [_lblOpposite setHidden:NO];
            }

        }
        
        else
        {
            NSArray *MrPContentArabicArray = [database ArabicWordDatabaseArray:gId andpId:pId];
            tempContentDicCorrect = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArabicArray count]];
            
            for (MrPContentInfo *info in MrPContentArabicArray)
            {
                [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDicCorrect setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
                [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]];
                [tempContentDicCorrect setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                [tempContentDicCorrect setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
                [tempContentDicCorrect setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];
                [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordsUniqueNumber%d",position]];
                position++;
            }
            if ([[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"opposite0"]] isEqualToString:@""])
            {
                [_lblOpposite setHidden:YES];
                [_lblSynonym setHidden:YES];
            }
            else{
                [_lblOpposite setHidden:NO];
            }

        }
        DictationaryCount = [tempContentDicCorrect count]/7;
        
        if (DictationaryCount < 4) {
            UIAlertView *alerts = [[UIAlertView alloc] init];
            [alerts setTitle:@"In Sufficient Data"];
            [alerts setMessage:@"Please go to Main Menu and select another location"];
            [alerts setDelegate:self];
            [alerts addButtonWithTitle:@"OK"];
            [alerts setTag:12];
            [alerts show];
            [alerts release];
        }
        return;
    }
    
    
    
    if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
        MrPContentArray = [database EnglishWordDatabaseArray:cId];
    }
    else {
        MrPContentArray = [database ArabicWordDatabaseArray:cId];
    }
    tempContentDicCorrect = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    for (MrPContentInfo *info in MrPContentArray) {
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]]
        ;
    if (activeLanguage == 2 || (cId > 989 && cId < 999)) 
        {
            [tempContentDicCorrect setObject:info.englishWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        else
        {
            [tempContentDicCorrect setObject:info.arabicWord forKey:[NSString stringWithFormat:@"Word%d",position]];
        }
        [tempContentDicCorrect setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
        [tempContentDicCorrect setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
        [tempContentDicCorrect setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
        [tempContentDicCorrect setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];

        position++;
    }
    position = 0;
    DictationaryCount = [tempContentDicCorrect count]/7;
}

-(void) playAnimation{
    
    // Cheering Anmiation
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:20];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1) // SIMPLE BOY
    {
        for(int count = 1; count <= 4; count++)
        {
            [images addObject:[UIImage imageNamed:@"Cheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"Cheering_7.png"]];
            [images addObject:[UIImage imageNamed:@"Cheering_8.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2) // SIMPLE GIRL
    {
        for(int count = 1; count <= 2; count++)
        {
            [images addObject:[UIImage imageNamed:@"GCheering_1.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_2.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_3.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_4.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_7.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_11.png"]];
            [images addObject:[UIImage imageNamed:@"GCheering_12.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3) // BASKET BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"BCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"BCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4) // BASKET GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"BCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"BCheering-girl_5.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5) // DOCTOR BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"DCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"DCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6) // DOCTOR GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"DCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"DCheering-girl_5.png"]];
        }
        
    }
    
    
    
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7) // Chef  BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"CCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"CCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8) // Chef GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"CCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"CCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9) // FIRE BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FireCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"FireCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10) // FIRE GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"FireCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11) // FOOTBALL BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"FCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12) // Football GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"FCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"FCheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13) // Arab BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"ACheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"ACheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14) // Arab GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"ACheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"ACheering-girl_5.png"]];
        }
        
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15) // POLICE BOY
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"PCheering_5.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_6.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_7.png"]];
            
            [images addObject:[UIImage imageNamed:@"PCheering_8.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_9.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_10.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering_11.png"]];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16) // POLICE GIRL
    {
        for(int count = 1; count <= 3; count++)
        {
            [images addObject:[UIImage imageNamed:@"PCheering-girl_1.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_2.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_3.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_4.png"]];
            [images addObject:[UIImage imageNamed:@"PCheering-girl_5.png"]];
        }
        
    }
    
    _imgViewCheering.animationImages = images;
    _imgViewCheering.animationDuration = 1.5;
    _imgViewCheering.animationRepeatCount = 2; //Repeats indefinitely
    
    [_imgViewCheering startAnimating];
    [images removeAllObjects];
    [images release];
    
    [_imgViewCheering setImage:NULL];
    
}

-(void) balloonAnimation:(NSString*) animationNumber{
    //    Animation Number Convention :
    //    b1 = left correct
    //    b2 = left wrong
    //    b3 = Right correct
    //    b4 = Right Wrong
    
    // baloon Anmiation
    NSMutableArray *imagesBackground = [[NSMutableArray alloc] initWithCapacity:18];
    
    for(int count = 1; count <= 18; count++)
    {
        [imagesBackground addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@%d.png",animationNumber,count] ]];
    }
    
    //    if ([animationNumber isEqualToString:@"L1"] || [animationNumber isEqualToString:@"L2"]) {
    //        [_btnLeft setImage:nil forState:UIControlStateNormal];
    //    }
    //    else{
    //        [_btnRight setImage:nil forState:UIControlStateNormal];
    //    }
    
    _imgViewBackground.animationImages = imagesBackground;
    _imgViewBackground.animationDuration = 2;
    _imgViewBackground.animationRepeatCount = 0;
    
    [_imgViewBackground startAnimating];
    [imagesBackground removeAllObjects];
    [imagesBackground release];
    //    [_imgViewBackground setImage:[UIImage imageNamed:@"spinIt01.png" ]];
    //    [_imgViewBackground setImage:Nil];
    [_imgViewBackground setImage:[UIImage imageNamed:@"basketball-zero.png" ]];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_btnLeft release];
    [_btnRight release];
    [_btnRepeatSound release];
    [_imgViewCheering release];
    [_imgViewBackground release];
    [imgBALL release];
    [_lblLeftGoal release];
    [_lblRightGoal release];
    [_lblSynonym release];
    [_lblOpposite release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setBtnLeft:nil];
    [self setBtnRight:nil];
    [self setBtnRepeatSound:nil];
    [self setImgViewCheering:nil];
    [self setImgViewBackground:nil];
    [self setImgBALL:nil];
    [self setLblLeftGoal:nil];
    [self setLblRightGoal:nil];
    [self setLblSynonym:nil];
    [self setLblOpposite:nil];
    [super viewDidUnload];
}

-(void) leftScreenTapped{
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    if ([_btnLeft tag] == 1) {
        NSLog(@"Correct Answer");
        reportCorrectCount = reportCorrectCount + 1;
        reportTotatCount = reportTotatCount + 1;
        [self answerPoints];
        
        
        //        [self balloonAnimation:@"L1"];
        
        if (!fireTimerToPlayCheeringCharacter) {
            fireTimerToPlayCheeringCharacter = [[NSTimer scheduledTimerWithTimeInterval:1
                                                                                 target:self
                                                                               selector:@selector(cheerUp)
                                                                               userInfo:nil
                                                                                repeats:NO]retain];
            
        }
    }
    else{
        NSLog(@"Wrong Answer");
        reportTotatCount = reportTotatCount + 1;
        //        [self balloonAnimation:@"L2"];
        
        if (!fireTimerToPlayEncouraging) {
            
            
            
            fireTimerToPlayEncouraging = [[NSTimer scheduledTimerWithTimeInterval:1
                                                                           target:self
                                                                         selector:@selector(EncourageUser)
                                                                         userInfo:nil
                                                                          repeats:NO]retain];
            
        }
    }
    
    
}

-(void) rightScreenTapped{
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    [_btnRight setEnabled:NO];
    [_btnLeft setEnabled:NO];
    if (_btnRight.tag == 1) {
        NSLog(@"Correct Answer");
        reportCorrectCount = reportCorrectCount + 1;
        reportTotatCount = reportTotatCount + 1;
        [self answerPoints];
        //        [self balloonAnimation:@"R1"];
        if (!fireTimerToPlayCheeringCharacter) {
            fireTimerToPlayCheeringCharacter = [[NSTimer scheduledTimerWithTimeInterval:1
                                                                                 target:self
                                                                               selector:@selector(cheerUp)
                                                                               userInfo:nil
                                                                                repeats:NO]retain];
            
        }
        
    }
    else{
        NSLog(@"Wrong Answer");
        reportTotatCount = reportTotatCount + 1;
        //        [self balloonAnimation:@"R2"];
        if (!fireTimerToPlayEncouraging) {
            fireTimerToPlayEncouraging = [[NSTimer scheduledTimerWithTimeInterval:1
                                                                           target:self
                                                                         selector:@selector(EncourageUser)
                                                                         userInfo:nil
                                                                          repeats:NO]retain];
            
        }
        
    }
    
    
    
}
- (IBAction)btnLeftPressed:(id)sender {
    //    if (player) {
    //        [player stop];
    //        [player release];
    //        player = Nil;
    //    }
    //
    //    [_btnRight setEnabled:NO];
    //    [_btnLeft setEnabled:NO];
    //    if ([sender tag] == 1) {
    //        NSLog(@"Correct Answer");
    //        reportCorrectCount = reportCorrectCount + 1;
    //        reportTotatCount = reportTotatCount + 1;
    //        [self answerPoints];
    //
    //
    //        [self balloonAnimation:@"L1"];
    //
    //        if (!fireTimerToPlayCheeringCharacter) {
    //            fireTimerToPlayCheeringCharacter = [[NSTimer scheduledTimerWithTimeInterval:2
    //                                                                   target:self
    //                                                                 selector:@selector(cheerUp)
    //                                                                 userInfo:nil
    //                                                                  repeats:NO]retain];
    //
    //        }
    //    }
    //    else{
    //        NSLog(@"Wrong Answer");
    //        reportTotatCount = reportTotatCount + 1;
    //        [self balloonAnimation:@"L2"];
    //        if (!fireTimerToPlayEncouraging) {
    //            fireTimerToPlayEncouraging = [[NSTimer scheduledTimerWithTimeInterval:2
    //                                                                           target:self
    //                                                                         selector:@selector(EncourageUser)
    //                                                                         userInfo:nil
    //                                                                          repeats:NO]retain];
    //
    //        }
    //    }
    
    
}


-(void) cheerUp{
    [imgBALL setHidden:YES];
    
    if ([hitSide isEqualToString:@"LeftGoal"]) {
        UIImageView* crossImgView = [[UIImageView alloc] initWithFrame:CGRectMake(225, 198, 110, 110)];
        crossImgView.image = [UIImage imageNamed:@"correct.png"];
        [crossImgView setTag:114];
        [self.view addSubview:crossImgView];
    }
    
    else{
        UIImageView* crossImgView = [[UIImageView alloc] initWithFrame:CGRectMake(710, 202, 110, 110)];
        crossImgView.image = [UIImage imageNamed:@"correct.png"];
        [crossImgView setTag:114];
        [self.view addSubview:crossImgView];
        
    }
    
    [self.view setUserInteractionEnabled:NO];
    NSLog(@"CheerUp Animation called");
    if (fireTimerToPlayCheeringCharacter) {
        [fireTimerToPlayCheeringCharacter invalidate];
        [fireTimerToPlayCheeringCharacter release];
        fireTimerToPlayCheeringCharacter = nil;
    }
    [self moveToNextSet];
    [self playCheeringSound];
    [self playAnimation];
}

-(void) EncourageUser{
    
    if ([hitSide isEqualToString:@"LeftGoal"]) {
        UIImageView* crossImgView = [[UIImageView alloc] initWithFrame:CGRectMake(225, 198, 110, 110)];
        crossImgView.image = [UIImage imageNamed:@"X.png"];
        [crossImgView setTag:114];
        [self.view addSubview:crossImgView];
    }
    
    else{
        UIImageView* crossImgView = [[UIImageView alloc] initWithFrame:CGRectMake(710, 202, 110, 110)];
        crossImgView.image = [UIImage imageNamed:@"X.png"];
        [crossImgView setTag:114];
        [self.view addSubview:crossImgView];
        
    }
    
    
    //    [imgBALL setHidden:YES];
    [self.view setUserInteractionEnabled:NO];
    NSLog(@"ENCOURAGE USER CALLED");
    if (fireTimerToPlayEncouraging) {
        [fireTimerToPlayEncouraging invalidate];
        [fireTimerToPlayEncouraging release];
        fireTimerToPlayEncouraging = nil;
    }
    [self moveToNextSet];
    [self playEncouragingSound];
    //        [self moveToNextSet];
    
    
}
- (IBAction)btnRightPressed:(id)sender {
    //    if (player) {
    //        [player stop];
    //        [player release];
    //        player = Nil;
    //    }
    //    [_btnRight setEnabled:NO];
    //    [_btnLeft setEnabled:NO];
    //    if ([sender tag] == 1) {
    //        NSLog(@"Correct Answer");
    //        reportCorrectCount = reportCorrectCount + 1;
    //        reportTotatCount = reportTotatCount + 1;
    //        [self answerPoints];
    //        [self balloonAnimation:@"R1"];
    //        if (!fireTimerToPlayCheeringCharacter) {
    //            fireTimerToPlayCheeringCharacter = [[NSTimer scheduledTimerWithTimeInterval:2
    //                                                                                 target:self
    //                                                                               selector:@selector(cheerUp)
    //                                                                               userInfo:nil
    //                                                                                repeats:NO]retain];
    //
    //        }
    //
    //    }
    //    else{
    //        NSLog(@"Wrong Answer");
    //        reportTotatCount = reportTotatCount + 1;
    //        [self balloonAnimation:@"R2"];
    //        if (!fireTimerToPlayEncouraging) {
    //            fireTimerToPlayEncouraging = [[NSTimer scheduledTimerWithTimeInterval:2
    //                                                                                 target:self
    //                                                                               selector:@selector(EncourageUser)
    //                                                                               userInfo:nil
    //                                                                                repeats:NO]retain];
    //
    //        }
    //
    //    }
}

-(void) answerPoints{
    
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],BasketBall,cId,activeLanguage];
    int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    if (!LevelLastCount) {
        LevelLastCount = 1;
    }
    
    NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",10,LevelLastCount];
    float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
    if (!PointsLastCount) {
        PointsLastCount = 0;
    }
    
    if (LevelLastCount < 3) {
        PointsLastCount = PointsLastCount + 1;
        [[NSUserDefaults standardUserDefaults] setInteger:PointsLastCount forKey:Points];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //    if (wordsCount + 1 == [tempContentDicCorrect count]/5) {
    //        LevelLastCount = LevelLastCount + 1;
    //    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount forKey:levelCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //    NSLog(@"Level is %d and points are %f",LevelLastCount,PointsLastCount);
    correctWordCounter = correctWordCounter + 1;
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 10) {
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
            correctWordCounter = 0;
            
        }
        if (buttonIndex == 1)
        {
            
            if (mySpinItPlayer) {
                [mySpinItPlayer stop];
                [mySpinItPlayer release];
                mySpinItPlayer = nil;
                
            }
            
            if (cheeringSoundTimer) {
                [cheeringSoundTimer invalidate];
                [cheeringSoundTimer release];
                cheeringSoundTimer = Nil;
            }
            
            if (player) {
                [player stop];
                [player release];
                player = Nil;
            }
            [self saveProgressForReport];
            [self dismissModalViewControllerAnimated:NO];
            //            [self saveProgressForReport];
        }
    }
    
    if (alertView.tag == 12) {
        if (mySpinItPlayer) {
            [mySpinItPlayer stop];
            [mySpinItPlayer release];
            mySpinItPlayer = nil;
            
        }
        
        if (cheeringSoundTimer) {
            [cheeringSoundTimer invalidate];
            [cheeringSoundTimer release];
            cheeringSoundTimer = Nil;
        }
        
        if (player) {
            [player stop];
            [player release];
            player = Nil;
        }
        
        [self saveProgressForReport];
        [self dismissModalViewControllerAnimated:NO];
        //            [self saveProgressForReport];
    }
}



-(void) sessionCompletionCheck{
    
    UIAlertView *sessionAlert;
    if (correctWordCounter > 7) {
        
        // Awarding Bonus
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],BasketBall,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        if (!LevelLastCount) {
            LevelLastCount = 1;
        }
        
        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",10,LevelLastCount];
        float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
        if (!PointsLastCount) {
            PointsLastCount = 0;
        }
        
        if (LevelLastCount < 3) {
            PointsLastCount = PointsLastCount + 2;
            [[NSUserDefaults standardUserDefaults] setInteger:PointsLastCount forKey:Points];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",10,1];
        [_lblLevel1 setText:[NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]]];
        
        NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",10,2];
        [_lblLevel2 setText:[NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]]];
        
        if (activeLanguage == 2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good! You have earned Bonus of +2" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
        }
        
        [sessionAlert setTag:10];
        
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"RewardUnlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        
        if (activeLanguage ==2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your progress needs improvement. Try again." delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عفوا !" message:@"تقدمك غير كافي , يجب أن تحاول أكثر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب" otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:1];
    }
    [sessionAlert show];
    [sessionAlert release];
}

-(void) moveToNextSet{
    
    
    [self updateReportCurrentWord];
    NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",10,1];
    [_lblLevel1 setText:[NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]]];
    
    NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",10,2];
    [_lblLevel2 setText:[NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]]];
    
    if (sessionCounter == 10) {
        sessionCounter = 0;
        
        if (wordsCount + 1== DictationaryCount) {
            wordsCount = -1; // because in next step it will be incremented by 1 and will be 1 again
            NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],BasketBall,cId,activeLanguage];
            int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
            [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount+1 forKey:levelCount];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self saveProgressForReport];
            [self insitiantiateForReportCouting];
            
        }
        wordsCount = wordsCount + 1;
        ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],BasketBall,cId,activeLanguage,ActiveGrade];
        
        [[NSUserDefaults standardUserDefaults] setInteger:wordsCount forKey:wordsCounterString];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"WordsCount is %d",wordsCount);
        
        
        [self sessionCompletionCheck];
        return;
    }
    sessionCounter = sessionCounter + 1;
    [_btnRepeatSound setEnabled:NO];
    [_btnLeft setEnabled:NO];
    [_btnRight setEnabled:NO];
    
    if (wordsCount + 1== DictationaryCount) {
        wordsCount = -1; // because in next step it will be incremented by 1 and will be 1 again
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],BasketBall,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        [[NSUserDefaults standardUserDefaults] setInteger:LevelLastCount+1 forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self saveProgressForReport];
        [self insitiantiateForReportCouting];
        
    }
    wordsCount = wordsCount + 1;
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCounterString = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],BasketBall,cId,activeLanguage,ActiveGrade];
    
    [[NSUserDefaults standardUserDefaults] setInteger:wordsCount forKey:wordsCounterString];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"WordsCount is %d",wordsCount);
    
    //    levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",Spinit,cId,activeLanguage];
    //    level = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    
    //    NSString *currentLevel = [NSString stringWithFormat:@"Level 01 : %d",level];
    //    [_lblLevel1 setText:currentLevel];
    
}

-(void) playEncouragingSound{
    
    if (_imgViewBackground.animationImages) {
        [_imgViewBackground stopAnimating];
    }
    
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:3.0
                                                               target:self
                                                             selector:@selector(endTimerAndLoadNextViewItems)
                                                             userInfo:nil
                                                              repeats:YES]retain];
        
    }
    
    int randomNumer = arc4random() % 3 ;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC25";
        NSString *second = @"EC30";
        NSString *third = @"EC31";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }else {
        NSString *first = @"AC04";
        NSString *second = @"AC05";
        NSString *third = @"AC06";
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }
}



-(void) playCheeringSound{
    
    if (_imgViewBackground.animationImages) {
        [_imgViewBackground stopAnimating];
    }
    
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:3.0
                                                               target:self
                                                             selector:@selector(endTimerAndLoadNextViewItems)
                                                             userInfo:nil
                                                              repeats:YES]retain];
        
        
        
    }
    
    int randomNumer = arc4random() % 3 ;
    //    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
    }
}

-(void) endTimerAndLoadNextViewItems{
    
    [imgBALL setHidden:NO];
    [self.view setUserInteractionEnabled:YES];
    
    if([self.view viewWithTag:114]){
        [[self.view viewWithTag:114] removeFromSuperview];
        [[self.view viewWithTag:114] release];
    }
    
    NSString *currentNumber = [NSString stringWithFormat:@"Current Word : %d / %d",wordsCount + 1, DictationaryCount];
    NSLog(@"WordsCount is %@",currentNumber);
    [_wordCounterlbl setText:currentNumber];
    
    [_btnRepeatSound setEnabled:YES];
    [_btnLeft setEnabled:NO];
    [_btnRight setEnabled:NO];
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        cheeringSoundTimer = nil;
    }
    //    if (viewCompletedTimer) {
    //        [viewCompletedTimer invalidate];
    //        viewCompletedTimer = nil;
    //    }
    
    [self populateBalloons];
    
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"5-MatchIT"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    mySpinItPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    mySpinItPlayer.volume = 0.15;
    mySpinItPlayer.numberOfLoops = 20;
    mySpinItPlayer.delegate = self;
    
    if ([mySpinItPlayer prepareToPlay]) {
        [mySpinItPlayer play];
    }
}

-(void) saveProgressForReport{
    // write in db!
    
    //    NSLog(@"retval is %@",retval);
    
    if (!retval || retval == NULL ) {
        return;
    }
    //
    //
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database saveReportProgress:retval];
    
    //    //    NSLog(@"temp is %@",tempContentDic);
    //
    if (retval) {
        [retval removeAllObjects];
        [retval release];
        retval = nil;
    }
}
-(void) updateReportCurrentWord{
    reportWordDesc = [tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"Word%d",wordsCount]];
    reportWordId = [[tempContentDicCorrect objectForKey:[NSString stringWithFormat:@"wordId%d",wordsCount]] intValue];
    int gameId = 11;
    MrPReport *info = [[MrPReport alloc]
                       initWithWordId:reportWordId andWordDesc:reportWordDesc andCorrectCount:reportCorrectCount andTotalCount:reportTotatCount andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
    
    [retval addObject:info];
    
    reportTotatCount = 0;
    reportCorrectCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
    
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
    
    if (touchPoint.x > imgBALL.frame.origin.x - 15 && touchPoint.x < imgBALL.frame.origin.x + 70 && touchPoint.y > imgBALL.frame.origin.y-10) {
        startingTouchPointCorrectionCheck = TRUE;
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (startingTouchPointCorrectionCheck) {
        touchMovedCorrectionCheck = TRUE;
    }
    
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    
    if (touchMovedCorrectionCheck && startingTouchPointCorrectionCheck) {
        
        [self.view setUserInteractionEnabled:NO];
        UITouch *touch = [touches anyObject];
        CGPoint drawPoint = [touch locationInView:self.view];
        
        int xMidPoint = drawPoint.x;
        int yMidPoint = drawPoint.y;
        
        x2 = 0;
        y2 = 0;
        
        int x1 = imgBALL.center.x;
        int y1 = imgBALL.center.y;
        
        float angleVal = (((atan2((xMidPoint - x1) , (yMidPoint - y1)))*180)/M_PI);
        NSLog(@"Angle %.0f", angleVal);
        
        if(angleVal > -160 && angleVal < -140){ // Left Goal
            hitSide = @"LeftGoal";
            
            float diffX = xMidPoint - x1;
            float diffY = yMidPoint - y1;
            double m = diffY/diffX;
            float xOnGraph = 423 / m ;
            x2 = 509 - xOnGraph;
            NSLog(@"slope is %f, so x2 is %f and ",m,x2);
            y2 = 250;
            [self hitBall];
            return;
        }
        
        if(angleVal > 138 && angleVal < 155){ // Right Goal
            hitSide = @"RightGoal";
            
            float diffX = xMidPoint - x1;
            float diffY = yMidPoint - y1;
            double m = diffY/diffX;
            float xOnGraph = 423 / m ;
            x2 = 509 - xOnGraph;
            NSLog(@"slope is %f, so x2 is %f and ",m,x2);
            y2 = 250;
            [self hitBall];
            return;
        }
        
        if((angleVal > 155 && angleVal < 181) || (angleVal < -160 && angleVal > -181)){ // CEntre
            hitSide = @"Centre";
            
            float diffX = xMidPoint - x1;
            float diffY = yMidPoint - y1;
            double m = diffY/diffX;
            float xOnGraph = 672 / m ; // 768 - 95(95 is the offset of the ball)
            x2 = 509 - xOnGraph;
            NSLog(@"slope is %f, so x2 is %f and ",m,x2);
            y2 = 0;
            [self hitBall];
            
            return;
        }
        
        if(angleVal < -90 && angleVal > -139){ // Left Side
            hitSide = @"LeftOut";
            float diffX = xMidPoint - x1;
            float diffY = yMidPoint - y1;
            double m = diffY/diffX;
            float yOnGraph = 509 * m ; // 509 is the offset in x.
            y2 = 673 - yOnGraph;
            NSLog(@"slope is %f, so x2 is %f and ",m,x2);
            
            x2 = 0;
            [self hitBall];
            return;
        }
        
        if(angleVal > 90 && angleVal < 137){ // Right Side
            hitSide = @"RightOut";
            float diffX = xMidPoint - x1;
            float diffY = yMidPoint - y1;
            double m = diffY/diffX;
            float yOnGraph = -509 * m ; // 509 is the offset in x.
            y2 = 673 - yOnGraph;
            NSLog(@"slope is %f, so x2 is %f and ",m,x2);
            
            x2 = 1024;
            [self hitBall];
            return;
        }
        [self reloadingFieldAgain]; // means that ball has been directed downwards;
        //    [self hitBall];
    }
}

-(void) ballReachedDestination{
    //
    //    [imgBALL setHidden:YES];
}
-(void) hitBall{
    
    [UIView animateWithDuration:1.0 animations:^{
        [self playBallHITSounds];
        
        CABasicAnimation* theAnim = [CABasicAnimation animationWithKeyPath:@"position"];
        theAnim.fromValue = [NSValue valueWithCGPoint:imgBALL.center];
        theAnim.toValue = [NSValue valueWithCGPoint:CGPointMake(x2, y2) ];
        theAnim.delegate = self;
        
        CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        fullRotation.fromValue = [NSNumber numberWithFloat:0];
        fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI_2)/180)];
        fullRotation.duration = 0.2;
        fullRotation.repeatCount = 5;
        
        
        CAAnimationGroup *group = [CAAnimationGroup animation];
        group.duration = 1.0;
        group.delegate = self;
        
        if ([hitSide isEqualToString:@"Centre"] || [hitSide isEqualToString:@"LeftOut"] || [hitSide isEqualToString:@"RightOut"]) {
            theAnim.duration = 1.0;
            //            group.delegate = self;
            group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            group.animations = [NSArray arrayWithObjects:fullRotation,theAnim, nil];
        }
        else{
            theAnim.duration = 1.0;
            group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            group.animations = [NSArray arrayWithObjects:fullRotation,theAnim, nil];
        }
        
        [imgBALL.layer addAnimation:group forKey:@"allMyAnimations"];
    }completion:^(BOOL finished) {
        NSLog(@"Animation Has Stopped");
        [self removeBall];
    }];
    
}

-(void) removeBall{
    
    if ([hitSide isEqualToString:@"LeftGoal"] || [hitSide isEqualToString:@"RightGoal"]) {
        ballRemovalTimer = [[NSTimer scheduledTimerWithTimeInterval:0.98
                                                             target:self
                                                           selector:@selector(ballRemovingTimerFired)
                                                           userInfo:nil
                                                            repeats:NO]retain];
        
    }
    else{
        
        ballRemovalTimer = [[NSTimer scheduledTimerWithTimeInterval:0.98
                                                             target:self
                                                           selector:@selector(ballRemovingTimerFired)
                                                           userInfo:nil
                                                            repeats:NO]retain];
    }
    
}

-(void) ballRemovingTimerFired{
    if (ballRemovalTimer) {
        [ballRemovalTimer invalidate];
        ballRemovalTimer = nil;
        [ballRemovalTimer release];
    }
    
    [imgBALL setHidden:YES];
    
    
}

- (void)animationDidStop:(CAAnimation *)theAnimation2 finished:(BOOL)flag {
    if ([hitSide isEqualToString:@"Centre"] || [hitSide isEqualToString:@"LeftOut"] || [hitSide isEqualToString:@"RightOut"]) {
        
        
        [UIView animateWithDuration:1.5 animations:^{
            [imgBALL setHidden:NO];
            CABasicAnimation * theAnimation=[CABasicAnimation animationWithKeyPath:@"position"];
            theAnimation.duration = 1.5;
            theAnimation.fromValue = [NSValue valueWithCGPoint:CGPointMake(x2, y2) ];
            if ([hitSide isEqualToString:@"Centre"]) {
                //                theAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(x2, y2+45) ];
                [self reloadingFieldAgain];
                return;
            }
            if ([hitSide isEqualToString:@"LeftOut"]) {
                theAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(x2+45, y2+45) ];
                [self playBallHittingFieldSidesSounds];
            }
            if ([hitSide isEqualToString:@"RightOut"]) {
                theAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(x2-45, y2+45) ];
                [self playBallHittingFieldSidesSounds];
            }
            
            //                theAnimation.removedOnCompletion = YES;
            
            CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
            fullRotation.fromValue = [NSNumber numberWithFloat:0];
            fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI_2)/180)];
            fullRotation.duration = 1.5;
            fullRotation.repeatCount = 4;
            
            CAAnimationGroup *group = [CAAnimationGroup animation];
            group.duration = 1.5;
            group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            group.animations = [NSArray arrayWithObjects:fullRotation,theAnimation, nil];
            
            
            [imgBALL.layer addAnimation:group forKey:@"animationTwo"];
        }completion:^(BOOL finished) {
            NSLog(@"Animation Has Stopped");
            [self animationCompleted];
        }];
        
        return;
    }
    else{
        [UIView animateWithDuration:0.3 animations:^{
            [imgBALL setHidden:NO];
            CABasicAnimation * theAnimation=[CABasicAnimation animationWithKeyPath:@"position"];
            theAnimation.duration = 0.30;
            theAnimation.fromValue = [NSValue valueWithCGPoint:CGPointMake(x2, y2) ];
            if ([hitSide isEqualToString:@"LeftGoal"]) {
                theAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(273, 323) ];
            }
            if ([hitSide isEqualToString:@"RightGoal"]) {
                theAnimation.toValue = [NSValue valueWithCGPoint:CGPointMake(760,323) ];
            }
            
            CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
            fullRotation.fromValue = [NSNumber numberWithFloat:0];
            fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI_2)/180)];
            fullRotation.duration = 0.3;
            //                fullRotation.repeatCount = 1;
            
            CAAnimationGroup *group = [CAAnimationGroup animation];
            group.duration = 0.30;
            group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            group.animations = [NSArray arrayWithObjects:fullRotation,theAnimation, nil];
            
            
            [imgBALL.layer addAnimation:group forKey:@"animationTwo"];
        }completion:^(BOOL finished) {
            NSLog(@"Animation Has Stopped");
            [self animationCompleted];
        }];
        
        return;
        
    }
}

-(void) animationCompleted{
    
    [imgBALL setHidden:NO];
    
    startingTouchPointCorrectionCheck = FALSE;
    touchMovedCorrectionCheck = FALSE;
    
    if ([hitSide isEqualToString:@"LeftGoal"] || [hitSide isEqualToString:@"RightGoal"]) {
        [imgBALL setHidden:NO];
        if ([hitSide isEqualToString:@"LeftGoal"]) {
            [self leftScreenTapped];
        }
        else
            if ([hitSide isEqualToString:@"RightGoal"]) {
                [self rightScreenTapped];
            }
    }
    else{
        ballHitTimer = [[NSTimer scheduledTimerWithTimeInterval:0.9
                                                         target:self
                                                       selector:@selector(ballWentOutside)
                                                       userInfo:nil
                                                        repeats:NO]retain];
        
    }
    
    
}

-(void) ballWentOutside{
    //        [self.imgBALL setHidden:YES];
    //    if ([hitSide isEqualToString:@"Centre"]) {
    //        UIImageView* cornerHitBallImgView = [[UIImageView alloc] initWithFrame:CGRectMake(x2-15, y2, 60, 60)];
    //        cornerHitBallImgView.image = [UIImage imageNamed:@"top-out.png"];
    //        [cornerHitBallImgView setTag:112];
    //        [self.view addSubview:cornerHitBallImgView];
    //
    //    }
    //    else
    //        if ([hitSide isEqualToString:@"LeftOut"]) {
    //            UIImageView* cornerHitBallImgView = [[UIImageView alloc] initWithFrame:CGRectMake(x2-15, y2-19, 60, 60)];
    //            cornerHitBallImgView.image = [UIImage imageNamed:@"left-out.png"];
    //            [cornerHitBallImgView setTag:112];
    //            [self.view addSubview:cornerHitBallImgView];
    //
    //        }
    //        else
    //            if ([hitSide isEqualToString:@"RightOut"]) {
    //                UIImageView* cornerHitBallImgView = [[UIImageView alloc] initWithFrame:CGRectMake(x2-47, y2-19, 60, 60)];
    //                cornerHitBallImgView.image = [UIImage imageNamed:@"RightOUT.png"];
    //                [cornerHitBallImgView setTag:112];
    //                [self.view addSubview:cornerHitBallImgView];
    //
    //            }
    
    loadNextBallTimer = [[NSTimer scheduledTimerWithTimeInterval:1
                                                          target:self
                                                        selector:@selector(reloadingFieldAgain)
                                                        userInfo:nil
                                                         repeats:NO]retain];
    
    if (ballHitTimer) {
        [ballHitTimer invalidate];
        ballHitTimer = nil;
        [ballHitTimer release];
    }
    
    
}

-(void) reloadingFieldAgain{
    if([self.view viewWithTag:112]){
        [[self.view viewWithTag:112] removeFromSuperview];
        [[self.view viewWithTag:112] release];
    }
    [imgBALL setHidden:NO];
    [self.view setUserInteractionEnabled:YES];
    
}

//- (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
//{
//    // do whatever.
//}

/* Junk
 //    CABasicAnimation *rotateAnim=[CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
 //    rotateAnim.fromValue=[NSNumber numberWithDouble:0.0];
 //    rotateAnim.toValue=[NSNumber numberWithDouble:M_PI_4];
 //
 //    // Customizing the group with duration etc, will apply to all the
 //    // animations in the group
 //    CAAnimationGroup *group = [CAAnimationGroup animation];
 //    group.duration = 0.2;
 //    group.repeatCount = 3;
 //    group.autoreverses = YES;
 //    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
 //    group.animations = [NSArray arrayWithObjects:rotateAnim, nil];
 //
 //    [imgBALL.layer addAnimation:group forKey:@"allMyAnimations"];
 
 
 
 
 //    CAKeyframeAnimation *animKF = [CAKeyframeAnimation animationWithKeyPath:@"position"];
 //    animKF.values = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(imgBALL.frame.origin.x, imgBALL.frame.origin.x)], [NSValue valueWithCGPoint:CGPointMake(400, 400)]/*, [NSValue valueWithCGPoint:CGPointMake(700, 100)], [NSValue valueWithCGPoint:CGPointMake(1000, 400)] , nil];
 //
 //    animKF.keyTimes = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.25]/* ,[NSNumber numberWithFloat:0.5], [NSNumber numberWithFloat:1.0], nil];
 //
 //    animKF.duration = 2;
 //
 //    animKF.rotationMode =  kCAAnimationRotateAuto;
 //
 //    [imgBALL.layer addAnimation:animKF forKey:@"keyframe"];
 
 
 //    UIBezierPath *animationPath = [UIBezierPath bezierPath];
 //    [animationPath moveToPoint:CGPointMake(100, 100)];
 //    [animationPath addLineToPoint:CGPointMake(400, 400)];
 //    [animationPath addLineToPoint:CGPointMake(700, 100)];
 //    [animationPath addLineToPoint:CGPointMake(1000, 400)];
 //
 //    animKF.path = animationPath.CGPath;
 
 
 */
-(void) insitiantiateForReportCouting{
    retval = [[NSMutableArray alloc] init];
    reportCorrectCount = 0;
    reportTotatCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
}

- (IBAction)repeatSound:(id)sender {
    [self playSound];
}
- (IBAction)gotoMainMenu:(id)sender {
    if (mySpinItPlayer) {
        [mySpinItPlayer stop];
        [mySpinItPlayer release];
        mySpinItPlayer = nil;
        
    }
    
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        [cheeringSoundTimer release];
        cheeringSoundTimer = Nil;
    }
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    [self saveProgressForReport];
}
- (BOOL)bannerViewActionShouldBegin:
(ADBannerView *)banner
               willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    [_bannerView removeFromSuperview];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Did Fail to receive ad");
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"received ad");
    [self.view addSubview:_bannerView];
    //    self.view = _bannerView;
}
@end
