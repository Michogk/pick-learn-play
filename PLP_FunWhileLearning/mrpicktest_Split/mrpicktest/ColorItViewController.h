//
//  ColorItViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 28/10/2015.
//  Copyright © 2015 SmartSoft. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>


@interface ColorItViewController : UIViewController<AVAudioPlayerDelegate,UIActionSheetDelegate>
{
    AVAudioPlayer *player;
    int stage;
    
    IBOutlet UILabel *lblCurrentWord;
    int currentWord; // count that at which word we are currently at!
    NSMutableDictionary *tempContentDic;
    int cId;
    int wordImg;
    int sessionCounter;
    IBOutlet UIView *drawingView;
    
    IBOutlet UIImageView *tempDrawImage;
    
    IBOutlet UIImageView *imageViewBackgroundPicture;
    IBOutlet UILabel *lblBackgroundText;
    BOOL grayScaleEffect;
}
@property (strong, nonatomic) IBOutlet UIImageView *mainImage;
- (IBAction)actionBlack:(id)sender;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionGotoMainMenu:(id)sender;
- (IBAction)actionReset:(id)sender;
- (IBAction)actionHearWord:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *imageViewWord;
- (IBAction)actionHelp:(id)sender;

- (IBAction)actionREd:(id)sender;
- (IBAction)actionBlue:(id)sender;
- (IBAction)actionGreen:(id)sender;

- (IBAction)pencilPressed:(id)sender;
- (IBAction)actionGrayScale:(id)sender;
- (IBAction)save:(id)sender;

@end
