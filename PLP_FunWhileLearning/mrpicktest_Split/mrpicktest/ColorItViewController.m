//
//  ColorItViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 28/10/2015.
//  Copyright © 2015 SmartSoft. All rights reserved.
//

#import "ColorItViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "SimpleAudioEngine.h"
#import "MyPlayGroundViewController.h"
#import "HelpSoundViewController.h"
#import "DocumentDirectory.h"
#import "MrPReport.h"
#import "Twitter/TWTweetComposeViewController.h"

@interface ColorItViewController ()
{
    CGPoint lastPoint;
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat brush;
    CGFloat opacity;
    BOOL mouseSwiped;
}

@end

@implementation ColorItViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    grayScaleEffect = false;
    
    [self loadDatabase];
    [self showUpMenu];
    opacity = 1.0;
    red = 0.0/255.0;
    green = 0.0/255.0;
    blue = 0.0/255.0;
    brush = 5.0;
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    [_imageViewWord release];
    [drawingView release];
    [lblBackgroundText release];
    [lblCurrentWord release];
    
    [tempDrawImage release];
    [imageViewBackgroundPicture release];
    [super dealloc];
}

-(void) setBackgroundImage{
    NSString *imageCurrent = [NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",currentWord]] ];
    UIImage *image = [DocumentDirectory imageNamed:imageCurrent];
    if (grayScaleEffect) {
        
        UIImage *filteredImage = [self convertImageToGrayScale:image];
        imageViewBackgroundPicture.image = filteredImage;
        imageViewBackgroundPicture.alpha = 1.0;

    }
    else{
        imageViewBackgroundPicture.image = image;
        imageViewBackgroundPicture.alpha = 0.8;
    }
}
- (void) showUpMenu
{
    if ([tempContentDic count] > 0 && (cId < 999 || cId > 989)) {
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"writeItwordForClassification%dLanguage%d%@",cId,activeLanguage,ActiveGrade]];
        if ((currentWord == [tempContentDic count]/4) || (!currentWord) || currentWord > [tempContentDic count]/4) {
            currentWord = 0;
        }
        
        
        [self setBackgroundImage];
        
        int totalWords = [tempContentDic count]/4;
        NSString *wordsCunter = [NSString stringWithFormat:@"%d / %d",currentWord + 1, totalWords];
        lblCurrentWord.text = wordsCunter;
        
        wordImg = [[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",currentWord]] intValue];
        if (!wordImg || wordImg == 0) {
            wordImg = 1;
        }
        
        
        if (cId > 989 && cId < 999) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
            NSString * documentsPath = [paths objectAtIndex:0];
            NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
            NSString * filename = [NSString stringWithFormat:@"%@/%d.png",activeGrade,wordImg];
            NSString * filePath = [documentsPath stringByAppendingPathComponent:filename];
            if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                
            }
            else {
                filePath = [NSString stringWithFormat:@"1.png"];
            }
            
            filePath = filename;
            
            if(cId > 989 && cId < 999){
                filename = [NSString stringWithFormat:@"%@/%d.png",activeGrade,wordImg];
                filePath = [documentsPath stringByAppendingPathComponent:filename];
                if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                    //        sprite = [CCSprite spriteWithFile:filePath];
                }
                else {
                    filePath = [NSString stringWithFormat:@"2.png"];
                }
                UIImage *image = [UIImage imageWithContentsOfFile:filePath];
                self.imageViewWord.image = image;
            }
        }
        else{
            self.imageViewWord.image = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%d",wordImg]];
        }
        
        
        //If file exist in documents folder use that one
        
        
        
    }
    else {
        
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        
        if (cId > 989 && cId < 999) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            if (activeLanguage == 1) {
                [alert setTitle:@"احذر"];
                [alert setMessage:@" البيانات لا تكفي في ملفاتك"];
            }
            else {
                [alert setTitle:@"WARNING"];
                [alert setMessage:@"Data NOT sufficient in your folder"];
            }
            
            [alert setDelegate:self];
            [alert addButtonWithTitle:@"OK"];
            [alert setTag:2];
            [alert show];
            [alert release];
        }
        //        else
        
        
        //
        //            {
        //                UIAlertView *alert = [[UIAlertView alloc] init];
        //                if (activeLanguage == 1) {
        //                    [alert setTitle:@"احذر "];
        //                    [alert setMessage:@" البيانات لا تكفي"];
        //                }
        //                else {
        //                    [alert setTitle:@"WARNING"];
        //                    [alert setMessage:@"SORRY! Data Not Sufficient"];
        //                }
        //
        //                [alert setDelegate:self];
        //                [alert addButtonWithTitle:@"OK"];
        //                [alert setTag:2];
        //                [alert show];
        //                [alert release];
        //            }
        //
        //
        //    }
    }
    [self hearWord];
}

-(void) loadDatabase{
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    //    int LanguageSupport = [[NSUserDefaults standardUserDefaults] integerForKey:@"LanguageSupport"];
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    
    int position = 0;
    
    // cId 99999 means its myskillscamp, otherwise its simple classification
    if (cId == 99999) {
        
        
        int gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
        int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
        
        int position = 0;
        
        if (activeLanguage == 2) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                if (info.isSentence == 1) {
                    NSString *sentence = info.englishWord;
                    NSArray *componentsSeparatedByWhiteSpace = [sentence componentsSeparatedByString:@" "];
                    if([componentsSeparatedByWhiteSpace count] < 3){
                        NSLog(@"Less then 3 spaces");
                        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                        [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                        position++;
                    }
                    
                }
                else {
                    [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                    [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                    [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                    [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                    position++;
                }
                
            }
        }
        
        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                if (info.isSentence == 1) {
                    NSString *sentence = info.arabicWord;
                    NSArray *componentsSeparatedByWhiteSpace = [sentence componentsSeparatedByString:@" "];
                    if([componentsSeparatedByWhiteSpace count] < 3){
                        NSLog(@"Less then 3 spaces");
                        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                        [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                        position++;
                    }
                }
                else {
                    [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                    [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                    [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                    [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                    position++;
                }
                
            }
        }
    }
    
    
    // if goes in the loop below, it means its simple classification from city map
    else
    {
        if (activeLanguage == 2 || cId > 989) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:cId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                if (info.isSentence == 1) {
                    NSString *sentence = info.englishWord;
                    NSArray *componentsSeparatedByWhiteSpace = [sentence componentsSeparatedByString:@" "];
                    if([componentsSeparatedByWhiteSpace count] < 3){
                        NSLog(@"Less then 3 spaces");
                        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                        [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                        position++;
                    }
                }
                else {
                    [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                    [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                    [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                    [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                    position++;
                }
                
            }
            NSLog(@"Dictationary at Exit is %@",tempContentDic);
        }
        
        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:cId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                if (info.isSentence == 1) {
                    NSString *sentence = info.arabicWord;
                    NSArray *componentsSeparatedByWhiteSpace = [sentence componentsSeparatedByString:@" "];
                    if([componentsSeparatedByWhiteSpace count] < 3){
                        NSLog(@"Less then 3 spaces");
                        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                        [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                        position++;
                        
                    }
                }
                else {
                    
                    [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                    [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]]; // keeping it english word so in class, our convention remain same
                    [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                    [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                    position++;
                }
            }
        }
    }
    
    if ([tempContentDic count] == 0) {
        
    }
    
    [database release];
    database = Nil;
    
}



-(void) DoNothing{
}

-(void) NextWord{
    self.mainImage.image = nil;
    //    if (sessionCounter == 10) {
    //        sessionCounter = 0;
    //        [self sessionCompletionCheck];
    //        return;
    //    }
    sessionCounter = sessionCounter + 1;
    
    currentWord  = currentWord + 1;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordSaver = [NSString stringWithFormat:@"writeItwordForClassification%dLanguage%d%@",cId,activeLanguage,ActiveGrade];
    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self showUpMenu];
    
    
}

#pragma mark - Sound
-(void) hearWord{
    
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    
    NSString* soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
    //    NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"];
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
    
    
}

-(void) playHelpSoundEng{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayEnglishHelpSound:nil];
    //    [helpSoundVC release];
    //    helpSoundVC = Nil;
    
}
-(void) playHelpSoundAra{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayArabicHelpSound:nil];
    //    [helpSoundVC release];
    //    helpSoundVC = Nil;
    
}
#pragma mark - Report
-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"1-writeIT-TypeIT"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.volume = 0.15;
    player.numberOfLoops = 20;
    player.delegate = self;
}

#pragma mark - Action
- (IBAction)actionGrayScale:(id)sender
{
    if (grayScaleEffect) {
        grayScaleEffect = false;
    }
    else{
        grayScaleEffect = true;
    }
    [self setBackgroundImage];
}
- (IBAction)actionBlack:(id)sender {
    //        self.autograph.strokeColor = [UIColor blackColor];
    red = 0.0/255.0;
    green = 0.0/255.0;
    blue = 0.0/255.0;
}

- (IBAction)actionNext:(id)sender {
    [self NextWord];
}

- (IBAction)actionGotoMainMenu:(id)sender {
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    
}

- (IBAction)actionReset:(id)sender {
    //	[self.autograph reset:self];
    self.mainImage.image = nil;
}

- (IBAction)actionHearWord:(id)sender {
    [self hearWord];
}
- (IBAction)actionHelp:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"GH019" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

#pragma mark - Alert
-(void) sessionCompletionCheck{
    
    UIAlertView *sessionAlert;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 2) {
        sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good!" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
    }
    else {
        sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
    }
    
    [sessionAlert setTag:1];
    [sessionAlert show];
    [sessionAlert release];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    
    if(alertView.tag == 2)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        }
        //#warning REMOVE WORD
        //                [self dismissModalViewControllerAnimated:NO];
        
    }
    else {
        
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
            [self NextWord];
        }
        if (buttonIndex == 1)
        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            }
            //#warning REMOVE WORD
            //                    [self dismissModalViewControllerAnimated:NO];
        }
    }
}

- (IBAction)actionREd:(id)sender {
    //        self.autograph.strokeColor = [UIColor redColor];
    red = 255.0/255.0;
    green = 0.0/255.0;
    blue = 0.0/255.0;
}

- (IBAction)actionBlue:(id)sender {
    //        self.autograph.strokeColor = [UIColor blueColor];
    red = 0.0/255.0;
    green = 0.0/255.0;
    blue = 255.0/255.0;
}

- (IBAction)actionGreen:(id)sender {
    //        self.autograph.strokeColor = [UIColor greenColor];
    red = 0.0/255.0;
    green = 255.0/255.0;
    blue = 0.0/255.0;
}

#pragma mark - Writing

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    mouseSwiped = NO;
    UITouch *touch = [touches anyObject];
    lastPoint = [touch locationInView:tempDrawImage];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    mouseSwiped = YES;
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:tempDrawImage];
    
    UIGraphicsBeginImageContext(tempDrawImage.frame.size);
    [tempDrawImage.image drawInRect:CGRectMake(0, 0, tempDrawImage.frame.size.width, tempDrawImage.frame.size.height)];
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush );
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
    
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    tempDrawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    [tempDrawImage setAlpha:opacity];
    UIGraphicsEndImageContext();
    
    lastPoint = currentPoint;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if(!mouseSwiped) {
        UIGraphicsBeginImageContext(tempDrawImage.frame.size);
        [tempDrawImage.image drawInRect:CGRectMake(0, 0, tempDrawImage.frame.size.width, tempDrawImage.frame.size.height)];
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, opacity);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
        tempDrawImage.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    UIGraphicsBeginImageContext(self.mainImage.frame.size);
    [self.mainImage.image drawInRect:CGRectMake(0, 0, tempDrawImage.frame.size.width, tempDrawImage.frame.size.height) blendMode:kCGBlendModeNormal alpha:1.0];
    [tempDrawImage.image drawInRect:CGRectMake(0, 0, tempDrawImage.frame.size.width, tempDrawImage.frame.size.height) blendMode:kCGBlendModeNormal alpha:opacity];
    self.mainImage.image = UIGraphicsGetImageFromCurrentImageContext();
    tempDrawImage.image = nil;
    UIGraphicsEndImageContext();
}


- (void)viewDidUnload
{
    [self setMainImage:nil];
    tempDrawImage = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (IBAction)pencilPressed:(id)sender {
    
    UIButton * PressedButton = (UIButton*)sender;
    
    switch(PressedButton.tag)
    {
        case 0:
            red = 0.0/255.0;
            green = 0.0/255.0;
            blue = 0.0/255.0;
            break;
        case 1:
            red = 105.0/255.0;
            green = 105.0/255.0;
            blue = 105.0/255.0;
            break;
        case 2:
            red = 255.0/255.0;
            green = 0.0/255.0;
            blue = 0.0/255.0;
            break;
        case 3:
            red = 0.0/255.0;
            green = 0.0/255.0;
            blue = 255.0/255.0;
            break;
        case 4:
            red = 102.0/255.0;
            green = 204.0/255.0;
            blue = 0.0/255.0;
            break;
        case 5:
            red = 102.0/255.0;
            green = 255.0/255.0;
            blue = 0.0/255.0;
            break;
        case 6:
            red = 51.0/255.0;
            green = 204.0/255.0;
            blue = 255.0/255.0;
            break;
        case 7:
            red = 160.0/255.0;
            green = 82.0/255.0;
            blue = 45.0/255.0;
            break;
        case 8:
            red = 255.0/255.0;
            green = 102.0/255.0;
            blue = 0.0/255.0;
            break;
        case 9:
            red = 255.0/255.0;
            green = 255.0/255.0;
            blue = 0.0/255.0;
            break;
        case 10:
            red = 255.0/255.0;
            green = 255.0/255.0;
            blue = 255.0/255.0;
            break;
        case 11:
            red = 255.0/255.0;
            green = 105.0/255.0;
            blue = 180.0/255.0;
            break;
        case 12:
            red = 138.0/255.0;
            green = 43.0/255.0;
            blue = 226.0/255.0;
            break;
    }
}

#pragma mark - Save Image
- (IBAction)save:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@""
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Save to Camera Roll", @"Cancel", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (buttonIndex == 1)
//    {
//        Class tweeterClass = NSClassFromString(@"TWTweetComposeViewController");
//        
//        if(tweeterClass != nil) {   // check for Twitter integration
//            
//            // check Twitter accessibility and at least one account is setup
//            if([TWTweetComposeViewController canSendTweet]) {
//                
//                UIGraphicsBeginImageContextWithOptions(self.mainImage.bounds.size, NO,0.0);
//                [self.mainImage.image drawInRect:CGRectMake(0, 0, self.mainImage.frame.size.width, self.mainImage.frame.size.height)];
//                UIImage *SaveImage = UIGraphicsGetImageFromCurrentImageContext();
//                UIGraphicsEndImageContext();
//                
//                TWTweetComposeViewController *tweetViewController = [[TWTweetComposeViewController alloc] init];
//                // set initial text
//                [tweetViewController setInitialText:@"Check out this drawing I made from a tutorial on raywenderlich.com:"];
//                
//                // add image
//                [tweetViewController addImage:SaveImage];
//                tweetViewController.completionHandler = ^(TWTweetComposeViewControllerResult result) {
//                    if(result == TWTweetComposeViewControllerResultDone) {
//                        // the user finished composing a tweet
//                    } else if(result == TWTweetComposeViewControllerResultCancelled) {
//                        // the user cancelled composing a tweet
//                    }
//                    [self dismissViewControllerAnimated:YES completion:nil];
//                };
//                
//                [self presentViewController:tweetViewController animated:YES completion:nil];
//            } else {
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You can't send a tweet right now, make sure you have at least one Twitter account setup and your device is using iOS5" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alertView show];
//            }
//        } else {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You must upgrade to iOS5.0 in order to send tweets from this application" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertView show];
//        }
//        
//    } else if(buttonIndex == 0) {
    
        UIGraphicsBeginImageContextWithOptions(self.mainImage.bounds.size, NO, 0.0);
        [self.mainImage.image drawInRect:CGRectMake(0, 0, self.mainImage.frame.size.width, self.mainImage.frame.size.height)];
        UIImage *SaveImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        UIImageWriteToSavedPhotosAlbum(SaveImage, self,@selector(image:didFinishSavingWithError:contextInfo:), nil);
//    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    // Was there an error?
    if (error != NULL)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Image could not be saved.Please try again"  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close", nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Image was successfully saved in photoalbum"  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Close", nil];
        [alert show];
    }
}

- (UIImage *)convertImageToGrayScale:(UIImage *)image {
    
    
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage;
}


@end

