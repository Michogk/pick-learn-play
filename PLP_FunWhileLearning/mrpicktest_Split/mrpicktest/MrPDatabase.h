//
//
//  Mr.Pick
//
//  Created by Muhammad Mosib Asad on 9/14/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import <QuartzCore/QuartzCore.h>

@interface MrPDatabase : NSObject{
    sqlite3 *_database;
    NSMutableDictionary *tempContentDic;
}

+ (MrPDatabase*) database;
- (NSArray *)ReadFolderDatabase;
- (NSArray *)ReadWordDatabase:(int) classification;
-(void ) removeWord:(int) wordIdToBeRemoved;
- (NSArray *)EnglishWordDatabaseArray:(int )cId;
- (NSArray *)ArabicWordDatabaseArray:(int )cId;
- (NSArray *)ClassificationDatabaseArray;
- (NSArray *)MySkillsGrammerArray;
- (NSArray *)MySkillsPrinciplesArray;
- (NSArray *)MySkillsLearningGrammar;
-(void) saveNewFolder:(NSString *) folderName andClassificationId:(int ) classificationId;
-(void) saveNewWordInFoler:(NSString*)word andWordId:(int )wordId whereClassificationId:(int )currentClassificationId andVoiceId:(NSString *)voice andisSentence:(int)isSentence andImageId:(int) imageId;
-(void) DeleteFolder:(int )classificationToBeRemoved;
- (NSArray *)ArabicWordDatabaseArray:(int )gId andpId:(int) pId;
- (NSArray *)fetchAllAvailableClassificationsForSelectedParamters:(int )cId andActiveGrade:(NSString*) activeGrade andActiveLanguage:(NSString*) table;
- (NSArray *)EnglishWordDatabaseArray:(int )gId andpId:(int) pId;
-(void) saveReportProgress:(NSMutableArray*) retVal;
-(NSArray*) getReportData;
- (NSArray *)ReadClassificationTable;
-(void) clearReportRecords;
- (NSArray *)ReadBooksDatabase;
- (NSArray *)ReadBookFoldersDatabaseWithClassification:(int) classificationIdOfBook;
- (NSArray *)ReadBooksWordsDatabase:(int) classification;
- (NSArray *)ArabicWordDatabaseArrayForMainMenu:(NSArray* )cIdArray;
- (NSArray *)loadAllClassificaton;
- (NSArray *)loadImageForAvailableClassifications:(NSArray*) pCIdArray;
- (NSArray *)databaseForParticularClassification:(int )cId andTable:(NSString*) table;
- (NSArray *)EnglishWordDatabaseArrayForMainMenu:(NSArray* )cIdArray;
- (NSArray *)fetchAllAvailableClassificationsWithGrammarIdsIncluded:(BOOL) pGIdIncluded;
- (NSArray *)fetchAllAvailableClassificationsForStores;
@end