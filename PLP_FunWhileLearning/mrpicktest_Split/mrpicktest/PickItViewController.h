//
//  DragImagesGameViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 3/10/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>

@interface PickItViewController : UIViewController<AVAudioPlayerDelegate,ADBannerViewDelegate>{
    int answersCounter;
    BOOL draggingBegin;
    CGRect itemOriginalframe;
    NSMutableDictionary *tempContentDicCorrect;
    NSMutableDictionary *tempIncontentDicCorrect;
    NSMutableArray *arrRecordCorrectTagsOfItems;
    NSMutableArray *arrRecordINCorrectTagsOfItems;
    
    NSMutableArray *arrRecordCorrectTagsOfItemsWithWordIds;
    NSMutableArray *arrRecordINCorrectTagsOfItemsWithWordIds;
    AVAudioPlayer *player;
    int currentCorrectIndexPosition;
    int wordsCount;
    NSTimer *viewCompletedTimer;
    int correctCounter;
    NSTimer* cheeringSoundTimer;
    AVAudioPlayer *myPickItPlayer;
    BOOL firstLaunch;
    int cId;
    
    UILabel *_lblLevel1;
    UILabel *_lblLevel2;
    BOOL cityMapClickedWithActiveFolder;
    
    // Report Data
    NSMutableArray *retval;
    
    int reportWordId01;
    NSString *reportWordDesc01;
    int reportCorrectCount01;
    int reportTotatCount01;
    
    int reportWordId02;
    NSString *reportWordDesc02;
    int reportCorrectCount02;
    int reportTotatCount02;
    
    int reportWordId03;
    NSString *reportWordDesc03;
    int reportCorrectCount03;
    int reportTotatCount03;

    IBOutlet UILabel *labelSynonym;
}
@property (retain, nonatomic) IBOutlet UIImageView *imgViewCheering;
@property (retain, nonatomic) IBOutlet UIButton *btnNext;
- (IBAction)btnNextPressed:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *imgCheck1;
@property (retain, nonatomic) IBOutlet UIImageView *imgCheck2;
@property (retain, nonatomic) IBOutlet UIImageView *imgCheck3;
@property (retain, nonatomic) IBOutlet UIImageView *imgClassification;
@property (retain, nonatomic) IBOutlet UILabel *labelClassification;
@property (strong, nonatomic) ADBannerView *bannerView;
- (IBAction)playClassifcationSound:(id)sender;
- (IBAction)gotoMainMenu:(id)sender;
- (IBAction)hearCorrectWord:(id)sender;

@end
