//
//  ExpressItViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 9/20/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>
#import "ExpressAnswersPopUp.h"

@interface ExpressItViewController : UIViewController<AVAudioPlayerDelegate,ADBannerViewDelegate,AVAudioRecorderDelegate,ExpressAnswersPopUpDelegate>{
    AVAudioPlayer *expressItBackgroundPlayer;
    AVAudioPlayer *wordPlayer;
    NSMutableDictionary* tempContentDic;
    int currentWord;
    int cId;
    NSString* activeGrade;
    int activeLanguage;
    
    // Recorder methods
    AVAudioPlayer *player;
    NSMutableDictionary *recordSetting;
    NSMutableDictionary *editedObject;
    NSString *recorderFilePath;
    AVAudioRecorder *recorder;
    SystemSoundID soundID;
    NSTimer *timer;
    ExpressAnswersPopUp *popup;
    

}
@property (retain, nonatomic) IBOutlet UIButton *btnFirstImage;
@property (retain, nonatomic) IBOutlet UIButton *btnSecondImg;
@property (retain, nonatomic) IBOutlet UIButton *btnThirdImg;
@property (retain, nonatomic) IBOutlet UIProgressView *secondProgressView;
@property (retain, nonatomic) IBOutlet UIProgressView *thirdProgressView;
- (IBAction)gotoMainMenu:(id)sender;
- (IBAction)btnBackPressed:(id)sender;

@property (retain, nonatomic) IBOutlet UIProgressView *firstProgessView;
- (IBAction)BtnFirstImagePressed:(id)sender;
- (IBAction)btnSecondImgPressed:(id)sender;
- (IBAction)btnThirdImgPressed:(id)sender;

- (IBAction)btnStartRecrodingFirstImgAnswer:(id)sender;
- (IBAction)btnStartRecrodingSecondImgAnswer:(id)sender;
- (IBAction)btnStartRecrodingThirdImgAnswer:(id)sender;

- (IBAction)btnStopRecordingFirstImgAnswer:(id)sender;
- (IBAction)btnStopRecordingSecondImgAnswer:(id)sender;
- (IBAction)btnStopRecordingThirdImgAnswer:(id)sender;

- (IBAction)btnListenFirstAnswer:(id)sender;
- (IBAction)btnListenSecondAnswer:(id)sender;
- (IBAction)btnListenThirdAnswer:(id)sender;
- (IBAction)btnNextPressed:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnNext;
@property (retain, nonatomic) IBOutlet UILabel *lblRecordingStatusFirst;
@property (retain, nonatomic) IBOutlet UILabel *lblRecordingStatusTwo;
@property (retain, nonatomic) IBOutlet UILabel *lblRecordingStatusThree;

- (IBAction)actionAnswerTypingTwo:(id)sender;
- (IBAction)actionAnswerTypingThree:(id)sender;
- (IBAction)actionAnswerTypingOne:(id)sender;
@end
