//
//  MyCampController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/17/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MyCampController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate,AVAudioPlayerDelegate>  {
        AVAudioPlayer *myCampPlayer;
            AVAudioPlayer *player;
    IBOutlet UIScrollView *scrollView1;	// holds five small images to scroll horizontally
   
    IBOutlet UIImageView *ActiveImage;
    UILabel *lblArabicWord;
    IBOutlet UIButton *btnMyCamp;
    int _kNumImages;
    int ps; // use for getting refrence
    IBOutlet UILabel *lblWord;
    UIImageView *selectedImageView;
    UIButton *englishWordOrSentence;
    int _currentWordIndex; // Use to keep track of count within same classification id in dictationary
    UIButton *arabicWordButton;
    BOOL showMovie;
    
    
    NSMutableDictionary *_tempContentDataDic;
    NSMutableDictionary *_tempClassificationDic;
    NSMutableDictionary *_arabicWordDic;
    
    NSMutableDictionary *tempContentDic;
    int _upperLimit;
    int _lowerLimit;
    int activeLanguage;
    int LanguageSupport;
    int sender_tag;
    NSMutableArray *soundArray;
    BOOL isSentence;
    
    IBOutlet UIImageView *backgroundImgView;
    UIView *charactersView;
    NSMutableArray *_completeWordArray;
    NSString *_correctText;
    NSMutableDictionary *characterDic;
    UIImageView *cheeringImgView;
    UILabel *cellText;
    UILabel *lblArabicWordButton;
    int cheeringAnimationCounter;
    int cheeringImageNumber;
    NSTimer *cheeringAnimationTimer;
    int cId;
    NSTimer *soundTimer;
    int count;
    int yLine;
    int sentenceWordsCounter;
    NSTimer *cheeringSoundTimer;
//    int autoCheckingDraggedText;
    
    IBOutlet UIImageView *imgView4Feather;
    IBOutlet UIImageView *imgView3Feather;
    IBOutlet UIImageView *imgView2Feather;
    IBOutlet UIImageView *imgView1Feather;

    
}
- (IBAction)GotoMainMenu:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *imgEnglishEar;
@property (retain, nonatomic) IBOutlet UIImageView *imgArabicEar;

- (IBAction)helpURL:(id)sender;
-(void) playSoundEnglish;
- (IBAction)playEnglishSound:(id)sender;
- (IBAction)playArabicSound:(id)sender;
-(void) playSoundArabic;
- (IBAction)HelpSound:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *checkButton;
@property (retain, nonatomic) IBOutlet UIButton *UndoButton;

- (IBAction)checkDraggedText:(id)sender;
- (IBAction)ResetText:(id)sender;
@property (nonatomic, retain) UIScrollView *scrollView1;
@property (nonatomic,retain) UIImageView *ActiveImage;
@property (nonatomic,retain) UIButton *btnMyCamp;
@property (nonatomic,retain ) UILabel *lplWord;
@property (copy) NSString* _correctText;

@property (retain, nonatomic) IBOutlet UILabel *lblSynonym;
@property (retain, nonatomic) IBOutlet UILabel *lblOpposite;

@end
