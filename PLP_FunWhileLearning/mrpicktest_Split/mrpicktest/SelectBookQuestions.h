//
//  SelectBookQuestions.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/3/14.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBookQuestions : UIView
@property (retain, nonatomic) IBOutlet UIButton *btnReadBook;
@property (retain, nonatomic) IBOutlet UIButton *btnQuestions;
@property (retain, nonatomic) IBOutlet UIButton *btnClose;
+(id) createView;
@end
