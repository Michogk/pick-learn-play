//
//  HelpViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "HelpViewController.h"
//#import "MKParentalGate.h"

@interface HelpViewController ()

@end

@implementation HelpViewController
@synthesize webView;

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

-(void) viewDidAppear:(BOOL)animated{
    
//    MKParentalGateSuccessBlock success = ^{
    
        NSString *helpURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"HelpURL"];
        webView.opaque = NO;
        webView.backgroundColor = [UIColor clearColor];
        self.webView.delegate = self;
        [self.webView setScalesPageToFit:YES];
        if (!helpURL) {
            return;
        }
        
        NSString *urlAddress = [NSString stringWithFormat:@"http://www.mrpickit.net/apphelp/%@.html",helpURL];
        //      NSString *urlAddress = @"http://www.apple.com/education/mobile-learning/?sr=hotnews?sr=hotnews.rss";
        NSLog(@"helpURL is %@",urlAddress);
        NSString *encodedUrl = [urlAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url = [NSURL URLWithString:encodedUrl];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [webView loadRequest:requestObj];
//    };
//    
//    [MKParentalGate displayGateWithCurrentViewController:self successBlock:success failureBlock:NULL];
    
    

}
- (void)viewDidLoad
{
    [super viewDidLoad];


}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if ([error code] != -999) {
        NSLog(@"Error code 999");
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.webView.delegate = nil;
    self.webView = nil;
}

- (void)viewDidUnload
{
    [self setWebView:nil];
    NSLog(@"ViewDidUnload CAlled for HelpViewController");
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

@end
