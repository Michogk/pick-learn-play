//
//  WordsCell.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 23/09/2015.
//  Copyright (c) 2015 SmartSoft. All rights reserved.
//

#import "WordsCell.h"

@implementation WordsCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(id) createCell{
    WordsCell *cell;
    cell = [Common loadNibName:@"WordsCell" owner:self options:nil];
    return cell;
}


- (void)dealloc {
    [_itemImgView release];
    [super dealloc];
}
@end
