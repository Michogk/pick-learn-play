
//
//  CityMapViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/13/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "CityMapViewController.h"
#import "CityPopupController.h"
#import "SetupViewController.h"
#import "MyHomeViewController.h"
#import "MySkillsViewController.h"
#import "MyCampController.h"
#import "MyAchievmentViewController.h"
#import "MyStoreViewController.h"
#import "MyPlayGroundViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "SimpleAudioEngine.h"
#import "DocumentDirectory.h"
#import "MovieViewController.h"
//---------------
#define ZOOM_VIEW_TAG 100
#define ZOOM_STEP 1.5F

#define BAKERY 20
#define OFFICE 240
#define ZOO 380 
#define BEACH 30 
#define COFFEE_SHOP 50  
#define FARM 80
#define SKY 340
#define MOUNTAINS 110
#define SEA 320
#define RACING_COURT 120
#define HOSPITAL 390
#define METRO 170
#define HOTEL 140
#define ICECREAM_PARLOUR 150
#define STADIUM 350
#define MALL 160
#define RADIO_STATION 70
#define GLASS_HOUSE 250
#define SUN_HOUSE 330
#define CHAIR_HOUSE 100
#define TEDDY_BEAR 370
#define POLICE_STATION 290
#define RESCUE911 90
#define TCHE 300
#define MRPICK_HOUSE 10
#define UNIVERSITY 60
#define UNIVERSITYSTREET 260
#define SCHOOL 310
#define HOSTEL 360
#define EGG_HOUSE 270
#define MACDONALDS 280
#define HUNTED_HOUSE 40

@interface CityMapViewController    (UtilityMethods)
- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;
    
@end

@implementation CityMapViewController 
//@synthesize BtnFarm;
@synthesize backgroundImg;

NSInteger  zoomTime =0;

@synthesize imageScrollView, imageView;/*,btnMall;*/


- (void)loadView {
    [super loadView];
    
//    alertToPurchaseContent = [[UIAlertView alloc] initWithTitle:@"Download Content from My BookStore" message:@"Please go to My BookStore and purchase Content for Mr. Pic" delegate:self cancelButtonTitle:@"Go to My Bookstore" otherButtonTitles:@"Exit", nil];
//
//    [alertToPurchaseContent setTag:12];

    
    
    imageView.userInteractionEnabled = YES;

    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        backgroundImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_map.png",[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"]]]; 
        NSLog(@"Map to load is %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"]);
    }
    
    // Adding Buttons
    UIButton *btnMySkills = [UIButton buttonWithType:UIButtonTypeCustom];    
    CGRect newSize = CGRectMake(580-15-15, 330+24-45-30, 50+10+15+4, 50+50); // position in the parent view and set the size of the button
    btnMySkills.frame = newSize;
//    [btnMySkills setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMySkills addTarget:self action:@selector(buttonClickedMySkills:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMySkills];    
        
    UIButton *btnMyPlayground = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(499,386, 142,61+15); // position in the parent view and set the size of the button
    btnMyPlayground.frame = newSize;
//    [btnMyPlayground setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMyPlayground addTarget:self action:@selector(buttonClickedMyPlayground:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMyPlayground];  
    
    UIButton *btnMyHome = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(436,456+4, 124-22,95); // position in the parent view and set the size of the button
    btnMyHome.frame = newSize;
//    [btnMyHome setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMyHome addTarget:self action:@selector(buttonClickedMyHome:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMyHome];  
    
    UIButton *btnMyCamp = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(580-15+24-452+90, 240+24+154+115-42-6, 150, 70); // position in the parent view and set the size of the button
    btnMyCamp.frame = newSize;
//    [btnMyCamp setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMyCamp addTarget:self action:@selector(buttonClickedMyCamp:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMyCamp];
    
    UIButton *btnMyBookStore = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(580-15-192, 240+24+76, 50+5, 50+62); // position in the parent view and set the size of the button
    btnMyBookStore.frame = newSize;
//    [btnMyBookStore setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMyBookStore addTarget:self action:@selector(buttonClickedMyBookStore:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMyBookStore]; 
    
    UIButton *btnMyAchievements = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(346-26,278+20+15+15,95-22+5-25,20+14+10+24+47-25); // position in the parent view and set the size of the button
    btnMyAchievements.frame = newSize;
//    [btnMyAchievements setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMyAchievements addTarget:self action:@selector(buttonClickedMyAchievements:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMyAchievements];        
    
    UIButton *btnMyAchievements2 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(346-46+25,278+40+43+50,39,50); // position in the parent view and set the size of the button
    btnMyAchievements2.frame = newSize;
//    [btnMyAchievements2 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMyAchievements2 addTarget:self action:@selector(buttonClickedMyAchievements:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMyAchievements2];            
    
    UIButton *btnFarm = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(900, 100+60, 96+60, 205-70);
    btnFarm.frame = newSize;
//    [btnFarm setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnFarm addTarget:self action:@selector(buttonClickedFarm:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnFarm];
    
    UIButton *btnFarm2 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(780, 100+90, 96+25, 70);
    btnFarm2.frame = newSize;
//    [btnFarm2 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnFarm2 addTarget:self action:@selector(buttonClickedFarm:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnFarm2];
    
    UIButton *btnBakery = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(734-40, 657-50, 83, 113);
    btnBakery.frame = newSize;
//    [btnBakery setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnBakery addTarget:self action:@selector(buttonClickedBakery:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnBakery];
    
    UIButton *btnZoo = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(204-100, 397-50, 318-204+60  , 471-397+25);
    btnZoo.frame = newSize;
//    [btnZoo setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnZoo addTarget:self action:@selector(buttonClickedZoo:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnZoo];
    
    UIButton *btnOffice = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(292+295+20, 567-399-5, 94, 81+50);
    btnOffice.frame = newSize;
//    [btnOffice setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnOffice addTarget:self action:@selector(buttonClickedOffice:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnOffice];
    
    UIButton *btnCoffeeShop = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(888-30-459, 494-160+120-183-2, 48,48);
    btnCoffeeShop.frame = newSize;
//    [btnCoffeeShop setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnCoffeeShop addTarget:self action:@selector(buttonClickedCoffeeShop:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnCoffeeShop];    
    
    UIButton *btnBeach = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(0, 359, 64, 88);
    btnBeach.frame = newSize;
//    [btnBeach setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnBeach addTarget:self action:@selector(buttonClickedBeach:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnBeach];    
    
    UIButton *btnBeach2 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(62, 359-20, 40, 49+30);
    btnBeach2.frame = newSize;
//    [btnBeach2 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnBeach2 addTarget:self action:@selector(buttonClickedBeach:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnBeach2];
    UIButton *btnBeach3 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(62, 298+30, 67+15, 69);
    btnBeach3.frame = newSize;
//    [btnBeach3 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnBeach3 addTarget:self action:@selector(buttonClickedBeach:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnBeach3];
    
    
    UIButton *btnBeach4 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(257-130, 316-25, 128+25, 55+20);
    btnBeach4.frame = newSize;
//    [btnBeach4 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnBeach4 addTarget:self action:@selector(buttonClickedBeach:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnBeach4];
    
    
    UIButton *btnBeach5 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(260, 240, 50, 50);
    btnBeach5.frame = newSize;
//    [btnBeach5 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnBeach5 addTarget:self action:@selector(buttonClickedBeach:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnBeach5];
    
    UIButton *btnSky = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake( 150, 0, 870,164);
    btnSky.frame = newSize;
//    [btnSky setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnSky addTarget:self action:@selector(buttonClickedSky:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnSky];
    
    
    UIButton *btnSea = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake( 0,245,244,76);
    btnSea.frame = newSize;
//    [btnSea setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnSea addTarget:self action:@selector(buttonClickedSea:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnSea];
    
    
    UIButton *btnSea2 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(0,314,119,50);
    btnSea2.frame = newSize;
//    [btnSea2 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnSea2 addTarget:self action:@selector(buttonClickedSea:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnSea2];

    
    UIButton *btnMountain = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(0,171,200,71);
    btnMountain.frame = newSize;
//    [btnMountain setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMountain addTarget:self action:@selector(buttonClickedMountain:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMountain];
    
    UIButton *btnIceCreamParlour = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(265,288,58,60);
    btnIceCreamParlour.frame = newSize;
//    [btnIceCreamParlour setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnIceCreamParlour addTarget:self action:@selector(buttonClickedIceCreamParlour:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnIceCreamParlour];

    
    UIButton *btnRacingCourt = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(636,275,151,38+55);
    btnRacingCourt.frame = newSize;
//    [btnRacingCourt setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnRacingCourt addTarget:self action:@selector(buttonClickedracingCourt:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnRacingCourt];
    
    UIButton *btnRacingCourt2 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(717,202+20,77,47+43);
    btnRacingCourt2.frame = newSize;
//    [btnRacingCourt2 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnRacingCourt2 addTarget:self action:@selector(buttonClickedracingCourt:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnRacingCourt2];
    
    
    UIButton *btnMetro = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(407,220+14,150,32+10);
    btnMetro.frame = newSize;
//    [btnMetro setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMetro addTarget:self action:@selector(buttonClickedMetro:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMetro];
    
    UIButton *btnHotel = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(282+20,144+35-25,100,114);
    btnHotel.frame = newSize;
//    [btnHotel setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnHotel addTarget:self action:@selector(buttonClickedHotel:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnHotel];
    
    UIButton *btnHotel2 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(313,254-10,75+10,53+25);
    btnHotel2.frame = newSize;
//    [btnHotel2 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnHotel2 addTarget:self action:@selector(buttonClickedHotel:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnHotel2];
    
    
    
    UIButton *btnHospital = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(445-18,303,105+12,80);
    btnHospital.frame = newSize;
//    [btnHospital setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnHospital addTarget:self action:@selector(buttonClickedHospital:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnHospital];
    
    
    UIButton *btnStadium = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(827,259,179,49+63);
    btnStadium.frame = newSize;
//    [btnStadium setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnStadium addTarget:self action:@selector(buttonClickedStadium:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnStadium];
    
    UIButton *btnStadium2 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(777,274+10,57,85);
    btnStadium2.frame = newSize;
//    [btnStadium2 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnStadium2 addTarget:self action:@selector(buttonClickedStadium:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnStadium2];
    
    
    
/* 3rd Lane starting  */
        
    UIButton *btnRadioStation = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(798,354,83,95);
    btnRadioStation.frame = newSize;
//    [btnRadioStation setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnRadioStation addTarget:self action:@selector(buttonClickedRadioStation:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnRadioStation];
    
    
    UIButton *btnGlassHouse = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(910,353,55,95);
    btnGlassHouse.frame = newSize;
//    [btnGlassHouse setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnGlassHouse addTarget:self action:@selector(buttonClickedGlassHouse:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnGlassHouse];
    
    
    UIButton *btnSunHouse = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(976,362-10,42,45+14+50);
    btnSunHouse.frame = newSize;
//    [btnSunHouse setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnSunHouse addTarget:self action:@selector(buttonClickedSunHouse:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnSunHouse];
    
    
    UIButton *btnMall = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(657,342+30,132,88);
    btnMall.frame = newSize;
//    [btnMall setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMall addTarget:self action:@selector(buttonClickedMall:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMall];
    
 /* 2nd Lane starting  */   
    
    UIButton *btnChairHouse = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(26-12,570-114,80,90);
    btnChairHouse.frame = newSize;
//    [btnChairHouse setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnChairHouse addTarget:self action:@selector(buttonClickedChairHouse:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnChairHouse];
    
    
    UIButton *btnTeddyBear = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(110-15,508-90,107-20,120+15);
    btnTeddyBear.frame = newSize;
//    [btnTeddyBear setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnTeddyBear addTarget:self action:@selector(buttonClickedTeddyBear:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnTeddyBear];
    
    UIButton *btnPoliceStation = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(782-166,560-122+12,128-35,129-15);
    btnPoliceStation.frame = newSize;
//    [btnPoliceStation setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnPoliceStation addTarget:self action:@selector(buttonClickedPoliceStation:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnPoliceStation];
    
    UIButton *btnRescue911 = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(907-191,557-100+2,80,90);
    btnRescue911.frame = newSize;
//    [btnRescue911 setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnRescue911 addTarget:self action:@selector(buttonClickedRescue911:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnRescue911];
    
    UIButton *btnTCHE = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(841,478-45,97,120);
    btnTCHE.frame = newSize;
//    [btnTCHE setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnTCHE addTarget:self action:@selector(buttonClickedTCHE:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnTCHE];
    
    UIButton *btnMrPickHouse = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(935,439+35,92,97-18);
    btnMrPickHouse.frame = newSize;
//    [btnMrPickHouse setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMrPickHouse addTarget:self action:@selector(buttonClickedMrPickHouse:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMrPickHouse];
    
    
    
/* 1st Lane */    
    
    UIButton *btnUniversity = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(20,622-20,112,118);
    btnUniversity.frame = newSize;
//    [btnUniversity setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnUniversity addTarget:self action:@selector(buttonClickedUniversity:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnUniversity];
    
    UIButton *btnUniversityStreet = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(20+112,622-20+70,50,118-80+30);
    btnUniversityStreet.frame = newSize;
//    [btnUniversityStreet setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnUniversityStreet addTarget:self action:@selector(buttonClickedUniversityStreet:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnUniversityStreet];
    
    UIButton *btnSchool = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(277,560+20,140,162);
    btnSchool.frame = newSize;
//    [btnSchool setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnSchool addTarget:self action:@selector(buttonClickedSchool:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnSchool];
    
    UIButton *btnHostel = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(427,613,70,139);
    btnHostel.frame = newSize;
//    [btnHostel setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnHostel addTarget:self action:@selector(buttonClickedHostel:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnHostel];
    
    UIButton *btnEggHouse = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(511-15,610,77,123+15);
    btnEggHouse.frame = newSize;
//    [btnEggHouse setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnEggHouse addTarget:self action:@selector(buttonClickedEggHouse:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnEggHouse];
    


    UIButton *btnMacDonalds = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(790,587+25,97,179-48);
    btnMacDonalds.frame = newSize;
    //    [btnMacDonalds setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnMacDonalds addTarget:self action:@selector(buttonClickedMacdonalds:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnMacDonalds];
    
    
    
    UIButton *btnHuntedHouse = [UIButton buttonWithType:UIButtonTypeCustom];    
    newSize = CGRectMake(888,571+25,123,195-48);
    btnHuntedHouse.frame = newSize;
//    [btnHuntedHouse setImage:[UIImage imageNamed:@"MyStoreBackground.png"] forState:UIControlStateNormal];
    [btnHuntedHouse addTarget:self action:@selector(buttonClickedHuntedHouse:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:btnHuntedHouse];
    
    NSLog(@"Loading Completed");

  
//    [imageView release];
//    // set the tag for the image view
    [imageView setTag:ZOOM_VIEW_TAG];
//    
//    // add gesture            recognizers to the image view
//    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
//    
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];
    
//    [imageView addGestureRecognizer:singleTap];
    [imageView addGestureRecognizer:doubleTap];
    [imageView addGestureRecognizer:twoFingerTap];
   
    [doubleTap release];
    doubleTap = Nil;
    [twoFingerTap release];
    twoFingerTap = Nil;
    [imageView release];
}





-(void) buttonClickedFarm:(id) sender{
    
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }

    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }

    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:FARM forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
        
}

-(void) buttonClickedBakery:(id) sender{

    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:BAKERY forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedZoo:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        

    [[NSUserDefaults standardUserDefaults] setInteger:ZOO forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedOffice:(id) sender{

    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:OFFICE forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedCoffeeShop:(id) sender{

    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:COFFEE_SHOP forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedBeach:(id) sender{
    BOOL freeContentDownload = [[NSUserDefaults standardUserDefaults] boolForKey:@"freeContentDownloaded"];
    if (!freeContentDownload || freeContentDownload == FALSE ) {
        NSLog(@"Free Content Not Downloaded");
        UIAlertView *purchaseAlert; 
        int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        if (activeLanguage == 1) {
            purchaseAlert = [[UIAlertView alloc] initWithTitle:@"تحميل مجاني " message:@"عفوا , قم بتحميل الكلمات المجانية من الشاطيء للبدأ" delegate:self cancelButtonTitle:@"اذهب الى متجري " otherButtonTitles:@"خروج", nil];
        }
        else {
            purchaseAlert = [[UIAlertView alloc] initWithTitle:@"DOWNLOAD FREE PACKAGE" message:@"Please download free package from My Bookstore to unlock content for beach" delegate:self cancelButtonTitle:@"Go to My Bookstore" otherButtonTitles:@"Exit", nil]; 

        }
        [purchaseAlert show];
        [purchaseAlert release];
        purchaseAlert = Nil;
        return;
    }
    
    BOOL currentObjectUnloced = TRUE;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        

    [[NSUserDefaults standardUserDefaults] setInteger:BEACH forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

// STARTING POINT /////////////////////////

-(void) buttonClickedSky:(id) sender{

    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:SKY forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedMountains:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"free"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:MOUNTAINS forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedSea:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"free"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        

    [[NSUserDefaults standardUserDefaults] setInteger:SEA forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}


-(void) buttonClickedMountain:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }

    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"free"]) {
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        

    [[NSUserDefaults standardUserDefaults] setInteger:MOUNTAINS forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }

}

-(void) buttonClickedracingCourt:(id) sender{

    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:RACING_COURT forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedHospital:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        

    [[NSUserDefaults standardUserDefaults] setInteger:HOSPITAL forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedMetro:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    
    [[NSUserDefaults standardUserDefaults] setInteger:METRO forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedHotel:(id) sender{

    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:HOTEL forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedIceCreamParlour:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:ICECREAM_PARLOUR forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedStadium:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:STADIUM forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }

    
}


-(void) buttonClickedMall:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:MALL forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedRadioStation:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:RADIO_STATION forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedGlassHouse:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:GLASS_HOUSE forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedSunHouse:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:SUN_HOUSE forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
            [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedChairHouse:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        

    [[NSUserDefaults standardUserDefaults] setInteger:CHAIR_HOUSE forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self playSoundOfSelectedClassificaton];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }    
}

-(void) buttonClickedTeddyBear:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        

    [[NSUserDefaults standardUserDefaults] setInteger:TEDDY_BEAR forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedPoliceStation:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:POLICE_STATION forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedRescue911:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:RESCUE911 forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedTCHE:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:TCHE forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedMrPickHouse:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = FALSE;
    }
//    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
//    
//    if ([ActiveGrade isEqualToString:@"G4"]) {
//        currentObjectUnloced = FALSE;
//    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:MRPICK_HOUSE forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedUniversity:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:UNIVERSITY forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedUniversityStreet:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:UNIVERSITYSTREET forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedSchool:(id) sender{
    BOOL currentObjectUnloced = TRUE;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:SCHOOL forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedHostel:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:HOSTEL forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedEggHouse:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:EGG_HOUSE forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedMacdonalds:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:MACDONALDS forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedHuntedHouse:(id) sender{
    BOOL currentObjectUnloced = false;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    [[NSUserDefaults standardUserDefaults] setInteger:HUNTED_HOUSE forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CityPopupController *vc = [sb instantiateViewControllerWithIdentifier:@"CityPopUpControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
    
}

-(void) buttonClickedMySkills:(id) sender{
    BOOL currentObjectUnloced = TRUE;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MySkillsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MySkillsViewControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];     
    }
}

-(void) buttonClickedMyPlayground:(id) sender{
    BOOL currentObjectUnloced = TRUE;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
//            [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MyPlayGroundViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyPlaygroundIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];   
    }
}

-(void) buttonClickedMyHome:(id) sender{
    BOOL currentObjectUnloced = TRUE;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        
        if ([ActiveGrade isEqualToString:@"FREE"]) {
            [alertToPurchaseContent show];
        }
        
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MyHomeViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyHomeViewControllerIdentifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];  
    }
}

-(void) buttonClickedMyBookStore:(id) sender{
    return;
    BOOL currentObjectUnloced = TRUE;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//    MyStoreViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyBookStoreViewControllerIderntifier"];
//    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//    [self presentViewController:vc animated:YES completion:NULL]; 
    }
    
}

-(void) buttonClickedMyAchievements:(id) sender{
    BOOL currentObjectUnloced = TRUE;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MyAchievmentViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyAcheivementViewControllerIderntifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    }
  
}

-(void) buttonClickedMyCamp:(id) sender{
    BOOL currentObjectUnloced = TRUE;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] != 0) {
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"kg"]) {    
        currentObjectUnloced = FALSE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"G1"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mapToLoad"] isEqualToString:@"gx"]) {    
        currentObjectUnloced = TRUE;
    }
    
    if (currentObjectUnloced) {
        
//            [self playSoundOfSelectedClassificaton];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MyCampController *vc = [sb instantiateViewControllerWithIdentifier:@"MyCampControllerIderntifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];  
    }
}

-(void) playHelpMovie{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == NO) {
        return;
    }
    
    NSString *movieName = @"CityMap";

    if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
        return;    
    }
    
    MovieViewController* moviePlayer = [[MovieViewController alloc] init];
    [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayer animated:NO];
    // Prep and play the movie

    [moviePlayer playMovie:movieName];
    
}

-(void) viewDidAppear:(BOOL)animated{
    if (!showMovie) {
        [self playHelpMovie];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    showMovie = TRUE;
}

- (void)dealloc {
//    [BtnFarm release];
    /*
    [imageScrollView release];
    [imageView release];

    */
    [backgroundImg release];
    [super dealloc];
}

- (void)viewDidUnload {
    //    [self setBtnFarm:nil];
    //    btnMall = nil;
    imageView = nil;
    imageScrollView = nil;
    
    self.imageScrollView = nil;
    self.imageView = nil;
    
    [self setBackgroundImg:nil];
    [super viewDidUnload];
}
//#pragma mark UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return [imageScrollView viewWithTag:ZOOM_VIEW_TAG];
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
    [scrollView setZoomScale:scale+0.01 animated:NO];
    [scrollView setZoomScale:scale animated:NO];
}

//#pragma mark TapDetectingImageViewDelegate methods

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    // single tap does nothing for now
    // We Will Load the POPUP menu here 
    // Get the location within the image view where we tapped
    // CGPoint pointInView = [gestureRecognizer locationInView:self.imageView]; 
//
    
    // Makes scrollView in single Tap WAIT I
    float newScale = [imageScrollView zoomScale] / (ZOOM_STEP * zoomTime);
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [imageScrollView zoomToRect:zoomRect animated:NO];
     // */
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    // double tap zooms in
    // Always zoom out on double Tap
    // /* WAIT III
    float newScale = [imageScrollView zoomScale] * ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [imageScrollView zoomToRect:zoomRect animated:NO];
    zoomTime++;
    // */
    
    
    /* WAIT II
    // we want to resize our button also 
    CGRect rect = btnMall.frame;
    rect.size.height = rect.size.height * ZOOM_STEP ;
    rect.size.width = rect.size.width * ZOOM_STEP;
    btnMall.frame = rect;
    btnMall.tag = 0;	
    
    CGRect rectTst = BtnFarm.frame;
    rectTst.size.height = rectTst.size.height * ZOOM_STEP ;
    rect.size.width = rectTst.size.width * ZOOM_STEP;
    BtnFarm.frame = rectTst;
    BtnFarm.tag = 0;	
    */
    
     
    
}

- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out
    float newScale = [imageScrollView zoomScale] / ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [imageScrollView zoomToRect:zoomRect animated:NO];
    
//    [imageScrollView addSubview:BtnFarm];
    
    
    /* WAIT IV
    // we want to resize our button also 
    CGRect rect = btnMall.frame;
    rect.size.height = rect.size.height / ZOOM_STEP ;
    rect.size.width = rect.size.width / ZOOM_STEP;
    btnMall.frame = rect;
    btnMall.tag = 0;	
    */
    
    if(zoomTime > 0)
        zoomTime--;
}

//#pragma mark Utility methods

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    
    CGRect zoomRect;    
    // the zoom rect is in the content view's coordinates. 
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [imageScrollView frame].size.height / scale;
    zoomRect.size.width  = [imageScrollView frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    return zoomRect;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{
    // get touch event
//    UITouch *touch = [[event allTouches] anyObject];
//    CGPoint touchLocation = [touch locationInView:self.view];
    
   // if ([touch view] == PopupImage) {
        // move the image view
//        PopupImage.hidden=NO;
//        PopupImage.center = touchLocation;
    //}
    //else  
    //    PopupImage.hidden=NO;
        
    
    
}
- (void) zoomSubView{
    
}

-(void) playSoundOfSelectedClassificaton{

    MrPDatabase *database = [[MrPDatabase alloc] init];
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    NSArray *MrPContentArray;
    if (activeLanguage == 1) { // arabic
        MrPContentArray = [database ArabicWordDatabaseArray:cId];
    }
    else { // english
        MrPContentArray = [database EnglishWordDatabaseArray:cId];
    }
    
    [database release];
    database = Nil;
    
        NSMutableDictionary* soundDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    int position = 0;
    for (MrPContentInfo *info in MrPContentArray) {
        [soundDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"soundDic%d",position]];          
        position++;            
    
    } 
    
    NSString* soundName = [soundDic objectForKey:[NSString stringWithFormat:@"soundDic%d",0]];  
    [soundDic removeAllObjects];
    [soundDic release];
    
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    
    NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
    SystemSoundID squishSoundID;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
    AudioServicesPlaySystemSound(squishSoundID);
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//        MyStoreViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyBookStoreViewControllerIderntifier"];
//        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//        [self presentViewController:vc animated:NO completion:NULL];    
        
    }
    if (buttonIndex == 1)
    {
//        [self dismissModalViewControllerAnimated:YES];
    }           
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}
@end
