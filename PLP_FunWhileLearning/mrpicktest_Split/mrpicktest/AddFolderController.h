//
//  AddFolderController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/24/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddFolderController : UIViewController
//@property (weak, nonatomic) IBOutlet UITextField *txtFolder;
//@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
//@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property (strong, nonatomic) IBOutlet UITextField *txtFolder;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;
//@property (strong, nonatomic) IBOutlet UIButton *btnCancel;

- (IBAction)CancelAddFolder:(id)sender;
- (IBAction)AddFolder:(id)sender;

@end
