//
//  Constants.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 24/07/2014.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import "Constants.h"



@implementation Constants

+(NSMutableString*) createQueryString:(NSArray*)pArray{
    NSMutableString* classificationsStr;
    if([pArray isKindOfClass:[NSString class]] == YES)
    {
        classificationsStr = [NSMutableString string];
        [classificationsStr appendString:[NSString stringWithFormat:@"(classification_id = %@)",pArray]];
    }
    else{
        classificationsStr = [NSMutableString string];
        for (int k = 0; k < [pArray count]; k++) {
            [classificationsStr appendString:[NSString stringWithFormat:@"(classification_id = %@) ||",[NSString stringWithFormat:@"%@",[pArray objectAtIndex:k]]] ];
        }
    
        if ([classificationsStr length] > 0) {
        [classificationsStr setString:[classificationsStr substringToIndex:[classificationsStr length]-2]];
        }
        

    }
    NSLog(@"string is %@",classificationsStr);
    return classificationsStr;
}

+(NSMutableString*) createPrincipleIdString:(NSArray*)pArray{
    
    NSMutableString* classificationsStr;
    if([pArray isKindOfClass:[NSString class]] == YES)
    {
        classificationsStr = [NSMutableString string];
        [classificationsStr appendString:[NSString stringWithFormat:@"(Principle_id = %@)",pArray]];
    }
    else{
        classificationsStr = [NSMutableString string];
        for (int k = 0; k < [pArray count]; k++) {
            [classificationsStr appendString:[NSString stringWithFormat:@"(Principle_id = %@) ||",[NSString stringWithFormat:@"%@",[pArray objectAtIndex:k]]] ];
        }
        
        [classificationsStr setString:[classificationsStr substringToIndex:[classificationsStr length]-2]];
        
    }
    NSLog(@"string is %@",classificationsStr);
    return classificationsStr;

}

+(NSMutableString*) createGrammarIdString:(NSArray*)pArray{
    
    NSMutableString* classificationsStr;
    if([pArray isKindOfClass:[NSString class]] == YES)
    {
        classificationsStr = [NSMutableString string];
        [classificationsStr appendString:[NSString stringWithFormat:@"(grammar_id = %@)",pArray]];
    }
    else{
        classificationsStr = [NSMutableString string];
        for (int k = 0; k < [pArray count]; k++) {
            [classificationsStr appendString:[NSString stringWithFormat:@"(grammar_id = %@) ||",[NSString stringWithFormat:@"%@",[pArray objectAtIndex:k]]] ];
        }
        
        [classificationsStr setString:[classificationsStr substringToIndex:[classificationsStr length]-2]];
        
    }
    NSLog(@"string is %@",classificationsStr);
    return classificationsStr;

}



+(NSMutableString*) createNotEqualToGrammarIdString:(NSArray*)pArray withGIDIncluded:(BOOL)pGIdIncluded{
    NSMutableString* classificationsStr;
    NSString *operatorValue;
    if (pGIdIncluded == true) {
        operatorValue = @"==";
    }
    else {
        operatorValue = @"!=";
    }
    if([pArray isKindOfClass:[NSString class]] == YES)
    {
        classificationsStr = [NSMutableString string];
        [classificationsStr appendString:[NSString stringWithFormat:@"(grammar_id %@ %@)",operatorValue,pArray]];
    }
    else{
        classificationsStr = [NSMutableString string];
        for (int k = 0; k < [pArray count]; k++) {
            [classificationsStr appendString:[NSString stringWithFormat:@"(grammar_id %@ %@) ||",operatorValue,[NSString stringWithFormat:@"%@",[pArray objectAtIndex:k]]] ];
        }
        
        [classificationsStr setString:[classificationsStr substringToIndex:[classificationsStr length]-2]];
        
    }
    NSLog(@"string is %@",classificationsStr);
    return classificationsStr;
    
}

@end
