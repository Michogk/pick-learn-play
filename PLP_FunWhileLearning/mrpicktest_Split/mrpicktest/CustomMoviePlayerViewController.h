//
//  CustomMoviePlayerViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 12/18/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface CustomMoviePlayerViewController : UIViewController 
{
    MPMoviePlayerController *mp;
    NSURL 									*movieURL;
}

- (id)initWithPath:(NSString *)moviePath;
- (void)readyPlayer:(NSString*) movieName;

@end
