//
//  BookStoreTableViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/15/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "BookStoreTableViewController.h"
#import "BookStoreCells.h"

@interface BookStoreTableViewController ()

@end

@implementation BookStoreTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    numberOfPackages = 0;
//    [self populateCell];

        
    [self.tableView setShowsHorizontalScrollIndicator:NO];
    [self.tableView setShowsVerticalScrollIndicator:NO];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d",indexPath.row];
    
    [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"BookStoreCellNumber"];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        cell = [[[BookStoreCells alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    tableView.separatorStyle = UITableViewCellSelectionStyleGray;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 258;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    
//    return 25;
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 25;
//}
-(void) BackFromPurchaseVC{
    int indexPath = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedCell"];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    self.tableView.scrollEnabled = YES;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self.tableView reloadData];
    
    
//    NSLog(@"Cell Clicked %d",indexPath.row);
//    
//    [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"selectedCell"];
//    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(BackFromPurchaseVC) name:@"backBtnPressed" object:nil];
//    
//    PurchasePackageViewController *detailViewController = [[PurchasePackageViewController alloc] init];
//    [self.view addSubview:detailViewController.view];
//    self.tableView.scrollEnabled = NO;
//    

     
}

@end
