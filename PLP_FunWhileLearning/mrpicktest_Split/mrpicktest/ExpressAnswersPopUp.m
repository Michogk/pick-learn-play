//
//  ExpressAnswersPopUp.m
//  GMG App
//
//  Created by Mosib on 4/27/15.
//  Copyright (c) 2015 Advansoft. All rights reserved.
//

#import "ExpressAnswersPopUp.h"
#import "AnimationUtility.h"

@implementation ExpressAnswersPopUp
{
    UIImagePickerController *imagePickerController;
}

+ (ExpressAnswersPopUp*) mainView {
    ExpressAnswersPopUp *popUp = [Common loadNibName:@"ExpressAnswersPopUp" owner:self options:nil];
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = popUp.imgViewBackground.bounds;
    [popUp.imgViewBackground addSubview:visualEffectView];
    return popUp;
}

#pragma mark -
#pragma mark Public Methods

- (void) showOnView:(UIView*)view withDelegate:(id<ExpressAnswersPopUpDelegate>)delegate {
        _textViewAnswer.delegate = self;
    [self ReadTextFile];
    _delegate = delegate;
    _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [view addSubview:self];
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                _viewPopUp.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
}

- (void) showOnView:(UIView*)view {

    [self showOnView:view withDelegate:nil];
}

- (void) hide {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
    [_textViewAnswer resignFirstResponder];

    [_viewPopUp removeFromSuperview];
}


#pragma mark -
#pragma mark Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
}

#pragma mark -
#pragma mark TextField Delegates
- (void) textViewDidBeginEditing:(UITextView *) textView {
    CGRect frame = _viewPopUp.frame;
    frame.origin.y = frame.origin.y - 130;
    [AnimationUtility moveFrame:_viewPopUp withFrame:frame withDuration:0.3];
}

- (void) textViewDidEndEditing:(UITextView *) textView {
    CGRect frame = _viewPopUp.frame;
    frame.origin.y = frame.origin.y + 130;
    [AnimationUtility moveFrame:_viewPopUp withFrame:frame withDuration:0.3];
}
//
//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    CGRect frame = _viewPopUp.frame;
//    frame.origin.y = frame.origin.y - 130;
//    [AnimationUtility moveFrame:_viewPopUp withFrame:frame withDuration:0.3];
//    return YES;
//}
//
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    CGRect frame = _viewPopUp.frame;
//    frame.origin.y = frame.origin.y + 130;
//    [AnimationUtility moveFrame:_viewPopUp withFrame:frame withDuration:0.3];
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
//    return YES;
//}
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    
//    
//    return YES;
//}

#pragma mark -
#pragma mark Actions

- (IBAction) actionSubmit:(id)sender {
    [self createAndStoreFile];
    
    [self hide];
    if ([_delegate respondsToSelector:@selector(RightsViewSubmitted:)]) {
        [_delegate  RightsViewSubmitted:self];
    }
}

- (IBAction)actionCancel:(id)sender {
        [self createAndStoreFile];
    [self hide];
//    if ([_delegate respondsToSelector:@selector(RightsViewSubmitted:)]) {
//        [_delegate  RightsViewSubmitted:self];
//    }
}

#pragma mark Storing
-(void) createAndStoreFile{
    
    NSString *dataPath = [self getDataPath];
    NSError *error;
    BOOL isDir = NO;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath  isDirectory:&isDir]){
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error];
        NSLog(@"Trying to create doc directory");
    }
    if (!isDir)
    {
        NSLog(@"Cannot proceed!");

    }

    NSString *fileAtPath = [dataPath stringByAppendingString:[NSString stringWithFormat:@"/%@.txt",_fileName]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:fileAtPath]) {
        [[NSFileManager defaultManager] createFileAtPath:fileAtPath contents:nil attributes:nil];
    }
    [[_textViewAnswer.text dataUsingEncoding:NSUTF8StringEncoding] writeToFile:fileAtPath atomically:NO];
}

-(void) ReadTextFile{
    NSString *dataPath = [self getDataPath];
    NSError *error;
    BOOL isDir = NO;
    NSString *fileAtPath = [NSString stringWithFormat:@"%@/%@.txt",dataPath,_fileName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath  isDirectory:&isDir]){
        NSString *content = [NSString stringWithContentsOfFile:fileAtPath
                                                      encoding:NSUTF8StringEncoding error:NULL];
        _textViewAnswer.text = content;

    }
    if (!isDir)
    {
        _textViewAnswer.text = @"";
    }
    


}

-(NSString*) getDataPath{
    NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
    NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString *folderNAme = [[NSUserDefaults standardUserDefaults] objectForKey:@"folderName"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    if (!pinNumber || pinNumber == NULL) {
        pinNumber = @"1";
    }
    if ((!folderNAme ) || folderNAme == NULL) {
        folderNAme = @"Express It";
    }
    
    // Folder to save
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@-%@",activeGrade,pinNumber,pinNumber,folderNAme]];

    return path;
}
@end
