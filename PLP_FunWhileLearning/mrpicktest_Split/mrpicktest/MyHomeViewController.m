//
//  MyHomeViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/21/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//



#import "MyHomeViewController.h"
#import "MrPContentInfo.h"
#import "MrPDatabase.h"
#import "AddFolderController.h"
#import "SimpleAudioEngine.h"
#import "DocumentDirectory.h"
#import "MovieViewController.h"
#import "XMLParser.h"
#import "BackendUpdateCheck.h"
#import "MyCampController.h"
#import "MyPlayGroundViewController.h"
#import "BookViewController.h"
#import "UIImage+animatedGIF.h"
#import "MyHomeCell.h"
#import "WordsCell.h"
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#define FOLDER_TABLE_TAG 20
#define WORDS_TABLE_TAG 30

#define MY_CAMP_ACTION_BTN_TAG 200
#define PLAYGROUND_ACTION_BTN_TAG 300

#define BOOKS_SCROLLVIEW_WIDTH_UNSELECTED_iPad 71
#define BOOKS_SCROLLVIEW_WIDTH_SELECTED_iPad 171

#define TICK_TAG_BUFFER 19

@implementation MyHomeViewController
@synthesize btnAddWord = _btnAddWord;
@synthesize btnHearWordSound = _btnHearWordSound;
@synthesize BtnDeleteWord = _BtnDeleteWord;
@synthesize BtnAddFolder = _BtnAddFolder;
@synthesize FoldersTable = _FoldersTable;
@synthesize WordsTable = _WordsTable;
@synthesize BtnDeleteFolder = _BtnDeleteFolder;
@synthesize btnRefreshDataFromServer = _btnRefreshDataFromServer;
@synthesize selectedIndex;
static MyHomeViewController *homeViewController;
static int _selectedFolderClassification;

+ (MyHomeViewController*)homeViewController {
    if (homeViewController == nil) {
        homeViewController = [[MyHomeViewController alloc] initWithNibName:@"MainStoryboard" bundle:nil];
        
    }
    return homeViewController;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}



- (void)viewDidLoad
{
    Position = 0;

    
    [super viewDidLoad];
    _FoldersTable.bounces=NO;
    _FoldersTable.scrollEnabled=YES;
    
    foldersCellBackgroundImageArray = [[NSArray alloc] initWithObjects:@"L1",@"L2",@"L3",@"L4",@"L5",@"L6",
                                       @"L1",@"L2",@"L3",@"L4",@"L5",@"L6",
                                       @"L1",@"L2",@"L3",@"L4",@"L5",@"L6",
                                       @"L1",@"L2",@"L3",@"L4",@"L5",@"L6",nil];
    
    wordssCellBackgroundImageArray = [[NSArray alloc] initWithObjects:@"R1",@"R2",@"R3",@"R4",@"R5",@"R6",
                                       @"R7",@"R1",@"R2",@"R3",@"R4",@"R5",
                                       @"R6",@"R7",@"R1",@"R2",@"R3",@"R4",@"R5",@"R6",@"R7",nil];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"feather" withExtension:@"gif"];
    imgView1Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    _FoldersTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_FoldersTable setDataSource:self];
    [_WordsTable setDataSource:self];
    
    [_FoldersTable setDelegate:self];
    [_WordsTable setDelegate:self];
    
    _FoldersTable.tag = FOLDER_TABLE_TAG;
    _WordsTable.tag = WORDS_TABLE_TAG;

    [self loadMyHomeData];
    [self loadBooks];
    booksSelected = FALSE;
//    [self playHelpMovie];
    
    imgView1Feather.hidden = YES;
    NSArray *classificationIdArray = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    
    if (classificationIdArray) {
        if (![classificationIdArray isKindOfClass:[NSString class]]) {
            if ([classificationIdArray count] == 1) {
                if ([[classificationIdArray objectAtIndex:0] intValue] == 510) {
                    [self showFeather];
                }
            }

        }
        
    }
}




-(void) showFeather{
    imgView1Feather.hidden = NO;
    [self.view bringSubviewToFront:imgView1Feather];
    [self performSelector:@selector(hideFeather) withObject:nil afterDelay:5.0];
}

-(void) hideFeather{
    imgView1Feather.hidden = YES;
    [imgView1Feather removeFromSuperview];
}

-(void) drawBookScrollView:(int) pBookScrollViewWidth{
    
    numberOfBooks = [retainArrTagsOfBooks count];
    int yPos = 25;
    for (int i = 0; i < numberOfBooks; i++) {
        UIButton *btn = [[UIButton alloc] init];
        btn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
        btn.frame = CGRectMake(0, yPos, pBookScrollViewWidth, 80);
        [btn setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"S%@",[retainArrTagsOfBooks objectAtIndex:i]]]
                       forState:UIControlStateNormal];
        btn.tag = [[retainArrTagsOfBooks objectAtIndex:i] intValue];
        [btn addTarget:self action:@selector(BookTapped:) forControlEvents:UIControlEventTouchUpInside];
        [booksScrollView addSubview:btn];
        yPos = yPos + 100;
    }
    

}

-(void) loadBooks{
    MrPDatabase *database = [[MrPDatabase alloc] init];

    NSArray *booksInfoArray = [database ReadBooksDatabase];

    bookFoldersContentDic = [[NSMutableDictionary alloc] initWithCapacity:9];
    bookWordsContentDic = [[NSMutableDictionary alloc] initWithCapacity:20];
    booksContentDic = [[NSMutableDictionary alloc] initWithCapacity:20];
    NSMutableArray* arrTagsOfBooks = [[NSMutableArray alloc] init];
    for (MrPContentInfo *info in booksInfoArray) {
        [booksContentDic setObject:info.foldername forKey:[NSString stringWithFormat:@"%d",info.folderid]];
        if (![arrTagsOfBooks containsObject:[NSString stringWithFormat:@"%d",info.folderid]]){
            [arrTagsOfBooks addObject:[NSString stringWithFormat:@"%d",info.folderid ]];
        }
    }

    if (retainArrTagsOfBooks) {
        [retainArrTagsOfBooks release];
        retainArrTagsOfBooks = nil;
    }
    retainArrTagsOfBooks = [[NSMutableArray alloc] init];
    for (int k = 0; k < [arrTagsOfBooks count]; k++) {
        [retainArrTagsOfBooks addObject:[arrTagsOfBooks objectAtIndex:k]];
    }
    
    [self drawBookScrollView:BOOKS_SCROLLVIEW_WIDTH_UNSELECTED_iPad];
    
    
    if (arrTagsOfBooks) {
        [arrTagsOfBooks release];
        arrTagsOfBooks = nil;
    }
    
    booksScrollView.backgroundColor = [UIColor clearColor];
    booksScrollView.contentSize = CGSizeMake(booksScrollView.frame.size.width, numberOfBooks * 100);
    
    
    // Reading all Words
//    int counter = 0;
//    NSArray *WordsInfoArray = [database ReadBooksWordsDatabase:400];
//    for (MrPContentInfo *info in WordsInfoArray) {
//        [bookWordsContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"%d",counter]];
//        [bookWordsContentDic setObject:[NSString stringWithFormat:@"%d",info.wordId ] forKey:[NSString stringWithFormat:@"wordId%d",counter]];
//        [bookWordsContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",counter]];
//        [bookWordsContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",counter]];
//        counter = counter  + 1;
//    }

    database = nil;
}

-(void) disableExtraButtons{
    _BtnAddFolder.enabled = NO;
    _BtnDeleteWord.enabled = NO;
    _BtnAddFolder.enabled = NO;
    _BtnDeleteFolder.enabled = NO;
    _btnRefreshDataFromServer.enabled = NO;
    
    booksScrollView.frame = CGRectMake(booksScrollView.frame.origin.x, booksScrollView.frame.origin.y, BOOKS_SCROLLVIEW_WIDTH_SELECTED_iPad, booksScrollView.frame.size.height);
        [self drawBookScrollView:BOOKS_SCROLLVIEW_WIDTH_SELECTED_iPad];

}

-(void) enableExtraButtons{
    _BtnAddFolder.enabled = YES;
    _BtnDeleteWord.enabled = YES;
    _BtnAddFolder.enabled = YES;
    _BtnDeleteFolder.enabled = YES;
    _btnRefreshDataFromServer.enabled = YES;
    
    booksScrollView.frame = CGRectMake(booksScrollView.frame.origin.x, booksScrollView.frame.origin.y, BOOKS_SCROLLVIEW_WIDTH_UNSELECTED_iPad, booksScrollView.frame.size.height);
    [self drawBookScrollView:BOOKS_SCROLLVIEW_WIDTH_UNSELECTED_iPad];

}

-(void) BookTapped:(id) sender{
    
    booksSelected = TRUE;
    [self disableExtraButtons];
    

    self.backgroundImageView.image = [UIImage imageNamed:@"Myhome_words-books.png"];
//    [self.FoldersTable reloadData];
    bookQuestionsView = [SelectBookQuestions createView];
    [bookQuestionsView.btnQuestions addTarget:self action:@selector(loadQuestions) forControlEvents:UIControlEventTouchUpInside];
    [bookQuestionsView.btnReadBook addTarget:self action:@selector(readBook) forControlEvents:UIControlEventTouchUpInside];
    [bookQuestionsView.btnClose addTarget:self action:@selector(closeBookPopup) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bookQuestionsView];
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    
    // Load Default Word

    _selectedFolderClassification = [sender tag];
    loadingClassification = _selectedFolderClassification;
    [self playBookSound:_selectedFolderClassification];
    [[NSUserDefaults standardUserDefaults] setInteger:_selectedFolderClassification forKey:@"selectedBookClassification"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSUserDefaults standardUserDefaults] setInteger:_selectedFolderClassification forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    MrPDatabase *database = [[MrPDatabase alloc] init];
    NSArray *booksInfoArray = [database ReadBookFoldersDatabaseWithClassification:_selectedFolderClassification];
    if ([bookFoldersContentDic count] > 1) {
        [bookFoldersContentDic removeAllObjects];
    }
    
    for (MrPContentInfo *info in booksInfoArray) {
            [bookFoldersContentDic setObject:info.foldername forKey:[NSString stringWithFormat:@"%d",info.folderid]];
    }
    
    numberOfFoldersInBook = [booksInfoArray count];
    
        if (wordsContentDic) {
            [wordsContentDic removeAllObjects];
        }
//        MrPDatabase *database = [[MrPDatabase alloc] init];
        NSArray *WordsInfoArray;
        WordsInfoArray = [database ReadBooksWordsDatabase:_selectedFolderClassification+1];
        database = Nil;
        int counter = 0;
        for (MrPContentInfo *info in WordsInfoArray) {
            [wordsContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"%d",counter]];
            [wordsContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",counter]];
            [wordsContentDic setObject:[NSString stringWithFormat:@"%d",info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",counter]];
            [wordsContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",counter]];
            counter = counter  + 1;
        }
    
    [self.WordsTable reloadData];
     [self.FoldersTable reloadData];

}

-(void) playBookSound:(int) classificationOfBook{
    
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    NSString *fileName = [NSString stringWithFormat:@"Book%d",classificationOfBook];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString *activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString* soundFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.mp3",activeGrade,fileName]];
    NSLog(@"soundfile path is %@",soundFilePath);
    
    if (soundFilePath == nil || !soundFilePath) {
        NSLog(@"Sound NOt found");
        fileName = @"EmptySound";
        soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        player.volume = 0.5;
        player.numberOfLoops = 0;
        player.delegate = self;
        [player play];
        
        return;
    }
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];

}
-(void) closeBookPopup{
    [bookQuestionsView removeFromSuperview];
    bookQuestionsView = nil;
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
}

-(void) dataAvailableForUpdateAtBackend{
    
    UIAlertView *alertUpdateAtBackEnd = [[UIAlertView alloc] init];
    [alertUpdateAtBackEnd setTitle:@"Data Update Available"];
    [alertUpdateAtBackEnd setMessage:@"Data is available at the backend. Are you sure you want to update Data in my home? This may take several minutes to download data."];
    [alertUpdateAtBackEnd setDelegate:self];
    [alertUpdateAtBackEnd addButtonWithTitle:@"YES"];
    [alertUpdateAtBackEnd addButtonWithTitle:@"NO"];
    [alertUpdateAtBackEnd setTag:55];
    [alertUpdateAtBackEnd show];
    [alertUpdateAtBackEnd release];
}

-(void) removeOlderData{
    NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
    NSString *activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    
    // Remove Zip File
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString * dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",activeGrade, pinNumber]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:dataPath])
    {
        [fileManager removeItemAtPath:dataPath error:NULL];
    }
}


-(void) UpDateBackendDataByTeacher{
    [self removeOlderData];
    NSLog(@"Synchornizing Backend Data");
            
    // screen waiting stuff
    [self characterWalkingAnimation];
    UIView *synchronizingProcessWaitingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    [synchronizingProcessWaitingView setBackgroundColor:[UIColor clearColor]];
    [synchronizingProcessWaitingView setTag:100];
    [self.view addSubview:synchronizingProcessWaitingView];
    
    
    UILabel *lblWaitPlease = [[UILabel alloc] initWithFrame:CGRectMake(160,20,800,200)];
    lblWaitPlease.textAlignment = UITextAlignmentCenter;
    [lblWaitPlease setBackgroundColor:[UIColor clearColor]];
    UIFont *font = [UIFont systemFontOfSize: 30];
    [lblWaitPlease setFont:font];
    lblWaitPlease.textColor = [UIColor blackColor];
    lblWaitPlease.lineBreakMode = UILineBreakModeWordWrap;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if(activeLanguage == 2){
        lblWaitPlease.text = @"Please Wait.. Synchronizing Data from Backend....";
    }
    else{
        lblWaitPlease.text = @"قيد التحميل";
    }
    [synchronizingProcessWaitingView addSubview:lblWaitPlease];
    [lblWaitPlease release];
    
    UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    av.frame = CGRectMake(500,352,50,50);
    av.tag  = 102;
    [synchronizingProcessWaitingView addSubview:av];
    [av startAnimating];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Reload) name:@"Reload" object:nil];
    
    // Starting Synchronizing Process
    

    
    XMLParser *parser = [[XMLParser alloc] init];
      [parser parseXMLFileAtURL:[NSString stringWithFormat:@"http://mrpickit.net/admin/getdata.aspx?pin=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"]]];
}

-(void) characterWalkingAnimation{
    
    if (characterWalkingAnimationImgView) {        
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
        
        
    }
    characterAnimationStep = 0;
    animationImageNumber = 1;
    animationImageDirection = @"R";
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        NSString* animationImg = [NSString stringWithFormat:@"Walking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        NSString* animationImg = [NSString stringWithFormat:@"GWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        NSString* animationImg = [NSString stringWithFormat:@"BBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        NSString* animationImg = [NSString stringWithFormat:@"BGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        NSString* animationImg = [NSString stringWithFormat:@"DBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        NSString* animationImg = [NSString stringWithFormat:@"DGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        NSString* animationImg = [NSString stringWithFormat:@"CBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        NSString* animationImg = [NSString stringWithFormat:@"CGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FireBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FireGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        NSString* animationImg = [NSString stringWithFormat:@"ABWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        NSString* animationImg = [NSString stringWithFormat:@"AGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        NSString* animationImg = [NSString stringWithFormat:@"PBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        NSString* animationImg = [NSString stringWithFormat:@"PGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    [characterWalkingAnimationImgView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:characterWalkingAnimationImgView];
    [self.view bringSubviewToFront:characterWalkingAnimationImgView];
    //    [characterWalkingAnimationImgView bringSubviewToFront:characterBtn];
    [characterWalkingAnimationImgView setUserInteractionEnabled:YES];
    
    
    
    characterWalkingTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1
                                                              target:self
                                                            selector:@selector(updateCharacterStep)
                                                            userInfo:nil
                                                             repeats:YES]retain];
    
    
}

-(void) updateCharacterStep{
    
    animationImageNumber = animationImageNumber + 1;
    if (animationImageNumber == 8) {
        animationImageNumber = 1;
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        animationImageGender = @"";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        animationImageGender = @"G";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        animationImageGender = @"BB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        animationImageGender = @"BG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        animationImageGender = @"DB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        animationImageGender = @"DG";
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        animationImageGender = @"CB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        animationImageGender = @"CG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        animationImageGender = @"FireB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        animationImageGender = @"FireG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        animationImageGender = @"FB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        animationImageGender = @"FG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        animationImageGender = @"AB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        animationImageGender = @"AG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        animationImageGender = @"PB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        animationImageGender = @"PG";
    }

    NSString* animationImg = [NSString stringWithFormat:@"%@Walking_%@%d.png",animationImageGender,animationImageDirection,animationImageNumber];
    [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    
    if ([animationImageDirection isEqualToString:@"R"]) {
        characterAnimationStep = characterAnimationStep + 8;
    }
    else {
        characterAnimationStep = characterAnimationStep - 8;
    }
    
    CGRect frame = characterWalkingAnimationImgView.frame;
    frame.origin.x =  characterAnimationStep;
    characterWalkingAnimationImgView.frame = frame;
    
    if (characterAnimationStep > 900) {
        animationImageDirection = @"L";
    }
    if (characterAnimationStep < 20) {
        animationImageDirection = @"R";
    }
    
}

-(void) loadMyHomeData{

    if(characterWalkingTimer){
        [characterWalkingTimer invalidate];
        [characterWalkingTimer release];
        characterWalkingTimer = nil;
    }
    
    if (characterWalkingAnimationImgView) {
        [characterWalkingAnimationImgView removeFromSuperview];
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
    }
    
    
    
    if ([self.view viewWithTag:100]) {
        if([[self.view viewWithTag:100] viewWithTag:102]){
            [[[self.view viewWithTag:100] viewWithTag:102] removeFromSuperview];
            
        }
        [[self.view viewWithTag:100] removeFromSuperview];
    }
    
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    
    // Loading Folders Data
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    NSArray *foldersInfoArray = [database ReadFolderDatabase];
    
    database = Nil;
    foldersContentDic = [[NSMutableDictionary alloc] initWithCapacity:9];
    wordsContentDic = [[NSMutableDictionary alloc] initWithCapacity:50];
    for (MrPContentInfo *info in foldersInfoArray) {
        [foldersContentDic setObject:info.foldername forKey:[NSString stringWithFormat:@"%d",info.folderid]];
    }
    
    
    
    _missingClassificationCounter = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Reload) name:@"Reload" object:nil];
    
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if ([ActiveGrade isEqualToString:@"FREE"]) {
        [_BtnDeleteFolder setEnabled:NO];
        [_BtnAddFolder setEnabled:NO];
        [_btnAddWord setEnabled:NO];
        [_BtnDeleteWord setEnabled:NO];
        [_btnHearWordSound setEnabled:NO];
        [self freePackage];
        return;
    }
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.FoldersTable reloadData];

    
}

-(void) viewDIdAppear{

}

- (void)Reload {
    
    // Reload Folder Data
    MrPDatabase *database = [[MrPDatabase alloc] init];
    NSArray *foldersInfoArray = [database ReadFolderDatabase];
    
    for (MrPContentInfo *info in foldersInfoArray) {
        [foldersContentDic setObject:info.foldername forKey:[NSString stringWithFormat:@"%d",info.folderid]];
    }
    [self.FoldersTable reloadData];
    NSLog(@"Reload completed");
    // Reload Words Data
    NSArray *WordsInfoArray = [database ReadWordDatabase:_selectedFolderClassification];    
    [database release];
//    database = Nil;
    int counter = 0;
    if (wordsContentDic) {
        [wordsContentDic removeAllObjects];
    }

    for (MrPContentInfo *info in WordsInfoArray) {
        [wordsContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"%d",counter]];
        [wordsContentDic setObject:[NSString stringWithFormat:@"%d",info.wordId ] forKey:[NSString stringWithFormat:@"wordId%d",counter]];
        [wordsContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",counter]];
        [wordsContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",counter]];
        counter = counter  + 1;
    }     
    if (removingData) {
        [wordsContentDic removeAllObjects];
        removingData = false;
    }
    [self.WordsTable reloadData];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Reload) name:@"Reload" object:nil];
    
    if ([self.view viewWithTag:100]) {
        if([[self.view viewWithTag:100] viewWithTag:102]){
            [[[self.view viewWithTag:100] viewWithTag:102] removeFromSuperview];
            
        }
        [[self.view viewWithTag:100] removeFromSuperview];
    }
    
    if (characterWalkingAnimationImgView) {
        [characterWalkingAnimationImgView removeFromSuperview];
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
    }
    
    if (characterWalkingTimer) {
        [characterWalkingTimer invalidate];
        characterWalkingTimer = nil;
        [characterWalkingTimer release];
    }

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 55) {
        if (buttonIndex == 0) {
            [self UpDateBackendDataByTeacher];
        }
        else{
        }
    }
    if (alertView.tag == 25) {
        if (buttonIndex == 0){
            NSLog(@"Delete Folder");
            MrPDatabase *database = [[MrPDatabase alloc] init];
            [database DeleteFolder:_selectedFolderClassification];
            removingData = true;
            [database release];
            database = Nil;
            [self deleteSoundFilesOfRemovedFolder];
            [self deleteImageFilesOfRemovedFolder];
            if ([ActiveGrade isEqualToString:@"KG0"]) {
                foldersCountForFreeVersion = foldersCountForFreeVersion - 1;
            }

            [self Reload];
        }
        else{
            NSLog(@"Dont Delete Folder");
        }
    }
    if (alertView.tag == 26) {
        if (buttonIndex == 0){
            NSLog(@"Delete Word");

            int wordIdToBeRemove = [[wordsContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",selectedWordCell]] intValue];
            MrPDatabase *database = [[MrPDatabase alloc] init];
            [database removeWord:wordIdToBeRemove];
            NSLog(@"Delete from DB");
            [self deleteSoundFileOfRemovedWord:wordIdToBeRemove];
            NSLog(@"Delete Sound");
            [self deleteImageFileOfRemovedWord:wordIdToBeRemove];
            NSLog(@"delete image");
            [wordsContentDic removeAllObjects];
            NSArray *WordsInfoArray = [database ReadWordDatabase:_selectedFolderClassification];
//            [database release];
            NSLog(@"Read words again image");
            database = Nil;
            int counter = 0;
            for (MrPContentInfo *info in WordsInfoArray) {
                [wordsContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"%d",counter]];
                [wordsContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",counter]];
                [wordsContentDic setObject:[NSString stringWithFormat:@"%d",info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",counter]];
                [wordsContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",counter]];
                counter = counter  + 1;
            }      
            [self.WordsTable reloadData];
           
        }
        else{
            NSLog(@"Dont Delete Word");
        }
    }
    
}

- (IBAction)DeleteFolder:(id)sender {
    
    UIAlertView *alertDeleteFolder = [[UIAlertView alloc] init];
    [alertDeleteFolder setTitle:@"Confirm Please"];
    [alertDeleteFolder setMessage:@"Are you sure you want to delete folder? If you delete any folder, data saved in the folder will be removed too and cannot be retreived later."];
    [alertDeleteFolder setDelegate:self];
    [alertDeleteFolder addButtonWithTitle:@"YES"];
    [alertDeleteFolder addButtonWithTitle:@"NO"];
    [alertDeleteFolder setTag:25];
    [alertDeleteFolder show];
    [alertDeleteFolder release];
    return;

}

- (IBAction)AddWord:(id)sender {


}

- (IBAction)AddFolder:(id)sender {
    
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if ([ActiveGrade isEqualToString:@"KG0"]) {
        if (foldersCountForFreeVersion == 1 || foldersCountForFreeVersion > 0) {
            UIAlertView *alertAddFolderLimitation = [[UIAlertView alloc] init];
            [alertAddFolderLimitation setTitle:@"Warning"];
            [alertAddFolderLimitation setMessage:@"Sorry, You can only use ONE folder for free version."];
            [alertAddFolderLimitation addButtonWithTitle:@"OK"];
            [alertAddFolderLimitation show];
            [alertAddFolderLimitation release];
            return;
  
        }
    }
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    AddFolderController *bookVC= [sb instantiateViewControllerWithIdentifier:@"AddFolderController"];
    bookVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:bookVC animated:NO completion:NULL];

}

- (IBAction)HelpSound:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"SH040" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)DeleteWord:(id)sender {
    
    UIAlertView *alertDeleteFolder = [[UIAlertView alloc] init];
    [alertDeleteFolder setTitle:@"Confirm Please"];
    [alertDeleteFolder setMessage:@"Are you sure you want to delete word?"];
    [alertDeleteFolder setDelegate:self];
    [alertDeleteFolder addButtonWithTitle:@"YES"];
    [alertDeleteFolder addButtonWithTitle:@"NO"];
    [alertDeleteFolder setTag:26];
    [alertDeleteFolder show];
    [alertDeleteFolder release];
    return;
    
}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"myhome" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) deleteSoundFileOfRemovedWord:(int) soundIdToBeRemoved{


    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);        
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* directory = [paths lastObject];
    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];    
    NSString *fileName = [NSString stringWithFormat:@"%@/E%d.caf",ActiveGrade,soundIdToBeRemoved];
    NSLog(@"File Name is %@",fileName);
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[directory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", fileName]]]) {
        BOOL itemRemoved = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@/%@",directory, fileName] error:&error];
        if (itemRemoved == TRUE) {
            NSLog(@"Item \"%@\" Removed", fileName);
        } else {
            NSLog(@"Item \"%@\" Removal Failed", fileName);
        }
    } else {
        NSLog(@"Item \"%@\" does not exist", fileName);
    }
}

-(void) deleteImageFileOfRemovedWord:(int) imageToBeRemoved{
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* directory = [paths lastObject];
       ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *fileName = [NSString stringWithFormat:@"%@/%d.png",ActiveGrade,imageToBeRemoved];
    NSLog(@"File Name is %@",fileName);
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[directory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", fileName]]]) {
        BOOL itemRemoved = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@/%@",directory, fileName] error:&error];
        if (itemRemoved == TRUE) {
            NSLog(@"Item \"%@\" Removed", fileName);
        } else {
            NSLog(@"Item \"%@\" Removal Failed", fileName);
        }
    } else {
        NSLog(@"Item \"%@\" does not exist", fileName);
    }
}

-(void) deleteSoundFilesOfRemovedFolder{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);        
    NSError *error = nil;  
    NSFileManager *fileManager = [NSFileManager defaultManager]; 
    NSString* directory = [paths lastObject];
    for (int i = 0; i < 9; i++) {
                ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *fileName = [NSString stringWithFormat:@"%@/E%d%d.caf",ActiveGrade,_selectedFolderClassification,i];
    NSLog(@"File Name is %@",fileName);
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[directory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", fileName]]]) {
        BOOL itemRemoved = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@/%@",directory, fileName] error:&error];
        if (itemRemoved == TRUE) {
            NSLog(@"Item \"%@\" Removed", fileName);
        } else {
            NSLog(@"Item \"%@\" Removal Failed", fileName);
        }
    } else {
        NSLog(@"Item \"%@\" does not exist", fileName);
        }
    }
    
}

-(void) deleteImageFilesOfRemovedFolder{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* directory = [paths lastObject];
        ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    for (int i = 0; i < 9; i++) {
        
        NSString *fileName = [NSString stringWithFormat:@"%@/%d%d.png",ActiveGrade,_selectedFolderClassification,i];
        NSLog(@"File Name is %@",fileName);
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:[directory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", fileName]]]) {
            BOOL itemRemoved = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@/%@",directory, fileName] error:&error];
            if (itemRemoved == TRUE) {
                NSLog(@"Item \"%@\" Removed", fileName);
            } else {
                NSLog(@"Item \"%@\" Removal Failed", fileName);
            }
        } else {
            NSLog(@"Item \"%@\" does not exist", fileName);
        }
    }
    
}

- (void)viewDidUnload
{
    [self setBtnRefreshDataFromServer:nil];
    [booksScrollView release];
    booksScrollView = nil;
    [self setBackgroundImageView:nil];
    [self setFoldersTable:nil];
    [self setWordsTable:nil];
    [self setBtnDeleteFolder:nil];
    [self setBtnAddFolder:nil];
    [self setBtnAddWord:nil];
    [self setBtnHearWordSound:nil];
    [self setBtnDeleteWord:nil];
//    [super viewDidUnload];
    
    
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
    NSLog(@"numberOfSectionsInTableView");
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"numberOfRowsInSection");
    if (booksSelected) {
        if (tableView.tag == WORDS_TABLE_TAG)
        {
            return 15;
        }
        else
        {
            return numberOfFoldersInBook;
        }
    }
    if (tableView.tag == WORDS_TABLE_TAG) 
    {
            return 15;
        }
    else
        {
//            NSLog(@"Returning No. Of Folder");
            return 9;
        }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    if (tableView.tag == FOLDER_TABLE_TAG) 
    {
        
        NSString *kCellIdentifier = @"mainmenuGrammarPrincipleCell";
        
        _folderCell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
        if (_folderCell == nil)
        {
            
            _folderCell = [MyHomeCell createCell];
            _folderCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        _folderCell.backgroundColor = [UIColor clearColor];

        if (!booksSelected) {
            int loadingFolder = indexPath.row + 990;
            for (int i = 0; i < 9; i++)
            {
                _folderCell.titleLbl.textAlignment = UITextAlignmentCenter;
                
//                int randNum = rand() % (6 - 1) + 1;
                NSString *backgroundImage = [NSString stringWithFormat:@"%@.png",[foldersCellBackgroundImageArray objectAtIndex:indexPath.row]];
                _folderCell.backgroundImgView.image = [UIImage imageNamed:backgroundImage];
                NSString *extractFolder = [foldersContentDic objectForKey:[NSString stringWithFormat:@"%d",loadingFolder]];
                if (extractFolder == @"" || extractFolder == @"<nil>" || extractFolder == nil)
                {
                    loadingFolder = loadingFolder + 1;
                }
                else
                {
                    _folderCell.titleLbl.text = [foldersContentDic objectForKey:[NSString stringWithFormat:@"%d",loadingFolder]];
                    _folderCell.tag = indexPath.row+990;
                    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
                    if ([ActiveGrade isEqualToString:@"KG0"] && ![extractFolder isEqualToString:@"Capture your words"]) {
                        if (foldersCountForFreeVersion == 0) {
                            foldersCountForFreeVersion = foldersCountForFreeVersion + 1;
                        }
                    }
                    return _folderCell;
                }
                
                _folderCell.selectionImageView.image = nil;

                
            }
        
        }
        else{
            int loadingFolder = _selectedFolderClassification + indexPath.row + 1;

            for (int i = 0; i < numberOfFoldersInBook; i++)
            {
                _folderCell.titleLbl.text = [bookFoldersContentDic objectForKey:[NSString stringWithFormat:@"%d",loadingFolder]];
                NSString *backgroundImage = [NSString stringWithFormat:@"%@.png",[foldersCellBackgroundImageArray objectAtIndex:indexPath.row]];
                _folderCell.backgroundImgView.image = [UIImage imageNamed:backgroundImage];

                _folderCell.tag = loadingFolder;
                _folderCell.titleLbl.textAlignment = UITextAlignmentCenter;
                _folderCell.selectionImageView.image = nil;
            }
        }

        return _folderCell;        
    }
    
    else 
    {
        NSString *kCellIdentifier = @"MyIdentifier";
        
        _wordsCell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
        if (_wordsCell == nil)
        {
            
            _wordsCell = [WordsCell createCell];
            _wordsCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        _wordsCell.backgroundColor = [UIColor clearColor];
        NSString *backgroundImage = [NSString stringWithFormat:@"%@.png",[wordssCellBackgroundImageArray objectAtIndex:indexPath.row]];

        _wordsCell.backgroundImgView.image = [UIImage imageNamed:backgroundImage];
        
        if (!booksSelected) {
            _wordsCell.titleLbl.textAlignment = UITextAlignmentCenter;
            NSString *word = [wordsContentDic objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
            _wordsCell.titleLbl.text = word;
            int imageIdentify = [[wordsContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",indexPath.row]] intValue];
            if (imageIdentify) {
                _wordsCell.itemImgView.image = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%d",imageIdentify]];
            }
            else{
                _wordsCell.itemImgView.image = [UIImage imageNamed:@"2.png"];
            }
            
        }
        else{
            _wordsCell.titleLbl.textAlignment = UITextAlignmentCenter;
            NSString *word = [wordsContentDic objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]];
            _wordsCell.titleLbl.text = word;
            int imageIdentify = [[wordsContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",indexPath.row]] intValue];
            if (imageIdentify) {
                _wordsCell.itemImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d",imageIdentify]];
            }
            else{
                if (booksSelected) {
                    _wordsCell.itemImgView.image = [UIImage imageNamed:@"2.png"];
                }
                _wordsCell.itemImgView.image = [UIImage imageNamed:@"2.png"];
            }
        }
        
        return _wordsCell;

    }
}

-(void) selectFolderInDatabase:(int) cId{
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:PRINCIPLE_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:GRAMMAR_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:CLASSIFICATIONS_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:PRINCIPLE_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:GRAMMAR_ID_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d" ,cId] forKey:CLASSIFICATIONS_ARRAY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (tableView.tag == FOLDER_TABLE_TAG) {
        

//        if (selectedIndex) {
//            _folderCell = [self.FoldersTable cellForRowAtIndexPath:selectedIndex];
//            _folderCell.selectionImageView.image = nil;
//        }
        
        NSArray *cells = [_FoldersTable visibleCells];
        for (MyHomeCell *cell in cells)
        {
            _folderCell.selectionImageView.image = nil;
        }
        
        _folderCell = [_FoldersTable cellForRowAtIndexPath:indexPath];
        _folderCell.selectionImageView.image = [UIImage imageNamed:@"icon-clip.png"];

        selectedIndex = indexPath;
        selectedIndexRow = indexPath.row;
        
        if (booksSelected) {
            currentTappedBookFolder = TRUE;
            _selectedFolderClassification = indexPath.row + loadingClassification + 1;
        }
        else{
            currentTappedBookFolder = FALSE;
            _selectedFolderClassification = indexPath.row + 990;
        }
        [[NSUserDefaults standardUserDefaults] setInteger:_selectedFolderClassification forKey:@"selectedClassificationId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self selectFolderInDatabase:_selectedFolderClassification];

        [[NSUserDefaults standardUserDefaults] setObject:[foldersContentDic objectForKey:[NSString stringWithFormat:@"%d", _selectedFolderClassification] ] forKey:@"folderName"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        
        
        NSLog(@"Folder cell tag is %d",_selectedFolderClassification);
        if (wordsContentDic) {
            [wordsContentDic removeAllObjects];
        }                
        MrPDatabase *database = [[MrPDatabase alloc] init];
        NSArray *WordsInfoArray;
        if (booksSelected) {
            WordsInfoArray = [database ReadBooksWordsDatabase:_selectedFolderClassification];
        }
        else{
            WordsInfoArray = [database ReadWordDatabase:_selectedFolderClassification];
        }


        database = Nil;
        int counter = 0;
        for (MrPContentInfo *info in WordsInfoArray) {
            [wordsContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"%d",counter]];
            [wordsContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",counter]];
            [wordsContentDic setObject:[NSString stringWithFormat:@"%d",info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",counter]];
            [wordsContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",counter]];
            counter = counter  + 1;
        }

        [self.WordsTable reloadData];
    }

    if (tableView.tag == WORDS_TABLE_TAG) {            
        
        selectedWordCell = indexPath.row;
  
    }
}


+(int) assignClassificationId{
    int classId = _selectedFolderClassification;
    return classId;
}

-(void) dealloc{
    [_btnRefreshDataFromServer release];
    [booksScrollView release];
    [_backgroundImageView release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [_BtnDeleteFolder release];
    [_BtnAddFolder release];
    [_btnAddWord release];
    [_btnHearWordSound release];
    [_BtnDeleteWord release];
//    [super dealloc];
}



- (IBAction)playSoundOfSelectedWord:(id)sender {

    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if ([ActiveGrade isEqualToString:@"FREE"]) {
        [self freePackage];
        return;
    }    
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }    
    
    NSString *fileName = [wordsContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",selectedWordCell]];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* soundFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.caf",ActiveGrade,fileName]];
    NSLog(@"soundfile path is %@",soundFilePath);
    
    if (booksSelected) {
//        soundFilePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",fileName] ofType:@"mp3"];
       soundFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.mp3",ActiveGrade,fileName]];


    }

    if (soundFilePath == nil || !soundFilePath) {
        NSLog(@"Sound NOt found");
        fileName = @"EmptySound";
        soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        player.volume = 0.5;
        player.numberOfLoops = 0;
        player.delegate = self;
        [player play];
        
        return;
    }
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;    
    [player play];     
}

-(void) freePackage{
    NSLog(@"User has free classification Active");
    
    UIAlertView *alertFreePackage = [[UIAlertView alloc] init];
    int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 2) {
        [alertFreePackage setTitle:@"CAUTION"]; 
        [alertFreePackage setMessage:@"This section becomes active when you become a friend of MR PICK IT. Please go to SETUP to change grade or download package from My Bookstore"];
        [alertFreePackage setDelegate:self];
        [alertFreePackage addButtonWithTitle:@"Ok, I'm coming back."];
    }
    else {
        [alertFreePackage setTitle:@"تحذير !"]; 
        [alertFreePackage setMessage:@"لايمكن أضافة كلمات خاصة بك مع ؟؟؟الرزمة؟؟؟ المجانية , يحب شراء احدى المهارات الاساسيه أولا  "];
        [alertFreePackage setDelegate:self];
        [alertFreePackage addButtonWithTitle:@"حسنا , سوف أعود لاحقا "];
    }

    [alertFreePackage setTag:2];
    [alertFreePackage show];
    [alertFreePackage release];
    return;

}

-(void) playHelpMovie{
    
    NSLog(@"playing help movie");
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == NO) {
//        return;
//    }
    
    NSString *movieName = @"My_Home_new_concept";
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
        return;
    }
    
    MovieViewController* moviePlayer = [[MovieViewController alloc] init];
    [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayer animated:NO];
    // Prep and play the movie

    [moviePlayer playMovie:movieName];    
}

-(void) viewDidAppear:(BOOL)animated{
    if (!showMovie) {
//        [self playHelpMovie];
    }

}

-(void)viewDidDisappear:(BOOL)animated{
    showMovie = TRUE;
}

- (IBAction)refrestDataFromBackend:(id)sender {
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"]) {
//        [self dataAvailableForUpdateAtBackend];
//    }
}

-(void) loadQuestions{
    if (bookQuestionsView) {
        [bookQuestionsView removeFromSuperview];
        bookQuestionsView = nil;
    }
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    [self.FoldersTable reloadData];
}
-(void) readBook{
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    BookViewController *bookVC= [sb instantiateViewControllerWithIdentifier:@"BookViewController"];
    bookVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:bookVC animated:NO completion:NULL];
}

- (IBAction)actionLoadBook:(id)sender {
}

- (IBAction)actionLoadMyHome:(id)sender {
    [self enableExtraButtons];
    booksSelected = FALSE;
    [self.FoldersTable reloadData];
    self.backgroundImageView.image = [UIImage imageNamed:@"Myhome_words.png"];
    if (wordsContentDic) {
        [wordsContentDic removeAllObjects];
        [self.WordsTable reloadData];
    }

    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }

}
- (IBAction)exitMyHome:(id)sender {
    UIButton *btn = (UIButton*) sender;
    if (btn.tag == MY_CAMP_ACTION_BTN_TAG) {
        
        NSString *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
        if([objectClassification isKindOfClass:[NSString class]] == YES)
        {
            
            if ([objectClassification isEqualToString:@""]) {
                UIAlertView *selectionAlert;
                selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly Select any folder" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                [selectionAlert show];
                return;
            }
            else{
                
                MrPDatabase *database = [[MrPDatabase alloc] init];
                NSArray *WordsInfoArray;
                if (booksSelected && currentTappedBookFolder) {
                    WordsInfoArray = [database ReadBooksWordsDatabase:_selectedFolderClassification];
                }
                else{
                    if (_selectedFolderClassification < 999) {
                        WordsInfoArray = [database ReadWordDatabase:_selectedFolderClassification];
                    }
                    else
                    {
                        
                    UIAlertView *selectionAlert;
                    selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly Select any folder" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                    [selectionAlert show];
                        return;
                    }
                }
                
                
                if ([WordsInfoArray count] == 0) {
                    UIAlertView *selectionAlert;
                    selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Not Sufficient Data in selected folder" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                    [selectionAlert show];
                    return;
                }
                
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                MyCampController *vc = [sb instantiateViewControllerWithIdentifier:@"MyCampControllerIderntifier"];
                vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self presentViewController:vc animated:YES completion:NULL];
                return;
            }
            
        }
    }
        else  if (btn.tag == PLAYGROUND_ACTION_BTN_TAG) {
            NSString *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
            if([objectClassification isKindOfClass:[NSString class]] == YES)
            {
                
                if ([objectClassification isEqualToString:@""]) {
                    UIAlertView *selectionAlert;
                    selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly Select any folder" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                    [selectionAlert show];
                    return;
                }
                else{
                    MrPDatabase *database = [[MrPDatabase alloc] init];
                    NSArray *WordsInfoArray;
                    if (booksSelected  && currentTappedBookFolder) {
                        WordsInfoArray = [database ReadBooksWordsDatabase:_selectedFolderClassification];
                    }
                    else{
                        if (_selectedFolderClassification < 999) {
                            WordsInfoArray = [database ReadWordDatabase:_selectedFolderClassification];
                        }
                        else{
                            UIAlertView *selectionAlert;
                            selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly Select any folder" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                            [selectionAlert show];
                                                    return;

                        }
                    }
                    
                    
                    if ([WordsInfoArray count] == 0) {
                        UIAlertView *selectionAlert;
                        selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Not Sufficient Data in selected folder" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                        [selectionAlert show];
                        return;
                    }

                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                    MyPlayGroundViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyPlaygroundIdentifier"];
                    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                    [self presentViewController:vc animated:YES completion:NULL];
                }
                
            }
        }
}
@end

