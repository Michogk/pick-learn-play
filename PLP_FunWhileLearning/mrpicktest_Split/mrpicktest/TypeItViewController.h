//
//  TypeItViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 09/10/2015.
//  Copyright © 2015 SmartSoft. All rights reserved.
//


#import <AVFoundation/AVFoundation.h>
@interface TypeItViewController : UIViewController<AVAudioPlayerDelegate>{
    
//    IBOutlet UITextField *myText;
    
    IBOutlet UITextView *myText;
    AVAudioPlayer *player;
    NSMutableDictionary* levelsDict;
    int sessionCounter;
    
    NSString* currentStageWord;
    NSString* currentStageWordAudio;
    
    int currentWord; // count that at which word we are currently at!

    
    int stage;
    NSMutableDictionary *tempContentDic;
    
    
    IBOutlet UILabel *labelLevelOne;
    IBOutlet UILabel *labelLevelTwo;
    
    IBOutlet UILabel *labelWordsCount;
    int cId;
    int wordImg;
    int activeLanguage;
    int level;
    int correctAnswerCounter;
    
    // Report Data
    NSMutableArray *retval;
    int reportWordId;
    NSString *reportWordDesc;
    int reportCorrectCount;
    int reportTotatCount;
}
- (IBAction)actionNext:(id)sender;
- (IBAction)actionGotoMainMenu:(id)sender;
- (IBAction)actionReset:(id)sender;
- (IBAction)actionHearWord:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *imageViewWord;
- (IBAction)actionHelp:(id)sender;
- (IBAction)actionCheck:(id)sender;

@end
