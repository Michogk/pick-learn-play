//
//  PickItViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 3/31/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>

@interface MoveItViewController : UIViewController<AVAudioPlayerDelegate,ADBannerViewDelegate>{
    int answersCounter;
    BOOL draggingBegin;
    BOOL cityMapClickedWithActiveFolder;
    CGRect itemOriginalframe;
    NSMutableDictionary *tempContentDicCorrect;
    NSMutableDictionary *tempIncontentDicCorrect;
    NSMutableArray *arrRecordCorrectTagsOfItems;
    NSMutableArray *arrRecordINCorrectTagsOfItems;
    AVAudioPlayer *player;
    int currentCorrectIndexPosition;
    int wordsCount;
    NSTimer *viewCompletedTimer;
    int correctCounter;
    NSTimer* cheeringSoundTimer;
    BOOL firstLaunch;
    int cId;
    AVAudioPlayer *myMoveItPlayer;
    
    int sessionCounter;
    int correctWordCounter;
    UILabel *_wordCounterlbl;
    UILabel *_levelCounterlbl;
    int currentWord;
    int level;
    UILabel *_lblLevel1;
    UILabel *_lblLevel2;
    
        UILabel *_lblPointsEarned;
    
    // Report Data
    NSMutableArray *retval;
    
    int reportWordId01;
    NSString *reportWordDesc01;
    int reportCorrectCount01;
    int reportTotatCount01;
    
    int reportWordId02;
    NSString *reportWordDesc02;
    int reportCorrectCount02;
    int reportTotatCount02;
    
    int reportWordId03;
    NSString *reportWordDesc03;
    int reportCorrectCount03;
    int reportTotatCount03;
    IBOutlet UILabel *labelSynonym;

}
- (IBAction)gotoMainMenu:(id)sender;


- (IBAction)playClassificationSound:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *lblClassification;
@property (retain, nonatomic) IBOutlet UIButton *btnNext;
//@property (retain, nonatomic) IBOutlet UIButton *BtnImages;
//@property (retain, nonatomic) IBOutlet UIButton *btnWords;
//- (IBAction)btnImagesPressed:(id)sender;
//- (IBAction)btnWordsPressed:(id)sender;
- (IBAction)btnNextPressed:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *imgCheck1;
@property (retain, nonatomic) IBOutlet UIImageView *imgCheck2;
@property (retain, nonatomic) IBOutlet UIImageView *imgCheck3;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewCheering;
@property (strong, nonatomic) ADBannerView *bannerView;
- (IBAction)hearCorrectWord:(id)sender;
@end
