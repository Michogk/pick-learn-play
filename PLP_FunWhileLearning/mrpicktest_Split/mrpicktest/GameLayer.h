//
//  HelloWorldLayer.h
//  BubbleIpad
//
//  Created by Muhammad Mosib Asad on 6/28/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


#import "cocos2d.h"
#import <AVFoundation/AVFoundation.h>
typedef enum
{
    LanguageIdOne = 0,
    LanguageIdTwo,
    LanguageIdThree,
    LanguageIdFour,
    
    LanguageId_MAX,
    
} LanguageId;

@class BaloonsGroup;

@interface GameLayer : CCLayer<AVAudioPlayerDelegate>
{
    AVAudioPlayer *player;
    CCLabelTTF *lblCorrectVerb;
    CCLabelTTF *lblSentence;
    CCLabelTTF *lblbottomBar;
    //    BOOL _answerIsCorrect;
    
    NSString* sentence;
    NSString* missngWord;
    NSString* audiofile;
    
    int missingPosition;
    int languageID;
    int voiceID;
    
    CCMenu *goToPlayGroundMenu;
    NSTimer *wordStopTimer;
    BaloonsGroup* balloons;
    NSMutableDictionary *_dummyGrammarDic;
    NSMutableDictionary* levelsDict;
    NSTimer *cheeringSoundTimerPopIt;
    int gId;
    NSMutableDictionary* levelPoints;
    CCMenu *helpMenu;
    //    NSArray *_arabicWordsArr;
    
    LanguageId currentLanguageId;
    NSMutableDictionary* tempContentDic;
    int wordNumber;
    int cId;
    int activeLanguage;
    int currentWord;
    BOOL correctBalloonAlreadyBusted;
    BOOL wrongBalloonAlreadyBusted;
    BOOL insufficientDataInSelectedSkillsPackage;
    int level;
    int sessionCounter;
    int sessionProgressCounter;
    
    NSMutableArray *retval;
    int reportWordId;
    NSString *reportWordDesc;
    int reportCorrectCount;
    int reportTotatCount;
}

+ (CCScene *) scene;
+ (GameLayer*) sharedLayer;
+ (int) currentLevel;

- (void) startGame;
- (void) loadLevelData: (int) level;
- (void) showCorrectWord;
- (void) showSentence;
- (void) callBubblesAnimation;
- (void) balloonTapped: (CGPoint) pos IsRight:(BOOL) isRight;
- (void) updateCompleteSentence;
- (void) birdAnimation;
- (void) birdExcitementAnimation;
- (void) birdDisappointmentAnimation;
- (void) removeCurrentLevel;
- (void) nextLevel:(id)sender;
- (NSString *) levelPointsFileSavePath;
- (void) updatePlayerPoints;
- (void) gameEnded;
- (void) showPlayerPoints;
- (void) updateGuesSentence;
-(void) nextLevel:(id)sender;
-(int ) getcurrentWord;
-(NSMutableDictionary *) getTempContentDic;
-(BOOL) wrongBalloonBusted;
-(BOOL) correctBallonBusted;
-(NSMutableDictionary *) getDummyDic;



@property (nonatomic, assign) BOOL _answerIsCorrect;
@end
