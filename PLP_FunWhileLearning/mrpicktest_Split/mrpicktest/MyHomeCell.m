//
//  MyHomeCell.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 23/09/2015.
//  Copyright (c) 2015 SmartSoft. All rights reserved.
//

#import "MyHomeCell.h"

@implementation MyHomeCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(id) createCell{
    MyHomeCell *cell;
    cell = [Common loadNibName:@"MyHomeCell" owner:self options:nil];
    return cell;
}


- (void)dealloc {
    [_backgroundImgView release];
    [_titleLbl release];
    [_selectionImageView release];
    [super dealloc];
}
@end
