//
//  VoiceRecordViewController.m
//  mrpicktest
//
//  Created by Muhammad Mosib Asad on 09/29/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//


#import "VoiceRecordViewController.h"
#import "AddWordViewController.h"
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]


@implementation VoiceRecordViewController

- (void)viewDidLoad 
{
	[super viewDidLoad];
	lblStatusMsg.text = @"Stopped";
	progressView.progress = 0.0;
}

- (void) handleTimer
{
	progressView.progress += .07/5;
	if(progressView.progress == 1.0)
	{
		[timer invalidate];
                timer = nil;
		lblStatusMsg.text = @"Stopped";
	}
}

- (IBAction)CancelRecording:(id)sender {
    
    [recorder stop];
    
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
    
    [self.view removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"CancelWord" object:self]];
}

- (IBAction) startRecording
{	
    NSLog(@"start Recording");
	AVAudioSession *audioSession = [AVAudioSession sharedInstance];
	NSError *err = nil;
	[audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
	if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
	}
	[audioSession setActive:YES error:&err];
	err = nil;
	if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
	}
	
        NSLog(@"#Record Setting");
	recordSetting = [[NSMutableDictionary alloc] init];	
	
	[recordSetting setValue :[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];

	// We can use 44100, 32000, 24000, 16000 or 12000 depending on sound quality
	[recordSetting setValue:[NSNumber numberWithFloat:16000.0] forKey:AVSampleRateKey];
	
	
	[recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
        
        NSString *voiceId = [AddWordViewController getVoiceId]; 
            NSLog(@"#Voice Id %@,",voiceId);
        NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];

	recorderFilePath = [[NSString stringWithFormat:@"%@/%@/%@.caf", DOCUMENTS_FOLDER,activeGrade,voiceId] retain];
    NSLog(@"########################### Recorded file path is  is %@",recorderFilePath);	
	NSLog(@"#recorderFilePath: %@",recorderFilePath);
	
	NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
	
	err = nil;
	
	NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
	if(audioData)
	{
		NSFileManager *fm = [NSFileManager defaultManager];
		[fm removeItemAtPath:[url path] error:&err];
	}
	
	err = nil;
	recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
	if(!recorder){
        NSLog(@"#recorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
								   message: [err localizedDescription]
								  delegate: nil
						 cancelButtonTitle:@"OK"
						 otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
	}
	
	//prepare to record
	[recorder setDelegate:self];
	[recorder prepareToRecord];
	recorder.meteringEnabled = YES;
	
	BOOL audioHWAvailable = audioSession.inputIsAvailable;
	if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
								   message: @"Audio input hardware not available"
								  delegate: nil
						 cancelButtonTitle:@"OK"
						 otherButtonTitles:nil];
        [cantRecordAlert show];
        [cantRecordAlert release]; 
        return;
	}
	
	// start recording
	[recorder recordForDuration:(NSTimeInterval) 10];
//            lblStatusMsg.font = [UIFont fontWithName:@"Arial" size:22];
    UIFont *font = [UIFont systemFontOfSize: 22];
    [lblStatusMsg setFont:font];
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        lblStatusMsg.text = @"Recording...";
    }
    else {
        lblStatusMsg.text = @"تسجيل...";

    }
	
	progressView.progress = 0.0;
	timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(handleTimer) userInfo:nil repeats:YES];
}

- (IBAction) stopRecording
{
	[recorder stop];
	
    if (timer) {
            [timer invalidate];
            timer = nil;
    }
 
	lblStatusMsg.text = @"Stopped";
	progressView.progress = 1.0;
    AudioSessionSetActive(NO);
	
	
}

- (IBAction)playSound
{
    if(!recorderFilePath){
                   NSString *voiceId = [AddWordViewController getVoiceId];
                NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];        
		recorderFilePath = [[NSString stringWithFormat:@"%@/%@/%@.caf", DOCUMENTS_FOLDER,activeGrade,voiceId] retain];
    }
	
	if(soundID)
	{
		AudioServicesDisposeSystemSoundID(soundID);
	}
	
	//Get a URL for the sound file
	NSURL *filePath = [NSURL fileURLWithPath:recorderFilePath isDirectory:NO];
	
    
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }  
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;    
    [player play];
    
    
    
    
	//Use audio sevices to create the sound
//	AudioServicesCreateSystemSoundID((CFURLRef)filePath, &soundID);
	
	//Use audio services to play the sound
//	AudioServicesPlaySystemSound(soundID);
    
    
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *) aRecorder successfully:(BOOL)flag
{
	NSLog (@"audioRecorderDidFinishRecording:successfully:");
	[timer invalidate];
        timer = nil;
	lblStatusMsg.text = @"Stopped";
	progressView.progress = 1.0;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end
