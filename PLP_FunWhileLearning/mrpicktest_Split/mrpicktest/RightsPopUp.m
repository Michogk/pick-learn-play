//
//  RightsPopUp.m
//  GMG App
//
//  Created by Mosib on 4/27/15.
//  Copyright (c) 2015 Advansoft. All rights reserved.
//

#import "RightsPopUp.h"
#import "AnimationUtility.h"

@implementation RightsPopUp
{
    UIImagePickerController *imagePickerController;
}

+ (RightsPopUp*) mainView {
    RightsPopUp *popUp = [Common loadNibName:@"RightsPopUp" owner:self options:nil];
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = popUp.imgViewBackground.bounds;
    [popUp.imgViewBackground addSubview:visualEffectView];
    return popUp;
}

#pragma mark -
#pragma mark Public Methods

- (void) showOnView:(UIView*)view withDelegate:(id<RightsPopUpDelegate>)delegate {
        _textViewName.delegate = self;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"]) {
        _textViewName.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
    }

    _delegate = delegate;
    _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [view addSubview:self];
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                _viewPopUp.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
}

- (void) showOnView:(UIView*)view {
//    [[NSUserDefaults standardUserDefaults] setObject:_txtFieldPinNumber.text forKey:@"PINNUMBER"];
//    [[NSUserDefaults standardUserDefaults] synchronize];

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"]) {
        _textViewName.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
    }

    [self showOnView:view withDelegate:nil];
}

- (void) hide {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _viewPopUp.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
    [_textViewName resignFirstResponder];

    [_viewPopUp removeFromSuperview];
}


#pragma mark -
#pragma mark Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
}

#pragma mark -
#pragma mark TextField Delegates


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    CGRect frame = _viewPopUp.frame;
    frame.origin.y = frame.origin.y - 130;
    [AnimationUtility moveFrame:_viewPopUp withFrame:frame withDuration:0.3];
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    CGRect frame = _viewPopUp.frame;
    frame.origin.y = frame.origin.y + 130;
    [AnimationUtility moveFrame:_viewPopUp withFrame:frame withDuration:0.3];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    return YES;
}

#pragma mark -
#pragma mark Actions

- (IBAction) actionSubmit:(id)sender {

    // Call WebService
    
    if ([_textViewName.text length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly Enter Your Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert.delegate = self;
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:_textViewName.text forKey:@"NAMEOFUSER"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PINNEDDEVICE"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    [[NSUserDefaults standardUserDefaults] setObject:_textViewName.text forKey:@"PINNUMBER"];
    [[NSUserDefaults standardUserDefaults] synchronize];


    
    [self hide];
//    if ([_delegate respondsToSelector:@selector(RightsViewSubmitted:)]) {
//        [_delegate  RightsViewSubmitted:self];
//    }
}

- (IBAction)actionCancel:(id)sender {
    [self hide];
//    if ([_delegate respondsToSelector:@selector(RightsViewSubmitted:)]) {
//        [_delegate  RightsViewSubmitted:self];
//    }
}

@end
