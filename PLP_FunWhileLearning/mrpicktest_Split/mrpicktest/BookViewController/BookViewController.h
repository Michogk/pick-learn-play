//
//  BookViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/3/14.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "DocumentDirectory.h"

@interface BookViewController : UIViewController<UIPageViewControllerDataSource,AVAudioPlayerDelegate,AVAudioRecorderDelegate,MFMailComposeViewControllerDelegate>{
    AVAudioPlayer *player;
    
    // Recorder methods

    NSMutableDictionary *recordSetting;
    NSMutableDictionary *editedObject;
    NSString *recorderFilePath;
    AVAudioRecorder *recorder;
    SystemSoundID soundID;
    NSTimer *timer;
    
    IBOutlet UIButton *buttonClearRecordedSounds;
    IBOutlet UIButton *buttonSendMail;
}
- (IBAction)actionClearRecordedSounds:(id)sender;
- (IBAction)actionSendMail:(id)sender;
- (IBAction)startWalkthrough:(id)sender;


@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSMutableArray *pageTitles;
@property (strong, nonatomic) NSMutableArray *pageImages;
@property (strong, nonatomic) NSMutableArray *pageSound;
@end
