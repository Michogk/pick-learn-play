//
//  BookViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/3/14.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface PageContentViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property  NSUInteger pageIndex;
@property (strong, nonatomic) NSString *titleText;
@property (strong, nonatomic) NSString *imageFile;
@end
