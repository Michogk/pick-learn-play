//
//  BookViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/3/14.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import "BookViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "SimpleAudioEngine.h"
#import "ZipArchive.h"

@interface BookViewController (){
    int activePage;
    BOOL silentMode;
    NSMutableDictionary *wordsContentDic;
    IBOutlet UIButton *buttonStartRecording;
    IBOutlet UIButton *buttonStopRecording;
//    BOOL buttonReadForMe;
    BOOL boolMuteSoundCompletely;
}

@property (retain, nonatomic) IBOutlet UIButton *btnReadForMe;
@property (retain, nonatomic) IBOutlet UIButton *btnSilent;
- (IBAction)actionReadForMe:(id)sender;
- (IBAction)actionSilent:(id)sender;
- (IBAction)actionStop:(id)sender;
- (IBAction)actionHearAgain:(id)sender;

- (IBAction)actionStartRecording:(id)sender;
- (IBAction)actionStopRecording:(id)sender;

- (IBAction)actionCloseBook:(id)sender;


@end

@implementation BookViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    buttonStartRecording.hidden = YES;
    buttonStopRecording.hidden = YES;
    
    silentMode = FALSE;
//    [self updateTopMenu];
    
    int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];

    if (selectedBookClassification < 1999) {
        buttonClearRecordedSounds.hidden = YES;
        buttonSendMail.hidden = YES;
    }

    NSString *activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    _pageSound = [[NSMutableArray alloc] init];
    _pageImages = [[NSMutableArray alloc] init];
    _pageTitles = [[NSMutableArray alloc] init];
                   
    NSArray *WordsInfoArray;
    WordsInfoArray = [database ReadBooksWordsDatabase:selectedBookClassification];

    database = Nil;
//    int counter = 0;
    for (MrPContentInfo *info in WordsInfoArray) {
        [_pageImages addObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:info.imageId]]];
        [_pageSound addObject:[NSString stringWithFormat:@"%@",info.voiceId]];
        [_pageTitles addObject:[NSString stringWithFormat:@"%@",info.englishWord]];
    }

    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    if (!startingViewController) {
        return;
    }
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 85, self.view.frame.size.width, self.view.frame.size.height-85);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionClearRecordedSounds:(id)sender {
    UIAlertView *alert;
    alert = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure you want to clear all recorded sounds?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil ];
    alert.tag = 10;
    [alert show];
    [alert release];
}

-(void) clearAllRecordedSounds{
    NSString *filePath = [self getRecordedFolderPath];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:filePath]) {
        NSLog(@"File Exist");
        NSError *error;
        BOOL success = [fileManager removeItemAtPath:filePath error:&error];
        if (!success || error) {
            
        }
        else
        {
            
        }
    }
}

- (IBAction)actionSendMail:(id)sender {
    NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    BOOL isDir=NO;

    NSString *toCompress = [self getRecordedFolderPath];
    NSString *pathToCompress = toCompress; //[documentsDirectory stringByAppendingPathComponent:toCompress];
    
    NSArray *subpaths;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:pathToCompress isDirectory:&isDir] && isDir){
        subpaths = [fileManager subpathsAtPath:pathToCompress];
    } else if ([fileManager fileExistsAtPath:pathToCompress]) {
        subpaths = [NSArray arrayWithObject:pathToCompress];
    }
    

    int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];
    NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.zip",selectedBookClassification ]];
    ZipArchive *za = [[ZipArchive alloc] init];
    [za CreateZipFile2:zipFilePath];
    if (isDir) {
        for(NSString *path in subpaths){
            NSString *fullPath = [pathToCompress stringByAppendingPathComponent:path];
            if([fileManager fileExistsAtPath:fullPath isDirectory:&isDir] && !isDir){
                [za addFileToZip:fullPath newname:path];
            }
        }
    } else {
        [za addFileToZip:pathToCompress newname:toCompress];
    }
    
    BOOL successCompressing = [za CloseZipFile2];
    if (successCompressing) {
        NSLog(@"True");
        
        [self mailReportToTeacher];
    }
    else{
        NSLog(@"fail");
    }
}

-(NSString*) getRecordedFolderPath{
    int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];
    NSString *bookRecordedSoundDirectory = [NSString stringWithFormat:@"%d",selectedBookClassification];
    NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    return [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",ActiveGrade,bookRecordedSoundDirectory]];
}
- (IBAction)startWalkthrough:(id)sender {
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    activePage = index;
    [self loadVoice:index];
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

-(void) loadVoice:(int) pageNumber{
//    if (boolMuteSoundCompletely) {
//        return;
//    }
    NSString *extension = @"mp3";
    NSString *bookRecordedSoundDirectory = @"";
    int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];
    if (selectedBookClassification > 1999 && (pageNumber %2 == 0) && (pageNumber != 0)) {
        extension = @"caf";
        bookRecordedSoundDirectory = [NSString stringWithFormat:@"%d/",selectedBookClassification];
        buttonStartRecording.hidden = NO;
        buttonStopRecording.hidden = NO;
        buttonStopRecording.enabled = NO;
        buttonStartRecording.enabled = YES;
        

    }
    else{
        buttonStartRecording.hidden = YES;
        buttonStopRecording.hidden = YES;
    }

    if (silentMode) {
//        [self updateTopMenu];
        return;
    }

    
    NSString *soundName = [_pageSound objectAtIndex:pageNumber];
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }

    if ([soundName isEqualToString:@""]) {
        return;
    }

    if ([extension isEqualToString:@"mp3"]) {
        
        NSURL *url = [NSURL fileURLWithPath:[DocumentDirectory playSound:soundName]];
        
        if (!url) {
            return;
        }
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        player.numberOfLoops = 0;
        player.delegate = self;
        [player play];

        return;
    }

    NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    NSString* soundFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@%@.%@",ActiveGrade,bookRecordedSoundDirectory, soundName,extension]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [NSURL fileURLWithPath:soundFilePath];
    if (![fileManager fileExistsAtPath:soundFilePath] && [self checkIfRecordingPage:pageNumber]) {
        url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                             pathForResource:soundName
                                             ofType:@"mp3"]];

        
    }



    if (!url) {
        return;
    }
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];

}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (IBAction)actionReadForMe:(id)sender {
    
//    silentMode = FALSE;
    boolMuteSoundCompletely = false;
    if (!silentMode) {
        if (player) {
            [player stop];
            [player release];
            player = Nil;
        }

        [self updateTopMenu];
        return;
    }
    
    NSString *soundName = [_pageSound objectAtIndex:activePage];
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    
    NSString *extension = @"mp3";
    int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];
        NSString *bookRecordedSoundDirectory = @"";
    if (selectedBookClassification > 1999 && (activePage %2 == 0) && (activePage != 0)) {
        extension = @"caf";
        bookRecordedSoundDirectory = [NSString stringWithFormat:@"%d/",selectedBookClassification];
    }
    
    if ([soundName isEqualToString:@""]) {
        return;
    }
    
    if ([extension isEqualToString:@"mp3"]) {
        
        NSURL *url = [NSURL fileURLWithPath:[DocumentDirectory playSound:soundName]];
        
        if (!url) {
            return;
        }
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        player.numberOfLoops = 0;
        player.delegate = self;
        [player play];
        [self updateTopMenu];
        return;
    }
    
    
    
//    
//    
//    NSString* soundFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@%@.%@",ActiveGrade,bookRecordedSoundDirectory,soundName,extension]];
//    NSURL *url = [NSURL fileURLWithPath:soundFilePath];
//    if (!url) {
//        return;
//    }
//
//    
//    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
//    player.numberOfLoops = 0;
//    player.delegate = self;
//    [player play];
//    
    [self updateTopMenu];
}

- (IBAction)actionSilent:(id)sender {
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    silentMode = TRUE;
    [self updateTopMenu];
}
-(void)updateTopMenu{
    if (silentMode) {
        [self.btnReadForMe setBackgroundImage:[UIImage imageNamed:@"Play.png"]
                            forState:UIControlStateNormal];
        silentMode = false;
        
    }
    else{
        silentMode = true;
        [self.btnReadForMe setBackgroundImage:[UIImage imageNamed:@"Play-off.png"]
                                     forState:UIControlStateNormal];
    }
}

-(BOOL) checkIfRecordingPage:(int) pageNumber{
 
    int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];

    if (selectedBookClassification > 1999 && (pageNumber %2 == 0) && (pageNumber != 0)) {
        return true;
    }
    else return false;
}

- (IBAction)actionStop:(id)sender {
    
//    if ([self checkIfRecordingPage:activePage]) {
        buttonStartRecording.enabled = YES;
        buttonStopRecording.enabled = NO;
        [recorder stop];
        AudioSessionSetActive(NO);
//    }
//    else{
//        if (player) {
//            [player stop];
//            [player release];
//            player = Nil;
//        }
//        silentMode = true;
//        boolMuteSoundCompletely = true;
//        [self updateTopMenu];
//    }
}

- (IBAction)actionHearAgain:(id)sender {
    NSString *soundName = [_pageSound objectAtIndex:activePage];
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                         pathForResource:soundName
                                         ofType:@"mp3"]];
            
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
}

- (IBAction)actionStartRecording:(id)sender {
    buttonStartRecording.enabled = NO;
    buttonStopRecording.enabled = YES;
    
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            NSLog(@"Permission granted");
        }
        else {
            NSLog(@"Permission denied");
        }
    }];
    // Sound Name
    NSString *soundName = [_pageSound objectAtIndex:activePage];
    
    // Sound PAth
    NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];

    int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];
    NSString* bookRecordedSoundDirectory = [NSString stringWithFormat:@"%d",selectedBookClassification];

    NSString* soundFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",ActiveGrade,bookRecordedSoundDirectory]];
    //Record
    
    
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    
    NSLog(@"#Record Setting");
    recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];
    
    // We can use 44100, 32000, 24000, 16000 or 12000 depending on sound quality
    [recordSetting setValue:[NSNumber numberWithFloat:16000.0] forKey:AVSampleRateKey];
    
    
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    


    
    recorderFilePath = [self createAndStoreFile:soundFilePath withSoundName:soundName];
    
    NSLog(@"#recorderFilePath: %@",recorderFilePath);
    
    NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
    
    err = nil;
    
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if(audioData)
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[url path] error:&err];
    }
    
    err = nil;
    recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
    if(!recorder){
        NSLog(@"#recorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    
    //prepare to record
    [recorder setDelegate:self];
    [recorder prepareToRecord];
    recorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        [cantRecordAlert release];
        return;
    }
    
    // start recording
    [recorder recordForDuration:(NSTimeInterval) 60];

    
}

- (IBAction)actionStopRecording:(id)sender {
    buttonStartRecording.enabled = YES;
    buttonStopRecording.enabled = NO;

    [recorder stop];
    
    AudioSessionSetActive(NO);
}

- (IBAction)actionCloseBook:(id)sender {
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
}
- (void)dealloc {
    [_btnReadForMe release];
    [_btnSilent release];
    [buttonStartRecording release];
    [buttonStopRecording release];
    [buttonClearRecordedSounds release];
    [buttonSendMail release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setBtnReadForMe:nil];
    [self setBtnSilent:nil];
    [super viewDidUnload];
}

-(NSString*) createAndStoreFile:(NSString*) filePath withSoundName:(NSString*) soundName{
    
    NSError *error;
    BOOL isDir = NO;
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath  isDirectory:&isDir]){
        [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:&error];
        NSLog(@"Trying to create doc directory");
    }
    if (!isDir)
    {
        NSLog(@"Cannot proceed!");
        // Throw exception
    }
    recorderFilePath = [[NSString stringWithFormat:@"%@/%@.caf", filePath, soundName] retain];
    return recorderFilePath;
}

#pragma mark - Mail
-(void) mailReportToTeacher{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:@"Dialogue Report"];

        NSString *pinNumber = @"";
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"]) {
            pinNumber = [NSString stringWithFormat:@"Student Dialogue Report\nStudent ID:%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"]];
        }

        [mailViewController setMessageBody:pinNumber isHTML:NO];
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths lastObject];
        int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];
                    NSString *activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.zip",selectedBookClassification]];
        NSData *myData = [NSData dataWithContentsOfFile:dataPath];
        [mailViewController addAttachmentData:myData mimeType:@"application/zip" fileName:[NSString stringWithFormat:@"%d.zip",selectedBookClassification]];
        [self presentModalViewController:mailViewController animated:YES];
        [mailViewController release];
    }
    else {
        NSLog(@"Device is unable to send email in its current state.");
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    if (!error) {

        NSString *activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
        // Remove Zip File
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths lastObject];
        int selectedBookClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedBookClassification"];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.zip",selectedBookClassification]];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if([fileManager fileExistsAtPath:dataPath])
        {
            [fileManager removeItemAtPath:dataPath error:NULL];
        }
        
        //        // Removing Folder
        //        dataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@-%@",activeGrade, pinNumber,folderNAme]];
        //        if([fileManager fileExistsAtPath:dataPath])
        //        {
        //            [fileManager removeItemAtPath:dataPath error:NULL];
        //        }
    }
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Alert
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 10) {
        if (buttonIndex == 0) {
            [self clearAllRecordedSounds];

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"All Records Cleared" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil ];
            [alert show];
            [alert release];

        }
    }
    
}
@end
