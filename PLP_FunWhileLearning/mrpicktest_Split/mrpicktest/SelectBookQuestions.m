//
//  SelectBookQuestions.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 5/3/14.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import "SelectBookQuestions.h"

@implementation SelectBookQuestions

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+(id) createView{
    SelectBookQuestions *view = nil;
    view = [Common loadNibName:@"SelectBookQuestions" owner:self options:nil];
    return view;
}

- (void)dealloc {
    [_btnReadBook release];
    [_btnQuestions release];
    [_btnClose release];
    [super dealloc];
}
@end
