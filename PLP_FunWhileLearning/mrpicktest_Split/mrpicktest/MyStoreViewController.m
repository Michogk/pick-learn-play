//
//  MyStoreViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/30/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MyStoreViewController.h"
#import "BookStoreTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CityMapViewController.h"



@implementation MyStoreViewController 

static NSString* packageRecentlyDownloaded;

- (void)viewDidLoad
{

    [super viewDidLoad];
    [self createBottomsForPackageSelection];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseProcessInProgress) name:@"purchaseStarted" object:nil];

}

-(void) createBottomsForPackageSelection{

    
    
    UIButton *btnkg1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnkg1 addTarget:self 
               action:@selector(loadPackageTableView:)
          forControlEvents:UIControlEventTouchUpInside];
    [btnkg1 setImage:[UIImage imageNamed:@"kg-1_Button"] forState:UIControlStateNormal];
        [btnkg1 setImage:[UIImage imageNamed:@"kg-1_Button"] forState:UIControlStateSelected];
//    [btnkg1 setBackgroundColor:[UIColor greenColor]];
    [btnkg1 setTitle:@"KG - 1" forState:UIControlStateNormal];
    btnkg1.frame = CGRectMake(100, 580, 100, 40);
    btnkg1.tag = 1;
    [self.view addSubview:btnkg1];
    
    UIButton *btnkg2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnkg2 addTarget:self 
               action:@selector(loadPackageTableView:)
     forControlEvents:UIControlEventTouchUpInside];
//    [btnkg2 setBackgroundColor:[UIColor greenColor]];
    [btnkg2 setTitle:@"KG - 2" forState:UIControlStateNormal];
    [btnkg2 setImage:[UIImage imageNamed:@"kg-2_Button"] forState:UIControlStateNormal];
    [btnkg2 setImage:[UIImage imageNamed:@"kg-2_Button"] forState:UIControlStateSelected];
    btnkg2.frame = CGRectMake(240-10, 580, 100, 40);
//    [[btnkg2 layer] setCornerRadius:10];
//    [[btnkg2 layer] setBorderColor:[[UIColor blackColor] CGColor]];
//    [[btnkg2 layer] setBorderWidth:6];
    btnkg2.tag = 2;
    [self.view addSubview:btnkg2];
    
    UIButton *btngrade1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btngrade1 addTarget:self 
               action:@selector(loadPackageTableView:)
     forControlEvents:UIControlEventTouchUpInside];
//    [btngrade1 setBackgroundColor:[UIColor greenColor]];
    [btngrade1 setTitle:@"Grade - 1" forState:UIControlStateNormal];
    btngrade1.frame = CGRectMake(360-10, 580, 100, 40);
    [btngrade1 setImage:[UIImage imageNamed:@"grade-1_Button"] forState:UIControlStateNormal];
    [btngrade1 setImage:[UIImage imageNamed:@"grade-1_Button"] forState:UIControlStateSelected];
    btngrade1.tag = 3;
    [self.view addSubview:btngrade1];
    
    UIButton *btngrade2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btngrade2 addTarget:self 
               action:@selector(loadPackageTableView:)
     forControlEvents:UIControlEventTouchUpInside];
//    [btngrade2 setBackgroundColor:[UIColor greenColor]];
    [btngrade2 setTitle:@"Grade - 2" forState:UIControlStateNormal];
    [btngrade2 setImage:[UIImage imageNamed:@"grade-2_Button"] forState:UIControlStateNormal];
    [btngrade2 setImage:[UIImage imageNamed:@"grade-2_Button"] forState:UIControlStateSelected];
    btngrade2.frame = CGRectMake(480-10, 580, 100, 40);
//    [[btngrade2 layer] setCornerRadius:10];
//    [[btngrade2 layer] setBorderColor:[[UIColor blackColor] CGColor]];
//    [[btngrade2 layer] setBorderWidth:6];
    btngrade2.tag = 4;
    [self.view addSubview:btngrade2];
    
    UIButton *btngrade3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btngrade3 addTarget:self 
               action:@selector(loadPackageTableView:)
     forControlEvents:UIControlEventTouchUpInside];
//    [btngrade3 setBackgroundColor:[UIColor greenColor]];
    [btngrade3 setTitle:@"Grade - 3" forState:UIControlStateNormal];
    btngrade3.frame = CGRectMake(620-10, 580, 100, 40);
    [btngrade3 setImage:[UIImage imageNamed:@"grade-3_Button"] forState:UIControlStateNormal];
    [btngrade3 setImage:[UIImage imageNamed:@"grade-3_Button"] forState:UIControlStateSelected];
    btngrade3.tag = 3;
    [self.view addSubview:btngrade3];
    
    UIButton *btngrade4 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btngrade4 addTarget:self 
               action:@selector(loadPackageTableView:)
     forControlEvents:UIControlEventTouchUpInside];
//    [btngrade4 setBackgroundColor:[UIColor greenColor]];
    [btngrade4 setTitle:@"Grade - 4" forState:UIControlStateNormal];
    btngrade4.frame = CGRectMake(760-10, 580, 100, 40);
    [btngrade4 setImage:[UIImage imageNamed:@"grade-4_Button"] forState:UIControlStateNormal];
    [btngrade4 setImage:[UIImage imageNamed:@"grade-4_Button"] forState:UIControlStateSelected];
    btngrade4.tag = 3;
    [self.view addSubview:btngrade4];

}

-(void) loadPackageTableView:(id) sender{
    NSString *gradeIsSelected = @"";
    NSString *activePackage;
    NSString *caller = [NSString stringWithFormat:@"%@",sender];
    if ([caller isEqualToString:@"FREE"]) {
    activePackage = @"FREE";
    }
    else {
        activePackage = [sender currentTitle];
    }


    if ([activePackage isEqualToString:@"FREE"]) {
        gradeIsSelected = @"free";
    }
    if ([activePackage isEqualToString:@"KG - 1"]) {
        gradeIsSelected = @"KG1";
    }
   if ([activePackage isEqualToString:@"KG - 2"]) {
        gradeIsSelected = @"KG2";
    }
    if ([activePackage isEqualToString:@"Grade - 1"]) {
        gradeIsSelected = @"G1";
    }   
   if ([activePackage isEqualToString:@"Grade - 2"]) {
        gradeIsSelected = @"G2";
    }   
   if ([activePackage isEqualToString:@"Grade - 3"]) {
        gradeIsSelected = @"G3";
    }    
    if ([activePackage isEqualToString:@"Grade - 4"]) {
        gradeIsSelected = @"G4";
    } 
    
    [[NSUserDefaults standardUserDefaults] setObject:gradeIsSelected forKey:@"activePackage"];
    if ([self.view viewWithTag:400]) {
        [[self.view viewWithTag:400] removeFromSuperview];
//        [[self.view viewWithTag:400] release];;
    }
    bookStoreTVC = [[BookStoreTableViewController alloc] init];
    bookStoreTVC.view.transform = CGAffineTransformMakeRotation(-M_PI_2);
    bookStoreTVC.view.frame = CGRectMake(0,190, bookStoreTVC.view.frame.size.width+250, bookStoreTVC.view.frame.size.height-400);
    [bookStoreTVC.view setTag:400];
    
    [self.view addSubview:bookStoreTVC.view];

}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"mybookstore" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void) purchaseProcessInProgress{
    NSLog(@"Creating purchase Process background animation");
    [self characterWalkingAnimation];
    UIView *purchaseProcessWaitingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    [purchaseProcessWaitingView setBackgroundColor:[UIColor clearColor]];
    [purchaseProcessWaitingView setTag:100];
    [self.view addSubview:purchaseProcessWaitingView];
    
    
    UILabel *lblWaitPlease = [[UILabel alloc] initWithFrame:CGRectMake(160,445 + 95,800,200)];
    lblWaitPlease.textAlignment = UITextAlignmentCenter;
    [lblWaitPlease setBackgroundColor:[UIColor clearColor]];
//    [lblWaitPlease setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:25]];
    UIFont *font = [UIFont systemFontOfSize: 30];
    [lblWaitPlease setFont:font];
    lblWaitPlease.textColor = [UIColor whiteColor];
    lblWaitPlease.lineBreakMode = UILineBreakModeWordWrap;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if(activeLanguage == 2){
        lblWaitPlease.text = @"Please Wait.. Downloading In Progress....";
    }
    else{
        lblWaitPlease.text = @"قيد التحميل";
    }
    [purchaseProcessWaitingView addSubview:lblWaitPlease]; 
    [lblWaitPlease release];
    
    UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    av.frame = CGRectMake(500,352,50,50);
    av.tag  = 102;
    [purchaseProcessWaitingView addSubview:av];    
    [av startAnimating];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseProcessDone) name:@"purchaseCompletedCallBack" object:nil];
    NSLog(@"Adding observer PURCHASE COMPLETE CALLBACK");
}

-(void) purchaseProcessDone{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"Acknowledgement Of Purchase Process Completed");
    if(characterWalkingTimer){
        [characterWalkingTimer invalidate];
        [characterWalkingTimer release];
    }
    
    if (characterWalkingAnimationImgView) {
        [characterWalkingAnimationImgView removeFromSuperview];
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
    }
    

    
    if ([self.view viewWithTag:100]) {
        if([[self.view viewWithTag:100] viewWithTag:102]){
            [[[self.view viewWithTag:100] viewWithTag:102] removeFromSuperview];    

        }
    [[self.view viewWithTag:100] removeFromSuperview];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchaseProcessInProgress) name:@"purchaseStarted" object:nil];
    NSString *photoCheck = [[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"];
    
    if ([photoCheck isEqualToString:@"PHOTOBOUGHT"]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PHOTOFEATURE"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        
            downlaodingStatusAlert = [[UIAlertView alloc] init];
            [downlaodingStatusAlert setTitle:@"CONGRATULATIONS!"];
            [downlaodingStatusAlert setMessage:@"Photo Feature has been activated for My Home in all grades"];
            [downlaodingStatusAlert addButtonWithTitle:@"OK"];            
            [downlaodingStatusAlert setDelegate:self];
            [downlaodingStatusAlert show];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"downloadingStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"activeSelectionInBookStore"] forKey:@"activePackage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"PINNUMBER"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PINNEDDEVICE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        return;
    }
    
    int downloadingStatus =  [[[NSUserDefaults standardUserDefaults] objectForKey:@"downloadingStatus"] intValue];
    //downloadingStatus == 1 means successfully downloaed. downloadingStatus 2 means no network available. downloadingStatus 0 means downloading failed            
    packageRecentlyDownloaded = [[NSUserDefaults standardUserDefaults] objectForKey:@"gradeIsSelected"];
    int  activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (downloadingStatus == 1) {
 
        downlaodingStatusAlert = [[UIAlertView alloc] initWithTitle:@"CONGRATULATIONS" message:@"You Have Successfully Purchased Package. Please go to Main Menu to enjoy new locations " delegate:self cancelButtonTitle:@"City Map" otherButtonTitles:Nil, nil];
        [downlaodingStatusAlert setDelegate:self];
        [downlaodingStatusAlert setTag:23];
        
//        if ([packageRecentlyDownloaded isEqualToString:@"G1SKILLS"] || [packageRecentlyDownloaded isEqualToString:@"G2SKILLS"] || [packageRecentlyDownloaded isEqualToString:@"G3SKILLS"] || [packageRecentlyDownloaded isEqualToString:@"G4SKILLS"]/* || [packageRecentlyDownloaded isEqualToString:@"FREE"] */) {
//            UIAlertView *alert = [[UIAlertView alloc] init];
//            if (activeLanguage == 2) {
//                [alert setTitle:@"CONGRATULATIONS"]; 
//                [alert setMessage:@"Content Successfully downloaded"];
//                [alert addButtonWithTitle:@"OK"];
//            }
//            else {
//                [alert setTitle:@"انجار جيد"]; 
//                [alert setMessage:@" Content Successfully downloaded "]; 
//                [alert addButtonWithTitle:@"موافق"]; 
//            }
//            [alert setDelegate:self];
////            [alert setTag:110];
//            [alert show];
//            [alert release];
//        }
//        else {
                [downlaodingStatusAlert show];
//        }
        
        if (![packageRecentlyDownloaded isEqualToString:@"free"] ) {
            NSString* gradesPurchased = [[NSUserDefaults standardUserDefaults] objectForKey:@"gradesPurchased"];            
            
            NSArray * componentsArr = [[gradesPurchased componentsSeparatedByString: @" "] retain]; 
            BOOL packageHasBeenDownloadedBefore = FALSE;
            for (int i = 0; i < componentsArr.count; i++) {
                if ([[componentsArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@",packageRecentlyDownloaded]]) {
                    packageHasBeenDownloadedBefore = TRUE;
                }
            }
            [componentsArr release];
            componentsArr = Nil;
            if (!packageHasBeenDownloadedBefore) {
                NSString* package = packageRecentlyDownloaded;
                NSString* updatedPurchasedRecord = [gradesPurchased stringByAppendingFormat:@" %@",package]; 
                [[NSUserDefaults standardUserDefaults] setValue:updatedPurchasedRecord forKey:@"gradesPurchased"];
                [[NSUserDefaults standardUserDefaults] synchronize];   
            }
        }
        else {
            [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"freeContentDownloaded"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        NSString* packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"AlreadyDownloadedPackage"];
        if (!packagesDownloadedBefore || packagesDownloadedBefore == Nil || packagesDownloadedBefore == @"<nil>") {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"AlreadyDownloadedPackage"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"AlreadyDownloadedPackage"];
        }
        NSString* updatedPackageDownloadedStatus = [packagesDownloadedBefore stringByAppendingFormat:@" %@",packageRecentlyDownloaded];
        [[NSUserDefaults standardUserDefaults] setValue:updatedPackageDownloadedStatus forKey:@"AlreadyDownloadedPackage"];
        [[NSUserDefaults standardUserDefaults] synchronize];            
        
        // changing default value to 4 now
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"downloadingStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else if (downloadingStatus == 2) {
        downlaodingStatusAlert = [[UIAlertView alloc] init];
        if (activeLanguage == 2) {
            [downlaodingStatusAlert setTitle:@"Network Error!"]; 
            [downlaodingStatusAlert setMessage:@"Please Check your internet connection and try again later"]; 
            [downlaodingStatusAlert addButtonWithTitle:@"OK"];
        }
        else {
            [downlaodingStatusAlert setTitle:@"Network Error!"]; 
            [downlaodingStatusAlert setMessage:@"الرجاء التأكد من الانترنت والمحاوله مرة اخرى "]; 
            [downlaodingStatusAlert addButtonWithTitle:@"خروج"]; 
        }
        [downlaodingStatusAlert setDelegate:self];
        [downlaodingStatusAlert show];
    }
    

    
    else if(downloadingStatus == 4){
        downlaodingStatusAlert = [[UIAlertView alloc] init];
        if (activeLanguage == 2) {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"] isEqualToString:@"PHOTOBUYING"] ) {
                [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"activeSelectionInBookStore"] forKey:@"activePackage"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }              
            [downlaodingStatusAlert setTitle:@"Oops!"]; 
            [downlaodingStatusAlert setMessage:@"Package Purchasing Failed. Please Try Again Later"];
            [downlaodingStatusAlert addButtonWithTitle:@"OK"];
        }
        else {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"] isEqualToString:@"PHOTOBUYING"] ) {
                [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"activeSelectionInBookStore"] forKey:@"activePackage"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [downlaodingStatusAlert setTitle:@"Network Error!"]; 
            [downlaodingStatusAlert setMessage:@"شراء البيانات فشل . الرجاء المحاوله مره اخرى  "];
            [downlaodingStatusAlert addButtonWithTitle:@"خروج"]; 
        }

        [downlaodingStatusAlert setDelegate:self];
        [downlaodingStatusAlert show];
        return;
        
    }
    else {
        downlaodingStatusAlert = [[UIAlertView alloc] init];
        if (activeLanguage == 2) {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"] isEqualToString:@"PHOTOBUYING"] ) {
                [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"activeSelectionInBookStore"] forKey:@"activePackage"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [downlaodingStatusAlert setTitle:@"Oops!"];
            [downlaodingStatusAlert setMessage:@"Package Purchasing Failed. Please Try Again Later"];
            [downlaodingStatusAlert addButtonWithTitle:@"Exit"];
        }
        else {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"activePackage"] isEqualToString:@"PHOTOBUYING"] ) {
                [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"activeSelectionInBookStore"] forKey:@"activePackage"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            [downlaodingStatusAlert setTitle:@"Network Error!"]; 
            [downlaodingStatusAlert setMessage:@"شراء البيانات فشل . الرجاء المحاوله مره اخرى  "];
            [downlaodingStatusAlert addButtonWithTitle:@"خروج"]; 
        }
        [downlaodingStatusAlert setDelegate:self];
        [downlaodingStatusAlert show];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 23) {
        if ([packageRecentlyDownloaded isEqualToString:@"KG1"] || [packageRecentlyDownloaded isEqualToString:@"KG2"] ) {
            [[NSUserDefaults standardUserDefaults] setValue:@"kg" forKey:@"mapToLoad"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if ([packageRecentlyDownloaded isEqualToString:@"FREE"] || [packageRecentlyDownloaded isEqualToString:@"free"] ) {
            [[NSUserDefaults standardUserDefaults] setValue:@"free" forKey:@"mapToLoad"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if ([packageRecentlyDownloaded isEqualToString:@"G1"]) {
            [[NSUserDefaults standardUserDefaults] setValue:@"G1" forKey:@"mapToLoad"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if ([packageRecentlyDownloaded isEqualToString:@"G2"] || [packageRecentlyDownloaded isEqualToString:@"G3"] || [packageRecentlyDownloaded isEqualToString:@"G4"]) {
            [[NSUserDefaults standardUserDefaults] setValue:@"gx" forKey:@"mapToLoad"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        if ([packageRecentlyDownloaded isEqualToString:@"free"]) {
            packageRecentlyDownloaded = @"FREE";
        }
        

        
        [[NSUserDefaults standardUserDefaults] setObject:packageRecentlyDownloaded forKey:@"activeGrade"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if ([packageRecentlyDownloaded isEqualToString:@"G1SKILLS"] || [packageRecentlyDownloaded isEqualToString:@"G2SKILLS"] || [packageRecentlyDownloaded isEqualToString:@"G3SKILLS"] || [packageRecentlyDownloaded isEqualToString:@"G4SKILLS"]) {
            
            NSString *extractString = [packageRecentlyDownloaded substringToIndex:2];
            //            NSLog(@"sadasfdasfdsf %@",extractString);
            [[NSUserDefaults standardUserDefaults] setObject:extractString forKey:@"activeGrade"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if ([extractString isEqualToString:@"KG1"] || [extractString isEqualToString:@"KG2"] ) {
                [[NSUserDefaults standardUserDefaults] setValue:@"kg" forKey:@"mapToLoad"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            if ([extractString isEqualToString:@"FREE"] || [extractString isEqualToString:@"free"] ) {
                [[NSUserDefaults standardUserDefaults] setValue:@"free" forKey:@"mapToLoad"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            if ([extractString isEqualToString:@"G1"]) {
                [[NSUserDefaults standardUserDefaults] setValue:@"G1" forKey:@"mapToLoad"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            if ([extractString isEqualToString:@"G2"] || [extractString isEqualToString:@"G3"] || [extractString isEqualToString:@"G4"]) {
                [[NSUserDefaults standardUserDefaults] setValue:@"gx" forKey:@"mapToLoad"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }

            
            
        }
        
        
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentGrammarRule"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentPrincipleRule"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentGrammarString"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentPrincipleString"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CityMapViewController *vc = [sb instantiateViewControllerWithIdentifier:@"CityMapViewControllerIdentifier"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:YES completion:NULL];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
    }
                
    
//    [self dismissModalViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retnained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)HelpSound:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"MS008" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)exitMyStore:(id)sender {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if (downlaodingStatusAlert) {
        [downlaodingStatusAlert release];
        downlaodingStatusAlert = nil;
    }
}

-(void) characterWalkingAnimation{
    
    if (characterWalkingAnimationImgView) {
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
        
        
    }
    characterAnimationStep = 0;
    animationImageNumber = 1;
    animationImageDirection = @"R";
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        NSString* animationImg = [NSString stringWithFormat:@"Walking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        NSString* animationImg = [NSString stringWithFormat:@"GWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        NSString* animationImg = [NSString stringWithFormat:@"BBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        NSString* animationImg = [NSString stringWithFormat:@"BGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        NSString* animationImg = [NSString stringWithFormat:@"DBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
  
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        NSString* animationImg = [NSString stringWithFormat:@"DGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        NSString* animationImg = [NSString stringWithFormat:@"CBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        NSString* animationImg = [NSString stringWithFormat:@"CGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FireBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FireGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        NSString* animationImg = [NSString stringWithFormat:@"ABWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        NSString* animationImg = [NSString stringWithFormat:@"AGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        NSString* animationImg = [NSString stringWithFormat:@"PBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        NSString* animationImg = [NSString stringWithFormat:@"PGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    [characterWalkingAnimationImgView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:characterWalkingAnimationImgView];
    [self.view bringSubviewToFront:characterWalkingAnimationImgView];
    //    [characterWalkingAnimationImgView bringSubviewToFront:characterBtn];
    [characterWalkingAnimationImgView setUserInteractionEnabled:YES];
    
    
    
    characterWalkingTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1
                                                              target:self
                                                            selector:@selector(updateCharacterStep)
                                                            userInfo:nil
                                                             repeats:YES]retain];
    
    
}

-(void) updateCharacterStep{
    
    animationImageNumber = animationImageNumber + 1;
    if (animationImageNumber == 8) {
        animationImageNumber = 1;
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        animationImageGender = @"";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        animationImageGender = @"G";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        animationImageGender = @"BB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        animationImageGender = @"BG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        animationImageGender = @"DB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        animationImageGender = @"DG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        animationImageGender = @"CB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        animationImageGender = @"CG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        animationImageGender = @"FireB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        animationImageGender = @"FireG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        animationImageGender = @"FB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        animationImageGender = @"FG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        animationImageGender = @"AB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        animationImageGender = @"AG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        animationImageGender = @"PB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        animationImageGender = @"PG";
    }

    NSString* animationImg = [NSString stringWithFormat:@"%@Walking_%@%d.png",animationImageGender,animationImageDirection,animationImageNumber];
    [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    
    if ([animationImageDirection isEqualToString:@"R"]) {
        characterAnimationStep = characterAnimationStep + 8;
    }
    else {
        characterAnimationStep = characterAnimationStep - 8;
    }
    
    CGRect frame = characterWalkingAnimationImgView.frame;
    frame.origin.x =  characterAnimationStep;
    characterWalkingAnimationImgView.frame = frame;
    
    if (characterAnimationStep > 900) {
        animationImageDirection = @"L";
    }
    if (characterAnimationStep < 20) {
        animationImageDirection = @"R";
    }
    
}

@end
