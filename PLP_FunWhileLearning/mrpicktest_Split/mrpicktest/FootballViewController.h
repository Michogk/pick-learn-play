//
//  FootballViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 8/7/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>

@interface FootballViewController : UIViewController<AVAudioPlayerDelegate,ADBannerViewDelegate>{
NSMutableDictionary* tempContentDicCorrect;
AVAudioPlayer *player;
AVAudioPlayer *ballPlayer;
int cId;
int activeLanguage;
NSString* ActiveGrade;
int wordsCount;
AVAudioPlayer *mySpinItPlayer;
NSTimer* cheeringSoundTimer;
NSTimer* fireTimerToPlayCheeringCharacter;
NSTimer* fireTimerToPlayEncouraging;
int sessionCounter;
int correctWordCounter;
UILabel *_wordCounterlbl;
UILabel *_lblLevel1;
UILabel *_lblLevel2;
int currentWord;
    NSTimer *ballRemovalTimer;
    
    NSTimer *ballHitTimer;
    NSTimer *loadNextBallTimer;
    
int level;
    float x2;
    float y2;
    NSString *hitSide;
    
    BOOL startingTouchPointCorrectionCheck;
    BOOL touchMovedCorrectionCheck;

// Report Data
NSMutableArray *retval;
int reportWordId;
NSString *reportWordDesc;
int reportCorrectCount;
int reportTotatCount;
            int dictationaryCount;
}
@property (retain, nonatomic) IBOutlet UILabel *lblRightGoal;
@property (retain, nonatomic) IBOutlet UILabel *lblLeftGoal;
@property (retain, nonatomic) IBOutlet UIButton *btnLeft;
@property (retain, nonatomic) IBOutlet UIButton *btnRight;
@property (retain, nonatomic) IBOutlet UIImageView *imgBALL;

- (IBAction)btnLeftPressed:(id)sender;
- (IBAction)btnRightPressed:(id)sender;
- (IBAction)repeatSound:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnRepeatSound;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewCheering;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (strong, nonatomic) ADBannerView *bannerView;
- (IBAction)gotoMainMenu:(id)sender;
@property (retain, nonatomic) IBOutlet UILabel *lblSynonym;
@property (retain, nonatomic) IBOutlet UILabel *lblOpposite;


@end
