//
//  MyAchievmentViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MyAchievmentViewController.h"
#import "MyRewardsViewController.h"
#import "MovieViewController.h"
#import "MrPDatabase.h"
#import "MrPReport.h"
#import "PDFRenderer.h"
#import "MrPContentInfo.h"
#import "cocos2d.h"

#define FIXIT 1
#define SPIN 2
#define POPIT 3
#define PICKIT 4
#define MOVEIT 5
#define MATCHIT 6
#define TYPEIT 7

@interface MyAchievmentViewController ()

@end

@implementation MyAchievmentViewController
@synthesize BtnRewards;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    
//    Game1 = @"Pop It";
//    Game2 = @"Fix It";
//    Game3 = @"Type It";
//    Game4 = @"Match It";
    
    
    NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
    int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount]; 
    
    NSString *correctWordsCount = [NSString stringWithFormat:@"correctWordsCount"];
    int correctWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:correctWordsCount];

    lblCorrectWords.text = [NSString stringWithFormat:@"%d/%d",correctWordsLastCount, totalWordsLastCount];   
    
    float totalPoints = 0;
    for (int i = 1; i < 9; i ++) {
        NSString *PointsLevel1 = [NSString stringWithFormat:@"Game%dLevel%d",i,1];
        float level1Points = [[NSUserDefaults standardUserDefaults] integerForKey:PointsLevel1];     

        NSString *PointsLevel2 = [NSString stringWithFormat:@"Game%dLevel%d",i,2];
        float level2Points = [[NSUserDefaults standardUserDefaults] integerForKey:PointsLevel2]; 
        
        totalPoints = totalPoints + level1Points  + level2Points;
    }
    
    
    int alreadyDownloadedRewardsCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"rewardsDownloadedCount"];
            int alreadyPurchasedCostumersCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"CostumePurchasedCount"];
    totalPoints = totalPoints - (alreadyDownloadedRewardsCount * 150) - (alreadyPurchasedCostumersCount * 1000);

    lblTotalWords.text = [NSString stringWithFormat:@"%.1f",totalPoints];
    float ratio;
    int average;
    if (totalWordsLastCount == 0) {
        average = 0;
    }
    else {
        ratio = (float) correctWordsLastCount/totalWordsLastCount;
        average = ratio * 100;    
    }

    
    
    
    lblAverage.text = [NSString stringWithFormat:@"%d%%",average];
    
    
}

-(void) playHelpMovie{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == NO) {
        return;
    }
    NSString *movieName = @"Achievements";
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
        return;
    }
    
    MovieViewController* moviePlayer = [[MovieViewController alloc] init];
    [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayer animated:NO];
    // Prep and play the movie
//    NSString *movieName = @"Achievements";
    [moviePlayer playMovie:movieName];
    
}

-(void) viewDidAppear:(BOOL)animated{
    if (!showMovie) {
//        [self playHelpMovie];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    showMovie = TRUE;
}

- (void)viewDidUnload
{
    [lblCorrectWords release];
    lblCorrectWords = nil;
    [lblTotalWords release];
    lblTotalWords = nil;
    [lblAverage release];
    lblAverage = nil;
    [self setBtnRewards:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *kCellIdentifier = @"MyGame";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil)
    {
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSString *PointsLevel1 = [NSString stringWithFormat:@"Game%dLevel%d",indexPath.row + 1,1];
    float level1Points = [[NSUserDefaults standardUserDefaults] integerForKey:PointsLevel1];
    
    
    NSString *PointsLevel2 = [NSString stringWithFormat:@"Game%dLevel%d",indexPath.row + 1,2];
    float level2Points = [[NSUserDefaults standardUserDefaults] integerForKey:PointsLevel2];
    
    Game1 = [NSString stringWithFormat:@"  Pop It                                      %.1f                                                    %.1f",level1Points,level2Points];
    Game2 = [NSString stringWithFormat:@"  Fix It                                       %.1f                                                     %.1f",level1Points,level2Points];
    Game3 = [NSString stringWithFormat:@"  Type It                                    %.1f                                                     %.1f",level1Points,level2Points];
    Game4 = [NSString stringWithFormat:@"  Match It                                  %.1f                                                     %.1f",level1Points,level2Points];
    
    Game5 = [NSString stringWithFormat:@"  Spin It                                      %.1f                                                    %.1f",level1Points,level2Points];
    Game6 = [NSString stringWithFormat:@"  Move It                                    %.1f                                                     %.1f",level1Points,level2Points];
    Game7 = [NSString stringWithFormat:@"  Pick It                                     %.1f                                                     %.1f",level1Points,level2Points];
        Game8 = [NSString stringWithFormat:@"  Kick it                                     %.1f                                                     %.1f",level1Points,level2Points];
        Game9 = [NSString stringWithFormat:@"  Hit it                                     %.1f                                                     %.1f",level1Points,level2Points];
        Game10 = [NSString stringWithFormat:@"  Shoot it                                     %.1f                                                     %.1f",level1Points,level2Points];
    
    if (indexPath.row == 0) {
        cell.textLabel.text = Game1;
    }
    else 
    if (indexPath.row == 1) {
        cell.textLabel.text = Game2;
    }
    else 
    if (indexPath.row == 2) {
        cell.textLabel.text = Game3;
    }
    else 
    if (indexPath.row == 3) {
        cell.textLabel.text = Game4;
    }
    if (indexPath.row == 4) {
        cell.textLabel.text = Game5;
    }
    if (indexPath.row == 5) {
        cell.textLabel.text = Game6;
    }
    if (indexPath.row == 6) {
        cell.textLabel.text = Game7;
    }
    
    if (indexPath.row == 7) {
        cell.textLabel.text = Game8;
    }
    if (indexPath.row == 8) {
        cell.textLabel.text = Game9;
    }
    
    if (indexPath.row == 9) {
        cell.textLabel.text = Game10;
    }

    
    return cell;
}

- (IBAction)HelpSound:(id)sender {
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"MS009" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)dealloc {
    [lblCorrectWords release];
    [lblTotalWords release];
    [lblAverage release];
    [BtnRewards release];
    [super dealloc];
}
- (IBAction)RewardBtnPressed:(id)sender {
    
      
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        MyRewardsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"rewardVCIdentifier"];
        vc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self presentViewController:vc animated:NO completion:NULL]; 

}

-(void) runSpinner{
//    UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
//    av.frame = CGRectMake(500,352,50,50);
//    av.tag  = 102;
//    [self.view addSubview:av];
//
//    [av startAnimating];

    UIView *purchaseProcessWaitingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    [purchaseProcessWaitingView setBackgroundColor:[UIColor clearColor]];
    [purchaseProcessWaitingView setTag:100];
    [self.view addSubview:purchaseProcessWaitingView];
    
    
    UILabel *lblWaitPlease = [[UILabel alloc] initWithFrame:CGRectMake(140,490,800,200)];
    lblWaitPlease.textAlignment = UITextAlignmentCenter;
    [lblWaitPlease setBackgroundColor:[UIColor clearColor]];
    //    [lblWaitPlease setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:25]];
    UIFont *font = [UIFont systemFontOfSize: 30];
    [lblWaitPlease setFont:font];
    lblWaitPlease.textColor = [UIColor blackColor];
    lblWaitPlease.lineBreakMode = UILineBreakModeWordWrap;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if(activeLanguage == 2){
        lblWaitPlease.text = @"Please Wait.. Preparing Report....";
    }
    else{
        lblWaitPlease.text = @"قيد التحميل";
    }
    [purchaseProcessWaitingView addSubview:lblWaitPlease];
    [lblWaitPlease release];
    
    [self performSelector:@selector(generateReportFrmDatabase) withObject:nil afterDelay:0.2];
    

    
    
    
//    UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
//    av.frame = CGRectMake(500,352,50,50);
//    av.tag  = 102;
//    [purchaseProcessWaitingView addSubview:av];
//    [av startAnimating];

    
    
}

-(void) generateReportFrmDatabase{
    [self generateReport];
    if (clearPrevData) {
        [self clearPreviousRecords];
        clearPrevData = NO;
    }
    clearPrevData = NO;


}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {

    if (alertView.tag == 29) {
        if (buttonIndex == 0)
        {
            clearPrevData = YES;
            [self performSelector:@selector(runSpinner) withObject:nil afterDelay:0.2];
            
            //            [self characterWalkingAnimation];
//            [alertView dismissWithClickedButtonIndex:0 animated:YES];
            
//            [self generateReport];
//            [self clearPreviousRecords];
            
        }
        if (buttonIndex == 1)
        {
            
            clearPrevData = NO;
            [self performSelector:@selector(runSpinner) withObject:nil afterDelay:0.2];
//            [self performSelector:@selector(runSpinner) withObject:nil afterDelay:0];
            //            [self characterWalkingAnimation];
//            [alertView dismissWithClickedButtonIndex:0 animated:YES];
//            [self generateReport];
        }
        
        if (buttonIndex == 1)
        {
            [alertView dismissWithClickedButtonIndex:0 animated:YES];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (alertView.tag == 29) {
//        if (buttonIndex == 0)
//        {            
////            [self characterWalkingAnimation];
//            [alertView dismissWithClickedButtonIndex:0 animated:YES];
//            
//            
//            
//            
//            [self generateReport];
//            [self clearPreviousRecords];
//        
//        }
//        if (buttonIndex == 1)
//        {        
////            [self characterWalkingAnimation];
//            [alertView dismissWithClickedButtonIndex:0 animated:YES];
//            [self generateReport];
//        }
//        
//        if (buttonIndex == 1)
//        {
//            [alertView dismissWithClickedButtonIndex:0 animated:YES];
//        }
//    }
}

-(void) clearPreviousRecords{
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database clearReportRecords];

}

-(void) generateReport{
    
//    if ([self.view viewWithTag:29]) {
//        [[self.view viewWithTag:29] removeFromSuperview];
//        [[self.view viewWithTag:29] release];
//    }
    pageSize = CGSizeMake(612, 792);
    NSString *fileName = @"Report.pdf";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pdfFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    [self createReportDictationary];
    //    [self generatePdfWithFilePath:pdfFileName];
    
    //    [self createReportDictationary];
    
    [self mailIt];
}

-(void) characterWalkingAnimation{
    
    if (characterWalkingAnimationImgView) {
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
        
        
    }
    characterAnimationStep = 0;
    animationImageNumber = 1;
    animationImageDirection = @"R";
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        NSString* animationImg = [NSString stringWithFormat:@"Walking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        NSString* animationImg = [NSString stringWithFormat:@"GWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        NSString* animationImg = [NSString stringWithFormat:@"BBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        NSString* animationImg = [NSString stringWithFormat:@"BGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        NSString* animationImg = [NSString stringWithFormat:@"DBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        NSString* animationImg = [NSString stringWithFormat:@"DGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        NSString* animationImg = [NSString stringWithFormat:@"CBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        NSString* animationImg = [NSString stringWithFormat:@"CGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FireBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FireGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        NSString* animationImg = [NSString stringWithFormat:@"FGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        NSString* animationImg = [NSString stringWithFormat:@"ABWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        NSString* animationImg = [NSString stringWithFormat:@"AGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        NSString* animationImg = [NSString stringWithFormat:@"PBWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        NSString* animationImg = [NSString stringWithFormat:@"PGWalking_%@%d.png",animationImageDirection,animationImageNumber];
        characterWalkingAnimationImgView = [[UIImageView alloc] initWithFrame:CGRectMake( characterAnimationStep , 173+350+120, 120, 120)];
        [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    }

    
    [characterWalkingAnimationImgView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:characterWalkingAnimationImgView];
    [self.view bringSubviewToFront:characterWalkingAnimationImgView];
    //    [characterWalkingAnimationImgView bringSubviewToFront:characterBtn];
    [characterWalkingAnimationImgView setUserInteractionEnabled:YES];
    
    
    
    characterWalkingTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1
                                                              target:self
                                                            selector:@selector(updateCharacterStep)
                                                            userInfo:nil
                                                             repeats:YES]retain];
    
    
}

-(void) updateCharacterStep{
    
    animationImageNumber = animationImageNumber + 1;
    if (animationImageNumber == 8) {
        animationImageNumber = 1;
    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        animationImageGender = @"";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        animationImageGender = @"G";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        animationImageGender = @"BB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        animationImageGender = @"BG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        animationImageGender = @"DB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        animationImageGender = @"DG";
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        animationImageGender = @"CB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        animationImageGender = @"CG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        animationImageGender = @"FireB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        animationImageGender = @"FireG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        animationImageGender = @"FB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        animationImageGender = @"FG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        animationImageGender = @"AB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        animationImageGender = @"AG";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        animationImageGender = @"PB";
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        animationImageGender = @"PG";
    }
    NSString* animationImg = [NSString stringWithFormat:@"%@Walking_%@%d.png",animationImageGender,animationImageDirection,animationImageNumber];
    [characterWalkingAnimationImgView setImage:[UIImage imageNamed:animationImg]];
    
    if ([animationImageDirection isEqualToString:@"R"]) {
        characterAnimationStep = characterAnimationStep + 8;
    }
    else {
        characterAnimationStep = characterAnimationStep - 8;
    }
    
    CGRect frame = characterWalkingAnimationImgView.frame;
    frame.origin.x =  characterAnimationStep;
    characterWalkingAnimationImgView.frame = frame;
    
    if (characterAnimationStep > 900) {
        animationImageDirection = @"L";
    }
    if (characterAnimationStep < 20) {
        animationImageDirection = @"R";
    }
    
}

-(void) loadMyHomeData{
    
    if(characterWalkingTimer){
        [characterWalkingTimer invalidate];
        [characterWalkingTimer release];
        characterWalkingTimer = nil;
    }
    
    if (characterWalkingAnimationImgView) {
        [characterWalkingAnimationImgView removeFromSuperview];
        [characterWalkingAnimationImgView release];
        characterWalkingAnimationImgView = nil;
    }
    
    
    
    if ([self.view viewWithTag:100]) {
//        if([[self.view viewWithTag:100] viewWithTag:102]){
//            [[[self.view viewWithTag:100] viewWithTag:102] removeFromSuperview];
//            
//        }
        [[self.view viewWithTag:100] removeFromSuperview];
    }
    
//    ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"myachievements" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)generatePDFMail:(id)sender {
    
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    [alert setTitle:@"Generating Report"];
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if(activeLanguage == 2){
        [alert setMessage:@"(please insure your email account is active). Wait untill your status report is compiled. This may take several seconds. Do you like to reset the report's data?"];

    }
    else{
        [alert setMessage:@"(الرجاء التأكد أن البريد الإلكتروني فعال)  هل تود الغاء بيانات التقرير؟"];
    }
    
     
    [alert setDelegate:self];
    [alert addButtonWithTitle:@"YES"];
    [alert addButtonWithTitle:@"NO"];
    [alert addButtonWithTitle:@"Exit"];
    alert.tag = 29;
    [alert show];
    alert.delegate = self;
    [alert release];
}

-(void) createReportDictationary{
    
    MrPDatabase *database = [[MrPDatabase alloc] init];

    NSArray *arrAllClassifications = [database ReadClassificationTable];
    NSMutableDictionary *classificationDic = [[NSMutableDictionary alloc] init];
    int counting = 0;
    for (MrPContentInfo *info in arrAllClassifications) {

        [classificationDic setObject:[NSString stringWithFormat:@"%d",info.folderid] forKey:[NSString stringWithFormat:@"folderId%d",counting]];
        [classificationDic setObject:info.foldername forKey:[NSString stringWithFormat:@"folderName%d",info.folderid]];
        counting = counting + 1;
        
    }
    
    
    
    NSArray* retVal = [[NSArray alloc] init];
    retVal = [database getReportData];
    int position = 0;
    tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[retVal count]];
    for (MrPReport *info in retVal) {
        if (!info.wordDescription || info.wordDescription == NULL ) {
            
        }
        else{
        
        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [tempContentDic setObject:info.wordDescription forKey:[NSString stringWithFormat:@"WordDesc%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.PINNUMBER] forKey:[NSString stringWithFormat:@"PINNUMBER%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersFixIt] forKey:[NSString stringWithFormat:@"totalAnswersFixIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectFixIt] forKey:[NSString stringWithFormat:@"noOfCorrectFixIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersSpinIt] forKey:[NSString stringWithFormat:@"totalAnswersSpinIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectSpinIt] forKey:[NSString stringWithFormat:@"noOfCorrectSpinIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersPopIt] forKey:[NSString stringWithFormat:@"totalAnswersPopIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectPopIt] forKey:[NSString stringWithFormat:@"noOfCorrectPopIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersPickIt] forKey:[NSString stringWithFormat:@"totalAnswersPickIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectPickIt] forKey:[NSString stringWithFormat:@"noOfCorrectPickIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersMoveIt] forKey:[NSString stringWithFormat:@"totalAnswersMoveIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectMoveIt] forKey:[NSString stringWithFormat:@"noOfCorrectMoveIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersMatchIt] forKey:[NSString stringWithFormat:@"totalAnswersMatchIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectMatchIt] forKey:[NSString stringWithFormat:@"noOfCorrectMatchIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersTypeIt] forKey:[NSString stringWithFormat:@"totalAnswersTypeIt%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectTypeIt] forKey:[NSString stringWithFormat:@"noOfCorrectTypeIt%d",position]];
            
            [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersFootball] forKey:[NSString stringWithFormat:@"totalAnswersFootball%d",position]];
            [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectFootball] forKey:[NSString stringWithFormat:@"noOfCorrectFootball%d",position]];
            
            [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersHockey] forKey:[NSString stringWithFormat:@"totalAnswersHockey%d",position]];
            [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectHockey] forKey:[NSString stringWithFormat:@"noOfCorrectHockey%d",position]];
            
            [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswersBasketBall] forKey:[NSString stringWithFormat:@"totalAnswersBasketball%d",position]];
            [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrectBasketBall] forKey:[NSString stringWithFormat:@"noOfCorrectBasketBall%d",position]];
            
        [tempContentDic setObject:[NSNumber numberWithInt:info.classification] forKey:[NSString stringWithFormat:@"classification%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.activelanguage] forKey:[NSString stringWithFormat:@"activeLanguage%d",position]];
        [tempContentDic setObject:[NSNumber numberWithInt:info.gameId] forKey:[NSString stringWithFormat:@"gameId%d",position]];

            NSLog(@"info.PINNYMBER is %d",info.PINNUMBER);
        position = position + 1;
        }
    }
    

//    for (int J = 0; J < [tempContentDic count]/18; J++){
//    NSLog(@"temp is %@",[tempContentDic objectForKey:[NSString stringWithFormat:@"WordDesc%d",J]]);
//    }
        arrAllWordIds = [[NSMutableArray alloc] init];
        arrAllWordDesc = [[NSMutableArray alloc] init];
        arrAllPin = [[NSMutableArray alloc] init];
        arrClassifications = [[NSMutableArray alloc] init];
        arrAllWordGameId01 = [[NSMutableArray alloc] init];
        arrAllWordGameId02 = [[NSMutableArray alloc] init];
        arrAllWordGameId03 = [[NSMutableArray alloc] init];
        arrAllWordGameId04 = [[NSMutableArray alloc] init];
        arrAllWordGameId05 = [[NSMutableArray alloc] init];
        arrAllWordGameId06 = [[NSMutableArray alloc] init];
        arrAllWordGameId07 = [[NSMutableArray alloc] init];
        arrAllWordGameId08 = [[NSMutableArray alloc] init];
        arrAllWordGameId09 = [[NSMutableArray alloc] init];
        arrAllWordGameId10 = [[NSMutableArray alloc] init];
    int post = 0;

    int currentDisplayingClassificationNumber = [[tempContentDic objectForKey:[NSString stringWithFormat:@"classification%d",post]] intValue];
    int checkNowsClassificationNumber;
    int tempIntForClassificationNumber;;


//        for (int i = 0; i < [classificationDic count]; i++)
//        {
//            tempIntForClassificationNumber = [[classificationDic objectForKey:[NSString stringWithFormat:@"folderId%d",i]] intValue];
//            NSLog(@"%d",tempIntForClassificationNumber);
//            if (tempIntForClassificationNumber == currentDisplayingClassificationNumber) {
//                classificationName = [classificationDic objectForKey:[NSString stringWithFormat:@"folderName%d",tempIntForClassificationNumber]];
//                checkNowsClassificationNumber = currentDisplayingClassificationNumber;
//                NSLog(@"Classifcation Name is changed to %@",classificationName);
//            }
//        }

    
    for (int J = 0; J < [tempContentDic count]/26; J++){
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"WordDesc%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"WordDesc%d",J]] == Nil){
            [arrAllWordDesc addObject:@""];
        }else{
            
            NSString *wordSpell = [tempContentDic objectForKey:[NSString stringWithFormat:@"WordDesc%d",J ]];

            [arrAllWordDesc addObject:wordSpell];
//            NSLog(@"NEw word spell is %@!",wordSpell);
        }
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"PINNUMBER%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"PINNUMBER%d",J]] == Nil){
            [arrAllPin addObject:@""];
        }else{
            
            NSString *wordSpell = [tempContentDic objectForKey:[NSString stringWithFormat:@"PINNUMBER%d",J ]];
            
            [arrAllPin addObject:wordSpell];

        }
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectFixIt%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectFixIt%d",J]] == Nil) {
            [arrAllWordGameId01 addObject:@"0"];
        }
        else{
            [arrAllWordGameId01 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectFixIt%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersFixIt%d",J]]]];
            NSLog(@"%@,",[arrAllWordGameId01 objectAtIndex:J]);
        }
        
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectSpinIt%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectSpinIt%d",J]] == Nil) {
            [arrAllWordGameId02 addObject:@"0"];
        }
        else{
            [arrAllWordGameId02 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectSpinIt%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersSpinIt%d",J]]]];
        }
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectPopIt%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectPopIt%d",J]] == Nil) {
            [arrAllWordGameId03 addObject:@"0"];
        }
        else{
            [arrAllWordGameId03 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectPopIt%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersPopIt%d",J]]]];
        }
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectPickIt%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectPickIt%d",J]] == Nil) {
            [arrAllWordGameId04 addObject:@"0"];
        }
        else{
            [arrAllWordGameId04 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectPickIt%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersPickIt%d",J]]]];
        }
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectMoveIt%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectMoveIt%d",J]] == Nil) {
            [arrAllWordGameId05 addObject:@"0"];
        }
        else{
            [arrAllWordGameId05 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectMoveIt%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersMoveIt%d",J]]]];
        }
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectMatchIt%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectMatchIt%d",J]] == Nil) {
            [arrAllWordGameId06 addObject:@"0"];
        }
        else{
            [arrAllWordGameId06 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectMatchIt%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersMatchIt%d",J]]]];
        }
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectTypeIt%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectTypeIt%d",J]] == Nil) {
            [arrAllWordGameId07 addObject:@"0"];
        }
        else{
            [arrAllWordGameId07 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectTypeIt%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersTypeIt%d",J]]]];
        }
        
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectFootball%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectFootball%d",J]] == Nil) {
            [arrAllWordGameId08 addObject:@"0"];
        }
        else{
            [arrAllWordGameId08 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectFootball%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersFootball%d",J]]]];
        }
        
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectHockey%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectHockey%d",J]] == Nil) {
            [arrAllWordGameId09 addObject:@"0"];
        }
        else{
            [arrAllWordGameId09 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectHockey%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersHockey%d",J]]]];
        }
        NSLog(@"%@", [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectBasketBalll%d",J]]);
        if ([tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectBasketBall%d",J]] == NULL || [tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectBasketBall%d",J]] == Nil) {
            [arrAllWordGameId10 addObject:@"0"];
        }
        else{
            [arrAllWordGameId10 addObject:[NSString stringWithFormat:@"%@/%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrectBasketBall%d",J]],[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswersBasketball%d",J]]]];
        }
        
     
    }
    
    NSMutableString *xlsString = [[NSMutableString alloc] init];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"]) {
        NSString *pinNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"];
        [xlsString setString:[NSString stringWithFormat: @"PIN : %@ \nPIN NUMBER\tWORDS\tFIXIT\tSPINIT\tPOPIT\tPICKIT\tMOVEIT\tMATCHIT\tTYPEIT\tKICKIT\tHITIT\tSHOOTIT\n",pinNumber]];
    }
    else{
        NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"NAMEOFUSER"];
        [xlsString setString:[NSString stringWithFormat: @"NAME : %@ \nPIN NUMBER\tWORDS\tFIXIT\tSPINIT\tPOPIT\tPICKIT\tMOVEIT\tMATCHIT\tTYPEIT\tKICKIT\tHITIT\tSHOOTIT\n",userName]];
    
    }
    
    NSLog(@"PAGE 3");

//    NSLog(@"Classifcation Name is changed to %@",classificationName);
    for (int count = 0; count < [tempContentDic count]/26; count++) {
        
        
        currentDisplayingClassificationNumber = [[tempContentDic objectForKey:[NSString stringWithFormat:@"classification%d",count]] intValue];
        //        NSLog(@"Check now is %d, current display us %d",checkNowsClassificationNumber,currentDisplayingClassificationNumber);
//        if (checkNowsClassificationNumber !=  currentDisplayingClassificationNumber ) {
//            
//            for (int i = 0; i < [classificationDic count]; i++)
//            {
//                tempIntForClassificationNumber = [[classificationDic objectForKey:[NSString stringWithFormat:@"folderId%d",i]] intValue];
//                NSLog(@"%d",tempIntForClassificationNumber);
//                if (tempIntForClassificationNumber == currentDisplayingClassificationNumber) {
//                    classificationName = [classificationDic objectForKey:[NSString stringWithFormat:@"folderName%d",tempIntForClassificationNumber]];
//                    checkNowsClassificationNumber = currentDisplayingClassificationNumber;
//                    break;
//                }
//            }
//        }
    
        if (([[arrAllWordGameId01 objectAtIndex:count] isEqualToString:@"0/0"] && [[arrAllWordGameId02 objectAtIndex:count] isEqualToString:@"0/0"] && [[arrAllWordGameId03 objectAtIndex:count] isEqualToString:@"0/0"] && [[arrAllWordGameId04 objectAtIndex:count] isEqualToString:@"0/0"] && [[arrAllWordGameId05 objectAtIndex:count] isEqualToString:@"0/0"] && [[arrAllWordGameId06 objectAtIndex:count] isEqualToString:@"0/0"] && [[arrAllWordGameId07 objectAtIndex:count] isEqualToString:@"0/0"] && [[arrAllWordGameId08 objectAtIndex:count] isEqualToString:@"0/0"]  && [[arrAllWordGameId09 objectAtIndex:count] isEqualToString:@"0/0"]  && [[arrAllWordGameId10 objectAtIndex:count] isEqualToString:@"0/0"])
            ||
            ([[arrAllWordGameId01 objectAtIndex:count] isEqualToString:@"0"] && [[arrAllWordGameId02 objectAtIndex:count] isEqualToString:@"0"] && [[arrAllWordGameId03 objectAtIndex:count] isEqualToString:@"0"] && [[arrAllWordGameId04 objectAtIndex:count] isEqualToString:@"0"] && [[arrAllWordGameId05 objectAtIndex:count] isEqualToString:@"0"] && [[arrAllWordGameId06 objectAtIndex:count] isEqualToString:@"0"] && [[arrAllWordGameId07 objectAtIndex:count] isEqualToString:@"0"]  && [[arrAllWordGameId08 objectAtIndex:count] isEqualToString:@"0"]  && [[arrAllWordGameId09 objectAtIndex:count] isEqualToString:@"0"]  && [[arrAllWordGameId10 objectAtIndex:count] isEqualToString:@"0"]) ) {
        // DO NOTING
        
        }
        else{
        [xlsString appendString:[NSString stringWithFormat:@"%@\t%@\t'%@\t'%@\t'%@\t'%@\t'%@\t'%@\t'%@\t'%@\t'%@\t'%@\n",[arrAllPin objectAtIndex:count], [arrAllWordDesc objectAtIndex:count],/*classificationName,*/[arrAllWordGameId01 objectAtIndex:count],[arrAllWordGameId02 objectAtIndex:count],[arrAllWordGameId03 objectAtIndex:count],[arrAllWordGameId04 objectAtIndex:count],[arrAllWordGameId05 objectAtIndex:count],[arrAllWordGameId06 objectAtIndex:count],[arrAllWordGameId07 objectAtIndex:count],[arrAllWordGameId08 objectAtIndex:count],[arrAllWordGameId09 objectAtIndex:count],[arrAllWordGameId10 objectAtIndex:count] ]];

        }
    }
            NSLog(@"PAGE 4");
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths lastObject];
    
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", @"userdata"]]; //add our file to the path
    
    [fileManager createFileAtPath:fullPath contents:[xlsString dataUsingEncoding:NSUTF16StringEncoding] attributes:nil]; //finally save the path (file)
}
/* PDF generator/mail CLASSES */

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    if ( [ CCDirector sharedDirector ].view != nil ){
        [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    }
}


-(void)mailIt {
    
    if ([self.view viewWithTag:100]) {
        if([[self.view viewWithTag:100] viewWithTag:102]){
            [[[self.view viewWithTag:100] viewWithTag:102] removeFromSuperview];
            
        }
        [[self.view viewWithTag:100] removeFromSuperview];
    }
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:@"Pick Learn Play Achievements Report"];
        [mailViewController setMessageBody:@"Attached is the achievements report of Pick Learn Play. For best result, open the file on your PC." isHTML:NO];
        
        //        NSString* fileName = @"Report.pdf";
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths lastObject];

//        NSString* PdfFilePath = [documentsDirectory stringByAppendingPathComponent:@"Report.pdf"];
//        NSData *myData = [NSData dataWithContentsOfFile:PdfFilePath];
//        [mailViewController addAttachmentData:myData mimeType:@"application/pdf" fileName:@"Report.pdf"];

        NSString* PdfFilePath = [documentsDirectory stringByAppendingPathComponent:@"userdata.csv"];
        NSData *myData = [NSData dataWithContentsOfFile:PdfFilePath];
        [mailViewController addAttachmentData:myData mimeType:@"application/csv" fileName:@"userdata.csv"];
        
        [self presentModalViewController:mailViewController animated:YES];
        [mailViewController release];
    }
    else {
        NSLog(@"Device is unable to send email in its current state.");
    }
}

- (void) drawBorder
{
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    UIColor *borderColor = [UIColor brownColor];
    
    CGRect rectFrame = CGRectMake(kBorderInset, kBorderInset, pageSize.width-kBorderInset*2, pageSize.height-kBorderInset*2);
    
    CGContextSetStrokeColorWithColor(currentContext, borderColor.CGColor);
    CGContextSetLineWidth(currentContext, kBorderWidth);
    CGContextStrokeRect(currentContext, rectFrame);
}

- (void)drawPageNumber:(NSInteger)pageNumber
{
    NSString* pageNumberString = [NSString stringWithFormat:@"Page %d", pageNumber];
    UIFont* theFont = [UIFont systemFontOfSize:12];
    
    CGSize pageNumberStringSize = [pageNumberString sizeWithFont:theFont
                                               constrainedToSize:pageSize
                                                   lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect stringRenderingRect = CGRectMake(kBorderInset,
                                            pageSize.height - 40.0,
                                            pageSize.width - 2*kBorderInset,
                                            pageNumberStringSize.height);
    
    [pageNumberString drawInRect:stringRenderingRect withFont:theFont lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentCenter];
}

- (void) drawHeader
{
    
    
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.3, 0.7, 0.2, 1.0);
    
    NSString *textToDraw = @"\t\t\t\t\t\t\t\t\t\t\t\t         Fix it \t Spin it \t Pop It \t Pick It \t Match It \t Move It";
    
    UIFont *font = [UIFont systemFontOfSize:19.0];
    
    CGSize stringSize = [textToDraw sizeWithFont:font constrainedToSize:CGSizeMake(pageSize.width - 2*kBorderInset-2*kMarginInset, pageSize.height - 2*kBorderInset - 2*kMarginInset) lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect renderingRect = CGRectMake(kBorderInset + kMarginInset, kBorderInset + kMarginInset, pageSize.width - 2*kBorderInset - 2*kMarginInset, stringSize.height);
    
    [textToDraw drawInRect:renderingRect withFont:font lineBreakMode:UILineBreakModeWordWrap alignment:UITextAlignmentLeft];
}

- (void) drawText:(int ) currentPage
{
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(currentContext, 0.0, 0.0, 0.0, 1.0);
    
    NSMutableString *textToDraw = [[NSMutableString alloc] init];
    
    int position = 0;
    int currentDisplayingClassification = [[tempContentDic objectForKey:[NSString stringWithFormat:@"classification%d",position]] intValue];
    
    int currentactiveLanguage = [[tempContentDic objectForKey:[NSString stringWithFormat:@"activeLanguage%d",position]] intValue];

    int checkNowsClassification = currentDisplayingClassification;

//    textToDraw = [NSMutableString stringWithFormat:@"Location 1\n"];
    int all_words = [arrAllWordDesc count];
    for (int k = all_words/10*currentPage; k < all_words; k ++) {
        
        checkNowsClassification = [[tempContentDic objectForKey:[NSString stringWithFormat:@"classification%d",k ]] intValue];
        if (checkNowsClassification != currentDisplayingClassification) {
            currentDisplayingClassification = checkNowsClassification;
            [textToDraw appendString:[NSMutableString stringWithFormat:@"\n"]];
        }
        
        
        [textToDraw appendString:[NSMutableString stringWithFormat:@"%@          %@          %@          %@          %@          %@          %@\n",[arrAllWordDesc objectAtIndex:k],[arrAllWordGameId01 objectAtIndex:k],[arrAllWordGameId02 objectAtIndex:k],[arrAllWordGameId03 objectAtIndex:k],[arrAllWordGameId04 objectAtIndex:k],[arrAllWordGameId05 objectAtIndex:k],[arrAllWordGameId06 objectAtIndex:k]]];
    }
        
    
//    MrPDatabase *database = [[MrPDatabase alloc] init];
//    NSArray* retVal = [[NSArray alloc] init];
//    retVal = [database getReportData];
    
//    int position = 0;
//    tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[retVal count]];
//    for (MrPReport *info in retVal) {
//        
//        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
//        [tempContentDic setObject:info.wordDescription forKey:[NSString stringWithFormat:@"WordDesc%d",position]];
//        [tempContentDic setObject:[NSNumber numberWithInt:info.totalAnswers] forKey:[NSString stringWithFormat:@"totalAnswers%d",position]];
//        [tempContentDic setObject:[NSNumber numberWithInt:info.noOfCorrect] forKey:[NSString stringWithFormat:@"noOfCorrect%d",position]];
//        [tempContentDic setObject:[NSNumber numberWithInt:info.classification] forKey:[NSString stringWithFormat:@"classification%d",position]];
//        [tempContentDic setObject:[NSNumber numberWithInt:info.activelanguage] forKey:[NSString stringWithFormat:@"activeLanguage%d",position]];
//        
//        [tempContentDic setObject:[NSNumber numberWithInt:info.gameId] forKey:[NSString stringWithFormat:@"gameId%d",position]];
//        position = position + 1;
//    }
//    NSLog(@"temp is %@",tempContentDic);

//    position = 0;
//    int currentDisplayingClassification = [[tempContentDic objectForKey:[NSString stringWithFormat:@"classification%d",position]] intValue];
////    int currentGameId = [[tempContentDic objectForKey:[NSString stringWithFormat:@"gameId%d",position]] intValue];
////    int currentactiveLanguage = [[tempContentDic objectForKey:[NSString stringWithFormat:@"activeLanguage%d",position]] intValue];
////    NSString* wordDesc;
////    int correctAns;
////    int totalAns;
//    int checkNowsClassification = currentDisplayingClassification;
////    NSMutableString *textToDraw = [[NSMutableString alloc] init];
////    int locationsCounters = 1;
////    textToDraw = [NSMutableString stringWithFormat:@"Location 1\n"];
////    for (int i = 0; i < [tempContentDic count]/7; i++) {
//        checkNowsClassification = [[tempContentDic objectForKey:[NSString stringWithFormat:@"classification%d",i ]] intValue];
//        if (checkNowsClassification != currentDisplayingClassification) {
//            currentDisplayingClassification = checkNowsClassification;
////            locationsCounters = locationsCounters + 1;
//            [textToDraw appendString:[NSMutableString stringWithFormat:@"\nLocation %d\n",locationsCounters]];
//        }
//        wordDesc = [tempContentDic objectForKey:[NSString stringWithFormat:@"WordDesc%d",i]];
//        correctAns = [[tempContentDic objectForKey:[NSString stringWithFormat:@"noOfCorrect%d",i]] intValue];
//        totalAns = [[tempContentDic objectForKey:[NSString stringWithFormat:@"totalAnswers%d",i]] intValue];
//        if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"gameId%d",position]] intValue] == 1){
//            [textToDraw appendString:[NSMutableString stringWithFormat:@"  %@ \t\t\t\t         %d/%d\n",wordDesc,correctAns,totalAns]];
//        }
        
//        int length = [wordDesc length];
//        if (length < 10) {
//            int addSpaces = 10 - length;
//            for (int p = 0; p < addSpaces; p++) {
//                wordDesc = [wordDesc stringByAppendingString:@" "];
//            }
//            addSpaces = 0;
//        }
//        NSLog(@"word desc is %@!",wordDesc);
//
//    
//        if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"gameId%d",i]] intValue] == FIXIT){
//            [textToDraw appendString:[NSMutableString stringWithFormat:@"  %@ \t\t\t\t         %d/%d\n",wordDesc,correctAns,totalAns]];
//        }
//        else        
//        if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"gameId%d",i]] intValue] == SPIN){
//            [textToDraw appendString:[NSMutableString stringWithFormat:@"  %@ \t\t\t\t                          %d/%d\n",wordDesc,correctAns,totalAns]];
//        }
//        else
//            if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"gameId%d",i]] intValue] == POPIT){
//                [textToDraw appendString:[NSMutableString stringWithFormat:@"  %@ \t\t\t\t                                       %d/%d\n",wordDesc,correctAns,totalAns]];
//            }
//        
//            else
//                if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"gameId%d",i]] intValue] == PICKIT){
//                    [textToDraw appendString:[NSMutableString stringWithFormat:@"  %@ \t\t\t\t                                                    %d/%d\n",wordDesc,correctAns,totalAns]];
//                }
//        
//        if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"gameId%d",i]] intValue] == MATCHIT){
//            [textToDraw appendString:[NSMutableString stringWithFormat:@"  %@ \t\t\t\t                                                                   %d/%d\n",wordDesc,correctAns,totalAns]];
//        }
//                else
//                    if ([[tempContentDic objectForKey:[NSString stringWithFormat:@"gameId%d",i]] intValue] == MOVEIT){
//                        [textToDraw appendString:[NSMutableString stringWithFormat:@"  %@ \t\t\t\t                                                                                        %d/%d\n",wordDesc,correctAns,totalAns]];
//                    }
//}
    UIFont *font = [UIFont systemFontOfSize:8.0];
    
    CGSize stringSize = [textToDraw sizeWithFont:font
                               constrainedToSize:CGSizeMake(pageSize.width - 2*kBorderInset-2*kMarginInset, pageSize.height - 2*kBorderInset - 2*kMarginInset-60)
                                   lineBreakMode:UILineBreakModeWordWrap];
    
    CGRect renderingRect = CGRectMake(kBorderInset + kMarginInset, kBorderInset + kMarginInset + 50.0, pageSize.width - 2*kBorderInset - 2*kMarginInset, stringSize.height);
    
    [textToDraw drawInRect:renderingRect
                  withFont:font
             lineBreakMode:UILineBreakModeWordWrap
                 alignment:UITextAlignmentLeft];    
}

- (void) drawLine
{
    CGContextRef    currentContext = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(currentContext, kLineWidth);
    
    CGContextSetStrokeColorWithColor(currentContext, [UIColor blueColor].CGColor);
    
    CGPoint startPoint = CGPointMake(kMarginInset + kBorderInset, kMarginInset + kBorderInset + 40.0);
    CGPoint endPoint = CGPointMake(pageSize.width - 2*kMarginInset -2*kBorderInset, kMarginInset + kBorderInset + 40.0);
    
    CGContextBeginPath(currentContext);
    CGContextMoveToPoint(currentContext, startPoint.x, startPoint.y);
    CGContextAddLineToPoint(currentContext, endPoint.x, endPoint.y);
    
    CGContextClosePath(currentContext);
    CGContextDrawPath(currentContext, kCGPathFillStroke);
}

- (void) drawImage
{
    UIImage * demoImage = [UIImage imageNamed:@"demo.png"];
    [demoImage drawInRect:CGRectMake( (pageSize.width - demoImage.size.width/2)/2, 350, demoImage.size.width/2, demoImage.size.height/2)];
    

}

- (void) generatePdfWithFilePath: (NSString *)thefilePath
{
    UIGraphicsBeginPDFContextToFile(thefilePath, CGRectZero, nil);
    
    NSInteger currentPage = 0;
    BOOL done = NO;
    do
    {
        //Start a new page.
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, pageSize.width, pageSize.height), nil);
        
        //Draw a page number at the bottom of each page.
        currentPage++;
        [self drawPageNumber:currentPage];
        
        //Draw a border for each page.
        [self drawBorder];
        
        //Draw text fo our header.
        [self drawHeader];
        
        //Draw a line below the header.
        [self drawLine];
        
        //Draw some text for the page.
        [self drawText:currentPage];
        
        //Draw an image
        [self drawImage];
        done = YES;
    }
    while (currentPage !=15);
    
    // Close the PDF context and write the contents out.
    UIGraphicsEndPDFContext();
}


/* MAIL CLASSES ENDS here*/

@end
