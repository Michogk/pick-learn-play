//
//  Baloons.h
//  KidsBubbleLearner
//
//  Created by Muhammad Mosib Asad on 6/24/12.
//  Copyright (c) 2012 GameView Studios. All rights reserved.
//

#import "GameLayer.h"
#import "Balloon.h"

@interface BaloonsGroup : CCMenu {
    
    NSMutableArray* wordsList;
    Balloon* balloonLeft;
    Balloon* balloonMiddle;
    Balloon* balloonRight;
    
    NSString* wordOne;
    NSString* wordTwo;
    NSString* wordCorrect;
    
    CCMenu* balloons;
    NSMutableDictionary *arabicWordListArr;
}
+ (id) balloons: (NSString*) word;
- (id) initWithCorrectWord:(NSString*) missingWord;

- (void) initializeLeftRightWords;
- (NSString*) getRandomWord;
- (BalloonTypes) getRandomBalloonType;
- (void) setBalloonsInitialPositions;
- (void) stopAllBalloons;

@end
