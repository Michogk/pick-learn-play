

#import "CAURLDownload.h"

@implementation CAURLDownload
@synthesize url, target, selector, data, userInfo, error, failSelector;
#ifdef __BLOCKS__
@synthesize finishedBlock, failedBlock;
#endif

- (void)start {
   NSURLRequest *req = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
   NSURLConnection *conn = [NSURLConnection connectionWithRequest:req delegate:self];
   data = [NSMutableData data];
   [data retain];
   [conn start];
}

+ (void)downloadURL:(NSURL *)m_url target:(id)m_target selector:(SEL)m_selector failSelector:(SEL)m_failSelector userInfo:(NSDictionary *)m_userInfo waitTime:(int)waitSecs {
   CAURLDownload *download = [[CAURLDownload alloc] init];
   download.url = m_url;
   download.target = m_target;
   download.userInfo = m_userInfo;
   download.selector = m_selector;
   download.failSelector = m_failSelector;
   [NSTimer scheduledTimerWithTimeInterval:waitSecs target:download selector:@selector(start) userInfo:nil repeats:NO];
   [download autorelease];
}

+ (void)downloadURL:(NSURL *)m_url target:(id)m_target selector:(SEL)m_selector failSelector:(SEL)m_failSelector userInfo:(NSDictionary *)m_userInfo {
   [CAURLDownload downloadURL:m_url target:m_target selector:m_selector failSelector:m_failSelector userInfo:m_userInfo waitTime:0];
}

#ifdef  __BLOCKS__

+ (void)downloadURL:(NSURL *)m_url finished:(void(^)(void))finished failed:(void(^)(void))failure userInfo:(NSDictionary *)m_userInfo waitTime:(int)waitSecs {
   CAURLDownload *download = [[CAURLDownload alloc] init];
   download.url = m_url;
   download.userInfo = m_userInfo;
   download.finishedBlock = finished;
   download.failedBlock = failure;
   [NSTimer scheduledTimerWithTimeInterval:waitSecs target:download selector:@selector(start) userInfo:nil repeats:NO];
   [download autorelease];
}

+ (void)downloadURL:(NSURL *)m_url finished:(void (^)(void))finished failed:(void (^)(void))failure userInfo:(NSDictionary *)m_userInfo {
   [CAURLDownload downloadURL:m_url finished:finished failed:failure userInfo:m_userInfo];
}

#endif

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)m_error {
   self.error = m_error;
#ifdef __BLOCKS__
   if (failedBlock)
      failedBlock();
#endif
   if (target && failSelector)
      [target performSelector:failSelector withObject:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
   [self.data setLength:0];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
   return nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)dataR
{
   [self.data appendData:dataR];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
#ifdef __BLOCKS__
   if (finishedBlock)
      finishedBlock();
#endif
   if (target && selector)
      [self.target performSelector:selector withObject:[NSData dataWithData:data] withObject:self];
}

- (void)dealloc {
   [url release];
   [target release];
   [userInfo release];
   if (error)
      [error release];
   [data release];
   [super dealloc];
}

@end
