//
//  HelpSoundViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 10/22/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "HelpSoundViewController.h"
#import "SimpleAudioEngine.h"



@interface HelpSoundViewController ()

@end

@implementation HelpSoundViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    
    [super viewDidLoad];
    
    [[AVAudioSession sharedInstance] setDelegate: self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error: nil];
    
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,sizeof (audioRouteOverride),&audioRouteOverride);
//    
    NSError *activationError = nil;
    [[AVAudioSession sharedInstance] setActive:YES error:&activationError];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)PlayEnglishHelpSound:(id)sender {

    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    NSString *helpSound = [[NSUserDefaults standardUserDefaults] objectForKey:@"Help"];        
    NSString* fileName = [NSString stringWithFormat:@"E%@",helpSound];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    if (soundFilePath == nil || !soundFilePath) {
        NSLog(@"Sound NOt found");
        fileName = @"EmptySound";
        soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];

        player.volume = 1;
        player.numberOfLoops = 0;
        player.delegate = self;
        if ([player prepareToPlay]) {
            [player play];            
        }
//            [player release];
        return;
    }

    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.volume = 1;
    player.numberOfLoops = 0;
    player.delegate = self;
    
    if ([player prepareToPlay]) {
        [player play];            
    }
//        [player release];
}

- (IBAction)PlayArabicHelpSound:(id)sender {
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }    
    NSString *helpSound = [[NSUserDefaults standardUserDefaults] objectForKey:@"Help"];        
    NSString* fileName = [NSString stringWithFormat:@"A%@",helpSound];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    if (soundFilePath == nil || !soundFilePath) {
        NSLog(@"Sound NOt found");
        fileName = @"EmptySound";
        soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
        
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        player.volume = 0.5;
        player.numberOfLoops = 0;
        player.delegate = self;
        [player play];
        
        return;
    }
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.volume = 0.5;
    player.numberOfLoops = 0;
    player.delegate = self;
    
    [player play];
    
}


- (void)viewWillDisappear:(BOOL)animated
{
    if([player isPlaying]){
    [player stop];
    [player release];
    }
//    [self setAudioPlayer:nil];
    NSError *activationError = nil;
    [[AVAudioSession sharedInstance] setActive:NO
                                         error:&activationError];
}

@end
