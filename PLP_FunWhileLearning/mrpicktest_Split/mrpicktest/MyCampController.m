//
//  MyCampController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/17/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MyCampController.h"
#import "MrPContentInfo.h"
#import "MrPDatabase.m"
#import "SimpleAudioEngine.h"
#import "MySkillsViewController.h"
#import <QuartzCore/CALayer.h>
#import "DocumentDirectory.h"
#import "MyStoreViewController.h"
#import "MovieViewController.h"
#import "UIImage+animatedGIF.h"

#define CURRENT_CLASSIFICATION 20

#define BAKERY 20
#define OFFICE 240
#define ZOO 380
#define COFFEE_SHOP 50
#define BEACH 30
#define FARM 80

#define FIRST_FEATHER 40
#define SECOND_FEATHER 50
#define THIRD_FEATHER 60
#define FOURTH_FEATHER 70


@implementation MyCampController 
@synthesize checkButton;
@synthesize UndoButton;

@synthesize scrollView1;
@synthesize ActiveImage;
@synthesize btnMyCamp;
@synthesize lplWord;
@synthesize _correctText;
@synthesize imgArabicEar = _imgArabicEar;
@synthesize imgEnglishEar = _imgEnglishEar;

const CGFloat kScrollObjHeight	= 100.0;
const CGFloat kScrollObjWidth	= 120.0;
//const NSUInteger kNumImages		= 32;


- (void)layoutScrollImages
{
    UIImageView *view = nil;
    NSArray *subviews = [scrollView1 subviews];
                            
                        
    // reposition all image subviews in a horizontal serial fashion
    CGFloat curXLoc = 0;
    for (view in subviews)
    {
        if ([view isKindOfClass:[UIImageView class]] && view.tag > 0)
        {
            CGRect frame = view.frame;
            frame.origin = CGPointMake(curXLoc, 0);
            view.frame = frame;
            
            curXLoc += (kScrollObjWidth);
        }
    }
    
    // set the content offest x,y
    [scrollView1 setContentOffset:CGPointMake(2, 2)];

    // set the content size so it can be scrollable
    [scrollView1 setContentSize:CGSizeMake((_kNumImages * kScrollObjWidth), [scrollView1 bounds].size.height)];
    
}

-(void) moveResourcesFromBundleDirToDocumentDir{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"100.png"];  //folder contain images in your bundle
    NSString *destPath = [documentsDirectory stringByAppendingPathComponent:@"imagesAll"];  //images is your folder under document directory
    
    NSError *error;
    [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destPath error:&error];  //copy every files from sourcePath to destPath

}

-(void) loadClassificationImage{
    
    NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    if([objectClassification isKindOfClass:[NSArray class]] == YES)
    {
        MrPDatabase *database = [[MrPDatabase alloc] init];
        NSArray* MrPContentArray  = [database loadImageForAvailableClassifications:objectClassification];

        for (MrPContentInfo *info in MrPContentArray) {
            UIImage *classificationImg = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png",info.imageId ]];
            UIImageView *classificationImgView = [[UIImageView alloc] initWithImage:classificationImg];
            classificationImgView.frame = CGRectMake(20+746-19+10+20+30-18, 20+144-27+54+10+403+10, 201-20, 150-10);
            [self.view addSubview:classificationImgView];
            break;
        }
    }
}

-(void) showSecondFeather{
    [self showFeather:SECOND_FEATHER];
        [self.view bringSubviewToFront:imgView2Feather];
//        [self performSelector:@selector(showThirdFeather) withObject:nil afterDelay:2.0];
}
-(void) showThirdFeather{
        [self showFeather:THIRD_FEATHER];
        [self.view bringSubviewToFront:imgView3Feather];
            [self performSelector:@selector(showFourthFeather) withObject:nil afterDelay:3.0];
}

-(void) showFourthFeather{
        [self showFeather:FOURTH_FEATHER];
    [self.view bringSubviewToFront:imgView4Feather];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadClassificationImage];
    NSLog(@"%@",[UIFont familyNames]);

    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:SYNONYM_ACTIVE]) {
          [_lblSynonym setHidden:YES];
    }
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"feather" withExtension:@"gif"];
    imgView1Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    imgView2Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    imgView3Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    imgView4Feather.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    
    [self showFeather:FIRST_FEATHER];


    
//    for (int k = 1; k < 101; k++ ){
//        if( k % 3 == 0){
//            if(k%5 == 0){
//                NSLog(@"MOJO");
//            }
//            else
//            {
//                NSLog(@"MO");
//            }
//        }
//        else
//            if(k%5 == 0){
//                NSLog(@"JO");
//            }
//            else{
//                NSLog(@"%d",k);
//            }
//    }

    [_imgEnglishEar setHidden:YES];
    [_imgArabicEar setHidden:YES];

    [self playBackGroundMusic];
    
    int backgroundImage = [[NSUserDefaults standardUserDefaults] integerForKey:@"MyCampBackground"];
    
    if (backgroundImage == 1) {
        UIImage *pinkImg = [UIImage imageNamed:[NSString stringWithFormat:@"Mycamp_pink.png"]];
        [backgroundImgView setImage:pinkImg];
    }
    else {
        UIImage *blueImg = [UIImage imageNamed:[NSString stringWithFormat:@"Mycamp.png"]];
        [backgroundImgView setImage:blueImg];
    }
    int position = 0;
    _currentWordIndex = 0;
    // Loaading data  from database and saving in dictationaries

    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    LanguageSupport = [[NSUserDefaults standardUserDefaults] integerForKey:@"LanguageSupport"];    
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    NSArray *MrPContentArray;
    NSArray *MrPContentArabicArray;
//    if (activeLanguage == 2 || (cId > 989 && cId < 999) ) {
    MrPContentArray = [database EnglishWordDatabaseArray:cId];

    tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];

    for (MrPContentInfo *info in MrPContentArray) {

            [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
//            [tempContentDic setObject:[NSNumber numberWithInt:info.arabicRef] forKey:[NSString stringWithFormat:@"arabicRef%d",position]];
            [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
        NSLog(@"Info.english word is %@",info.englishWord);
            [tempContentDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]];
            [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
            [tempContentDic setObject:[NSNumber numberWithInt:info.arabicRef] forKey:[NSString stringWithFormat:@"arabicRef%d",position]];
            [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
            [tempContentDic setObject:[NSNumber numberWithInt:info.translate] forKey:[NSString stringWithFormat:@"translate%d",position]];
        [tempContentDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
        [tempContentDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];

            position++;
        }
    position = 0;
    MrPContentArabicArray = [database ArabicWordDatabaseArray:cId];
    if ((activeLanguage == 1 && cId > 989) &&  (activeLanguage == 1 && cId < 999)) {
        MrPContentArabicArray = MrPContentArray;
        _arabicWordDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
    }
    else{
        _arabicWordDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArabicArray count]];
    }
    
    if([MrPContentArabicArray count] == 0);
    for (MrPContentInfo *info in MrPContentArabicArray) {
        [_arabicWordDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
        [_arabicWordDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"arabicWord%d",position]];
        [_arabicWordDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]];
        [_arabicWordDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
        [_arabicWordDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"arabicvoiceId%d",position]]; ;
        [_arabicWordDic setObject:[NSNumber numberWithInt:info.translate] forKey:[NSString stringWithFormat:@"translate%d",position]];
        [_arabicWordDic setObject:[NSNumber numberWithInt:info.english_ref] forKey:[NSString stringWithFormat:@"englishRef%d",position]];
        [_arabicWordDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];
        [_arabicWordDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];
 
        position++;
        }
//    }
    NSLog(@"arabicWord Dictationary cout is %d and englishword dictationary count is %d",[_arabicWordDic count],[tempContentDic count]);
    


    if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
        _kNumImages = [_arabicWordDic count]/9;
    }
    else {
        _kNumImages = [tempContentDic count]/9;
    }

    // Loading screen parameters

    
    lblArabicWord = [[UILabel alloc] initWithFrame:CGRectMake(698-600+60, 258-10-50+80, 600-60, 80)];
//    UIFont *font = [UIFont systemFontOfSize: 40];
    UIFont *font = [UIFont fontWithName:@"Futura" size:40];
//    [label1 setFont:font];

//    UIFont *font = [UIFont fontWithName:@"Helvetica" size:10];
    [lblArabicWord setFont:font];
    lblArabicWord.textAlignment = UITextAlignmentRight;
    lblArabicWord.adjustsFontSizeToFitWidth = YES;
    [lblArabicWord setBackgroundColor:[UIColor clearColor]];

    
    arabicWordButton = [[UIButton alloc] initWithFrame:CGRectMake(698-600+60, 258-10-50+80, 600-60, 80)];
    [arabicWordButton setBackgroundColor:[UIColor clearColor]];
//    [arabicWordButton setFont:[UIFont fontWithName:@"UthmanicHafs1 Ver09" size:150]];
//    font = [UIFont systemFontOfSize: 40];
    font = [UIFont fontWithName:@"Futura" size:40];
//    [label1 setFont:font];

    [arabicWordButton.titleLabel setFont:font];
    [arabicWordButton setTitle:@"" forState:UIControlStateNormal];
        arabicWordButton.titleLabel.textColor = [UIColor whiteColor];
    arabicWordButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    arabicWordButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [arabicWordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [arabicWordButton addTarget:self action:@selector(playSoundArabic) forControlEvents:UIControlEventTouchDown];
    


    
    englishWordOrSentence = [[UIButton alloc] initWithFrame:CGRectMake(698-600+60, 258-10-50+80, 600-60, 80)];
    [englishWordOrSentence setBackgroundColor:[UIColor clearColor]];
//    [englishWordOrSentence setFont:[UIFont fontWithName:@"Arial" size:50]];
//    UIFont *font = [UIFont systemFontOfSize: 40];

    [englishWordOrSentence.titleLabel setFont:font];

//    [englishWordOrSentence setBackgroundColor:[UIColor grayColor]];
    
    [englishWordOrSentence setTitle:@"" forState:UIControlStateNormal];
    englishWordOrSentence.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    englishWordOrSentence.titleLabel.textColor = [UIColor whiteColor];
    [englishWordOrSentence setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    englishWordOrSentence.titleLabel.adjustsFontSizeToFitWidth = YES;
    [englishWordOrSentence addTarget:self action:@selector(playSoundEnglish) forControlEvents:UIControlEventTouchDown];
    [englishWordOrSentence setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    [englishWordOrSentence setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [englishWordOrSentence setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [englishWordOrSentence setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

//    
//    if (activeLanguage == 2 && cId < 989) {
//        [self.view addSubview:englishWordOrSentence];
//        [_imgEnglishEar setHidden:NO];
//
//    }
//    
//    if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
//        [self.view addSubview:arabicWordButton];
//        [self.view addSubview:lblArabicWord];
//            [_imgArabicEar setHidden:NO];
//    }
//    if (LanguageSupport == 1 && activeLanguage == 1) {
//          [self.view addSubview:englishWordOrSentence];
//                    [_imgEnglishEar setHidden:NO];
//    }
//    if (LanguageSupport == 1 && activeLanguage == 2) {
//        [self.view addSubview:arabicWordButton];
//                [self.view addSubview:lblArabicWord];
//                    [_imgArabicEar setHidden:NO];
////            [self.view addSubview:lblArabicWord];
//    }
//
//    if (cId > 989 && cId < 999) {
//        [self.view addSubview:englishWordOrSentence];
//                    [_imgEnglishEar setHidden:NO];
//        if (LanguageSupport == 1) {
//            [arabicWordButton removeFromSuperview];
//                                [_imgArabicEar setHidden:YES];
//        }
//    }
    
    if(activeLanguage == 1){
        [self.view addSubview:arabicWordButton];
        [self.view addSubview:lblArabicWord];
        lblArabicWord.textColor = [UIColor whiteColor];
        [_imgArabicEar setHidden:NO];
    }
    else{
        [self.view addSubview:englishWordOrSentence];
        [_imgEnglishEar setHidden:NO];
        
    }

    [scrollView1 setCanCancelContentTouches:YES];
    scrollView1.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView1.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our scrollview
    scrollView1.scrollEnabled = YES;
    
//     scrollView1.pagingEnabled = YES;

    NSUInteger i;

//    int imgSel = [[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId0"]] intValue];
    int imgSel;
    if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
        imgSel = [[_arabicWordDic objectForKey:[NSString stringWithFormat:@"imageId0"]] intValue];
    }
    else {
        imgSel = [[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId0"]] intValue];
    }
    
    
    if (!imgSel || imgSel == 0) {
        imgSel = 1;
    }
    NSString *imgSelStrg = [NSString stringWithFormat:@"%d",imgSel]; 
    NSString *imgSelCom = [imgSelStrg stringByAppendingFormat:@".png"];
    NSLog(@"selected image is %@",imgSelCom);
//    UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@", imgSelCom]];
    UIImage *selectedImage = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@", imgSelCom]];
    selectedImageView = [[UIImageView alloc] initWithImage:selectedImage];
    selectedImageView.frame = CGRectMake(20+746-19+10, 20+144-27+54+10, 230, 240);
    [self.view addSubview:selectedImageView];
    
    if (activeLanguage == 1) {
        imgSel = [[_arabicWordDic objectForKey:[NSString stringWithFormat:@"imageId0"]] intValue];
    }
    else {
        imgSel = [[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId0"]] intValue];
    }
    if (!imgSel || imgSel == 0 || cId > 989) {
        imgSel = 1;
    }
    imgSelStrg = [NSString stringWithFormat:@"%d",imgSel]; 
    imgSelCom = [imgSelStrg stringByAppendingFormat:@".png"];
    
//    UIImage *classificationImg = [UIImage imageNamed:[NSString stringWithFormat:@"%@", imgSelCom]];
//    UIImage *classificationImg = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%@", imgSelCom]];
//    UIImageView *classificationImgView = [[UIImageView alloc] initWithImage:classificationImg];
//    classificationImgView.frame = CGRectMake(20+746-19+10+20+30-18, 20+144-27+54+10+403+10, 201-20, 150-10);
//    [self.view addSubview:classificationImgView];
    
    sender_tag = 1;
//    if (cId > 989) {
//        sender_tag = 0;
//    }
    int numberOfImages;
    if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
        numberOfImages = [MrPContentArabicArray count];
    }
    else {
        numberOfImages = [MrPContentArray count];
    }
    
//    if (cId > 989 && cId < 999 || activeLanguage == 2) {
//        numberOfImages = numberOfImages - 1;
//    }
    for (i = 0; i < numberOfImages; i++)
    {
        id imgIdExtraction;
        if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
            imgIdExtraction = [_arabicWordDic objectForKey:[NSString stringWithFormat:@"imageId%d",i]];
        }
        else {
            imgIdExtraction = [tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",i]];
        }        
      
        int imgId = [imgIdExtraction intValue];
        if (!imgId || imgId == 0) {
            imgId = 1;
        }
        NSString *imageName = [NSString stringWithFormat:@"%d.png",imgId];
//        UIImage *image = [UIImage imageNamed:imageName];
        UIImage *image = [DocumentDirectory imageNamed:imageName];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        
        [imageView setTag:sender_tag];

        [imageView setBackgroundColor:[UIColor clearColor]];
        
        imageView.userInteractionEnabled=YES;

        // setup each frame to a default height and width, it will be properly placed when we call "updateScrollList"
        CGRect rect = imageView.frame;
        rect.size.height = kScrollObjHeight;
        rect.size.width = kScrollObjWidth;
        
        imageView.frame = rect;
           
        [scrollView1 addSubview:imageView];
        
        // add gesture recognizers to the image view
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        singleTap.delegate = self;
        [imageView addGestureRecognizer:singleTap];
                sender_tag++;                
    }
    
    [self layoutScrollImages];	// now place the photos in serial layout within the scrollview
    
    self.scrollView1.delegate=self;
    

    self.btnMyCamp.highlighted=YES;
    self.btnMyCamp.enabled=NO;

    _completeWordArray = [[NSMutableArray alloc] initWithCapacity:60];
    
    cellText = [[UILabel alloc] initWithFrame:CGRectMake(158, 390, 590-60, 70)];
//    font = [UIFont systemFontOfSize: 35];
    font = [UIFont fontWithName:@"Futura" size:40];
    [cellText setFont:font];
    cellText.textAlignment = UITextAlignmentCenter;
    cellText.adjustsFontSizeToFitWidth = YES;
    [cellText setBackgroundColor:[UIColor lightTextColor]];
    [self.view addSubview:cellText];
    cellText.alpha = 0.5;
    


    
}
-(void) playHelpMovie{

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == NO) {
        return;
    }
    
    if (myCampPlayer) {
        [myCampPlayer stop];
        [myCampPlayer release];
        myCampPlayer = nil;
    }
    
    if (player) {
        [player stop];
        [player release];
        player = nil;
    }
    
    NSString *movieName = @"MyCamp";
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
        return;
    }
    
    MovieViewController* moviePlayer = [[MovieViewController alloc] init];
    [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayer animated:NO];
    // Prep and play the movie
    [moviePlayer playMovie:movieName];

}

-(void) viewDidAppear:(BOOL)animated{
    if (!showMovie) {
//        [self playHelpMovie];        
    }
    else{
        [self playBackGroundMusic];
    }

}

-(void)viewDidDisappear:(BOOL)animated{
    showMovie = TRUE;
}

- (void)viewDidUnload
{

   
    ActiveImage = nil;
    btnMyCamp = nil;
    lblWord = nil;
    [backgroundImgView release];
    backgroundImgView = nil;
    [self setCheckButton:nil];
    [self setUndoButton:nil];
    [self setImgEnglishEar:nil];
    [self setImgArabicEar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
        if (myCampPlayer) {
            [myCampPlayer stop];
            myCampPlayer = nil;
        }
        
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//        MyStoreViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyBookStoreViewControllerIderntifier"];
//        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//        [self presentViewController:vc animated:NO completion:NULL];    
    
    }
    if (buttonIndex == 1)
    {
        
    }           
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
     
    NSLog(@"We are in handlesigneTap");
}

- (void)handleTap:(UITapGestureRecognizer *)sender{
    
//        [self performSelector:@selector(showSecondFeather) withObject:nil afterDelay:2.0];
    [self showSecondFeather];
    
    arabicWordButton.titleLabel.text = @"";
    lblArabicWord.text = @"";
    englishWordOrSentence.titleLabel.text = @"";
    
    id imgIdExtraction;
    if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
        imgIdExtraction = [_arabicWordDic objectForKey:[NSString stringWithFormat:@"imageId%d",sender.view.tag - 1]];
    }
    else {
        imgIdExtraction = [tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",sender.view.tag - 1]];
    }
    
//    id imgIdExtraction = [tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",sender.view.tag - 1]];
    int imgId = [imgIdExtraction intValue];
    if (!imgId || imgId == 0) {
        imgId = 1;
    }
    NSString *selectedImg = [NSString stringWithFormat:@"%d.png",imgId];        
//    [selectedImageView setImage:[UIImage imageNamed:selectedImg]];            
    [selectedImageView setImage:[DocumentDirectory imageNamed:selectedImg]];            
     NSLog(@"you selected tag number is : %d",sender.view.tag);
    
    NSString *englishText = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",sender.view.tag - 1]];

    if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
    englishText = @"";
    int engRef = [[_arabicWordDic objectForKey:[NSString stringWithFormat:@"englishRef%d",sender.view.tag - 1]] intValue];
    for (ps = 0; ps < [tempContentDic count]; ps++) {
        int engWordId = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",ps]] intValue];
        
        if (engRef == engWordId) {
           englishText = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",ps]]; 
            break;
            }
        }
    }
    

    if (englishText) {
        [englishWordOrSentence setEnabled:YES];
        [englishWordOrSentence setTitle:[NSString stringWithFormat:@"%@",englishText] forState:UIControlStateNormal];
        [englishWordOrSentence setTitle:[NSString stringWithFormat:@"%@",englishText] forState:UIControlStateSelected];
        englishWordOrSentence.titleLabel.text = englishText;
        englishWordOrSentence.titleLabel.textColor = [UIColor whiteColor];
    }
    else {
        [englishWordOrSentence setEnabled:NO];
        [englishWordOrSentence setTitle:[NSString stringWithFormat:@""] forState:UIControlStateNormal];
        [englishWordOrSentence setTitle:[NSString stringWithFormat:@""] forState:UIControlStateSelected]; 
        englishWordOrSentence.titleLabel.textColor = [UIColor whiteColor];

    }
    

    NSLog(@"Title label is %@ and text is %@:",englishWordOrSentence.titleLabel.text,englishText);
//    arabicWordButton.titleLabel = 
    NSString* arabicWord  = [_arabicWordDic objectForKey:[NSString stringWithFormat:@"arabicWord%d",sender.view.tag - 1]]; 
    
    
    if (activeLanguage == 2) {
        arabicWord = @"";
        int arabicRef = [[tempContentDic objectForKey:[NSString stringWithFormat:@"arabicRef%d",sender.view.tag - 1]] intValue];
        for (ps = 0; ps < [_arabicWordDic count]; ps++) {
            int arabicWordid = [[_arabicWordDic objectForKey:[NSString stringWithFormat:@"wordId%d",ps]] intValue];
            
            if (arabicRef == arabicWordid) {
                arabicWord = [_arabicWordDic objectForKey:[NSString stringWithFormat:@"arabicWord%d",ps]]; 
                break;
            }
        }
    }
    
    if (arabicWord) {
        [arabicWordButton setEnabled:YES];
    [arabicWordButton setTitle:arabicWord forState:UIControlStateNormal];        
    [arabicWordButton setTitle:arabicWord forState:UIControlStateSelected];
        [lblArabicWord setText:arabicWord];
    arabicWordButton.titleLabel.textColor = [UIColor whiteColor];
    }
    else {      
        [arabicWordButton setEnabled:NO];
        [arabicWordButton setTitle:[NSString stringWithFormat:@""] forState:UIControlStateNormal];        
        [arabicWordButton setTitle:[NSString stringWithFormat:@""] forState:UIControlStateSelected];
    }
    _currentWordIndex = sender.view.tag;
    
        [self charactersInteraction];

}

- (IBAction)GotoMainMenu:(id)sender {
    if (myCampPlayer) {
        [myCampPlayer stop];
        myCampPlayer = nil;
    }
    
    if (soundTimer) {        
        [soundTimer invalidate];
        soundTimer = nil;
        [soundTimer release];
    }
    
    if (cheeringAnimationTimer) {
        [cheeringAnimationTimer invalidate];
        cheeringAnimationTimer = nil;
        [cheeringAnimationTimer release];
    }
    
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        cheeringSoundTimer = nil;
        [cheeringSoundTimer release];
    }

    if (cheeringImgView) {
        [cheeringImgView setImage:Nil];
        [cheeringImgView removeFromSuperview];
        [cheeringImgView release];
        cheeringImgView = Nil;
    }
    
    if (charactersView) {
        [charactersView removeFromSuperview];
        [charactersView release];
        charactersView = nil;
    }

}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"mycamp" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) playSoundEnglish{
    
    if (!soundTimer) {
        soundTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                       target:self 
                                                     selector:@selector(stopTimerAndMakeSoundButtonsClickable) 
                                                     userInfo:nil 
                                                      repeats:YES]retain];
//        [arabicWordButton setEnabled:NO];
//        [englishWordOrSentence setEnabled:NO];
//        [checkButton setEnabled:NO];
//        [UndoButton setEnabled:NO];
    }


    
    arabicWordButton.titleLabel.textColor = [UIColor whiteColor];
    englishWordOrSentence.titleLabel.textColor = [UIColor whiteColor];
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }  
    
    
    NSString* soundName;
    
    if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
        soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",ps]];
        // ps is used to fetch sound based on arabic refrence instead of array indexing
    }
    else {
        soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",_currentWordIndex - 1]];
    }
    
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;    
    [player play];   
    
} 

- (IBAction)playEnglishSound:(id)sender {
    [self showThirdFeather];
    if (_currentWordIndex == 0) {
        UIAlertView *selectionAlert;
        selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly select a word" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
        [selectionAlert show];

        return;
    }
    if (activeLanguage == 2) {
        [self playSoundEnglish];
    }

}

- (IBAction)playArabicSound:(id)sender {
        [self showThirdFeather];
    if (_currentWordIndex == 0) {
//        UIAlertView *selectionAlert;
//        selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Kindly select a word" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
//        [selectionAlert show];

        return;
    }
    
    if (cId > 989 && cId < 999) {
        [self playSoundEnglish];
        return;
    }
        if (activeLanguage == 1) {
            [self playSoundArabic];
        }
}


-(void) playSoundArabic{
    
    if (!soundTimer) {
        soundTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                       target:self 
                                                     selector:@selector(stopTimerAndMakeSoundButtonsClickable) 
                                                     userInfo:nil 
                                                      repeats:YES]retain];
//        [arabicWordButton setEnabled:NO];
//        [englishWordOrSentence setEnabled:NO];
//        [checkButton setEnabled:NO];
//        [UndoButton setEnabled:NO];
    }
    
    
    
    arabicWordButton.titleLabel.textColor = [UIColor whiteColor];
    englishWordOrSentence.titleLabel.textColor = [UIColor whiteColor];
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }  
    

    NSString* soundName;
    if (activeLanguage == 2 && (cId < 989 || cId > 999)) {
        soundName = [_arabicWordDic objectForKey:[NSString stringWithFormat:@"arabicvoiceId%d",ps]]; 
        // ps is used to fetch sound based on arabic refrence instead of array indexing
    }
    else {
        soundName = [_arabicWordDic objectForKey:[NSString stringWithFormat:@"arabicvoiceId%d",_currentWordIndex - 1]];
    }
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;    
    [player play];
    
}


- (IBAction)HelpSound:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"MS005" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) stopTimerAndMakeSoundButtonsClickable{
    
    if (cheeringImgView) {
        NSLog(@"#Testing: Setting Cheering imgView setImage as Nil");
        [cheeringImgView setImage:Nil];
        NSLog(@"#Testing: Done as Setting Cheering imgView setImage as Nil");
        [cheeringImgView removeFromSuperview];
        [cheeringImgView release];
        cheeringImgView = Nil;
    }
    if (soundTimer) {
        
        [arabicWordButton setEnabled:YES];
        [englishWordOrSentence setEnabled:YES];
        [checkButton setEnabled:YES];
        [UndoButton setEnabled:YES];
        
        [soundTimer invalidate];
        soundTimer = nil;
        [soundTimer release];
    }
    
    if (cheeringSoundTimer) 
    {
        [cheeringSoundTimer invalidate];
        cheeringSoundTimer = nil;
        [cheeringSoundTimer release];
        [arabicWordButton setEnabled:YES];
        [englishWordOrSentence setEnabled:YES]; 
        [checkButton setEnabled:YES];
        [UndoButton setEnabled:YES];            
    }
    
    arabicWordButton.titleLabel.textColor = [UIColor whiteColor];
    englishWordOrSentence.titleLabel.textColor = [UIColor whiteColor];
    

}

- (IBAction)checkDraggedText:(id)sender {
    
    NSString *cellTxt = cellText.text;
    if ([cellTxt isEqualToString:Nil] || !cellTxt) {
        return;
    }
    // Copying to get rid of releasing string _correctText
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 1 && (cId < 989 || cId > 999)) {
//        _correctText = arabicWordButton.titleLabel.text;
        _correctText = lblArabicWord.text;
    }else {
        _correctText = englishWordOrSentence.titleLabel.text;
    }
    
    NSString *tempStoreCorrectWord = _correctText;
    if ( [tempStoreCorrectWord length] > 0){
        NSString *lastWord = [tempStoreCorrectWord substringFromIndex: [tempStoreCorrectWord length] - 1];
        if ([lastWord isEqualToString:@" "]) 
        {
            tempStoreCorrectWord = [tempStoreCorrectWord substringToIndex:[tempStoreCorrectWord length] - 1];        
        }
    }
    _correctText = tempStoreCorrectWord;
    if (isSentence) {
        _correctText = [NSString stringWithFormat:@"%@ ",_correctText];        
    }

    if( [_correctText isEqualToString:cellTxt])
    {
        NSLog(@"Correct!");
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1) {
                    cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2) {
                    cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_1.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3) {
                    cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4) {
                    cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_1.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5) {
                    cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6) {
                    cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300,159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 159, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_1.png"]];
        }
        

        
        


        [cheeringImgView setBackgroundColor:[UIColor clearColor]];
        [charactersView addSubview:cheeringImgView];
        
        cheeringAnimationCounter = 0;
        cheeringImageNumber = 1;
        cheeringAnimationTimer = [[NSTimer scheduledTimerWithTimeInterval:0.2
                                                                   target:self 
                                                                 selector:@selector(changeImage) 
                                                                 userInfo:nil 
                                                                  repeats:YES]retain]; 
        [self playCheeringSound];
    }
    else {
        
        cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 152, 120, 120)];
        [cheeringImgView setImage:[UIImage imageNamed:@"X.png"]];
        [charactersView addSubview:cheeringImgView];
        [self playEncouragingSound];
        NSLog(@"Wrong Answer");
    }
}

- (IBAction)ResetText:(id)sender {
    
    if ([cellText.text length] > 0) {
        
        
        NSString *lastCharacter = [cellText.text substringFromIndex: [cellText.text length] - 1];
        if (isSentence) {
            lastCharacter = [_completeWordArray lastObject];
        }
        [_completeWordArray removeLastObject];
        [_completeWordArray removeLastObject];
        
        if ( [cellText.text length] > 0 && !isSentence)
            cellText.text = [cellText.text substringToIndex:[cellText.text length] - 1];
        
        if (isSentence && [cellText.text length] > 0) {
            NSLog(@"cell Text is %@",cellText.text);
            
            cellText.text = [cellText.text stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ",lastCharacter] withString:@""];            
        
            
        }
        
        // adding character now
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 128 + yLine, 60, 62)];
        UIFont *font = [UIFont fontWithName:@"Futura" size:45];
//                [label1 setFont:[UIFont boldSystemFontOfSize:45]];
        [label1 setFont:font];
        int sentenceCheck;
        if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
            sentenceCheck = [[_arabicWordDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_currentWordIndex-1]] intValue];
        }
        else {
            sentenceCheck = [[tempContentDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_currentWordIndex-1]] intValue];
        }
        
        
        if (sentenceCheck == 1) {
            label1.frame = CGRectMake(10+count, 100+ yLine, 150, 48);
//                                [label1 setFont:[UIFont boldSystemFontOfSize:35]];
                    UIFont *font = [UIFont fontWithName:@"Futura" size:35];
            label1.font = font;
        }
        
        if (isSentence) {

        }
        label1.text = lastCharacter;
//        [label1 setFont:[UIFont fontWithName:@"Arial" size:25]];
//        UIFont* font = [UIFont systemFontOfSize: 20];

//        [label1 setFont:font];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        else {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        
        
        
        
        label1.textAlignment = UITextAlignmentCenter;
        label1.layer.borderWidth = 1.0;
        label1.layer.cornerRadius = 8;
        
        label1.userInteractionEnabled = YES;
        
        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc] 
                                            initWithTarget:self 
                                            action:@selector(labelDragging:)] autorelease];
        [label1 addGestureRecognizer:gesture];
        [charactersView addSubview:label1];
        
        count = count + 65;
        if (isSentence) {
            count = count + 100;
        }
        if (count > 950) {
            if (isSentence) {
                yLine = yLine + 45;
            }
            else{
                yLine = yLine + 75;
            }
            
            count = 10;
        }
        
        if (count == 10 && yLine > 46) {
            if (isSentence) {
                yLine = 20;
            }
            else{
                yLine = 1;
            }
            count = 10;
        }
    }
}

- (void)MyCampImageTap:(id)sender {

    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case 1:
            NSLog(@"Erase IT");
            break;
        case 2:
            NSLog(@"Select IT");
            break;
        case 3:
            NSLog(@"Fix IT");
            break;
        case 4:
            NSLog(@"Write IT");
            break;
        default:
            break;
    }
    
    
}
-(void) charactersInteraction{
    
    cellText.text = @"";
    
    if (_completeWordArray) {
        [_completeWordArray removeAllObjects];
    }
    
    if (charactersView) {
        [charactersView removeFromSuperview];
        [charactersView release];
        charactersView = nil;        
    }
    
    charactersView = [[UIView alloc] initWithFrame:CGRectMake(0, 346, 1024, 300)];
    [charactersView setBackgroundColor:[UIColor clearColor]];
    charactersView.userInteractionEnabled = YES;
    [self.view addSubview:charactersView];
    
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 1 && (cId < 989 || cId > 999)) {
        _correctText = lblArabicWord.text;
    }else {
        _correctText = englishWordOrSentence.titleLabel.text;
    }

    
    NSString *tempStoreCorrectWord = _correctText;
    if ( [tempStoreCorrectWord length] > 0){
    NSString *lastWord = [tempStoreCorrectWord substringFromIndex: [tempStoreCorrectWord length] - 1];
    if ([lastWord isEqualToString:@" "]) 
        {
            tempStoreCorrectWord = [tempStoreCorrectWord substringToIndex:[tempStoreCorrectWord length] - 1];        
        }
    }
    
    _correctText = tempStoreCorrectWord;
    NSLog(@"correct text is %@",_correctText);
    
    int noOfCharacters;
    isSentence = false;
    
    int sentenceCheck;
    int translationCheck;
    if ((activeLanguage == 1 && cId < 989) ||  (activeLanguage == 1 && cId > 999)) {
        sentenceCheck = [[_arabicWordDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_currentWordIndex-1]] intValue];
        translationCheck = [[_arabicWordDic objectForKey:[NSString stringWithFormat:@"translate%d",_currentWordIndex-1]] intValue];
    }
    else {
        sentenceCheck = [[tempContentDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_currentWordIndex-1]] intValue];
        translationCheck = [[tempContentDic objectForKey:[NSString stringWithFormat:@"translate%d",_currentWordIndex-1]] intValue];
    }
    
//    if (sentenceCheck == 1) 
//    { // sentenceCheck 1 means its sentence and zero means its word
        if(LanguageSupport == 1)
        { // language support 1 means ON and language support zero means off
            if (translationCheck == 1) 
            { // translateCheck 1 means its support translation, else it doesnot.                
                NSLog(@"It supports translation of sentence");
            }
            else 
            {
                if(activeLanguage == 2)
                    { // Active Language 2 means itsEnglish, and 1 means its Arabic
//                        [arabicWordButton removeFromSuperview];
                        [arabicWordButton setEnabled:NO];
                    }
                else
                    {
                        if (cId > 989 && cId < 999) {
                            // skip in case of my camp
                        }
                        else {
                            [englishWordOrSentence setEnabled:NO];
                        }

                    }        
            }                
        }
//    }
    
    if (sentenceCheck == 1) {
        noOfCharacters =  [[_correctText componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] count];
        isSentence = TRUE;
        
    }
    else {
        noOfCharacters = _correctText.length;
    }
    
    
    if (characterDic) {
        [characterDic removeAllObjects];
    }
    
    
    // populatin character array with words/ characters
    NSMutableArray *charactersArray = [[NSMutableArray alloc] initWithCapacity:noOfCharacters]; // level - 1 is because we have by default level one and we dnt want extra word in level one
    if (isSentence) {    
        NSArray *sentenceArray = [_correctText componentsSeparatedByString: @" "];
        [charactersArray addObjectsFromArray:sentenceArray];                
    }
    else {
        
        unichar characters; // initializng characters
        for (int k = 0; k < noOfCharacters ; k++) {
            characters = [_correctText characterAtIndex:k]; // extracting character at any index of string
            // check for unicodes here and button for the check
            if (1 == 2) {
                NSLog(@"Removing fatah");
            }
            else{
                NSString * charactr = [NSString stringWithFormat:@"%C", characters]; // // changing character to string
                NSLog(@"Character at index %d is %@",k,charactr);
                [charactersArray addObject:charactr]; // inserting string in an array
            }
        }
    }
    
   
//    [charactersArray removeObjectIdenticalTo:@","];
    
    count = 10;
    yLine = 1;
    int randomCounter = noOfCharacters;
    if (noOfCharacters < 44) {
        yLine = yLine + 75;
    }
    
    for (int n = 0; n< noOfCharacters; n++) {                
        int randomNumer = arc4random() % randomCounter ;
        NSString *characterExtracted = [charactersArray objectAtIndex:randomNumer];
        [charactersArray removeObjectAtIndex:randomNumer];            
        [characterDic removeObjectForKey:[NSString stringWithFormat:@"%d",randomNumer]];
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 128 + yLine, 60, 62)];
                UIFont *font = [UIFont fontWithName:@"Futura" size:45];
                [label1 setFont:font];
        if (isSentence) {
            label1.frame = CGRectMake(10+count, 100 + yLine, 150, 48);
            UIFont *font = [UIFont fontWithName:@"Futura" size:35];
            [label1 setFont:font];

//                    [label1 setFont:[UIFont boldSystemFontOfSize:35]];
        }
        label1.textAlignment = UITextAlignmentCenter;
        label1.text = characterExtracted;
        label1.tag = n;

//        [label1 setFont:font];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
        else {
            [label1 setBackgroundColor:[UIColor greenColor]];
            label1.textColor = [UIColor blackColor];
            label1.layer.borderColor = [UIColor greenColor].CGColor;
        }
                                
        label1.textAlignment = UITextAlignmentCenter;
        label1.layer.borderWidth = 1.0;
        label1.layer.cornerRadius = 8;
        
        label1.userInteractionEnabled = YES;
        
        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc] 
                                            initWithTarget:self 
                                            action:@selector(labelDragging:)] autorelease];
        [label1 addGestureRecognizer:gesture];
        [charactersView addSubview:label1];
        
        count = count + 65;
        if (isSentence) {
            count = count + 100;
        }
        if (count > 950) {
            if (isSentence) {
                yLine = yLine + 45;
            }
            else{
                yLine = yLine + 75;
            }
            
            count = 10;
        }
        randomCounter = randomCounter - 1;
    }
    
    if (activeLanguage == 2) {
    
    [_lblOpposite setText:[NSString stringWithFormat:@"%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"opposite%d",_currentWordIndex - 1]]]];
    
    [_lblSynonym setText:[NSString stringWithFormat:@"Meaning :%@",[tempContentDic objectForKey:[NSString stringWithFormat:@"synonym%d",_currentWordIndex - 1]]]];
    
    if([_lblSynonym.text isEqualToString:@"0"] || [_lblSynonym.text isEqualToString:@"Meaning :"]){
        [_lblSynonym setHidden:YES];
    }
    
    if([_lblOpposite.text isEqualToString:@"0"]){
        [_lblOpposite setHidden:YES];
    }
    else{
        [_lblOpposite setHidden:NO];
    }

    }
    else{
        [_lblOpposite setText:[NSString stringWithFormat:@"%@",[_arabicWordDic objectForKey:[NSString stringWithFormat:@"opposite%d",_currentWordIndex - 1]]]];
        
        [_lblSynonym setText:[NSString stringWithFormat:@"Meaning :%@",[_arabicWordDic objectForKey:[NSString stringWithFormat:@"synonym%d",_currentWordIndex - 1]]]];
        
        if([_lblSynonym.text isEqualToString:@"0"] || [_lblSynonym.text isEqualToString:@"Meaning :"]){
            [_lblSynonym setHidden:YES];
        }
        
        if([_lblOpposite.text isEqualToString:@"0"]){
            [_lblOpposite setHidden:YES];
    }
        else{
            [_lblOpposite setHidden:NO];
        }
    }
    
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    if (cId > 989) {
        _lblOpposite.hidden = YES;
        _lblSynonym.hidden = YES;
    }
    
}
- (void)labelDragging:(UIPanGestureRecognizer *)gesture
    {
        UILabel *label2 = (UILabel *)gesture.view;
        
        CGPoint translation = [gesture translationInView:label2];
        
        // move label
        label2.center = CGPointMake(label2.center.x + translation.x, 
                                    label2.center.y + translation.y);
        
//        [label2 setFont:[UIFont fontWithName:@"Arial" size:42]];
//        UIFont *font = [UIFont systemFontOfSize: 20];
//                        [label2 setFont:[UIFont boldSystemFontOfSize:35]];
        UIFont *font = [UIFont fontWithName:@"Futura" size:35];
        [label2 setFont:font];

//        [label2 setFont:font];
        
        NSLog(@"%f", [gesture locationInView:label2.superview].x);
        NSLog(@"%f", [gesture locationInView:label2.superview].y);
        
        int xLowerLimit = 50; // 113
        int yLowerLimit = 50; // 269
        int xUpperLimit = 50+590; // 654
        int yUpperLimit = 50+50; //345
        if ([gesture locationInView:label2.superview].x > xLowerLimit && [gesture locationInView:label2.superview].x < xUpperLimit && [gesture locationInView:label2.superview].y > yLowerLimit && [gesture locationInView:label2.superview].y < yUpperLimit) {
            [label2 removeFromSuperview];
            [_completeWordArray addObject:label2.text];
            NSString *guessWord = @"";
            int numberOfCharacters = [_completeWordArray count];
            for (int k = 0; k < numberOfCharacters; k++) {
                if (isSentence) {
                    guessWord = [guessWord stringByAppendingFormat:[NSString stringWithFormat:@"%@ ",[_completeWordArray objectAtIndex:k]]];
                }
                else {
                    guessWord = [guessWord stringByAppendingFormat:[NSString stringWithFormat:@"%@",[_completeWordArray objectAtIndex:k]]];
                }
                k = k+1;
            }
            
            cellText.text = guessWord;
//         [self checkDraggedText:Nil]
            
        }
        
        [gesture setTranslation:CGPointZero inView:label2];
        
        if(gesture.state == UIGestureRecognizerStateEnded)
        {
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 128 + yLine, 60, 62)];
//            [label1 setFont:[UIFont boldSystemFontOfSize:45]];
            UIFont *font = [UIFont fontWithName:@"Futura" size:45];
            [label1 setFont:font];

            if (isSentence) {
                label1.frame = CGRectMake(10 + count, 100 + yLine, 150, 48);
                UIFont *font = [UIFont fontWithName:@"Futura" size:35];
                [label1 setFont:font];

//                [label1 setFont:[UIFont boldSystemFontOfSize:35]];
            }
            label1.text = label2.text;
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
                [label1 setBackgroundColor:[UIColor greenColor]];
                label1.textColor = [UIColor blackColor];
                label1.layer.borderColor = [UIColor greenColor].CGColor;
            }
            else {
                [label1 setBackgroundColor:[UIColor greenColor]];
                label1.textColor = [UIColor blackColor];
                label1.layer.borderColor = [UIColor greenColor].CGColor;
            }                                
            label1.textAlignment = UITextAlignmentCenter;
            label1.layer.borderWidth = 1.0;
            label1.layer.cornerRadius = 8;        
            label1.userInteractionEnabled = YES;        
            UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc] 
                                                initWithTarget:self 
                                                action:@selector(labelDragging:)] autorelease];
            [label1 addGestureRecognizer:gesture];
            [charactersView addSubview:label1];
            
            count = count + 65;
            if (isSentence) {
                count = count + 100;
            }
            if (count > 950) {
                if (isSentence) {
                    yLine = yLine + 45;
                }
                else{
                    yLine = yLine + 75;
                }
                
                count = 10;
            }
            
            if (count == 10 && yLine > 46) {
                if (isSentence) {
                    yLine = 20;
                }
                else{
                    yLine = 1;
                }
                

                count = 10;
            }
            [label2 removeFromSuperview];
        }     
}

-(void) changeImage{
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_7.png"]];
                
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_8.png"]];
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 1;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_5.png"]];
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 9) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_6.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 8:
                cheeringImageNumber = 9;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_9.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 9:
                cheeringImageNumber = 10;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 10:
                cheeringImageNumber = 11;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 11:
                cheeringImageNumber = 1;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 18) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 10) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    
    // Chef Boy
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // CHEF GIRL
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // FIRE
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // Fire Girl
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // Football Boy
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    //ARAB BOY
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // POLICE BOY
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    
    
    
}


-(void) playCheeringSound{
    
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                       target:self 
                                                     selector:@selector(stopTimerAndMakeSoundButtonsClickable) 
                                                     userInfo:nil 
                                                      repeats:YES]retain];
        [arabicWordButton setEnabled:NO];
        [englishWordOrSentence setEnabled:NO]; 
        [checkButton setEnabled:NO];
        [UndoButton setEnabled:NO];
        
    }
    
    int randomNumer = arc4random() % 3 ;
    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        if (squishPath == nil || !squishPath) {
            NSLog(@"Sound NOt found");
            NSString *soundNameNil = @"EmptySound";
            NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundNameNil ofType:@"mp3"];
            NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
            SystemSoundID squishSoundID;
            AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
            AudioServicesPlaySystemSound(squishSoundID);            
            return;            
        }

        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);

        
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        if (squishPath == nil || !squishPath) {
            NSLog(@"Sound NOt found");
            NSString *soundNameNil = @"EmptySound";
            NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundNameNil ofType:@"mp3"];
            NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
            SystemSoundID squishSoundID;
            AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
            AudioServicesPlaySystemSound(squishSoundID);            
            return;            
        }
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);

        
        
    }
    
}

-(void) playEncouragingSound{
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                               target:self 
                                                             selector:@selector(stopTimerAndMakeSoundButtonsClickable) 
                                                             userInfo:nil 
                                                              repeats:YES]retain];
        [arabicWordButton setEnabled:NO];
        [englishWordOrSentence setEnabled:NO]; 
        [checkButton setEnabled:NO];
        [UndoButton setEnabled:NO];
        
    }
    
    int randomNumer = arc4random() % 3 ;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC25";
        NSString *second = @"EC30";
        NSString *third = @"EC31";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);  
        
    }else {
        NSString *first = @"AC04";
        NSString *second = @"AC05";
        NSString *third = @"AC06";
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
    
    }
}

-(void) playBackGroundMusic{
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    
    NSString* fileName = [NSString stringWithFormat:@"2-Camp"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    myCampPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    myCampPlayer.volume = 0.15;
    myCampPlayer.numberOfLoops = 20;
    myCampPlayer.delegate = self;
    
    if ([myCampPlayer prepareToPlay]) {
        [myCampPlayer setCurrentTime:0.0];
        [myCampPlayer play];            
    }
}

- (void)dealloc {
    [backgroundImgView release];
    [_correctText release];
    
    [checkButton release];
    [UndoButton release];
    [_imgEnglishEar release];
    [_imgArabicEar release];
    [super dealloc];
}
-(void) showFeather:(int) feather{
    switch (feather) {
        case FIRST_FEATHER:
        {
            imgView1Feather.hidden = NO;
            imgView2Feather.hidden = YES;
            imgView3Feather.hidden = YES;
            imgView4Feather.hidden = YES;
        }
            break;
        case SECOND_FEATHER:
        {
            imgView1Feather.hidden = YES;
            imgView2Feather.hidden = NO;
            imgView3Feather.hidden = YES;
            imgView4Feather.hidden = YES;
        }
            break;
            
        case THIRD_FEATHER:
        {
            imgView1Feather.hidden = YES;
            imgView2Feather.hidden = YES;
            imgView3Feather.hidden = NO;
            imgView4Feather.hidden = YES;
        }
            break;
            
        case FOURTH_FEATHER:
        {
            imgView1Feather.hidden = YES;
            imgView2Feather.hidden = YES;
            imgView3Feather.hidden = YES;
            imgView4Feather.hidden = NO;
        }
            break;
            
        default:
            break;
    }
    
}

@end
