//
//  SetupViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/23/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "SetupViewController.h"
#import "MovieViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface SetupViewController ()

@end

@implementation SetupViewController
@synthesize grades,VideoHelp = _VideoHelp;
@synthesize txtFieldPinNumber = _txtFieldPinNumber;
@synthesize txtFieldName = _txtFieldName;
@synthesize imgViewBackground = _imgViewBackground;
@synthesize btnBoy = _btnBoy;
@synthesize btnGirl = _btnGirl;
@synthesize characterScrollView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.grades = [[NSMutableArray alloc] init];
    NSString* gradesPurchased = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"gradesPurchased"]];
    if (gradesPurchased.length < 5 || [gradesPurchased isEqualToString:@"free"]) {
        gradesPurchased = @"FREE";
        [self.grades addObject:gradesPurchased];
    }
    else {
        [self.grades removeAllObjects];
        NSArray *componentsArr = [[NSArray alloc] init];
        componentsArr = [gradesPurchased componentsSeparatedByString: @" "];           
        [self.grades addObjectsFromArray:componentsArr];
        [self.grades removeObject:@"G1SKILLS"];
        [self.grades removeObject:@"G2SKILLS"];
        [self.grades removeObject:@"G3SKILLS"];
        [self.grades removeObject:@"G4SKILLS"];
    }
    
    int backgroundImageMyCamp = [[NSUserDefaults standardUserDefaults] integerForKey:@"MyCampBackground"];
    if (backgroundImageMyCamp == 1) {

        [BtnMyCampBlue setBackgroundImage:nil forState:UIControlStateNormal];
        [BtnMyCampPink setBackgroundImage:[UIImage imageNamed:@"selected_two.png"] forState:UIControlStateNormal];
        [BtnMyCampPink setEnabled:NO];
        [BtnMyCampBlue setEnabled:YES];

    }
    else   {              
        [BtnMyCampBlue setBackgroundImage:[UIImage imageNamed:@"selected_one.png"] forState:UIControlStateNormal];
        [BtnMyCampPink setBackgroundImage:nil forState:UIControlStateNormal];
        [BtnMyCampBlue setEnabled:NO];
        [BtnMyCampPink setEnabled:YES];
    
    }

    
    int backgroundImageMyPlayground = [[NSUserDefaults standardUserDefaults] integerForKey:@"MyPlaygroundBackground"];
    if (backgroundImageMyPlayground == 1) {
        
        [BtnMyPlaygroundPink setBackgroundImage:[UIImage imageNamed:@"selected_two.png"] forState:UIControlStateNormal];
        [BtnMyPlaygroundBlue setBackgroundImage:nil forState:UIControlStateNormal];

        [BtnMyPlaygroundPink setEnabled:NO];
        [BtnMyPlaygroundBlue setEnabled:YES];

    }
    else {
        [BtnMyPlaygroundBlue setEnabled:NO];
        [BtnMyPlaygroundPink setEnabled:YES];
        [BtnMyPlaygroundPink setBackgroundImage:nil forState:UIControlStateNormal];
        [BtnMyPlaygroundBlue setBackgroundImage:[UIImage imageNamed:@"selected_one.png"] forState:UIControlStateNormal];
        
    }
    
    NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];    
    Name.text = name;
 
    int gender = [[NSUserDefaults standardUserDefaults] integerForKey:@"gender"];
        
//    if (gender == 0) {
//        
//    }
    if (gender == 2 || gender == 4 || gender == 6 || gender == 8 || gender == 10 || gender == 12 || gender == 14 || gender == 16) {
        [_imgViewBackground setImage:[UIImage imageNamed:@"My_home_setup-final-girl-area-selected.png"]];
        if (gender == 2) {
            [_btnGirl setBackgroundImage:[UIImage imageNamed:@"GCheering_12.png"] forState:UIControlStateNormal];            
        }
        
        if (gender == 4) {
            [_btnGirl setBackgroundImage:[UIImage imageNamed:@"BCheering-girl_5.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 6) {
            [_btnGirl setBackgroundImage:[UIImage imageNamed:@"DCheering-girl_5.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 8) {
            [_btnGirl setBackgroundImage:[UIImage imageNamed:@"CCheering-girl_5.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 10) {
                [_btnGirl setBackgroundImage:[UIImage imageNamed:@"FireCheering-girl_5.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 12) {
                [_btnGirl setBackgroundImage:[UIImage imageNamed:@"FCheering-girl_5.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 14) {
                [_btnGirl setBackgroundImage:[UIImage imageNamed:@"ACheering-girl_5.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 16) {
                [_btnGirl setBackgroundImage:[UIImage imageNamed:@"PCheering-girl_5.png"] forState:UIControlStateNormal];
            
        }
        [_btnBoy setBackgroundImage:Nil forState:UIControlStateNormal];
        
        
        
    }
    else {
        if (gender == 1) {
            [_btnBoy setBackgroundImage:[UIImage imageNamed:@"Cheering_8.png"] forState:UIControlStateNormal];
        }
        
        if (gender == 3) {
            [_btnBoy setBackgroundImage:[UIImage imageNamed:@"BCheering_11.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 5) {
            [_btnBoy setBackgroundImage:[UIImage imageNamed:@"DCheering_11.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 7) {
            [_btnBoy setBackgroundImage:[UIImage imageNamed:@"CCheering_11.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 9) {
            [_btnBoy setBackgroundImage:[UIImage imageNamed:@"FireCheering_11.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 11) {
            [_btnBoy setBackgroundImage:[UIImage imageNamed:@"FCheering_11.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 13) {
            [_btnBoy setBackgroundImage:[UIImage imageNamed:@"ACheering_11.png"] forState:UIControlStateNormal];
            
        }
        
        if (gender == 15) {
            [_btnBoy setBackgroundImage:[UIImage imageNamed:@"PCheering_11.png"] forState:UIControlStateNormal];
            
        }
        
        [_btnGirl setBackgroundImage:Nil forState:UIControlStateNormal];

        [_imgViewBackground setImage:[UIImage imageNamed:@"My_home_setup-final-boy-area-selected.png"]];
    }
    
    if (gender == 0) {
        [_btnBoy setBackgroundImage:[UIImage imageNamed:@"Cheering_8.png"] forState:UIControlStateNormal];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 2) {
        languageSegment.selectedSegmentIndex = 0;
    }
    else{
        languageSegment.selectedSegmentIndex = 1;
    }
//    }     = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] - 1 ;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:SYNONYM_ACTIVE] == true) {
        [LSSwitch setOn:YES animated:YES];        
    }
    else{
        [LSSwitch setOn:NO animated:YES];    
    }
    
      NSString *selectedGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    if (selectedGrade){
        [BtnGrade setTitle:[NSString stringWithFormat:@"Current Grade : %@",selectedGrade] forState:UIControlStateNormal];
    }else {
        [BtnGrade setTitle:[NSString stringWithFormat:@"Current Grade : FREE"] forState:UIControlStateNormal];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == YES) {
        [_VideoHelp setSelectedSegmentIndex:0];
    }
    else{
        [_VideoHelp setSelectedSegmentIndex:1];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"] == YES) {
        [_backgroundMusicSegmentControl setSelectedSegmentIndex:0];
    }
    else{
        [_backgroundMusicSegmentControl setSelectedSegmentIndex:1];
    }
    
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"harkat"] == NO) {
        [_noTashkeelSegmentedControl setSelectedSegmentIndex:0];
    }
    else{
        [_noTashkeelSegmentedControl setSelectedSegmentIndex:1];
    }
}

-(void) viewWillAppear:(BOOL)animated{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"]) {
        _txtFieldPinNumber.text = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"PINNUMBER"]];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 613) {
        if (buttonIndex == 0) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelp"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMyHomeSetup"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffAchievements"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMyCamp"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMyHomeAddword"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffPlayground"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffSkills"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffCityMap"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffFixIT"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMainScreenFirstVideo"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMatchIt"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMoveIT"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMyPlayground"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffPickIT"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffPOPIT"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffSpinIt"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffTypeIT"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffWriteIt"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffFootballVideo"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffhockeyVideo"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffBasketballVideo"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    if (alertView.tag == 5001) {
        if (buttonIndex == 0) {
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"CostumesIdentifier"];
            vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];
        }
    }

    
    
}

- (IBAction)segmentPressedVideoHelp:(id)sender {
    

    
    
    
    UISegmentedControl *seg = sender;

    if (seg.selectedSegmentIndex == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Video Help"];
        
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        if (activeLanguage == 1) {
            [alert setMessage:@"هل انت متأكد من تشغيل فيديو المساعدة؟"];
        }
        else{
            [alert setMessage:@"Are you sure to turn on video help?"];
        }
        
        [alert addButtonWithTitle:@"YES"];
        [alert addButtonWithTitle:@"NO"];
        [alert setDelegate:self];
        [alert show];
        alert.tag = 613;
        [alert release];

    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMyHomeSetup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffAchievements"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMyCamp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMyHomeAddword"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffPlayground"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffSkills"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffCityMap"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffFixIT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMainScreenFirstVideo"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMatchIt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMoveIT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffMyPlayground"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffPickIT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffPOPIT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffSpinIt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffTypeIT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelpOffWriteIt"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
}

- (void)viewDidUnload
{
    [languageSegment release];
    languageSegment = nil;
    [LSSwitch release];
    LSSwitch = nil;
    [Name release];
    Name = nil;
    [Age release];
    Age = nil;
    [BtnMyCampBlue release];
    BtnMyCampBlue = nil;
    [BtnMyCampPink release];
    BtnMyCampPink = nil;
    [BtnMyPlaygroundBlue release];
    BtnMyPlaygroundBlue = nil;
    [BtnMyPlaygroundPink release];
    BtnMyPlaygroundPink = nil;
    [BtnGrade release];
    BtnGrade = nil;
    [self setVideoHelp:nil];
    [self setTxtFieldPinNumber:nil];
    [self setTxtFieldName:nil];
    [self setBackgroundMusicSegmentControl:nil];
    [self setNoTashkeelSegmentedControl:nil];
    _imgViewBackground = nil;
    [self setImgViewBackground:nil];
    [self setBtnBoy:nil];
    [self setBtnGirl:nil];
    [self setCharacterScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)noHarkatPressed:(id)sender {
    UISegmentedControl *seg = sender;
    
    if (seg.selectedSegmentIndex == 0) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"harkat"];
        [[NSUserDefaults standardUserDefaults] synchronize];        
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"harkat"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)backgroundMusicStatusChangePressed:(id)sender {
    
    UISegmentedControl *seg = sender;
    
    if (seg.selectedSegmentIndex == 0) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allBackgroundMusic"];
        [[NSUserDefaults standardUserDefaults] synchronize];        
    }
    else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"allBackgroundMusic"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)SelectLanguage:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentGrammarRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentPrincipleRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentGrammarString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentPrincipleString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

    UISegmentedControl *seg = sender;
    if(LSSwitch.on){
        NSLog(@"Switch is on");
    }
    else {
        NSLog(@"Switch is off");
    }
    
    if (seg.selectedSegmentIndex == 0) {
        NSLog(@"English Language Selected");

        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"ActiveLanguage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:SYNONYM_ACTIVE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else if (seg.selectedSegmentIndex == 1) {

        NSLog(@"Arabic Language Selected");
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"ActiveLanguage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        

    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:SYNONYM_ACTIVE];
    [[NSUserDefaults standardUserDefaults] synchronize];


    }
}

- (IBAction)LanguageSupportSwitch:(id)sender {

    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentGrammarRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentPrincipleRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentGrammarString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentPrincipleString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if(LSSwitch.on && languageSegment.selectedSegmentIndex == 1){
        NSLog(@"Switch is on");
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:SYNONYM_ACTIVE];
        [[NSUserDefaults standardUserDefaults] synchronize];        
    }
    else {
        NSLog(@"Switch is off");
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:SYNONYM_ACTIVE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)BoySelected:(id)sender {
    
    [_imgViewBackground setImage:[UIImage imageNamed:@"My_home_setup-final-boy-area-selected.png"]];

    [self createScrollView];
    
    [_btnGirl setBackgroundImage:Nil forState:UIControlStateNormal];
    [_btnBoy setBackgroundImage:[UIImage imageNamed:@"Cheering_8.png"] forState:UIControlStateNormal];
    
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];


}

- (IBAction)GirlSelected:(id)sender {
    
    [_imgViewBackground setImage:[UIImage imageNamed:@"My_home_setup-final-girl-area-selected.png"]];
        [self createScrollViewGirl];
    [_btnBoy setBackgroundImage:Nil forState:UIControlStateNormal];
    [_btnGirl setBackgroundImage:[UIImage imageNamed:@"GCheering_12.png"] forState:UIControlStateNormal];
    
    [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];

  
}

-(void) createScrollViewGirl{
    
    NSArray *viewsToRemove = [characterScrollView subviews];
    for (UIView *v in viewsToRemove) [v removeFromSuperview];
    
    NSArray* charactersArray = [[NSArray alloc] initWithObjects:@"GCheering_12.png",@"BCheering-girl_5.png",@"DCheering-girl_5.png",@"CCheering-girl_5.png",@"FireCheering-girl_5.png",@"FCheering-girl_5.png",@"ACheering-girl_5.png",@"PCheering-girl_5.png", nil];
    
    NSUInteger i;
    int xCoord=0;
    int yCoord=10;
    int buttonWidth=60;
    int buttonHeight=100;
    int buffer = 5;
    for (i = 0; i < [charactersArray count]; i++)
    {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [aButton setBackgroundImage:[UIImage imageNamed:[charactersArray objectAtIndex:i]] forState:UIControlStateNormal];
        
        if (i == 0) {
            [[aButton layer] setBorderWidth:2.0f];
            [[aButton layer] setBorderColor:[UIColor greenColor].CGColor];
            [[aButton layer] setCornerRadius:8.0f];
            [[aButton layer] setMasksToBounds:YES];
        }
        else{
            [[aButton layer] setBorderWidth:2.0f];
            [[aButton layer] setBorderColor:[UIColor redColor].CGColor];
            [[aButton layer] setCornerRadius:8.0f];
            [[aButton layer] setMasksToBounds:YES];
        }
        
        aButton.tag = i + 10;
        aButton.frame     = CGRectMake(xCoord, yCoord,buttonWidth,buttonHeight );
        [aButton addTarget:self action:@selector(selectCharactorTapped:) forControlEvents:UIControlEventTouchUpInside];
        [characterScrollView addSubview:aButton];
        
        xCoord += buttonWidth + buffer;
    }
    [characterScrollView setContentSize:CGSizeMake(xCoord, 50)];
  
}

-(void) createScrollView{

    NSArray *viewsToRemove = [characterScrollView subviews];
    for (UIView *v in viewsToRemove) [v removeFromSuperview];

    
    NSArray* charactersArray = [[NSArray alloc] initWithObjects:@"Cheering_8.png",@"BCheering_11.png",@"DCheering_11.png",@"CCheering_11.png",@"FireCheering_11.png",@"FCheering_11.png",@"ACheering_11.png",@"PCheering_11.png", nil];
    
    NSUInteger i;
    int xCoord=0;
    int yCoord=0;
    int buttonWidth=60;
    int buttonHeight=100;
    int buffer = 5;
    for (i = 0; i < [charactersArray count]; i++)
    {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [aButton setBackgroundImage:[UIImage imageNamed:[charactersArray objectAtIndex:i]] forState:UIControlStateNormal];

        if (i == 0) {
            [[aButton layer] setBorderWidth:2.0f];
            [[aButton layer] setBorderColor:[UIColor greenColor].CGColor];
            [[aButton layer] setCornerRadius:8.0f];
            [[aButton layer] setMasksToBounds:YES];
        }
        else{
            [[aButton layer] setBorderWidth:2.0f];
            [[aButton layer] setBorderColor:[UIColor redColor].CGColor];
            [[aButton layer] setCornerRadius:8.0f];
            [[aButton layer] setMasksToBounds:YES];
        }
        
        aButton.tag = i + 10;
        aButton.frame     = CGRectMake(xCoord, yCoord,buttonWidth,buttonHeight );
        [aButton addTarget:self action:@selector(selectCharactorTapped:) forControlEvents:UIControlEventTouchUpInside];
        [characterScrollView addSubview:aButton];
        
        xCoord += buttonWidth + buffer;
    }
    [characterScrollView setContentSize:CGSizeMake(xCoord, 50)];
    
}

-(void) selectCharactorTapped:(id) sender{
    int tag = [sender tag];
    if (tag > 10) {
        UIAlertView *alert = [[UIAlertView alloc] init];
        alert.tag = 5001;
        [alert setTitle:@"Unlock Character"];
        [alert setMessage:@"You need 350 points to unlock character"];
        [alert addButtonWithTitle:@"Unlock Now"];
        [alert addButtonWithTitle:@"Later"];
        [alert setDelegate:self];
        [alert show];
        [alert release];
    }
}

- (IBAction)NameTxt:(id)sender {

    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",Name.text] forKey:@"name"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"myhome" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)MyCampBlueBackground:(id)sender {
    
//    [BtnMyCampBlue setBackgroundColor:[UIColor grayColor]];
    [BtnMyCampBlue setBackgroundImage:[UIImage imageNamed:@"blue_btn_checked.png"] forState:UIControlStateNormal];
    [BtnMyCampBlue setEnabled:NO];
    [BtnMyCampPink setEnabled:YES];
    [BtnMyCampPink setBackgroundImage:nil forState:UIControlStateNormal];
//    [BtnMyCampPink setBackgroundColor:[UIColor clearColor]];
    
    [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"MyCampBackground"]; 
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)MyCampPinkBackground:(id)sender {
    
    [BtnMyCampBlue setBackgroundImage:nil forState:UIControlStateNormal];
//    [BtnMyCampPink setBackgroundColor:[UIColor grayColor]];
    [BtnMyCampPink setEnabled:NO];
    [BtnMyCampBlue setEnabled:YES];
//    [BtnMyCampBlue setBackgroundColor:[UIColor clearColor]];
    [BtnMyCampPink setBackgroundImage:[UIImage imageNamed:@"blue_btn_checked.png"] forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"MyCampBackground"]; 
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)MyPlaygroundBlueBackground:(id)sender {
    [BtnMyPlaygroundBlue setBackgroundImage:[UIImage imageNamed:@"selected_one.png"] forState:UIControlStateNormal];
//    [BtnMyPlaygroundBlue setBackgroundColor:[UIColor grayColor]];
    [BtnMyPlaygroundBlue setEnabled:NO];
    [BtnMyPlaygroundPink setEnabled:YES];
//    [BtnMyPlaygroundPink setBackgroundColor:[UIColor clearColor]];
    [BtnMyPlaygroundPink setBackgroundImage:nil forState:UIControlStateNormal];
    
    [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"MyPlaygroundBackground"]; 
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)MyPlaygroundPinkBackground:(id)sender {
    [BtnMyPlaygroundBlue setBackgroundImage:nil forState:UIControlStateNormal];
//    [BtnMyPlaygroundPink setBackgroundColor:[UIColor grayColor]];
    [BtnMyPlaygroundPink setEnabled:NO];
    [BtnMyPlaygroundBlue setEnabled:YES];
//    [BtnMyPlaygroundBlue setBackgroundColor:[UIColor clearColor]];
    [BtnMyPlaygroundPink setBackgroundImage:[UIImage imageNamed:@"selected_two.png"] forState:UIControlStateNormal];

    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"MyPlaygroundBackground"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)selectGrade:(id)sender {
    
    if (myPickerView) {
        [myPickerView removeFromSuperview];
        [myPickerView release];
        myPickerView = Nil;
    }
    myPickerView = [[UIPickerView alloc] init];    
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.frame = CGRectMake(180,385,300,50);
     [myPickerView setBackgroundColor:[UIColor colorWithRed: 127.0/255.0f green:212.0/255.0f blue:247.0/255.0f alpha:1.0]];
//    [myPickerView setBackgroundColor:[UIColor blueColor1]];
    [self.view addSubview:myPickerView];
    myPickerView.showsSelectionIndicator = TRUE;
    UITapGestureRecognizer *myGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerTapped:)];
    [myPickerView addGestureRecognizer:myGR];
    
    // target method
    

}
-(void)pickerTapped:(id)sender
{
    [self removePickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.grades count];
}

#pragma mark Picker Delegate Methods

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{

    return [self.grades objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component 
{
    [BtnGrade setTitle:[NSString stringWithFormat:@"Current Grade : %@",[self.grades objectAtIndex:row]] forState:UIControlStateNormal];
    NSString *selectedGrade;
    
        
    if ([self.grades count] == 1) {
        selectedGrade = [self.grades objectAtIndex:0];
    }
    else 
    {                    
//        [self.grades removeAllObjects];
//        NSArray *componentsArr = [[NSArray alloc] init];
//        componentsArr = [gradesPurchased componentsSeparatedByString: @" "];   
//        [self.grades addObjectsFromArray:componentsArr];
    selectedGrade = [self.grades objectAtIndex:row];
        
//    if (row == 0) {
//        selectedGrade = [self.grades objectAtIndex:0];
//    }
//    if (row == 1) {
//        selectedGrade = [self.grades objectAtIndex:1];
//    }
//    if (row == 2) {
//        selectedGrade = [self.grades objectAtIndex:2];
//    }
//    if (row == 3) {
//        selectedGrade = [self.grades objectAtIndex:3];
//    }   
//    if (row == 4) {
//        selectedGrade = [self.grades objectAtIndex:4];
//    }   
//    if (row == 5) {
//        selectedGrade = [self.grades objectAtIndex:5];
//    }    
//    if (row == 6) {
//        selectedGrade = [self.grades objectAtIndex:6];
//    }
    }
    
    if ([selectedGrade isEqualToString:@"KG1"] || [selectedGrade isEqualToString:@"KG2"] ) {
        [[NSUserDefaults standardUserDefaults] setValue:@"kg" forKey:@"mapToLoad"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if ([selectedGrade isEqualToString:@"FREE"]) {
        [[NSUserDefaults standardUserDefaults] setValue:@"free" forKey:@"mapToLoad"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if ([selectedGrade isEqualToString:@"G1"]) {
        [[NSUserDefaults standardUserDefaults] setValue:@"G1" forKey:@"mapToLoad"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if ([selectedGrade isEqualToString:@"G2"] || [selectedGrade isEqualToString:@"G3"] || [selectedGrade isEqualToString:@"G4"]) {
        [[NSUserDefaults standardUserDefaults] setValue:@"gx" forKey:@"mapToLoad"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:selectedGrade forKey:@"activeGrade"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentGrammarRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentPrincipleRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentGrammarString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentPrincipleString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:508 forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}

-(void) playHelpMovie{
    
    
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == NO) {
        return;
    }
    NSString *movieName = @"MainFirst";
    if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
        return;
    }
    
    MovieViewController* moviePlayer = [[MovieViewController alloc] init];
    [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayer animated:NO];
    // Prep and play the movie

    [moviePlayer playMovie:movieName];
    
}

-(void) viewDidAppear:(BOOL)animated{
    if (!showMovie) {
        [self playHelpMovie];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    showMovie = TRUE;
}

-(void) removePickerView{
    [myPickerView removeFromSuperview];
}

- (IBAction)btnCheckPinPressed:(id)sender {
    NSString* pinNumber = _txtFieldPinNumber.text;

    
    if (!pinNumber || pinNumber == 0 || pinNumber == NULL || [pinNumber isEqualToString:@""]) {
        [[NSUserDefaults standardUserDefaults] setObject:_txtFieldPinNumber.text forKey:@"PINNUMBER"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PINNEDDEVICE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;

    }
    
    
//    
//    NSString* urlPin = [NSString stringWithFormat:@"http://mrpickit.net/admin/register.aspx?Pin=%@",pinNumber];
//    NSString *agentString = [NSString stringWithFormat:@"application/x-%@",urlPin];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:
//                                    [NSURL URLWithString:urlPin]];
//    
//    [request setValue:agentString forHTTPHeaderField:@"content-type"];
//    //    [request setValue:agentString forHTTPHeaderField:@"User-Agent"];
//    xmlFile = [ NSURLConnection sendSynchronousRequest:request returningResponse: nil error: nil ];
//    
//    errorParsing=NO;
//    rssParser = [[NSXMLParser alloc] initWithData:xmlFile];
//    [rssParser setDelegate:self];
//    [rssParser setShouldProcessNamespaces:NO];
//    [rssParser setShouldReportNamespacePrefixes:NO];
//    [rssParser setShouldResolveExternalEntities:NO];
//    
//    [rssParser parse];
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    NSString *errorString = [NSString stringWithFormat:@"Error code %i", [parseError code]];
    NSLog(@"Error parsing XML: %@", errorString);
    errorParsing=YES;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    currentElement = [elementName copy];
    ElementValue = [[NSMutableString alloc] init];
//    NSString *registrationStatus = [NSString retain];
//    registrationStatus = @"first";
//    if ([elementName isEqualToString:@"Status"]) {
//        registrationStatus = ElementValue;
//    registrationStatus = [NSString stringWithString:ElementValue];


    

}

-(void) showAlertForPinRegistration{
    UIAlertView *alertPinStatus = [[UIAlertView alloc] init];
    [alertPinStatus setTitle:@" "];
    [alertPinStatus setMessage:reasonPINRegistration];
    [alertPinStatus setDelegate:self];
    [alertPinStatus addButtonWithTitle:@"OK"];
    [alertPinStatus setTag:25];
    [alertPinStatus show];
    [alertPinStatus release];
    
}




- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
        [ElementValue appendString:string];   
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    if ([elementName isEqualToString:@"Reason"]) {
        reasonPINRegistration = ElementValue;
        NSLog(@"Element VAlue is %@",ElementValue);
        
        NSString *registrationStatus;
        registrationStatus = ElementValue;
    
    
    if ([registrationStatus isEqualToString:@"Pin Already Registred\n"] || [registrationStatus isEqualToString:@"Successfull"]) {
        NSLog(@"true");
        [[NSUserDefaults standardUserDefaults] setObject:_txtFieldPinNumber.text forKey:@"PINNUMBER"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PINNEDDEVICE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setObject:_txtFieldPinNumber.text forKey:@"PINNUMBER"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PINNEDDEVICE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"wrong Pin.");
    }
        
//    }
    
        
        [self showAlertForPinRegistration];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    if (errorParsing == NO)
    {
        NSLog(@"XML processing done!");
    }
    else {
        NSLog(@"Error occurred during XML processing");
    }
}

- (IBAction)btnPressedSaveName:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:_txtFieldName.text forKey:@"NAMEOFUSER"];
    [[NSUserDefaults standardUserDefaults] synchronize];    
}

- (IBAction)helpButton:(id)sender {
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"MS041" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)dealloc {
    [languageSegment release];
    [LSSwitch release];
    [Name release];
    [Age release];
    [BtnMyCampBlue release];
    [BtnMyCampPink release];
    [BtnMyPlaygroundBlue release];
    [BtnMyPlaygroundPink release];
    [BtnGrade release];
    [grades release];

    [_VideoHelp release];
    [_txtFieldPinNumber release];
    [_txtFieldName release];
    [_backgroundMusicSegmentControl release];
    [_noTashkeelSegmentedControl release];
    [_imgViewBackground release];
    [_btnBoy release];
    [_btnGirl release];
    [characterScrollView release];
    [super dealloc];
}
@end
