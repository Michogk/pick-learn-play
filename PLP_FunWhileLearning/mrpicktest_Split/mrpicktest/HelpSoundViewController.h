//
//  HelpSoundViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 10/22/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface HelpSoundViewController : UIViewController<AVAudioPlayerDelegate>{
        AVAudioPlayer *player;
}
- (IBAction)PlayEnglishHelpSound:(id)sender;
- (IBAction)PlayArabicHelpSound:(id)sender;

@end
