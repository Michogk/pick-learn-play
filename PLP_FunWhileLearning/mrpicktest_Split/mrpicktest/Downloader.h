//
//  Downloader.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/29/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Reachability;

@interface Downloader : NSObject{

    Reachability* internetReachable;
    Reachability* hostReachable;
    BOOL internetActiveStatus;
    BOOL hostActiveStatus;

}

-(void) checkNetworkStatus:(NSNotification *)notice;
-(void) checkForInternetConnection;
- (BOOL)connected;
-(void) fetchData;


@end
