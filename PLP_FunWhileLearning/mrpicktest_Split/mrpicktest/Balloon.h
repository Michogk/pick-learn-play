//
//  baloon.h
//  KidsBubbleLearner
//
//  Created by Muhammad Mosib Asad on 6/28/12.
//  Copyright 2012 GameView Studios. All rights reserved.
//

#import "cocos2d.h"

typedef enum
{
	BlueType = 0,
    GreenType,
    OrangeType,
    PurpleType,
    RedType,
    YellowType,
    
	BalloonType_MAX,

} BalloonTypes;

@interface Balloon : CCMenuItemImage {
    
    BOOL isHavingCorrectWord;
    BalloonTypes type;
    NSString* myWord;
    
    CCLabelTTF* myWordLabel;
    float speed;
    
    int attempt;
    BOOL wrongBaloonBurst;
}


@property BOOL isHavingCorrectWord;

+ (id) balloonWithType:(BalloonTypes) balloonType IsCorrect: (BOOL) isCorrect Word: (NSString*) word;
- (id) initWithType:(BalloonTypes) balloonType IsCorrect: (BOOL) isCorrect Word: (NSString*) word;

- (NSString*) getFileForType: (BalloonTypes) balloonType;
- (void) balloonTapped: (id) ballon;
- (void) updatePosition: (ccTime) delta;
- (void) stop;
@end
