//
//  MySkillsViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/24/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MySkillsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "MyCampSkillsViewController.h"
#import "MyPlayGroundViewController.h"
#import "MyStoreViewController.h"
#import "MovieViewController.h"


@interface MySkillsViewController ()

@end

#define PRINCIPLE_TABLEVIEW_TAG 2
#define GRAMMAR_TABLEVIEW_TAG 1

@implementation MySkillsViewController
@synthesize PrincipleTable = _PrincipleTable;
@synthesize GrammarTable = _GrammarTable;
@synthesize currentGrammarRule = _currentGrammarRule;
@synthesize currentPrincipleRule = _currentPrincipleRule;
@synthesize btnSmallSkills = _btnSmallSkills;
//@synthesize _principleIdForSelectedCellArr
@synthesize downloadedPackArr;

static MySkillsViewController *skillViewController;

+ (MySkillsViewController*)skillViewController {
    if (skillViewController == nil) {
        skillViewController = [[MySkillsViewController alloc] initWithNibName:@"MainStoryboard" bundle:nil];

    }
    return skillViewController;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self setBackground];

    

    [_GrammarTable setDataSource:self]; 
    [_PrincipleTable setDataSource:self];
            
    [_GrammarTable setDelegate:self];
    [_PrincipleTable setDelegate:self];
    
    _GrammarTable.tag = GRAMMAR_TABLEVIEW_TAG;
    _PrincipleTable.tag = PRINCIPLE_TABLEVIEW_TAG;
//            NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
//    if ([activeGrade isEqualToString:@"FREE"]) {
    
//        UIAlertView *purchaseAlert;
//        
//        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//        if (activeLanguage == 2) {
//            purchaseAlert = [[UIAlertView alloc] initWithTitle:@"DOWNLOAD PACKAGE" message:@"Sorry! You cannot go to Skills with free package. Please go to “My Bookstore” to download content”" delegate:self cancelButtonTitle:@"Go to My Bookstore" otherButtonTitles:@"Exit", nil];
//        }
//        else {
//            purchaseAlert = [[UIAlertView alloc] initWithTitle:@"تحميل البيانات " message:@"عفوا , المهارات ليست متوفرة في النسخة المجانية , الرجاء تحميل البيانات من مكتبتي" delegate:self cancelButtonTitle:@"اذهب الى متجري " otherButtonTitles:@"خروج", nil];
//        }
//        
//        [self.view addSubview:purchaseAlert];
//        [purchaseAlert show];
//        [purchaseAlert release];
//        purchaseAlert = Nil;
//        return;
//    }

    
    [self loadContentFromDataBase];
    
 
}

-(void) setBackground{
        NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
            int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    if (activeLanguage == 1 || [activeGrade isEqualToString:@"KG1"] || [activeGrade isEqualToString:@"KG2"]) {
        
        [_imgViewSkillsCampBackGround setImage:[UIImage imageNamed:@"Myskills_OLD.png"]];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"newSmallSkills"];
        [_btnSmallSkills setEnabled:NO];
    }
    else{
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"newSmallSkills"]) {
            [_imgViewSkillsCampBackGround setImage:[UIImage imageNamed:@"Myskills2.png"]];
        }
        else{
            [_imgViewSkillsCampBackGround setImage:[UIImage imageNamed:@"Myskills.png"]];
        }
    }


}

-(void) loadContentFromDataBase{
    MrPDatabase *database = [[MrPDatabase alloc] init];
    MrPGrammarArray = [database MySkillsGrammerArray];
    
    _tempGrammarContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPGrammarArray count]];
    position = 0;
    for (MrPContentInfo *infos in MrPGrammarArray) {
        
        [_tempGrammarContentDic setObject:[NSNumber numberWithInt:infos.grammarId] forKey:[NSString stringWithFormat:@"grammarId%d",position]];
        [_tempGrammarContentDic setObject:[NSNumber numberWithInt:infos.principleId] forKey:[NSString stringWithFormat:@"principleId%d",position]];
        [_tempGrammarContentDic setObject:infos.grammarWord forKey:[NSString stringWithFormat:@"grammarWord%d",position]];
        [_tempGrammarContentDic setObject:[NSNumber numberWithInt:infos.groupId] forKey:[NSString stringWithFormat:@"groupId%d",position]];
        
        position++;
    }
    
    position = 0;
    
    NSArray *MrPPrincinplesArray = [database MySkillsPrinciplesArray];
    
    _tempPrinciplesContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPPrincinplesArray count]];
    for (MrPContentInfo *infos in MrPPrincinplesArray) {
        
        [_tempPrinciplesContentDic setObject:[NSNumber numberWithInt:infos.principleVoiceId] forKey:[NSString stringWithFormat:@"principleVoiceId%d",position]];
        [_tempPrinciplesContentDic setObject:[NSNumber numberWithInt:infos.principleId] forKey:[NSString stringWithFormat:@"principleId%d",position]];
        [_tempPrinciplesContentDic setObject:infos.principleWord forKey:[NSString stringWithFormat:@"principleWord%d",position]];
        [_tempPrinciplesContentDic setObject:[NSNumber numberWithInt:infos.langId] forKey:[NSString stringWithFormat:@"langId%d",position]];
        position++;
        
        
    }
    
    position = 0;
    
    NSArray *MrPLEarningGrammarArray = [database MySkillsLearningGrammar];
    
    [database release];
    database = Nil;
    _tempLearningGrammarDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPPrincinplesArray count]];
    for (MrPContentInfo *infoss in MrPLEarningGrammarArray) {
        
        [_tempLearningGrammarDic setObject:[NSNumber numberWithInt:infoss.grammarId] forKey:[NSString stringWithFormat:@"grammarId%d",position]];
        [_tempLearningGrammarDic setObject:infoss.grammarWord forKey:[NSString stringWithFormat:@"grammarWord%d",position]];
        
        position++;
    }
    _currentGrammar = [[UILabel alloc] initWithFrame:CGRectMake(698-600+201-15-40-64, 258+100+295+22, 120+80+84+60, 25)];
    //    [_currentGrammar setFont:[UIFont fontWithName:@"Arial" size:17]];
    UIFont *font = [UIFont systemFontOfSize: 17];
    [_currentGrammar setFont:font];
    [_currentGrammar setBackgroundColor:[UIColor clearColor]];
    _currentGrammar.textAlignment = UITextAlignmentLeft;
    [self.view addSubview:_currentGrammar];
    
    
    _currentPrinciple = [[UILabel alloc] initWithFrame:CGRectMake(698-600+201-15+520-40-213, 258+100+295+22, 350, 25)];
    //    [_currentPrinciple setFont:[UIFont fontWithName:@"Arial" size:17]];
    [_currentPrinciple setFont:font];
    [_currentPrinciple setBackgroundColor:[UIColor clearColor]];
    _currentPrinciple.textAlignment = UITextAlignmentRight;
    [self.view addSubview:_currentPrinciple];
    
    NSString* loadPrincipleRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentPrincipleString"];
    _currentPrinciple.text = loadPrincipleRule;
    
    NSString* loadGrammarRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentGrammarString"];
    _currentGrammar.text = loadGrammarRule;
    
    NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    if ([activeGrade isEqualToString:@"FREE"]) {
        
        UIAlertView *purchaseAlert;
        
//        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//        if (activeLanguage == 2) {
//            purchaseAlert = [[UIAlertView alloc] initWithTitle:@"DOWNLOAD PACKAGE" message:@"Sorry! You cannot go to Skills with free package. Please go to “My Bookstore” to download content”" delegate:self cancelButtonTitle:@"Go to My Bookstore" otherButtonTitles:@"Exit", nil];
//        }
//        else {
//            purchaseAlert = [[UIAlertView alloc] initWithTitle:@"تحميل البيانات " message:@"عفوا , المهارات ليست متوفرة في النسخة المجانية , الرجاء تحميل البيانات من مكتبتي" delegate:self cancelButtonTitle:@"اذهب الى متجري " otherButtonTitles:@"خروج", nil];
//        }
//        
//        [self.view addSubview:purchaseAlert];
//        [purchaseAlert show];
//        [purchaseAlert release];
//        purchaseAlert = Nil;
    }



}

- (void)viewDidUnload
{
//    [self setGrammarTable:nil];
//    [self setPrincipleTable:nil];
//  [self setPrincipleTable:nil];
    [self setImgViewSkillsCampBackGround:nil];
    [self setBtnSmallSkills:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    //    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == PRINCIPLE_TABLEVIEW_TAG) {
        int principleIdCounter = [_principleIdForSelectedCellArr count];
        if ([_principleIdForSelectedCellArr count]) {
                return principleIdCounter/2;
        }
        else {
        return 0;
        }
        
    }
    else {
        return [_tempLearningGrammarDic count]/2;
    }

    //     
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == GRAMMAR_TABLEVIEW_TAG) {
        NSString *kCellIdentifier = @"MyGrammer";
        
            _grammarCell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
        if (_grammarCell == nil)
        {
            
            _grammarCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
            _grammarCell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
        
        _grammarCell.textLabel.text = [_tempLearningGrammarDic objectForKey:[NSString stringWithFormat:@"grammarWord%d",indexPath.row]];
        _grammarCell.tag = [[_tempLearningGrammarDic objectForKey:[NSString stringWithFormat:@"grammarId%d",indexPath.row]] intValue]; 
        [_grammarCell.textLabel setFont:[UIFont boldSystemFontOfSize:18]];
        return _grammarCell;
    }
    else
    {
        NSString *CellIdentifier = @"MyPrinciple";
        
        principleCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (principleCell == nil)
        {
            
            principleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            principleCell.selectionStyle = UITableViewCellSelectionStyleBlue;                                    
        }
        
            principleCell.accessoryType = UITableViewCellAccessoryNone;
        
        if (!_principleIdForSelectedCellArr) {
                principleCell.textLabel.text = @"";

        }
        else {
            NSString *principle = [_principleIdForSelectedCellArr objectForKey:[NSString stringWithFormat:@"pId%d",indexPath.row]];
            principleCell.textLabel.text = principle;


        }

       return principleCell;
    }                  
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag == GRAMMAR_TABLEVIEW_TAG) {

        if (principlesIds) {
            [principlesIds release];                                
        }
        
        // Getting principle ids for selected grammar and populating thr values in _principleIdForSelectedCellArr
        principlesIds = [[NSMutableArray alloc] initWithCapacity:10];

        int selectedGrammarId = [[_tempLearningGrammarDic objectForKey:[NSString stringWithFormat:@"grammarId%d",indexPath.row]] intValue];
        NSString *selectedGrammarRule = [_tempLearningGrammarDic objectForKey:[NSString stringWithFormat:@"grammarWord%d",indexPath.row]];

        [[NSUserDefaults standardUserDefaults] setInteger:selectedGrammarId forKey:@"currentGrammarRule"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        if (_currentGrammar.text) {
            _currentGrammar.text = @"";
            
            [_currentGrammar removeFromSuperview];
            _currentGrammar = Nil;
            
            _currentGrammar = [[UILabel alloc] initWithFrame:CGRectMake(698-600+201-15-40-64, 258+100+295+22, 120+80+84+60, 25)];
            //    [_currentGrammar setFont:[UIFont fontWithName:@"Arial" size:17]];
            UIFont *font = [UIFont systemFontOfSize: 17];
            [_currentGrammar setFont:font];
            [_currentGrammar setBackgroundColor:[UIColor clearColor]];
            _currentGrammar.textAlignment = UITextAlignmentLeft;
            [self.view addSubview:_currentGrammar];
            
        }
        _currentGrammar.text = selectedGrammarRule;
        NSLog(@"item is ###################################### %@",_currentGrammar.text);
        [[NSUserDefaults standardUserDefaults] setValue:_currentGrammar.text forKey:@"currentGrammarString"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        for (int P = 0;  P < [_tempGrammarContentDic count]/4; P++) {
            if ([[_tempGrammarContentDic objectForKey:[NSString stringWithFormat:@"grammarId%d",P]] intValue] == selectedGrammarId ) {
                [principlesIds addObject:[NSString stringWithFormat:@"%d",[[_tempGrammarContentDic objectForKey:[NSString stringWithFormat:@"principleId%d",P]] intValue]]];
            }
        }      
        
        int load = 0;
        _principleIdForSelectedCellArr = [[NSMutableDictionary alloc] initWithCapacity:20];      
        for (int P = 0;  P <= [_tempGrammarContentDic count]/4 ; P++) {
            NSLog(@"Loop repetation %d",P);
            id pIds = [_tempPrinciplesContentDic objectForKey:[NSString stringWithFormat:@"principleId%d",P]];
            int pId = [pIds intValue]; 
            NSLog(@"Value of pId %d is %d", P, pId);
            int pIdFromLocalArray;
            if (load < [principlesIds count]) {
                 pIdFromLocalArray = [[principlesIds objectAtIndex:load] intValue];
            }
            else {
                NSLog(@"Exiting loops");
                NSLog(@"Pricniples id count is >>>>>>>>>>>>>>>>>>>> %d",[principlesIds count]) ;   
                [self.PrincipleTable reloadData];
                return;
            }            

            NSLog(@"Value of pId from local array is %d",pIdFromLocalArray);            
            if (pId == pIdFromLocalArray ) {
//                NSLog(@"Yes, equals");
                id principleWord = [_tempPrinciplesContentDic objectForKey:[NSString stringWithFormat:@"principleWord%d",P]];
                NSString *principle = [NSString stringWithFormat:@"%@",principleWord];
                [_principleIdForSelectedCellArr setObject:principle forKey:[NSString stringWithFormat:@"pId%d",load]];
                
                [_principleIdForSelectedCellArr setObject:[NSString stringWithFormat:@"%d",pIdFromLocalArray] forKey:[NSString stringWithFormat:@"pIdNumber%d",load]];

                load = load + 1;
                    }
                }
                [self.PrincipleTable reloadData];
    }
    // Getting PRINCIPLE Rule    
    if (tableView.tag == PRINCIPLE_TABLEVIEW_TAG) {            
        
        NSString *principle = [_principleIdForSelectedCellArr objectForKey:[NSString stringWithFormat:@"pId%d",indexPath.row]];
        _currentPrincipleRule = principle;
        _currentPrinciple.text = _currentPrincipleRule;
    
        int _currentPrincipleId = [[_principleIdForSelectedCellArr objectForKey:[NSString stringWithFormat:@"pIdNumber%d",indexPath.row]] intValue];
        
        [[NSUserDefaults standardUserDefaults] setInteger:_currentPrincipleId forKey:@"currentPrincipleRule"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setValue:_currentPrinciple.text forKey:@"currentPrincipleString"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"Current Principle Id is %d and Principle desc is %@",_currentPrincipleId, _currentPrinciple.text);
    }
}

-(void) playHelpMovie{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == NO) {
        return;
    }
    
    NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    if ([activeGrade isEqualToString:@"FREE"]) {
        return;
    }
    
        NSString *movieName = @"Skills";
    if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
        return;
    }
    
    MovieViewController* moviePlayer = [[MovieViewController alloc] init];
    [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayer animated:NO];
    // Prep and play the movie

    [moviePlayer playMovie:movieName];
    
}

-(void) viewDidAppear:(BOOL)animated{
    if (!showMovie) {
        [self playHelpMovie];
    }
    
}

-(void)viewDidDisappear:(BOOL)animated{
    showMovie = TRUE;
}



- (void)dealloc {
//    [_GrammarTable release];
//    [_PrincipleTable release];
//    [_GrammarTable release];
//    [_PrincipleTable release];
    [_imgViewSkillsCampBackGround release];
    [_btnSmallSkills release];
    [super dealloc];
}
- (IBAction)MySkillsPlayground:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setInteger:99999 forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];        
//    if ([activeGrade isEqualToString:@"FREE"] || [activeGrade isEqualToString:@"KG1"] || [activeGrade isEqualToString:@"KG2"] ) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        MyPlayGroundViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyPlaygroundIdentifier"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:NO completion:NULL];
        return; 
//    }
    
    // Check if Skills of current active grade has already been downloaded else show alert
    NSString* packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"AlreadyDownloadedPackage"];        
    if (packagesDownloadedBefore.length > 2){
        self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];       
        BOOL skillAlreadyPurchased = false;
        for (int i = 0; i < [self.downloadedPackArr count]; i++) {
            if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {                    
//                skillAlreadyPurchased = TRUE;
                
                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                MyPlayGroundViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyPlaygroundIdentifier"];
                vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self presentViewController:vc animated:NO completion:NULL];
                return;
            }
        }
//        if (skillAlreadyPurchased == FALSE) {
//            UIAlertView *alert = [[UIAlertView alloc] init];
//            int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//            if (activeLanguage == 2) {
//                [alert setTitle:@"CAUTION"]; 
//                [alert setMessage:@"PLEASE DOWNLOAD PACKAGE AND TRY AGAIN"];
//                [alert addButtonWithTitle:@"OK"];
//            }                
//            else {
//                [alert setTitle:@"تحذير !"]; 
//                [alert setMessage:@"الرجاء الذهاب الى متجري لتحميل الكلمات والمهارات "];
//                [alert addButtonWithTitle:@"OK"];
//                
//            }
//            [alert setDelegate:self];
//            [alert setTag:6];
//            [alert show];
//            [alert release];
//            return;
//        }
    }
}

- (IBAction)gotoSkillsCamp:(id)sender {

//    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SkillsCampSound"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];  
//    if (!activeGrade || activeGrade == Nil) {
//        activeGrade = @"FREE";
//    }
//    if ([activeGrade isEqualToString:@"FREE"] || [activeGrade isEqualToString:@"KG1"] || [activeGrade isEqualToString:@"KG2"] ) {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//        MyCampSkillsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MySkillsCampIdentifier"];
//        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//        [self presentViewController:vc animated:NO completion:NULL];
//        return;  
//    }
    
//    // Check if Skills of current active grade has already been downloaded else show alert
//    NSString* packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"AlreadyDownloadedPackage"];        
//    if (packagesDownloadedBefore.length > 2){
//            self.downloadedPackArr = [packagesDownloadedBefore componentsSeparatedByString:@" "];       
//            BOOL skillAlreadyPurchased = false;
//            for (int i = 0; i < [self.downloadedPackArr count]; i++) {
//                if ([[self.downloadedPackArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@SKILLS",activeGrade]]) {                    
//                    skillAlreadyPurchased = TRUE;
                    
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                    MyCampSkillsViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MySkillsCampIdentifier"];
                    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                    [self presentViewController:vc animated:NO completion:NULL];
                    return;
//                }
//            }
//            if (skillAlreadyPurchased == FALSE) {
//                UIAlertView *alert = [[UIAlertView alloc] init];
//               int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//                if (activeLanguage == 2) {
//                    [alert setTitle:@"CAUTION"]; 
//                    [alert setMessage:@"PLEASE DOWNLOAD PACKAGE AND TRY AGAIN"];
//                    [alert addButtonWithTitle:@"OK"];
//                }                
//                else {
//                    [alert setTitle:@"تحذير !"]; 
//                    [alert setMessage:@"الرجاء الذهاب الى متجري لتحميل الكلمات والمهارات "];
//                    [alert addButtonWithTitle:@"OK"];
//                   
//                }
//                [alert setDelegate:self];
//                
//                [alert setTag:6];
//                [alert show];
//                [alert release];
//                return;
//            }
//    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0)
    {
//        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
//        MyStoreViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyBookStoreViewControllerIderntifier"];
//        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//        [self presentViewController:vc animated:NO completion:NULL];    
        
    }
    if (buttonIndex == 1)
    {
        [self dismissModalViewControllerAnimated:NO];
    }           
}

- (IBAction)gotoMainMenu:(id)sender {
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SkillsCampSound"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (IBAction)btnSmallSkillsPressed:(id)sender {
    if (_currentGrammar) {
        [_currentGrammar removeFromSuperview];
        _currentGrammar = Nil;
    }

    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"newSmallSkills"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self setBackground];
    [self loadContentFromDataBase];
    [self.GrammarTable reloadData];
    [self.PrincipleTable reloadData];
    
}

- (IBAction)btnCompleteSkillsPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"newSmallSkills"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (_currentGrammar) {
        [_currentGrammar removeFromSuperview];
        _currentGrammar = Nil;
    }

    
    [self setBackground];
    [self loadContentFromDataBase];
    [self.GrammarTable reloadData];
    [self.PrincipleTable reloadData];
}

- (IBAction)HelpSound:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"MS007" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"myskills" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
