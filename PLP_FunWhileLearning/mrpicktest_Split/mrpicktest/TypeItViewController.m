//
//  TypeItViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 09/10/2015.
//  Copyright © 2015 SmartSoft. All rights reserved.
//

#import "TypeItViewController.h"

#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "SimpleAudioEngine.h"
#import "MyPlayGroundViewController.h"
#import "HelpSoundViewController.h"
#import "DocumentDirectory.h"
#import "MrPReport.h"
#define FIXIT 7



#define FILLIT 3

@interface TypeItViewController ()

@end

@implementation TypeItViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadDatabase];
    [self insitiantiateForReportCouting];
    [self showMenu];
    
    [self playBackGroundMusic];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showMenu
{
    myText.text = @"";
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 1) {
        myText.textAlignment = UITextAlignmentRight;
//        myText.adjustsFontSizeToFitWidth = YES;
    }
    if ([tempContentDic count] > 0) {
        
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FILLIT,cId,activeLanguage,ActiveGrade];
        currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCount];
        
        
        if ((currentWord == [tempContentDic count]/4) || (!currentWord)) {
            currentWord = 0;
        }
        
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],FILLIT,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        if (!LevelLastCount || LevelLastCount != 2) {
            //        LevelLastCount = 1;
            level = 1;
        }
        
        wordImg = [[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",currentWord]] intValue];
        if (!wordImg || wordImg == 0) {
            wordImg = 1;
        }
        
        if (cId > 989 && cId < 999) {
            NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
            NSString * documentsPath = [paths objectAtIndex:0];
            NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
            NSString * filename = [NSString stringWithFormat:@"%@/%d.png",activeGrade,wordImg];
            NSString * filePath = [documentsPath stringByAppendingPathComponent:filename];
            if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                
            }
            else {
                filePath = [NSString stringWithFormat:@"1.png"];
            }
            
            filePath = filename;
            
            if(cId > 989 && cId < 999){
                filename = [NSString stringWithFormat:@"%@/%d.png",activeGrade,wordImg];
                filePath = [documentsPath stringByAppendingPathComponent:filename];
                if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                    //        sprite = [CCSprite spriteWithFile:filePath];
                }
                else {
                    filePath = [NSString stringWithFormat:@"2.png"];
                }
                UIImage *image = [UIImage imageWithContentsOfFile:filePath];
                self.imageViewWord.image = image;
            }
        }
        else{
            self.imageViewWord.image = [DocumentDirectory imageNamed:[NSString stringWithFormat:@"%d",wordImg]];
        }
        
        
        
        
        int totalWords = [tempContentDic count]/4;
        NSString *wordsCunter = [NSString stringWithFormat:@"%d / %d",currentWord + 1, totalWords];
        
        labelWordsCount.text = wordsCunter;
        
        
        // Points 1
        NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",FILLIT,1];
        NSString *lblLevel1 = [NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]];
        labelLevelOne.text = lblLevel1;
        // Points 2
        NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",FILLIT,2];
        NSString *lblLevel2 = [NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]];
        labelLevelTwo.text  =lblLevel2;
        
        NSString* loadPrincipleRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentPrincipleString"];
        NSString* loadGrammarRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentGrammarString"];
        cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];

        
        
    }
    else {
        if (cId > 989 && cId < 999) {

            UIAlertView *alert = [[UIAlertView alloc] init];
            if (activeLanguage == 1) {
                [alert setTitle:@"احذر"];
                [alert setMessage:@" البيانات لا تكفي في ملفاتك"];
            }
            else {
                [alert setTitle:@"WARNING"];
                [alert setMessage:@"Data NOT sufficient in your folder"];
            }
            
            [alert setDelegate:self];
            [alert addButtonWithTitle:@"OK"];
            [alert setTag:2];
            [alert show];
            [alert release];
        }
        else {

            UIAlertView *alert = [[UIAlertView alloc] init];
            [alert setTitle:@"WARNING"]; 
            [alert setMessage:@"SORRY! Data No Sufficient. Please Make Sure you have selected correct Object"];
            [alert setDelegate:self];
            [alert setTag:2];
            [alert addButtonWithTitle:@"OK"];
            [alert show];
            [alert release];
        }
    }
    [self hearWord];
}


- (void)dealloc {
    [myText release];

    [_imageViewWord release];
    [labelLevelOne release];
    [labelLevelTwo release];
    [labelWordsCount release];
    [myText release];
    [super dealloc];
}

-(void) NextWord{
        myText.text = @"";
    
//    if (sessionCounter == 10) {
//        sessionCounter = 0;
//        [self sessionCompletionCheck];
//        return;
//    }
    sessionCounter = sessionCounter + 1;
    
    // if correct, Go ahead to next Word
    currentWord  = currentWord + 1;
    if (currentWord > [tempContentDic count]/4) {
        level = level + 1;
        [self saveProgressForReport];
    }
    NSString *wordSaver = [NSString stringWithFormat:@"fillItwordForClassification%d",cId];
    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FILLIT,cId,activeLanguage,ActiveGrade];
    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self updateProgress:FALSE];

    [self showMenu];
}

-(void) loadDatabase{
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    //    int LanguageSupport = [[NSUserDefaults standardUserDefaults] integerForKey:@"LanguageSupport"];
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    
    int position = 0;
    
    // cId 99999 means its myskillscamp, otherwise its simple classification
    if (cId == 99999) {
        
        
        int gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
        int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
        
        int position = 0;
        
        if (activeLanguage == 2) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                position++;
                //                }
                
            }
        }
        
        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                //                if (info.isSentence == 1) {
                //
                //                }
                //                else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                position++;
                //                }
                
            }
        }
    }
    
    
    // if goes in the loop below, it means its simple classification from city map
    else
    {
        if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:cId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                //            if (info.isSentence == 1) {
                //
                //            }
                //            else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                position++;
                //            }
                
            }
        }
        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:cId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                //            if (info.isSentence == 1) {
                //
                //            }
                //            else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]]; // keeping it english word so in class, our convention remain same
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                position++;
            }
            //            }
        }
    }
    
    [database release];
    database = Nil;
}
-(void) resignKeyBoard{
    [myText resignFirstResponder];
    myText.text = @"";

}
-(void) CheckWord{
    
    if (sessionCounter == 10) {
        sessionCounter = 0;
        [self sessionCompletionCheck];
        return;
    }
    sessionCounter = sessionCounter + 1;
    
    NSString *userEnteredtext = myText.text;
    NSString *correctWord = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",currentWord]];
    if ([correctWord caseInsensitiveCompare:userEnteredtext] == NSOrderedSame){
        
        [self resignKeyBoard];
        
        
        reportCorrectCount = reportCorrectCount + 1;
        reportTotatCount = reportTotatCount + 1;
        
        [self updateReportCurrentWord];
        [self saveProgressForReport];
        [self insitiantiateForReportCouting];
        
        // if correct, Go ahead to next Word
        currentWord  = currentWord + 1;
        correctAnswerCounter = correctAnswerCounter + 1;
        if (currentWord > [tempContentDic count]/4) {
            level = level + 1;
            [self saveProgressForReport];
            [self insitiantiateForReportCouting];
        }
        NSString *wordSaver = [NSString stringWithFormat:@"fillItwordForClassification%d",cId];
        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
        
        [self playCheeringSound];
        [self performSelector:@selector(moveToNextStep) withObject:nil afterDelay:2.0];
    }
    else{
        [self playEncouragingSound];
        reportTotatCount = reportTotatCount + 1;
        
        [self updateProgress:FALSE];
        
    }
}

-(void) moveToNextStep{
    [self updateProgress:TRUE];
    [self showMenu];

}


#pragma mark - Sounds

-(void) playHelpSoundEng{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayEnglishHelpSound:nil];
    
}
-(void) playHelpSoundAra{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayArabicHelpSound:nil];
    
}

-(void) playCheeringSound{
    
    
    int randomNumer = arc4random() % 3 ;
    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
        
    }
    
}

-(void) playEncouragingSound{
    
    int randomNumer = arc4random() % 3 ;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC25";
        NSString *second = @"EC30";
        NSString *third = @"EC31";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }else {
        NSString *first = @"AC04";
        NSString *second = @"AC05";
        NSString *third = @"AC06";
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }
}

#pragma mark - Report
-(void) saveProgressForReport{
    // write in db!
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database saveReportProgress:retval];
    
    //    NSLog(@"temp is %@",tempContentDic);
    
    if (retval) {
        [retval removeAllObjects];
        [retval release];
        retval = nil;
    }
}
-(void) updateReportCurrentWord{
    reportWordDesc = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",currentWord]];
    reportWordId = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord]] intValue];
    int gameId = 7;
    MrPReport *info = [[MrPReport alloc]
                       initWithWordId:reportWordId andWordDesc:reportWordDesc andCorrectCount:reportCorrectCount andTotalCount:reportTotatCount andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
    
    [retval addObject:info];
    
    reportTotatCount = 0;
    reportCorrectCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
    
}

-(void) updateProgress:(BOOL) correctAnswer{
    
    if (correctAnswer) {
        
        NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
        int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount];
        totalWordsLastCount = totalWordsLastCount + 1;
        
        NSString *correctWordsCount = [NSString stringWithFormat:@"correctWordsCount"];
        int correctWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:correctWordsCount];
        correctWordsLastCount = correctWordsLastCount + 1;
        
        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",[Common getPinNumber],FILLIT,cId,activeLanguage];
        int triesLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:triesCount];
        
        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",FILLIT,level];
        float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
        if (!PointsLastCount) {
            PointsLastCount = 0;
        }
        
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],FILLIT,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        
        if (triesLastCount < 10) {
            if (LevelLastCount == 2) {
                //                PointsLastCount = PointsLastCount + 2 * 1.5;
                
            }
            else {
                PointsLastCount = PointsLastCount + (2 * 1.5);
            }
            
        }
        else {
            
        }
        triesLastCount = 0;
        NSLog(@"Tries Last Count is %d", triesLastCount);
        NSLog(@"Current Points is %f", PointsLastCount);
        NSLog(@"Current Word is %d", currentWord);
        //        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",FIXIT,cId,activeLanguage];
        [[NSUserDefaults standardUserDefaults] setInteger:triesLastCount forKey:triesCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FILLIT,cId,activeLanguage,ActiveGrade];
        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",FIXIT,cId,activeLanguage];
        [[NSUserDefaults standardUserDefaults] setInteger:level forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        //        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",FIXIT,level];
        [[NSUserDefaults standardUserDefaults] setFloat:PointsLastCount forKey:Points]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:totalWordsLastCount forKey:totalWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:correctWordsLastCount forKey:correctWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else {
        
        NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
        int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount];
        totalWordsLastCount = totalWordsLastCount + 1;
        
        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",[Common getPinNumber],FILLIT,cId,activeLanguage];
        int triesLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:triesCount];
        triesLastCount = triesLastCount + 1;
        
        [[NSUserDefaults standardUserDefaults] setInteger:totalWordsLastCount forKey:totalWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:triesLastCount forKey:triesCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"Tries Last Count is %d", triesLastCount);
    }    
}

-(void) insitiantiateForReportCouting{
    retval = [[NSMutableArray alloc] init];
    reportCorrectCount = 0;
    reportTotatCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"1-writeIT-TypeIT"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.volume = 0.15;
    player.numberOfLoops = 20;
    player.delegate = self;
}

#pragma mark - Actions
- (IBAction)actionNext:(id)sender {

    [self NextWord];
    
}

- (IBAction)actionGotoMainMenu:(id)sender {
    [self resignKeyBoard];
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    
    
    
}

- (IBAction)actionReset:(id)sender {
    myText.text = @"";
}


-(void) hearWord{
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    
    NSString* soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
    
}
- (IBAction)actionHearWord:(id)sender {
    [self hearWord];
}
- (IBAction)actionHelp:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"GH020" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (IBAction)actionCheck:(id)sender {
    [self CheckWord];
}

#pragma mark - Alert
-(void) sessionCompletionCheck{
    UIAlertView *sessionAlert;
    if (correctAnswerCounter > 7) {
        
        if (activeLanguage == 2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good!" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:10];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"RewardUnlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        if (activeLanguage ==2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your progress needs improvement. Try again." delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عفوا !" message:@"تقدمك غير كافي , يجب أن تحاول أكثر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب" otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:1];
    }
    
    [sessionAlert setTag:1];
    [sessionAlert show];
    [sessionAlert release];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    
    if(alertView.tag == 2)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        }
        [self resignKeyBoard];
        
        //        [self updateReportCurrentWord];
        //        [self saveProgressForReport];
     
    }
    else {
        
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
            [self NextWord];
        }
        if (buttonIndex == 1)
        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            }
            [self resignKeyBoard];
//        [self dismissModalViewControllerAnimated:NO];
        }
    }     
}
@end
