//
//  DrawingUIView.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface DrawingUIView : UIView<AVAudioPlayerDelegate>
{
    BOOL touchMovedTrue;
    BOOL touchStartingPoint;
    AVAudioPlayer *wordPlayer;
    CGPoint touchPoint;
    CGPoint lastTouchPoint;
    
    CGPoint line1StartPoint;
    CGPoint line1EndPoint;
    
    CGPoint line2StartPoint;
    CGPoint line2EndPoint;
    
    CGPoint line3StartPoint;
    CGPoint line3EndPoint;
    
    CGPoint line4StartPoint;
    CGPoint line4EndPoint;
    NSMutableArray *wordsSoundArray;
    
    BOOL line1Drawn;
    BOOL line2Drawn;
    BOOL line3Drawn;
    BOOL line4Drawn;
    
    int lineDrawingInProgress;
    NSMutableArray *positionArray;
        NSMutableArray *imagesArray;
    NSMutableDictionary *userResults;
    CGContextRef drawingContext;
    int resultWord;
    BOOL touchImageButton;
    
    BOOL touchFromWordBegan;
    BOOL touchFromImageBegan;
}

- (id)initWithFrame:(CGRect)frame andWordsSoundArray:(NSMutableArray *) pArray andImagesSoundArray:(NSMutableArray*) pImagesArray;

@end
