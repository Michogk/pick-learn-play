//
//  MyRewardsViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/25/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MyRewardsViewController.h"
//#import "MyRewardTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MyRewardTableViewCell.h"
#define FIRST_TABLEVIEW_TAG 2
#define SECOND_TABLEVIEW_TAG 1
static MyRewardsViewController* _RewardRefrence;

@interface MyRewardsViewController ()

@end

@implementation MyRewardsViewController
@synthesize firstRewardTableView = _firstRewardTableView;
@synthesize secondRewardTableView = _secondRewardTableView;
@synthesize rewardsAvailable = _rewardsAvailable;

+(MyRewardsViewController*) sharedInstance{
    return _RewardRefrence;

}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    MyRewardTableViewController *tableVC = [[MyRewardTableViewController alloc] init];
//    tableVC.view.transform = CGAffineTransformMakeRotation(-M_PI_2);
//    tableVC.view.frame = CGRectMake(200,230, 707, 200);
//    [tableVC.view setTag:400];
//    [self.view addSubview:tableVC.view];
   
    _firstRewardTableView.backgroundColor = [UIColor clearColor];
    _firstRewardTableView.opaque = NO;
    
    _secondRewardTableView.backgroundColor = [UIColor clearColor];
    _secondRewardTableView.opaque = NO;
    
    [_firstRewardTableView setDataSource:self]; 
    [_secondRewardTableView setDataSource:self];
    
    [_firstRewardTableView setDelegate:self];
    [_secondRewardTableView setDelegate:self];
    
    _firstRewardTableView.tag = FIRST_TABLEVIEW_TAG;
    _secondRewardTableView.tag = SECOND_TABLEVIEW_TAG; 
    
    _firstRewardTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);    
    _secondRewardTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadRewardsTableView) name:@"updateRewardsTableView" object:nil];
    
    _rewardsAvailable = [[[NSMutableArray alloc] init] autorelease];
    NSString *rewardsAvailableInMemory = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"availableRewards"]];
    [_rewardsAvailable removeAllObjects];

    NSArray* componentsArr = [[rewardsAvailableInMemory componentsSeparatedByString: @" "] retain];           
    [_rewardsAvailable addObjectsFromArray:componentsArr];   
    [componentsArr release];
    componentsArr = Nil;
    reloadCells = 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    _rewardsAvailable = [[[NSMutableArray alloc] init] autorelease];
    NSString *rewardsAvailableInMemory = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"availableRewards"]];
    [_rewardsAvailable removeAllObjects];
    NSArray* componentsArr = [[rewardsAvailableInMemory componentsSeparatedByString: @" "] retain];           
    [_rewardsAvailable addObjectsFromArray:componentsArr]; 
    [componentsArr release];
    componentsArr = Nil;
    if ([_rewardsAvailable count] == 1) {
        [[_rewardsAvailable objectAtIndex:0] isEqualToString:@" "];
        return 0;
    }
    int availableRewardsCount = [_rewardsAvailable count];
    int firstTableViewRows = 0;
    int secondTableViewRows = 0;
    if (availableRewardsCount > 10) {
        firstTableViewRows = 10;
        secondTableViewRows = availableRewardsCount - firstTableViewRows;
    }
    else {
        firstTableViewRows = availableRewardsCount;
//      secondTableViewRows = 0;
    }
    if (tableView == _firstRewardTableView) {
        return firstTableViewRows;
    }
    else {
        if (secondTableViewRows > 10) {
            secondTableViewRows = 10;
        }
        return secondTableViewRows;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == FIRST_TABLEVIEW_TAG) {
        
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"RewardsCellNumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:FIRST_TABLEVIEW_TAG forKey:@"RewardsTableNumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d",indexPath.row,reloadCells];
        
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"BookStoreCellNumber"];
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) 
        {
            cell = [[[MyRewardTableViewCell alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        tableView.separatorStyle = UITableViewCellSelectionStyleGray;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell; 
    }
    else
    {        
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"RewardsCellNumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:SECOND_TABLEVIEW_TAG forKey:@"RewardsTableNumber"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%d%d",indexPath.row,reloadCells];
        
        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"BookStoreCellNumber"];
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) 
        {
            cell = [[[MyRewardTableViewCell alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        tableView.separatorStyle = UITableViewCellSelectionStyleGray;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;       

    }   
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 225;
}


- (void)viewDidUnload
{
    [self setFirstRewardTableView:nil];
    [self setSecondRewardTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"myachievements" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)HelpSound:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"E014" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) reloadRewardsTableView{

//    [cell release];
//    cell = Nil;
    reloadCells = reloadCells + 1;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadRewardsTableView) name:@"updateRewardsTableView" object:nil];
    [_firstRewardTableView reloadData];
    [_secondRewardTableView reloadData];

}

- (void)dealloc {
    [_firstRewardTableView release];
    [_secondRewardTableView release];
    [super dealloc];
}
@end
