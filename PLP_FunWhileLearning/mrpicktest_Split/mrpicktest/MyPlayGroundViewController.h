//
//  MyPlayGroundViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/17/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPlayGroundViewController : UIViewController <UIScrollViewDelegate, UIApplicationDelegate, UITextFieldDelegate> {
    
 
    IBOutlet UIScrollView *PlayGroundScoll;  
    
    int GameToBePlayed;
    //Write It Game
    NSTimer* timerLoadGames;
    UITextField *levelEntryTextField;
        BOOL showMovie;

}
@property (nonatomic,retain) UIScrollView *PlayGroundScoll; 
//@property (readonly) UINavigationController *navController;

// Write it Game

+ (MyPlayGroundViewController*) sharedPlayGround;
- (void) specifyStartLevel;
- (void) toggleKeyBoardVisibility;
//- (NSString*) getUserInput;
- (void) setKeyBoardVisibility: (BOOL) change;
- (void) setInputText:(NSString*) text;
- (void) removeLabel;
+ (NSString*) getUserInput;

@property (retain, nonatomic) IBOutlet UIImageView *imgViewRightArrow;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewLeftArrow;
- (IBAction)gotoMainMenu:(id)sender;
- (IBAction)actionMyCamp:(id)sender;

- (IBAction)helpButton:(id)sender;
- (IBAction)helpURL:(id)sender;


@end
