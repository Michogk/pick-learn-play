//
//  mrpViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/13/12.
//  Copyright (c) 2012 School. All rights reserved.
//

#import "mrpViewController.h"
#import "MyStoreViewController.h"
//#import "CustomMoviePlayerViewController.h"
#import "introViewController.h"
#import "Downloader.h"
#import "MrPDatabase.h"
#import "MovieViewController.h"
#define GRADE @"G2"
#define GRADE_INTRO_VIDEO @"G1G4IntroVideo"
//#define GRADE_INTRO_VIDEO @"KGIntroVideo"


//#define mapToLoad @"G1"
#define mapToLoad @"gx"
//#define mapToLoad @"kg"


//Video = KG_Video, Grade_Video



@interface mrpViewController ()

@end

@implementation mrpViewController

@synthesize CityMapView;

@synthesize MyCampView;
//@synthesize MyPlayGroundView;

- (void)viewDidLoad
{

    [super viewDidLoad];
    [self playBackGroundMusic];

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"ActiveLanguage"] intValue] == 1) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"ActiveLanguage"]; 
        [[NSUserDefaults standardUserDefaults] synchronize];  
    }
    else {
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"ActiveLanguage"]; 
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    

    int activeClassification;
    
    
    activeClassification = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    if (!activeClassification || activeClassification < 1) {
        [[NSUserDefaults standardUserDefaults] setInteger:508 forKey:@"selectedClassificationId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (void)loadInBackground
{
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"LanguageSupport"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:GRADE forKey:[NSString stringWithFormat:@"gradesPurchased"]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:GRADE forKey:@"activeGrade"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"freeContentDownloaded"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"01 02 03 04 05 06 07 08 09 010 011 012 013 014 015 016 017 018 019 020" forKey:@"availableRewards"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self moveFreeResourcesToDocumentDirectory];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allBackgroundMusic"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:mapToLoad forKey:@"mapToLoad"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"freeContentDownloaded"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:508 forKey:@"selectedClassificationId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"appUsedBefore"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"freeContentDownloaded2"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"freeContentDownloaded1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PHOTOFEATURE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentGrammarRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"currentPrincipleRule"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentGrammarString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"currentPrincipleString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self replaceExistingDatabasesWithNewOnes];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"version1.5Downloaded"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"videoHelp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMyHomeSetup"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffAchievements"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMyCamp"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMyHomeAddword"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffPlayground"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffSkills"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffCityMap"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffFixIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMainScreenFirstVideo"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMatchIt"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMoveIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffMyPlayground"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffPickIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffPOPIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffSpinIt"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffTypeIT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"videoHelpOffWriteIt"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allBackgroundMusic"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"harkat"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    

}
-(void) viewDidAppear:(BOOL)animated{
    int appUsedBefore = [[[NSUserDefaults standardUserDefaults] objectForKey:@"appUsedBefore"] intValue];
    if (appUsedBefore != 1) {
        
         [self performSelectorInBackground:@selector(loadInBackground) withObject:Nil];
        
//        UIAlertView *purchaseAlert;
//        purchaseAlert = [[UIAlertView alloc] initWithTitle:@"WELCOME TO MR PICK IT" message:Nil delegate:self cancelButtonTitle:@"START" otherButtonTitles:nil ];
//        repeatLaunch = TRUE;
//        purchaseAlert.tag = 100002;
//        purchaseAlert.delegate = self;
//        [self.view addSubview:purchaseAlert];
//        [purchaseAlert show];
//        [purchaseAlert release];
//        purchaseAlert = Nil;

    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 120) {
        return;
    }
    
    if (alertView.tag == 121) {
        return;
    }
    
    if (alertView.tag == 100001) {
        
//        MovieViewController* moviePlayers = [[MovieViewController alloc] init];
//        [moviePlayers.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//        [self presentModalViewController:moviePlayers animated:NO];
//        NSString* movieName = [NSString stringWithFormat:@"G1G4IntroVideo"];//@"Grade_Video";
//        [moviePlayers playMovie:movieName];
    }
    else{
//    if (buttonIndex == 0)
//    {
//        if (myCampPlayer) {
//            [myCampPlayer stop];
//            [myCampPlayer release];
//            myCampPlayer = nil;
//        }
//
//        MovieViewController* moviePlayers = [[MovieViewController alloc] init];
//        [moviePlayers.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//        [self presentModalViewController:moviePlayers animated:NO];
//        NSString* movieName = @"KG_Video";
//        [moviePlayers playMovie:movieName];
//        }
    }
}

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    
    NSString* fileName = [NSString stringWithFormat:@"2-Camp"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    myCampPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    myCampPlayer.volume = 0.15;
    myCampPlayer.numberOfLoops = 20;
    myCampPlayer.delegate = self;
    
    if ([myCampPlayer prepareToPlay]) {
        [myCampPlayer setCurrentTime:0.0];
        [myCampPlayer play];            
    }
    
}
- (void)viewDidUnload
{    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    CityMap =nil;
    SetupBtn = nil;           
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (IBAction)exitMainMenu:(id)sender {
    if (myCampPlayer) {
        [myCampPlayer stop];
        [myCampPlayer release];
        myCampPlayer = nil;
    }    
}

- (IBAction)introButtonPressed:(id)sender {
}

- (IBAction)playVideo:(id)sender {
        [self loadMoviePlayer];
}

- (void)loadMoviePlayer
{  
    // Play movie from the bundle
    
//    moviePlayer = [[[CustomMoviePlayerViewController alloc] initWithPath:moviePlayed] autorelease];
//    [self presentModalViewController:moviePlayer animated:NO];    
//    // Prep and play the movie
//    NSString* movieName = @"loadFromURL";
//    [moviePlayer readyPlayer:movieName];
    

    Downloader *downloadPackage = [[Downloader alloc] init];
    if (![downloadPackage connected]) {
        UIAlertView *internetConnectivityAlert;
        
        internetConnectivityAlert = [[UIAlertView alloc] initWithTitle:@"Network Error" message:@"Please check your internet connection and check again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil ];
        
        
        //        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == TRUE && repeatLaunch == FALSE) {
        repeatLaunch = TRUE;
        [internetConnectivityAlert setTag:120];
        [self.view addSubview:internetConnectivityAlert];
        [internetConnectivityAlert setTag:120];
        [internetConnectivityAlert show];
        [internetConnectivityAlert release];
        internetConnectivityAlert = Nil;
        return;
    }
    if (myCampPlayer) {
        [myCampPlayer stop];
        [myCampPlayer release];
        myCampPlayer = nil;
    }
    
    MovieViewController* moviePlayers = [[MovieViewController alloc] init];
    [moviePlayers.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayers animated:NO];
 
    // Prep and play the movie
    NSString* movieName = @"loadFromURL";
    [moviePlayers playMovie:movieName];
    
}

- (void)replaceExistingDatabasesWithNewOnes{
    // First, test for existence.
    NSArray *arrDatabases = [[NSArray alloc] initWithObjects:@"G1.sqlite3",@"G1.sqlite3",/*@"NH_G3.sqlite3",@"G3_Skiils_New.sqlite3",@"G2.sqlite3",@"G3.sqlite3",@"G4.sqlite3",*/ nil];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    for (int i = 0; i < [arrDatabases count]; i++) {
    NSLog(@"Copying Database %@ to doc dir",[arrDatabases objectAtIndex:i]);
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:[arrDatabases objectAtIndex:i]];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success){
        
        [[NSFileManager defaultManager] removeItemAtPath:writableDBPath error:nil];
        
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[arrDatabases objectAtIndex:i]];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
        
        if (!success) {
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
        NSLog(@"Successfully copied %@ to doc dir",[arrDatabases objectAtIndex:i]);
        NSURL *pathURL= [NSURL fileURLWithPath:writableDBPath];
        [self addSkipBackupAttributeToItemAtURL:pathURL];
        }
    }
}

- (void)didReceiveMemoryWarning {

    
}

-(void) moveFreeResourcesToDocumentDirectory{
    
    NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"G2"];

    
    NSString *destinationPath;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    destinationPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"G2"];
    NSError *error;
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL freecontent = [fileManager fileExistsAtPath:destinationPath];
    if (!freecontent){
        BOOL success = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        if (!success) {
            NSLog(@"copying not successful with error: %@",error);
        }
        NSLog(@"Documents directory: %@", [[NSFileManager defaultManager] contentsOfDirectoryAtPath:destinationPath error:&error]);
        NSURL *pathURL= [NSURL fileURLWithPath:destinationPath];
        [self addSkipBackupAttributeToItemAtURL:pathURL];
    }
    
    sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"G2Skills"];
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    destinationPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"G2Skills"];
    fileManager = [NSFileManager defaultManager];
    freecontent = [fileManager fileExistsAtPath:destinationPath];
    
    if (!freecontent){
        BOOL success = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        if (!success) {
            NSLog(@"copying not successful with error: %@",error);
        }
        NSLog(@"Documents directory: %@", [[NSFileManager defaultManager] contentsOfDirectoryAtPath:destinationPath error:&error]);
        NSURL *pathURL= [NSURL fileURLWithPath:destinationPath];
        [self addSkipBackupAttributeToItemAtURL:pathURL];
    }

    
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

@end
