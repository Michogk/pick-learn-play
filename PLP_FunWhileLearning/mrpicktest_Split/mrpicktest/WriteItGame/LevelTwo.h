//
//  LevelTwo.h
//  Smooth Drawing
//
//  Created by Muhammad Mosib on 7/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "LineDrawer.h"
#import <AVFoundation/AVFoundation.h>

@interface LevelTwo : CCLayer<AVAudioPlayerDelegate> {
        AVAudioPlayer *player;
    LineDrawer* myDrawingCanvas;
    CCLabelTTF* word;
    CCMenu* colorMenu;
    CCLabelTTF *wordsCounterlbl;
    
    CCSprite *wordSprite;
    int stage;
    
    int currentWord; // count that at which word we are currently at!
    CCMenu* mainMenu;
    NSMutableDictionary *tempContentDic;
    
    
    CCMenuItemLabel* counterBtn;
    CCMenuItemLabel* englishWordBtn;
    CCMenu *helpMenu;
    
    CCMenuItemImage *blueBtn;
    CCMenuItemImage *redBtn;
    CCMenuItemImage *blackBtn;
    CCMenuItemImage *greenBtn;
    
    int cId;
    int wordImg;
    int sessionCounter;
    
}

- (void) showMenu;
- (void) changeColor:(id) sender;
- (void) clearCanvas;


@end
