//
//  LevelOne.h
//  Smooth Drawing
//
//  Created by Muhammad Mosib on 7/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <AVFoundation/AVFoundation.h>
@interface LevelOne : CCLayer<AVAudioPlayerDelegate>
{
    AVAudioPlayer *player;
    NSMutableDictionary* levelsDict; 
    int sessionCounter;
    
    NSString* currentStageWord;
    NSString* currentStageWordAudio;
    
    CCLabelTTF* result;
    CCLabelTTF* wordsCounterlbl;
    int currentWord; // count that at which word we are currently at!
    CCMenu* mainMenu;
    
    int stage;
    NSMutableDictionary *tempContentDic;
    
    
    CCMenuItemLabel* counterBtn;
    CCMenuItemLabel* englishWordBtn;
    
    CCMenuItem*  helpMenu;
    int cId;
    int wordImg;
    int activeLanguage;
    int level;
    int correctAnswerCounter;
    
    // Report Data
    NSMutableArray *retval;
    int reportWordId;
    NSString *reportWordDesc;
    int reportCorrectCount;
    int reportTotatCount;
}
- (void) loadLevelData;
- (void) loadStageData: (int) stage;
- (void) showLevel;
- (void) showMenu;
- (void) checkResult;
- (void) playNextStage;
- (void) loadDatabase;

@end
