//
//  LevelOne.m
//  Smooth Drawing
//
//  Created by Muhammad Mosib on 7/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "LevelOne.h"
//#import "AppDelegate.h"
//#import "GameLayerWriteIt.h"
#import "GameLayerFillIt.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "SimpleAudioEngine.h"
#import "MyPlayGroundViewController.h"
#import "HelpSoundViewController.h"
#import "DocumentDirectory.h"
#import "MrPReport.h"

#define FIXIT 7



#define FILLIT 3

@implementation LevelOne

-(id) init
{
	if( (self=[super init])) 
    {

        [self loadDatabase];
        [self insitiantiateForReportCouting];
        [self showMenu];

        
        result = [CCLabelTTF labelWithString:@"" fontName:@"Courier-Bold" fontSize:50];
        result.position = ccp(750, 700);
        sessionCounter = 0;
        correctAnswerCounter = 0;
        [self addChild:result];
        
    }
	return self;
}


- (void) showMenu
{
    if ([tempContentDic count] > 0) {

    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FILLIT,cId,activeLanguage,ActiveGrade];
    currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:wordsCount]; 
        
        
    if ((currentWord == [tempContentDic count]/4) || (!currentWord)) {
        currentWord = 0;
    }
        
    NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],FILLIT,cId,activeLanguage];
    int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
    if (!LevelLastCount || LevelLastCount != 2) {
//        LevelLastCount = 1;
        level = 1;
    }   
        

        
//    else{
//    currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"wordForClassification%d",cId]]; 
//        later we may load current word from user defaults as well..
//    }
    CCLabelTTF* returnLbl = [CCLabelTTF labelWithString:@"Return" fontName:@"Courier-Bold" fontSize:50];
    returnLbl.anchorPoint = ccp(0, 0);
    returnLbl.color = ccRED;

    CCMenuItemLabel* returnBtn = [CCMenuItemLabel itemWithLabel:returnLbl target:self selector:@selector(returnPrevWord)];
    returnBtn.position = ccp(450, 60);

    CCLabelTTF* nextLbl = [CCLabelTTF labelWithString:@"Next" fontName:@"Courier-Bold" fontSize:40];
    nextLbl.anchorPoint = ccp(0, 0);
    
    CCMenuItemLabel* nextBtn = [CCMenuItemLabel itemWithLabel:nextLbl target:self selector:@selector(NextWord)];
    nextBtn.position = ccp(650, 60);
    nextLbl.color = ccGREEN;
    
    CCMenuItem *nxtBtn = [CCMenuItemImage 
                                itemFromNormalImage:@"next.png" selectedImage:@"next.png" 
                                target:self selector:@selector(NextWord)];
    nxtBtn.scaleX = 0.35;
    nxtBtn.scaleY = 0.35;
    nxtBtn.position = ccp(650, 700);
    
    CCMenuItem *retrnBtn = [CCMenuItemImage 
                          itemFromNormalImage:@"return.png" selectedImage:@"return.png" 
                          target:self selector:@selector(returnPrevWord)];
    retrnBtn.scaleX = 0.35;
    retrnBtn.scaleY = 0.35;
    retrnBtn.position = ccp(470, 700);
        
        CCMenuItem *checkmarkBtn = [CCMenuItemImage 
                                itemFromNormalImage:@"checkmark.png" selectedImage:@"checkmark.png" 
                                target:self selector:@selector(CheckWord)];
        checkmarkBtn.scaleX = 0.35;
        checkmarkBtn.scaleY = 0.35;
        checkmarkBtn.position = ccp(560, 700);
        
        CCMenuItem *helpBtn = [CCMenuItemImage 
                                              itemFromNormalImage:@"Sound_Help.png" selectedImage:@"Sound_Help.png" 
                                              target:self selector:@selector(HelpSound:)];
        helpBtn.position = ccp(903, 710);

    
    wordImg = [[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",currentWord]] intValue];
    if (!wordImg || wordImg == 0) {
        wordImg = 1;
    }
        
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString * documentsPath = [paths objectAtIndex:0];
        NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
        NSString * filename = [NSString stringWithFormat:@"%@/%d.png",activeGrade,wordImg];
        NSString * filePath = [documentsPath stringByAppendingPathComponent:filename];
        //    CCSprite * sprite;
        //If file exist in documents folder use that one
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            //        sprite = [CCSprite spriteWithFile:filePath];
        }
        else {
            filePath = [NSString stringWithFormat:@"1.png"];
        }
        
        filePath = filename;
        
        if(cId > 989 && cId < 999){
            filename = [NSString stringWithFormat:@"%@/%d.png",activeGrade,wordImg];
            filePath = [documentsPath stringByAppendingPathComponent:filename];
            if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                //        sprite = [CCSprite spriteWithFile:filePath];
            }
            else {
                filePath = [NSString stringWithFormat:@"2.png"];
            }
            
        }
        
        
    CCMenuItem *wordSprite = [CCMenuItemImage 
                            itemFromNormalImage:filePath selectedImage:filePath target:self selector:@selector(hearWord)];
    wordSprite.scaleX = 1.0;
    wordSprite.scaleY = 0.9;
    wordSprite.position = ccp(860, 500);
    
        self.isTouchEnabled = YES;
    
    NSString *Word = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",currentWord]];
    CCLabelTTF* englishWordLbl = [CCLabelTTF labelWithString:Word fontName:@"Courier-Bold" fontSize:50];
    englishWordLbl.anchorPoint = ccp(0, 0);
    englishWordLbl.color = ccBLACK;
    englishWordBtn = [CCMenuItemLabel itemWithLabel:englishWordLbl target:self selector:@selector(hearWord)];
    englishWordBtn.position = ccp(530, 230);
    
    int totalWords = [tempContentDic count]/4;
    NSString *wordsCunter = [NSString stringWithFormat:@"%d / %d",currentWord + 1, totalWords];
    
    wordsCounterlbl = [CCLabelTTF labelWithString:wordsCunter fontName:@"Courier-Bold" fontSize:20];
    wordsCounterlbl.anchorPoint = ccp(0, 0);
    
    counterBtn = [CCMenuItemLabel itemWithLabel:wordsCounterlbl target:self selector:@selector(playNextStage)];
    counterBtn.position = ccp(770, 650);
    
//        if ([self getChildByTag:125]) {
//            [[[self getChildByTag:125] getChildByTag:124] removeFromParentAndCleanup:YES];
//            [[[self getChildByTag:125] getChildByTag:123] removeFromParentAndCleanup:YES];
//        }


    
        // Points 1
        NSString *Points1 = [NSString stringWithFormat:@"Game%dLevel%d",FILLIT,1];
        NSString *lblLevel1 = [NSString stringWithFormat:@"Level 01 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points1]];
        CCLabelTTF *lblPoints1TTF = [CCLabelTTF labelWithString:lblLevel1 fontName:@"Helvetica" fontSize:20];
        lblPoints1TTF.anchorPoint = ccp(0, 0);
        lblPoints1TTF.color = ccBLACK;
        CCMenuItemLabel* pointsMenuItem1 = [CCMenuItemLabel itemWithLabel:lblPoints1TTF target:self selector:@selector(DoNothing:)];
        pointsMenuItem1.position = ccp(790, 720);
        pointsMenuItem1.tag =123;
        
        // Points 2
        NSString *Points2 = [NSString stringWithFormat:@"Game%dLevel%d",FILLIT,2];
        NSString *lblLevel2 = [NSString stringWithFormat:@"Level 02 : %d",[[NSUserDefaults standardUserDefaults] integerForKey:Points2]];
        CCLabelTTF *lblPoints2TTF = [CCLabelTTF labelWithString:lblLevel2 fontName:@"Helvetica" fontSize:20];
        lblPoints2TTF.anchorPoint = ccp(0, 0);
        lblPoints2TTF.color = ccBLACK;
        CCMenuItemLabel* pointsMenuItem2 = [CCMenuItemLabel itemWithLabel:lblPoints2TTF target:self selector:@selector(DoNothing:)];
        pointsMenuItem2.position = ccp(790, 690);
        pointsMenuItem2.tag =124;
        
//        CCMenu *pointsMenu = [CCMenu menuWithItems:pointsMenuItem1,pointsMenuItem2, nil];
//        [pointsMenu setEnabled:NO];
//        pointsMenu.position = CGPointZero;
//        [self addChild:pointsMenu z:25 tag:125];
        
    NSString* loadPrincipleRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentPrincipleString"];
    NSString* loadGrammarRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentGrammarString"];
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];

    if (cId == 99999) { // in case of My skills playground
        
        CCLabelTTF* myskillsLbl = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@ / %@",loadGrammarRule,loadPrincipleRule] fontName:@"Courier-Bold" fontSize:50];
        myskillsLbl.anchorPoint = ccp(0, 0);
        myskillsLbl.color = ccRED;
        
        CCMenuItemLabel* myskillsMenuItem = [CCMenuItemLabel itemWithLabel:myskillsLbl target:self selector:@selector(returnPrevWord)];
        myskillsMenuItem.position = ccp(470, 60);
        mainMenu = [CCMenu menuWithItems:/*returnBtn, nextBtn,*/myskillsMenuItem,helpBtn,retrnBtn,checkmarkBtn, wordSprite,nxtBtn,/*englishWordBtn,*/counterBtn,pointsMenuItem2,pointsMenuItem1, nil];
    }
    else {
            mainMenu = [CCMenu menuWithItems:/*returnBtn, nextBtn,*/retrnBtn,helpBtn, checkmarkBtn,nxtBtn,wordSprite,/*englishWordBtn,*/counterBtn,pointsMenuItem2,pointsMenuItem1, nil];
        
    }
    

    mainMenu.anchorPoint = ccp(0, 0);
    mainMenu.position = ccp(0, 0);
    [self addChild:mainMenu];
    }
    else {
        if (cId > 989 && cId < 999) {
            [[GameLayerFillIt sharedLayer] setUserEnteredText];
            UIAlertView *alert = [[UIAlertView alloc] init];
            if (activeLanguage == 1) {
                [alert setTitle:@"احذر"]; 
                [alert setMessage:@" البيانات لا تكفي في ملفاتك"];  
            }
            else {
                [alert setTitle:@"WARNING"]; 
                [alert setMessage:@"Data NOT sufficient in your folder"];
            }

            [alert setDelegate:self];
            [alert addButtonWithTitle:@"OK"];
            [alert setTag:2];
            [alert show];
            [alert release];
        }
        else {
//            if (cId == 99999) {
//                UIAlertView *alert = [[UIAlertView alloc] init];
//                if (activeLanguage == 1) {
//                    [alert setTitle:@"حذرآسف"]; 
//                    [alert setMessage:@" قاعدة الدرس الحالية لا تدعم هذه اللعبة"];  
//                }
//                else {
//                    [alert setTitle:@"WARNING"]; 
//                    [alert setMessage:@"SORRY! Current Selected Grammar Rule doesnot support this game"];
//                }
//
//                [alert setDelegate:self];
//                [alert addButtonWithTitle:@"OK"];
//                [alert setTag:2];
//                [alert show];
//                [alert release];
//            }
//        else {
            UIAlertView *alert = [[UIAlertView alloc] init];
            [alert setTitle:@"WARNING"]; 
            [alert setMessage:@"SORRY! Data No Sufficient. Please Make Sure you have selected correct Object"];
            [alert setDelegate:self];
            [alert setTag:2];
            [alert addButtonWithTitle:@"OK"];
            [alert show];
            [alert release];
        }
    }
}

-(void) sessionCompletionCheck{
    UIAlertView *sessionAlert;
    if (correctAnswerCounter > 7) {
        
        if (activeLanguage == 2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good!" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:10];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"RewardUnlocked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        if (activeLanguage ==2) {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Your progress needs improvement. Try again." delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
        }
        else {
            sessionAlert = [[UIAlertView alloc] initWithTitle:@"عفوا !" message:@"تقدمك غير كافي , يجب أن تحاول أكثر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب" otherButtonTitles:@"خروج", nil];
        }
        [sessionAlert setTag:1];
    }

    [sessionAlert setTag:1];
    [sessionAlert show];
    [sessionAlert release];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    
    if(alertView.tag == 2)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        }
        [[GameLayerFillIt sharedLayer] removeFillItScene];
//        [self updateReportCurrentWord];
//        [self saveProgressForReport];
    }
    else {
        
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
            [self NextWord];
        }
        if (buttonIndex == 1)
        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            }
            [[GameLayerFillIt sharedLayer] removeFillItScene];
//                    [self updateReportCurrentWord];
//                    [self saveProgressForReport];
        }
    }     
}
-(void) NextWord{

    
    if (sessionCounter == 10) {
        sessionCounter = 0;
        [self sessionCompletionCheck];
        return;
    }
    sessionCounter = sessionCounter + 1;
    
    // if correct, Go ahead to next Word    
    currentWord  = currentWord + 1;
    if (currentWord > [tempContentDic count]/4) {
        level = level + 1;
                    [self saveProgressForReport];
    }
    NSString *wordSaver = [NSString stringWithFormat:@"fillItwordForClassification%d",cId];
    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FILLIT,cId,activeLanguage,ActiveGrade];
    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self updateProgress:FALSE];
    [self removeChild:mainMenu cleanup:YES];
    [self showMenu];
    }

-(void) CheckWord{    
    
    if (sessionCounter == 10) {
        sessionCounter = 0;
        [self sessionCompletionCheck];
        return;
    }
    sessionCounter = sessionCounter + 1;
 
    NSString *userEnteredtext = [[GameLayerFillIt sharedLayer] getUserEnteredText];
    NSString *correctWord = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",currentWord]];
//    if ([correctWord isEqualToString:userEnteredtext]) {
//        if   ([correctWord caseInsenstiveCompare:userEnteredtext] == NSOrderedSame){
        if ([correctWord caseInsensitiveCompare:userEnteredtext] == NSOrderedSame){
        [[GameLayerFillIt sharedLayer] setUserEnteredText];
            
            
        reportCorrectCount = reportCorrectCount + 1;
        reportTotatCount = reportTotatCount + 1;

        [self updateReportCurrentWord];
        [self saveProgressForReport];
            [self insitiantiateForReportCouting];
        
        // if correct, Go ahead to next Word
        currentWord  = currentWord + 1;
        correctAnswerCounter = correctAnswerCounter + 1;
        if (currentWord > [tempContentDic count]/4) {
            level = level + 1;
                        [self saveProgressForReport];
            [self insitiantiateForReportCouting];
        }
        NSString *wordSaver = [NSString stringWithFormat:@"fillItwordForClassification%d",cId];
        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
            

        
        [self updateProgress:TRUE];
        [self removeChild:mainMenu cleanup:YES];
        [self showMenu]; 
            [self playCheeringSound];
    }
    else{
        [self playEncouragingSound];
        reportTotatCount = reportTotatCount + 1;
        
        [self updateProgress:FALSE];
        
    }
}

-(void) playCheeringSound{
    
    
    int randomNumer = arc4random() % 3 ;
    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
        
    }
    
}

-(void) playEncouragingSound{
    
    int randomNumer = arc4random() % 3 ;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC25";
        NSString *second = @"EC30";
        NSString *third = @"EC31";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);  
        
    }else {
        NSString *first = @"AC04";
        NSString *second = @"AC05";
        NSString *third = @"AC06";
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }
}
-(void) updateProgress:(BOOL) correctAnswer{
    
    if (correctAnswer) {
        
        NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
        int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount]; 
        totalWordsLastCount = totalWordsLastCount + 1;
        
        NSString *correctWordsCount = [NSString stringWithFormat:@"correctWordsCount"];
        int correctWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:correctWordsCount];
        correctWordsLastCount = correctWordsLastCount + 1;
        
        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",[Common getPinNumber],FILLIT,cId,activeLanguage];
        int triesLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:triesCount];
        
        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",FILLIT,level];
        float PointsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:Points];
        if (!PointsLastCount) {
            PointsLastCount = 0;
        }
        
        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",[Common getPinNumber],FILLIT,cId,activeLanguage];
        int LevelLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:levelCount];
        
        if (triesLastCount < 10) {
            if (LevelLastCount == 2) {
//                PointsLastCount = PointsLastCount + 2 * 1.5;
                
            }
            else {
                PointsLastCount = PointsLastCount + (2 * 1.5);                
            }
            
        }   
        else {
            
        }
        triesLastCount = 0;                        
        NSLog(@"Tries Last Count is %d", triesLastCount);
        NSLog(@"Current Points is %f", PointsLastCount);
        NSLog(@"Current Word is %d", currentWord);
        //        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",FIXIT,cId,activeLanguage];
        [[NSUserDefaults standardUserDefaults] setInteger:triesLastCount forKey:triesCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
            NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        NSString *wordsCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dWordsCount%@",[Common getPinNumber],FILLIT,cId,activeLanguage,ActiveGrade];
        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordsCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //        NSString *levelCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dLevelCount",FIXIT,cId,activeLanguage];
        [[NSUserDefaults standardUserDefaults] setInteger:level forKey:levelCount];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        //        NSString *Points = [NSString stringWithFormat:@"Game%dLevel%d",FIXIT,level];
        [[NSUserDefaults standardUserDefaults] setFloat:PointsLastCount forKey:Points]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:totalWordsLastCount forKey:totalWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:correctWordsLastCount forKey:correctWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    else {
        
        NSString *totalWordsCount = [NSString stringWithFormat:@"totalWordsCount"];
        int totalWordsLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:totalWordsCount]; 
        totalWordsLastCount = totalWordsLastCount + 1;
        
        NSString *triesCount = [NSString stringWithFormat:@"Pin%@Game%dClassification%dlanguage%dtriesCount",[Common getPinNumber],FILLIT,cId,activeLanguage];
        int triesLastCount = [[NSUserDefaults standardUserDefaults] integerForKey:triesCount];
        triesLastCount = triesLastCount + 1;                
        
        [[NSUserDefaults standardUserDefaults] setInteger:totalWordsLastCount forKey:totalWordsCount]; // update points
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setInteger:triesLastCount forKey:triesCount];
        [[NSUserDefaults standardUserDefaults] synchronize];  
        
        NSLog(@"Tries Last Count is %d", triesLastCount);
    }    
}


-(void) returnPrevWord{
//    if (currentWord) {
//        currentWord  = currentWord - 1;
//        NSString *wordSaver = [NSString stringWithFormat:@"wordForClassification%d",cId];
//        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
//        [self removeChild:mainMenu cleanup:YES];
//        [self showMenu];
//    }

    
    [[GameLayerFillIt sharedLayer] setUserEnteredText];

}

-(void) hearWord{

//    NSString* soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
//    NSString *squishPath = [DocumentDirectory playSound:soundName];

//    if (squishPath == nil || !squishPath) {
//        NSLog(@"Sound NOt found");
//        NSString *soundNameNil = @"EmptySound";
//        NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundNameNil ofType:@"mp3"];
//        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
//        SystemSoundID squishSoundID;
//        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
//        AudioServicesPlaySystemSound(squishSoundID);
//        
//        return;
//    }
//    NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
//    SystemSoundID squishSoundID;
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
//    AudioServicesPlaySystemSound(squishSoundID);
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }  

    
    NSString* soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
    NSString *squishPath = [DocumentDirectory playSound:soundName];
    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;    
    [player play];


}
-(void) loadDatabase{
    
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    //    int LanguageSupport = [[NSUserDefaults standardUserDefaults] integerForKey:@"LanguageSupport"];            
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    
    int position = 0;
    
    // cId 99999 means its myskillscamp, otherwise its simple classification
    if (cId == 99999) {

        
        int gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
        int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
        
        int position = 0;
        
        if (activeLanguage == 2) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
                position++;
//                }
                
            } 
        }

        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) { 
//                if (info.isSentence == 1) {
//                    
//                }
//                else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
                position++;
//                }
                
            }          
        }
    }
 

        // if goes in the loop below, it means its simple classification from city map
    else 
    {        
    if (activeLanguage == 2 || (cId > 989 && cId < 999)) {
        NSArray *MrPContentArray = [database EnglishWordDatabaseArray:cId];
        
        tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
        for (MrPContentInfo *info in MrPContentArray) {
//            if (info.isSentence == 1) {
//                
//            }
//            else {
            [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
            [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
            [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];          
            [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
            position++;
//            }
            
        } 
    }
    else {
        
        NSArray *MrPContentArray = [database ArabicWordDatabaseArray:cId];
        
        tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
        for (MrPContentInfo *info in MrPContentArray) {
//            if (info.isSentence == 1) {
//                
//            }
//            else {
            [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
            [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]]; // keeping it english word so in class, our convention remain same
            [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];          
            [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
            position++;
            }
//            }
        }
    } 
    
    [database release];
    database = Nil;
}

- (void)HelpSound:(id)sender {
    // 159,56
    [[NSUserDefaults standardUserDefaults] setObject:@"GH020" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    CCMenuItem *helpBtnEnglish = [CCMenuItemImage 
                           itemFromNormalImage:@"HelpEnglish.png" selectedImage:@"HelpEnglish.png" 
                                  target:self selector:@selector(playHelpSoundEng)];

    helpBtnEnglish.position = ccp(901, 640);
    
    CCMenuItem *helpBtnArabic = [CCMenuItemImage 
                                  itemFromNormalImage:@"HelpArabic.png" selectedImage:@"HelpArabic.png" 
                                  target:self selector:@selector(playHelpSoundAra)];
    
    helpBtnArabic.position = ccp(901, 585);
    
    helpMenu = [CCMenu menuWithItems:helpBtnEnglish,helpBtnArabic, nil];
    helpMenu.anchorPoint = ccp(0, 0);
    helpMenu.position = ccp(0, 0);
    [self addChild:helpMenu];    
} 

-(void) playHelpSoundEng{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayEnglishHelpSound:nil];
//    [helpSoundVC release];
//    helpSoundVC = Nil;

}
-(void) playHelpSoundAra{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayArabicHelpSound:nil];
//    [helpSoundVC release];
//    helpSoundVC = Nil;
    
}

-(void) saveProgressForReport{
    // write in db!
    
    MrPDatabase *database = [[MrPDatabase alloc] init];
    [database saveReportProgress:retval];
    
    //    NSLog(@"temp is %@",tempContentDic);
    
    if (retval) {
        [retval removeAllObjects];
        [retval release];
        retval = nil;
    }
}
-(void) updateReportCurrentWord{
    reportWordDesc = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",currentWord]];
    reportWordId = [[tempContentDic objectForKey:[NSString stringWithFormat:@"wordId%d",currentWord]] intValue];
    int gameId = 7;
    MrPReport *info = [[MrPReport alloc]
                       initWithWordId:reportWordId andWordDesc:reportWordDesc andCorrectCount:reportCorrectCount andTotalCount:reportTotatCount andActiveLanguage:activeLanguage andClassification:cId andGameId:gameId];
    
    [retval addObject:info];
    
    reportTotatCount = 0;
    reportCorrectCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
    
}

-(void) insitiantiateForReportCouting{
    retval = [[NSMutableArray alloc] init];
    reportCorrectCount = 0;
    reportTotatCount = 0;
    reportWordDesc = @"";
    reportWordId = 0;
}

-(void) DoNothing{
}

- (void)ccTouchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{    
    if(helpMenu)
        {
            [helpMenu removeFromParentAndCleanup:YES];
            helpMenu = nil;
        }
 
}



@end
