//
//  HelloWorldLayer.m
//  FunLearning
//
//  Created by Muhammad Mosib on 7/22/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.

//

#import "GameLayerWriteIt.h"
#import "LevelTwo.h"
#import "SimpleAudioEngine.h"
#import "Common.h"
@implementation GameLayerWriteIt

@synthesize stage;

static GameLayerWriteIt* _sharedLayer;
//AppController *navController_;

+ (GameLayerWriteIt*) sharedLayer
{
    if (_sharedLayer == NULL) {
        [NSException exceptionWithName:@"GameLayerWriteIt" reason:@"Layer not initialized" userInfo:nil];
    }
    return _sharedLayer;
}

+ (CCScene *) scene
{
	CCScene *scene = [CCScene node];
	GameLayerWriteIt *layer = [GameLayerWriteIt node];
	[scene addChild: layer];
	return scene;
}

- (id) init
{
	if( (self=[super init])) 
    {
        _sharedLayer = self;
        
        if (stage == 0) {
            stage = 1;
        }
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"1-writeIT-TypeIT.mp3" loop:YES];
            [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume :0.10f];
        }
        CCSprite* background = [CCSprite spriteWithFile:@"Writeit.jpg"];
        background.anchorPoint = ccp(0, 0);
        [self addChild:background z:-1];
        background.position = ccp(0, 0);
        
        CCSprite* hearImg = [CCSprite spriteWithFile:@"hear.png"];
        background.anchorPoint = ccp(0,0);
        hearImg.scaleX = 0.20;
        hearImg.scaleY = 0.20;
        [self addChild:hearImg z:-1];
        hearImg.position = ccp(750, 218);

        CCMenuItem *goToPlayGroundMenuitem = [CCMenuItemImage
                                              itemFromNormalImage:@"MyPlayground_btn.png" selectedImage:@"MyPlayground_btn.png" 
                                              target:self selector:@selector(removeWriteItScene)];
        goToPlayGroundMenuitem.position = ccp(120, 700);
        goToPlayGroundMenuitem.scaleX = 0.7;
        goToPlayGroundMenuitem.scaleY = 0.6;
        CCMenu *goToPlayGroundMenu = [CCMenu menuWithItems:goToPlayGroundMenuitem, nil];
        goToPlayGroundMenu.position = CGPointZero;
        [self addChild:goToPlayGroundMenu];   
    
        [self showLevelOne];
        
    }
	return self;
}



-(void) startGame:(int) pStageNumber{


}

-(void) hearSpelling{

//    NSString* filePath;
//    int currentLanguageId = 0;
//    switch (currentLanguageId) {
//        case 0:
//            filePath = @"LevelsEnglish";
//            break;
//        case 1:
//            filePath = @"LevelsArabic";
//            break;
//            
//            /* Use remaining languages ids */
//        default:
//            break;
//    }
//    
//    NSString *myFile = [[NSBundle mainBundle] pathForResource:filePath ofType:@"plist"];
//    NSMutableDictionary *levelsDict = [[NSMutableDictionary alloc] initWithContentsOfFile:myFile];
//    
//    NSString *stageArr = [NSString stringWithFormat:@"Stage%i", stage];        
//    NSString *currentStageWordAudio = [[levelsDict objectForKey:stageArr] objectAtIndex:1];
//        
//    NSString *squishPath = [[NSBundle mainBundle] pathForResource:currentStageWordAudio ofType:@"mp3"];
//    NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
//    SystemSoundID squishSoundID;
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
//    AudioServicesPlaySystemSound(squishSoundID);
//
//    
//    [mainMenu setEnabled:NO];
//    [levelWordMenu setEnabled:NO];
//    soundCompletionTimer = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(soundCompleted) userInfo:nil repeats:NO];
//
}

-(void) soundCompleted{
//        [mainMenu setEnabled:YES];
//    if (soundCompletionTimer) {
//        [soundCompletionTimer invalidate];
//        soundCompletionTimer = nil;
//    } 

//    [self startWritingExcercise];
}

- (void) showLevelOne
{
    levelTwoLayer = [[LevelTwo alloc] init];
    [self addChild:levelTwoLayer];   
  
}

- (void) switchToLevelTwo
{
    // On correct Answer, load next Data
}

-(void) removeWriteItScene{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    }
    [[CCDirector sharedDirector] end];
    [[CCDirector sharedDirector].openGLView removeFromSuperview]; 
}

- (void) dealloc
{
	//[super dealloc];
}
@end
