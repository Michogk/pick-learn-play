//
//  HelloWorldLayer.m
//  FunLearning
//
//  Created by Muhammad Mosib on 7/22/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.

//

#import "GameLayerFillIt.h"
#import "LevelTwo.h"
#import "SimpleAudioEngine.h"
#import "Common.h"
#import "HelpSoundViewController.h"

@implementation GameLayerFillIt

@synthesize stage;

static GameLayerFillIt* _sharedLayer;
//AppController *navController_;

+ (GameLayerFillIt*) sharedLayer
{
    if (_sharedLayer == NULL) {
        [NSException exceptionWithName:@"GameLayerFillIt" reason:@"Layer not initialized" userInfo:nil];
    }
    return _sharedLayer;
}
//+ (void) setNavController:(AppController*) navCon
//{
//    navController_ = navCon;
//}
//+ (AppController*) getNavController
//{
//    return navController_;
//}

+ (CCScene *) scene
{
    CCScene *scene = [CCScene node];
    GameLayerFillIt *layer = [GameLayerFillIt node];
    [scene addChild: layer];
    return scene;
}

- (id) init
{
    if( (self=[super init])) 
    {
        _sharedLayer = self;
        
        if (stage == 0) {
            stage = 1;
        }
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"1-writeIT-TypeIT.mp3" loop:YES];
            [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume :0.10f];
        }
        
//        myText = [[UITextField alloc] initWithFrame:CGRectMake(0, 570, 555, 300)];
        myText = [[UITextField alloc] initWithFrame:CGRectMake(45, 100, 530, 300)];
        [myText setDelegate:self];
        [myText setFont:[UIFont systemFontOfSize:60]];
        [myText setText:@""];
        [myText setTextColor: [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0]];
        myText.adjustsFontSizeToFitWidth = YES;
        [[[[CCDirector sharedDirector] openGLView] window] addSubview:myText];
        [myText becomeFirstResponder];
        
        //playerTextField.bounds = CGRectMake(0, 0, 145, 38);
//        myText.transform = CGAffineTransformConcat(myText.transform, CGAffineTransformMakeRotation(M_PI_2+3.14));
        
        CCSprite* background = [CCSprite spriteWithFile:@"typeit.png"];
        background.anchorPoint = ccp(0, 0);
        [self addChild:background z:-1];
        background.position = ccp(0, 0);
        
        
        CCSprite* hearImg = [CCSprite spriteWithFile:@"hear.png"];
        background.anchorPoint = ccp(0,0);
        hearImg.scaleX = 0.20;
        hearImg.scaleY = 0.20;
        [self addChild:hearImg z:-1];
        hearImg.position = ccp(741, 603);
        
        CCMenuItem *goToPlayGroundMenuitem = [CCMenuItemImage 
                                              itemFromNormalImage:@"MyPlayground_btn.png" selectedImage:@"MyPlayground_btn.png" 
                                              target:self selector:@selector(removeFillItScene)];
        goToPlayGroundMenuitem.position = ccp(120, 700);
        goToPlayGroundMenuitem.scaleX = 0.7;
        goToPlayGroundMenuitem.scaleY = 0.6;
        CCMenu *goToPlayGroundMenu = [CCMenu menuWithItems:goToPlayGroundMenuitem, nil];
        goToPlayGroundMenu.position = CGPointZero;
        [self addChild:goToPlayGroundMenu];   

        [self showLevelOne];
        
    }
    return self;
}



-(void) startGame:(int) pStageNumber{
    
    // Load Data from database as progression demands
    
//    stage = pStageNumber;
//    if (!levelInProgress) {
//        
//        NSString* filePath;
//        int currentLanguageId = 0;
//        switch (currentLanguageId) {
//            case 0:
//                filePath = @"LevelsEnglish";
//                break;
//            case 1:
//                filePath = @"LevelsArabic";
//                break;
//                
//                /* Use remaining languages ids */
//            default:
//                break;
//        }
//        
//        NSString *myFile = [[NSBundle mainBundle] pathForResource:filePath ofType:@"plist"];
//        NSMutableDictionary *levelsDict = [[NSMutableDictionary alloc] initWithContentsOfFile:myFile];
//        
//        NSString *stageArr = [NSString stringWithFormat:@"Stage%i", stage];        
//        NSString *currentStageWord = [[levelsDict objectForKey:stageArr] objectAtIndex:0];
//        
//        
//        levelWord = [CCLabelTTF labelWithString:currentStageWord fontName:@"Courier-Bold" fontSize:225];
//        levelWord.anchorPoint = ccp(0, 0);
//        levelWord.color = ccBLACK;
//        
//        CCMenuItemLabel* tapWordBtn = [CCMenuItemLabel itemWithLabel:levelWord target:self selector:@selector(hearSpelling)];
//        tapWordBtn.position = ccp(520, 450);
//        
//        levelWordMenu = [CCMenu menuWithItems:tapWordBtn, nil];
//        levelWordMenu.anchorPoint = ccp(0, 0);
//        levelWordMenu.position = ccp(0, 0);
//        [self addChild:levelWordMenu];
//        [levelWordMenu setEnabled:YES];
//        
//    }
//    else {
//        //        [self startWritingExcercise];
//    }
    
}

-(void) hearSpelling{
    
//    NSString* filePath;
//    int currentLanguageId = 0;
//    switch (currentLanguageId) {
//        case 0:
//            filePath = @"LevelsEnglish";
//            break;
//        case 1:
//            filePath = @"LevelsArabic";
//            break;
//            
//            /* Use remaining languages ids */
//        default:
//            break;
//    }
//    
//    NSString *myFile = [[NSBundle mainBundle] pathForResource:filePath ofType:@"plist"];
//    NSMutableDictionary *levelsDict = [[NSMutableDictionary alloc] initWithContentsOfFile:myFile];
//    
//    NSString *stageArr = [NSString stringWithFormat:@"Stage%i", stage];        
//    NSString *currentStageWordAudio = [[levelsDict objectForKey:stageArr] objectAtIndex:1];
//    
//    NSString *squishPath = [[NSBundle mainBundle] pathForResource:currentStageWordAudio ofType:@"mp3"];
//    NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
//    SystemSoundID squishSoundID;
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
//    AudioServicesPlaySystemSound(squishSoundID);
//    
//    
//    [mainMenu setEnabled:NO];
//    [levelWordMenu setEnabled:NO];
//    soundCompletionTimer = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(soundCompleted) userInfo:nil repeats:NO];
//    
    
}

-(void) soundCompleted{
    
//    [mainMenu setEnabled:YES];
//    if (soundCompletionTimer) {
//        [soundCompletionTimer invalidate];
//        soundCompletionTimer = nil;
//    } 
//    
//    //    [self startWritingExcercise];
    
    
}

-(NSString *) getUserEnteredText{
    return myText.text;

}

-(void ) setUserEnteredText{
    [myText resignFirstResponder];
    myText.text = @"";
    
}
- (void) showLevelOne
{    
    levelOneLayer = [[LevelOne alloc] init];
    [self addChild:levelOneLayer];   
}

-(void) removeFillItScene{
    
    [myText resignFirstResponder];
    myText.text = @"";
    [myText removeFromSuperview];
    myText = nil;
    [myText release];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    }
     
    [[CCDirector sharedDirector] end];
    [[CCDirector sharedDirector].openGLView removeFromSuperview]; 
}
- (void) dealloc
{
    //[super dealloc];
}
@end
