//
//  LevelTwo.m
//  Smooth Drawing
//
//  Created by Muhammad Mosib on 7/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "LevelTwo.h"
#import "GameLayerWriteIt.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "SimpleAudioEngine.h"
#import "HelpSoundViewController.h"
#import "DocumentDirectory.h"



@implementation LevelTwo

-(id) init
{
	if( (self=[super init])) 
    {
        //CGSize size = [[CCDirector sharedDirector] winSize];
        
//        CCSprite* pencil = [CCSprite spriteWithFile:@"pensil.png"];
//        [self addChild:pencil z:-1];
//        pencil.position = ccp(350, 50);      
        stage = [[GameLayerWriteIt sharedLayer] stage];
        
        NSString* filePath;
        int currentLanguageId = 0;
        switch (currentLanguageId) {
            case 0:
                filePath = @"LevelsEnglish";
                break;
            case 1:
                filePath = @"LevelsArabic";
                break;
                
                /* Use remaining languages ids */
            default:
                break;
        }
        
        NSString *myFile = [[NSBundle mainBundle] pathForResource:filePath ofType:@"plist"];
        NSMutableDictionary *levelsDict = [[NSMutableDictionary alloc] initWithContentsOfFile:myFile];
        
        NSString *stageArr = [NSString stringWithFormat:@"Stage%i", stage];        
        NSString *currentStageWord = [[levelsDict objectForKey:stageArr] objectAtIndex:0];
        [levelsDict release];
        levelsDict = Nil;
        UIGraphicsBeginImageContext(CGSizeMake(1000 , 850));
        UIFont *font = [UIFont fontWithName:@"Futura" size:550];
        [currentStageWord drawAtPoint:CGPointMake(10, 20)withFont:font];
        

        UIImage* result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    

        wordSprite = [CCSprite spriteWithCGImage:result.CGImage key:[NSString stringWithFormat:@"Result%d",stage]];
        wordSprite.position = ccp(520, 350);
        wordSprite.opacity = 100;
        wordSprite.color = ccBLACK;
        [self addChild:wordSprite];
        
        [self loadDatabase];
        
        [self showMenu];
        [self showUpMenu];
        myDrawingCanvas = [LineDrawer node];
        [self addChild:myDrawingCanvas];
                
    }
	return self;
}

- (void) showMenu
{
    //black

    blackBtn = [CCMenuItemImage itemFromNormalImage:@"black.jpg" selectedImage:@"black.jpg" target:self selector:@selector(changeColor:)];
    blackBtn.position = ccp(50, 50);
    blackBtn.tag = 0;
    
    redBtn = [CCMenuItemImage itemFromNormalImage:@"Red.png" selectedImage:@"Red.png" target:self selector:@selector(changeColor:)];

    redBtn.position = ccp(120, 50);
    //redBtn.anchorPoint = ccp(0, 0);
    redBtn.tag = 1;
    
    greenBtn = [CCMenuItemImage itemFromNormalImage:@"green.png" selectedImage:@"blue.png" target:self selector:@selector(changeColor:)];
    greenBtn.position = ccp(190, 50);
    //greenBtn.anchorPoint = ccp(0, 0);
    greenBtn.tag = 2;
    
    blueBtn = [CCMenuItemImage itemFromNormalImage:@"blue.png" selectedImage:@"blue.png" target:self selector:@selector(changeColor:)];
    blueBtn.position = ccp(260, 50);
    //blueBtn.anchorPoint = ccp(0, 0);
    blueBtn.tag = 3;
    

    colorMenu = [CCMenu menuWithItems: blackBtn,redBtn, greenBtn, blueBtn, nil];
    colorMenu.anchorPoint = ccp(0, 0);
    colorMenu.position = ccp(0, 0);
    [self addChild:colorMenu];
    
    [self changeColor:blackBtn];
}

- (void) clearCanvas
{
    [myDrawingCanvas clear];
    NSLog(@"Cleared");
}

- (void) changeColor:(id) sender
{
    for (CCMenuItemImage* tempBtn in colorMenu.children) {
        tempBtn.scale = 1;
    }  
    
    CCMenuItemImage* btn = (CCMenuItemImage*) sender;
    btn.scale = 1.3f;
    [myDrawingCanvas setDrawingColor:btn.tag];
    NSLog(@"Color changed");

    [[NSUserDefaults standardUserDefaults] setInteger:[sender tag] forKey:@"pencilColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) showUpMenu
{
    if ([tempContentDic count] > 0 && (cId < 999 || cId > 989)) {
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        currentWord = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"writeItwordForClassification%dLanguage%d%@",cId,activeLanguage,ActiveGrade]]; 
    if ((currentWord == [tempContentDic count]/4) || (!currentWord) || currentWord > [tempContentDic count]/4) {
        currentWord = 0;
    }

    CCLabelTTF* returnLbl = [CCLabelTTF labelWithString:@"Return" fontName:@"Courier-Bold" fontSize:50];
    returnLbl.anchorPoint = ccp(0, 0);
    returnLbl.color = ccRED;
    
    CCMenuItemLabel* returnBtn = [CCMenuItemLabel itemWithLabel:returnLbl target:self selector:@selector(returnPrevWord)];
    returnBtn.position = ccp(450, 60);
    
    CCLabelTTF* nextLbl = [CCLabelTTF labelWithString:@"Next" fontName:@"Courier-Bold" fontSize:40];
    nextLbl.anchorPoint = ccp(0, 0);
    
    CCMenuItemLabel* nextBtn = [CCMenuItemLabel itemWithLabel:nextLbl target:self selector:@selector(NextWord)];
    nextBtn.position = ccp(650, 60);
    nextLbl.color = ccGREEN;
    
    CCMenuItem *nxtBtn = [CCMenuItemImage 
                          itemFromNormalImage:@"next.png" selectedImage:@"next.png" 
                          target:self selector:@selector(NextWord)];
    nxtBtn.scaleX = 0.35;
    nxtBtn.scaleY = 0.35;
    nxtBtn.position = ccp(650, 700);
    
    CCMenuItem *retrnBtn = [CCMenuItemImage 
                            itemFromNormalImage:@"return.png" selectedImage:@"return.png" 
                            target:self selector:@selector(returnPrevWord)];
    retrnBtn.scaleX = 0.35;
    retrnBtn.scaleY = 0.35;
    retrnBtn.position = ccp(560, 700);

        
        CCMenuItem *helpBtn = [CCMenuItemImage 
                               itemFromNormalImage:@"Sound_Help.png" selectedImage:@"Sound_Help.png" 
                               target:self selector:@selector(HelpSound:)];
        helpBtn.position = ccp(903, 710);
        self.isTouchEnabled = YES;
        
    NSString *Word = [tempContentDic objectForKey:[NSString stringWithFormat:@"englishWord%d",currentWord]];

    if (!Word) {
        CCLabelTTF *englishWordLbl = [[CCLabelTTF alloc] initWithString: @""
                                                             dimensions: CGSizeMake(958,267)
                                                              alignment: UITextAlignmentCenter
                                                               fontName: @"Futura"
                                                               fontSize: 180];  
        
        
        englishWordLbl.color =  ccc3(0,0,0);
        englishWordLbl.anchorPoint = ccp(0,0);
        englishWordBtn = [CCMenuItemLabel itemWithLabel:englishWordLbl target:self selector:nil];
        [englishWordLbl release];
        englishWordLbl = Nil;
        englishWordBtn.position = ccp(500, 430);
        englishWordBtn.isEnabled = FALSE;
 
            
    }
    else {
        

        CCLabelTTF *englishWordLbl = [[CCLabelTTF alloc] initWithString: Word
                                                    dimensions: CGSizeMake(958,237)
                                                     alignment: UITextAlignmentCenter
                                                      fontName: @"Futura"
                                                      fontSize: 145];  
        
        
//    englishWordLbl.color =  ccc3(245, 222, 179);
    englishWordLbl.anchorPoint = ccp(0,0);
    englishWordBtn = [CCMenuItemLabel itemWithLabel:englishWordLbl target:self selector:nil];
        [englishWordLbl release];
        englishWordLbl = Nil;
    englishWordBtn.position = ccp(500, 420);
    englishWordBtn.isEnabled = FALSE;
//        englishWordBtn.color = ccBLUE;
    }
        
        
    int totalWords = [tempContentDic count]/4;
    NSString *wordsCunter = [NSString stringWithFormat:@"%d / %d",currentWord + 1, totalWords];
    
//    wordsCounterlbl = [CCLabelTTF labelWithString:wordsCunter fontName:@"Courier-Bold" fontSize:20];
    wordsCounterlbl = [CCLabelTTF labelWithString:wordsCunter fontName:[UIFont systemFontOfSize:[UIFont systemFontSize]].fontName fontSize:20];
        
    wordsCounterlbl.anchorPoint = ccp(0, 0);
    
    counterBtn = [CCMenuItemLabel itemWithLabel:wordsCounterlbl target:self selector:@selector(playNextStage)];
    counterBtn.position = ccp(770, 650);
    
    
    wordImg = [[tempContentDic objectForKey:[NSString stringWithFormat:@"imageId%d",currentWord]] intValue];
    if (!wordImg || wordImg == 0) {
        wordImg = 1;
    }
        
        
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsPath = [paths objectAtIndex:0];
            NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    NSString * filename = [NSString stringWithFormat:@"%@/%d.png",activeGrade,wordImg];
    NSString * filePath = [documentsPath stringByAppendingPathComponent:filename];
    if (cId > 999) {
            filename = [NSString stringWithFormat:@"%d.png",wordImg];
    }
    filePath = filename;
        if(cId > 989 && cId < 999){
            filename = [NSString stringWithFormat:@"%@/%d.png",activeGrade,wordImg];
            filePath = [documentsPath stringByAppendingPathComponent:filename];
            if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                //        sprite = [CCSprite spriteWithFile:filePath];
            }
            else {
                filePath = [NSString stringWithFormat:@"2.png"];
            }

        }
    //If file exist in documents folder use that one

    CCMenuItem *wordSprte = [CCMenuItemImage 
                              itemFromNormalImage:filePath selectedImage:filePath target:self selector:@selector(hearWord)];
    wordSprte.scaleX = 0.8;
    wordSprte.scaleY = 0.8;
    wordSprte.position = ccp(886, 125);
    
    
    NSString* loadPrincipleRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentPrincipleString"];        
    NSString* loadGrammarRule = [[NSUserDefaults standardUserDefaults] valueForKey:@"currentGrammarString"];
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    
    if (cId == 99999) { // in case of My skills playground
        
//        CCLabelTTF* myskillsLbl = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@ / %@",loadGrammarRule,loadPrincipleRule] fontName:@"Courier-Bold" fontSize:50];
        
        CCLabelTTF* myskillsLbl = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%@ / %@",loadGrammarRule,loadPrincipleRule] fontName:[UIFont systemFontOfSize:[UIFont systemFontSize]].fontName fontSize:50];
        
        
        myskillsLbl.anchorPoint = ccp(0, 0);
        myskillsLbl.color = ccRED;
        
        CCMenuItemLabel* myskillsMenuItem = [CCMenuItemLabel itemWithLabel:myskillsLbl target:self selector:@selector(returnPrevWord)];
        myskillsMenuItem.position = ccp(470, 170);
        mainMenu = [CCMenu menuWithItems:/*returnBtn, nextBtn,*/myskillsMenuItem,retrnBtn, helpBtn,wordSprte,nxtBtn,englishWordBtn,counterBtn, nil];
    }
    else {
        mainMenu = [CCMenu menuWithItems:/*returnBtn, nextBtn,*/retrnBtn, nxtBtn,wordSprte,helpBtn,englishWordBtn,counterBtn, nil];
    }
    mainMenu.anchorPoint = ccp(0, 0);
    mainMenu.position = ccp(0, 0);
    [self addChild:mainMenu];
    }
    else {
        
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        
        if (cId > 989 && cId < 999) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            if (activeLanguage == 1) {
                [alert setTitle:@"احذر"]; 
                [alert setMessage:@" البيانات لا تكفي في ملفاتك"];  
            }
            else {
                [alert setTitle:@"WARNING"]; 
                [alert setMessage:@"Data NOT sufficient in your folder"];
            }

            [alert setDelegate:self];
            [alert addButtonWithTitle:@"OK"];
            [alert setTag:2];
            [alert show];
            [alert release];
        }
        else 
            if (cId == 99999 && (![[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"] == 570)) {
                UIAlertView *alert = [[UIAlertView alloc] init];
                if (activeLanguage == 1) {
                    [alert setTitle:@"حذرآسف"]; 
                    [alert setMessage:@" قاعدة الدرس الحالية لا تدعم هذه اللعبة"];  
                }
                else {
                    [alert setTitle:@"WARNING"]; 
                    [alert setMessage:@"SORRY! Current Selected Grammar Rule doesnot support this game"];
                }
                [alert setDelegate:self];
                [alert addButtonWithTitle:@"OK"];
                [alert setTag:2];
                [alert show];
                [alert release];
        }
        
        else 
        
        {
            UIAlertView *alert = [[UIAlertView alloc] init];
            if (activeLanguage == 1) {
                [alert setTitle:@"احذر "]; 
                [alert setMessage:@" البيانات لا تكفي"];  
            }
            else {
                [alert setTitle:@"WARNING"]; 
                [alert setMessage:@"SORRY! Data Not Sufficient"];
            }

            [alert setDelegate:self];
            [alert addButtonWithTitle:@"OK"];
            [alert setTag:2];
            [alert show];
            [alert release];
        }

        
    }
    
    
}

-(void) sessionCompletionCheck{
   
    UIAlertView *sessionAlert;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];    
    if (activeLanguage == 2) {
        sessionAlert = [[UIAlertView alloc] initWithTitle:@"Good Job" message:@"You are doing Good!" delegate:self cancelButtonTitle:@"Continue Playing" otherButtonTitles:@"Exit", nil];
    }
    else {
        sessionAlert = [[UIAlertView alloc] initWithTitle:@"عمل جيد " message:@"أنت تقوم بعمل جيد , أستمر" delegate:self cancelButtonTitle:@"الاستمرار في اللعب " otherButtonTitles:@"خروج", nil];
    }
    
    [sessionAlert setTag:1];
    [sessionAlert show];
    [sessionAlert release];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
    
    if(alertView.tag == 2)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        }
        [[CCDirector sharedDirector] end];
        [[CCDirector sharedDirector].openGLView removeFromSuperview];
    }
    else {
        
        if (buttonIndex == 0)
        {
            sessionCounter = 0;
            [self NextWord];
        }
        if (buttonIndex == 1)
        {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            }
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].openGLView removeFromSuperview];
        }        
    }     
}
-(void) NextWord{

    if (sessionCounter == 10) {
        sessionCounter = 0;
        [self sessionCompletionCheck];
        return;
    }
    sessionCounter = sessionCounter + 1;
    
    // if correct, Go ahead to next Word    
    currentWord  = currentWord + 1;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSString *wordSaver = [NSString stringWithFormat:@"writeItwordForClassification%dLanguage%d%@",cId,activeLanguage,ActiveGrade];
    [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self removeChild:mainMenu cleanup:YES];
    [self showUpMenu];
    
    [self clearCanvas];
    if (myDrawingCanvas) {
    [myDrawingCanvas removeFromParentAndCleanup:YES];        
    }

    myDrawingCanvas = [LineDrawer node];
    [self addChild:myDrawingCanvas];
    
    
    int currentColor = [[NSUserDefaults standardUserDefaults] integerForKey:@"pencilColor"];
    CCMenuItemImage* btn = blackBtn;
    if (currentColor == 0) {
        btn = blackBtn;
    }
    if (currentColor == 1) {
        btn = redBtn;
    }
    if (currentColor == 2) {
        btn = greenBtn;
    }
    if (currentColor == 3) {
        btn = blueBtn;
    }
    [self changeColor:btn];
}

-(void) returnPrevWord{
//    if (currentWord) {
//        currentWord  = currentWord - 1;
//        NSString *wordSaver = [NSString stringWithFormat:@"wordForClassification%d",cId];
//        [[NSUserDefaults standardUserDefaults] setInteger:currentWord forKey:wordSaver];
//        [self removeChild:mainMenu cleanup:YES];
//        [self showUpMenu];
//        
//        [self clearCanvas];
//        myDrawingCanvas = [LineDrawer node];
//        [self addChild:myDrawingCanvas];
//        
//    }

    [self clearCanvas];
    
}

-(void) hearWord{
    
//    NSString* soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
//    NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"];
//    NSString *squishPath = [DocumentDirectory playSound:soundName];
    
////    if (cId > 989) 
////    { // for My home
//////        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//////        NSString* documentsDirectory = [paths lastObject];
//////        NSString* soundPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.caf",soundName]];
////        
////        if (soundPath == nil || !soundPath) {
////            
////            NSLog(@"Sound NOt found");
////            NSString *soundNameNil = @"EmptySound";
////            NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundNameNil ofType:@"mp3"];
////            NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
////            SystemSoundID squishSoundID;
////            AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
////            AudioServicesPlaySystemSound(squishSoundID);
////            
////            return;
////        }
//        
//        
//        
////        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
////        SystemSoundID squishSoundID;
////        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
////        AudioServicesPlaySystemSound(squishSoundID);
////        
////        return;
//        
////    }
//
//    
//    
//    if (squishPath == nil || !squishPath) {
//        NSLog(@"Sound NOt found");
//        NSString *soundNameNil = @"EmptySound";
//        NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundNameNil ofType:@"mp3"];
//        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
//        SystemSoundID squishSoundID;
//        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
//        AudioServicesPlaySystemSound(squishSoundID);
//        
//        return;
//    }
//    NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
//    SystemSoundID squishSoundID;
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
//    AudioServicesPlaySystemSound(squishSoundID);
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }  

    
    NSString* soundName = [tempContentDic objectForKey:[NSString stringWithFormat:@"voiceId%d",currentWord]];
    //    NSString *squishPath = [[NSBundle mainBundle] pathForResource:soundName ofType:@"mp3"];
    NSString *squishPath = [DocumentDirectory playSound:soundName];

    NSURL *soundFileURL = [NSURL fileURLWithPath:squishPath];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;    
    [player play];
    
    
}
-(void) loadDatabase{
        
    MrPDatabase *database = [[MrPDatabase alloc] init];
    
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    //    int LanguageSupport = [[NSUserDefaults standardUserDefaults] integerForKey:@"LanguageSupport"];            
    cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    
    int position = 0;
    
    // cId 99999 means its myskillscamp, otherwise its simple classification
    if (cId == 99999) {
        
        
        int gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
        int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];
        
        int position = 0;
        
        if (activeLanguage == 2) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {  
                if (info.isSentence == 1) {
                    NSString *sentence = info.englishWord;
                    NSArray *componentsSeparatedByWhiteSpace = [sentence componentsSeparatedByString:@" "];
                    if([componentsSeparatedByWhiteSpace count] < 3){
                        NSLog(@"Less then 3 spaces");
                        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                        [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                           position++;
                    }
                    
                }
                else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
                position++;
                }
                
            } 
        }
        
        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:gId andpId:pId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {  
                if (info.isSentence == 1) {
                    NSString *sentence = info.arabicWord;
                    NSArray *componentsSeparatedByWhiteSpace = [sentence componentsSeparatedByString:@" "];
                    if([componentsSeparatedByWhiteSpace count] < 3){
                        NSLog(@"Less then 3 spaces");
                        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                        [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                                                position++;
                    }
                }
                else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
                position++;
                }
                
            }          
        }
    }
    
    
    // if goes in the loop below, it means its simple classification from city map
    else 
    {        
        if (activeLanguage == 2 || cId > 989) {
            NSArray *MrPContentArray = [database EnglishWordDatabaseArray:cId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                if (info.isSentence == 1) {
                    NSString *sentence = info.englishWord;
                    NSArray *componentsSeparatedByWhiteSpace = [sentence componentsSeparatedByString:@" "];
                    if([componentsSeparatedByWhiteSpace count] < 3){
                        NSLog(@"Less then 3 spaces");
                        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                        [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                        position++;
                    }
                }
                else {
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.englishWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];          
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
                position++;
                }
                
            }
            NSLog(@"Dictationary at Exit is %@",tempContentDic);
        }
        
        else {
            
            NSArray *MrPContentArray = [database ArabicWordDatabaseArray:cId];
            
            tempContentDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArray count]];
            for (MrPContentInfo *info in MrPContentArray) {
                if (info.isSentence == 1) {
                    NSString *sentence = info.arabicWord;
                    NSArray *componentsSeparatedByWhiteSpace = [sentence componentsSeparatedByString:@" "];
                    if([componentsSeparatedByWhiteSpace count] < 3){
                        NSLog(@"Less then 3 spaces");
                        [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                        [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
                        [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];
                        [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];
                                                position++;
                        
                    }
                }
                else {
                
                [tempContentDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
                [tempContentDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]]; // keeping it english word so in class, our convention remain same
                [tempContentDic setObject:[NSNumber numberWithInt:info.imageId] forKey:[NSString stringWithFormat:@"imageId%d",position]];          
                [tempContentDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
                position++;
                }
            }
        }
    }
    
    if ([tempContentDic count] == 0) {
        
    }
    
    [database release];
    database = Nil;
    
}


-(void) DoNothing{
}

- (void)HelpSound:(id)sender {
    // 159,56
    [[NSUserDefaults standardUserDefaults] setObject:@"GH019" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    CCMenuItem *helpBtnEnglish = [CCMenuItemImage 
                                  itemFromNormalImage:@"HelpEnglish.png" selectedImage:@"HelpEnglish.png" 
                                  target:self selector:@selector(playHelpSoundEng)];
    
    helpBtnEnglish.position = ccp(901, 640);
    
    CCMenuItem *helpBtnArabic = [CCMenuItemImage 
                                 itemFromNormalImage:@"HelpArabic.png" selectedImage:@"HelpArabic.png" 
                                 target:self selector:@selector(playHelpSoundAra)];
    
    helpBtnArabic.position = ccp(901, 585);
    
    helpMenu = [CCMenu menuWithItems:helpBtnEnglish,helpBtnArabic, nil];
    helpMenu.anchorPoint = ccp(0, 0);
    helpMenu.position = ccp(0, 0);
    [self addChild:helpMenu];
    
    
} 

-(void) playHelpSoundEng{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayEnglishHelpSound:nil];
//    [helpSoundVC release];
//    helpSoundVC = Nil;
    
}
-(void) playHelpSoundAra{
    HelpSoundViewController *helpSoundVC = [[HelpSoundViewController alloc] init];
    [helpSoundVC PlayArabicHelpSound:nil];
//    [helpSoundVC release];
//    helpSoundVC = Nil;
    
}

- (void)ccTouchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{    
    if(helpMenu)
    {
        [helpMenu removeFromParentAndCleanup:YES];
        helpMenu = nil;
    }
    
}




@end
