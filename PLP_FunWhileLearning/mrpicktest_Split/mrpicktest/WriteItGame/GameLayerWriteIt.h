//
//  HelloWorldLayer.h
//  FunLearning
//
//  Created by Muhammad Mosib on 7/22/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "cocos2d.h"
#import "LevelTwo.h"
//#import "AppDelegate.h"

@interface GameLayerWriteIt : CCLayer
{

    LevelTwo* levelTwoLayer;
    int stage;
    
    CCLabelTTF *levelWord;
    
    NSTimer *soundCompletionTimer;
    CCMenu *levelWordMenu;
    CCMenu *mainMenu;


    
}

@property int stage;

+ (GameLayerWriteIt*) sharedLayer;
//+ (void) setNavController:(AppController*) navCon;
//+ (AppController*) getNavController;

-(void) startGame:(int) pStageNumber;

+ (CCScene *) scene;
-(void) removeWriteItScene;

@end
