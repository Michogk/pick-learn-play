//
//  HelloWorldLayer.h
//  FunLearning
//
//  Created by Muhammad Mosib on 7/22/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "cocos2d.h"
#import "LevelOne.h"
//#import "AppDelegate.h"

@interface GameLayerFillIt : CCLayer<UITextFieldDelegate>
{
    LevelOne* levelOneLayer;
    UITextField *myText;
    

 
    
}

@property int stage;

+ (GameLayerFillIt*) sharedLayer;
-(void) startGame:(int) pStageNumber;
+ (CCScene *) scene;
-(void) removeFillItScene;
-(NSString *) getUserEnteredText;
-(void ) setUserEnteredText;
@end
