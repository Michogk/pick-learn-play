//
//  GirlCharacterSelectionViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 8/25/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GirlCharacterSelectionViewController : UIViewController<UIActionSheetDelegate>
@property (retain, nonatomic) IBOutlet UIButton *btnBasketGirl;
- (IBAction)btnCharacterBasketBallPressed:(id)sender;
- (IBAction)btnCharacterDoctorPressed:(id)sender;
- (IBAction)btnCharacterSimpleGirlPressed:(id)sender;
- (IBAction)btnCharacterChefPressed:(id)sender;
- (IBAction)btnCharacterFootballPressed:(id)sender;
- (IBAction)btnCharacterPolicePressed:(id)sender;
- (IBAction)btnCharacterArabPressed:(id)sender;

@property (retain, nonatomic) IBOutlet UIImageView *imgViewSimpleGirl;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewChef;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewFootball;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewFiregirl;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewDoctor;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewPolice;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBasketBall;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewArab;

- (IBAction)btnCharacterFiremanPressed:(id)sender;


@end
