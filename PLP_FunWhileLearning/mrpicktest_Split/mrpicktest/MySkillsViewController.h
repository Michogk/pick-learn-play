//
//  MySkillsViewController.h
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/24/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface MySkillsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {

    
    
    NSArray *_MySkillsGrammerRules;
    NSMutableDictionary *_principleIdForSelectedCellArr;
    UITableViewCell *principleCell;
    
    NSMutableDictionary *_tempGrammarContentDic;
    NSMutableDictionary *_tempPrinciplesContentDic;

    NSMutableDictionary *_tempLearningGrammarDic;
    NSMutableDictionary *_tempPrinciplesDic;
        BOOL showMovie;
    
    UITableViewCell *_grammarCell;
    int position;
    NSMutableArray* principlesIds;
    UILabel *_currentGrammar;
    UILabel *_currentPrinciple;
    
    NSString *_currentGrammarRule;
    NSString *_currentPrincipleRule;
    NSArray *MrPGrammarArray;
    
}

+(MySkillsViewController *) skillViewController;
- (IBAction)MySkillsPlayground:(id)sender;
- (IBAction)gotoSkillsCamp:(id)sender;
- (IBAction)gotoMainMenu:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *btnSmallSkills;

@property (retain, nonatomic) IBOutlet UIImageView *imgViewSkillsCampBackGround;
@property (retain, nonatomic) IBOutlet UITableView *PrincipleTable;
@property (retain, nonatomic) IBOutlet UITableView *GrammarTable;

@property (nonatomic, retain) NSString *currentGrammarRule;
@property (nonatomic, retain) NSString *currentPrincipleRule;

@property (nonatomic, retain) NSArray *downloadedPackArr;
- (IBAction)btnSmallSkillsPressed:(id)sender;
- (IBAction)btnCompleteSkillsPressed:(id)sender;

- (IBAction)HelpSound:(id)sender;
- (IBAction)helpURL:(id)sender;

@end

