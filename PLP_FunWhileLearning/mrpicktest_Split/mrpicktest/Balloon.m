//
//  baloon.m
//  KidsBubbleLearner
//
//  Created by Muhammad Mosib Asad on 6/24/12.
//  Copyright 2012 GameView Studios. All rights reserved.
//

#import "Balloon.h"
#import "GameLayer.h"
#import "Common.h"
#define FIRST_BALOON 50
#define SECOND_BALOON 60
#define THIRD_BALOON 70

@implementation Balloon

@synthesize isHavingCorrectWord;

+ (id) balloonWithType:(BalloonTypes) balloonType IsCorrect: (BOOL) isCorrect Word: (NSString*) word{

	return [[self alloc] initWithType:balloonType IsCorrect:isCorrect Word:word];
}

- (id) initWithType:(BalloonTypes) balloonType IsCorrect: (BOOL) isCorrect Word: (NSString*) word;
{	
	NSString* fileName = [self getFileForType:balloonType];
    
	if ( self = [super initFromNormalImage:fileName selectedImage:fileName disabledImage:fileName 
                               target:self selector:@selector(balloonTapped:)] )
	{
        
        type = balloonType;
        isHavingCorrectWord = isCorrect;
        myWord = word;
        self.scale = 0.55f;
   
        
//        myWordLabel = [CCLabelTTF labelWithString:word fontName:@"Marker Felt" fontSize:60];
        if (word) {
            myWordLabel = [CCLabelTTF labelWithString:word fontName:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]].fontName fontSize:35];
            myWordLabel.color = ccc3(0,0,0);
            
            float labelX = [self contentSize].width/2;
            float labelY = [self contentSize].height/2;
            
            myWordLabel.position = ccp(labelX, labelY);
            [self addChild:myWordLabel];

       
        }
            [self schedule:@selector(updatePosition:) interval:0.01f];           
        //lblCorrectVerb.position =  ccp( size.width/2 , size.height/2);
        
           speed = 2;
    }
	
	return self;

}

- (void) stop
{
 
    [self unschedule:@selector(updatePosition:)]; 
}

- (void) updatePosition: (ccTime) delta
{

//    CGSize size = [[CCDirector sharedDirector] winSize];

    float bottom = -500;
    CGPoint posSelf = self.position;
    
    posSelf.y -= speed;
    if (posSelf.y < bottom) {
        [[GameLayer sharedLayer] updateGuesSentence];
    }
    self.position = ccp(posSelf.x, posSelf.y);
}

- (void) balloonTapped: (id) ballon
{
    CGPoint pos = [self position];
    pos.y = pos.y + 150;
        NSLog(@"position x is %f and position y is %f in baloons",pos.x,pos.y);
    if (isHavingCorrectWord) {
        [[GameLayer sharedLayer] balloonTapped:pos IsRight:FALSE];
        BOOL correctBalTapped = [[GameLayer sharedLayer] correctBallonBusted];
        if (!correctBalTapped) {
            self.visible = FALSE;
        }


    }
    else {
        [[GameLayer sharedLayer] balloonTapped:pos IsRight:TRUE];
        
        BOOL wrongBaloonTapped = [[GameLayer sharedLayer] wrongBalloonBusted];
        if (!wrongBaloonTapped) {
            self.visible = FALSE;
        }
//        self.visible = FALSE;
    }
}
- (NSString*) getFileForType: (BalloonTypes) balloonType
{

    wrongBaloonBurst = false;
    NSString* fileName = @"";

    switch (balloonType)
    {
        case BlueType:
            fileName = @"balloon1.png";
            break;
        case GreenType:
            fileName = @"balloon2.png";
            break;
        case OrangeType:
            fileName = @"balloon3.png";
            break;
        case PurpleType:
            fileName = @"balloon4.png";
            break;            
        case RedType:
            fileName = @"balloon5.png";
            break;  
        
        case YellowType:
            fileName = @"balloon5.png";
        
        default:
            [NSException exceptionWithName:@"balloon Exception" reason:@"unhandled balloon type" userInfo:nil];
    }
    return fileName;
}

- (void) dealloc
{
	[super dealloc];
}

@end
