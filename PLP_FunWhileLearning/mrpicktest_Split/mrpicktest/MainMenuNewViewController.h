//
//  MainMenuNewViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 15/07/2014.
//  Copyright (c) 2014 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityMapViewController.h"
#import "MyCampController.h"
#import "MyPlayGroundViewController.h"
#import "MyHomeViewController.h"
#import <AudioToolbox/AudioToolbox.h>


@class CustomMoviePlayerViewController;

@interface MainMenuNewViewController : UIViewController<AVAudioPlayerDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate> {
    
    IBOutlet UIButton *CityMap;
    IBOutlet UIButton *SetupBtn;
    
    CityMapViewController *CityMapView;
    
    MyCampController *MyCampView;
    MyPlayGroundViewController *MyPlayGroundView;
    MyHomeViewController *MyHomeView;
    
    AVAudioPlayer *myCampPlayer;
    CustomMoviePlayerViewController *moviePlayer;
    BOOL repeatLaunch;
    int _kNumImages;
    NSMutableArray *principleTagsArr;
    NSMutableArray *grammarsTagArray;
    NSMutableDictionary *_arabicWordDic;
    NSMutableDictionary *principlesArray;
    NSMutableDictionary *grammarsArray;
    NSMutableArray *classificationIdArray;
    
    NSMutableArray *grammarIdArray;
    NSMutableArray *principleIdArray;
    
    NSArray *MrPContentArray;
    
    int currentSelectedPrincipleIndex;
    
    NSMutableArray *principleIdSelectedArray;
    NSMutableArray *grammarIdSelectedArray;
    IBOutlet UIButton *btnPlayHelpSpund;
    
    NSString *_dbname;
    NSString *_target_name;
    NSMutableArray *grammerIconClipArray;

    
    IBOutlet UIImageView *imgView5Feather;
    IBOutlet UIImageView *imgView4Feather;
    IBOutlet UIImageView *imgView3Feather;
    IBOutlet UIImageView *imgView2Feather;
    IBOutlet UIImageView *imgView1Feather;
}

@property(nonatomic,retain) IBOutlet UIView *principleSelectionViewBackground;
@property(nonatomic,retain) IBOutlet UIView *grammerSelectionViewBackground;
- (IBAction)actionPlayHelpSound:(id)sender;

@property(nonatomic,retain) CityMapViewController *CityMapView;
- (IBAction)playVideo:(id)sender;

@property(nonatomic,retain ) MyCampController *MyCampView;
- (IBAction)exitMainMenu:(id)sender;
- (IBAction)introButtonPressed:(id)sender;


@end
