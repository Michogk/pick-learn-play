//
//  Downloader.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 11/29/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "Downloader.h"
#import "Reachability.h"
#import "CAURLDownload.h"
#import "ZipArchive.h"



@implementation Downloader



-(void) checkForInternetConnection
{
    // check for internet connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [[Reachability reachabilityForInternetConnection] retain];
    [internetReachable startNotifier];
    
    // check if a pathway to a random host exists
    hostReachable = [[Reachability reachabilityWithHostName: @"www.apple.com"] retain];
    [hostReachable startNotifier];

}

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            internetActiveStatus = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            internetActiveStatus = YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            internetActiveStatus = YES;
            
            break;
        }
            internetActiveStatus = YES;
    }
    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)
    {
        case NotReachable:
        {
            NSLog(@"A gateway to the host server is down.");
            hostActiveStatus = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"A gateway to the host server is working via WIFI.");
            hostActiveStatus = YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"A gateway to the host server is working via WWAN.");
            hostActiveStatus = YES;
            
            break;
        }
            hostActiveStatus = YES;
    }
    [self dismissNotification];
    
    
}

-(void) dismissNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (BOOL)connected 
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];  
    NetworkStatus networkStatus = [reachability currentReachabilityStatus]; 
    return !(networkStatus == NotReachable);
}

-(void) fetchData
{    
    NSString* gradeToBeDownloaded = [[NSUserDefaults standardUserDefaults] objectForKey:@"gradeIsSelected"];
    if ([gradeToBeDownloaded isEqualToString:@"free"]) {
        gradeToBeDownloaded = @"FREE";
    }
    
    
    NSString *URL = [NSString stringWithFormat:@"http://www.mrpickit.net/packages/%@.zip",gradeToBeDownloaded];
    NSLog(@"URL is %@",URL);
    
    
    [CAURLDownload downloadURL:[NSURL URLWithString:URL]
                        target:self
                      selector:@selector(downloadFinished:conneciton:)
                  failSelector:@selector(downloadFailed:)
                      userInfo:nil];
}

- (void)downloadFinished:(NSData *)recievedData conneciton:(CAURLDownload *)connection {

    NSLog(@"Data Saved Successfully");
    
    NSString* gradeToBeDownloaded = [[NSUserDefaults standardUserDefaults] objectForKey:@"gradeIsSelected"];
    
    NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];          
    NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"package.zip"];
    [recievedData writeToFile:filePath atomically:YES];
    NSURL *pathURL= [NSURL fileURLWithPath:filePath];
    [self addSkipBackupAttributeToItemAtURL:pathURL];
    

    NSString* password = @"";
    BOOL *copySkillsToItsBasics = FALSE; // this is meant to remove skills from the end of "gradeToBeDownloaded" so skills of any package are saved in same folder as package itself.
    if ([gradeToBeDownloaded isEqualToString:@"KG1"]) {
        password = @"carT@reAP?17";
    }
    if ([gradeToBeDownloaded isEqualToString:@"KG2"]) {
        password = @"opra88aulD=";
    }
    if ([gradeToBeDownloaded isEqualToString:@"G1"]) {
        password = @"Bis84AwN+ais";
    }
    if ([gradeToBeDownloaded isEqualToString:@"G2"]) {
        password = @"carT@reAP?17";
    }
    if ([gradeToBeDownloaded isEqualToString:@"G3"]) {
        password = @"Bis84AwN+ais";
    }
    if ([gradeToBeDownloaded isEqualToString:@"G4"]) {
        password = @"Bis84AwN+ais";
    }
    if ([gradeToBeDownloaded isEqualToString:@"G1SKILLS"]) {
        password = @"dud^siB~cLAds97";
    }
    if ([gradeToBeDownloaded isEqualToString:@"G2SKILLS"]) {
        password = @"fOyS84kiSt?";
    }
    if ([gradeToBeDownloaded isEqualToString:@"G3SKILLS"]) {
        password = @"dud^siB~cLAds97";
    }
    if ([gradeToBeDownloaded isEqualToString:@"G4SKILLS"]) {
        password = @"YEGG16Fain:";
    }
    if ([gradeToBeDownloaded isEqualToString:@"free"]) {
        password = @"carT@reAP?10";
    }
    
    NSString *outputUnzipped = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"",gradeToBeDownloaded]];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive UnzipOpenFile:filePath Password:password];  

    
//    [zipArchive UnzipFileTo:filePath overWrite:YES];
    if( [zipArchive UnzipFileTo:outputUnzipped overWrite:YES] != NO ) {
        NSLog(@"Unziping Successfull");
        NSURL *pathURL= [NSURL fileURLWithPath:outputUnzipped];
        [self addSkipBackupAttributeToItemAtURL:pathURL];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath: filePath error:NULL];        
//        long folderSize = [self folderSize:outputUnzipped];
         [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"downloadingStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // Copy if its skills.. currently NOT copying folder.
        if (copySkillsToItsBasics) {
            // Current Folder == DocuemntDirectory/gradeToBeDownloaded+Skills
            // Destination Folder == DocuemntDirectory/gradeToBeDownloaded
            
        }     
            [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"downloadingStatus"];

    }
    else {
        NSLog(@"Unzipping Failed");
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"downloadingStatus"];
    }
    [zipArchive UnzipCloseFile];
    [zipArchive release];
    
    BOOL combo = [[NSUserDefaults standardUserDefaults] boolForKey:@"COMBO"];
    if ( combo == YES) {
        NSLog(@"Yes, its Combo");
        // Setting it NO so just in case we downloaded any Combo, the package shall be NO now for future downloads!
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"COMBO"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PHOTOFEATURE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        /* */
        
  
       
         NSString* packageRecentlyDownloaded = [[NSUserDefaults standardUserDefaults] objectForKey:@"gradeIsSelected"];
        if ([packageRecentlyDownloaded isEqualToString:@"KG1"] || [packageRecentlyDownloaded isEqualToString:@"KG2"]) {
                    NSLog(@"Before notification Kg1 Kg2 Grade Completed, going to store to show success Message");        
            [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseCompletedCallBack" object:self]];
                    NSLog(@"After notification Kg1 Kg2 Grade Completed, going to store to show success Message");
            return;
        }
                
            NSString* gradesPurchased = [[NSUserDefaults standardUserDefaults] objectForKey:@"gradesPurchased"];
            
            NSArray * componentsArr = [[gradesPurchased componentsSeparatedByString: @" "] retain];
            BOOL packageHasBeenDownloadedBefore = FALSE;
            for (int i = 0; i < componentsArr.count; i++) {
                if ([[componentsArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@",packageRecentlyDownloaded]]) {
                    packageHasBeenDownloadedBefore = TRUE;
                }
            }
            [componentsArr release];
            componentsArr = Nil;
            if (!packageHasBeenDownloadedBefore) {
                NSString* package = packageRecentlyDownloaded;
                NSString* updatedPurchasedRecord = [gradesPurchased stringByAppendingFormat:@" %@",package];
                [[NSUserDefaults standardUserDefaults] setValue:updatedPurchasedRecord forKey:@"gradesPurchased"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
                
        NSString* packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"AlreadyDownloadedPackage"];
        if (!packagesDownloadedBefore || packagesDownloadedBefore == Nil || packagesDownloadedBefore == @"<nil>") {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"AlreadyDownloadedPackage"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            packagesDownloadedBefore = [[NSUserDefaults standardUserDefaults] objectForKey:@"AlreadyDownloadedPackage"];
        }
        
        NSString* updatedPackageDownloadedStatus = [packagesDownloadedBefore stringByAppendingFormat:@" %@",packageRecentlyDownloaded];
        [[NSUserDefaults standardUserDefaults] setValue:updatedPackageDownloadedStatus forKey:@"AlreadyDownloadedPackage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    
             
         /**/
        NSString* gradeRecentlyDownloaded = [[NSUserDefaults standardUserDefaults] objectForKey:@"gradeIsSelected"];
        [[NSUserDefaults standardUserDefaults] setObject:gradeRecentlyDownloaded forKey:@"activeGrade"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@SKILLS",gradeRecentlyDownloaded] forKey:@"gradeIsSelected"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"Downloading Simple Grade Completed, Starting Skills downloading to continue to next step");
        
  
        [self fetchData];
    }
    else{
        NSLog(@"Downloading Completed, Going to Store to show success Message");
        [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseCompletedCallBack" object:self]];
    }


    
    //Etc.
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] fileAttributesAtPath:[folderPath stringByAppendingPathComponent:fileName] traverseLink:YES];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}

- (void)downloadFailed:(CAURLDownload *)connection {
    NSLog(@"Download Failed!");
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"purchaseCompletedCallBack" object:self]];
    
    [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"downloadingStatus"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
