//
//  CostumesViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 8/29/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "CostumesViewController.h"

@interface CostumesViewController ()

@end

@implementation CostumesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ChefBoy"]) {
        [_btnChefBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnChefBoy setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnChefBoy setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnChefBoy setBackgroundColor:[UIColor redColor]];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"BasketBallBoy"]) {
        [_btnBasketBallBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnBasketBallBoy setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnBasketBallBoy setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnBasketBallBoy setBackgroundColor:[UIColor redColor]];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"BasketBallGirl"]) {
        [_btnBasketBallGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnBasketBallGirl setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnBasketBallGirl setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnBasketBallGirl setBackgroundColor:[UIColor redColor]];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ArabBoy"]) {
        [_btnArabBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnArabBoy setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnArabBoy setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnArabBoy setBackgroundColor:[UIColor redColor]];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ArabGirl"]) {
        [_btnArabGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnArabGirl setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnArabGirl setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnArabGirl setBackgroundColor:[UIColor redColor]];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FiremanBoy"]) {
        [_btnFireManBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnFireManBoy setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnFireManBoy setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnFireManBoy setBackgroundColor:[UIColor redColor]];
    }
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FiremanGirl"]) {
        [_btnFireGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnFireGirl setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnFireGirl setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnFireGirl setBackgroundColor:[UIColor redColor]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FootBallBoy"]) {
        [_btnFootballBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnFootballBoy setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnFootballBoy setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnFootballBoy setBackgroundColor:[UIColor redColor]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FootBallGirl"]) {
        [_btnFootBallGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnFootBallGirl setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnFootBallGirl setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnFootBallGirl setBackgroundColor:[UIColor redColor]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ChefGirl"]) {
        [_btnChefGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnChefGirl setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnChefGirl setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnChefGirl setBackgroundColor:[UIColor redColor]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"DoctorBoy"]) {
        [_btnDoctorBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnDoctorBoy setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnDoctorBoy setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnDoctorBoy setBackgroundColor:[UIColor redColor]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"LadyDoctor"]) {
        [_btnLadyDoctor setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnLadyDoctor setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnLadyDoctor setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnLadyDoctor setBackgroundColor:[UIColor redColor]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PoliceMan"]) {
        [_btnPoliceMan setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnPoliceMan setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnPoliceMan setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnPoliceMan setBackgroundColor:[UIColor redColor]];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PoliceWoman"]) {
        [_btnPoliceWoman setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnPoliceWoman setBackgroundColor:[UIColor greenColor]];
    }
    else{
        [_btnPoliceWoman setTitle:@"Activate" forState:UIControlStateNormal];
        [_btnPoliceWoman setBackgroundColor:[UIColor redColor]];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BtnChefBoyPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ChefBoy"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ChefBoy"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:7 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnChefBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnChefBoy setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnChefGirlPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ChefGirl"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ChefGirl"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:8 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnChefGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnChefGirl setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnFootballBoyPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FootBallBoy"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FootBallBoy"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:11 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnFootballBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnFootballBoy setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnFootballGirlPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FootBallGirl"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FootBallGirl"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:12 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnFootBallGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnFootBallGirl setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnFireManPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FiremanBoy"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FiremanBoy"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:9 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnFireManBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnFireManBoy setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnFireGirlPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FiremanGirl"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FiremanGirl"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:10 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnFireGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnFireGirl setBackgroundColor:[UIColor greenColor]];
    }
}
- (void)dealloc {
    [_btnChefBoy release];
    [_btnChefGirl release];
    [_btnFootBallGirl release];
    [_btnFootballBoy release];
    [_btnFireManBoy release];
    [_btnFireGirl release];
    [_btnDoctorBoy release];
    [_btnLadyDoctor release];
    [_btnPoliceMan release];
    [_btnPoliceWoman release];
    [_btnBasketBallBoy release];
    [_btnBasketBallGirl release];
    [_btnArabBoy release];
    [_btnArabGirl release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setBtnChefBoy:nil];
    [self setBtnChefGirl:nil];
    [self setBtnFootBallGirl:nil];
    [self setBtnFootballBoy:nil];
    [self setBtnFireManBoy:nil];
    [self setBtnFireGirl:nil];
    [self setBtnDoctorBoy:nil];
    [self setBtnLadyDoctor:nil];
    [self setBtnPoliceMan:nil];
    [self setBtnPoliceWoman:nil];
    [self setBtnBasketBallBoy:nil];
    [self setBtnBasketBallGirl:nil];
    [self setBtnArabBoy:nil];
    [self setBtnArabGirl:nil];
    [super viewDidUnload];
}
- (IBAction)btnLadyDoctorPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"DoctorBoy"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DoctorBoy"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:6 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnLadyDoctor setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnLadyDoctor setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnDoctorPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"LadyDoctor"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LadyDoctor"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:5 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnDoctorBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnDoctorBoy setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)BtnPoliceManPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PoliceMan"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PoliceMan"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:15 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnPoliceMan setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnPoliceMan setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnPoliceGirlPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PoliceWoman"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PoliceWoman"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:16 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnPoliceWoman setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnPoliceWoman setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnBasketBallBoyPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"BasketBallBoy"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"BasketBallBoy"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnBasketBallBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnBasketBallBoy setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnBasketBallGirlPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"BasketBallGirl"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"BasketBallGirl"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnBasketBallGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnBasketBallGirl setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnArabBoyPressed:(id)sender {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ArabBoy"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ArabBoy"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:13 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnArabBoy setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnArabBoy setBackgroundColor:[UIColor greenColor]];
    }
}

- (IBAction)btnArabGirlPressed:(id)sender {
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ArabGirl"]) {
        return;
    }
    
    if ([self purchaseCostume]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ArabGirl"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        [alert setTitle:@"Congratulations"];
        [alert setMessage:@"You have Purchase New Costume."];
        [alert addButtonWithTitle:@"Exit"];
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        
        [[NSUserDefaults standardUserDefaults] setInteger:14 forKey:@"gender"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [_btnArabGirl setTitle:@"Activated" forState:UIControlStateNormal];
        [_btnArabGirl setBackgroundColor:[UIColor greenColor]];        
    }
}

-(BOOL) purchaseCostume{
    
    float totalPoints = 0;
    for (int i = 1; i < 9; i ++) {
        NSString *PointsLevel1 = [NSString stringWithFormat:@"Game%dLevel%d",i,1];
        float level1Points = [[NSUserDefaults standardUserDefaults] integerForKey:PointsLevel1];
        
        NSString *PointsLevel2 = [NSString stringWithFormat:@"Game%dLevel%d",i,2];
        float level2Points = [[NSUserDefaults standardUserDefaults] integerForKey:PointsLevel2];
        
        totalPoints = totalPoints + level1Points  + level2Points;
    }
    
    
    int alreadyDownloadedRewardsCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"rewardsDownloadedCount"];
    totalPoints = totalPoints - (alreadyDownloadedRewardsCount * 150);
    NSLog(@" total points are %f",totalPoints);
    if (totalPoints < 150) {
        
        UIAlertView *alert = [[UIAlertView alloc] init];
        int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        if (activeLanguage == 2) {
            [alert setTitle:@"WARNING"];
            [alert setMessage:@"SORRY! You dont have sufficient points to Purchase this character"];
            [alert addButtonWithTitle:@"OK"];
        }
        else {
            [alert setTitle:@"اتحذير !"];
            [alert setMessage:@" لايوجد نقاط كافية لتحميل أية صور  "];
            [alert addButtonWithTitle:@"خروج"];
        }
        
        [alert setDelegate:self];
        [alert setTag:1];
        [alert show];
        [alert release];
        return FALSE;
    }
    
    else{
                
        int alreadyPurchasedCostumersCount = [[NSUserDefaults standardUserDefaults] integerForKey:@"CostumePurchasedCount"];
        if (!alreadyDownloadedRewardsCount) {
            alreadyDownloadedRewardsCount = 0;
        }
        
        [[NSUserDefaults standardUserDefaults] setInteger:alreadyPurchasedCostumersCount+1 forKey:@"CostumePurchasedCount"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return TRUE;

    }
    

}
@end
