//
//  MyPlayGroundViewController.m
//  mrpicktest
//
//  Created by EMRAN HAMDAN on 6/17/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MyPlayGroundViewController.h"
#import "GameLayer.h"
#import "GameLayerWriteIt.h"
#import "GameLayerFillIt.h"
#import "cocos2d.h"
#import "FixItViewController.h"
#import "MatchIt.h"
#import "PickItViewController.h"
#import "SpinItViewController.h"
#import "MoveItViewController.h"
#import "FootballViewController.h"
#import "HockeyViewController.h"
#import "BasketBallViewController.h"
//#import "CustomMoviePlayerViewController.h"
#import "MovieViewController.h"
#import "ExpressItViewController.h"
#import "TypeItViewController.h"
#import "WriteItViewController.h"
#import "ColorItViewController.h"
#import "Constants.h"

#define MATCH_IT 5
#define COLOR_IT 3

@implementation MyPlayGroundViewController  

@synthesize PlayGroundScoll;/* navController; */
@synthesize imgViewLeftArrow = _imgViewLeftArrow;
@synthesize imgViewRightArrow = _imgViewRightArrow;


const CGFloat pScrollObjHeight	= 400.0;
const CGFloat pScrollObjWidth	= 472.0;
const NSUInteger pNumImages		= 13;

static MyPlayGroundViewController* _playGroundShared;
static NSString *textFieldText;
static UITextField *levelEntryTextField;
//@synthesize arabicWordsArr = _arabicWordsArr;

+ (MyPlayGroundViewController*) sharedPlayGround
{
    return _playGroundShared;
}

- (void)layoutScrollImages
{
    UIImageView *view = nil;
    
    NSArray *subviews  = [PlayGroundScoll subviews];
    
    // reposition all image subviews in a horizontal serial fashion
    CGFloat curXLoc = -462;
    for (view in subviews)
    {
        //if ([view isKindOfClass:[UIImageView class]] && view.tag > 0)
        //if ([view isKindOfClass:[UIImageView class]] && view.tag > 0)
        {
            CGRect frame = view.frame;
            frame.origin = CGPointMake(curXLoc, 0);
            view.frame = frame;
            
            
            curXLoc += (pScrollObjWidth);
        }
    }
    
    // set the content size so it can be scrollable
    [PlayGroundScoll setContentSize:CGSizeMake((pNumImages * pScrollObjWidth), [PlayGroundScoll bounds].size.height)];
}

-(void) viewWillAppear:(BOOL)animated{

    NSLog(@"View appeared");
}

-(void) loadGame{
    
    NSLog(@"Load Games after movie is played");
    if (timerLoadGames) {
        [timerLoadGames invalidate];
        [timerLoadGames release];
        timerLoadGames = Nil;
    }
    
    BOOL multipleClassifications = false;
    BOOL onlyForExpressIt = false;
    BOOL onlyForWriteIT = FALSE;
    BOOL onlyMatchItExcluded = FALSE;
    
    NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    
    NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    if([objectClassification isKindOfClass:[NSArray class]] == YES)
    {
        if ([objectClassification count] > 1) {
            multipleClassifications = true;
        }
    }
    NSArray *objectPrincipleId = [[NSUserDefaults standardUserDefaults] objectForKey:PRINCIPLE_ID_ARRAY];
    if([objectPrincipleId isKindOfClass:[NSArray class]] == YES){
        if ([[objectPrincipleId objectAtIndex:0] isEqualToString:@"3020"]) {
            onlyForExpressIt = true;
        }
        if ([ActiveGrade isEqualToString:@"G2"]) {
            if ([[objectPrincipleId objectAtIndex:0] isEqualToString:@"3010"]) {
                onlyMatchItExcluded = true;
            }
        }
    
    }
    
    
    if ([ActiveGrade isEqualToString:@"KG1"]) {
    NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
    if([objGrammarId isKindOfClass:[NSArray class]] == YES){
        if ([objGrammarId count] == 1) {
            if ([[objGrammarId objectAtIndex:0] isEqualToString:@"2340"]) {
                onlyForWriteIT =  true;
            }
        }
    }
    }

    
#warning Attempt 1
    // Loading Game
    
//    CCGLView *glviews = (CCGLView *)[[CCDirector sharedDirector] view];
//    glviews.frame = CGRectMake(0, 0, 1024, 768);
//    
////    EAGLView *glviews = [EAGLView viewWithFrame:CGRectMake(0, 0, 1024,768)];
//    [self.view addSubview:glviews];
//
//
//    CCDirector *directors = [CCDirector sharedDirector];
////    [directors setOpenGLView:glviews];

    
    CCDirector *directors = [CCDirector sharedDirector];
    CCGLView *glviews = [CCGLView viewWithFrame:CGRectMake(0, 0, 1024, 768)
                                   pixelFormat:kEAGLColorFormatRGBA8
                                   depthFormat:0];
    glviews.opaque = NO;
    [directors setView:glviews];
    [self.view addSubview:glviews];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    switch (GameToBePlayed) {
        case 3:
            if (onlyForExpressIt || onlyForWriteIT) {
                [[CCDirector sharedDirector] end];
                [[CCDirector sharedDirector].view removeFromSuperview];
                if (glviews) {
                    [glviews removeFromSuperview];
                    glviews = Nil;
                    
                }
                return;
            }

            ColorItViewController *colorItVC = [sb instantiateViewControllerWithIdentifier:@"ColorItViewController"];
            colorItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:colorItVC animated:NO completion:NULL];
            break;

            NSLog(@"POP IT");
//            [directors runWithScene:[GameLayer scene]];

            
            break;
            
        case 8:
            NSLog(@"Fix it IT");
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }
            FixItViewController *FixItVC = [sb instantiateViewControllerWithIdentifier:@"FixItController"];
            FixItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:FixItVC animated:NO completion:NULL];
            break;
            
        case 11:
            if (onlyForExpressIt || onlyForWriteIT) {
                [[CCDirector sharedDirector] end];
                [[CCDirector sharedDirector].view removeFromSuperview];
                if (glviews) {
                    [glviews removeFromSuperview];
                    glviews = Nil;
                    
                }
                return;
            }
            NSLog(@"Type IT");
//            [directors runWithScene:[GameLayerFillIt scene]];
            TypeItViewController *typeItVC = [sb instantiateViewControllerWithIdentifier:@"TypeItViewController"];
            typeItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:typeItVC animated:NO completion:NULL];

            break;
            
        case 1:
            if (onlyForExpressIt) {
                [[CCDirector sharedDirector] end];
                [[CCDirector sharedDirector].view removeFromSuperview];
                if (glviews) {
                    [glviews removeFromSuperview];
                    glviews = Nil;
                    
                }
                return;
            }
            NSLog(@"Write IT");
            WriteItViewController *WriteItVC = [sb instantiateViewControllerWithIdentifier:@"WriteItViewController"];
            WriteItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:WriteItVC animated:NO completion:NULL];

//            [directors  runWithScene:[GameLayerWriteIt scene]];
            break;
            
        case 5:
            NSLog(@"MatchIt");
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            
//            if (multipleClassifications) {
//                UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot play this game with multiple locations" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
//                [selectionAlert show];
//
//                return;
//            }
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }
            
            if (onlyMatchItExcluded) {
                return;
            }
            MatchIt *matchItVC = [sb instantiateViewControllerWithIdentifier:@"MatchItIdentifer"];
            int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
            
            if (cId > 989 && cId < 999 && [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"] == FALSE && (![[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"]))
            {
                UIAlertView *alert = [[UIAlertView alloc] init];
                [alert setTitle:@"Wait!"];
                [alert setMessage:@"Please select any location from map and try again"];
                [alert setDelegate:self];
                [alert addButtonWithTitle:@"OK"];
                [alert setTag:200];
                [alert show];
                [alert release];
                break;
            }
            else {
                matchItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self presentViewController:matchItVC animated:NO completion:NULL];
                break;
            }
            
        case 6:
        {
            //Pick It actuall
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            if (multipleClassifications) {
                UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot play this game with multiple locations" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                [selectionAlert show];

                return;
            }
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }

    
            cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
            
            if ((cId > 989) && (cId < 999) && ([[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"] == FALSE) && (![[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"])) {
                UIAlertView *alert = [[UIAlertView alloc] init];
                [alert setTitle:@"SORRY"];
                [alert setMessage:@"You have not unlocked Photo Feature for your folders! Please unlock them and come back"];
                [alert addButtonWithTitle:@"OK"];
                [alert setDelegate:self];
                [alert setTag:1];
                [alert show];
                [alert release];
                
                break;
            }
            
            
            PickItViewController *pickItVC= [sb instantiateViewControllerWithIdentifier:@"footballWordsIdentifier"];
            pickItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:pickItVC animated:NO completion:NULL];
        }
            break;
            
            
        case 13:
            
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }

      
            UIAlertView *alert = [[UIAlertView alloc] init];
            int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
            
            if (activeLanguage == 2) {
                [alert setTitle:@"Check For Update"];
                [alert setMessage:@"More Games Coming Soon. Comeback Later!"];
                [alert addButtonWithTitle:@"Exit"];
            }
            else {
                [alert setTitle:@"االرجاء التأكد من التحديث "];
                [alert setMessage:@" العاب اخرى قريبا   "];
                [alert addButtonWithTitle:@"خروج"];
            }
            [alert setDelegate:self];
            [alert setTag:1];
            [alert show];
            [alert release];
            break;

            
            
        case 7:            
            NSLog(@"BasketBall");


            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }

            alert = [[UIAlertView alloc] init];
            
            cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
            //            BOOL photoFeature = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
            if ((cId > 989) && (cId < 999) && ([[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"] == FALSE) && (![[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"])) {
                alert = [[UIAlertView alloc] init];
                [alert setTitle:@"SORRY"];
                [alert setMessage:@"You have not unlocked Photo Feature for your folders! Please unlock them and come back"];
                [alert addButtonWithTitle:@"OK"];
                [alert setDelegate:self];
                [alert setTag:1];
                [alert show];
                [alert release];
                
                break;
            }
            
            
            BasketBallViewController *basketBallVC= [sb instantiateViewControllerWithIdentifier:@"basketBallIdentifier"];
            basketBallVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:basketBallVC animated:NO completion:NULL];
            
            break;
            
        case 12:
            
            NSLog(@"Express It");
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            
            if (onlyForWriteIT) {
                return;
            }

            
            alert = [[UIAlertView alloc] init];
          
            
            cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
            
 
            //            BOOL photoFeature = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
            if ((cId > 989) && (cId < 999) && ([[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"] == FALSE) && (![[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"])) {
                alert = [[UIAlertView alloc] init];
                [alert setTitle:@"SORRY"];
                [alert setMessage:@"You have not unlocked Photo Feature for your folders! Please unlock them and come back"];
                [alert addButtonWithTitle:@"OK"];
                [alert setDelegate:self];
                [alert setTag:1];
                [alert show];
                [alert release];
                
                break;
            }
            if (cId < 1600 && cId > 999) {
                ExpressItViewController *expressItVC= [sb instantiateViewControllerWithIdentifier:@"ExpressItIdentifier"];
                expressItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self presentViewController:expressItVC animated:NO completion:NULL];
                break;
            }
#warning New concept only
//            if (cId < 989 || cId == 99999) {
//                alert = [[UIAlertView alloc] init];
//                [alert setTitle:@"SORRY"];
//                [alert setMessage:@"You can only play Express it from My home"];
//                [alert addButtonWithTitle:@"OK"];
//                [alert setDelegate:self];
//                [alert setTag:1];
//                [alert show];
//                [alert release];
//                
//                break;
//            }
            
            ExpressItViewController *expressItVC= [sb instantiateViewControllerWithIdentifier:@"ExpressItIdentifier"];
            expressItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:expressItVC animated:NO completion:NULL];
            
            break;
            
        case 4:
            
            NSLog(@"Hockey");
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }

            alert = [[UIAlertView alloc] init];
            
            cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
            //            BOOL photoFeature = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
            if ((cId > 989) && (cId < 999) && ([[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"] == FALSE)&& (![[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"])) {
                alert = [[UIAlertView alloc] init];
                [alert setTitle:@"SORRY"];
                [alert setMessage:@"You have not unlocked Photo Feature for your folders! Please unlock them and come back"];
                [alert addButtonWithTitle:@"OK"];
                [alert setDelegate:self];
                [alert setTag:1];
                [alert show];
                [alert release];
                
                break;
            }
            
            
            HockeyViewController *hockeyVC= [sb instantiateViewControllerWithIdentifier:@"hockeyIdentifier"];
            hockeyVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:hockeyVC animated:NO completion:NULL];
            
            break;
            
        case 9:
            
            NSLog(@"FootBall it");
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }

            alert = [[UIAlertView alloc] init];

            cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
            //            BOOL photoFeature = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
//            if ((cId > 989) && (cId < 999) && ([[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"] == FALSE)) {
//                alert = [[UIAlertView alloc] init];
//                [alert setTitle:@"SORRY"];
//                [alert setMessage:@"You have not unlocked Photo Feature for your folders! Please unlock them and come back"];
//                [alert addButtonWithTitle:@"OK"];
//                [alert setDelegate:self];
//                [alert setTag:1];
//                [alert show];
//                [alert release];
//                
//                break;
//            }
            
            
            FootballViewController *FootballVC= [sb instantiateViewControllerWithIdentifier:@"footballIdentifier"];
            FootballVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:FootballVC animated:NO completion:NULL];
            
            break;
            
        case 2:
            NSLog(@"Spin it");
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }

            cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
//            BOOL photoFeature = [[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"];
            if ((cId > 989) && (cId < 999) && ([[NSUserDefaults standardUserDefaults] boolForKey:@"PHOTOFEATURE"] == FALSE)&& (![[NSUserDefaults standardUserDefaults] boolForKey:@"PINNEDDEVICE"])) {
                alert = [[UIAlertView alloc] init];
                [alert setTitle:@"SORRY"];
                [alert setMessage:@"You have not unlocked Photo Feature for your folders! Please unlock them and come back"];
                [alert addButtonWithTitle:@"OK"];
                [alert setDelegate:self];
                [alert setTag:1];
                [alert show];
                [alert release];
                
                break;
            }
            
            
            SpinItViewController *spinItVC= [sb instantiateViewControllerWithIdentifier:@"spinItIdentifier"];
            spinItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:spinItVC animated:NO completion:NULL];
            
            break;
            
        case 10:
        {
//            NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
//            if ([objectClassification count] > 1) {
//                UIAlertView *selectionAlert;
//                selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot play this game with multiple locations" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
//                [selectionAlert show];
//
//
//                return;
//            }

            
            NSLog(@"Move It"); // Dragging Images
            [[CCDirector sharedDirector] end];
            [[CCDirector sharedDirector].view removeFromSuperview];
            if (glviews) {
                [glviews removeFromSuperview];
                glviews = Nil;
    
            }
            if (onlyForExpressIt || onlyForWriteIT) {
                return;
            }

            if (multipleClassifications) {
                UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot play this game with multiple locations" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                [selectionAlert show];

                return;
            }

            MoveItViewController *moveItVC= [sb instantiateViewControllerWithIdentifier:@"PickItIdentifier"];
            moveItVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:moveItVC animated:NO completion:NULL];
        }
            break;
            
        default:
            break;
    }    

}

-(void) videoTutorialDone{

    NSLog(@"Movie Completed");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
    timerLoadGames = [[NSTimer scheduledTimerWithTimeInterval:0.1
                                                                target:self
                                                                selector:@selector(loadGame)
                                                                userInfo:nil
                                                                repeats:NO]retain];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_imgViewRightArrow setImage:[UIImage imageNamed:@"arrowRight.png"]];
//    [_imgViewLeftArrow setImage:[UIImage imageNamed:@"arrowLeft.png"]];
    [_imgViewLeftArrow setImage:Nil];
    
    [PlayGroundScoll setCanCancelContentTouches:YES];
     PlayGroundScoll.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    PlayGroundScoll.scrollEnabled = YES;
    PlayGroundScoll.pagingEnabled = NO;
    
    // load all the images from our bundle and add them to the scroll view
    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
    NSUInteger i;
    for (i = 1; i <= pNumImages; i++)
    {
        NSString *imageName = [NSString stringWithFormat:@"game%d.png", i];
        UIImage *image = [UIImage imageNamed:imageName];
        UIImageView *imageView = [[[UIImageView alloc] initWithImage:image] autorelease];
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 300)];
//        [imageView setImage:image];
        // setup each frame to a default height and width, it will be properly placed when we call "updateScrollList"
        CGRect rect = imageView.frame;
        rect.size.height = pScrollObjHeight;
        rect.size.width = pScrollObjWidth;
        //imageView.frame = rect;
        imageView.tag = i;	// tag our images for later use when we place them in serial fashion
        
        UIButton *button = [[UIButton alloc] initWithFrame:rect];

        [button setImage:image forState:UIControlStateNormal];
        [button setImage:image forState:UIControlStateHighlighted];
        
        //create the touch event target, i am calling the 'productImagePressed' method 
        [button addTarget:self action:@selector(GameImagePressed:) forControlEvents:UIControlEventTouchUpInside];

         button.tag = i;

        
        if (i == 12) {
              [button setEnabled:NO];
        }
        
        if (((cId > 989) && (cId < 999)) || ((cId > 999) && (cId < 1600))) {
               if (i == 12) {
                   [button setEnabled:YES];            
               }
        }
        
        NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
        cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
        if (cId == 60 && activeLanguage == 2) {
            if (i == 4) {
//                [button setEnabled:NO];
            }
        }
        
#warning Onlyu for New concept version
        if (i == 12) {
            [button setEnabled:YES];
        }
        
        if (cId > 989 && cId < 999) {
            if(i == 6)
            {
                [button setEnabled:NO];
            }
        }
        



        
        if ([ActiveGrade isEqualToString:@"KG1"]) {
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 1) {
                    if ([[objGrammarId objectAtIndex:0] isEqualToString:@"2340"]) {
                        if (i == 1) {
                            [button setEnabled:YES];
                        }
                        else{
                            [button setEnabled:NO];
                        }
                    }
                }
            }
        }
        
        
        if ([ActiveGrade isEqualToString:@"KG2"]) {
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 1) {
                    if ([[objGrammarId objectAtIndex:0] isEqualToString:@"1310"]) {
                        if (i == 1) {
                            [button setEnabled:YES];
                        }
                        else{
                            [button setEnabled:NO];
                        }
                    }
                }
            }
        }
        
        if ([ActiveGrade isEqualToString:@"G1"]) {
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 1) {
                    if ([[objGrammarId objectAtIndex:0] isEqualToString:@"1380"] || [[objGrammarId objectAtIndex:0] isEqualToString:@"1370"] || [[objGrammarId objectAtIndex:0] isEqualToString:@"2330"]) {
//                        if (i == 5) {
//                            [button setEnabled:NO];
//                        }
//                        else{
//                            [button setEnabled:YES];
//                        }
                    }
                }
            }
        }
        
        
        
        
        
        if ([ActiveGrade isEqualToString:@"G2"]) {
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 1) {
                    if (
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1170"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1180"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1190"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1200"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1210"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1220"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1270"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1260"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1290"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1340"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"1370"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"2350"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"2360"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"2370"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"2380"]
                        )
                    {
//                        if (i == 5) {
//                            [button setEnabled:NO];
//                        }
//                        else{
//                            [button setEnabled:YES];
//                        }
                    }
                }
            }
        }
//        principle id 3020
//        grammar id 1370
        
        if ([ActiveGrade isEqualToString:@"KG1"]) {
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 1) {
                    if ([[objGrammarId objectAtIndex:0] isEqualToString:@"1380"] || [[objGrammarId objectAtIndex:0] isEqualToString:@"1370"] || [[objGrammarId objectAtIndex:0] isEqualToString:@"1360"]) {
//                        if (i == 5) {
//                            [button setEnabled:NO];
//                        }
//                        else{
//                            [button setEnabled:YES];
//                        }
                    }
                }
            }
        }
        
        if ([ActiveGrade isEqualToString:@"KG2"]) {
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 1) {
                    if ([[objGrammarId objectAtIndex:0] isEqualToString:@"1380"] || [[objGrammarId objectAtIndex:0] isEqualToString:@"1370"]) {
//                        if (i == 5) {
//                            [button setEnabled:NO];
//                        }
//                        else{
//                            [button setEnabled:YES];
//                        }
                    }
                }
            }
        }
        
        
         if ([ActiveGrade isEqualToString:@"KG0"]) {
            cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];
             if (cId > 990) {
                 if (i == 6 || i == 10) {
                     [button setEnabled:NO];
                 }
             }
        }
        
        
        if ([ActiveGrade isEqualToString:@"KG0"]) {
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 1) {
                    if (
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"294"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"295"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"296"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"297"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"298"] ||
                        [[objGrammarId objectAtIndex:0] isEqualToString:@"299"]
                        )
                    {
                        if (i == 7 || i == 8 || i == 9) {
                            [button setEnabled:YES];
                        }
                        else{
                            [button setEnabled:NO];
                        }
                    }
                }
            }
        }
        
        // MAP CONCEPT
        
        if ([ActiveGrade isEqualToString:@"KG1"]) {
            
            NSArray *objectPrincipleId = [[NSUserDefaults standardUserDefaults] objectForKey:PRINCIPLE_ID_ARRAY];
            if([objectPrincipleId isKindOfClass:[NSArray class]] == YES)
                if ([[objectPrincipleId objectAtIndex:0] isEqualToString:@"8770"]) {
                    NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
                    if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                            if ([objGrammarId containsObject:@"2940"] || [objGrammarId containsObject:@"2950"] || [objGrammarId containsObject:@"2960"] || [objGrammarId containsObject:@"2970"] || [objGrammarId containsObject:@"2980"] || [objGrammarId containsObject:@"2990"]){
                                if (i == 7 || i == 8 || i == 9) {
                                    [button setEnabled:YES];
                                }
                                else{
                                    [button setEnabled:NO];
                                }

                            }
                            
                    }
                }
            }
        
        
        
        
        //
        
        
        NSArray *objectPrincipleId = [[NSUserDefaults standardUserDefaults] objectForKey:PRINCIPLE_ID_ARRAY];
        if([objectPrincipleId isKindOfClass:[NSArray class]] == YES)
            if ([[objectPrincipleId objectAtIndex:0] isEqualToString:@"3020"]) {
                if (i == 12) {
                    [button setEnabled:YES];
                }
                else{
                    [button setEnabled:NO];
                }
            }
            else{
                if ([ActiveGrade isEqualToString:@"G2"]) {
                    if ([[objectPrincipleId objectAtIndex:0] isEqualToString:@"3010"]) {
                        if (i == MATCH_IT) {
                            [button setEnabled:NO];
                        }
                        else{
                            [button setEnabled:YES];
                        }
                    }
                    
                }
            }
    
        
//#warning Pop It Blocking.
//        if(i == COLOR_IT){
//            [button setEnabled:NO];
//        }
        
        if (cId > 989 && i == 6) {
            [button setEnabled:NO];
        }
        
        activeLanguage = [Common getCurrentLanguage];
        if (activeLanguage == ENGLISH) {
//        if ([ActiveGrade isEqualToString:@"KG1"]) {
            NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
            if([objGrammarId isKindOfClass:[NSArray class]] == YES){
                if ([objGrammarId count] == 1) {
                    if ([[objGrammarId objectAtIndex:0] isEqualToString:@"2940"]) {
                        if (i == 8 || i == 9 || i == 11) {
                            [button setEnabled:YES];
                        }
                        else{
                            [button setEnabled:NO];
                        }
                    }
                }
            }
        }
        
        
        [PlayGroundScoll addSubview:button];
    }
    
    [self layoutScrollImages];	// now place the photos in serial layout within the scrollview
    
}

- (void)viewDidUnload
{
    PlayGroundScoll = nil;
    
    [self setImgViewRightArrow:nil];
    [self setImgViewLeftArrow:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}



- (void)GameImagePressed:(id)sender {
    
    if (timerLoadGames) {
        [timerLoadGames invalidate];
        [timerLoadGames release];
        timerLoadGames = Nil;
    }
    // Loading Game
    UIButton *button = (UIButton *)sender;
    GameToBePlayed = button.tag;
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == NO) {
//        [self loadGame];
//        return;
//    }
    
    // Playing Relevant Video
    MovieViewController* moviePlayer;
    if (GameToBePlayed == 6 || GameToBePlayed == 10) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoTutorialDone) name:@"videoTutorialDone" object:nil];
        moviePlayer = [[MovieViewController alloc] init];
        
    }
    else{
        [self loadGame];
        return;
    }
    
    
    BOOL multipleClassifications = FALSE;
    BOOL onlyForExpressIt = FALSE;
    NSArray *objectClassification = [[NSUserDefaults standardUserDefaults] objectForKey:CLASSIFICATIONS_ARRAY];
    if([objectClassification isKindOfClass:[NSArray class]] == YES)
    {
        if ([objectClassification count] > 1) {
            multipleClassifications = true;
        }
    }
    
    NSArray *objectPrincipleId = [[NSUserDefaults standardUserDefaults] objectForKey:PRINCIPLE_ID_ARRAY];
    if([objectPrincipleId isKindOfClass:[NSArray class]] == YES)
    if ([[objectPrincipleId objectAtIndex:0] isEqualToString:@"3020"]) {
            onlyForExpressIt = true;
    }


    
    switch (button.tag) {
        case 3:
            if (onlyForExpressIt) {
                return;
            }
            NSLog(@"POP IT");
            NSString *movieName = @"POPIT";
            [self loadGame];
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];                
//            }

            break;
            
        case 8:
            if (onlyForExpressIt) {
                return;
            }

//            NSLog(@"Fix it IT");
//            movieName = @"FixIT";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
//            
            [self loadGame];
            
            break;
            
        case 11:
            if (onlyForExpressIt) {
                return;
            }

            NSLog(@"Type IT");
//            movieName = @"TypeIT";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
            [self loadGame];

            break;
            
        case 1:
            if (onlyForExpressIt) {
                return;
            }

//            NSLog(@"Write IT");
//            movieName = @"WriteIt";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
            [self loadGame];

            break;
            
        case 5:
            if (onlyForExpressIt) {
                return;
            }
            

//            if (multipleClassifications) {
//                UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot play this game with multiple locations" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
//                [selectionAlert show];
//
//                return;
////            }
//            NSLog(@"MatchIt");
//            movieName = @"MatchIt";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
                [self loadGame];

                break;
            
        case 6:
            if (onlyForExpressIt) {
                return;
            }

            //Pick It actuall
            if (multipleClassifications) {
                UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot play this game with multiple locations" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                [selectionAlert show];

                return;
            }

            movieName = @"PickIT";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
            

            break;
            
        case 9:
            if (onlyForExpressIt) {
                return;
            }

            NSLog(@"Coming Soon");
            
//                        movieName = @"MyPlayground";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
//
//            break;

//            NSLog(@"Hockey");
//            movieName = @"KickIT";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
            [self loadGame];
            break;
            
        case 4:
            if (onlyForExpressIt) {
                return;
            }

            NSLog(@"Coming Soon");
   
            
//            NSLog(@"Hockey");
//            movieName = @"hockeyVideo";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
//            break;
            [self loadGame];
            
        case 7:
            if (onlyForExpressIt) {
                return;
            }

            NSLog(@"Coming Soon");
            
            //                        movieName = @"MyPlayground";
            //            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
            //                [self loadGame];
            //            }
            //            else{
            //                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
            //                [self presentModalViewController:moviePlayer animated:NO];
            //                [moviePlayer playMovie:movieName];
            //            }
            //
            //            break;
            
//            NSLog(@"BAsket Ball");
//            movieName = @"BasketballVideo";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
//            break;
            [self loadGame];
        case 2:
            if (onlyForExpressIt) {
                return;
            }

//            NSLog(@"Spin it");
//            movieName = @"SpinIt";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
            [self loadGame];
            break;
            
        case 10:
            if (onlyForExpressIt) {
                return;
            }

            if (multipleClassifications) {
                UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"You cannot play this game with multiple locations" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                [selectionAlert show];

                return;
            }

            NSLog(@"Move It");
            movieName = @"MoveIT";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
            break;
            
        case 12:
            
            NSLog(@"Express It");
//            movieName = @"ExpressIT";
//            if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
//                [self loadGame];
//            }
//            else{
//                [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
//                [self presentModalViewController:moviePlayer animated:NO];
//                [moviePlayer playMovie:movieName];
//            }
            [self loadGame];
            break;
            
        default:
            break;
    }
}


// Write It Game
- (void) setInputText:(NSString*) text
{
    NSLog(@" new label : %@" , text);
    levelEntryTextField.text = text;
}

- (BOOL)textFieldShouldReturn:(UITextField*)textField 
{
    //Terminate editing
    [textField resignFirstResponder];
    return YES;
}

- (void) setKeyBoardVisibility: (BOOL) change
{
    if (change == TRUE) 
    {
        [levelEntryTextField becomeFirstResponder];
    }
    else
    {
        [levelEntryTextField resignFirstResponder];
    }
}

- (void) toggleKeyBoardVisibility
{
    BOOL curStatus = [levelEntryTextField isFirstResponder];
    
    if (curStatus) 
    {
        [levelEntryTextField resignFirstResponder];
        
    }
    else
    {
        [levelEntryTextField becomeFirstResponder];
    }
}
-(NSString *) userText{

    textFieldText = levelEntryTextField.text;
    return textFieldText;
}

+ (NSString*) getUserInput
{
    NSString *userText = levelEntryTextField.text;
    NSLog(@"User Entered %@",userText);
    return userText;
}

- (IBAction)gotoMainMenu:(id)sender {
    
    if (timerLoadGames) {
        [timerLoadGames invalidate];
        [timerLoadGames release];
        timerLoadGames = Nil;
    }
    
}

- (IBAction)actionMyCamp:(id)sender {
    if (timerLoadGames) {
        [timerLoadGames invalidate];
        [timerLoadGames release];
        timerLoadGames = Nil;
    }
    
    
    NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
    if([objGrammarId isKindOfClass:[NSArray class]] == YES){
        if ([objGrammarId count] == 1) {
            if ([[objGrammarId objectAtIndex:0] isEqualToString:@"1380"]) {
                UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Sorry, you cannot visit My Camp with selected Grammar Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                [selectionAlert show];
                return;
            }
        }
    }
    
    NSString* ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    if ([ActiveGrade isEqualToString:@"KG0"]) {
        NSArray *objGrammarId = [[NSUserDefaults standardUserDefaults] objectForKey:GRAMMAR_ID_ARRAY];
        if([objGrammarId isKindOfClass:[NSArray class]] == YES){
            if ([objGrammarId count] == 1) {
                if (
                    [[objGrammarId objectAtIndex:0] isEqualToString:@"294"] ||
                    [[objGrammarId objectAtIndex:0] isEqualToString:@"295"] ||
                    [[objGrammarId objectAtIndex:0] isEqualToString:@"296"] ||
                    [[objGrammarId objectAtIndex:0] isEqualToString:@"297"] ||
                    [[objGrammarId objectAtIndex:0] isEqualToString:@"298"] ||
                    [[objGrammarId objectAtIndex:0] isEqualToString:@"299"]
                    )
                {
                    UIAlertView *selectionAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Sorry, you cannot visit My Camp with selected Grammar Id" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil ];
                    [selectionAlert show];
                    return;
                    
                }
                
            }
        }
    }
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyCampControllerIderntifier"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];

}


- (IBAction)helpButton:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"MS006" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"myplayground" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) removeLabel
{
    [levelEntryTextField removeFromSuperview];    
}

- (void) specifyStartLevel
{
    [levelEntryTextField setText:@""];
    [self.view addSubview:levelEntryTextField];
    //[levelEntryTextField becomeFirstResponder];    
}

-(void) playHelpMovie{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"videoHelp"] == NO) {
        return;
    }
    
    NSString *movieName = @"Playground";
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"videoHelpOff%@",movieName]]){
        return;
    }
    
    MovieViewController* moviePlayer = [[MovieViewController alloc] init];
    [moviePlayer.moviePlayer.view setFrame:CGRectMake(0, 0, 1024, 768)];
    [self presentModalViewController:moviePlayer animated:NO];
    // Prep and play the movie
//    NSString *movieName = @"Playground";
    [moviePlayer playMovie:movieName];
    
}

-(void) viewDidAppear:(BOOL)animated{
    if (!showMovie) {
        [self playHelpMovie];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    showMovie = TRUE;
}


- (void)scrollViewDidScroll:(UIScrollView *)sender {
        
    if (sender.contentOffset.x < 600) {
        [_imgViewRightArrow setImage:[UIImage imageNamed:@"arrowRight.png"]];
        [_imgViewLeftArrow setImage:Nil];
    }
   
    if (sender.contentOffset.x > 600) {
        [_imgViewRightArrow setImage:[UIImage imageNamed:@"arrowRight.png"]];
        [_imgViewLeftArrow setImage:[UIImage imageNamed:@"arrowLeft.png"]];
    }    
    
    if (sender.contentOffset.x > 3300) {
        [_imgViewRightArrow setImage:Nil];
        [_imgViewLeftArrow setImage:[UIImage imageNamed:@"arrowLeft.png"]];
        return;
    }
}


- (void)dealloc {
    [_imgViewRightArrow release];
    [_imgViewLeftArrow release];
    [super dealloc];
}
@end
