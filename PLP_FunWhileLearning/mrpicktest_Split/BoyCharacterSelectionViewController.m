//
//  BoyCharacterSelectionViewController.m
//  MrPick
//
//  Created by Muhammad Mosib Asad on 8/25/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import "BoyCharacterSelectionViewController.h"

@interface BoyCharacterSelectionViewController ()

@end

@implementation BoyCharacterSelectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFireman setImage:[UIImage imageNamed:Nil]];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCharacterBasketBallPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"BasketBallBoy"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:@"boySelectedCurrent.png"]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFireman setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }

}

- (IBAction)btnCharacterDoctorPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"DoctorBoy"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:@"boySelectedCurrent.png"]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFireman setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:5 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }

}

- (IBAction)btnCharacterSimpleBoyPressed:(id)sender {
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:@"boySelectedCurrent.png"]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFireman setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
            [self showActionSheetForActiveCharacter];


}

- (IBAction)btnCharacterChefPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"ChefBoy"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:@"boySelectedCurrent.png"]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFireman setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:7 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }
}

- (IBAction)btnCharacterFootballPlayerPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FootBallBoy"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:@"boySelectedCurrent.png"]];
    [_imgViewFireman setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:11 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }
}

- (IBAction)btnCharacterFiremanPressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FiremanBoy"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFireman setImage:[UIImage imageNamed:@"boySelectedCurrent.png"]];
    [[NSUserDefaults standardUserDefaults] setInteger:9 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
}
}

- (IBAction)btnCharacterPolicePressed:(id)sender {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"PoliceMan"]) {
        [self showActionSheet];
    }
    else{
        
    [_imgViewArab setImage:[UIImage imageNamed:Nil]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:@"boySelectedCurrent.png"]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFireman setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:15 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
}

}

- (IBAction)btnCharacterArabPressed:(id)sender {
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"ArabBoy"]) {
        [self showActionSheet];
    }
    else{
    [_imgViewArab setImage:[UIImage imageNamed:@"boySelectedCurrent.png"]];
    [_imgViewBasketBall setImage:[UIImage imageNamed:Nil]];
    [_imgViewChef setImage:[UIImage imageNamed:Nil]];
    [_imgViewDoctor setImage:[UIImage imageNamed:Nil]];
    [_imgViewPolice setImage:[UIImage imageNamed:Nil]];
    [_imgViewSimpleBoy setImage:[UIImage imageNamed:Nil]];
    [_imgViewFootball setImage:[UIImage imageNamed:Nil]];
    [_imgViewFireman setImage:[UIImage imageNamed:Nil]];
    [[NSUserDefaults standardUserDefaults] setInteger:13 forKey:@"gender"];
    [[NSUserDefaults standardUserDefaults] synchronize];
                [self showActionSheetForActiveCharacter];
    }

}

-(void) showActionSheet{
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    UIActionSheet *action;
    if (activeLanguage == 1) {
        action = [[UIActionSheet alloc] initWithTitle:@"لتفعيل الشخصية يجب زيارة انجازاتي"
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:@"OK"
                                    otherButtonTitles:nil, nil];
    }
    
    else{
        action = [[UIActionSheet alloc] initWithTitle:@"To activate character visit 'My Achievements'"
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:@"OK"
                                    otherButtonTitles:nil, nil];
    }
    
    
    [action showFromRect:CGRectMake(0, 0, 200, 200) inView:self.view animated:NO];
    
}

-(void) showActionSheetForActiveCharacter{
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    UIActionSheet *action;
    if (activeLanguage == 1) {
        action = [[UIActionSheet alloc] initWithTitle:@"ستجد الشخصية المختارة في ملعبي"
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:@"OK"
                                    otherButtonTitles:nil, nil];
    }
    
    else{
        action = [[UIActionSheet alloc] initWithTitle:@"Chosen character is active. Check it in 'My Playground'"
                                             delegate:self
                                    cancelButtonTitle:nil
                               destructiveButtonTitle:@"OK"
                                    otherButtonTitles:nil, nil];
    }
    
    
    [action showFromRect:CGRectMake(0, 0, 200, 200) inView:self.view animated:NO];
    
}
- (void)dealloc {
    [_imgViewSimpleBoy release];
    [_imgViewChef release];
    [_imgViewFootball release];
    [_imgViewDoctor release];
    [_imgViewPolice release];
    [_imgViewBasketBall release];
    [_imgViewArab release];
    [_imgViewFireman release];
    [super dealloc];
}

-(void) viewWillAppear:(BOOL)animated{



}
- (void)viewDidUnload {
    [self setImgViewSimpleBoy:nil];
    [self setImgViewChef:nil];
    [self setImgViewFootball:nil];
    [self setImgViewDoctor:nil];
    [self setImgViewPolice:nil];
    [self setImgViewBasketBall:nil];
    [self setImgViewArab:nil];
    [self setImgViewFireman:nil];
    [super viewDidUnload];
}
@end
