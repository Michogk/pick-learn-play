//
//  InAppRageIAPHelper.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 12/16/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IAPHelper.h"

@interface InAppRageIAPHelper : IAPHelper {
    
}

+ (InAppRageIAPHelper *) sharedHelper;

@end