//
//  AddWordViewController.m
//  Mr.Pick
//
//  Created by Muhammad Mosib Asad on 9/28/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "AddWordViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "MyHomeViewController.h"
#import "VoiceRecordViewController.h"
#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]


@interface AddWordViewController ()

@end

@interface UIImagePickerController(Nonrotating)
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
@end

@implementation UIImagePickerController(Nonrotating)

//- (BOOL)shouldAutorotate
//{
//    return NO;
//}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortraitUpsideDown;
}

@end


@implementation AddWordViewController
@synthesize txtField;
@synthesize BtnRecordingStart;
@synthesize BtnListenRecording;
@synthesize BtnSaveRecording;
static int voiceId;


//static AddWordViewController *homeViewController;
//
//+ (AddWordViewController*)homeViewController {
//    if (homeViewController == nil) {
//        homeViewController = [[AddWordViewController alloc] initWithNibName:@"MainStoryboard" bundle:nil];
//        
//    }
//    return homeViewController;
//}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    
    [super viewDidLoad];
    lblStatusMsg.text = @"Stopped";
    progressView.progress = 0.0;
    [btnTakePhotoFromCamera setEnabled:NO];
    [btnTakePhotoFromGallery setEnabled:NO];
    [btnRecorderBegin setEnabled:NO];
    [btnRecorderEnd setEnabled:NO];
    [btnRecorderReplay setEnabled:NO];
    [btnSaveAddingWord setEnabled:NO];
    [_btnReady setEnabled:YES];
    
    

    
//    [BtnStartRecording setEnabled:NO];
//    [BtnSaveWord setEnabled:YES];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
//    [self setTxtField:nil];
    [self setBtnRecordingStart:nil];
    [self setBtnListenRecording:nil];
    [self setBtnSaveRecording:nil];
//    [self setBtnCancel:nil];
    [BtnStartRecording release];
    BtnStartRecording = nil;
//    [BtnSaveWord release];
//    BtnSaveWord = nil;
    [btnTakePhotoFromCamera release];
    btnTakePhotoFromCamera = nil;
    [btnTakePhotoFromGallery release];
    btnTakePhotoFromGallery = nil;
    [btnRecorderBegin release];
    btnRecorderBegin = nil;
    [btnRecorderEnd release];
    btnRecorderEnd = nil;
    [btnRecorderReplay release];
    btnRecorderReplay = nil;
    [btnSaveAddingWord release];
    btnSaveAddingWord = nil;
    [self setBtnReady:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)dealloc {
    [_btnReady release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
        
//    [txtField release];
    [BtnRecordingStart release];
    [BtnListenRecording release];
    [BtnSaveRecording release];
//    [BtnCancel release];
    [BtnStartRecording release];
//    [BtnSaveWord release];
    [btnTakePhotoFromCamera release];
    [btnTakePhotoFromGallery release];
    [btnRecorderBegin release];
    [btnRecorderEnd release];
    [btnRecorderReplay release];
    [btnSaveAddingWord release];
//    [super dealloc];
}
- (IBAction)takePhoto:(id)sender {
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    UIPopoverController *popoverController=[[UIPopoverController alloc] initWithContentViewController:imagePickerController];
    popoverController.delegate=self;
    imagePickerController.showsCameraControls = YES;
//    popoverController.contentViewController.view.transform = CGAffineTransformMakeRotation (M_PI/2.0);
    [popoverController presentPopoverFromRect:((UIButton *)sender).bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionLeft animated:YES];
}

- (IBAction)loadPhotoFromGallery:(id)sender {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
     imagePickerController.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    UIPopoverController *popoverController=[[UIPopoverController alloc] initWithContentViewController:imagePickerController];
    popoverController.delegate=self;
    [popoverController presentPopoverFromRect:((UIButton *)sender).bounds inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)btnGetReady:(id)sender {
    
    
    bool imageFeature =  [[NSUserDefaults standardUserDefaults] objectForKey:@"PHOTOFEATURE"];
    if (imageFeature == YES) {
        [btnTakePhotoFromCamera setEnabled:YES];
        [btnTakePhotoFromGallery setEnabled:YES];
    }
    else{
        [btnTakePhotoFromCamera setEnabled:NO];
        [btnTakePhotoFromGallery setEnabled:NO];
    }
    
    [btnRecorderBegin setEnabled:YES];
    [btnRecorderEnd setEnabled:YES];
    [btnRecorderReplay setEnabled:YES];
    [btnSaveAddingWord setEnabled:YES];
        [_btnReady setEnabled:NO];
    
    
    NSLog(@"text field text is %@",txtField.text);
    
    int    activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
    NSString *folderNameText = txtField.text;
    
    NSCharacterSet* englishSet = [NSCharacterSet characterSetWithCharactersInString:
                                  @"abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVXYZ"];
    
    NSCharacterSet* arabicSet = [NSCharacterSet characterSetWithCharactersInString:
                                 @"ابتثجخدذرزسشصضطظعغفقكلمنهوي"];
    
    if (activeLanguage == 1) {
        // Perform Arabic Test
        BOOL languageTest = TRUE;
        languageTest = [[folderNameText stringByTrimmingCharactersInSet:englishSet] isEqualToString:@""];
        
        if (languageTest) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            [alert setTitle:@"WARNING"];
            [alert setMessage:@"SORRY! You can only add Word in Arabic Language OR go to setup screen and switch language "];
            [alert addButtonWithTitle:@"OK"];
            [alert setDelegate:self];
            [alert show];
            [alert release];
            return;
        }
    }
    else{
        // Perform English Test
        BOOL languageTest = TRUE;
        languageTest = [[folderNameText stringByTrimmingCharactersInSet:arabicSet] isEqualToString:@""];
        
        
        if (languageTest) {
            UIAlertView *alert = [[UIAlertView alloc] init];
            [alert setTitle:@"WARNING"];
            [alert setMessage:@"SORRY! You can only add Word in English Language OR go to setup screen and switch language"];
            [alert addButtonWithTitle:@"OK"];
            [alert setDelegate:self];
            [alert show];
            [alert release];
            return;
        }
    }
    
    if (![txtField.text isEqualToString:@""]) {
        
        
        [txtField resignFirstResponder];
        [self.txtField resignFirstResponder];
        
        int isSentence = 0;
        NSString *enteredword = txtField.text;
        int Length = [enteredword length];
        for (int i = 0; i <Length ; i++) {
            NSString * newString = [enteredword substringWithRange:NSMakeRange(i, 1)];
            if ([newString isEqualToString:@" "])
            {
                isSentence = 1;
            }
        }
        
        NSLog(@"....is sentence is %d",isSentence);
        // Reading Database
        
        MrPDatabase *database = [[MrPDatabase alloc] init];
        int currentClassificationId = [MyHomeViewController assignClassificationId];
        //    NSArray *WordsInfoArray = [database ReadWordDatabase:currentClassificationId];
        
        NSArray *WordsInfoArray = [database ReadWordDatabase:currentClassificationId];
        [database release];
        database = Nil;
        
        
        if ([WordsInfoArray count] == 0) {
            int wordIdCounter = 0;
            NSString *class = [NSString stringWithFormat:@"%d",currentClassificationId];
            NSString *WordIdMake = [class stringByAppendingFormat:[NSString stringWithFormat:@"%d",wordIdCounter]];
            
            int wordId = [WordIdMake intValue];
            
            // Making Voice Id
            NSString *voiceLanguage = @"E";
            _voiceIdNumber = wordId;
            voiceId = _voiceIdNumber;
            imageId = _voiceIdNumber;
            NSString* voiceIdComp = [voiceLanguage stringByAppendingFormat:[NSString stringWithFormat:@"%d", _voiceIdNumber]];
            
            NSString *word = txtField.text;
            word = [word stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            MrPDatabase *databaseWriteMode = [[MrPDatabase alloc] init];
            [databaseWriteMode saveNewWordInFoler:word andWordId:wordId whereClassificationId:currentClassificationId andVoiceId:voiceIdComp andisSentence:isSentence andImageId:imageId];
            [databaseWriteMode release];
            databaseWriteMode = Nil;
            [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"Reload" object:self]];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CancelWord:) name:@"CancelWord" object:nil];
            
            //        VoiceRecordViewController *voiceRecordController = [[VoiceRecordViewController alloc] init];
            //        [self.view addSubview:voiceRecordController.view];
            
            
            // Save image in document Directory
//            NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsDirectory = [paths objectAtIndex:0];
//            NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%d.png",ActiveGrade,imageId]]; // Set coding Convention
//            UIImage *image = imageTakenView.image; // imageView is my image from camera
//            NSData *imageData = UIImagePNGRepresentation(image);
//            [imageData writeToFile:savedImagePath atomically:NO];
            [recorder stop];
            
            if (timer) {
                [timer invalidate];
                timer = nil;
            }
            
//            [self dismissModalViewControllerAnimated:NO];
            
        }
        
        else {
            int wordIdCounter = 0;
            prevWordId = 0;
            currentloadedWordId = 0;
            NSMutableArray *arrayOfAllWordIds = [[NSMutableArray alloc] init];
            for (MrPContentInfo *info in WordsInfoArray)
            {
                
//                [arrayOfAllWordIds addObject:[NSString stringWithFormat:@"%d",info.wordId]];
                int class = [[NSString stringWithFormat:@"%d",currentClassificationId] intValue];
                int wordTestIfAvailable = [[NSString stringWithFormat:@"%d%d",class,wordIdCounter] intValue];

                if (wordTestIfAvailable != info.wordId) {
////                    forceAssigning = TRUE;
                    break;
                }
                
                wordIdCounter = wordIdCounter + 1;
                
                
                
                
//                int wordIdsAlreadyAssigned = [[NSString stringWithFormat:@"%d",info.wordId] intValue];
//                NSLog(@"%d Word Id assigned is %d",wordIdCounter,wordIdsAlreadyAssigned);
//
//                
//                
//                
//                if (wordIdsAlreadyAssigned - 2 == prevWordId) {
//                    forceAssigning = TRUE;
//                    forcedAssignWordIdtoFillGap = wordIdsAlreadyAssigned - 1;
//                    for (MrPContentInfo *infos in WordsInfoArray)
//                    {
//                        int wordIdsAll = [[NSString stringWithFormat:@"%d",infos.wordId] intValue];
//                        NSLog(@"wordIdsAll is %d and forcedAssignWordIdToFillGap is %d",wordIdsAll,forcedAssignWordIdtoFillGap);
//                        if (wordIdsAll == forcedAssignWordIdtoFillGap) {
//                            forceAssigning = false;
//                        }
//                    }
//                    
//                }
//                else{
//                    prevWordId = wordIdsAlreadyAssigned;
//                }
            }
            
            
            
            
            NSLog(@"WordIdCounter is %d",wordIdCounter);
            if (wordIdCounter < 15)
            {
                //                cancetinate currentClassificationId and wordIdCounter and put in wordId;
                NSString *class = [NSString stringWithFormat:@"%d",currentClassificationId];
                NSString *WordIdMake = [class stringByAppendingFormat:[NSString stringWithFormat:@"%d",wordIdCounter]];
                
                int wordId = [WordIdMake intValue];
//                if (forceAssigning) {
//                    wordId = forcedAssignWordIdtoFillGap;
//                    forceAssigning = false;
//                }
                NSLog(@"Word Make Is %d",wordId);
                NSString *voiceLanguage = @"E";
                _voiceIdNumber = wordId;
                voiceId = _voiceIdNumber;
                
                imageId = _voiceIdNumber;
                NSString* voiceIdComp = [voiceLanguage stringByAppendingFormat:[NSString stringWithFormat:@"%d", _voiceIdNumber]];
                
                NSString *word = txtField.text;
                word = [word stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                MrPDatabase *databaseWriteMode = [[MrPDatabase alloc] init];
                [databaseWriteMode saveNewWordInFoler:word andWordId:wordId whereClassificationId:currentClassificationId andVoiceId:voiceIdComp andisSentence:isSentence andImageId:imageId];
                [databaseWriteMode release];
                databaseWriteMode = Nil;
                [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"Reload" object:self]];
                
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CancelWord:) name:@"CancelWord" object:nil];
                
                //            VoiceRecordViewController *voiceRecordController = [[VoiceRecordViewController alloc] init];
                //            [self.view addSubview:voiceRecordController.view];
                
                // Save image in document Directory
//                NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
//                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                NSString *documentsDirectory = [paths objectAtIndex:0];
//                NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%d.png",ActiveGrade,imageId]]; // Set coding Convention
//                UIImage *image = imageTakenView.image; // imageView is my image from camera
//                NSData *imageData = UIImagePNGRepresentation(image);
//                [imageData writeToFile:savedImagePath atomically:NO];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] init];
                int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
                if (activeLanguage == 2) {
                    [alert setTitle:@"WARNING"];
                    [alert setMessage:@"You have reached maximum Word Limit"];
                    [alert addButtonWithTitle:@"OK"];
                }
                else {
                    [alert setTitle:@"تحذير !"];
                    [alert setMessage:@"عدد الكلمات الاقصى "];
                    [alert addButtonWithTitle:@"موافق"];
                    
                }
                
                
                [alert setDelegate:self];
                [alert show];
                [alert release];
            }
            
            [recorder stop];
            
            if (timer) {
                [timer invalidate];
                timer = nil;
            }
            
//            [self dismissModalViewControllerAnimated:NO];
            

            
            return;        
        }         
        
    }
    
    

    
  
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    imageTakenView=[[UIImageView alloc] initWithFrame:CGRectMake(120, 315, 315, 200)];
    imageCamera = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    NSData *imageAsData = UIImageJPEGRepresentation(imageCamera, 0.3);
    UIImage *downsampledImaged = [UIImage imageWithData:imageAsData];
    imageTakenView.image=downsampledImaged;
    
    CGRect rect = CGRectMake(0.0, 0.0, 230   , 230);
    UIGraphicsBeginImageContext(rect.size);
    [downsampledImaged drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    imageTakenView.image=img;
    
    
    
    
//    imageTakenView.image=imageCamera;
    [self.view addSubview:imageTakenView];
    
    // Access the uncropped image from info dictionary
    // imageCamera = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    // Save image in Photo Album
//    UIImageWriteToSavedPhotosAlbum(imageCamera, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    //    temp Save Image for displaying to user
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"CurrentImage.png"]; // Set coding Convention
//    UIImage *image = imageTakenView.image; // imageView is my image from camera
//    NSData *imageData = UIImagePNGRepresentation(image);
//    [imageData writeToFile:savedImagePath atomically:NO];

    
    // Get Image from directory
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:@"CurrentImage.png"];
//    UIImage *img = [UIImage imageWithContentsOfFile:getImagePath];
    
    
}

//Just for testing that image has been saved or not, uncomment the code below
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;
    
    // Unable to save the image
    if (error){
        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    alert.tag = 20;
    }
    else // All is well
    {
        alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                           message:@"Image saved to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    alert.tag = 20;
        
        

        
        
    }
    [alert show];
    
    
//    [alert release];
}

- (IBAction)startRecording:(id)sender {
      
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
    lblStatusMsg.text = @"Stopped";
    progressView.progress = 1.0;
    AudioSessionSetActive(NO);
    
    NSString *ActiveGrade  = [[NSUserDefaults standardUserDefaults] stringForKey:@"activeGrade"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%d.png",ActiveGrade,imageId]]; // Set coding Convention
    UIImage *image = imageTakenView.image; // imageView is my image from camera
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:savedImagePath atomically:NO];
    NSLog(@"Reloading after adding image");
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"Reload" object:self]];
    NSLog(@"Reloaded");
     [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"CancelWord" object:self]];
//    [self dismissModalViewControllerAnimated:NO];
    
    
  }

- (IBAction)ListenRecordedVoice:(id)sender {

}

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}

- (IBAction)SaveRecordingVoice:(id)sender {
    
    // Saving Word
}

- (IBAction)CancelWord:(id)sender {
    
    [self dismissModalViewControllerAnimated:NO];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 20) {
        
    }
    else{
        [self dismissModalViewControllerAnimated:NO];
    }
    

}

+(NSString *) getVoiceId{
    NSString *voiceLanguage = @"E";        
    NSString* voiceIdComp = [voiceLanguage stringByAppendingFormat:[NSString stringWithFormat:@"%d", voiceId]];
    
    return voiceIdComp;

}




- (void) handleTimer
{
    progressView.progress += .07/5;
    if(progressView.progress == 1.0)
    {
        [timer invalidate];
        timer = nil;
        lblStatusMsg.text = @"Stopped";
    }
}

- (IBAction)CancelRecording:(id)sender {
    
    [recorder stop];
    
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
    
//    [self.view removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] postNotification: [NSNotification notificationWithName:@"CancelWord" object:self]];
}

- (IBAction) startRecording
{
    NSLog(@"start Recording");
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *err = nil;
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&err];
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    [audioSession setActive:YES error:&err];
    err = nil;
    if(err){
        NSLog(@"#audioSession: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        return;
    }
    
    NSLog(@"#Record Setting");
    recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue :[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];
    
    // We can use 44100, 32000, 24000, 16000 or 12000 depending on sound quality
    [recordSetting setValue:[NSNumber numberWithFloat:16000.0] forKey:AVSampleRateKey];
    
    
    [recordSetting setValue:[NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey];
    
//    NSLog(@"Crash test log before");
    NSString *voiceId = [AddWordViewController getVoiceId];
    NSLog(@"#Voice Id %@,",voiceId);
    NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
    recorderFilePath = [[NSString stringWithFormat:@"%@/%@/%@.caf", DOCUMENTS_FOLDER,activeGrade,voiceId] retain];
    
    NSLog(@"#recorderFilePath: %@",recorderFilePath);
    
    NSURL *url = [NSURL fileURLWithPath:recorderFilePath];
    
    err = nil;
    
    NSData *audioData = [NSData dataWithContentsOfFile:[url path] options: 0 error:&err];
    if(audioData)
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        [fm removeItemAtPath:[url path] error:&err];
    }
    
    err = nil;
    recorder = [[ AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&err];
    if(!recorder){
        NSLog(@"#recorder: %@ %d %@", [err domain], [err code], [[err userInfo] description]);
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: [err localizedDescription]
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    
    //prepare to record
    [recorder setDelegate:self];
    [recorder prepareToRecord];
    recorder.meteringEnabled = YES;
    
    BOOL audioHWAvailable = audioSession.inputIsAvailable;
    if (! audioHWAvailable) {
        UIAlertView *cantRecordAlert =
        [[UIAlertView alloc] initWithTitle: @"Warning"
                                   message: @"Audio input hardware not available"
                                  delegate: nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [cantRecordAlert show];
        [cantRecordAlert release];
        return;
    }
    
    // start recording
    [recorder recordForDuration:(NSTimeInterval) 10];
    //            lblStatusMsg.font = [UIFont fontWithName:@"Arial" size:22];
    UIFont *font = [UIFont systemFontOfSize: 22];
    [lblStatusMsg setFont:font];
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        lblStatusMsg.text = @"Recording...";
    }
    else {
        lblStatusMsg.text = @"تسجيل...";
        
    }
    
    progressView.progress = 0.0;
    timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(handleTimer) userInfo:nil repeats:YES];
}

- (IBAction) stopRecording
{
    [recorder stop];
    
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
    lblStatusMsg.text = @"Stopped";
    progressView.progress = 1.0;
    AudioSessionSetActive(NO);
    
    
}

- (IBAction)playSound
{
    if(!recorderFilePath){
        NSString *voiceId = [AddWordViewController getVoiceId];
        NSString* activeGrade = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeGrade"];
        recorderFilePath = [[NSString stringWithFormat:@"%@/%@/%@.caf", DOCUMENTS_FOLDER,activeGrade,voiceId] retain];
    }
    
    if(soundID)
    {
        AudioServicesDisposeSystemSoundID(soundID);
    }
    
    //Get a URL for the sound file
    NSURL *filePath = [NSURL fileURLWithPath:recorderFilePath isDirectory:NO];
    
    
    
    if (player) {
        [player stop];
        [player release];
        player = Nil;
    }
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
    player.numberOfLoops = 0;
    player.delegate = self;
    [player play];
    
    
    
    
    //Use audio sevices to create the sound
    //	AudioServicesCreateSystemSoundID((CFURLRef)filePath, &soundID);
    
    //Use audio services to play the sound
    //	AudioServicesPlaySystemSound(soundID);
    
    
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *) aRecorder successfully:(BOOL)flag
{
    NSLog (@"audioRecorderDidFinishRecording:successfully:");
    [timer invalidate];
    timer = nil;
    lblStatusMsg.text = @"Stopped";
    progressView.progress = 1.0;
}



@end
