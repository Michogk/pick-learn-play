//
//  Common.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 22/11/2013.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Common : NSObject


+(NSString*) getPinNumber;
+(void)showAllert;
+ (id) loadNibName:(NSString*)nibName owner:(id)owner options:(NSDictionary*)options;
+(int) getCurrentLanguage;
@end
