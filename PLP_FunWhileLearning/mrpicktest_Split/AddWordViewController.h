//
//  AddWordViewController.h
//  Mr.Pick
//
//  Created by Muhammad Mosib Asad on 9/28/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AddWordViewController : UIViewController<AVAudioRecorderDelegate,AVAudioPlayerDelegate,UIPickerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UIPopoverControllerDelegate>{

    IBOutlet UIButton *BtnSaveWord;
    UIImage *imageCamera;
    IBOutlet UIButton *BtnStartRecording;
    IBOutlet UITextField *txtField;
    int _voiceIdNumber;
    int prevWordId;
    int currentloadedWordId;
    BOOL forceAssigning;
    int forcedAssignWordIdtoFillGap;
    UIImageView *imageTakenView;
    
    IBOutlet UIButton *btnTakePhotoFromCamera;
    IBOutlet UIButton *btnTakePhotoFromGallery;
    AVAudioPlayer *player;
    IBOutlet UIProgressView *progressView;
    IBOutlet UILabel *lblStatusMsg;
    NSMutableDictionary *recordSetting;
    NSMutableDictionary *editedObject;
    NSString *recorderFilePath;
    AVAudioRecorder *recorder;    
    SystemSoundID soundID;
    NSTimer *timer;
    int imageId;
    
    IBOutlet UIButton *btnSaveAddingWord;
    IBOutlet UIButton *btnRecorderReplay;
    IBOutlet UIButton *btnRecorderEnd;
    IBOutlet UIButton *btnRecorderBegin;
}
@property (retain, nonatomic) IBOutlet UITextField *txtField;
@property (retain, nonatomic) IBOutlet UIButton *BtnRecordingStart;
@property (retain, nonatomic) IBOutlet UIButton *BtnListenRecording;
@property (retain, nonatomic) IBOutlet UIButton *BtnSaveRecording;
- (IBAction)takePhoto:(id)sender;
- (IBAction)loadPhotoFromGallery:(id)sender;
- (IBAction)btnGetReady:(id)sender;

- (IBAction)startRecording:(id)sender;
- (IBAction)ListenRecordedVoice:(id)sender;
- (IBAction)CancelWord:(id)sender;
+(NSString *) getVoiceId;
@property (retain, nonatomic) IBOutlet UIButton *btnReady;


- (IBAction)CancelRecording:(id)sender;

- (IBAction) startRecording;
- (IBAction) stopRecording;
- (IBAction)playSound;
- (void) handleTimer;

//+ (MyHomeViewController*)homeViewController;


@end
