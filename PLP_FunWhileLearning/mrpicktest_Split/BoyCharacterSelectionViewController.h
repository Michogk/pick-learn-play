//
//  BoyCharacterSelectionViewController.h
//  MrPick
//
//  Created by Muhammad Mosib Asad on 8/25/13.
//  Copyright (c) 2013 SmartSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoyCharacterSelectionViewController : UIViewController<UIActionSheetDelegate>
- (IBAction)btnCharacterBasketBallPressed:(id)sender;
- (IBAction)btnCharacterDoctorPressed:(id)sender;
- (IBAction)btnCharacterSimpleBoyPressed:(id)sender;
- (IBAction)btnCharacterChefPressed:(id)sender;
- (IBAction)btnCharacterFootballPlayerPressed:(id)sender;
- (IBAction)btnCharacterFiremanPressed:(id)sender;
- (IBAction)btnCharacterPolicePressed:(id)sender;
- (IBAction)btnCharacterArabPressed:(id)sender;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewSimpleBoy;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewChef;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewFootball;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewDoctor;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewPolice;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewBasketBall;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewArab;
@property (retain, nonatomic) IBOutlet UIImageView *imgViewFireman;

@end
