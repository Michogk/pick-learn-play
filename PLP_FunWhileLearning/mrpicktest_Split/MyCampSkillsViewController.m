//
//  MyCampSkillsViewController.m
//  Mr.Pick
//
//  Created by Muhammad Mosib Asad on 10/5/12.
//  Copyright (c) 2012 SmartSoft. All rights reserved.
//

#import "MyCampSkillsViewController.h"
#import "MrPDatabase.h"
#import "MrPContentInfo.h"
#import "SimpleAudioEngine.h"
#import "DocumentDirectory.h"

@interface MyCampSkillsViewController ()

@end

@implementation MyCampSkillsViewController
@synthesize lblSynonym;
@synthesize lblOpposite;
@synthesize MyCampSkillsTable = _MyCampSkillsTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self playBackGroundMusic];
    prevWordComplete = true;
    
    int gId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentGrammarRule"];
    int pId = [[NSUserDefaults standardUserDefaults] integerForKey:@"currentPrincipleRule"];

    int position = 0;
    MrPDatabase *database = [[MrPDatabase alloc] init];    

        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
    NSArray *MrPContentEnglishArray = [database EnglishWordDatabaseArray:gId andpId:pId];
    _kNum = [MrPContentEnglishArray count];
    
    EnglishDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentEnglishArray count]];
    for (MrPContentInfo *info in MrPContentEnglishArray) {  

            [EnglishDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
            [EnglishDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"englishWord%d",position]];
            [EnglishDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]];
            [EnglishDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];          
            [EnglishDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];          
            [EnglishDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];          
            position++;      
        } 
    }
    
    else 
    {            
    NSArray *MrPContentArabicArray = [database ArabicWordDatabaseArray:gId andpId:pId];
    _kNum = [MrPContentArabicArray count];
    
    ArabicDic = [[NSMutableDictionary alloc] initWithCapacity:[MrPContentArabicArray count]];
    for (MrPContentInfo *info in MrPContentArabicArray) 
        {   
            [ArabicDic setObject:[NSNumber numberWithInt:info.wordId] forKey:[NSString stringWithFormat:@"wordId%d",position]];
            [ArabicDic setObject:info.arabicWord forKey:[NSString stringWithFormat:@"arabicWord%d",position]];
            [ArabicDic setObject:[NSNumber numberWithInt:info.isSentence] forKey:[NSString stringWithFormat:@"isSentence%d",position]];
            [ArabicDic setObject:info.voiceId forKey:[NSString stringWithFormat:@"voiceId%d",position]];    
            [ArabicDic setObject:info.synonymWord forKey:[NSString stringWithFormat:@"synonym%d",position]];          
            [ArabicDic setObject:info.oppositeWord forKey:[NSString stringWithFormat:@"opposite%d",position]];   
            position++;  
            }
    }
    
    NSString* gString = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"currentGrammarString"]];
    NSString* pString = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"currentPrincipleString"]];
    
    NSString *strg = [gString stringByAppendingFormat:[NSString stringWithFormat:@" / "]];
    NSString *GrammarLabelText = [strg stringByAppendingFormat:pString];    
//    [GrammarRuleLabel setFont:[UIFont fontWithName:@"Arial" size:37]];
    UIFont *font = [UIFont systemFontOfSize: 37];
    [GrammarRuleLabel setFont:font];
    [GrammarRuleLabel setBackgroundColor:[UIColor clearColor]];
    GrammarRuleLabel.textColor = [UIColor blackColor];
    GrammarRuleLabel.textAlignment = UITextAlignmentCenter;
    GrammarRuleLabel.text = GrammarLabelText;
    GrammarRuleLabel.adjustsFontSizeToFitWidth = YES;
    
    
    
    font = [UIFont systemFontOfSize: 20];
    [lblSynonym setFont:font];
    [lblSynonym setBackgroundColor:[UIColor clearColor]];
    lblSynonym.textColor = [UIColor blackColor];
    lblSynonym.textAlignment = UITextAlignmentCenter;
    lblSynonym.adjustsFontSizeToFitWidth = YES;
    
    font = [UIFont systemFontOfSize: 20];
    [lblOpposite setFont:font];
    [lblOpposite setBackgroundColor:[UIColor clearColor]];
    lblOpposite.textColor = [UIColor blackColor];
    lblOpposite.textAlignment = UITextAlignmentCenter;
    lblOpposite.adjustsFontSizeToFitWidth = YES;
    
//    [CellText setFont:[UIFont fontWithName:@"Arial" size:37]];
    font = [UIFont systemFontOfSize: 37];
    [CellText setFont:font];
    [CellText setBackgroundColor:[UIColor clearColor]];
    CellText.textColor = [UIColor blackColor];
    CellText.textAlignment = UITextAlignmentCenter;
    CellText.adjustsFontSizeToFitWidth = YES;
    
    [_MyCampSkillsTable setDataSource:self];
    [_MyCampSkillsTable setDelegate:self];

     self.view.userInteractionEnabled = YES;
    characterDic = [[NSMutableDictionary alloc] initWithCapacity:20];
    
    charactersView = [[UIView alloc] initWithFrame:CGRectMake(0, 520, 1024, 200)];
    [charactersView setBackgroundColor:[UIColor clearColor]];
    charactersView.userInteractionEnabled = YES;

    [self.view addSubview:charactersView];
    
    _completeWordArray = [[NSMutableArray alloc] initWithCapacity:60];
    
    
    selectedCellTxt = [[UILabel alloc] initWithFrame:CGRectMake(70, 120, 900, 100)];
    font = [UIFont systemFontOfSize: 45];
    [selectedCellTxt setFont:font];
    selectedCellTxt.adjustsFontSizeToFitWidth = YES;
    [selectedCellTxt setBackgroundColor:[UIColor clearColor]];
    selectedCellTxt.textColor = [UIColor blackColor];
    selectedCellTxt.textAlignment = UITextAlignmentCenter;
    [self.view addSubview:selectedCellTxt];
    
    UIButton *hearWordBtn = [UIButton buttonWithType:UIButtonTypeCustom]; 
    [hearWordBtn setBackgroundColor:[UIColor clearColor]];
    CGRect size = CGRectMake(70, 120, 900, 100);
    hearWordBtn.frame = size;
    [hearWordBtn addTarget:self action:@selector(hearSelectedWord) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:hearWordBtn]; 
    
    if ([ArabicDic count] == 0 && [EnglishDic count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] init];

        int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];        
        if (activeLanguage == 2) {
            [alert setTitle:@"CAUTION"]; 
            [alert setMessage:@"Please select any Grammar and Principle Rule and come back again"];
            [alert addButtonWithTitle:@"OK"];
        }                
        else {
            [alert setTitle:@"تحذير !"]; 
            [alert setMessage:@"الرجاء اختيار فاعدة درس ونوع القاعده وحاول مرة اخرى"];
            [alert addButtonWithTitle:@"موافق"];
            
        }
        [alert setDelegate:self];
        [alert setTag:6];
        [alert show];
        [alert release];
        return;
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{    
        [self dismissModalViewControllerAnimated:NO];
}

- (void)labelDragged:(UIPanGestureRecognizer *)gesture
{
    UILabel *label2 = (UILabel *)gesture.view;
    
    CGPoint translation = [gesture translationInView:label2];
    
    // move label
    label2.center = CGPointMake(label2.center.x + translation.x, 
                                label2.center.y + translation.y);
    
//    [label2 setFont:[UIFont fontWithName:@"Arial" size:35]];
//    UIFont *font = [UIFont systemFontOfSize: 20];
    [label2 setFont:[UIFont boldSystemFontOfSize:25]];
//    [label2 setFont:font];
    
    NSLog(@"%f", [gesture locationInView:label2.superview].x);
    NSLog(@"%f", [gesture locationInView:label2.superview].y);
    
    int xLowerLimit = 50; // 113
    int yLowerLimit = 0; // 269
    int xUpperLimit = 50+590; // 654
    int yUpperLimit = 50; //345
    if ([gesture locationInView:label2.superview].x > xLowerLimit && [gesture locationInView:label2.superview].x < xUpperLimit && [gesture locationInView:label2.superview].y > yLowerLimit && [gesture locationInView:label2.superview].y < yUpperLimit) {
        [label2 removeFromSuperview];
        [_completeWordArray addObject:label2.text];
        NSString *guessWord = @"";
        int numberOfCharacters = [_completeWordArray count];
        for (int k = 0; k < numberOfCharacters; k++) {
            if (isSentence) {
                guessWord = [guessWord stringByAppendingFormat:[NSString stringWithFormat:@"%@ ",[_completeWordArray objectAtIndex:k]]];
            }
            else {
                guessWord = [guessWord stringByAppendingFormat:[NSString stringWithFormat:@"%@",[_completeWordArray objectAtIndex:k]]];
            }
            k = k+1;
        }
        
        CellText.text = guessWord;
        
        
    }
    
    [gesture setTranslation:CGPointZero inView:label2];
    
    if(gesture.state == UIGestureRecognizerStateEnded)
    {
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 105 + yLine, 60, 62)];
        [label1 setFont:[UIFont boldSystemFontOfSize:35]];
        if (isSentence) {
            label1.frame = CGRectMake(10 + count, 100 + yLine, 140, 38);
            [label1 setFont:[UIFont boldSystemFontOfSize:25]];
        }
        label1.text = label2.text;
//        [label1 setFont:[UIFont fontWithName:@"Arial" size:25]];
//        UIFont *font = [UIFont systemFontOfSize: 20];
        
//        [label1 setFont:font];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
            [label1 setBackgroundColor:[UIColor redColor]];
            label1.textColor = [UIColor whiteColor];
            label1.layer.borderColor = [UIColor redColor].CGColor;
        }
        else {
            [label1 setBackgroundColor:[UIColor blueColor]];
            label1.textColor = [UIColor whiteColor];
            label1.layer.borderColor = [UIColor blueColor].CGColor;
        }                                
        label1.textAlignment = UITextAlignmentCenter;
        label1.layer.borderWidth = 1.0;
        label1.layer.cornerRadius = 8;        
        label1.userInteractionEnabled = YES;        
        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc] 
                                            initWithTarget:self 
                                            action:@selector(labelDragged:)] autorelease];
        [label1 addGestureRecognizer:gesture];
        [charactersView addSubview:label1];
        count = count + 65;
        if (isSentence) {
            count = count + 100;
        }
        if (count > 950) {
            if (isSentence) {
                yLine = yLine + 45;
            }
            else{
                yLine = yLine + 75;
            }
            
            count = 10;
        }
        
        if (count == 10 && yLine > 46) {
            if (isSentence) {
                yLine = 20;
            }
            else{
                yLine = 1;
            }
            
            
            count = 10;
        }
        [label2 removeFromSuperview];
    }

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;   
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _kNum;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        NSString *kCellIdentifier = @"MyGrammarRules";
        
        _grammarCell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
        if (_grammarCell == nil)
        {            
            _grammarCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
            _grammarCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
         // Language Check!
    self.MyCampSkillsTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    _grammarCell.textLabel.textAlignment = UITextAlignmentCenter;
     [_grammarCell.textLabel setFont:[UIFont boldSystemFontOfSize:18]];
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        _grammarCell.textLabel.text = [EnglishDic objectForKey:[NSString stringWithFormat:@"englishWord%d",indexPath.row]]; 
        _grammarCell.tag = indexPath.row;
        return _grammarCell;
    }
    else
    {
        _grammarCell.textLabel.text = [ArabicDic objectForKey:[NSString stringWithFormat:@"arabicWord%d",indexPath.row]]; 
        _grammarCell.tag = indexPath.row;
        return _grammarCell;
    }              
    return _grammarCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    _currentWordIndex = indexPath.row; // currentWordIndex is for playing Sound ONLY;

    if (cheeringAnimationTimer) {
        [cheeringAnimationTimer invalidate];
        cheeringAnimationTimer = nil;
    }
    _selectedCell = indexPath.row;
        
        CellText.text = @"";
        
        if (_completeWordArray) {
            [_completeWordArray removeAllObjects];
        }
        
        if (charactersView) {
            [charactersView removeFromSuperview];
            [charactersView release];
            charactersView = nil;        
        }
        
    charactersView = [[UIView alloc] initWithFrame:CGRectMake(0, 460, 1024, 260)];
        [charactersView setBackgroundColor:[UIColor clearColor]];
        charactersView.userInteractionEnabled = YES;
        [self.view addSubview:charactersView];
        
        
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 1) {
            _correctText = [ArabicDic objectForKey:[NSString stringWithFormat:@"arabicWord%d",indexPath.row]]; 
            
            if ([[ArabicDic objectForKey:[NSString stringWithFormat:@"opposite%d",indexPath.row]] isEqualToString:@"0"]) {
                NSLog(@"Nothing for opposite Word");   
            }
            else {
                [lblOpposite setText:[NSString stringWithFormat:@"Opposite: %@",[ArabicDic objectForKey:[NSString stringWithFormat:@"opposite%d",indexPath.row]]]];
            }
            
            if ([[ArabicDic objectForKey:[NSString stringWithFormat:@"synonym%d",indexPath.row]] isEqualToString:@"0"]) {
                NSLog(@"Nothing for synonym Word");   
            }
            else {
                [lblOpposite setText:[NSString stringWithFormat:@" %@",[ArabicDic objectForKey:[NSString stringWithFormat:@"synonym%d",indexPath.row]]]];
            }
 
        }else {
            _correctText = [EnglishDic objectForKey:[NSString stringWithFormat:@"englishWord%d",indexPath.row]]; 
            
            if ([[EnglishDic objectForKey:[NSString stringWithFormat:@"opposite%d",indexPath.row]] isEqualToString:@"0"]) {
                NSLog(@"Nothing for opposite Word");  
                [lblOpposite setText:@""];
            }
            else {
                [lblOpposite setText:[NSString stringWithFormat:@"Opposite: %@",[EnglishDic objectForKey:[NSString stringWithFormat:@"opposite%d",indexPath.row]]]];
            }
            
            NSLog(@"index path.row is :%d and synonym is %@",indexPath.row,[EnglishDic objectForKey:[NSString stringWithFormat:@"synonym%d",indexPath.row]]);
            if ([[EnglishDic objectForKey:[NSString stringWithFormat:@"synonym%d",indexPath.row]] isEqualToString:@"0"]) {
                NSLog(@"Nothing for synonym Word"); 
                [lblSynonym setText:@""];
            }
            else {
                [lblSynonym setText:[NSString stringWithFormat:@" %@",[EnglishDic objectForKey:[NSString stringWithFormat:@"synonym%d",indexPath.row]]]];
            }
        }
    selectedCellTxt.text = _correctText;
//    selectedCellTxt.textColor = [UIColor whiteColor];
        NSLog(@"correct text is %@",_correctText);
        
        int noOfCharacters;
        isSentence = false;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 1) {
            if ([[ArabicDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_selectedCell]] intValue] == 1) {
                noOfCharacters =  [[_correctText componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] count];
                isSentence = TRUE;            
            }
            else {
                noOfCharacters = _correctText.length;
            }
        }
        else {
            if ([[EnglishDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_selectedCell]] intValue] == 1) {
                noOfCharacters =  [[_correctText componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] count];
                isSentence = TRUE;            
            }
            else {
                noOfCharacters = _correctText.length;
            }
        }

        
        
        if (characterDic) {
            [characterDic removeAllObjects];
        }
        
        
        // populatin character array with words/ characters
        NSMutableArray *charactersArray = [[NSMutableArray alloc] initWithCapacity:noOfCharacters]; // level - 1 is because we have by default level one and we dnt want extra word in level one
        if (isSentence) {    
            NSArray *sentenceArray = [_correctText componentsSeparatedByString: @" "];
            [charactersArray addObjectsFromArray:sentenceArray];                
        }
        else {
            
            unichar characters;
            for (int k = 0; k < noOfCharacters ; k++) {
                characters = [_correctText characterAtIndex:k];
                NSString * charactr = [NSString stringWithFormat:@"%C", characters]; 
                [charactersArray addObject:charactr];
            }
        }
        
        count = 10;
        yLine = 1;
        int randomCounter = noOfCharacters;
        if (noOfCharacters < 44) {
            yLine = yLine + 75;
        }
        
        for (int n = 0; n< noOfCharacters; n++) {                
            int randomNumer = arc4random() % randomCounter ;
            NSString *characterExtracted = [charactersArray objectAtIndex:randomNumer];
            [charactersArray removeObjectAtIndex:randomNumer];            
            [characterDic removeObjectForKey:[NSString stringWithFormat:@"%d",randomNumer]];
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 105 + yLine, 60, 62)];
            [label1 setFont:[UIFont boldSystemFontOfSize:35]];
            if (isSentence) {
                label1.frame = CGRectMake(10+count, 100 + yLine, 140, 38);
                [label1 setFont:[UIFont boldSystemFontOfSize:25]];
            }
            label1.text = characterExtracted;
            label1.tag = n;
//            [label1 setFont:[UIFont fontWithName:@"Arial" size:25]];
//            UIFont *font = [UIFont systemFontOfSize: 20];
//            [label1 setFont:font];
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
                [label1 setBackgroundColor:[UIColor redColor]];
                label1.textColor = [UIColor whiteColor];
                label1.layer.borderColor = [UIColor blueColor].CGColor;
            }
            else {
                [label1 setBackgroundColor:[UIColor blueColor]];
                label1.textColor = [UIColor whiteColor];
                label1.layer.borderColor = [UIColor blueColor].CGColor;
            }
            
            
            
            
            label1.textAlignment = UITextAlignmentCenter;
            label1.layer.borderWidth = 1.0;
            label1.layer.cornerRadius = 8;
            
            label1.userInteractionEnabled = YES;
            
            UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc] 
                                                initWithTarget:self 
                                                action:@selector(labelDragged:)] autorelease];
            [label1 addGestureRecognizer:gesture];
            [charactersView addSubview:label1];
            
            count = count + 65;
            if (isSentence) {
                count = count + 100;
            }
            if (count > 950) {
                if (isSentence) {
                    yLine = yLine + 45;
                }
                else{
                    yLine = yLine + 75;
                }
                
                count = 10;
            }            randomCounter = randomCounter - 1;
        }
}


- (void)viewDidUnload
{
    [MyCampSkillsTable release];
    MyCampSkillsTable = nil;
    [GrammarRuleLabel release];
    GrammarRuleLabel = nil;
    [CellText release];
    CellText = nil;
    [btnCheckAnswer release];
    btnCheckAnswer = nil;
    [self setLblSynonym:nil];
    [self setLblOpposite:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

- (void)dealloc {
    [MyCampSkillsTable release];
    [GrammarRuleLabel release];
    [CellText release];
    [btnCheckAnswer release];
    [lblSynonym release];
    [lblOpposite release];
    [super dealloc];
}
- (IBAction)ResetContent:(id)sender {
    
    if ([CellText.text length] > 0) {
        
        
        NSString *lastCharacter = [CellText.text substringFromIndex: [CellText.text length] - 1];
        if (isSentence) {
            lastCharacter = [_completeWordArray lastObject];
        }
        [_completeWordArray removeLastObject];
        [_completeWordArray removeLastObject];
        
        if ( [CellText.text length] > 0 && !isSentence)
            CellText.text = [CellText.text substringToIndex:[CellText.text length] - 1];
        
        if (isSentence && [CellText.text length] > 0) {
            NSLog(@"cell Text is %@",CellText.text);
            
            CellText.text = [CellText.text stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@ ",lastCharacter] withString:@""];            
            //            if ([cellText.text length] == 1) {
            //                cellText.text ;
            //            }
            
        }
        
        // adding character now
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(10+count, 105 + yLine, 60, 62)];
        [label1 setFont:[UIFont boldSystemFontOfSize:35]];
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2)
        {
            if ([[EnglishDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_selectedCell]] intValue] == 1) 
            {
                label1.frame = CGRectMake(10+count, 100+ yLine, 140, 38);
                [label1 setFont:[UIFont boldSystemFontOfSize:25]];
            }
        }
        else {
            if ([[ArabicDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_selectedCell]] intValue] == 1) 
            {
                label1.frame = CGRectMake(10+count, 100+ yLine, 140, 38);
                [label1 setFont:[UIFont boldSystemFontOfSize:25]];
            }
        }
//        if ([[EnglishDic objectForKey:[NSString stringWithFormat:@"isSentence%d",_selectedCell]] intValue] == 1) {
//            label1.frame = CGRectMake(10+count, 80+ yLine, 140, 30);
//        }
//        if (isSentence) {
//            
//        }
        label1.text = lastCharacter;
//        [label1 setFont:[UIFont fontWithName:@"Arial" size:25]];
//        UIFont *font = [UIFont systemFontOfSize: 20];
//        [label1 setFont:font];
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"MyCampBackground"] intValue] == 1) {
            [label1 setBackgroundColor:[UIColor redColor]];
            label1.textColor = [UIColor whiteColor];
            label1.layer.borderColor = [UIColor redColor].CGColor;
        }
        else {
            [label1 setBackgroundColor:[UIColor blueColor]];
            label1.textColor = [UIColor whiteColor];
            label1.layer.borderColor = [UIColor blueColor].CGColor;
        }
        
        
        
        
        label1.textAlignment = UITextAlignmentCenter;
        label1.layer.borderWidth = 1.0;
        label1.layer.cornerRadius = 8;
        
        label1.userInteractionEnabled = YES;
        
        UIPanGestureRecognizer *gesture = [[[UIPanGestureRecognizer alloc] 
                                            initWithTarget:self 
                                            action:@selector(labelDragged:)] autorelease];
        [label1 addGestureRecognizer:gesture];
        [charactersView addSubview:label1];
        
        count = count + 65;
        if (isSentence) {
            count = count + 100;
        }
        if (count > 950) {
            if (isSentence) {
                yLine = yLine + 45;
            }
            else{
                yLine = yLine + 75;
            }
            
            count = 10;
        }
        
        if (count == 10 && yLine > 46) {
            if (isSentence) {
                yLine = 20;
            }
            else{
                yLine = 1;
            }
            count = 10;
        }
    }
}
-(void) activateCheckButton{

    if (checkTimer) {
        [checkTimer invalidate];
        checkTimer = nil;
        [checkTimer release];
    }   
    [btnCheckAnswer setEnabled:YES];
}

- (IBAction)CheckCellTex:(id)sender {
    
    if (!checkTimer) 
    {            
        checkTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                                   target:self 
                                                                 selector:@selector(activateCheckButton) 
                                                                 userInfo:nil 
                                                                  repeats:YES]retain];
        [btnCheckAnswer setEnabled:NO];
    }
    
    NSString *cellText = CellText.text;
    
    NSLog(@"Correct text is \n|%@|\n|%@|",_correctText,cellText);
    if([_correctText isEqualToString:cellText] || [cellText isEqualToString:[NSString stringWithFormat:@"%@ ",_correctText]])
    {
        NSLog(@"Correct!"); 
                        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_1.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_1.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_1.png"]];
        }
        
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_1.png"]];
        }
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_5.png"]];
        }
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16) {
            cheeringImgView = [[UIImageView alloc] initWithFrame:CGRectMake(300, 0, 120, 120)];
            [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_1.png"]];
        }
        
        
        [cheeringImgView setBackgroundColor:[UIColor clearColor]];

        [charactersView addSubview:cheeringImgView];
        [charactersView setBackgroundColor:[UIColor clearColor]];
        cheeringAnimationCounter = 0;
        cheeringImageNumber = 1;
        if (!cheeringSoundTimer) 
        {            
            cheeringAnimationTimer = [[NSTimer scheduledTimerWithTimeInterval:0.2
                                                                   target:self 
                                                                 selector:@selector(changeImage) 
                                                                 userInfo:nil 
                                                                repeats:YES]retain];
        }
        [self playCheeringSound];
    }
    else {
    NSLog(@"Wrong Answer");
    [self playEncouragingSound];
    }
        
}
-(void) stopTimerAndMakeSoundButtonsClickable{
    if (cheeringSoundTimer) {
        [cheeringSoundTimer invalidate];
        cheeringSoundTimer = nil;
        [cheeringSoundTimer release];
    }
}
-(void) playCheeringSound{
    
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                               target:self 
                                                             selector:@selector(stopTimerAndMakeSoundButtonsClickable) 
                                                             userInfo:nil 
                                                              repeats:YES]retain];                
        
    }
    
    int randomNumer = arc4random() % 3 ;
    NSLog(@"Random Number is %d",randomNumer);
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC01";
        NSString *second = @"EC05";
        NSString *third = @"EC10";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
    }else {
        NSString *first = @"AC01";
        NSString *second = @"AC02";
        NSString *third = @"AC03";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
        
        
    }
    
}


-(void) playEncouragingSound{
    if (!cheeringSoundTimer) {
        cheeringSoundTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0
                                                               target:self 
                                                             selector:@selector(stopTimerAndMakeSoundButtonsClickable) 
                                                             userInfo:nil 
                                                              repeats:YES]retain];
        
    }
    
    int randomNumer = arc4random() % 3 ;
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"] == 2) {
        NSString *first = @"EC25";
        NSString *second = @"EC30";
        NSString *third = @"EC31";
        
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);  
        
    }else {
        NSString *first = @"AC04";
        NSString *second = @"AC05";
        NSString *third = @"AC06";
        NSString *cheeringSound;
        if (randomNumer == 0) {
            cheeringSound = first;
        }
        else 
            if(randomNumer == 1){
                cheeringSound = second;
            }
            else {
                cheeringSound = third;
            }
        
        NSString *squishPath = [[NSBundle mainBundle] pathForResource:cheeringSound ofType:@"mp3"];
        NSURL *squishURL = [NSURL fileURLWithPath:squishPath];
        SystemSoundID squishSoundID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
        AudioServicesPlaySystemSound(squishSoundID);
        
    }
}


-(void) changeImage{
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 1)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_7.png"]];
                
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_8.png"]];
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 1;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"Cheering_5.png"]];
                if (cheeringAnimationCounter > 9) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 9) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 2)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_6.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 8:
                cheeringImageNumber = 9;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_9.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 9:
                cheeringImageNumber = 10;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 10:
                cheeringImageNumber = 11;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 11:
                cheeringImageNumber = 1;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"GCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 18) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 3)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 4)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"BCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 10) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 5)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 6)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"DCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    
    // Chef Boy
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 7)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // CHEF GIRL
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 8)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"CCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // FIRE
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 9)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // Fire Girl
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 10)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FireCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // Football Boy
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 11)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 12)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"FCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    //ARAB BOY
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 13)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 14)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"ACheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    // POLICE BOY
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 15)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_6.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_7.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_8.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_9.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_10.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 6:
                cheeringImageNumber = 7;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_11.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 7:
                cheeringImageNumber = 8;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering_5.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender"] intValue] == 16)
    {
        switch (cheeringImageNumber)
        {
            case 1:
                cheeringImageNumber = 2;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_2.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 2:
                cheeringImageNumber = 3;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_3.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
            case 3:
                cheeringImageNumber = 4;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_4.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
            case 4:
                cheeringImageNumber = 5;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_5.png"]];
                
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                break;
            case 5:
                cheeringImageNumber = 6;
                cheeringAnimationCounter = cheeringAnimationCounter + 1;
                [cheeringImgView setImage:[UIImage imageNamed:@"PCheering-girl_1.png"]];
                if (cheeringAnimationCounter > 18) {
                    [cheeringAnimationTimer invalidate];
                    cheeringAnimationTimer = nil;
                    break;
                }
                
                break;
                
                
                
            default:
                NSLog(@"");
        }
        
        if (cheeringAnimationCounter > 12) {
            cheeringAnimationCounter = 0;
            cheeringImageNumber = 1;
            [cheeringImgView removeFromSuperview];
        }
        
    }
    
    
    

}

-(void) hearSelectedWord{
        
    NSString* soundName;
    int activeLanguage = [[NSUserDefaults standardUserDefaults] integerForKey:@"ActiveLanguage"];
//    int cId = [[NSUserDefaults standardUserDefaults] integerForKey:@"selectedClassificationId"];

    
    if (activeLanguage == 1) {
        if ([ArabicDic count] > 1) {
        soundName = [ArabicDic objectForKey:[NSString stringWithFormat:@"voiceId%d",_currentWordIndex]];        
        }
    }
    else {
        if ([EnglishDic count] > 1) {
            soundName = [EnglishDic objectForKey:[NSString stringWithFormat:@"voiceId%d",_currentWordIndex]];            
        }
    }
    
    NSString *squishPath = [DocumentDirectory playSkillsCampSound:soundName];    
    NSURL *squishURL = [NSURL fileURLWithPath:squishPath];

    if (campSkillsHearWordPlayer) {
        [campSkillsHearWordPlayer stop];
        [campSkillsHearWordPlayer release];
        campSkillsHearWordPlayer = nil;
    }
    
    campSkillsHearWordPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:squishURL error:nil];
    campSkillsHearWordPlayer.volume = 1;
    campSkillsHearWordPlayer.numberOfLoops = 0;
    campSkillsHearWordPlayer.delegate = self;
    
    if ([campSkillsHearWordPlayer prepareToPlay]) {
        [campSkillsHearWordPlayer play];
    }

//    SystemSoundID squishSoundID;
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)squishURL, &squishSoundID);
//    AudioServicesPlaySystemSound(squishSoundID);
    
} 

-(void) playBackGroundMusic{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"allBackgroundMusic"]) {
        return;
    }
    NSString* fileName = [NSString stringWithFormat:@"2-Camp"];
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"mp3"];
    
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    myCampSkillsPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    myCampSkillsPlayer.volume = 0.15;
    myCampSkillsPlayer.numberOfLoops = 20;
    myCampSkillsPlayer.delegate = self;
    
    if ([myCampSkillsPlayer prepareToPlay]) {
        [myCampSkillsPlayer play];            
    }    
}


- (IBAction)HelpButton:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"MS007" forKey:@"Help"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)helpURL:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@"mycamp" forKey:@"HelpURL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)gotoMainMenu:(id)sender {
    if (myCampSkillsPlayer) {
        [myCampSkillsPlayer stop];
        myCampSkillsPlayer = nil;
    }
    
    if (campSkillsHearWordPlayer) {
        [campSkillsHearWordPlayer stop];
        [campSkillsHearWordPlayer release];
        campSkillsHearWordPlayer = nil;
    }
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SkillsCampSound"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
